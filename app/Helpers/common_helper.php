<?php
    use App\Admin;
    use App\Model\Address\Country;
    use App\Model\Address\State;
    use App\Model\Address\City;
    use App\Model\School\School;
    use App\Model\School\SchoolBoard;
    use App\Model\Session\Session;
    use App\Model\Classes\Classes;
    use App\Model\CoScholastic\CoScholastic;
    use App\Model\Section\Section;
    use App\Model\Caste\Caste;
    use App\Model\Title\Title;
    use App\Model\Holiday\Holiday;
    use App\Model\Religion\Religion;
    use App\Model\Nationality\Nationality;
    use App\Model\SchoolGroup\SchoolGroup; 
    use App\Model\Student\Student;
    use App\Model\Student\StudentParent;
    use App\Model\Student\StudentAcademic;
    use App\Model\Student\StudentHealth;
    use App\Model\Designation\Designation;
    use App\Model\Staff\Staff;
    use App\Model\StaffRoles\StaffRoles;
    use App\Model\Subject\Subject;
    use App\Model\DocumentCategory\DocumentCategory; 
    use App\Model\Term\Term; // Model @Pratyush on 11  Aug 2018
    use App\Model\BookCupboard\BookCupboard; // Model @Pratyush on 13 Aug 2018
    use App\Model\BookCategory\BookCategory; // Model @Pratyush on 14 Aug 2018
    use App\Model\BookVendor\BookVendor;     // Model @Pratyush on 14 Aug 2018
    use App\Model\BookCupboardshelf\BookCupboardshelf;     // Model @Pratyush on 16 Aug 2018
    use App\Model\SubjectClassMapping\SubjectClassmapping;     // Model @Ashish on 22 Aug 2018
    use App\Model\SubjectSectionMapping\SubjectSectionMapping; 
    use App\Model\StudentSubjectManage\StudentSubjectManage; 
    use App\Model\Exam\Exam;     // Model @Ashish on 22 Aug 2018
    use App\Model\Stream\Stream; // Model
    use App\Model\Competition\Competition; // Model 
    use App\Model\Competition\CompetitionMapping; // Model 
    use App\Model\Brochure\Brochure; // Model 
    use App\Model\Admission\AdmissionForm;
    use App\Model\Admission\AdmissionFields;
    use App\Model\Admission\AdmissionData;
    use App\Model\Shift\Shift;   
    use App\Model\FeesCollection\Bank;
    use App\Model\FeesCollection\OneTime; // Model
    use App\Model\FeesCollection\RecurringHead; // Model
    use App\Model\FeesCollection\RTEHead; // Model
    use App\Model\ApiToken\ApiToken; // Model @Pratyush 11 Oct 2018
    use App\Model\FeesCollection\Fine; // Model
    use App\Model\School\ImprestAcConfig; // Model
    use App\Model\Student\ImprestAc; // Model
    use App\Model\Student\WalletAc; // Model
    use App\Model\FeesCollection\ConcessionMap; // Model
    use App\Model\FeesCollection\FeesStMap; // Model
    use App\Model\FeesCollection\Receipt;
    use App\Model\FeesCollection\ReceiptDetail;
    use App\Model\FeesCollection\Reason;
    use App\Model\Examination\ExamMap; // Model
    use App\Model\Examination\MarksCriteria; // Model
    use App\Model\Examination\GradeScheme; // Model
    use App\Model\LibraryMember\LibraryMember; // Model
    use App\Model\Examination\Grades; // Model
    use App\Model\Mediums\Mediums; // Model
    use App\Model\Student\StudentAttendance; // Model
    use App\Model\Staff\StaffAttendance; // Model
    use App\Model\Staff\StaffAttendanceDetails; // Model
    use App\Model\Hostel\Hostel; // Model
    use App\Model\Hostel\HostelBlock; // Model
    use App\Model\Hostel\HostelRoomCategory; // Model
    use App\Model\Hostel\HostelRooms; // Model
    use App\Model\Hostel\HostelFloor; // Model
    use App\Model\Hostel\HostelStudentMap; // Model
    use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model
    use App\Model\HomeworkGroup\HomeworkGroup; // Model
    use App\Model\TimeTable\TimeTable; // Model
    use App\Model\TimeTable\TimeTableMap; // Model
    use App\Model\Exam\ExamScheduleMap; // Model
    use App\Model\RoomNo\RoomNo; // Model
    use Illuminate\Support\Facades\Crypt;
    use App\Model\InventoryCategory\Category; // Model
    use App\Model\InventoryUnit\Unit; // Model
    use App\Model\InventoryVendor\Vendor; // Model
    use App\Model\InventoryItem\Items; // Model
    use App\Model\Transport\Vehicle; // Model
    use App\Model\Transport\Route; // Model
    use App\Model\Notification\Notification; // Model
    use App\Model\Recruitment\Job; // Model
    use App\Model\Payroll\Arrear; // Model
    use App\Model\Recruitment\RecruitmentForm; // Model
    use App\Model\Payroll\SalaryStructure; // Model
    use App\Model\Book\Book;
    use App\Model\Transport\MapStudentStaffVehicle;
    use App\Model\Permissions\MasterModule; // Model
    use App\Model\Permissions\Permissions; // Model
    use App\Model\Accounts\AccMainHead; // Model
    use App\Model\Accounts\AccSubHead; // Model
    use App\Model\Accounts\AccGroup; // Model
    use App\Model\Accounts\AccGroupHead; // Model

    use Session as LSession;
    
    use Carbon\Carbon;
 
    function p($p, $exit = 1)
    {
        echo '<pre>';
        print_r($p);
        echo '</pre>';
        if ($exit == 1)
        {
            exit;
        }
    }
    
    function set_session($session_id, $session_year)
    {
        $session_data = [];
        $session_data = array(
            'session_id'   => $session_id,
            'session_year' => $session_year,
        );
        LSession::put('set_session_year', $session_data);
        return LSession::get('set_session_year');
    }
    
    function get_loggedin_user_data()
    {
        $user_data  = array();
        $admin_user = Auth::guard('admin')->user();
        $staff_id   = "";
        $class_id   = Null;
        $section_id   = Null;
        $permissionId = 1;
        $permissionData = Permissions::find($permissionId);
        if(!empty($permissionData)){
            $permissions = explode(',',$permissionData->permissions);
        }
        
        if (!empty($admin_user['admin_id']))
        {
            $encrypted_school_id = $school_id = $student_id = $staff_id = $encrypted_staff_id = $staff_role_id = $imprest_ac_status = $encrypted_student_id = null;
            
            if($admin_user['admin_type'] == 0){
                $adminType = "Super Admin";
                
            } else if($admin_user['admin_type'] == 1){
                $adminType = "School Admin";
                
            } else if($admin_user['admin_type'] == 2){
                $adminType = "Staff Admin";

            }  else if($admin_user['admin_type'] == 3){
                $adminType = "Parent Admin";

                $parentdata = parent_info($admin_user['admin_id']);

                $get_student_info = get_student_info($parentdata['student_parent_id']);
                $current_student_session = get_current_student_session();

                if($current_student_session == ""){
                    $current_student_session['student_id'] = $get_student_info[0]['student_id'];
                }

                $parent_id        = get_encrypted_value($parentdata->student_parent_id, true);
                
                
                if(COUNT(Student::where('student_parent_id',$parentdata['student_parent_id'])->where('students.student_id',$current_student_session['student_id'])->get())) {
                    $student    = Student::where('student_parent_id',$parentdata['student_parent_id'])->where('students.student_id',$current_student_session['student_id'])
                        ->join('student_academic_info', function($join) {
                            $join->on('student_academic_info.student_id', '=', 'students.student_id');
                        })->first();

                    $school_id  = $student->school_id; 
                    $student_id = $student->student_id;  
                    $class_id  = $student->current_class_id; 
                    $section_id  = $student->current_section_id; 
                    $parent_id  = $parent_id; 
                    $encrypted_school_id = get_encrypted_value($student->school_id, true);
                    $encrypted_student_id = get_encrypted_value($student->student_id, true);
                }


            } else if($admin_user['admin_type'] == 4){

                $adminType = "Student Admin";
                // @Pratyush on 17 Aug 2018
                if(COUNT(Student::where('reference_admin_id',$admin_user['admin_id'])->get())) {
                    $student    = Student::where('reference_admin_id',$admin_user['admin_id'])
                        ->join('student_academic_info', function($join) {
                            $join->on('student_academic_info.student_id', '=', 'students.student_id');
                        })->first();

                    $school_id  = $student->school_id; 
                    $student_id = $student->student_id;  
                    $class_id  = $student->current_class_id; 
                    $section_id  = $student->current_section_id; 
                    $encrypted_school_id = get_encrypted_value($student->school_id, true);
                    $encrypted_student_id = get_encrypted_value($student->student_id, true);
                }
    
            }
            if(COUNT(School::where('school_id','=' ,1)->get()->toArray())) {
                $school = School::where('school_id','=' ,1)->get()->toArray();
                $school              = isset($school[0]) ? $school[0] : [];
                $encrypted_school_id = get_encrypted_value($school['school_id'], true);
                $school_id           = $school['school_id'];
                $imprest_ac_status   = $school['imprest_ac_status'];
            }
            if($admin_user['admin_type'] == 2){
                $staff    = Staff::where('reference_admin_id',$admin_user['admin_id'])->with('StaffClassAllocation')->first();
                $staff_id = $staff['staff_id'];
                $staff_role_id = explode(',',$staff['staff_role_id']);
                $class_id  = $staff['StaffClassAllocation']['class_id']; 
                $section_id  = $staff['StaffClassAllocation']['section_id']; 
                $encrypted_staff_id = get_encrypted_value($staff['staff_id'], true);
            }
            $user_data = array(
                'admin_id'              => $admin_user['admin_id'],
                'admin_name'            => $admin_user['admin_name'],
                'email'                 => $admin_user['email'],
                'admin_type'            => $admin_user['admin_type'],
                'admin_profile_img'     => $admin_user['admin_profile_img'],
                'admin_role'            => $adminType,
                'encrypted_school_id'   => $encrypted_school_id,
                'school_id'             => $school_id,
                'student_id'            => $student_id,
                'class_id'              => $class_id,
                'section_id'            => $section_id,
                'encrypted_student_id'  => $encrypted_student_id,
                'staff_id'              => $staff_id,
                'encrypted_staff_id'    => $encrypted_staff_id,
                'staff_role_id'         => $staff_role_id,
                'admin_role'            => $adminType,
                'parent_id'             => $parent_id,
                'imprest_ac_status'     => $imprest_ac_status,
                'permissions'           => $permissions
            );
        }
        return $user_data;
    }
    
    function last_query($raw = false, $last = false)
    {
        DB::enableQueryLog();
        $log = DB::getQueryLog();
    
        if ($raw)
        {
            foreach ($log as $key => $query)
            {
                $log[$key] = vsprintf(str_replace('?', "'%s'", $query['query']), $query['bindings']);
            }
        }
    
        // return ($last) ? end($log) : $log; // return single table query
        return $log;  // return join tables queries 
    }
    
    function check_string_empty($value)
    {
        if (!empty($value))
        {
            return $value;
        }
        else
        {
            return '';
        }
    }
    
    function get_class_order()
    {
    
        $arr_classes          = [];
        $arr_existing_classes = Classes::where(array('class_status' => 1))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        foreach ($arr_existing_classes as $existing_class)
        {
            $arr_classes[$existing_class['class_id'] . '_' . $existing_class['class_order']] = $existing_class['class_name'];
        }
        return $arr_classes;
    }
    /**
     * Get all mediums 
     */
    function get_all_mediums()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_mediums = [];
        $arr_mediums_list = Mediums::where(array('medium_type_status' => 1))->select('medium_type', 'medium_type_id')->orderBy('medium_type', 'ASC')->get();
        if (!empty($arr_mediums_list))
        {
            foreach ($arr_mediums_list as $arr_medium)
            {
                $arr_mediums[$arr_medium['medium_type_id']] = $arr_medium['medium_type'];
            }
        }
        return $arr_mediums;
    }
    /**
     * Get all classes 
     */
    function get_all_classes($medium_type)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = [];
        $arr_classes_list = Classes::where(array('class_status' => 1,'medium_type'=> $medium_type))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_classes[$arr_class['class_id']] = $arr_class['class_name'];
            }
        }
        return $arr_classes;
    }

     /**
     * Get all classes 
     */
    function get_all_admission_classes($class_ids)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = [];
        $arr_classes_list = Classes::where(array('class_status' => 1))->whereIn('class_id',explode(',',$class_ids))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_classes[$arr_class['class_id']] = $arr_class['class_name'];
            }
        }
        return $arr_classes;
    }
    
    /**
     * Get all classes 
     */
    function get_all_classes_mediums($class_ids = NULL)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = [];
        $arr_medium     = get_all_mediums();
        $arr_boards     = get_all_school_boards();
        $arr_classes_list		= Classes::where(function($query) use ($class_ids) 
        {
            $query->where('class_status', "=", 1);
            if (!empty($class_ids) && $class_ids != null)
            {
                $query->whereIn('class_id',explode(',',$class_ids));
            }
           
        })->select('class_name','medium_type', 'class_order', 'class_id','board_id')->orderBy('class_order', 'ASC')->get();
       
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $class_name     = $arr_class['class_name']." - ".$arr_medium[$arr_class['medium_type']]." - ".$arr_boards[$arr_class['board_id']];
                $arr_classes[$arr_class['class_id']] = $class_name;
            }
        }
        return $arr_classes;
    }
    
    /**
     * Get all shift 
     */
    function get_all_shift()
    {
        $arr_shifts = [];
        $arr_shift_list = Shift::where(array('shift_status' => 1))->select('shift_name', 'shift_id')->orderBy('shift_id', 'ASC')->get();
        if (!empty($arr_shift_list))
        {
            foreach ($arr_shift_list as $arr_shift)
            {
                $arr_shifts[$arr_shift['shift_id']] = $arr_shift['shift_name'];
            }
        }
        return $arr_shifts;
    }
    
    /**
     * Get all types of Co-Scholastic  
     */
    function get_all_co_scholastic()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_scholastic = [];
        $arr_scholastic_list = CoScholastic::where(array('co_scholastic_type_status' => 1))->select('co_scholastic_type_name', 'co_scholastic_type_id')->orderBy('co_scholastic_type_name', 'ASC')->get();
        if (!empty($arr_scholastic_list))
        {
            foreach ($arr_scholastic_list as $arr_scholastics)
            {
                $arr_scholastic[$arr_scholastics['co_scholastic_type_id']] = $arr_scholastics['co_scholastic_type_name'];
            }
        }
        return $arr_scholastic;
    }
    
    function add_blank_option($arr, $option)
    {
        $arr_option = array();
        if (!empty($option))
        {
            $arr_option[''] = $option;
        }
        else
        {
            $arr_option[''] = '';
        }
        // operator on array
        $result = $arr_option + $arr;
        
        return $result;
    }
    
    function get_formatted_date($date)
    {
        $return      = array();
        $date_format = Config::get('custom.date_format');
        if (!empty($date))
        {
            $return = date($date_format, strtotime($date));
        }
        else
        {
            $return = '';
        }
        return $return;
    }
    
    /**
     * Get all caste 
     */
    function get_caste()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_caste          = [];
        $arr_caste_data = Caste::where([['caste_status', '=', 1]])->select('caste_id', 'caste_name')->get();
        if (!empty($arr_caste_data))
        {
            foreach ($arr_caste_data as $arr_caste_data)
            {
                $arr_caste[$arr_caste_data['caste_id']] = $arr_caste_data['caste_name'];
            }
        }
        return $arr_caste;
    }
    /**
     * Get Caste name
     */
    function get_caste_name($id)
    {
        $casteName = '';
        if(!empty($id)) {
            $arr_caste_data = Caste::where([['caste_status', '=', 1],['caste_id', '=', $id]])->select('caste_id', 'caste_name')->first()->toArray();
            $casteName = $arr_caste_data['caste_name'];
        }
        return $casteName;
    }
    
    /**
     * Get all titles 
     */
    function get_titles()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_titles          = [];
        $arr_title_data = Title::where([['title_status', '=', 1]])->select('title_id', 'title_name')->get();
        if (!empty($arr_title_data))
        {
            foreach ($arr_title_data as $arr_title_data)
            {
                $arr_titles[$arr_title_data['title_id']] = $arr_title_data['title_name'];
            }
        }
        return $arr_titles;
    }
    /**
     * Get title name
     */
    function get_title_name($id)
    {
        $titleName = '';
        if(!empty($id)) {
            $arr_title_data = Title::where([['title_status', '=', 1],['title_id', '=', $id]])->select('title_id', 'title_name')->first()->toArray();
            $titleName = $arr_title_data['title_name'];
        }
        return $titleName;
    }
    
     /**
     * Get all religions 
     */
    function get_religion()
    {
        $loginInfo 		= get_loggedin_user_data();
        $arr_religion   = [];
        $arr_religion_data = Religion::where([['religion_status', '=', 1]])->select('religion_id', 'religion_name')->get();
        if (!empty($arr_religion_data))
        {
            foreach ($arr_religion_data as $arr_religion_data)
            {
                $arr_religion[$arr_religion_data['religion_id']] = $arr_religion_data['religion_name'];
            }
        }
        return $arr_religion;
    }

    /**
     * Get religion name
     */
    function get_religion_name($id)
    {
        $religionName = '';
        if(!empty($id)) {
            $arr_religion_data = Religion::where([['religion_status', '=', 1],['religion_id', '=', $id]])->select('religion_id', 'religion_name')->first()->toArray();
            $religionName = $arr_religion_data['religion_name'];
        }
        return $religionName;
    }
    
    /**
     * Get all nationality 
     */
    function get_nationality()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_nationality       = [];
        $arr_nationality_data = Nationality::where([['nationality_status', '=', 1]])->select('nationality_id', 'nationality_name')->get();
        if (!empty($arr_nationality_data))
        {
            foreach ($arr_nationality_data as $arr_nationality_data)
            {
                $arr_nationality[$arr_nationality_data['nationality_id']] = $arr_nationality_data['nationality_name'];
            }
        }
        return $arr_nationality;
    }

    /**
     * Get nationality name
     */
    function get_nationality_name($id)
    {
        $nationalityName = '';
        if(!empty($id)) {
            $arr_nationality_data = Nationality::where([['nationality_status', '=', 1],['nationality_id', '=', $id]])->select('nationality_id', 'nationality_name')->first()->toArray();
            $nationalityName = $arr_nationality_data['nationality_name'];
        }
        return $nationalityName;
    }
    
    /**
     * Get all house/group
     */
    function get_student_groups()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_group       = [];
        $arr_group_data = SchoolGroup::where([['group_status', '=', 1]])->select('group_id', 'group_name')->get();
        if (!empty($arr_group_data))
        {
            foreach ($arr_group_data as $arr_group_data)
            {
                $arr_group[$arr_group_data['group_id']] = $arr_group_data['group_name'];
            }
        }
        return $arr_group;
    }
    
    function get_student_type()
    {
        $student_type     = [];
        $arr_student_type = StudentType::where('student_type_status', 1)->select('student_type_id', 'student_type')->get();
        if (!empty($arr_student_type))
        {
            foreach ($arr_student_type as $arr_type)
            {
                $student_type[$arr_type['student_type_id']] = $arr_type['student_type'];
            }
        }
        return $student_type;
    }
    
    function get_class_section($class_id=null)
    {
        $loginInfo 			= get_loggedin_user_data();
        $section     = [];
        if(isset($class_id) && $class_id != null){
            $arr_section = Section::where([['section_status', '=', 1],['class_id', '=', $class_id]])->select('section_id', 'section_name')->get();
        } else {
            $arr_section = Section::where([['section_status', '=', 1]])->select('section_id', 'section_name')->get();
        }
        
        if (!empty($arr_section))
        {
            foreach ($arr_section as $value)
            {
                $section[$value['section_id']] = $value['section_name'];
            }
        }
        return $section;
    }
    /**
     * Get class - section info using section ID
     */
    function get_class_section_info($section_id=null)
    {
        $loginInfo 			= get_loggedin_user_data();
        $sectionInfo     = [];
        $list_arr = [];
        if(isset($section_id) && $section_id != null){
            $arr_section = Section::where([['section_status', '=', 1],['section_id', '=', $section_id]])->with('sectionClass')->get();
        } else {
            $arr_section = Section::where([['section_status', '=', 1]])->with('sectionClass')->get();
        }
        if (!empty($arr_section))
        {   
            foreach ($arr_section as $value)
            {
                $totalStudents = 0;
                $totalStudents = StudentAcademic::where(function($query) use ($loginInfo,$value) 
                {
                    $query->where('current_class_id', "=", $value['class_id']);
                    $query->where('current_section_id', "=", $value['section_id']);
                   
                })->get()->count();
                $list_arr = array(
                    'section_name'      => $value['section_name'],
                    'section_order'     => $value['section_order'],
                    'total_students'    => $totalStudents,
                );
                if (isset($value['sectionClass']['class_name']))
                {
                    $list_arr['class_name'] = $value['sectionClass']['class_name'];
                }
            }
        }
        return $list_arr;
    }
    
    function get_session()
    {
        $set_session     = LSession::get('set_session_year');
        $session_id      = null;
        $updated_session = [];
        if (!empty($set_session))
        {
            $session_id = $set_session['session_id'];
        }
        $session = Session::where(function($query) use ($session_id)
            {
                if (!empty($session_id))
                {
                    $query->where('session_id', $session_id);
                }
                else
                {
                    $query->where('session_status', 1);
                }
            })->select('session_id', 'session_name')->first();
        if (!empty($session->session_id))
        {
            $updated_session[$session->session_id] = $session['session_name'];
        }
        return $updated_session;
    }
    
    function get_arr_session()
    {
        $session     = [];
        $arr_session = Session::where('session_status',1)->select('session_id', 'session_name')->get();
        if (!empty($arr_session))
        {
            foreach ($arr_session as $value)
            {
                $session[$value['session_id']] = $value['session_name'];
            }
        }
        return add_blank_option($session, 'Select session');
    }
    
    function get_encrypted_value($key, $encrypt = false)
    {
        $encrypted_key = null;
        if (!empty($key))
        {
            if ($encrypt == true)
            {
                $key = Crypt::encrypt($key);
            }
            $encrypted_key = $key;
        }
        return $encrypted_key;
    }
    
    function get_decrypted_value($key, $decrypt = false)
    {
        $decrypted_key = null;
        if (!empty($key))
        {
            if ($decrypt == true)
            {
                $key = Crypt::decrypt($key);
            }
            $decrypted_key = $key;
        }
        return $decrypted_key;
    }
    
    // Check for siblings 
    function check_sibling($student_parent_id,$student_id)
    {
        
        $loginInfo          = get_loggedin_user_data();
        $info      = Student::where(function($query) use ($student_parent_id,$student_id) 
        {
            // For student parent id
            if (!empty($student_parent_id) && $student_parent_id != null)
            {
                $query->where('student_parent_id', "=", $student_parent_id);
            }
            // For student student id
            if (!empty($student_parent_id) && $student_parent_id != null)
            {
                $query->where('student_id', "!=", $student_id);
            }
        })->get();
        if(!empty($info[0]->student_id)){
            return 'true';
        } else {
            return 'false';
        }
    }
    // Check parents already exist or not?
    function check_parents_exist($mobile_no)
    {
        $parent_info = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_parent_info    = StudentParent::where(function($query) use ($mobile_no) 
        {
             // For Mobile
             if (!empty($mobile_no) && $mobile_no != null)
             {
                 $query->where('student_login_contact_no', '=', $mobile_no);
             }
            
        })
        ->get();

        if(!empty($arr_parent_info[0]->student_login_email)){
            $parent_info = (object) $arr_parent_info[0];
        }
        return $parent_info;
    }
    
    // Get student list using section ID
    function get_student_list($request)
    {
        // p($request->all());
        $arr_student_list = [];
        $session       = get_current_session();
        $arr_medium         = get_all_mediums();
        $arr_student_type   = \Config::get('custom.student_type');
        $arr_rte_status     = \Config::get('custom.rte_status');
        $arr_student        = Student::where(function($query) use ($request,$session) 
        {
            $query->where('students.student_status',1);
            // For Roll No
            if (!empty($request) && !empty($request->get('roll_no')))
            {
                $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
            }
            // For Enroll No
            if (!empty($request) && !empty($request->get('enroll_no')))
            {
                $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
            }
            // For Student Name
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
            // For student type
            if (!empty($request) && $request->get('student_type')!= null)
            {
                $query->where('student_type', "=", $request->get('student_type'));
            }
            // For RTE status
            if (!empty($request) && $request->get('rte_apply_status') != null)
            {
                $query->where('students.rte_apply_status', "=", $request->get('rte_apply_status'));
            }
            
        })->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
            $join->where('current_session_id',$session['session_id']);
        })->with('getStudentAcademic')
        ->join('student_parents', function($join) use ($request){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            if (!empty($request) && !empty($request->get('father_name')))
            {
                $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
            }
        })->orderBy('students.student_id', 'DESC')
        ->with('getParent')->with(['getRTEInfo' => function($query) use ($request,$session){
            
            $query->where('session_id', "=", $session['session_id']);
            
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('rte_head_id')))
            {
                $query->where('rte_head_id', "=", $request->get('rte_head_id'));
            }
        }])
        ->with('getImprestAc')->with('getWalletAc')
        ->get();
        // p($arr_student);
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                $rteApplyStatus = "No";
                $rte_head_map_id = null;
                if(isset($student['getRTEInfo'][0]->rte_head_id) && $student['getRTEInfo'][0]->rte_head_id == $request->get('rte_head_id')){
                    $rteApplyStatus = "Yes";
                    $rte_head_map_id = $student['getRTEInfo'][0]->rte_head_map_id;
                }
                $student_image = '';
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student->student_roll_no)){
                    $rollNo = $student->student_roll_no;
                } else {
                    $rollNo = "----";
                }
                $wallet_amt = 0;
                if(isset($student['getWalletAc'][0]->wallet_ac_amt)){
                    $wallet_amt = $student['getWalletAc'][0]->wallet_ac_amt;
                }
                $imprest_amt = 0;
                if(isset($student['getImprestAc'][0]->imprest_ac_amt)){
                    $imprest_amt = $student['getImprestAc'][0]->imprest_ac_amt;
                }
                $arr_student_list[] = array(
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student->student_name,
                    'student_status'        => $student->student_status,
                    'medium_type'           => $arr_medium[$student->medium_type],
                    'rte_status'            => $arr_rte_status[$student->rte_apply_status],
                    'rte_apply_status'      => $student->rte_apply_status,
                    'section_id'            => $request->get('section_id'),
                    'student_parent_id'     => $student['getParent']->student_parent_id,
                    'student_father_name'   => $student['getParent']->student_father_name,
                    'student_father_mobile_number'   => $student['getParent']->student_father_mobile_number,
                    'wallet_amt'            => $wallet_amt,
                    'imprest_amt'           => $imprest_amt,
                    'rte_fees_apply_status'      => $rteApplyStatus,
                    'rte_head_map_id'           => $rte_head_map_id
                );
            }
        }
        // p($arr_student_list);
        return $arr_student_list;
    }
    
    // Get Student signle data
    function get_student_signle_data($student_id = null) {
        $gender             = \Config::get('custom.student_gender');
        $st_status             = \Config::get('custom.student_status');
        $arr_student_type   = \Config::get('custom.student_type');
        $arr_medium         = get_all_mediums();

        $loginInfo = get_loggedin_user_data();
        $student       = [];
        $student_array = [];
        $session_id    = null;
        $session       = get_current_session();
        if (!empty($session))
        {
            $session_id = $session['session_id'];
        }
        $get_student_details = Student::where(function($query) use ($student_id)
        {
            if (!empty($student_id))
            {
                $query->where('student_id', $student_id);
            }
        })->with('getStudentAcademic')->get();
        $get_student_details = isset($get_student_details[0]) ? $get_student_details[0] : [];
       
        $class_id = $get_student_details['getStudentAcademic']['admission_class_id'];
        $section_id = $get_student_details['getStudentAcademic']['admission_section_id'];
        // $class_id   = $loginInfo['class_id']; 
        // $section_id = $loginInfo['section_id']; 
        $arr_student = Student::where(function($query) use ($student_id,$class_id,$section_id)
            {
                if (!empty($student_id))
                {
                    $query->where('student_id', $student_id);
                }
            })
        ->with('getParent')->with('getTitle')->with('getStudentCaste')->with('getStudentReligion')->with('getStudentNationality')->with('getStudentAcademic')->with('getStudentAcademic.getAdmitSession')->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getStudentGroup')->with('getStudentAcademic.getStudentStream')->with('getStudentHealth')->with('documents')->with('documents.getDocumentCategory')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')->with('getPermanentCity')->with('getPermanentState')->with('getPermanentCountry')->with('leaves')
            ->with(['getStudentAttendence'=>function($query) use ($request,$class_id,$section_id) {
                $query->where('class_id', "=", $class_id);
                $query->where('section_id', "=", $section_id);                
            }])
            ->with(['remarks'=>function($query) use ($request,$class_id,$section_id,$session_id) {
                if($class_id != ''){
                    $query->where('class_id', "=", $class_id);
                }
                if($section_id != ''){
                    $query->where('section_id', "=", $section_id);
                }
                $query->where('session_id', "=", $session_id)->with('getStaff')->with('getSubject');                
            }])
        ->get();
        
        // p($arr_student);
        foreach ($arr_student as $key => $student)
        {
           // p($student['getStudentAttendence']);

            if (!empty($student['student_id']))
            {
                if (!empty($student['student_image']))
                {
                    $profile = check_file_exist($student['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student['profile'] = $profile;
                    }
                }
                $document_list = [];
                foreach ($student['documents'] as $documents)
                {
                    $document_data['student_document_id']       = $documents['student_document_id'];
                    $document_data['document_category_id']      = $documents['document_category_id'];
                    $document_data['document_category_name']    = $documents['getDocumentCategory']['document_category_name'];
                    $document_data['student_document_details']  = $documents['student_document_details'];
                    $document_data['student_document_status']  = $documents['student_document_status']; //@pratyush 10 Aug 2018
                    $document_data['document_submit_date']      = date("d F, Y ", strtotime($documents['created_at']->toDateString()));
                    $document_data['student_document_file'] = '';
                    if (!empty($documents['student_document_file']))
                    {
                        $document_file = check_file_exist($documents['student_document_file'], 'student_document_file');
                        if (!empty($document_file))
                        {
                            $document_data['student_document_file'] = $document_file;
                        } 
                    }else {
                        $document_data['student_document_file'] = "";
                    }
                    $document_list[] = $document_data;
                }
                $leave_list = [];
                foreach ($student['leaves'] as $leaves)
                {
                    $datetime1 = new DateTime($leaves['student_leave_from_date']);
                    $datetime2 = new DateTime($leaves['student_leave_to_date']);
                    $interval = $datetime1->diff($datetime2);
                    $days = $interval->format('%a');//now do whatever you like with $days
                    $leave_data['student_leave_id']       = $leaves['student_leave_id'];
                    $leave_data['student_leave_reason']   = $leaves['student_leave_reason'];
                    $leave_data['student_leave_from_date']  = date("d F, Y ", strtotime($leaves['student_leave_from_date']));
                    $leave_data['student_leave_to_date']    = date("d F, Y ", strtotime($leaves['student_leave_to_date']));
                    $leave_data['student_leave_days']       = $days;
                    if($leaves['student_leave_status'] == 0){
                        $status = 'Pending';
                    } if($leaves['student_leave_status'] == 1){
                        $status = 'Approved';
                    } if($leaves['student_leave_status'] == 2){
                        $status = 'Disapproved';
                    } 
                    $leave_data['leave_status']       = $status;
                    $leave_data['student_leave_attachment']       = check_file_exist($leaves['student_leave_attachment'], 'student_leave_document_file');
                    
                    $leave_list[] = $leave_data;
                }

                $attendence_list = [];
                foreach ($student['getStudentAttendence'] as $getStudentAttendences)
                {

                    if($getStudentAttendences['student_attendance_type'] == 1){
                        $getStudentAttendences['student_attendance_type'] = "Present";
                        $attendence_class = "attendence_green";
                    } else if($getStudentAttendences['student_attendance_type'] == '0' ){
                        $getStudentAttendences['student_attendance_type'] = "Absent";
                        $attendence_class = "attendence_red";
                    } else if($getStudentAttendences['student_attendance_type'] == ''){
                        $getStudentAttendences['student_attendance_type'] = "Present";
                        $attendence_class = "attendence_green";
                    }

                    $attendence_data['student_id']              = $getStudentAttendences['student_id'];
                    $attendence_data['student_attendance_id']   = $getStudentAttendences['student_attendance_id'];
                    $attendence_data['student_attendance_type'] = $getStudentAttendences['student_attendance_type'];
                    $attendence_data['attendence_class']        = $attendence_class;
                    $attendence_data['student_attendance_date'] = date("Y-m-d", strtotime($getStudentAttendences['student_attendance_date']));

                    $attendence_list[] = $attendence_data;
                }


                $remarks_list = [];
                foreach ($student['remarks'] as $remarks)
                {

                    $remarks_data['remark_id']              = $remarks['remark_id'];
                    $remarks_data['remark_date']            = date("d F Y", strtotime($remarks['remark_date']));
                    $remarks_data['subject_name']           = $remarks['getSubject']['subject_name'].' - '.$remarks['getSubject']['subject_code'];
                    $remarks_data['teacher_name']           = $remarks['getStaff']['staff_name'];
                    $remarks_data['remark_description']     = $remarks['remark_text'];
                    
                    $remarks_list[] = $remarks_data;
                }

                $student_array[] = array(
                    'student_enroll_number'         => $student['student_enroll_number'],
                    'student_roll_no'               => $student['student_roll_no'],
                    'student_reg_date'              => $student['student_reg_date'],
                    'student_id'                    => $student['student_id'],
                    'title_name'                    => $student['getTitle']['title_name'],
                    'student_name'                  => $student['student_name'],
                    'student_email'                 => $student['student_email'],
                    'student_image'                 => $student['profile'],
                    'student_gender'                => $gender[$student['student_gender']],
                    'status'                        => $st_status[$student['student_status']],
                    'student_gender1'               => $student['student_gender'],
                    'student_dob'                   => $student['student_dob'],
                    'student_category'              => $student['student_category'],
                    'title_id'                      => $student['title_id'],
                    'caste_id'                      => $student['caste_id'],
                    'caste_name'                    => $student['getStudentCaste']['caste_name'],
                    'religion_id'                   => $student['religion_id'],
                    'religion_name'                 => $student['getStudentReligion']['religion_name'],
                    'nationality_id'                => $student['nationality_id'],
                    'nationality_name'              => $student['getStudentNationality']['nationality_name'],
                    'student_sibling_name'          => $student['student_sibling_name'],
                    'student_sibling_class_id'      => $student['student_sibling_class_id'],
                    'student_sibling_class_name'    => $student['getSiblingClass']['class_name'],
                    'student_temporary_address'     => $student['student_temporary_address'],
                    'student_temporary_city'        => $student['student_temporary_city'],
                    'student_temporary_city_name'   => $student['getTemporaryCity']['city_name'],
                    'student_temporary_state'       => $student['student_temporary_state'],
                    'student_temporary_state_name'   => $student['getTemporaryState']['state_name'],
                    'student_temporary_county'      => $student['student_temporary_county'],
                    'student_temporary_county_name' => $student['getTemporaryCountry']['country_name'],
                    'student_temporary_pincode'     => $student['student_temporary_pincode'],
                    'student_permanent_address'     => $student['student_permanent_address'],
                    'student_permanent_city'        => $student['student_permanent_city'],
                    'student_permanent_city_name'   => $student['getPermanentCity']['city_name'],
                    'student_permanent_state'       => $student['student_permanent_state'],
                    'student_permanent_state_name'  => $student['getPermanentState']['state_name'],
                    'student_permanent_county'      => $student['student_permanent_county'],
                    'student_permanent_county_name' => $student['getPermanentCountry']['country_name'],
                    'student_permanent_pincode'     => $student['student_permanent_pincode'],
                    'student_adhar_card_number'     => $student['student_adhar_card_number'],
                    'medium_type'                   => $student['medium_type'],
                    'medium_type1'                  => $arr_medium[$student['medium_type']],
                    'student_type'                  => $student['student_type'],
                    'student_type1'                 => $arr_student_type[$student['student_type']],
                    'student_privious_school'       => $student['student_privious_school'],
                    'student_privious_class'        => $student['student_privious_class'],
                    'student_privious_tc_no'        => $student['student_privious_tc_no'],
                    'student_privious_tc_date'      => $student['student_privious_tc_date'],
                    'student_privious_result'       => $student['student_privious_result'],
    
                    
                    'student_father_name'               => $student['getParent']['student_father_name'],
                    'student_father_mobile_number'      => $student['getParent']['student_father_mobile_number'],
                    'student_father_email'              => $student['getParent']['student_father_email'],
                    'student_father_occupation'         => $student['getParent']['student_father_occupation'],
                    'student_father_annual_salary'      => $student['getParent']['student_father_annual_salary'],
                    'student_mother_name'               => $student['getParent']['student_mother_name'],
                    'student_mother_mobile_number'      => $student['getParent']['student_mother_mobile_number'],
                    'student_mother_email'              => $student['getParent']['student_mother_email'],
                    'student_mother_occupation'         => $student['getParent']['student_mother_occupation'],
                    'student_mother_tongue'             => $student['getParent']['student_mother_tongue'],
                    'student_mother_annual_salary'      => $student['getParent']['student_mother_annual_salary'],
                    'student_guardian_name'             => $student['getParent']['student_guardian_name'],
                    'student_guardian_email'            => $student['getParent']['student_guardian_email'],
                    'student_guardian_mobile_number'    => $student['getParent']['student_guardian_mobile_number'],
                    'student_guardian_relation'         => $student['getParent']['student_guardian_relation'],
                    'student_login_name'                => $student['getParent']['student_login_name'],
                    'student_login_email'               => $student['getParent']['student_login_email'],
                    'student_login_contact_no'          => $student['getParent']['student_login_contact_no'],
                    'student_parent_status'             => $student['getParent']['student_parent_status'],
    
                    'student_unique_id'                 => $student['getStudentAcademic']['student_unique_id'],
                    'admission_session_id'              => $student['getStudentAcademic']['admission_session_id'],
                    'admission_class_id'                => $student['getStudentAcademic']['admission_class_id'],
                    'admission_section_id'              => $student['getStudentAcademic']['admission_section_id'],
                    'current_session_id'                => $student['getStudentAcademic']['current_session_id'],
                    'current_class_id'                  => $student['getStudentAcademic']['current_class_id'],
                    'current_section_id'                => $student['getStudentAcademic']['current_section_id'],
                    'group_id'                          => $student['getStudentAcademic']['group_id'],
                    'stream_id'                         => $student['getStudentAcademic']['stream_id'],
    
                    'current_session'                   => $student['getStudentAcademic']['getCurrentSession']['session_name'],
                    'current_class'                     => $student['getStudentAcademic']['getCurrentClass']['class_name'],
                    'current_section'                   => $student['getStudentAcademic']['getCurrentSection']['section_name'],
                    'admission_session'                 => $student['getStudentAcademic']['getAdmitSession']['session_name'],
                    'admission_class'                   => $student['getStudentAcademic']['getAdmitClass']['class_name'],
                    'admission_section'                 => $student['getStudentAcademic']['getAdmitSection']['section_name'],
                    'student_group_name'                => $student['getStudentAcademic']['getStudentGroup']['group_name'],
                    'stream_name'                       => $student['getStudentAcademic']['getStudentStream']['stream_name'],
                    'class_section'                     => $student['getStudentAcademic']['getCurrentClass']['class_name'].' - '.$student['getStudentAcademic']['getCurrentSection']['section_name'],
    
    
                    'student_height'                    => $student['getStudentHealth']['student_height'],
                    'student_weight'                    => $student['getStudentHealth']['student_weight'],
                    'student_blood_group'               => $student['getStudentHealth']['student_blood_group'],
                    'student_vision_left'               => $student['getStudentHealth']['student_vision_left'],
                    'student_vision_right'              => $student['getStudentHealth']['student_vision_right'],
                    'medical_issues'                    => $student['getStudentHealth']['medical_issues'],
    
                    'documents'                         => $document_list,
                    'leaves'                            => $leave_list,
                    'attendence'                        => $attendence_list,
                    'remarks'                           => $remarks_list,
                );
            }
        }

        // p($student_array);
        return $student_array;
    }
    
    function get_current_session()
    {
        $set_session = LSession::get('set_session_year');
        $session_id  = null;
        if (!empty($set_session))
        {
            $session_id = $set_session['session_id'];
        }
        $session_data = [];

        $arr_session = Session::where(function($query) use ($session_id)
            {
                if (!empty($session_id))
                {
                    $query->where('session_id', $session_id);
                }
                else
                {
                    $query->where('session_status', 1);
                }
            })->select('session_id', 'session_name', 'session_start_date', 'session_end_date', 'session_status')->first();
        if (!empty($arr_session))
        {
            $session_start_date = date_create($arr_session->session_start_date);
            $session_end_date   = date_create($arr_session->session_end_date);
            $session_period     = date_diff($session_start_date, $session_end_date);
            $session_period     = $session_period->format("%a");
            $session_days       = $session_period + 1; // included end date
            $session_data       = array(
                'session_id'     => $arr_session->session_id,
                'session_year'   => $arr_session->session_name,
                'start_date'     => $arr_session->session_start_date,
                'end_date'       => $arr_session->session_end_date,
                'session_status' => $arr_session->session_status,
                'session_days'   => $session_days,
            );
        }
        return $session_data;
    }
    
    
    function check_file_exist($file_name, $custome_key)
    {
        $return_file        = '';
        $config_upload_path = \Config::get('custom.' . $custome_key);
        if (!empty($file_name))
        {
            if (is_file($config_upload_path['display_path'] . $file_name))
            {
                $return_file = $config_upload_path['display_path'] . $file_name;
            }
        }
        return $return_file;
    }
    
    function get_academic_year()
    {
        $year_list      = [];
        $current        = new \DateTime();
        $end            = new \DateTime('+ 5 years'); // end year 
        $interval_year  = new DateInterval('P1Y');
        $arr_year_range = new DatePeriod($current, $interval_year, $end);
        foreach ($arr_year_range as $key => $year)
        {
            $year_list[$year->format('Y')] = $year->format('Y');
        }
        return $year_list;
    }
    
     // Get parent list
     function get_parent_list($student_parent_id=null,$request)
     {
         $loginInfo = get_loggedin_user_data();
         $arr_parent_list = [];
         $arr_parent      = StudentParent::where(function($query) use ($loginInfo,$request,$student_parent_id) 
         {
             if (!empty($student_parent_id))
             {
                 $query->where('student_parents.student_parent_id', "=", $student_parent_id);
             }
             // For Father Name
             if (!empty($request) && !empty($request->get('father_name')) && $request->get('father_name') != null)
             {
                 $query->where('student_father_name', "like", "%{$request->get('father_name')}%");
             }
             
         })
         ->join('students', function($join) use ($request){
             $join->on('students.student_parent_id', '=', 'student_parents.student_parent_id');
             // For Roll No
             if (!empty($request) && !empty($request->get('roll_no')) && $request->get('roll_no') != null)
             {
                 $join->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
             }
             // For Enroll No
             if (!empty($request) && !empty($request->has('enroll_no')) && $request->get('enroll_no') != null)
             {
                 $join->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
             }
             if (!empty($request) && !empty($request->has('student_name')) && $request->get('student_name') != null)
             {
                 $join->where('student_name', "like", "%{$request->get('student_name')}%");
             }
         })
         ->with('getStudent','getStudent.getStudentAcademic','getStudent.getStudentAcademic.getCurrentClass','getStudent.getStudentAcademic.getCurrentSection')
         // ->groupBy('students.student_parent_id')
         // ->select('student_parents.*')
         ->get();
         // p($arr_parent);
         if (!empty($arr_parent[0]->student_parent_id))
         {
             foreach ($arr_parent as $parent)
             {
                $profile = "";
                if (!empty($parent->student_image))
                {
                    $profile = check_file_exist($parent->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $profile = URL::to($profile);
                    }
                } else {
                    $profile = "";
                }
                $student_roll_no = '-----';
                if($parent->student_roll_no != ''){
                    $student_roll_no = $parent->student_roll_no;
                }
                $class_section = '';
                if(!empty($parent['getStudent'])){
                    foreach ($parent['getStudent'] as $student_info) {
                        if($parent->student_id == $student_info->student_id){
                            $class_section = $student_info['getStudentAcademic']['getCurrentClass']->class_name.' - '.$student_info['getStudentAcademic']['getCurrentSection']->section_name;
                        }
                    }
                }
                // p($class_section);
                // $class_section = $parent['getStudent']['getStudentAcademic']['getCurrentClass']->class_name.' - '.$parent['getStudent']['getStudentAcademic']['getCurrentSection']->section_name;
                $arr_parent_list[] = array(
                    'student_parent_id'                => $parent->student_parent_id,
                    'student_enroll_number'            => $parent->student_enroll_number,
                    'student_roll_no'                  => $student_roll_no,
                    'student_name'                     => $parent->student_name,
                    'profile'                          => $profile,
                    'student_status'                   => $parent->student_status,
                    'student_login_name'               => $parent->student_login_name,
                    'student_login_email'              => $parent->student_login_email,
                    'student_login_contact_no'         => $parent->student_login_contact_no,
                    'student_father_name'              => $parent->student_father_name,
                    'student_father_mobile_number'     => $parent->student_father_mobile_number,
                    'student_father_email'             => $parent->student_father_email,
                    'student_father_occupation'        => $parent->student_father_occupation,
                    'student_father_annual_salary'     => $parent->student_father_annual_salary,
                    'student_mother_name'              => $parent->student_mother_name,
                    'student_mother_mobile_number'     => $parent->student_mother_mobile_number,
                    'student_mother_email'             => $parent->student_mother_email,
                    'student_mother_occupation'        => $parent->student_mother_occupation,
                    'student_mother_annual_salary'     => $parent->student_mother_annual_salary,
                    'student_mother_tongue'            => $parent->student_mother_tongue,
                    'student_guardian_name'            => $parent->student_guardian_name,
                    'student_guardian_email'           => $parent->student_guardian_email,
                    'student_guardian_mobile_number'   => $parent->student_guardian_mobile_number,
                    'student_guardian_relation'        => $parent->student_guardian_relation,
                    'class_section'                    => $class_section,
                );
                
             }
         }
         return $arr_parent_list;
     }
    /*
    * Funtion Name get_all_student_list_for_virtual_class.
    * To get all student details.
    * @pratyush 30 July 2018.  
    */
    
    function get_all_student_list_for_virtual_class($request)
    {  
        $session = get_current_session();
        $loginInfo        = get_loggedin_user_data();
        $arr_student_list = [];
        if($request->get('operation_type') == 1){
            $arr_student      = Student::where(function($query) use ($loginInfo,$request,$session) 
            {
                $query->where('student_status', 1);
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')) && $request->get('roll_no') != null)
                {
                    $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')) && $request->get('enroll_no') != null )
                {
                    $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')) && $request->get('student_name') != null)
                {
                    $query->where('student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })->join('student_academic_info', function($join) use ($request,$session){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                } 
                $join->where('current_session_id',$session['session_id']);
            })->with('getStudentAcademic')->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')) && $request->get('father_name') != null)
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->with('getParent')->whereNotExists( function ($query) use ($request) {
                    
                        $query->select(DB::raw(1))
                        ->from('virtual_class_map')
                        ->whereRaw('students.student_id = virtual_class_map.student_id')
                        ->where('virtual_class_map.virtual_class_id', '=', $request->get('virtual_class_id'))
                        ->where('students.student_id', '!=', 'virtual_class_map.student_id');
                    
                })->get();
        
        }elseif($request->get('operation_type') == 2){
            $arr_student      = Student::where(function($query) use ($loginInfo,$request) 
            {
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')) && $request->get('roll_no') != null)
                {
                    $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')) && $request->get('enroll_no') != null)
                {
                    $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')) && $request->get('student_name') != null)
                {
                    $query->where('student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {   
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')) && $request->get('father_name') != null)
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->with('getParent')->whereExists( function ($query) use ($request) {
                    
                        $query->select(DB::raw(1))
                        ->from('virtual_class_map')
                        ->whereRaw('students.student_id = virtual_class_map.student_id')
                        ->where('virtual_class_map.virtual_class_id', '=', $request->get('virtual_class_id'));
                })
            ->get();
        }
        // p($arr_student);
        
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                $student_image = "";
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
    
                if($request->has('virtual_class_id')){
                    $arr_student_list[] = array(
                        'student_id'            => $student->student_id,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student->student_enroll_number,
                        'student_roll_no'       => $student->student_roll_no,
                        'student_name'          => $student->student_name,
                        'student_status'        => $student->student_status,
                        'student_father_name'   => $student['getParent']->student_father_name,
                        'student_session'       => $student['getStudentAcademic']['getCurrentSection']->section_name,
                        'student_class'         => $student['getStudentAcademic']['getCurrentClass']->class_name,
                        'virtual_class_id'      => $request->get('virtual_class_id')
                    );
                }else{
                    $arr_student_list[] = array(
                        'student_id'            => $student->student_id,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student->student_enroll_number,
                        'student_roll_no'       => $student->student_roll_no,
                        'student_name'          => $student->student_name,
                        'student_status'        => $student->student_status,
                        'student_father_name'   => $student['getParent']->student_father_name,
                        'student_session'       => $student['getStudentAcademic']['getCurrentSection']->section_name,
                        'student_class'         => $student['getStudentAcademic']['getCurrentClass']->class_name,
                    );
                }
            }
        }
        return $arr_student_list;
    }
    
    
    
    /**
     * Get all staff roles 
     */
    function get_all_staff_roles()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_staff_roles = [];
        $arr_staff_role_list = StaffRoles::where(array('staff_role_status' => 1))->select('staff_role_name', 'staff_role_id')->orderBy('staff_role_id', 'ASC')->get();
        if (!empty($arr_staff_role_list))
        {
            foreach ($arr_staff_role_list as $arr_staff_role)
            {
                $arr_staff_roles[$arr_staff_role['staff_role_id']] = $arr_staff_role['staff_role_name'];
            }
        }
        return $arr_staff_roles;
    }
    /**
     * Get Staff role name
     */
    function get_staff_role_name($id)
    {
        $roleName = '';
        $arr_role_data = StaffRoles::where([['staff_role_status', '=', 1],['staff_role_id', '=', $id]])->select('staff_role_id', 'staff_role_name')->first()->toArray();
        $roleName = $arr_role_data['staff_role_name'];
        return $roleName;
    }

     /**
     * Get shift name
     */
    function get_shift_name($id)
    {
        $shiftName = '';
        $arr_shift_data = Shift::where([['shift_status', '=', 1],['shift_id', '=', $id]])->select('shift_id', 'shift_name')->first()->toArray();
        $shiftName = $arr_shift_data['shift_name'];
        return $shiftName;
    }
    
    /**
     * Get all designations
     */
    function get_all_designations()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_designations = [];
        $arr_designation_list = Designation::where(array('designation_status' => 1))->select('designation_name', 'designation_id')->orderBy('designation_name', 'ASC')->get();
        if (!empty($arr_designation_list))
        {
            foreach ($arr_designation_list as $arr_designation)
            {
                $arr_designations[$arr_designation['designation_id']] = $arr_designation['designation_name'];
            }
        }
        return $arr_designations;
    }
    /**
     * Get designation name
     */
    function get_designation_name($id)
    {
        $designationName = '';
        $arr_designation_data = Designation::where([['designation_status', '=', 1],['designation_id', '=', $id]])->select('designation_id', 'designation_name')->first()->toArray();
        $designationName = $arr_designation_data['designation_name'];
        return $designationName;
    }

    /**
     * Get subject name
     */
    function get_subject_name($id)
    {
        $subjectName = '';
        $arr_subject_data = Subject::where([['subject_status', '=', 1],['subject_id', '=', $id]])->select('subject_id', 'subject_name', 'subject_code')->first()->toArray();
        $subjectName = $arr_subject_data['subject_name'].' - '.$arr_subject_data['subject_code'];
        return $subjectName;
    }
    
    /**
     * Get all subject here 1/8/2018 
     */
    function get_all_subject()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_subjects = [];
        $arr_subject_list = Subject::where(array('subject_status' => 1))->select('subject_name', 'subject_code', 'subject_id')->orderBy('subject_name', 'ASC')->get();
       
        if (!empty($arr_subject_list))
        {
            foreach ($arr_subject_list as $arr_subject)
            {
                $arr_subjects[$arr_subject['subject_id']] = $arr_subject['subject_name'];
            }
        }
        return $arr_subjects;
    }
    /**
     * Get all class teachers
     * @Pratyush 06 Aug 2018
     */
    function get_all_class_teachers()
    {
        $arr_staffs = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1,'teaching_status' => 1))->whereRaw('FIND_IN_SET(2,staff_role_id)')->select('staff_name', 'staff_id')->orderBy('staff_name', 'ASC')->get();
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $arr_staff)
            {
                $arr_staffs[$arr_staff['staff_id']] = $arr_staff['staff_name'];
            }
        }
        return $arr_staffs;
    }
    /**
     * Get all Staff
     * @Pratyush 06 Aug 2018
     */
    function get_all_staffs()
    {
        $arr_staffs = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1,'teaching_status' => 1))->select('staff_name', 'staff_id')->whereRaw('FIND_IN_SET(2,staff_role_id)')->orWhereRaw('FIND_IN_SET(3,staff_role_id)')->orderBy('staff_name', 'ASC')->get();
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $arr_staff)
            {
                $arr_staffs[$arr_staff['staff_id']] = $arr_staff['staff_name'];
            }
        }
        return $arr_staffs;
    }
    /**
     * Get all Staff
     * @Pratyush 06 Aug 2018
     */
    function get_multiple_staff($staff_ids)
    {
        $arr_staffs = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereIn('staff_id',$staff_ids)->select('staff_name', 'staff_id')->orderBy('staff_name', 'ASC')->get();
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $arr_staff)
            {
                $arr_staffs[$arr_staff['staff_id']] = $arr_staff['staff_name'];
            }
        }
        return $arr_staffs;
    }

    /**
     * Get class name
     */
    function get_class_name($id)
    {
        $className = '';
        if(!empty($id)) {
            $arr_class_data = Classes::where([['class_status', '=', 1],['class_id', '=', $id]])->select('class_id', 'class_name')->first()->toArray();
            $className = $arr_class_data['class_name'];
        }
        return $className;
    }

    /**
     * Get staff name
     */
    function get_staff_name($id)
    {
        $staffName = '';
        if(!empty($id)) {
            $arr_staff_data = Staff::where([['staff_status', '=', 1],['staff_id', '=', $id]])->select('staff_id', 'staff_name')->first()->toArray();
            $staffName = $arr_staff_data['staff_name'];
        }
        return $staffName;
    }

    /**
     * Get student name
     */
    function get_student_name($id)
    {
        $studentName = '';
        if(!empty($id)) {
            $arr_student_data = Student::where([['student_status', '=', 1],['student_id', '=', $id]])->select('student_id', 'student_name')->first()->toArray();
            $studentName = $arr_student_data['student_name'];
        }
        return $studentName;
    }
    
     // Get child list according to parent list
     function get_child_list($student_parent_id)
     {
        $loginInfo = get_loggedin_user_data();
        $arr_child_list = [];
        $arr_child      = Student::where(function($query) use ($loginInfo,$student_parent_id) 
        {
            if (!empty($student_parent_id))
            {
                $query->where('students.student_parent_id', "=", $student_parent_id);
            }
            
        })->with('getStudentAcademic.getCurrentClass','getStudentAcademic.getCurrentSection','getStudentMedium')->get();
        
         if (!empty($arr_child[0]->student_parent_id))
         {
             foreach ($arr_child as $child)
             {
                $profile = "";
                if (!empty($child->student_image))
                {
                    $profile = check_file_exist($child->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $profile = URL::to($profile);
                    }
                } else {
                    $profile = "";
                }
                $arr_child_list[] = array(
                    'student_parent_id'                => $child->student_parent_id,
                    'student_id'                       => $child->student_id,
                    'encrypted_student_id'             => get_encrypted_value($child->student_id, true),
                    'student_enroll_number'            => $child->student_enroll_number,
                    'student_roll_no'                  => $child->student_roll_no,
                    'student_name'                     => $child->student_name,
                    'profile'                          => $profile,
                    'student_status'                   => $child->student_status,
                    'student_class_section'            => $child['getStudentAcademic']['getCurrentClass']['class_name'].' - '.$child['getStudentAcademic']['getCurrentSection']['section_name'],
                    'student_medium'            => $child['getStudentMedium']['medium_type'],
    
                );
                
             }
         }
         return $arr_child_list;
     }
     /**
     * Get all document category
     */
    function get_all_document_category($type)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_document_categories = [];
        $arr_document_category_list = DocumentCategory::where(function($query) use ($type) 
        {
            $query->where('document_category_for', "=", $type);
            $query->orWhere('document_category_for', "=", 2);
            $query->where('document_category_status', "=", 1); 
        })
        ->select('document_category_name', 'document_category_for', 'document_category_id')->orderBy('document_category_name', 'ASC')->get();

        if (!empty($arr_document_category_list))
        {
            foreach ($arr_document_category_list as $arr_document_category)
            {
                $arr_document_categories[$arr_document_category['document_category_id']] = $arr_document_category['document_category_name'];
            }
        }
        return $arr_document_categories;
    }
    
    /*  Get class name by class id
    *   @Pratyush 08 Aug 2018.
    */ 
    
    function get_class_name_by_id($id)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_class_id = get_decrypted_value($id, true);
        $class      = Classes::select('class_name','medium_type')->where('class_id',$decrypted_class_id)->first();
        $arr_medium             = get_all_mediums();
        $class_name = '';
        if(!empty($class)){
            $class_name = $class->class_name.' - '.$arr_medium[$class->medium_type];
        }
        return $class_name;
    }
    /*  Get class name by class id
    *   @Pratyush 08 Aug 2018.
    */ 
    
    function get_section_info_by_id($id)
    {
        $section_info = [];
        $loginInfo  = get_loggedin_user_data();
        $decrypted_section_id = get_decrypted_value($id, true);
        $section      = Section::where('section_id',$decrypted_section_id)->with('sectionClass')->first();
        $arr_medium             = get_all_mediums();
        $section_info['section_id'] = $section->section_id;
        $section_info['class_id'] = $section->class_id;
        $section_info['section_name'] = $section->sectionClass['class_name'].' - '.$arr_medium[$section->sectionClass['medium_type']].' - '.$section->section_name;
        
        return $section_info;
    }
    
    /*  Get class section name by section id
    *   @Shree 10 Oct 2018.
    */ 
    
    function get_class_section_name_by_sectionid($id)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_section_id = get_decrypted_value($id, true);
        $section      = Section::where('section_id',$decrypted_section_id)->with('sectionClass')->get()->toArray();
        $arr_medium             = get_all_mediums();
        $section = isset($section[0]) ? $section[0] : [];
        $class_section_name = '';
        
        if(!empty($section)){
            $class_section_name = $section['section_class']['class_name'].' - '.$arr_medium[$section['section_class']['medium_type']].' - '.$section['section_name'];
        }
        return $class_section_name;
    }
    
    /*  Get All Terms 
    *   @Pratyush 11 Aug 2018.
    */ 
    
    function get_all_terms()
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_term   = [];
        $arr_term_list   = Term::where(array('term_exam_status' => 1))->select('term_exam_name','term_exam_id')->orderBy('term_exam_name', 'ASC')->get();
        
        if (!empty($arr_term_list))
        {
            foreach ($arr_term_list as $arr_term_value)
            {
                $arr_term[$arr_term_value['term_exam_id']] = $arr_term_value['term_exam_name'];
            }
        }
        return $arr_term;
    }
    
    /*  Get All Cupboard data
    *   @Pratyush 13 Aug 2018.
    */ 
    
    function get_all_cupbpard_data()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_capboard           = [];
    
        $arr_cupboard_list  = BookCupboard::where(array('book_cupboard_status' => 1))->select('book_cupboard_name','book_cupboard_id')->orderBy('book_cupboard_name', 'ASC')->get();
        
        if (!empty($arr_cupboard_list))
        {
            foreach ($arr_cupboard_list as $arr_cupboard_value)
            {
                $arr_capboard[$arr_cupboard_value['book_cupboard_id']] = $arr_cupboard_value['book_cupboard_name'];
            }
        }
        return $arr_capboard;
    }
    
    /*  Get All Book Category
    *   @Pratyush 14 Aug 2018.
    */ 
    
    function get_all_book_categoory_data()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_book_category      = [];
    
        $arr_cupboard_list  = BookCategory::where(array('book_category_status' => 1))->select('book_category_name','book_category_id')->orderBy('book_category_name', 'ASC')->get();
        
        if (!empty($arr_cupboard_list))
        {
            foreach ($arr_cupboard_list as $arr_cupboard_value)
            {
                $arr_book_category[$arr_cupboard_value['book_category_id']] = $arr_cupboard_value['book_category_name'];
            }
        }
        return $arr_book_category;
    }
    
    /*  Get All Book Vendors
    *   @Pratyush 14 Aug 2018.
    */ 
    
    function get_all_book_vendor_data()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_book_vendor        = [];
    
        $arr_vendor_list  = BookVendor::where(array('book_vendor_status' => 1))->select('book_vendor_name','book_vendor_id')->orderBy('book_vendor_name', 'ASC')->get();
        
        if (!empty($arr_vendor_list))
        {
            foreach ($arr_vendor_list as $arr_vendor_value)
            {
                $arr_book_vendor[$arr_vendor_value['book_vendor_id']] = $arr_vendor_value['book_vendor_name'];
            }
        }
        return $arr_book_vendor;
    }
    
    /*  Get CupBoard Shelf
    *   @Pratyush 16 Aug 2018.
    */ 
    function get_cupboard_shelf($cupboard_id=null)
    {
        $loginInfo          = get_loggedin_user_data();
        $cupboard_shelf     = [];
        if(isset($cupboard_id) && $cupboard_id != null){
            $arr_section = BookCupboardshelf::where([['book_cupboardshelf_status', '=', 1],['book_cupboard_id', '=', $cupboard_id]])->select('book_cupboardshelf_id', 'book_cupboardshelf_name')->get();
        } else {
            $arr_section = BookCupboardshelf::where(['book_cupboardshelf_status', '=', 1])->select('book_cupboardshelf_id', 'book_cupboardshelf_name')->get();
        }
        
        if (!empty($arr_section))
        {
            foreach ($arr_section as $value)
            {
                $cupboard_shelf[$value['book_cupboardshelf_id']] = $value['book_cupboardshelf_name'];
            }
        }
        return $cupboard_shelf;
    }
    
    /*  Get Book Type
    *   @Pratyush 16 Aug 2018.
    */ 
    function get_book_type($cupboard_id=null)
    {
        $book_type       = [];
        $book_type       = array(
                '0' => 'General',
                '1' => 'Barcoded'
            ); 
        return $book_type;
    }
    
     /**
     * Get all subject here @Ashish 21/8/2018 
     */
    function get_all_exam_type()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_exam_types = [];
        $arr_exam_type_list = Exam::where(array('exam_status' => 1))->select('exam_name', 'exam_id')->orderBy('exam_id', 'DESC')->get();
       
        if (!empty($arr_exam_type_list))
        {
            foreach ($arr_exam_type_list as $arr_exam_type)
            {
                $arr_exam_types[$arr_exam_type['exam_id']] = $arr_exam_type['exam_name'];
            }
        }
        return $arr_exam_types;
    }
    
    /**
     * Get all subject here @Ashish 22/8/2018 
     */
    function get_all_subject_mapping($class_id = false)
    {
        
        $loginInfo              = get_loggedin_user_data();
        $arr_subject_mapping = [];
        $arr_subject_mapping_list = SubjectClassMapping::where('class_id','=',$class_id)->with('getSubjects')->get();
    
       
        if (!empty($arr_subject_mapping_list))
        {
            foreach ($arr_subject_mapping_list as $arr_subject_mappings)
            {
                $arr_subject_mapping[$arr_subject_mappings['subject_id']] = $arr_subject_mappings['getSubjects']['subject_name'];
            }
        }
        return $arr_subject_mapping;
    }
    
    /**
     * Get all school boards 
     */
    function get_all_school_boards()
    {
        $arr_boards = [];
        $arr_boards_list = SchoolBoard::where(array('board_status' => 1))->select('board_name', 'board_id')->orderBy('board_id', 'ASC')->get();
        if (!empty($arr_boards_list))
        {
            foreach ($arr_boards_list as $arr_board)
            {
                $arr_boards[$arr_board['board_id']] = $arr_board['board_name'];
            }
        }
        return $arr_boards;
    }
    
    /**
    * Get all country
    */
    function get_all_country()
    {
        $arr_country_data = [];
        $arr_country_list = Country::where(array('country_status' => 1))->select('country_name', 'country_id')->orderBy('country_name', 'ASC')->get();
        if (!empty($arr_country_list))
        {
            foreach ($arr_country_list as $arr_country)
            {
                $arr_country_data[$arr_country['country_id']] = $arr_country['country_name'];
            }
        }
        return $arr_country_data;
    }

    /**
     * Get country name
     */
    function get_country_name($id)
    {
        $countryName = '';
        if(!empty($id)) {
            $arr_country_data = Country::where([['country_status', '=', 1],['country_id', '=', $id]])->select('country_id', 'country_name')->first()->toArray();
            $countryName = $arr_country_data['country_name'];
        }
        return $countryName;
    }
    
    /**
    * Get all states without country id
    */
    function getting_all_states()
    {
        $arr_state_data = [];
        $arr_state_list = State::where(array('state_status' => 1))->select('state_name', 'state_id')->orderBy('state_name', 'DESC')->get();
        if (!empty($arr_state_list))
        {
            foreach ($arr_state_list as $arr_state)
            {
                $arr_state_data[$arr_state['state_id']] = $arr_state['state_name'];
            }
        }
        return $arr_state_data;
    }
    /**
    * Get all states
    */
    function get_all_states($country_id)
    {
        $arr_state_data = [];
        $arr_state_list = State::where(array('state_status' => 1, 'country_id' => $country_id))->select('state_name', 'state_id')->orderBy('state_name', 'ASC')->get();
        if (!empty($arr_state_list))
        {
            foreach ($arr_state_list as $arr_state)
            {
                $arr_state_data[$arr_state['state_id']] = $arr_state['state_name'];
            }
        }
        return $arr_state_data;
    }
    /**
     * Get state name
     */
    function get_state_name($id)
    {
        $stateName = '';
        if(!empty($id)) {
            $arr_state_data = State::where([['state_status', '=', 1],['state_id', '=', $id]])->select('state_id', 'state_name')->first()->toArray();
            $stateName = $arr_state_data['state_name'];
        }
        return $stateName;
    }
    /**
    * Get all city
    */
    function get_all_city($state_id)
    {
        $arr_city_data = [];
        $arr_city_list = City::where(array('city_status' => 1, 'state_id' => $state_id))->select('city_name', 'city_id')->orderBy('city_name', 'ASC')->get();
        if (!empty($arr_city_list))
        {
            foreach ($arr_city_list as $arr_city)
            {
                $arr_city_data[$arr_city['city_id']] = $arr_city['city_name'];
            }
        }
        return $arr_city_data;
    }
    /**
     * Get city name
     */
    function get_city_name($id)
    {
        $cityName = '';
        if(!empty($id)) {
            $arr_city_data = City::where([['city_status', '=', 1],['city_id', '=', $id]])->select('city_id', 'city_name')->first()->toArray();
            $cityName = $arr_city_data['city_name'];
        }
        return $cityName;
    }
    
    /**
     * Get all classes 
     */
    function get_all_streams()
    {
        $arr_streams = [];
        $arr_stream_list = Stream::where(array('stream_status' => 1))->select('stream_name', 'stream_id')->orderBy('stream_name', 'ASC')->get();
        if (!empty($arr_stream_list))
        {
            foreach ($arr_stream_list as $arr_stream)
            {
                $arr_streams[$arr_stream['stream_id']] = $arr_stream['stream_name'];
            }
        }
        return $arr_streams;
    }
    
    /** 
     * Check section capacity
     */
    
    function check_section_capacity($section_id)
    {
        $sectionInfo = Section::where(array('section_status' => 1,'section_id'=> $section_id))->select('section_intake', 'section_id')->first();
    
        $studentCount = StudentAcademic::where(array('current_section_id'=> $section_id))->count();
        if($sectionInfo->section_intake > $studentCount){
            return "True";
        } else {
            return "False";
        }
    }
    
    /** 
     * Get student for roll number assign feature
     */
    
    function assign_roll_no_by_section($section_id){
        $arr_student_list = [];
        $arr_student      = Student::where(array('students.student_status'=> 1))->join('student_academic_info', function($join) use ($section_id){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            $join->where('current_section_id', '=',$section_id);
        })->select('students.student_name', 'students.student_id','students.student_roll_no','student_academic_info.current_section_id')->orderBy('student_name', 'ASC')->get()->toArray();
    
        $key = 1;
        foreach($arr_student as $student){
            $studentData = Student::find($student['student_id']);
            $studentData->student_roll_no = $key;
            $studentData->save();
            $key++;
        }
        return "Success";
    }
    
    /**
     * Get all brochures
     */
    function get_all_brochure($session_id)
    {
        $arr_brochures = [];
        $arr_brochure_list = Brochure::where(array('brochure_status' => 1, 'session_id'=> $session_id))->select('brochure_name', 'brochure_id')->orderBy('brochure_id', 'ASC')->get();
        if (!empty($arr_brochure_list))
        {
            foreach ($arr_brochure_list as $arr_brochure)
            {
                $arr_brochures[$arr_brochure['brochure_id']] = $arr_brochure['brochure_name'];
            }
        }
        return $arr_brochures;
    }
    
    /** 
     * Check class capacity
     */
    
    function get_total_intake($class_id)
    {
        $totalCount = DB::table('sections')
        ->join('classes', 'sections.class_id', '=', 'classes.class_id')
        ->where('sections.section_status', '=', 1)
        ->where('sections.class_id', '=', $class_id)
        ->sum('sections.section_intake');
       
        $studentCount = StudentAcademic::where(array('current_class_id'=> $class_id))->count();
        
        if($totalCount > $studentCount){
            return $totalCount - $studentCount;
        } else {
            return 0;
        }
    }
    
    /** 
     * Check class total capacity
     */
    
    function get_overall_intake($class_id)
    {
        $totalCount = DB::table('sections')
        ->join('classes', 'sections.class_id', '=', 'classes.class_id')
        ->where('sections.section_status', '=', 1)
        ->where('sections.class_id', '=', $class_id)
        ->sum('sections.section_intake');
        
        return $totalCount;
    }
    
    function get_boards_info($data){
        $data = explode(',', $data);
        $name = [];
        $boards = get_all_school_boards();
        foreach ($boards as $key => $keyVal) {
            if(in_array($key, $data)){
                $name[] = $keyVal;
            }
        }
        return implode(',', $name);
    }

    function get_medium_info($data){
        $data = explode(',', $data);
        $name = [];
        $mediums = get_all_mediums();
        foreach ($mediums as $key => $keyVal) {
            if(in_array($key, $data)){
                $name[] = $keyVal;
            }
        }
        return implode(',', $name);
    }
    
    function get_all_class_subject($class_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_subjects = [];
        $arr_subject_list = SubjectClassmapping::where(array('class_id' => $class_id))->with('getSubjects')->get();
        if (!empty($arr_subject_list))
        {
            foreach ($arr_subject_list as $arr_subject)
            {
                $arr_subjects[$arr_subject['subject_id']] = $arr_subject['getSubjects']['subject_name'];
            }
        }
        return $arr_subjects;
    }
    
    function check_admission_field_availability($admission_form_id){
        $status  = "false";
        $data = AdmissionFields::where(array('admission_form_id'=> $admission_form_id))->count();
        if($data > 0){
            $status = "true";
        }
        return $status;
    }
    
    function get_form_number($admission_form_id){
        $data  = 0;
        $info = AdmissionForm::where(array('admission_form_id' => $admission_form_id))->select('form_prefix')->get();
        $info = isset($info[0]) ? $info[0] : [];
        $data = AdmissionData::where(array('admission_form_id'=> $admission_form_id))->count();
        $data++;
        return $info->form_prefix.$data;
    }
    
    function get_available_section($class_id){
        $available = 0;
        $allSections = DB::table('sections')
        ->join('classes', 'sections.class_id', '=', 'classes.class_id')
        ->where('sections.section_status', '=', 1)
        ->where('sections.class_id', '=', $class_id)->get();
        
        foreach($allSections as $section) {
            $studentCount = StudentAcademic::where(array('current_class_id'=> $class_id,'current_section_id'=> $section->section_id))->count();
            $available = $section->section_intake - $studentCount;
            if($available > 0){
                return $section->section_id;
            }
        }
    }
    
    function store_admission_data($admission_data_id,$class_id,$section_id){
        $loginInfo                      = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $admissionData = AdmissionData::find($admission_data_id);
        
        $student            = New Student;
        $studentAdmin       = new Admin();
        $parentAdmin       = new Admin();
        $student_parent     = new StudentParent();
        $student_academic   = new StudentAcademic();
        $student_health     = new StudentHealth();
    
        DB::beginTransaction(); //Start transaction!
    
        try
        {
            $studentAdmin->admin_name = $admissionData->student_name;
            $studentAdmin->email = $admissionData->student_enroll_number;
            // $studentPassword = str_random(8);
            $studentPassword = "student@123!";
            $studentAdmin->admin_type = 4;
            $studentAdmin->password = Hash::make($studentPassword);
            $studentAdmin->save();

            $parentAdmin->admin_name = $admissionData->student_name;
            $parentAdmin->email = $admissionData->student_enroll_number;
            // $studentPassword = str_random(8);
            $parentPassword = "parent@123!";
            $parentAdmin->admin_type = 4;
            $parentAdmin->password = Hash::make($parentPassword);
            $parentAdmin->save();
            // $message = "";
            // $subject = "Welcome to ".trans('language.project_title')." | Student Panel";
            // $emailData = [];
            // $emailData['sender'] = trans('language.email_sender');
            // $emailData['email'] = $admissionData->student_email;
            // $emailData['password'] = $studentPassword;
            // $emailData['receiver'] = $admissionData->student_name;
            // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
            // {
            //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
            
            // });
    
            $student_parent->admin_id                        = $admin_id;
            $student_parent->update_by                       = $loginInfo['admin_id'];
            $student_parent->student_father_name             = $admissionData->student_father_name;
            $student_parent->student_father_mobile_number    = $admissionData->student_father_mobile_number;
            $student_parent->student_father_email            = $admissionData->student_father_email;
            $student_parent->student_father_occupation       = $admissionData->student_father_occupation;
            $student_parent->student_mother_name             = $admissionData->student_mother_name;
            $student_parent->student_mother_mobile_number    = $admissionData->student_mother_mobile_number;
            $student_parent->student_mother_email            = $admissionData->student_mother_email;
            $student_parent->student_mother_occupation       = $admissionData->student_mother_occupation;
            $student_parent->student_mother_tongue           = $admissionData->student_mother_tongue;
            $student_parent->student_guardian_name           = $admissionData->student_guardian_name;
            $student_parent->student_guardian_email          = $admissionData->student_guardian_email;
            $student_parent->student_guardian_mobile_number  = $admissionData->student_guardian_mobile_number;
            $student_parent->student_father_annual_salary    = $admissionData->student_father_annual_salary;
            $student_parent->student_mother_annual_salary    = $admissionData->student_mother_annual_salary;
            $student_parent->student_guardian_relation       = $admissionData->student_guardian_relation;
            $student_parent->save();
      
            // student table data
            $student->admin_id                      = $admin_id;
            $student->update_by                     = $loginInfo['admin_id'];
            $student->reference_admin_id            = $studentAdmin->admin_id;
            $student->student_parent_id             = $student_parent->student_parent_id;
            $student->student_enroll_number         = $admissionData->student_enroll_number;
            $student->student_type                  = $admissionData->student_type;
            $student->medium_type                   = $admissionData->medium_type;
            $student->student_email                 = $admissionData->student_email;
            $student->student_reg_date              = $admissionData->student_reg_date;
            $student->student_name                  = $admissionData->student_name;
            $student->student_gender                = $admissionData->student_gender;
            $student->student_dob                   = $admissionData->student_dob;
            $student->student_adhar_card_number     = $admissionData->student_adhar_card_number;
            $student->student_category              = $admissionData->student_category;
            $student->caste_id                      = $admissionData->caste_id;
            $student->title_id                      = $admissionData->title_id;
            $student->religion_id                   = $admissionData->religion_id;
            $student->nationality_id                = $admissionData->nationality_id;
            $student->student_sibling_name          = $admissionData->student_sibling_name;
            $student->student_sibling_class_id      = $admissionData->student_sibling_class_id;
            $student->student_temporary_address     = $admissionData->student_temporary_address;
            $student->student_temporary_city        = $admissionData->student_temporary_city;
            $student->student_temporary_state       = $admissionData->student_temporary_state;
            $student->student_temporary_county      = $admissionData->student_temporary_county;
            $student->student_temporary_pincode     = $admissionData->student_temporary_pincode;
            $student->student_permanent_address     = $admissionData->student_permanent_address;
            $student->student_permanent_city        = $admissionData->student_permanent_city;
            $student->student_permanent_state       = $admissionData->student_permanent_state;
            $student->student_permanent_county      = $admissionData->student_permanent_county;
            $student->student_permanent_pincode     = $admissionData->student_permanent_pincode;
            
            $student->student_privious_school       = $admissionData->student_privious_school;
            $student->student_privious_class        = $admissionData->student_privious_class;
            $student->student_privious_tc_no        = $admissionData->student_privious_tc_no;
            $student->student_privious_tc_date      = $admissionData->student_privious_tc_date;
            $student->student_privious_result       = $admissionData->student_privious_result;
            $student->save();

            $admissionData->student_id = $student->student_id;
            $admissionData->admission_data_status = 2;
            $admissionData->save();
    
            // student academic table data
            $student_academic->admin_id               = $admin_id;
            $student_academic->update_by              = $loginInfo['admin_id'];
            $student_academic->student_id             = $student->student_id;
            $student_academic->admission_session_id   = $admissionData->admission_session_id;
            $student_academic->admission_class_id     = $class_id;
            $student_academic->admission_section_id   = $section_id;
            $student_academic->current_session_id     = $admissionData->current_session_id;
            $student_academic->current_class_id       = $class_id;
            $student_academic->current_section_id     = $section_id;
            $student_academic->student_unique_id      = null;
            $student_academic->group_id               = $admissionData->group_id;
            $student_academic->stream_id              = $admissionData->stream_id;
            $student_academic->save();
    
            // student health table data
            $student_health->admin_id               = $admin_id;
            $student_health->update_by              = $loginInfo['admin_id'];
            $student_health->student_id             = $student->student_id;
            $student_health->student_height         = $admissionData->student_height;
            $student_health->student_weight         = $admissionData->student_weight;
            $student_health->student_blood_group    = $admissionData->student_blood_group;
            $student_health->student_vision_left    = $admissionData->student_vision_left;
            $student_health->student_vision_right   = $admissionData->student_vision_right;
            $student_health->medical_issues         = $admissionData->medical_issues;
            $student_health->save();
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            return $error_message;
        }
        DB::commit();
        return "Success";
    }
    
    function get_class_stream_status($class_id){
        $class_info = Classes::find($class_id);
        if($class_info->class_stream == 1){
            return "yes";
        } else {
            return "No";
        }
       
        
    }
     /**
     * Get all bank
     */
    function get_all_bank()
    {
        $arr_bank = [];
        $arr_bank_list = Bank::where(array('bank_status' => 1))->select('bank_name', 'bank_id')->orderBy('bank_name', 'ASC')->get();
        if (!empty($arr_bank_list))
        {
            foreach ($arr_bank_list as $arr_banks)
            {
                $arr_bank[$arr_banks['bank_id']] = $arr_banks['bank_name'];
            }
        }
        return $arr_bank;
    }
    
    /*  Get All Heads according to types 
    *   @Shree 8 Oct 2018.
    */ 
    
    function get_all_heads($type)
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_heads   = [];
        // For one time
        if($type == 0) {
            $arr_head_list   = OneTime::where(array('ot_status' => 1))->select('ot_particular_name','ot_head_id')->orderBy('ot_particular_name', 'ASC')->get();
            
            if (!empty($arr_head_list))
            {
                foreach ($arr_head_list as $arr_head_value)
                {
                    $head_id = $arr_head_value['ot_head_id'];
                    $arr_heads[$head_id] = $arr_head_value['ot_particular_name'];
                }
            }
            // For recurring
        } else if($type == 1) {
            $arr_head_list   = RecurringHead::where(array('rc_status' => 1))->select('rc_particular_name','rc_head_id')->orderBy('rc_particular_name', 'ASC')->get();
            
            if (!empty($arr_head_list))
            {
                foreach ($arr_head_list as $arr_head_value)
                {
                    $head_id = $arr_head_value['rc_head_id'];
                    $arr_heads[$head_id] = $arr_head_value['rc_particular_name'];
                }
            }
        }
        return $arr_heads;
    }
    
    
    /*  Get head name according to types 
    *   @Shree 8 Oct 2018.
    */ 
    
    function get_head_name($type,$id)
    {
        $loginInfo  = get_loggedin_user_data();
        $head   = '';
        // For one time
        if($type == 0) {
            $arr_head_list   = OneTime::where(array('ot_status' => 1, 'ot_head_id'=>$id))->select('ot_particular_name','ot_head_id')->orderBy('ot_particular_name', 'ASC')->get();
            return $arr_head_list[0]['ot_particular_name'];
            
            // For recurring
        } else if($type == 1) {
            $arr_head_list   = RecurringHead::where(array('rc_status' => 1, 'rc_head_id'=>$id))->select('rc_particular_name','rc_head_id')->orderBy('rc_particular_name', 'ASC')->get();
            return $arr_head_list[0]['rc_particular_name'];
            
        }
        return $head;
    }
    
    /*  Get All RTE Heads 
    *   @Shree 9 Oct 2018.
    */ 
    function get_all_rte_heads(){
        $arr_heads   = [];
        $arr_head_list   = RTEHead::where(array('rte_status' => 1))->select('rte_head','rte_head_id')->orderBy('rte_head', 'ASC')->get();
        
        if (!empty($arr_head_list))
        {
            foreach ($arr_head_list as $arr_head_value)
            {
                $arr_heads[$arr_head_value['rte_head_id']] = $arr_head_value['rte_head'];
            }
        }
        return $arr_heads;
    } 
    
    /*  Get Complete fees by class id
    *   @Shree 10 Oct 2018.
    */ 
    function get_complete_fees($request){
        
        $session = get_current_session();
        $class_id = $request->get('class_id');
        $student_id = $request->get('student_id');
        $arr_heads   = [];
        $currentDate = date('Y-m-d');
        
        $arr_onetime_head_list   = OneTime::whereRaw('FIND_IN_SET('.$class_id.',ot_classes)')
        ->where(array('ot_status' => 1,'deduct_imprest_status' => 0))
        ->leftJoin('concession_map', function($joinDD) use($session,$class_id,$student_id) {
            $joinDD->on('concession_map.head_id', '=', 'one_time_heads.ot_head_id');
            $joinDD->where('concession_map.session_id', '=',$session['session_id']);
            $joinDD->where('concession_map.class_id', '=',$class_id);
            $joinDD->where('concession_map.student_id', '=',$student_id);
            $joinDD->where('concession_map.head_type', '=','ONETIME');
        })
        ->orderBy('ot_head_id', 'ASC')->get();
        // p($arr_onetime_head_list);
        if (!empty($arr_onetime_head_list))
        {
            foreach ($arr_onetime_head_list as $arr_head_value)
            {
                $checkFeesPaid = checkFeesPaidStatus($session['session_id'],$class_id,$student_id,$arr_head_value->ot_head_id,'ONETIME');
                $counter = COUNT($arr_heads);
                $headData = [];
                $feesExpire = 0;
                if($arr_head_value->concession_status == 1){
                    $feesExpire = 1;
                }
                if($checkFeesPaid > 0){
                    $feesExpire = 1;
                }
                $headData['counter'] = $counter;
                $headData['head_id'] = $arr_head_value->ot_head_id;
                $headData['head_name'] = $arr_head_value->ot_particular_name;
                $headData['head_amt'] = $arr_head_value->ot_amount;
                $headData['head_type'] = "ONETIME";
                $headData['head_date'] = $arr_head_value->ot_date;
                $headData['head_expire'] = $feesExpire;
                $headData['head_inst_id'] = null;
                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                $headData['concession_amt'] = $arr_head_value->concession_amt;
                $headData['concession_status'] = $arr_head_value->concession_status;
                $headData['reason_id'] = $arr_head_value->reason_id;
                $headData['class_id'] = $class_id;
                $headData['student_id'] = $student_id;
                
                $arr_heads[] = $headData;
            }
        }
        $arr_recurring_head_list   = RecurringHead::whereRaw('FIND_IN_SET('.$class_id.',rc_classes)')->where(array('rc_status' => 1))
        ->join('recurring_inst', function($join) {
            $join->on('recurring_inst.rc_head_id', '=', 'recurring_heads.rc_head_id');
        })
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('head_type', '=','RECURRING');
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
        })
        ->orderBy('rc_particular_name', 'ASC')->get();
        
        if (!empty($arr_recurring_head_list))
        {
            foreach ($arr_recurring_head_list as $arr_head_value)
            {
                $checkFeesPaid = checkFeesPaidStatus($session['session_id'],$class_id,$student_id,$arr_head_value->rc_inst_id,'RECURRING');
                $counter = COUNT($arr_heads);
                $headData = [];
                $feesExpire1 = 0;
                if($arr_head_value->concession_status == 1){
                    $feesExpire1 = 1;
                }
                if($checkFeesPaid > 0){
                    $feesExpire1 = 1;
                }
                $headData['counter'] = $counter;
                $headData['head_id'] = $arr_head_value->rc_inst_id;
                $headData['head_name'] = $arr_head_value->rc_inst_particular_name;
                $headData['head_amt'] = $arr_head_value->rc_inst_amount;
                $headData['head_type'] = "RECURRING";
                $headData['head_date'] = $arr_head_value->rc_inst_date;
                $headData['head_expire'] = $feesExpire1;
                $headData['head_inst_id'] = $arr_head_value->rc_inst_id;
                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                $headData['concession_amt'] = $arr_head_value->concession_amt;
                $headData['concession_status'] = $arr_head_value->concession_status;
                $headData['reason_id'] = $arr_head_value->reason_id;
                $headData['class_id'] = $class_id;
                $headData['student_id'] = $student_id;
               $arr_heads[] = $headData;
            }
        }
        // p($arr_heads);
        return $arr_heads;
        
    } 


    // @Pratyush on 11 Oct 2018
    // Check Token for APIs Auth , it will return true or false
   
    function validate_token($device_id='',$token=''){
        $return = false;
            if($device_id != '' && $token != ''){
            //    ! $token->isEmpty()
                $token = ApiToken::where('device_id',$device_id)->where('csrf_token',$token)->first();
                if(!empty($token)){
                    $return = true;
                }
            }
        return $return;
    }
    
    /*  Get Imprest ac configuration amt
    *   @Shree 12 Oct 2018.
    */ 
    function get_imprest_ac_config_amt(){
        $imprestAmt = 0;
        $school = School::first()->toArray();
        if($school['imprest_ac_status'] == 1){
            $imprestAcConfig = ImprestAcConfig::first()->toArray();
            $imprestAmt = $imprestAcConfig['imprest_ac_confi_amt'];
        }
        return $imprestAmt;
    }
    
    /*  Get student's account existance
    *   @Shree 12 Oct 2018.
    */ 
    function check_account_existance($type,$student_id){
        $check = 0;
        if($type =="Imprest"){
            $acInfo = ImprestAc::where(array('student_id' => $student_id))->first();
            if(isset($acInfo->imprest_ac_id)){
                $check = 1;
            }
        }
        if($type =="Wallet"){
            $acInfo = WalletAc::where(array('student_id' => $student_id))->first();
            if(isset($acInfo->wallet_ac_id)){
                $check = 1;
            }
        }
        return $check;
    }
    
    /*  Get classes of fees head
    *   @Shree 13 Oct 2018.
    */ 
    function get_head_classes($head_id,$type){
        $arr_classes = [];
        if($type == "ONETIME") {
            $headinfo  	= OneTime::where(function($query) use ($head_id) 
            {
                $query->where('ot_head_id', "=", $head_id);
            })->orderBy('ot_head_id', 'DESC')->get()->toArray();
            
            $classIds = explode(',',$headinfo[0]['ot_classes']);
        } else if($type == "RECURRING") {
            $headinfo  	= RecurringHead::where(function($query) use ($head_id) 
            {
                $query->where('rc_head_id', "=", $head_id);
            })->orderBy('rc_head_id', 'DESC')->get()->toArray();
            
            $classIds = explode(',',$headinfo[0]['rc_classes']);
        }
        $arr_classes_list = Classes::whereIn('class_id',$classIds)->select('class_name','medium_type', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_medium     = get_all_mediums();
                $class_name     = $arr_class['class_name']." - ".$arr_medium[$arr_class['medium_type']];
                $arr_classes[$arr_class['class_id']] = $class_name;
            }
        }
    
        return $arr_classes;
    }
    
    /*  Get fine amount according to days and head id
    *   @Shree 16 Oct 2018.
    */ 
    function get_fine_info($total_days,$head_id,$head_type){
        
        $for = null;
        $session = get_current_session();
        if($head_type == "ONETIME"){
            $for = 0;
        } else if($head_type == 'RECURRING') {
            $for = 1;
        } else if($head_type == 'FACILITY'){
            $for = 2;
        }
        $fineInfo = [];
        $fineData   = Fine::where(array('head_id' => $head_id,'fine_for' => $for))
        ->leftJoin('fine_details', function($join) use($session,$total_days) {
            $join->on('fine_details.fine_id', '=', 'fine.fine_id');
            $join->where('fine_detail_days', '>=',$total_days);
        })
        ->orderBy('fine.fine_id', 'ASC')->first();
       
        if(!empty($fineData)) {
            $fineInfo = array(
                'fine_id' => $fineData->fine_id,
                'fine_name' => $fineData->fine_name,
                'fine_detail_id' => $fineData->fine_detail_id,
                'fine_detail_days' => $fineData->fine_detail_days,
                'fine_detail_amt' => $fineData->fine_detail_amt,
            );
        }
        return $fineInfo;
    }
    
    /*  Get Complete fees by student id and class id
    *   @Shree 16 Oct 2018.
    */ 
    function get_complete_fees_student($student_id,$class_id){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $arr_heads  = [];
        $map_onetime_head_for_student = $map_recurring_head_for_student = $map_onetime_receipt = $map_recurring_receipt = [];
        $currentDate = date('Y-m-d');
        $arr_onetime_head_list   = OneTime::whereRaw('FIND_IN_SET('.$class_id.',ot_classes)')
        ->where(array('ot_status' => 1,'deduct_imprest_status' => 0))
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'one_time_heads.ot_head_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('concession_map.head_type', '=','ONETIME');
        })
        ->leftJoin('fee_receipt_details', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt_details.head_id', '=', 'one_time_heads.ot_head_id');
            $join->where('fee_receipt_details.head_type', '=','ONETIME');
            $join->where('inst_status', '=',0);
            $join->where('cancel_status', '=',0);
        })
        ->orderBy('ot_head_id', 'ASC')->groupBy('ot_head_id')->get();

        $map_onetime_head_for_student = FeesStMap::whereRaw('FIND_IN_SET('.$student_id.',student_ids)')
        ->where('session_id', '=',$session['session_id'])
        ->where('head_type', '=','ONETIME')
        ->orderBy('fees_st_map_id', 'DESC')->get()->toArray();
        
        $map_onetime_receipt = ReceiptDetail::
        where('head_type', '=','ONETIME')
        // ->where('inst_status', '=','1')
        ->where('session_id', '=',$session['session_id'])
        ->where('receipt_status', '=','1')
        ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=','ONETIME');
            $join->where('receipt_status', '=',1);
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
        
        if (!empty($arr_onetime_head_list))
        {
            foreach ($arr_onetime_head_list as $arr_head_value)
            {
                if(!in_array($arr_head_value->ot_head_id, array_column($map_onetime_head_for_student, 'head_id'))){
                
                    if(!empty($map_onetime_receipt)){
                        $head_paid_amt = $head_pay_amt = 0;
                        foreach ($map_onetime_receipt as $receiptValues) {
                            if($receiptValues['inst_paid_amt'] != "" && $receiptValues['head_id'] == $arr_head_value->ot_head_id){
                                $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                $head_paid_amt += $receiptValues['inst_paid_amt'];
                            }
                        }
                    }
                    if(!in_array($arr_head_value->ot_head_id, array_column($map_onetime_receipt[0], 'head_id'))){
                    
                        $counter = COUNT($arr_heads);
                        $headData = [];
                        $feesExpire = 0;
                        $total_delay_days = 0;
                        $fineinfo = [];
                        if($currentDate > $arr_head_value->ot_date){
                            $feesExpire = 1;
                            $datediff = strtotime($currentDate) - strtotime($arr_head_value->ot_date);
                            $total_delay_days =  round($datediff / (60 * 60 * 24));
                            $fineinfo = get_fine_info($total_delay_days,$arr_head_value->ot_head_id,'ONETIME');
                        }
                        $head_pay_amt = $arr_head_value->ot_amount;
                        $head_pay_amt = $head_pay_amt - $head_paid_amt;
                        if($head_pay_amt != 0){
                            $headData['counter'] = $counter;
                            $headData['head_id'] = $arr_head_value->ot_head_id;
                            $headData['head_name'] = $arr_head_value->ot_particular_name;
                            $headData['head_amt'] = $arr_head_value->ot_amount;
                            $headData['head_paid_amt'] = $head_paid_amt;
                            $headData['head_pay_amt'] = $head_pay_amt;
                            $headData['head_type'] = "ONETIME";
                            $headData['head_month_year'] = NULL;
                            $headData['head_date'] = $arr_head_value->ot_date;
                            $headData['head_expire'] = $feesExpire;
                            $headData['head_inst_id'] = null;
                            $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                            $headData['concession_amt'] = $arr_head_value->concession_amt;
                            $headData['concession_status'] = $arr_head_value->concession_status;
                            $headData['class_id'] = $class_id;
                            $headData['student_id'] = $student_id;
                            $headData['total_delay_days'] = $total_delay_days;
                            $headData['fine_info'] = $fineinfo;
                            $headData['map_student_staff_id'] = NULL;
                            $headData['editable'] = 1;
                
                            $arr_heads[] = $headData;
                        }
                    } 
                    
                }
            }
        }
        // p($arr_heads);
        $arr_recurring_head_list   = RecurringHead::select()->whereRaw('FIND_IN_SET('.$class_id.',rc_classes)')->where(array('rc_status' => 1))
        ->join('recurring_inst', function($join) {
            $join->on('recurring_inst.rc_head_id', '=', 'recurring_heads.rc_head_id');
        })
        ->leftJoin('concession_map', function($join) use($session,$class_id,$student_id) {
            $join->on('concession_map.head_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('concession_map.session_id', '=',$session['session_id']);
            $join->where('concession_map.head_type', '=','RECURRING');
            $join->where('concession_map.class_id', '=',$class_id);
            $join->where('concession_map.student_id', '=',$student_id);
        })
        ->leftJoin('fee_receipt_details', function($join) use($session,$class_id,$student_id) {
            $join->on('fee_receipt_details.head_inst_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('fee_receipt_details.head_type', '=','RECURRING');
            $join->where('inst_status', '=',0);
            $join->where('cancel_status', '=','0');
        })
        ->orderBy('rc_particular_name', 'ASC')->get();
        $map_recurring_head_for_student = FeesStMap::whereRaw('FIND_IN_SET('.$student_id.',student_ids)')
        ->where('session_id', '=',$session['session_id'])
        ->where('head_type', '=','RECURRING')
        ->orderBy('fees_st_map_id', 'DESC')->get()->toArray();

        $map_recurring_receipt = ReceiptDetail::
            where('head_type', '=','RECURRING')
            // ->where('inst_status', '=','1')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','RECURRING');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
        
        if (!empty($arr_recurring_head_list))
        {
            foreach ($arr_recurring_head_list as $arr_head_value)
            {
                if(!in_array($arr_head_value->rc_head_id, array_column($map_recurring_head_for_student, 'head_id'))){
                        if(!empty($map_recurring_receipt)){
                            $head_paid_amt = $head_pay_amt = 0;
                            foreach ($map_recurring_receipt as $receiptValues) {
                                if($arr_head_value->rc_inst_id == $receiptValues['head_inst_id']){
                                    if($receiptValues['inst_paid_amt'] != ""){
                                        $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                        $head_paid_amt += $receiptValues['inst_paid_amt'];
                                    }
                                }
                                
                            }
                        }
                        if(!in_array($arr_head_value->rc_inst_id, array_column($map_recurring_receipt[0], 'head_inst_id'))){
                            $counter = COUNT($arr_heads);
                            $headData = [];
                            $feesExpire1 = 0;
                            $total_delay_days = 0;
                            $fineinfo = [];
                            if($currentDate > $arr_head_value->rc_inst_date){
                                $feesExpire = 1;
                                $datediff = strtotime($currentDate) - strtotime($arr_head_value->rc_inst_date);
                                $total_delay_days =  round($datediff / (60 * 60 * 24));
                                $fineinfo = get_fine_info($total_delay_days,$arr_head_value->rc_head_id,'RECURRING');
                            }
                            $head_pay_amt =  $arr_head_value->rc_inst_amount;
                            $head_pay_amt = $head_pay_amt - $head_paid_amt;
                            if($head_pay_amt != 0){
                                $headData['counter'] = $counter;
                                $headData['head_id'] = $arr_head_value->rc_inst_id;
                                $headData['head_name'] = $arr_head_value->rc_inst_particular_name;
                                $headData['head_amt'] = $arr_head_value->rc_inst_amount;
                                $headData['head_paid_amt'] = $head_paid_amt;
                                $headData['head_pay_amt'] = $head_pay_amt;
                                $headData['head_type'] = "RECURRING";
                                $headData['head_month_year'] = NULL;
                                $headData['head_date'] = $arr_head_value->rc_inst_date;
                                $headData['head_expire'] = $feesExpire1;
                                $headData['head_inst_id'] = $arr_head_value->rc_inst_id;
                                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                                $headData['concession_amt'] = $arr_head_value->concession_amt;
                                $headData['concession_status'] = $arr_head_value->concession_status;
                                $headData['class_id'] = $class_id;
                                $headData['student_id'] = $student_id;
                                $headData['total_delay_days'] = $total_delay_days;
                                $headData['fine_info'] = $fineinfo;
                                $headData['map_student_staff_id'] = NULL;
                                $headData['editable'] = 1;
                                $arr_heads[] = $headData;
                            }
                        }
                   
                }
            }
        }

        $checkForTransport   = MapStudentStaffVehicle::where(function($query) use ($student_id,$session) 
        {
            $query->where('student_staff_id', $student_id);
            $query->where('student_staff_type', 1);
            $query->where('student_staff_status', 1);
            $query->where('session_id', $session['session_id']);
        })
        ->with('get_location')
        ->get();

        if(!empty($checkForTransport[0])){
            
            $checkForTransport = isset($checkForTransport[0]) ? $checkForTransport[0] : [];
            $transportAssignDdate = date("Y-m-d", strtotime($checkForTransport->created_at));

            $kms = explode(" ", $checkForTransport['get_location']->location_km_distance);
            
            $start    = (new DateTime($transportAssignDdate));
            $end      = (new DateTime($session['end_date']));
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
            $headInfo = DB::select("select * from `transport_fees_head` where '".$kms[0]."' BETWEEN fees_head_km_from and fees_head_km_to
            ");
            $map_transport_receipt = ReceiptDetail::
            where('head_type', '=','TRANSPORT')
            ->where('session_id', '=',$session['session_id'])
            ->where('cancel_status', '=','0')
            ->where('map_student_staff_id', '=',$checkForTransport->map_student_staff_id)
            ->leftJoin('fee_receipt', function($join) use($session,$class_id,$student_id) {
                $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
                $join->where('session_id', '=',$session['session_id']);
                $join->where('class_id', '=',$class_id);
                $join->where('student_id', '=',$student_id);
                $join->where('head_type', '=','TRANSPORT');
                $join->where('receipt_status', '=',1);
            })
            ->orderBy('fee_receipt_detail_id', 'DESC')->get()->toArray();
            
            $map_student_staff_id = $checkForTransport->map_student_staff_id;
            
            foreach ($period as $dt) {
                if(!empty($map_transport_receipt)){
                    $head_paid_amt = $head_pay_amt = 0;
                    foreach ($map_transport_receipt as $receiptValues) {
                        if($map_student_staff_id == $receiptValues['map_student_staff_id'] && $dt->format("Y-m") == $receiptValues['head_month_year']){
                            if($receiptValues['inst_paid_amt'] != ""){
                                $amt = $receiptValues['inst_paid_amt'] + $receiptValues['r_concession_amt'];
                                $head_paid_amt += $receiptValues['inst_paid_amt'];
                            }
                        }
                        
                    }
                }
                $year_month = $dt->format("Y-m");
                if(!in_array($year_month, $map_transport_receipt[0])){
                    
                    $counter = COUNT($arr_heads);
                    $headData = [];
                    $feesExpire1 = 0;
                    $total_delay_days = 0;
                    $fineinfo = [];
                    // p($headInfo);
                    $head_pay_amt =  $headInfo[0]->fees_head_amount;
                    // p($headInfo[0]->fees_head_amount);
                    if($head_pay_amt != $head_paid_amt){
                        $head_pay_amt = $head_pay_amt - $head_paid_amt;
                    }
                    
                    if($head_pay_amt != 0){
                        $headData['counter'] = $counter;
                        $headData['head_id'] = $headInfo[0]->fees_head_id;
                        $headData['head_name'] = "Transport Fee - ".$dt->format("Y - F");
                        $headData['head_amt'] = $headInfo[0]->fees_head_amount;
                        $headData['head_paid_amt'] = $head_paid_amt;
                        $headData['head_pay_amt'] = $head_pay_amt;
                        $headData['head_type'] = "TRANSPORT";
                        $headData['head_month_year'] = $dt->format("Y-m");
                        $headData['head_date'] = $dt->format("Y-m-d");
                        $headData['head_expire'] = $feesExpire1;
                        $headData['head_inst_id'] = NULL;
                        $headData['concession_map_id'] = NULL;
                        $headData['concession_amt'] = NULL;
                        $headData['concession_status'] = NULL;
                        $headData['class_id'] = $class_id;
                        $headData['student_id'] = $student_id;
                        $headData['total_delay_days'] = $total_delay_days;
                        $headData['fine_info'] = $fineinfo;
                        $headData['map_student_staff_id'] = $map_student_staff_id;
                        $headData['editable'] = 1;
            
                        $arr_heads[] = $headData;
                    }
                }
            }
            
        }

        //  p($arr_heads);
        if($loginInfo['imprest_ac_status'] == 1) {
            $imprest_fees = DB::select("SELECT ac_entry_id,IMPST.class_id,IMPST.section_id,ac_entry_name,ac_entry_date,ac_entry_amt FROM `imprest_ac_entries` as IMPST
            WHERE `ac_entry_id` NOT IN (SELECT DISTINCT ac_entry_id FROM `ac_entry_map` where `session_id` = '".$session['session_id']."' AND `class_id` = '".$class_id."'  AND `student_id` = '".$student_id."' AND `ac_entry_map_amt_status` = 1)
            AND  `session_id` = '".$session['session_id']."'
            ");
            
            if(!empty($imprest_fees)){
                foreach($imprest_fees as $fees){
                    $fineinfo = [];
                    $counter = COUNT($arr_heads);
                    $headData['counter'] = $counter;
                    $headData['head_id'] = $fees->ac_entry_id;
                    $headData['head_name'] = $fees->ac_entry_name;
                    $headData['head_amt'] = $fees->ac_entry_amt;
                    $headData['head_paid_amt'] = 0;
                    $headData['head_pay_amt'] =  $fees->ac_entry_amt;
                    $headData['head_type'] = "IMPREST";
                    $headData['head_month_year'] = NULL;
                    $headData['head_date'] = $fees->ac_entry_date;
                    $headData['head_expire'] = 0;
                    $headData['head_inst_id'] = '';
                    $headData['concession_map_id'] = '';
                    $headData['concession_amt'] = '';
                    $headData['concession_status'] = '';
                    $headData['class_id'] = $class_id;
                    $headData['student_id'] = $student_id;
                    $headData['total_delay_days'] = '';
                    $headData['fine_info'] = $fineinfo;
                    $headData['map_student_staff_id'] = NULL;
                    $headData['editable'] = 0;
        
                    $arr_heads[] = $headData;
                }
            }
        }
        // p($arr_heads);
        return $arr_heads;
        
    } 
    /*  Get student total concession of student fee according to current session.
    *   @Shree 16 Oct 2018.
    */ 
    function get_student_total_concession($student_id,$class_id){
        
        $session_id    = null;
        $session       = get_current_session();
        if (!empty($session))
        {
            $session_id = $session['session_id'];
        }
        $concession_amt = DB::table('concession_map')->where('session_id', $session_id)->where('student_id', $student_id)->where('class_id', $class_id)->sum('concession_amt');
        return $concession_amt;
    }
    /*  Get student basic details with their sibling details
    *   @Shree 16 Oct 2018.
    */ 
    function get_student_basic_details_with_siblings($student_id){
        $session_id    = null;
        $student_parent_id    = null;
        $session       = get_current_session();
        $arr_bank = get_all_bank();
        if (!empty($session))
        {
            $session_id = $session['session_id'];
        }
        $arr_student = Student::where(function($query) use ($student_id)
            {
                if (!empty($student_id))
                {
                    $query->where('student_id', $student_id);
                }
            })->with('getParent')->with('getStudentAcademic')
            ->with('getStudentAcademic.getAdmitSession')
            ->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')
            ->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')
            ->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getStudentGroup')->with('getWalletAc')->get();
        
        foreach ($arr_student as $key => $student)
        {
            if (!empty($student['student_id']))
            {
                // if (!empty($student['student_image']))
                // {
                //     $profile = check_file_exist($student['student_image'], 'student_image');
                //     if (!empty($profile))
                //     {
                //         $student['profile'] = $profile;
                //     }
                // }
                $student['getWalletAc'] = isset($student['getWalletAc'][0]) ? $student['getWalletAc'][0] : [];
    
                $total_concession = 0;
                $total_concession = get_student_total_concession($student['student_id'],$student['getStudentAcademic']['current_class_id']);
                $complete_fees = [];
                $complete_fees = get_complete_fees_student($student['student_id'],$student['getStudentAcademic']['current_class_id']);
               
                $student_array[] = array(
                    'student_enroll_number'         => $student['student_enroll_number'],
                    'student_roll_no'               => $student['student_roll_no'],
                    'student_id'                    => $student['student_id'],
                    'student_name'                  => $student['student_name'],
                    'student_email'                 => $student['student_email'],
                    'student_image'                 => check_file_exist($student['student_image'], 'student_image'),
                    'student_father_name'           => $student['getParent']['student_father_name'],
                    'student_father_mobile_number'  => $student['getParent']['student_father_mobile_number'],
                    'student_unique_id'             => $student['getStudentAcademic']['student_unique_id'],
                    'current_session_id'            => $student['getStudentAcademic']['current_session_id'],
                    'current_class_id'              => $student['getStudentAcademic']['current_class_id'],
                    'current_section_id'            => $student['getStudentAcademic']['current_section_id'],
                    'current_session'               => $student['getStudentAcademic']['getCurrentSession']['session_name'],
                    'current_class'                 => $student['getStudentAcademic']['getCurrentClass']['class_name'],
                    'current_section'               => $student['getStudentAcademic']['getCurrentSection']['section_name'],
                    'wallet_ac_amt'                 => $student['getWalletAc']['wallet_ac_amt'],
                    'wallet_ac_id'                  => $student['getWalletAc']['wallet_ac_id'],
                    'total_concession'              => $total_concession,
                    'complete_fees'                 => $complete_fees,
                    'arr_bank'                      => $arr_bank
                );
                $student_parent_id = $student['getParent']['student_parent_id'];
            }
        }
        if($student_parent_id != null && $student_parent_id != "") {
            $siblings = Student::where(function($query) use ($student_id,$student_parent_id)
            {
                $query->where('student_parent_id','=', $student_parent_id);
                $query->where('student_id','!=', $student_id);
            })->with('getParent')->with('getStudentAcademic')->with('getStudentAcademic.getAdmitSession')->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getStudentGroup')->with('getWalletAc')->get();
            
            foreach ($siblings as $key => $sibling)
            {
                if (!empty($sibling['student_id']))
                {
                    if (!empty($sibling['student_image']))
                    {
                        $profile = check_file_exist($sibling['student_image'], 'student_image');
                        if (!empty($profile))
                        {
                            $sibling['profile'] = $profile;
                        }
                    }
                    $sibling['getWalletAc'] = isset($sibling['getWalletAc'][0]) ? $sibling['getWalletAc'][0] : [];
    
                    $total_concession = 0;
                    $total_concession = get_student_total_concession($sibling['student_id'],$sibling['getStudentAcademic']['current_class_id']);
    
                    $complete_fees = [];
                    $complete_fees = get_complete_fees_student($sibling['student_id'],$sibling['getStudentAcademic']['current_class_id']);
                    $student_array[] = array(
                        'student_enroll_number'         => $sibling['student_enroll_number'],
                        'student_roll_no'               => $sibling['student_roll_no'],
                        'student_id'                    => $sibling['student_id'],
                        'student_name'                  => $sibling['student_name'],
                        'student_email'                 => $sibling['student_email'],
                        'student_image'                 => $sibling['profile'],
                        'student_father_name'           => $sibling['getParent']['student_father_name'],
                        'student_father_mobile_number'  => $sibling['getParent']['student_father_mobile_number'],
                        'student_unique_id'             => $sibling['getStudentAcademic']['student_unique_id'],
                        'current_session_id'            => $sibling['getStudentAcademic']['current_session_id'],
                        'current_class_id'              => $sibling['getStudentAcademic']['current_class_id'],
                        'current_section_id'            => $sibling['getStudentAcademic']['current_section_id'],
                        'current_session'               => $sibling['getStudentAcademic']['getCurrentSession']['session_name'],
                        'current_class'                 => $sibling['getStudentAcademic']['getCurrentClass']['class_name'],
                        'current_section'               => $sibling['getStudentAcademic']['getCurrentSection']['section_name'],
                        'wallet_ac_amt'                 => $sibling['getWalletAc']['wallet_ac_amt'],
                        'wallet_ac_id'                  => $sibling['getWalletAc']['wallet_ac_id'],
                        'total_concession'              => $total_concession,
                        'complete_fees'                 => $complete_fees,
                        'arr_bank'                      => $arr_bank
                    );
                }
            }
        }
        return $student_array;
    } 

     /**
     * Get all reasons
     */
    function get_all_reasons($reason_id)
    {
        $arr_reason = [];
        $arr_reason_list = Reason::where(array('reason_status' => 1))->select('reason_text', 'reason_id')->orderBy('reason_text', 'ASC')->get();
        if (!empty($arr_reason_list))
        {
            foreach ($arr_reason_list as $arr_reasons)
            {
                $arr_reason[$arr_reasons['reason_id']] = $arr_reasons['reason_text'];
            }
        }
        $data      = view('admin-panel.reasons.ajax-reason-select',compact('arr_reason','reason_id'))->render();
        return $data;
    }

    // Get staff signle data
    function get_staff_signle_data($staff_id = null) {
        $gender                 = \Config::get('custom.staff_gender');
        $marital_status         = \Config::get('custom.staff_marital');
        $arr_teaching_status    = \Config::get('custom.teaching_status');
        $student       = [];
        $arr_staff = [];
        $session_id    = null;
        
        $arr_staff = staff::where(function($query) use ($staff_id)
        {
            if (!empty($staff_id))
            {
                $query->where('staff_id', $staff_id);
            }
        })->with('documents')->with('leaves')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')->with('getPermanentCity')->with('getPermanentState')->with('getPermanentCountry')->with('getTitle')->with('getDesignation')->with('getCaste')->with('getNationality')->with('getReligion')->with('getDesignation')
        ->with('getStaffAttendence')               
        ->get();
        // p($arr_staff);
        foreach ($arr_staff as $key => $staff)
        {
            if (!empty($staff['staff_id']))
            {
                if (!empty($staff['staff_profile_img']))
                {
                    $staff['profile'] = check_file_exist($staff['staff_profile_img'], 'staff_profile');
                }
                $staff_role_names = [];
                if($staff['staff_role_id'] != ''){
                    $staff_roles = explode(',', $staff['staff_role_id']);
                    foreach ($staff_roles as $roles) {
                        $staff_role_names[] = get_staff_role_name($roles);
                    }
                }
                
                $staff_shift_names = [];
                if($staff['shift_ids'] != ''){
                    $arr_shift = explode(',', $staff['shift_ids']);
                    foreach ($arr_shift as $shift_id) {
                        $staff_shift_names[] = get_shift_name($shift_id);
                    }
                }
                $document_list = [];
                foreach ($staff['documents'] as $documents)
                {
                    $document_data['staff_document_id']       = $documents['staff_document_id'];
                    $document_data['document_category_id']      = $documents['document_category_id'];
                    $document_data['document_category_name']    = $documents['getDocumentCategory']['document_category_name'];
                    $document_data['staff_document_details']    = $documents['staff_document_details'];
                    $document_data['staff_document_status']     = $documents['staff_document_status']; 
                    $document_data['document_submit_date']      = date("d F, Y ", strtotime($documents['created_at']->toDateString()));
                    $document_data['staff_document_file'] = '';
                    $document_data['staff_document_file'] = check_file_exist($documents['staff_document_file'], 'staff_document_file');
                    
                    $document_list[] = $document_data;
                }
                $leave_list = [];
                foreach ($staff['leaves'] as $leaves)
                {
                    $datetime1 = new DateTime($leaves['staff_leave_from_date']);
                    $datetime2 = new DateTime($leaves['staff_leave_to_date']);
                    $interval = $datetime1->diff($datetime2);
                    $days = $interval->format('%a');//now do whatever you like with $days
                    $leave_data['staff_leave_id']       = $leaves['staff_leave_id'];
                    $leave_data['staff_leave_reason']   = $leaves['staff_leave_reason'];
                    $leave_data['staff_leave_from_date']  = date("d F, Y ", strtotime($leaves['staff_leave_from_date']));
                    $leave_data['staff_leave_to_date']    = date("d F, Y ", strtotime($leaves['staff_leave_to_date']));
                    $leave_data['staff_leave_days']       = $days;
                    if($leaves['staff_leave_status'] == 0){
                        $status = 'Pending';
                    } if($leaves['staff_leave_status'] == 1){
                        $status = 'Approved';
                    } if($leaves['staff_leave_status'] == 2){
                        $status = 'Disapproved';
                    } 
                    $leave_data['leave_status']       = $status;
                    $leave_data['staff_leave_attachment'] = '';
                    $leave_data['staff_leave_attachment'] = check_file_exist($leaves['staff_leave_attachment'], 'staff_leave_attachment');
                    
                    $leave_list[] = $leave_data;
                }

                $attendence_list = [];
                foreach ($staff['getStaffAttendence'] as $getStaffAttendences)
                {

                    if($getStaffAttendences['staff_attendance_type'] == 1){
                        $getStaffAttendences['staff_attendance_type'] = "Present";
                        $attendence_class = "attendence_green";
                    } else if($getStaffAttendences['staff_attendance_type'] == '0' ){
                        $getStaffAttendences['staff_attendance_type'] = "Absent";
                        $attendence_class = "attendence_red";
                    } else if($getStaffAttendences['staff_attendance_type'] == ''){
                        $getStaffAttendences['staff_attendance_type'] = "Present";
                        $attendence_class = "attendence_green";
                    }

                    $attendence_data['staff_id']              = $getStaffAttendences['staff_id'];
                    $attendence_data['staff_attendance_id']   = $getStaffAttendences['staff_attendance_id'];
                    $attendence_data['staff_attendance_type'] = $getStaffAttendences['staff_attendance_type'];
                    $attendence_data['attendence_class']      = $attendence_class;
                    $attendence_data['staff_attendance_date'] = date("Y-m-d", strtotime($getStaffAttendences['staff_attendance_date']));

                    $attendence_list[] = $attendence_data;
                }

                $staff_array[] = array(
                    'staff_id'                  => $staff['staff_id'],
                    'reference_admin_id'        => $staff['reference_admin_id'],
                    'staff_name'                => $staff['staff_name'],
                    'staff_profile_img'         => $staff['profile'],
                    'staff_aadhar'              => $staff['staff_aadhar'],
                    'staff_UAN'                 => $staff['staff_UAN'],
                    'staff_ESIN'                => $staff['staff_ESIN'],
                    'staff_PAN'                 => $staff['staff_PAN'],
                    'designation_id'            => $staff['designation_id'],
                    'designation_name'          => $staff['getDesignation']['designation_name'],
                    'staff_role_id'             => $staff['staff_role_id'],
                    'staff_role'                => implode(',', $staff_role_names),
                    'staff_shifts'              => implode(',', $staff_shift_names),
                    'staff_email'               => $staff['staff_email'],
                    'staff_mobile_number'       => $staff['staff_mobile_number'],
                    'staff_dob'                 => date('d F Y',strtotime($staff['staff_dob'])),
                    'staff_gender'              => $staff['staff_gender'],
                    'gender'                    => $gender[$staff['staff_gender']],
                    'teaching_status'                    => $arr_teaching_status[$staff['teaching_status']],
                    'staff_father_name_husband_name'    => $staff['staff_father_name_husband_name'],
                    'staff_father_husband_mobile_no'    => $staff['staff_father_husband_mobile_no'],
                    'staff_mother_name'         => $staff['staff_mother_name'],
                    'staff_blood_group'         => $staff['staff_blood_group'],
                    'staff_marital_status'      => $staff['staff_marital_status'],
                    'marital'                   => $marital_status[$staff['staff_marital_status']],
                    'title_id'                  => $staff['title_id'],
                    'title_name'                => $staff['getTitle']['title_name'],
                    'caste_id'                  => $staff['caste_id'],
                    'caste_name'                => $staff['getCaste']['caste_name'],
                    'nationality_id'            => $staff['nationality_id'],
                    'nationality_name'          => $staff['getNationality']['nationality_name'],
                    'religion_id'               => $staff['religion_id'],
                    'religion_name'             => $staff['getReligion']['religion_name'],
                    'staff_attendance_unique_id'    => $staff['staff_attendance_unique_id'],
                    'staff_qualification'       => $staff['staff_qualification'],
                    'staff_specialization'      => $staff['staff_specialization'],
                    'staff_reference'           => $staff['staff_reference'],
                    'staff_experience'          => json_decode($staff['staff_experience']),
                    'staff_temporary_address'   => $staff['staff_temporary_address'],
                    'staff_temporary_city'      => $staff['staff_temporary_city'],
                    'staff_temporary_city_name' => $staff['getTemporaryCity']['city_name'],
                    'staff_temporary_state'     => $staff['staff_temporary_state'],
                    'staff_temporary_state_name'    => $staff['getTemporaryState']['state_name'],
                    'staff_temporary_county'        => $staff['staff_temporary_county'],
                    'staff_temporary_county_name'   => $staff['getTemporaryCountry']['country_name'],
                    'staff_temporary_pincode'       => $staff['staff_temporary_pincode'],
                    'staff_permanent_address'       => $staff['staff_permanent_address'],
                    'staff_permanent_pincode'       => $staff['staff_permanent_pincode'],
                    'staff_permanent_city'          => $staff['staff_permanent_city'],
                    'staff_permanent_city_name'     => $staff['getPermanentCity']['city_name'],
                    'staff_permanent_state'         => $staff['staff_permanent_state'],
                    'staff_permanent_state_name'    => $staff['getPermanentState']['state_name'],
                    'staff_permanent_county'        => $staff['staff_permanent_county'],
                    'staff_permanent_county_name'   => $staff['getPermanentCountry']['country_name'],
    
                    'documents'                     => $document_list,
                    'leaves'                        => $leave_list,
                    'attendance'                    => $attendence_list
                );
            }
        }
        return $staff_array;
    }

    /**
     * Get all exam classes 
     */
    function get_all_exam_class($decrypted_exam_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = $class_ids = [];
        $class_ids = ExamMap::where('exam_id','=',$decrypted_exam_id)->pluck('class_id')->all();
        
        $arr_classes_list = Classes::whereIn('class_id', $class_ids)->where(array('class_status' => 1))->select('class_name','medium_type', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_medium     = get_all_mediums();
                $class_name     = $arr_class['class_name']." - ".$arr_medium[$arr_class['medium_type']];
                $arr_classes[$arr_class['class_id']] = $class_name;
            }
        }
        return $arr_classes;
    }
    /**
     * Get all classes for exam 
     */
    function get_all_class_for_exam_map($decrypted_exam_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = $class_ids = [];
        $class_ids = ExamMap::where('exam_id','=',$decrypted_exam_id)->pluck('class_id')->all();
        
        $arr_classes_list = Classes::whereNotIn('class_id', $class_ids)->where(array('class_status' => 1))->select('class_name','medium_type', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_medium     = get_all_mediums();
                $class_name     = $arr_class['class_name']." - ".$arr_medium[$arr_class['medium_type']];
                $arr_classes[$arr_class['class_id']] = $class_name;
            }
        }
        return $arr_classes;
    }
    /**
     * Get all class sections with exam id and class id 
     */
    function get_all_class_sections($exam_id,$class_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_sections = $section_ids = [];
        $section_ids = ExamMap::where('exam_id','=',$exam_id)->where('class_id','=',$class_id)->pluck('section_id')->all();
        
        $arr_section_list = Section::whereNotIn('section_id', $section_ids)->where('class_id', '=', $class_id)->where(array('section_status' => 1))->select('section_name', 'section_id')->orderBy('section_name', 'ASC')->get();
        if (!empty($arr_section_list))
        {
            foreach ($arr_section_list as $arr_section)
            {
                $section_name     = $arr_section['section_name'];
                $arr_sections[$arr_section['section_id']] = $section_name;
            }
        }
        return $arr_sections;
    }

    /**
     * Get all section that are map with exams 
     */
    function get_all_sections_map_exams($exam_id,$class_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_sections = $section_ids = [];
        $section_ids = ExamMap::where('exam_id','=',$exam_id)->where('class_id','=',$class_id)->pluck('section_id')->all();
        
        $arr_section_list = Section::whereIn('section_id', $section_ids)->where(array('section_status' => 1))->select('section_name', 'section_id')->orderBy('section_name', 'ASC')->get();
        if (!empty($arr_section_list))
        {
            foreach ($arr_section_list as $arr_section)
            {
                $section_name     = $arr_section['section_name'];
                $arr_sections[$arr_section['section_id']] = $section_name;
            }
        }
        return $arr_sections;
    }

    function get_all_section_subjects($section_id){
        $arr_subjects = [];
        $subjects = SubjectSectionMapping::where('section_id','=', $section_id)->with('getSubjects')->get();
        
        if (!empty($subjects))
        {
            foreach ($subjects as $subject)
            {
                $arr_subjects[$subject['subject_id']] = $subject['getSubjects']['subject_name'];
            }
        }
        return $arr_subjects;
    }
    function get_all_marks_criteria($marks_criteria_id){
        $arr_criteria = [];
        $criteria = MarksCriteria::where('criteria_status', '=', 1)->select('marks_criteria_id','criteria_name')->get();
        if (!empty($criteria))
        {
            foreach ($criteria as $criteriaData)
            {
                $arr_criteria[$criteriaData['marks_criteria_id']] = $criteriaData['criteria_name'];
            }
        }
        $data      = view('admin-panel.marks-criteria.ajax-criteria-select',compact('arr_criteria','marks_criteria_id'))->render();
        return $data;
    }

    function get_all_grade_schemes($grade_scheme_id){
        $arr_schemes = [];
        $scheme = GradeScheme::where('scheme_status', '=', 1)->select('grade_scheme_id','scheme_name')->get();
        if (!empty($scheme))
        {
            foreach ($scheme as $schemeData)
            {
                $arr_schemes[$schemeData['grade_scheme_id']] = $schemeData['scheme_name'];
            }
        }
        $data      = view('admin-panel.grade-scheme.ajax-grade-select',compact('arr_schemes','grade_scheme_id'))->render();
        return $data;
    }

    function get_library_member_info($id){
        $member_id = get_decrypted_value($id, true);
        $memberInfo = LibraryMember::Find($member_id)->toArray();
       
        if($memberInfo['library_member_type'] == 1){
            $member = Student::where('student_id', '=', $memberInfo['member_id'])->get()->toArray();
        } else {
            $member = Staff::where('staff_id', '=', $memberInfo['member_id'])->get()->toArray();
        }
        
        $memberInfo['member'] = $member[0];
        return $memberInfo;
    }

    /**
     * Get all map subjects according to students
     * @shree 1 Nov 2018
     */
    function get_all_student_subjects()
    {
        $arr_subjects = [];
        $arr_subject_list = StudentSubjectManage::orderBy('subject_id', 'ASC')->groupBy('subject_id')->with('getSubject')->get()->toArray();
        if (!empty($arr_subject_list))
        {
            foreach ($arr_subject_list as $arr_subject)
            {
                $arr_subjects[$arr_subject['subject_id']] = $arr_subject['get_subject']['subject_name'].' - '.$arr_subject['get_subject']['subject_code'];
            }
        }
        return $arr_subjects;
    }

    // Get student list using class & section ID
    function get_student_list_class_section($class_id,$section_id)
    {
        // p($section_id);
        $arr_student_list = [];
        $session            = get_current_session();
        $arr_students        = Student::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('student_status', 1);
        })->join('student_academic_info', function($join) use ($class_id,$section_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })->select('students.student_id','students.student_name','students.student_enroll_number')->get();
        
        if (!empty($arr_students))
        {
            foreach ($arr_students as $arr_student)
            {
                $arr_student_list[$arr_student['student_id']] = $arr_student['student_name'].' - '.$arr_student['student_enroll_number'];
            }
        }
        // p($arr_student_list);
        return $arr_student_list;
        
    }

    /**
     * Get subject list using class ID & Section ID
     * @shree 6 Nov 2018
     */
    function get_subject_list_class_section($class_id,$section_id)
    {
        $session = get_current_session();
        $arr_subjects = [];
        $arr_subject_list = SubjectSectionMapping::where('class_id', '=', $class_id)->where('section_id', '=', $section_id)->where('session_id', '=', $session['session_id'])->orderBy('subject_id', 'ASC')->groupBy('subject_id')->with('getSubject')->get()->toArray();
        if (!empty($arr_subject_list))
        {
            foreach ($arr_subject_list as $arr_subject)
            {
                $arr_subjects[$arr_subject['subject_id']] = $arr_subject['get_subject']['subject_name'].' - '.$arr_subject['get_subject']['subject_code'];
            }
        }
        return $arr_subjects;
    }

    function get_sheduled_school_holidays($type)
    {
        // type 2=staff, type 1=student 
        $total_holidays = 0;
        $session_data   = get_current_session();
        $session_id     = $session_data['session_id'];
        $arr_holiday    = Holiday::where(function($query) use ($session_id)
            {
                if (!empty($session_id))
                {
                    $query->where('session_id', $session_id);
                }
            })->where('holiday_status', 1)
            ->orWhere('holiday_for', 0)
            ->where('holiday_for', $type)
            ->get();
        if (!empty($arr_holiday))
        {
            $holiday_list['holiday_details'] = [];
            foreach ($arr_holiday as $key => $holiday)
            {
                // calculate holidays period 
                $start_date   = $holiday['holiday_start_date'];
                $end_date     = $holiday['holiday_end_date'];
                $format       = "Y-m-d";
                $begin        = new DateTime($start_date);
                $end          = new DateTime($end_date);
                $end          = $end->modify('+1 day');
                $interval_day = new DateInterval('P1D');
                $date_range   = new DatePeriod($begin, $interval_day, $end);

                $holiday_period = [];
                foreach ($date_range as $key => $date)
                {
                    $holiday_period[] = $date->format($format);
                }
                $holiday_list['holiday_details'][] = array(
                    'holiday_id'           => $holiday['holiday_id'],
                    'holiday_name'         => $holiday['holiday_name'],
                    'holiday_start_date'   => $start_date,
                    'holiday_end_date'     => $end_date,
                    'holiday_period_count' => count($holiday_period),
                    'holiday_period'       => $holiday_period,
                );
            }
        }
        return $holiday_list;
    }

    // get all dates of a session
    function get_session_date_range($type = '')
    {
        $session    = get_current_session();
        $start_date = $session['start_date'];
        if ($type == 'holiday')
        {
            $end_date = $session['end_date'];
        }
        else
        {
            $end_date = date('Y-m-d') < $session['end_date'] ? date('Y-m-d') : $session['end_date'];
        }
        // session date range list
        $begin          = new DateTime($start_date);
        $end            = new DateTime($end_date);
        $end            = $end->modify('+1 day');
        $interval_day   = new DateInterval('P1D');
        $arr_date_range = new DatePeriod($begin, $interval_day, $end);
        return $arr_date_range;
    }

    function get_school_all_holidays($type)
    {
        // 2=staff, 1= student
        // variable iniliazation
        $sunday_list            = [];
        $arr_holiday_final_list = [];
        $sheduled_holiday_list  = [];

        // get sundays holiday list
        $arr_session_date = get_session_date_range('holiday');
        foreach ($arr_session_date as $key => $date)
        {
            if ($date->format('w') == 0)
            {
                $sunday_list[] = $date->format('Y-m-d');
            }
        }

        // sheduled holiday list 
        $sheduled_school_holidays = get_sheduled_school_holidays($type);
        $list                     = array_column($sheduled_school_holidays['holiday_details'], 'holiday_period');
        foreach ($list as $key => $arr_date)
        {
            foreach ($arr_date as $key => $date)
            {
                $sheduled_holiday_list[] = $date;
            }
        }

        // both sunday and sheduled holiday list
        $arr_holiday_list = array(
            'sunday' => $sunday_list,
            'other'  => $sheduled_holiday_list,
        );

        // merged sunday and sheduled holiday date
        $arr_holiday_combine_list = array_merge($arr_holiday_list['sunday'], $arr_holiday_list['other']);

        // sorting holiday list 
        asort($arr_holiday_combine_list);
        foreach ($arr_holiday_combine_list as $key => $value)
        {
            $arr_holiday_final_list[] = $value;
        }
        // return final holidays list of a session
        // remove duplicate dates
        $holiday_final_list = array_unique($arr_holiday_final_list);
        // p($holiday_final_list);
        return $holiday_final_list;
    }
    // Get student list using class & section ID
    function get_student_list_class_section_info($class_id,$section_id)
    {
        $arr_student_list = [];
        $session          = get_current_session();
        $arr_medium       = get_all_mediums();
        
        $arr_students        = Student::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('student_status',1);
        })->join('student_academic_info', function($join) use ($class_id,$section_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })->with('getParent')->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
        ->get();
        
        if (!empty($arr_students))
        {
            foreach ($arr_students as $student)
            {
                
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student->student_roll_no)){
                    $rollNo = $student->student_roll_no;
                } else {
                    $rollNo = "----";
                }
               
                $arr_student_list[] = array(
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student->student_name,
                    'student_status'        => $student->student_status,
                    'student_father_name'   => $student['getParent']->student_father_name,
                    'class_section'   => $student['getStudentAcademic']['getCurrentClass']->class_name.' - '.$student['getStudentAcademic']['getCurrentSection']->section_name,
                );
            }
        }
        return $arr_student_list;
        
    }

    // Get student attendance class-section wise
    function get_student_attendance($request)
    {
        //p($request->all());
        $arr_student_list = [];
        $session            = get_current_session();
        $arr_attendance        = StudentAttendance::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->get('student_attendance_id')) && $request->get('student_attendance_id') != null){
                $query->where('student_attendance_id', '=',$request->get('student_attendance_id'));
            }
            $query->where('session_id', '=',$session['session_id']);
            if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                $query->where('student_attendance_date', '=',$request->get('attendance_date'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $query->where('section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $query->where('class_id', '=',$request->get('class_id'));
            }
        })->first();
        return $arr_attendance;
    }

    /**
     * Get all Staff
     * @shree 13 Nov 2018
     */
    function get_all_staff_for_attendance($request)
    {
        $arr_staffData = [];
        $arr_staff_list = Staff::where(function($query) use ($request) 
            {
                $query->where('staff.staff_status',1);
            })
            ->leftJoin('staff_attend_details', function($join) use ($request){
                $join->on('staff_attend_details.staff_id', '=', 'staff.staff_id');
                if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                    $join->where('staff_attend_details.staff_attendance_date', '=',$request->get('attendance_date'));
                }
                if (!empty($request) && !empty($request->get('staff_attendance_id')) && $request->get('staff_attendance_id') != null){
                    $join->where('staff_attend_details.staff_attendance_id', '=',$request->get('staff_attendance_id'));
                }
                
            })
            
            ->select('staff.staff_name','staff.staff_profile_img','staff.staff_attendance_unique_id','staff_attend_details.*','staff.staff_id')->get();
        
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $arr_staff)
            {
                $staff_image = "";
                $staff_image = check_file_exist($arr_staff->staff_profile_img, 'staff_profile');
                if(!empty($arr_staff->staff_attendance_unique_id)){
                    $attendanceId = $arr_staff->staff_attendance_unique_id;
                } else {
                    $attendanceId = "----";
                }
            
                $arr_staffData[] = array(
                    'staff_id'                  => $arr_staff->staff_id,
                    'profile'                   => $staff_image,
                    'attendance_id'             => $attendanceId,
                    'staff_name'                => $arr_staff->staff_name,
                    'staff_attendance_unique_id'    => $arr_staff->staff_attendance_unique_id,
                    'staff_attend_d_id'         => $arr_staff->staff_attend_d_id,
                    'staff_attendance_id'       => $arr_staff->staff_attendance_id,
                    'session_id'                => $arr_staff->session_id,
                    'in_time'                   => $arr_staff->in_time,
                    'out_time'                  => $arr_staff->out_time,
                    'staff_attendance_type'     => $arr_staff->staff_attendance_type,
                    'staff_overtime_type'       => $arr_staff->staff_overtime_type,
                    'staff_overtime_value'      => $arr_staff->staff_overtime_value,
                    'staff_attendance_date'     => $arr_staff->staff_attendance_date,
                );
            }
        }
        return $arr_staffData;
    }

    /**
     * Get all Staff
     * @shree 13 Nov 2018
     */
    function get_all_student_for_attendance($request)
    {
        $session          = get_current_session();
        $arr_studentData = [];
        $arr_student_list = Student::where(function($query) use ($request) 
            {
                $query->where('student_status',1);
            })
            ->leftJoin('student_academic_info', function($join) use ($request,$session){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $join->where('student_academic_info.current_class_id', '=',$request->get('class_id'));
                }
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $join->where('student_academic_info.current_section_id', '=',$request->get('section_id'));
                }
                $join->where('student_academic_info.current_session_id', '=',$session['session_id']);
                
            })
            ->leftJoin('student_attend_details', function($join) use ($request,$session){
                $join->on('student_attend_details.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                    $join->where('student_attend_details.student_attendance_date', '=',$request->get('attendance_date'));
                }
                if (!empty($request) && !empty($request->get('student_attendance_id')) && $request->get('student_attendance_id') != null){
                    $join->where('student_attend_details.student_attendance_id', '=',$request->get('student_attendance_id'));
                }
                $join->where('student_attend_details.session_id', '=',$session['session_id']);
                
            })
            ->select('students.student_name','students.student_image','students.student_roll_no','students.student_enroll_number','student_attend_details.*','students.student_id')->get();
        
        if (!empty($arr_student_list))
        {
            foreach ($arr_student_list as $arr_student)
            {
                $student_image = "";
                $student_image = check_file_exist($arr_student->student_image, 'student_image');
            
                $arr_studentData[] = array(
                    'student_id'                => $arr_student->student_id,
                    'profile'                   => $student_image,
                    'student_name'              => $arr_student->student_name,
                    'student_enroll_number'     => $arr_student->student_enroll_number,
                    'student_attend_d_id'       => $arr_student->student_attend_d_id,
                    'student_attendance_id'     => $arr_student->student_attendance_id,
                    'student_attendance_type'   => $arr_student->student_attendance_type,
                    'student_attendance_date'   => $arr_student->student_attendance_date,
                );
            }
        }
        return $arr_studentData;
    }

    /*  Get All hostels 
    *   @Shree 14 Nov 2018.
    */ 
    
    function get_all_hostels()
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_hostel   = [];
        $arr_hostel_list   = Hostel::where(array('hostel_status' => 1))->select('hostel_name','hostel_id')->orderBy('hostel_name', 'ASC')->get();
        
        if (!empty($arr_hostel_list))
        {
            foreach ($arr_hostel_list as $arr_hostel_value)
            {
                $arr_hostel[$arr_hostel_value['hostel_id']] = $arr_hostel_value['hostel_name'];
            }
        }
        return $arr_hostel;
    }

     /*  Get All room No 
    *   @Shree 13 Dec 2018.
    */ 
    
    function get_all_room_no()
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_room_no   = [];
        $arr_room   = RoomNo::where(array('room_no_status' => 1))->select('room_no','room_no_id')->orderBy('room_no', 'ASC')->get();
        
        if (!empty($arr_room))
        {
            foreach ($arr_room as $arr_room_value)
            {
                $arr_room_no[$arr_room_value['room_no_id']] = $arr_room_value['room_no'];
            }
        }
        return $arr_room_no;
    }

    /*  Get All room category 
    *   @Shree 14 Nov 2018.
    */ 
    
    function get_all_room_categories()
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_room_cate   = [];
        $arr_room_cate_list   = HostelRoomCategory::where(array('h_room_cate_status' => 1))->select('h_room_cate_name','h_room_cate_id')->orderBy('h_room_cate_name', 'ASC')->get();
        
        if (!empty($arr_room_cate_list))
        {
            foreach ($arr_room_cate_list as $arr_room_cate_value)
            {
                $arr_room_cate[$arr_room_cate_value['h_room_cate_id']] = $arr_room_cate_value['h_room_cate_name'];
            }
        }
        return $arr_room_cate;
    }

    function get_block_by_hostel($hostel_id)
    {
        $loginInfo 			= get_loggedin_user_data();
        $block     = [];
        $arr_block = HostelBlock::where([['h_block_status', '=', 1],['hostel_id', '=', $hostel_id]])->select('h_block_id', 'h_block_name')->get();
        
        if (!empty($arr_block))
        {
            foreach ($arr_block as $value)
            {
                $block[$value['h_block_id']] = $value['h_block_name'];
            }
        }
        return $block;
    }

    function get_floor_by_block($h_block_id)
    {
        $loginInfo 			= get_loggedin_user_data();
        $floor     = [];
        $arr_floor = HostelFloor::where([['h_block_id', '=', $h_block_id]])->select('h_floor_id', 'h_floor_no')->get();
        
        if (!empty($arr_floor))
        {
            foreach ($arr_floor as $value)
            {
                $floor[$value['h_floor_id']] = $value['h_floor_no'];
            }
        }
        return $floor;
    }

    function get_room_by_floor($h_floor_id)
    {
        $loginInfo 			= get_loggedin_user_data();
        $room     = [];
        $arr_room = HostelRooms::where([['h_floor_id', '=', $h_floor_id]])->select('h_room_id', 'h_room_no')->get();
        
        if (!empty($arr_room))
        {
            foreach ($arr_room as $value)
            {
                $room[$value['h_room_id']] = $value['h_room_no'];
            }
        }
        return $room;
    }

    function get_room_info($h_room_id)
    {
        $loginInfo 			= get_loggedin_user_data();
        $roomInfo     = '';
        $roomInfo = HostelRooms::where([['h_room_id', '=', $h_room_id],['h_room_status', '=', '1']])->with('getHostel')->with('getBlock')->with('getFloor')->with('getRoomCate')->first();
        $arr_room = array(
            'h_room_id' => $roomInfo['h_room_id'],
            'hostel_id' => $roomInfo['hostel_id'],
            'h_block_id' => $roomInfo['h_block_id'],
            'h_floor_id' => $roomInfo['h_floor_id'],
            'h_room_cate_id' => $roomInfo['h_room_cate_id'],
            'h_room_no' => $roomInfo['h_room_no'],
            'h_room_alias' => $roomInfo['h_room_alias'],
            'h_room_capacity' => $roomInfo['h_room_capacity'],
            'h_room_status' => $roomInfo['h_room_status'],
            'hostel_block_floor' => $roomInfo['getHostel']->hostel_name.' - '.$roomInfo['getBlock']->h_block_name.' - '.$roomInfo['getFloor']->h_floor_no,
            'room_category' => $roomInfo['h_room_no'].'('.$roomInfo['getRoomCate']->h_room_cate_name.')',
        );
        return $arr_room;
    }
    function UpdateRoomAvailability($h_room_id,$h_room_available_capacity){
        $roomInfo = HostelRooms::FIND($h_room_id);
        $roomInfo->h_room_available_capacity = $h_room_available_capacity;
        $roomInfo->save();
        return true;
    }
    function get_room_occupant_count($h_room_id)
    {
        $loginInfo 			= get_loggedin_user_data();
        $count     = 0;
        $count = HostelStudentMap::where([['h_room_id', '=', $h_room_id],['leave_status', '=', 0]])->get()->count();
        return $count;
    }

    function get_staff_subjects($request,$staff_id)
    {
        $subjects     = [];
        $arr_subjects     = SubjectTeacherMapping::where(function($query) use ($request,$staff_id) 
        {
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $query->where('class_id', '=',$request->get('class_id'));
            }
            $query->where('staff_ids', '=',$staff_id);
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $query->where('section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
                $query->where('subject_id', '=',$request->get('subject_id'));
            }
            
        })->with('getStaff')->with('getClass')->with('getSection')->with('getSubject')->get();
        
        if (!empty($arr_subjects))
        {
            foreach ($arr_subjects as $arr_subject)
            {
                $staffName = '';
                if (isset($arr_subject['getStaff']->staff_name) && !empty($arr_subject['getStaff']->staff_name))
                {
                    $staffName = $arr_subject['getStaff']->staff_name;
                }
                $subjectName = '';
                if (isset($arr_subject['getSubject']->subject_name) && !empty($arr_subject['getSubject']->subject_name))
                {
                    $subjectName = $arr_subject['getSubject']->subject_name.' - '.$arr_subject['getSubject']->subject_code;
                }
                $classSection = '';
                if (isset($arr_subject['getClass']->class_name) && isset($arr_subject['getSection']->section_name))
                {
                    $classSection = $arr_subject['getClass']->class_name.' - '.$arr_subject['getSection']->section_name;
                }
            
                $subjects[] = array(
                    'staff_id'          => $arr_subject->staff_ids,
                    'class_id'          => $arr_subject->class_id,
                    'section_id'          => $arr_subject->section_id,
                    'subject_id'          => $arr_subject->subject_id,
                    'staff_name'         => $staffName,
                    'subject_name'       => $subjectName,
                    'class_section'      => $classSection,
                    'class_name'      => $arr_subject['getClass']->class_name,
                    'section_name'      => $arr_subject['getSection']->section_name,
                );
            }
        }
        return $subjects;
    }
    // Get staff for time table
    function get_subject_staff($request)
    {
        $staff     = [];
        $arr_staff     = SubjectTeacherMapping::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $query->where('class_id', '=',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
                $query->where('subject_id', '=',$request->get('subject_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $query->where('section_id', '=',$request->get('section_id'));
            }
            
        })->select('staff_ids')->get()->toArray();
        foreach($arr_staff as $staffData){
            $staff[] = $staffData['staff_ids'];
        }
        return $staff;
    }
    // Get staff for time table
    function get_teaching_staff()
    {
        $staff     = [];
        $arr_staff     = Staff::where(function($query) 
        {
            $query->where('teaching_status', '=',1);
            
        })->select('staff_id')->get()->toArray();
        foreach($arr_staff as $staffData){
            $staff[] = $staffData['staff_id'];
        }
        return $staff;
    }
    // Check availability for time table
    function check_staff_availability($request,$staff_id){
        $staff     = [];
        $session          = get_current_session();
        $arr_staff     = TimeTableMap::where(function($query) use ($request,$session,$staff_id) 
        {
            $query->where('session_id', '=',$session['session_id']);
            $query->where('staff_id', '=',$staff_id);
            $query->where('day', '=',$request->get('day'));
            $query->where('expire_status', '=',1);
        })->get()->toArray();
        return $arr_staff;
    }

    // Check availability for time table
    function check_staff_availability_for_examschedule($request,$staff_id){
        $staff     = [];
        $session       = get_current_session();
        $arr_staff     = ExamScheduleMap::where(function($query) use ($request,$session,$staff_id) 
        {
            $query->where('session_id', '=',$session['session_id']);
            $query->whereRaw('FIND_IN_SET('.$staff_id.',staff_ids)');
            $query->where('exam_date', '=',$request->get('exam_date'));
        })->get()->toArray();
        return $arr_staff;
    }

    // Get student list using class & section ID
    function get_student_list_by_subject($request,$student_id = NULL)
    {
        $arr_student_list = [];
        $session          = get_current_session();
        $subject_id = null;
        if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
            $subject_id = $request->get('subject_id');
        }
        $arr_students        = Student::where(function($query) use ($request,$student_id) 
        {
            if ($student_id != null){
                $query->where('students.student_id', '=',$student_id);
            }
            if (!empty($request) && !empty($request->get('roll_no')) && $request->get('roll_no') != null){
                $query->where('student_roll_no', '=',$request->get('roll_no'));
            }
            if (!empty($request) && !empty($request->get('enroll_no')) && $request->get('enroll_no') != null){
                $query->where('student_enroll_number', '=',$request->get('enroll_no'));
            }
            if (!empty($request) && !empty($request->get('student_name')) && $request->get('student_name') != null){
                $query->where('student_name', '=',$request->get('student_name'));
            }
            $query->where('student_status', 1);
        })->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->with('getParent')->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
        ->get();
        // p($arr_students);
        if (!empty($arr_students))
        {
            foreach ($arr_students as $student)
            {
                
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student->student_roll_no)){
                    $rollNo = $student->student_roll_no;
                } else {
                    $rollNo = "----";
                }
               
                $arr_student_list[] = array(
                    'class_id'              => $student->current_class_id,
                    'section_id'            => $student->current_section_id,
                    'subject_id'            => $subject_id,
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student->student_name,
                    'student_status'        => $student->student_status,
                    'student_father_name'   => $student['getParent']->student_father_name,
                    'class_section'   => $student['getStudentAcademic']['getCurrentClass']->class_name.' - '.$student['getStudentAcademic']['getCurrentSection']->section_name,
                );
            }
        }
        return $arr_student_list;
        
    }

    // Get excluded subject list using class & section ID
    function get_excluded_subject_list($request)
    {
        $arr_student_list = [];
        $session          = get_current_session();
        
        $arr_students        = StudentSubjectManage::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $query->where('section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('subject_id')) && $request->get('subject_id') != null){
                $query->where('subject_id', '=',$request->get('subject_id'));
            }

        })
        ->get();
        if (!empty($arr_students))
        {
            foreach ($arr_students as $student)
            {
                $arr_student_list[] = $student->student_id;
            }
        }
        return $arr_student_list;
        
    }
    // Get excluded subject list using class & section ID
    function get_excluded_subjects($class_id,$section_id,$student_id)
    {
        $arr_subject_list = [];
        $session          = get_current_session();
        
        $arr_subjects  = StudentSubjectManage::where(function($query) use ($class_id,$section_id,$student_id) 
        {
            if (!empty($class_id)){
                $query->where('class_id', '=',$class_id);
            }
            if (!empty($class_id)){
                $query->where('section_id', '=',$section_id);
            }
            if (!empty($student_id)){
                $query->where('student_id', '=',$student_id);
            }

        })
        ->get();
        if (!empty($arr_subjects))
        {
            foreach ($arr_subjects as $subject)
            {
                $arr_subject_list[] = $subject->subject_id;
            }
        }
        return $arr_subject_list;
        
    }

    /**
     * Get homework group id
     */
    function get_homework_group($class_id,$section_id)
    {
        $group = [];
        $group_info = HomeworkGroup::where(array('class_id' => $class_id,'section_id'=> $section_id))->select('homework_group_name', 'homework_group_id')->first();
        if(isset($group_info->homework_group_id)){
            $group =  $group_info;
        }
        return $group;
    }
    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
        
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        
        $string = array(
             'y' => 'year',
             'm' => 'month',
             'w' => 'week',
             'd' => 'day',
             'h' => 'hour',
             'i' => 'minute',
             's' => 'second',
         );
         foreach ($string as $k => &$v) {
             if ($diff->$k) {
                 $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
             } else {
                 unset($string[$k]);
             }
         }
        
         if (!$full) $string = array_slice($string, 0, 1);
         return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    /**
     * Get student list using class, section for hostel
     */
    function get_students_for_hostel($request,$type)
    {
        if($type == 0){
            $arr_student        = Student::where(function($query) use ($request,$type)
            {
                if ( $request->get('student_name') !='' ){
                    $query->where('students.student_name', "like", "%{$request->get('student_name')}%");
                }

            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $join->where('student_academic_info.current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $join->where('student_academic_info.current_class_id', '=',$request->get('class_id'));
                }
            })
            ->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
            ->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            })
            
            // ->leftJoin('hostel_student_map', function($join) use ($request){
            //     $join->on('hostel_student_map.student_id', '=', 'students.student_id');
            //     if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null){
            //         $join->where('hostel_student_map.hostel_id', '=',$request->get('hostel_id'));
            //     }
            // })
            // ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','hostel_student_map.h_student_map_id','hostel_student_map.hostel_id','hostel_student_map.h_block_id','hostel_student_map.h_floor_id','hostel_student_map.h_room_id','hostel_student_map.join_date','hostel_student_map.leave_date','hostel_student_map.fees_status','hostel_student_map.leave_status')
            ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number')
            ->orderBy('students.student_id', 'DESC')
            ->with(['getStudentHostelMap' => function($query) use($request) {
                $query->where('hostel_student_map.leave_status',0);
                $query->with('getHostel');
                $query->with('getBlock');
                $query->with('getFloor');
                $query->with('getRoom');
                $query->with('getRoomCate');
            }])
            // ->groupBy('student_id')
            ->get();
            
        } else if($type == 1){
            $arr_student        = Student::where(function($query) use ($request,$type) 
            {
                if ( $request->get('student_name') !='' ){
                    $query->where('students.student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
            ->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            })
            
            ->join('hostel_student_map', function($join) use ($request){
                $join->on('hostel_student_map.student_id', '=', 'students.student_id');
                $join->Where('h_student_map_id', '!=', NULL);
                if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null){
                    $join->where('hostel_id', '=',$request->get('hostel_id'));
                }
                if (!empty($request) && empty($request->get('status')) && $request->get('status') == null){
                    $join->Where('hostel_id', '=', NULL);
                } else {
                    $join->where('h_room_id', '!=',NULL);
                }
                $join->where('leave_status', '=',0);
                if (!empty($request) && !empty($request->get('h_room_id')) && $request->get('h_room_id') != null){
                    $join->where('h_room_id', '=',$request->get('h_room_id'));
                } 
            })
            ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','hostel_student_map.h_student_map_id','hostel_student_map.hostel_id','hostel_student_map.h_block_id','hostel_student_map.h_floor_id','hostel_student_map.h_room_cate_id','hostel_student_map.h_room_id','hostel_student_map.join_date','hostel_student_map.leave_date','hostel_student_map.fees_status','hostel_student_map.leave_status')
            ->orderBy('hostel_student_map.h_student_map_id', 'DESC')
            
            ->with(['getStudentHostelMap' => function($query) use($request) {
                $query->where('hostel_student_map.leave_status',$request->get('leave_status'));
                $query->with('getHostel');
                $query->with('getBlock');
                $query->with('getFloor');
                $query->with('getRoom');
                $query->with('getRoomCate');
            }])
            ->whereHas('getStudentHostelMap', function($query) use($request) {
                $query->where('hostel_student_map.leave_status',$request->get('leave_status'));
                $query->with('getHostel');
                $query->with('getBlock');
                $query->with('getFloor');
                $query->with('getRoom');
                $query->with('getRoomCate');
                if (!empty($request) && $request->get('leave_status')== 1 && !empty($request->get('start_date')) && !empty($request->get('end_date')))
                {
                    $query->whereBetween('leave_date', [$request->get('start_date'), $request->get('end_date')]);
                }
                if (!empty($request) && $request->get('leave_status')== 0 && !empty($request->get('start_date')) && !empty($request->get('end_date')))
                {
                    $query->whereBetween(DB::raw('CAST(created_at as date)'), [$request->get('start_date'), $request->get('end_date')]);
                }
                // $query->where([[DB::raw('CAST(created_at as date)'), '>=',$request->get('start_date')], [DB::raw('CAST(created_at as date)'), '<=', $request->get('end_date')]]);
                
            })
            ->groupBy('student_id')
            ->get();
            // p($arr_student);
        } else if($type == 2){
            $arr_student        = Student::where(function($query) use ($request,$type) 
            {
                if ( $request->get('student_name') !='' ){
                    $query->where('students.student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })
            ->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $join->where('student_academic_info.current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $join->where('student_academic_info.current_class_id', '=',$request->get('class_id'));
                }
            })
            ->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
            ->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            })
            
            ->join('hostel_student_map', function($join) use ($request){
                $join->on('hostel_student_map.student_id', '=', 'students.student_id');
                $join->Where('h_student_map_id', '!=', NULL);

                if (!empty($request) && !empty($request->get('hostel_id')) && $request->get('hostel_id') != null){
                    $join->where('hostel_id', '=',$request->get('hostel_id'));
                }

                if (!empty($request) && !empty($request->get('status')) && $request->get('status') != null){
                    $join->Where('hostel_id', '!=', NULL);
                }
                if (!empty($request) && !empty($request->get('leave_status')) && $request->get('leave_status') != null){
                    $join->where('leave_status', '=',1);
                } else {
                    $join->where('leave_status', '=',0);
                }
                if (!empty($request) && !empty($request->get('h_room_id')) && $request->get('h_room_id') != null){
                    $join->where('h_room_id', '=',$request->get('h_room_id'));
                } 

            })
            ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','hostel_student_map.h_student_map_id','hostel_student_map.hostel_id','hostel_student_map.h_block_id','hostel_student_map.h_floor_id','hostel_student_map.h_room_cate_id','hostel_student_map.h_room_id','hostel_student_map.join_date','hostel_student_map.leave_date','hostel_student_map.fees_status','hostel_student_map.leave_status')
            ->orderBy('hostel_student_map.h_student_map_id', 'DESC')
            ->with(['getStudentHostelMap' => function($query) use($request) {
                $query->where('hostel_student_map.leave_status',$request->get('leave_status'));
                $query->with('getHostel');
                $query->with('getBlock');
                $query->with('getFloor');
                $query->with('getRoom');
                $query->with('getRoomCate');
            }])
            ->whereHas('getStudentHostelMap', function($query) use($request) {
                $query->where('hostel_student_map.leave_status',$request->get('leave_status'));
                $query->with('getHostel');
                $query->with('getBlock');
                $query->with('getFloor');
                $query->with('getRoom');
                $query->with('getRoomCate');
                if (!empty($request) && $request->get('leave_status')== 1 && !empty($request->get('start_date')) && !empty($request->get('end_date')))
                {
                    $query->whereBetween('leave_date', [$request->get('start_date'), $request->get('end_date')]);
                }
                if (!empty($request) && $request->get('leave_status')== 0 && !empty($request->get('start_date')) && !empty($request->get('end_date')))
                {
                    $query->whereBetween(DB::raw('CAST(created_at as date)'), [$request->get('start_date'), $request->get('end_date')]);
                }
                // $query->where([[DB::raw('CAST(created_at as date)'), '>=',$request->get('start_date')], [DB::raw('CAST(created_at as date)'), '<=', $request->get('end_date')]]);
                
            })
            ->groupBy('student_id')
            ->get();
            // p($arr_student);
        }

        $students = [];
        
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                $student_image = '';
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                $leave_status = 0;
                if(isset($student->leave_status) && $student->leave_status == 1){
                    $leave_status = 1;
                }
                $join_status = 0;
                if(isset($student->leave_status) && $student->leave_status == 0){
                    $join_status = 1;
                }
                if(isset($student['getStudentHostelMap'])){
                    $join_status = 1;
                }
                // For class Name
                $class_name = '----';
                if(isset($student['getStudentAcademic']['getCurrentClass']->class_name) && $student['getStudentAcademic']['getCurrentClass']->class_name != ''){
                    $class_name = $student['getStudentAcademic']['getCurrentClass']->class_name;
                }
                // For Section Name
                $section_name = '----';
                if(isset($student['getStudentAcademic']['getCurrentSection']->section_name) && $student['getStudentAcademic']['getCurrentSection']->section_name != ''){
                    $section_name = $student['getStudentAcademic']['getCurrentSection']->section_name;
                }
                // For hostel Name
                $hostel_name = '----';
                if(isset($student['getStudentHostelMap']['getHostel']->hostel_name) && $student['getStudentHostelMap']['getHostel']->hostel_name != '' && $student->hostel_id != ''){
                    $hostel_name = $student['getStudentHostelMap']['getHostel']->hostel_name;
                }
                // For block Name
                $block_name = '----';
                if(isset($student['getStudentHostelMap']['getBlock']->h_block_name) && $student['getStudentHostelMap']['getBlock']->h_block_name != '' && $student->h_block_id != ''){
                    $block_name = $student['getStudentHostelMap']['getBlock']->h_block_name;
                }
                // For Floor No
                $floor_no = '----';
                if(isset($student['getStudentHostelMap']['getFloor']->h_floor_no) && $student['getStudentHostelMap']['getFloor']->h_floor_no != '' && $student->h_floor_id != ''){
                    $floor_no = $student['getStudentHostelMap']['getFloor']->h_floor_no;
                }
                // For Room Category
                $room_category = '----';
                if(isset($student['getStudentHostelMap']['getRoomCate']->h_room_cate_name) && $student['getStudentHostelMap']['getRoomCate']->h_room_cate_name != ''  && $student->h_room_cate_id != '' ) {
                    $room_category = $student['getStudentHostelMap']['getRoomCate']->h_room_cate_name;
                }
                // For Room No
                $room_no = '----';
                if(isset($student['getStudentHostelMap']['getRoom']->h_room_no) && $student['getStudentHostelMap']['getRoom']->h_room_no != ''  && $student->h_room_id != '' ) {
                    $room_no = $student['getStudentHostelMap']['getRoom']->h_room_no;
                }
                // For join date
                $join_date = '----';
                if($student->join_date != ''){
                    $join_date = date('d F Y', strtotime($student->join_date));
                }
                // For leave Date
                $leave_date = '----';
                if($student->leave_date != ''){
                    // $leave_date = date('d F Y', strtotime($student->leave_date));
                    $leave_date = date('d-m-Y', strtotime($student->leave_date));
                }
                $registeration_date = '----';
                if($student['getStudentHostelMap']['created_at'] != ''){
                    $registeration_date = date("d-m-Y", strtotime($student['getStudentHostelMap']['created_at']));
                }
                $students[] = array(
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_name'          => $student->student_name,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_status'        => $student->student_status,
                    'student_father_name'   => $student->student_father_name,
                    'class_section'         => $class_name.' - '.$section_name,
                    'leave_status'          => $leave_status,
                    'join_status'           => $join_status,
                    'hostel_block'          => $hostel_name.' - '.$block_name,
                    'room_category'         => $room_category,
                    'floor_room_no'         => $floor_no.' - '.$room_no,
                    'h_student_map_id'      => $student->h_student_map_id,
                    'hostel_id'             => $student->hostel_id,
                    'h_block_id'            => $student->h_block_id,
                    'h_floor_id'            => $student->h_floor_id,
                    'h_room_cate_id'        => $student->h_room_cate_id,
                    'h_room_id'             => $student->h_room_id,
                    'join_date'             => $join_date,
                    'leave_date'            => $leave_date,
                    'fees_status'           => $student->fees_status,
                    'registeration_date'    => $registeration_date,
                );
            }
        }
        return $students;
    }

    function get_selected_classes($class_ids)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = [];
        $arr_classes_list = Classes::where(array('class_status' => 1))->whereIn('class_id', explode(',',$class_ids))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_classes[] = $arr_class['class_name'];
            }
        }
        return implode(',',$arr_classes);
    }

    function get_selected_staff($staff_ids)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_staff = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereIn('staff_id', explode(',',$staff_ids))->select('staff_name',  'staff_id')->orderBy('staff_name', 'ASC')->get();
        
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $staff)
            {
                $arr_staff[] = $staff['staff_name'];
            }
        }
        return implode(',',$arr_staff);
    }

    /**
     * Get student list using class, section for hostel
     */
    function get_students_for_competition($request,$type)
    {
        if($type == 0){
            $arr_student        = Student::where(function($query) use ($request,$type) 
            {
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
            ->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            })
            
            ->leftJoin('competition_map', function($join) use ($request){
                $join->on('competition_map.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('competition_id')) && $request->get('competition_id') != null){
                    $join->where('competition_id', '=',$request->get('competition_id'));
                }
            })
            ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','competition_map.student_competition_map_id','competition_map.position_rank','competition_map.competition_id')
            ->orderBy('students.student_id', 'DESC')
            
            ->with('getCompetitionMap')->with('getCompetitionMap.getCompetition')
            ->get();
        } else if($type == 1){
            $arr_student        = Student::where(function($query) use ($request,$type) 
            {
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')
            ->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            })
            
            ->join('competition_map', function($join) use ($request){
                $join->on('competition_map.student_id', '=', 'students.student_id');
                $join->Where('student_competition_map_id', '!=', NULL);
                if (!empty($request) && !empty($request->get('competition_id')) && $request->get('competition_id') != null){
                    $join->where('competition_id', '=',$request->get('competition_id'));
                }
            })
            ->select('students.student_id','students.student_enroll_number', 'students.student_roll_no','students.student_name','students.student_image','student_academic_info.current_class_id','student_academic_info.current_section_id','student_parents.student_father_name','student_parents.student_father_mobile_number','competition_map.student_competition_map_id','competition_map.position_rank','competition_map.competition_id','competition_map.issue_certificate')
            ->orderBy('students.student_id', 'DESC')
            
            ->with('getCompetitionMap')->with('getCompetitionMap.getCompetition')
            ->get();
        } 
        // p($arr_student);
        $students = [];
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                
                $student_image = '';
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                // For class Name
                $class_name = '----';
                if(isset($student['getStudentAcademic']['getCurrentClass']->class_name) && $student['getStudentAcademic']['getCurrentClass']->class_name != ''){
                    $class_name = $student['getStudentAcademic']['getCurrentClass']->class_name;
                }
                // For Section Name
                $section_name = '----';
                if(isset($student['getStudentAcademic']['getCurrentSection']->section_name) && $student['getStudentAcademic']['getCurrentSection']->section_name != ''){
                    $section_name = $student['getStudentAcademic']['getCurrentSection']->section_name;
                }
                $students[] = array(
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_name'          => $student->student_name,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_status'        => $student->student_status,
                    'student_father_name'   => $student->student_father_name,
                    'class_section'         => $class_name.' - '.$section_name,
                    'student_competition_map_id'          => $student->student_competition_map_id,
                    'position_rank'          => $student->position_rank,
                    'competition_id'         => $student->competition_id,
                    'issue_certificate'      => $student->issue_certificate,
                   
                );
            }
        }
        return $students;
    }

   /**
     * Get subject list using class ID & Section ID for marks
     * @shree 6 Nov 2018
     */
    function get_subject_list_with_details($request)
    {
       
        $arr_subjects = [];
        $arr_subject_list = ExamMap::where(function($query) use ($request) 
        {
            if ( !empty($request) && $request->get('class_id') != null )
            {
                $query->where('exam_map.class_id', "=", $request->get('class_id'));
            }    
            if ( !empty($request) && $request->get('section_id') != null )
            {
                $query->where('exam_map.section_id', "=", $request->get('section_id'));
            }  
            if ( !empty($request) && $request->get('exam_id') != null )
            {
                $query->where('exam_map.exam_id', "=", $request->get('exam_id'));
            }  
            $query->where('marks_criteria_id', "!=", '');
        })
        ->join('schedule_map', function($join) use ($request){
            $join->on('schedule_map.subject_id', '=', 'exam_map.subject_id');
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('schedule_map.class_id', '=',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('schedule_map.section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('exam_id')) && $request->get('exam_id') != null){
                $join->where('schedule_map.exam_id', '=',$request->get('exam_id'));
            }
            $join->where('schedule_map.publish_status', 1);
        })
        ->orderBy('exam_map.subject_id', 'ASC')->groupBy('exam_map.subject_id')
        ->with('subjectInfo')->with('marksCriteriaInfo')->with('gradeSchemeInfo')
        ->get()->toArray();
        // p($arr_subject_list);
        foreach($arr_subject_list as $subject){
            $arr_subjects[] = array(
                'subject_id'    => $subject['subject_id'],
                'marks_criteria_id'    => $subject['marks_criteria_id'],
                'grade_scheme_id'    => $subject['grade_scheme_id'],
                'subject_name'    => $subject['subject_info']['subject_name'].' - '.$subject['subject_info']['subject_code'],
                'criteria_name'    => $subject['marks_criteria_info']['criteria_name'],
                'max_marks'    => $subject['marks_criteria_info']['max_marks'],
                'passing_marks'    => $subject['marks_criteria_info']['passing_marks'],
                'exam_date'    => date("d M Y", strtotime($subject['exam_date'])),
                'exam_time'    => date("h:i A", strtotime($subject['exam_time_from'])).' - '.date("h:i A", strtotime($subject['exam_time_to'])),
            );
        }
        // p($arr_subjects);
        return $arr_subjects;
    }

    // Get student list using section ID
    function get_student_list_for_marks($request,$all_subjects)
    {
        $session    = get_current_session();
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        //p($request->all());
        $arr_student_list = [];
        $session       = get_current_session();
        $arr_student        = Student::where(function($query) use ($request,$session) 
        {
            $query->where('student_status', 1);
        })
        ->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
            $join->where('current_session_id', $session['session_id']);
        })->with('getStudentAcademic')
        ->join('student_parents', function($join) use ($request){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            if (!empty($request) && !empty($request->get('father_name')))
            {
                $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
            }
        })->orderBy('students.student_id', 'DESC')
        ->with('getParent')
        ->with(['get_subject_with_marks' => function($query) use ($exam_id,$class_id,$section_id,$session){
            $query->where('session_id', "=", $session['session_id']);
            $query->where('class_id', "=", $class_id);
            $query->where('exam_id', "=", $exam_id);
            $query->where('section_id', "=", $section_id);
           
        }])
        ->get()->toArray();
        if (!empty($arr_student))
        {
            foreach ($arr_student as $student)
            {
               
                $student_image = '';
                if (!empty($student['student_image']))
                {
                    $profile = check_file_exist($student['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student['student_roll_no'])){
                    $rollNo = $student['student_roll_no'];
                } else {
                    $rollNo = "----";
                }
                $get_excluded_subjects = [];
                $student_subjects = [];
                $get_excluded_subjects = get_excluded_subjects($class_id,$section_id,$student['student_id']);
                
                foreach ($all_subjects as $subject) {
                    $subject['exclude'] = 0;
                    $subject['exam_mark_id'] = NULL;
                    $subject['marks'] = '';
                    $subject['attend_flag'] = 1;
                    $subject['marks_status'] = 0;
                    if(in_array($subject['subject_id'],$get_excluded_subjects)){
                        $subject['exclude'] = 1;
                    }
                    if(!empty($student['get_subject_with_marks'])) {
                        foreach ($student['get_subject_with_marks'] as $keyMarks => $marksData) {
                            if($subject['subject_id'] == $marksData['subject_id'] && $student['student_id'] == $marksData['student_id']){
                                $subject['exam_mark_id'] = $marksData['exam_mark_id'];
                                $subject['marks'] = $marksData['marks'];
                                $subject['attend_flag'] = $marksData['attend_flag'];
                                $subject['marks_status'] = $marksData['marks_status'];
                            }
                        }
                    }
                    $student_subjects[] = $subject;

                }

                $arr_student_list[] = array(
                    'student_id'            => $student['student_id'],
                    'class_id'            => $class_id,
                    'section_id'            => $section_id,
                    'exam_id'            => $exam_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student['student_enroll_number'],
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student['student_name'],
                    'student_status'        => $student['student_status'],
                    'student_subjects'      => $student_subjects
                );
            }
        }
        // p($arr_student_list);
        return $arr_student_list;
    }

    /**
     * Get all sections according to class id 
     */
    function get_all_sections($class_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_sections = [];
        $arr_section_list = Section::where('class_id', '=', $class_id)->where(array('section_status' => 1))->select('section_name', 'section_id')->orderBy('section_name', 'ASC')->get();
        if (!empty($arr_section_list))
        {
            foreach ($arr_section_list as $arr_section)
            {
                $section_name     = $arr_section['section_name'];
                $arr_sections[$arr_section['section_id']] = $section_name;
            }
        }
        return $arr_sections;
    }

    /** 
     *  Get all Units
    **/
    function get_all_units() 
    {
        $arr_units = [];
        $arr_units_list = Unit::where(array('unit_status' => 1))->get();

        if(!empty($arr_units_list))
        {
            foreach ($arr_units_list as $arr_unit_data)
                {
                    $arr_units[$arr_unit_data['unit_id']] = $arr_unit_data['unit_name'];
                }
        }
        return $arr_units;
    } 

     /**
      *   Get all Category with active status
     **/
    function get_all_category($type)
    {
        $arr_category = [];
        if($type == 0)
        {    
            $arr_category[0] = "Root";
        }    
        $arr_category_list = Category::where(array('category_status' => 1, 'category_level' => 0))->select('category_name', 'category_id')->orderBy('category_name', 'ASC')->get();
        if (!empty($arr_category_list))
        {
            foreach ($arr_category_list as $arr_categories)
            {
                $arr_category[$arr_categories['category_id']] = $arr_categories['category_name'];
            }
        }
        return $arr_category;
    }

    /**
     * Get all Categories
    **/
    function get_all_categories($type)
    {
        $arr_category = [];
        if($type == 0)
        {    
            $arr_category[0] = "Root";
        }    
        $arr_category_list = Category::where(array('category_level' => 0))->select('category_name', 'category_id')->orderBy('category_name', 'ASC')->get();
        if (!empty($arr_category_list))
        {
            foreach ($arr_category_list as $arr_categories)
            {
                $arr_category[$arr_categories['category_id']] = $arr_categories['category_name'];
            }
        }
        return $arr_category;
    }

    /**
     *   Get all Sub Categories according to thier Category
    **/
    function get_sub_categories($category_id)
    {
        $arr_sub_category      = []; 
        $arr_sub_category_list = Category::where('category_level','=',$category_id)->get();

        if(!empty($arr_sub_category_list))
        {
            foreach ($arr_sub_category_list as $arr_subcategory)
                {
                    $arr_sub_category[$arr_subcategory['category_id']] = $arr_subcategory['category_name'];
                }
        }
        return $arr_sub_category;
    }
    /**
     *  Get Send SMS with Parameters    
    **/   
    function send_sms_with_parameters($mobile_no, $message)
    {
        $send_sms = file_get_contents('http://bulksms.wscubetech.com/api/sendmsg.php?user=DEMACD&pass=demacd@123!&sender=DEMACD&phone='.htmlentities($mobile_no).'&text='.urlencode($message).'&priority=ndnd&stype=normal');

        return $send_sms;
    }

    /**
     *  Get Short Url 
    **/
    function shortenUrl($url) {
        $login = 'demov2r';
        $api_key = 'R_11adbb2305224dc6b3f1bc09fcb387de';
        $ch = curl_init('http://api.bitly.com/v3/shorten?login='.$login.'&apiKey='.$api_key.'&longUrl='.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $res = json_decode($result, true);
        return array(
            'short_url' => $res['data']['url'],
            'long_url' => $url
        );
    }

    /**
     *   Get all staff according to student  @sandeep 8 jan 2019
    **/
    function get_all_student_staffs($class_id,$section_id)
    {

        $staffs         = [];
        $arr_teacher    = [];
        $arr_staffs     = SubjectTeacherMapping::where(function($query) use ($request,$class_id,$section_id) {
            if (!empty($class_id != null)){
                $query->where('class_id', '=',$class_id);
            }
            if (!empty($section_id != null)){
                $query->where('section_id', '=',$section_id);
            }
        })->with('getStaff')->with('getClass')->with('getSection')->with('getSubject')->get();

        if (!empty($arr_staffs)) {

            foreach ($arr_staffs as $staff) {

                $staffName = '';
                if (isset($staff['getStaff']->staff_name) && !empty($staff['getStaff']->staff_name))
                {
                    $staffName = $staff['getStaff']->staff_name;
                }
                
                if($staff['getStaff']->staff_id != ""){
                    $arr_teacher[$staff['getStaff']->staff_id] = $staffName;
                }
            }
        }

        return $arr_teacher;
    }



    /**
     * Get Student attendence
     * @sandeep 9 Jan 2019
     */

    function get_attendance_for_student($request)
    {
        $session          = get_current_session();
        $loginInfo        = get_loggedin_user_data();
        
        $arr_studentData = [];
        $arr_student_list = StudentAttendance::
            leftJoin('student_attend_details', function($join) use ($request,$session){
                $join->on('student_attend_details.student_attendance_id', '=', 'student_attendance.student_attendance_id');
                if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                    $join->where('student_attend_details.student_attendance_date', '=',$request->get('attendance_date'));
                }
                
                $join->where('student_attend_details.session_id', '=',$session['session_id']);
                
            })
            ->where('student_attend_details.student_id', '=',$loginInfo['student_id'])
            ->get();
        
        if (!empty($arr_student_list))
        {
            foreach ($arr_student_list as $arr_student)
            {
                $student_image = "";
                $student_image = check_file_exist($arr_student->student_image, 'student_image');
            
                $arr_studentData[] = array(
                    'student_id'                => $arr_student->student_id,
                    'profile'                   => $student_image,
                    'student_name'              => $arr_student->student_name,
                    'student_enroll_number'     => $arr_student->student_enroll_number,
                    'student_attend_d_id'       => $arr_student->student_attend_d_id,
                    'student_attendance_id'     => $arr_student->student_attendance_id,
                    'student_attendance_type'   => $arr_student->student_attendance_type,
                    'student_attendance_date'   => $arr_student->student_attendance_date,
                );
            }
        }
        return $arr_studentData;
    }

    /**
     * Get all Imprest fees heads
     */
    function get_all_imprest_fee_heads()
    {
        $loginInfo = get_loggedin_user_data();
        $session  = get_current_session();
        $arr_fees_heads = [];
        $arr_fees_head_list = OneTime::where(function($query)
        {
            $query->where('deduct_imprest_status',1);
        })
        ->orderBy('ot_head_id', 'ASC')->get();
        if (!empty($arr_fees_head_list))
        {
            foreach ($arr_fees_head_list as $arr_fees_head)
            {
                $arr_fees_heads[$arr_fees_head['ot_head_id']] = $arr_fees_head['ot_particular_name'];
            }
        }
        return $arr_fees_heads;
    }

    /** 
     *  Get all Vendors
    **/
    function get_all_vendors() 
    {
        $arr_vendor = [];
        $arr_vendor_list = Vendor::where(array('vendor_status' => 1))->get();

        if(!empty($arr_vendor_list))
        {
            foreach ($arr_vendor_list as $arr_vendor_data)
                {
                    $arr_vendor[$arr_vendor_data['vendor_id']] = $arr_vendor_data['vendor_name'];
                }
        }
        return $arr_vendor;
    }

    /** 
     *  Get all Items with select category and sub-category
    **/
    function get_all_items($sub_category_id=false) {
        $arr_items = [];
        if($sub_category_id == ""){
            $arr_items_list = Items::where(array('item_status' => 1))->get();
        } else {
            $arr_items_list = Items::where(array('item_status' => 1, 'sub_category_id' => $sub_category_id))->get();
        }

        if(!empty($arr_items_list))
        {
            foreach ($arr_items_list as $arr_items_data)
                {
                    $arr_items[$arr_items_data['item_id']] = $arr_items_data['item_name'];
                }

        }
    //    p($arr_student_list);
        return $arr_items;
       
    }
    /**
     * Set Student Session 
     * @sandeep 21 Jan 2019
     */

    function set_student_session_data($student_id)
    {
        $student_data = [];
        $student_data = array(
            'student_id'   => $student_id,
        );
        LSession::put('set_student_data', $student_data);
        return LSession::get('set_student_data');
    }

    function get_current_student_session()
    {
        $set_student_session = LSession::get('set_student_data');
    //    print_r($set_student_session);
        $student_id = $set_student_session['student_id'];
        
        return $student_id;
    }


    /**
     * Get Single Parent Student 
     * @sandeep 23 Jan 2019
     */

    function get_student_info($parent_student_id)
    {
        //p($request->all());
        $arr_student_list = [];
        $arr_student        = Student::where(function($query) use ($parent_student_id) 
        {
            // For student type
            if($parent_student_id != null)
            {
                $query->where('student_parent_id', "=", $parent_student_id );
            }
        })
        ->get();
        //p($arr_student);
        foreach ($arr_student as $student)
        {
            $arr_student_list[] = array(
                'student_id'   => $student['student_id'],
            );
        }

    //    p($arr_student_list);
        return $arr_student_list;
    }


    /** 
     *  Get all Items according to item id
    **/
    function get_all_items_name($item_id) {
        $arr_items = [];
        $arr_items_list = Items::where(array('item_status' => 1, 'item_id' => $item_id))->get();

        if(!empty($arr_items_list))
        {
            foreach ($arr_items_list as $arr_items_data)
                {
                    $arr_items[$arr_items_data['item_id']] = $arr_items_data['item_name'];
                }
        }
        return $arr_items;
    }

    /**
     * Get Parent info 
     * @sandeep 21 Jan 2019
     */

    function parent_info($parent_id)
    {

        $parent_info = [];
        $arr_parent_info    = StudentParent::where(function($query) use ($parent_id) 
        {
            if (!empty($parent_id) && $parent_id != null)
            {
                $query->where('reference_admin_id', '=', $parent_id);
            }   
        })
        ->first();

        //p($arr_parent_info);

        if(!empty($arr_parent_info)){
            $parent_info = (object) $arr_parent_info;
        }
        
//        p($parent_info);
        return $parent_info;
    }


    /**
     * Get Parent Student 
     * @sandeep 21 Jan 2019
     */

    function get_parent_student($parent_student_id)
    {
        //p($request->all());
        $arr_student_list = [];
        $arr_student        = Student::where(function($query) use ($parent_student_id) 
        {
            // For student type
            if($parent_student_id != null)
            {
                $query->where('student_parent_id', "=", $parent_student_id );
            }
        })
        ->get();
        //p($arr_student);
        foreach ($arr_student as $student)
        {
            $arr_student_list[$student['student_id']] = $student['student_name'];
        }

    //    p($arr_student_list);
        return $arr_student_list;
    }
   


    function pushnotification($module_id=null,$notification_type=null,$staff_id=null,$parent_id=null,$student_id=null,$device_ids,$message,$title=null){

        if($title != ""){
            $title = $title;
        } else {
            $title = $message;
        }

        
        $data = json_encode(array (
            'title'=>$title,
            'body'=> $message,
            'module_id' => $module_id,
            'staff_id'  => $staff_id,
            'parent_id' => $parent_id,
            'student_id' => $student_id,
            'notification_type' => $notification_type,
        ),true);
        $res = PushNotification::setService('fcm')
        ->setMessage([
            'notification' => [
                'title'=>$title,
                'body'=> $message,
                'module_id' => $module_id,
                'staff_id'  => $staff_id,
                'parent_id' => $parent_id,
                'student_id' => $student_id,
                'notification_type' => $notification_type,
            ],
            'data' => [
                'data' => $data,
            ]
        ])
        ->setApiKey('AAAA_gx4-AI:APA91bHP3dZFFnnXVugvhGeowk7e2aFLlMF9BhUiaHCRmxD5D_oDm45NwPhG_ghsCdnrTSAhjXm52hUlD8Rgb597VRl3D63qWs-HLmL0MPJAKTe5anid_7syQ-GwVj7we7K_6qNLzctf')
        ->setDevicesToken($device_ids)
        ->send()
        ->getFeedback();
        return $res;
    }

    function get_selected_staff_json($staff_ids)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_staff = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereIn('staff_id', explode(',',$staff_ids))->select('staff_name',  'staff_id')->orderBy('staff_name', 'ASC')->get();
        
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $staff)
            {
                $arr_staff[] = array(
                    'staff_id' => $staff['staff_id'],
                    'staff_name' => $staff['staff_name'],
                );
            }
        }
        return $arr_staff;
    }

    function get_selected_classes_json($class_ids)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = [];
        $arr_classes_list = Classes::where(array('class_status' => 1))->whereIn('class_id', explode(',',$class_ids))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_classes[] = array(
                    'class_id'      => $arr_class['class_id'],
                    'class_name'    => $arr_class['class_name'],
                );
            }
        }
        return $arr_classes;
    }
      
    function pushnotification_by_topic($module_id=null,$notification_type=null,$staff_id=null,$parent_id=null,$student_id=null,$topic,$message,$class_id,$section_id,$topic_type=null,$title=null){
        $loginInfo  = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $session = get_current_session();

        if($title != ""){
            $title = $title;
        } else {
            $title = $message;
        }

        $data = json_encode(array (
            'title'=>$title,
            'body'=> $message,
            'module_id' => $module_id,
            'staff_id'  => $staff_id,
            'parent_id' => $parent_id,
            'student_id' => $student_id,
            'notification_type' => $notification_type,
            'class_id'  => "'".$class_id."'",
            'section_id'  => "'".$section_id."'",
        ),true);
        
        $res = PushNotification::setService('fcm')
        ->setMessage([
            'notification' => [
                'title'=>$title,
                'body'=> $message,
                'module_id' => $module_id,
                'staff_id'  => $staff_id,
                'parent_id' => $parent_id,
                'student_id' => $student_id,
                'notification_type' => $notification_type,
                'class_id'  => "'".$class_id."'",
                'section_id'  => "'".$section_id."'",
            ],
            'data' => [
                'data' => $data,
            ]
        ])
        ->setApiKey('AAAA_gx4-AI:APA91bHP3dZFFnnXVugvhGeowk7e2aFLlMF9BhUiaHCRmxD5D_oDm45NwPhG_ghsCdnrTSAhjXm52hUlD8Rgb597VRl3D63qWs-HLmL0MPJAKTe5anid_7syQ-GwVj7we7K_6qNLzctf')
        ->setConfig(['dry_run' => false])
        ->sendByTopic($topic)
        ->getFeedback();
        //p($res);
       if(!empty($res)){
           
            $notification = New Notification();
            $topic_via = 1;
            if($topic == 'active_staff'){
                $topic_via = 3;
            } else if($topic == 'active_student'){
                $topic_via = 2;
            } else if($topic == 'active_parent'){
                $topic_via = 2;
            } else {
                $notification->class_id = $class_id;
                $notification->section_id = $section_id;
            }
            
            $notification->admin_id = $admin_id;
            $notification->update_by = $admin_id;
            $notification->notification_via = $topic_via;
            $notification->topic_type = $topic_type;
            $notification->notification_type = $notification_type;
            $notification->session_id = $session['session_id'];
            $notification->module_id = $module_id;
            $notification->notification_text = $message;
            $notification->save();
       }
        return $res;
    }

    /**
     * Get all Vehicles
     * @Sandeep 1 feb 2019
     */
    function get_all_vehicles($vehicle_id = false)
    {
        $arr_vehicles = [];

        if($vehicle_id == ""){
            $arr_vehicle_list = Vehicle::where(array('vehicle_status' => 1))->select('vehicle_name', 'vehicle_id')->orderBy('vehicle_name', 'ASC')->get();
        } else {
            $arr_vehicle_list = Vehicle::where(array('vehicle_status' => 1,'vehicle_id' => $vehicle_id))->select('vehicle_name', 'vehicle_id')->orderBy('vehicle_name', 'ASC')->get();
        }
        
        if (!empty($arr_vehicle_list))
        {
            foreach ($arr_vehicle_list as $arr_vehicle)
            {
                $arr_vehicles[] = $arr_vehicle;
            }
        }
        return $arr_vehicles;
    }


    /**
     * Get all driver / conductor
     * @Sandeep 1 feb 2019
     */
    function get_all_driver_conductors($id)
    {
        $arr_drivers = [];
        $arr_driver_list = Staff::where(array('staff_status' => 1,'teaching_status' => 0))->select('staff_name', 'staff_id')->whereRaw('FIND_IN_SET('.$id.',staff_role_id)')->orderBy('staff_name', 'ASC')->get();
        if (!empty($arr_driver_list))
        {
            foreach ($arr_driver_list as $arr_driver)
            {
                $arr_drivers[$arr_driver['staff_id']] = $arr_driver['staff_name'];
            }
        }
        return $arr_drivers;
    }

    /**
     * Get all Routes
     * @Sandeep 1 feb 2019
     */
    function get_all_routes()
    {
        $arr_routes = [];
        $arr_route_list = Route::where(array('route_status' => 1))->select('route_name', 'route_id')->orderBy('route_name', 'ASC')->get();
        if (!empty($arr_route_list))
        {
            foreach ($arr_route_list as $arr_route)
            {
                $arr_routes[$arr_route['route_id']] = $arr_route['route_name'];
            }
        }
        return $arr_routes;
    }

    function save_notification($info){
        $notification = New Notification();
        $notification->admin_id = $info['admin_id'];
        $notification->update_by = $info['update_by'];
        $notification->notification_via = $info['notification_via'];
        $notification->notification_type = $info['notification_type'];
        $notification->session_id = $info['session_id'];
        $notification->class_id = $info['class_id'];
        $notification->section_id = $info['section_id'];
        $notification->module_id = $info['module_id'];
        $notification->student_admin_id = $info['student_admin_id'];
        $notification->parent_admin_id = $info['parent_admin_id'];
        $notification->staff_admin_id = $info['staff_admin_id'];
        $notification->school_admin_id = $info['school_admin_id'];
        $notification->notification_text = $info['notification_text'];
        $notification->save();
    }

    /**
     * Get all Jobs
     * @Sandeep 12 feb 2019
     */
    function get_all_jobs()
    {
        $arr_jobs = [];
        $arr_job_list = Job::where(array('job_status' => 1))->select('job_name', 'job_id')->orderBy('job_name', 'ASC')->get();
        if (!empty($arr_job_list))
        {
            foreach ($arr_job_list as $arr_job)
            {
                $arr_jobs[$arr_job['job_id']] = $arr_job['job_name'];
            }
        }
        return $arr_jobs;
    }

    function get_selected_reasons($reason_id)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_reason = [];
        $arr_reason_list = Reason::where(array('reason_id' => $reason_id))->select('reason_text',  'reason_id')->orderBy('reason_id', 'ASC')->first();
        
        if (!empty($arr_reason_list))
        {
            return $arr_reason_list['reason_text'];
        }
    }

    /** 
     *  Get all Arrears
    **/
    function get_all_arrear() 
    {
        $arr_arrear = [];
        $arr_arrear_list = Arrear::get();

        if(!empty($arr_arrear_list))
        {
            foreach ($arr_arrear_list as $arr_arrear_data)
                {
                    $arr_arrear[$arr_arrear_data['pay_arrear_id']] = $arr_arrear_data['arrear_name'];
                }
        }
        return $arr_arrear;
    } 

    // Get candidate signle data @Sandeep 16 feb 2019
    function get_candidate_signle_data($applied_cadidate_id = null) {
        $gender                 = \Config::get('custom.staff_gender');
        $marital_status         = \Config::get('custom.staff_marital');
        $arr_teaching_status    = \Config::get('custom.teaching_status');
        $student       = [];
        $arr_staff = [];
        $session_id    = null;
        
        $arr_staff = RecruitmentForm::where(function($query) use ($applied_cadidate_id)
        {
            if (!empty($applied_cadidate_id))
            {
                $query->where('applied_cadidate_id', $applied_cadidate_id);
            }
        })->with('documents')->with('leaves')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')
        ->with('getPermanentCity')->with('getPermanentState')->with('getPermanentCountry')
        ->with('getTitle')->with('getDesignation')->with('getCaste')->with('getNationality')->with('getReligion')
        ->with('getDesignation')->with('GetJob')->with('GetJob.get_medium')->get();

        //p($arr_staff[0]['GetJob']);
        foreach ($arr_staff as $key => $staff)
        {
            if (!empty($staff['applied_cadidate_id']))
            {
                
                $staff_array[] = array(
                    'applied_cadidate_id'       => check_string_empty($staff['applied_cadidate_id']),
                    'staff_name'                => check_string_empty($staff['staff_name']),
                    'staff_profile_img'         => check_string_empty($staff['staff_profile_img']),
                    'staff_aadhar'              => check_string_empty($staff['staff_aadhar']),
                    'staff_UAN'                 => check_string_empty($staff['staff_UAN']),
                    'staff_ESIN'                => check_string_empty($staff['staff_ESIN']),
                    'staff_PAN'                 => check_string_empty($staff['staff_PAN']),
                    'designation_id'            => check_string_empty($staff['designation_id']),
                    'designation_name'          => check_string_empty($staff['getDesignation']['designation_name']),
                    'staff_role_id'             => check_string_empty($staff['staff_role_id']),
                    'staff_email'               => check_string_empty($staff['staff_email']),
                    'staff_mobile_number'       => check_string_empty($staff['staff_mobile_number']),
                    'staff_dob'                 => date('d F Y',strtotime($staff['staff_dob'])),
                    'staff_gender'              => check_string_empty($staff['staff_gender']),
                    'gender'                    => check_string_empty($gender[$staff['staff_gender']]),
                    'teaching_status'                    => check_string_empty($arr_teaching_status[$staff['teaching_status']]),
                    'staff_father_name_husband_name'    => check_string_empty($staff['staff_father_name_husband_name']),
                    'staff_father_husband_mobile_no'    => check_string_empty($staff['staff_father_husband_mobile_no']),
                    'staff_mother_name'         => check_string_empty($staff['staff_mother_name']),
                    'staff_blood_group'         => check_string_empty($staff['staff_blood_group']),
                    'staff_marital_status'      => check_string_empty($staff['staff_marital_status']),
                    'marital'                   => check_string_empty($marital_status[$staff['staff_marital_status']]),
                    'title_id'                  => check_string_empty($staff['title_id']),
                    'title_name'                => check_string_empty($staff['getTitle']['title_name']),
                    'caste_id'                  => check_string_empty($staff['caste_id']),
                    'caste_name'                => check_string_empty($staff['getCaste']['caste_name']),
                    'nationality_id'            => check_string_empty($staff['nationality_id']),
                    'nationality_name'          => check_string_empty($staff['getNationality']['nationality_name']),
                    'religion_id'               => check_string_empty($staff['religion_id']),
                    'religion_name'             => check_string_empty($staff['getReligion']['religion_name']),
                    'staff_attendance_unique_id'    => check_string_empty($staff['staff_attendance_unique_id']),
                    'staff_qualification'       => check_string_empty($staff['staff_qualification']),
                    'staff_specialization'      => check_string_empty($staff['staff_specialization']),
                    'staff_reference'           => check_string_empty($staff['staff_reference']),
                    'staff_experience'          => check_string_empty($staff['staff_experience']),
                    'staff_temporary_address'   => check_string_empty($staff['staff_temporary_address']),
                    'staff_temporary_city'      => check_string_empty($staff['staff_temporary_city']),
                    'staff_temporary_city_name' => check_string_empty($staff['getTemporaryCity']['city_name']),
                    'staff_temporary_state'     => check_string_empty($staff['staff_temporary_state']),
                    'staff_temporary_state_name'    => check_string_empty($staff['getTemporaryState']['state_name']),
                    'staff_temporary_county'        => check_string_empty($staff['staff_temporary_county']),
                    'staff_temporary_county_name'   => check_string_empty($staff['getTemporaryCountry']['country_name']),
                    'staff_temporary_pincode'       => check_string_empty($staff['staff_temporary_pincode']),
                    'staff_permanent_address'       => check_string_empty($staff['staff_permanent_address']),
                    'staff_permanent_pincode'       => check_string_empty($staff['staff_permanent_pincode']),
                    'staff_permanent_city'          => check_string_empty($staff['staff_permanent_city']),
                    'staff_permanent_city_name'     => check_string_empty($staff['getPermanentCity']['city_name']),
                    'staff_permanent_state'         => check_string_empty($staff['staff_permanent_state']),
                    'staff_permanent_state_name'    => check_string_empty($staff['getPermanentState']['state_name']),
                    'staff_permanent_county'        => check_string_empty($staff['staff_permanent_county']),
                    'staff_permanent_county_name'   => check_string_empty($staff['getPermanentCountry']['country_name']),
                    'form_number'   => check_string_empty($staff['form_number']),
                    'job_name'   => check_string_empty($staff['GetJob']['job_name']),
                    'staff_resume'   => check_string_empty($staff['staff_resume']),

                );
            }
        }
        return $staff_array;
    }


    function get_total_class_students($class_id){
        $session       = get_current_session();
        $students = StudentAcademic::where('current_class_id',$class_id)
        ->where('current_session_id',$session['session_id'])
        ->select('student_id')->orderBy('student_id', 'ASC')->get()->count();
        return $students;
    }

    /**
     * Get all shift 
     */
    function get_all_vehicle_list()
    {
        $arr_vehicles = [];
        $arr_vehicle_list = Vehicle::where(array('vehicle_status' => 1))->select('vehicle_name', 'vehicle_id')->orderBy('vehicle_name', 'ASC')->get();
        if (!empty($arr_vehicle_list))
        {
            foreach ($arr_vehicle_list as $arr_vehicle)
            {
                $arr_vehicles[$arr_vehicle['vehicle_id']] = $arr_vehicle['vehicle_name'];
            }
        }
        return $arr_vehicles;
    }

    /** 
     *  Get all Salary Structure
    **/
    function get_all_salary_structure($salary_structure_id) 
    {
        $arr_sal_structure = [];
        $arr_sal_structure_list = SalaryStructure::get();

        if(!empty($arr_sal_structure_list))
        {
            foreach ($arr_sal_structure_list as $arr_sal_structure_data)
                {
                    $arr_sal_structure[$arr_sal_structure_data['salary_structure_id']] = $arr_sal_structure_data['sal_structure_name'];
                }
        }
        $data      = view('admin-panel.payroll-salary-structure.ajax-sal-struct-select',compact('arr_sal_structure','salary_structure_id'))->render();
        return $data;
    } 

    /**
     * Get Exam name
     */
    function get_exam_name($id)
    {
        $examName = '';
        $arr_exam_data = Exam::where([['exam_id', '=', $id]])->select('exam_id', 'exam_name')->first()->toArray();
        $examName = $arr_exam_data['exam_name'];
        return $examName;
    }

    function checkFeesPaidStatus($session_id,$class_id,$student_id,$head_id,$head_type){
        
        $receiptInfo = ReceiptDetail::where(function($query) use ($session_id,$class_id,$student_id,$head_id,$head_type) 
        {
            $query->where('head_type', $head_type );
            $query->where('session_id', $session_id );
            $query->where('receipt_status', 1 );
            if($head_type == 'ONETIME'){
                $query->where('head_id', '=',$head_id);
            } else {
                $query->where('head_inst_id', '=',$head_id);
            }
        })
        ->leftJoin('fee_receipt', function($join) use($session_id,$class_id,$student_id,$head_id,$head_type) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session_id);
            $join->where('class_id', '=',$class_id);
            $join->where('student_id', '=',$student_id);
            $join->where('head_type', '=',$head_type);
            
            $join->where('receipt_status', '=',1);
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get();
        return COUNT($receiptInfo);
    }
    /** 
     *  Get all Books
    **/
    function get_all_books() 
    {
        $arr_books = [];
        $arr_books_list = Book::where(array('book_status' => 1))->get();

        if(!empty($arr_books_list))
        {
            foreach ($arr_books_list as $arr_books_data)
                {
                    $arr_books[$arr_books_data['book_id']] = $arr_books_data['book_name'];
                }
        }
        return $arr_books;
    } 
    
    function get_total_complete_fees($class_id){
        $session = get_current_session();
        $arr_heads   = [];
        $currentDate = date('Y-m-d');
        
        $arr_onetime_head_list   = OneTime::whereRaw('FIND_IN_SET('.$class_id.',ot_classes)')
        ->where(array('ot_status' => 1,'deduct_imprest_status' => 0))
        ->leftJoin('concession_map', function($joinDD) use($session,$class_id) {
            $joinDD->on('concession_map.head_id', '=', 'one_time_heads.ot_head_id');
            $joinDD->where('concession_map.session_id', '=',$session['session_id']);
            $joinDD->where('concession_map.class_id', '=',$class_id);
            $joinDD->where('concession_map.head_type', '=','ONETIME');
        })
        ->orderBy('ot_head_id', 'ASC')->get();
        // p($arr_onetime_head_list);
        if (!empty($arr_onetime_head_list))
        {
            foreach ($arr_onetime_head_list as $arr_head_value)
            {
                $checkFeesPaid = checkFeesPaidStatusD($session['session_id'],$class_id,$arr_head_value->ot_head_id,'ONETIME');
                $counter = COUNT($arr_heads);
                $headData = [];
                $feesExpire = 0;
                if($arr_head_value->concession_status == 1){
                    $feesExpire = 1;
                }
                if($checkFeesPaid > 0){
                    $feesExpire = 1;
                }
                $headData['counter'] = $counter;
                $headData['head_id'] = $arr_head_value->ot_head_id;
                $headData['head_name'] = $arr_head_value->ot_particular_name;
                $headData['head_amt'] = $arr_head_value->ot_amount;
                $headData['head_type'] = "ONETIME";
                $headData['head_date'] = $arr_head_value->ot_date;
                $headData['head_expire'] = $feesExpire;
                $headData['head_inst_id'] = null;
                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                $headData['concession_amt'] = $arr_head_value->concession_amt;
                $headData['concession_status'] = $arr_head_value->concession_status;
                $headData['reason_id'] = $arr_head_value->reason_id;
                $headData['class_id'] = $class_id;
                
                $arr_heads[] = $headData;
            }
        }
        $arr_recurring_head_list   = RecurringHead::whereRaw('FIND_IN_SET('.$class_id.',rc_classes)')->where(array('rc_status' => 1))
        ->join('recurring_inst', function($join) {
            $join->on('recurring_inst.rc_head_id', '=', 'recurring_heads.rc_head_id');
        })
        ->leftJoin('concession_map', function($join) use($session,$class_id) {
            $join->on('concession_map.head_id', '=', 'recurring_inst.rc_inst_id');
            $join->where('session_id', '=',$session['session_id']);
            $join->where('head_type', '=','RECURRING');
            $join->where('class_id', '=',$class_id);
        })
        ->orderBy('rc_particular_name', 'ASC')->get();
        
        if (!empty($arr_recurring_head_list))
        {
            foreach ($arr_recurring_head_list as $arr_head_value)
            {
                $checkFeesPaid = checkFeesPaidStatusD($session['session_id'],$class_id,$arr_head_value->rc_inst_id,'RECURRING');
                $counter = COUNT($arr_heads);
                $headData = [];
                $feesExpire1 = 0;
                if($arr_head_value->concession_status == 1){
                    $feesExpire1 = 1;
                }
                if($checkFeesPaid > 0){
                    $feesExpire1 = 1;
                }
                $headData['counter'] = $counter;
                $headData['head_id'] = $arr_head_value->rc_inst_id;
                $headData['head_name'] = $arr_head_value->rc_inst_particular_name;
                $headData['head_amt'] = $arr_head_value->rc_inst_amount;
                $headData['head_type'] = "RECURRING";
                $headData['head_date'] = $arr_head_value->rc_inst_date;
                $headData['head_expire'] = $feesExpire1;
                $headData['head_inst_id'] = $arr_head_value->rc_inst_id;
                $headData['concession_map_id'] = $arr_head_value->concession_map_id;
                $headData['concession_amt'] = $arr_head_value->concession_amt;
                $headData['concession_status'] = $arr_head_value->concession_status;
                $headData['reason_id'] = $arr_head_value->reason_id;
                $headData['class_id'] = $class_id;
               $arr_heads[] = $headData;
            }
        }
        // p($arr_heads);
        return $arr_heads;
        
    } 

    function checkFeesPaidStatusD($session_id,$class_id,$head_id,$head_type){
        
        $receiptInfo = ReceiptDetail::where(function($query) use ($session_id,$class_id,$head_id,$head_type) 
        {
            $query->where('head_type', $head_type );
            $query->where('session_id', $session_id );
            $query->where('receipt_status', 1 );
            if($head_type == 'ONETIME'){
                $query->where('head_id', '=',$head_id);
            } else {
                $query->where('head_inst_id', '=',$head_id);
            }
        })
        ->leftJoin('fee_receipt', function($join) use($session_id,$class_id,$head_id,$head_type) {
            $join->on('fee_receipt.receipt_id', '=', 'fee_receipt_details.receipt_id');
            $join->where('session_id', '=',$session_id);
            $join->where('class_id', '=',$class_id);
            $join->where('head_type', '=',$head_type);
            
            $join->where('receipt_status', '=',1);
        })
        ->orderBy('fee_receipt_detail_id', 'DESC')->get();
        return COUNT($receiptInfo);
    }

    /**
     * Get all subject here @Ashish 21/8/2018 
     */
    function get_all_master_modules()
    {
        $loginInfo              = get_loggedin_user_data();
        $modules = [];
        $module_list    = MasterModule::select('module_name', 'mandatory','master_module_id')->orderBy('master_module_id', 'ASC')->get();
       
        return $module_list;
    }

    function get_permissions(){
        $permissions = '';
        $permissionId = 1;
        $permissionData = Permissions::find($permissionId);
        if(!empty($permissionData)){
            $permissions = explode(',',$permissionData->permissions);
        }
        return $permissions;
    }

    /**
     * Get all Accounts Heads
    **/
    function get_all_account_heads()
    {
        $arr_acc_head = [];  
        $arr_head_list = AccMainHead::with('getSubHeads')->get();
        if (!empty($arr_head_list))
        {
            foreach ($arr_head_list as $arr_acc_heads)
            {
                $arr_sub_head = [];
                foreach ($arr_acc_heads->getSubHeads as $subHeads)
                {
                    $arr_sub_head[$subHeads->acc_sub_head_id] = $subHeads->acc_sub_head;
                }
                $arr_acc_head[$arr_acc_heads->acc_main_head] = $arr_sub_head;
            }
        }
        return $arr_acc_head;
    }

    /**
     *   Get all Sub Heads according to thier Main Head
    **/
    function get_acc_groups($acc_sub_head_id)
    {
        $arr_groups     = []; 
        $arr_group_list = AccGroup::where('acc_sub_head_id',$acc_sub_head_id)->where('acc_group_status',1)->get();

        if(!empty($arr_group_list))
        {
            foreach ($arr_group_list as $arr_group)
                {
                    $arr_groups[$arr_group['acc_group_id']] = $arr_group['acc_group'];
                }
        }
        return $arr_groups;
    }

    function get_room_by_roomCategory($hostel_id,$h_block_id,$h_floor_id,$h_room_cate_id)
    {
        $loginInfo 			= get_loggedin_user_data();
        $room_category     = [];
        $arr_room_category = HostelRooms::where([['hostel_id', '=', $hostel_id],['h_block_id', '=', $h_block_id],['h_floor_id', '=', $h_floor_id],['h_room_cate_id', '=', $h_room_cate_id]])->select('h_room_id', 'h_room_no')->get();
        
        if (!empty($arr_room_category))
        {
            foreach ($arr_room_category as $value)
            {
                $room_category[$value['h_room_id']] = $value['h_room_no'];
            }
        }
        return $room_category;
    }

    /**
     * Get all Accounts Heads
    **/
    function get_all_group_heads($acc_sub_head_id)
    {
        $arr_acc_head = [];  
        $arr_head_list = AccGroup::where('acc_sub_head_id',$acc_sub_head_id)->with('getGroupHeads')->get();
        if (!empty($arr_head_list))
        {
            foreach ($arr_head_list as $arr_acc_heads)
            {
                $arr_sub_head = [];
                foreach ($arr_acc_heads->getGroupHeads as $subHeads)
                {
                    $arr_sub_head[$subHeads->acc_group_head_id] = $subHeads->acc_group_head;
                }
                $arr_acc_head[$arr_acc_heads->acc_group] = $arr_sub_head;
            }
        }
        return $arr_acc_head;
    }
?>
