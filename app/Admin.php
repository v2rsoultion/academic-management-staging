<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPassword;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table      = 'admins';
    protected $primaryKey = 'admin_id';

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPassword($token));
    }

    public function adminSchool()
    {
        return $this->hasOne('App\Model\School\School','admin_id');
    }

    public function adminStudent()
    {
        return $this->hasOne('App\Model\Student\Student','reference_admin_id');
    }

    public function adminStudentParent()
    {
        return $this->hasOne('App\Model\Student\StudentParent','reference_admin_id');
    }
    
}
