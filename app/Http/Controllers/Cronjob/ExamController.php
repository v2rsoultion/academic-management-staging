<?php

namespace App\Http\Controllers\Cronjob;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Exam\Exam; // Model
use App\Model\Exam\ExamScheduleMap; // Model
use App\Model\Examination\StudentExamResult;
use App\Model\Examination\StMarksheet;
use App\Model\Section\Section; // Model
use Yajra\Datatables\Datatables;

class ExamController extends Controller
{
    /**
     *  Exam Day Notification
     *  @Sandeep on 11 March 2019
    **/
    public function exam_day_notification()
    {

        $mapData = [];    
        $scheduleMap = ExamScheduleMap::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('exam_id')))
            {
                $query->where('exam_id', "=", $request->get('exam_id'));
            }
            $query->where('exam_date', "=", date('Y-m-d'));
            $query->where('publish_status', "=", 1);
            $query->where('cronjob_status', "=", 0);
        })
        ->orderBy('class_id','ASC')
        ->get();

        $message = "Best of luck for Exam, From Academic Eye Team";

        foreach ($scheduleMap as $scheduleMaps) {
            $notification = $this->send_push_notification_class($scheduleMaps->exam_id,$scheduleMaps->class_id,$scheduleMaps->section_id,'exam_good_luck',$message);

            $exam  = ExamScheduleMap::find($scheduleMaps->schedule_map_id); 
            $exam->cronjob_status   = 1;
            $exam->save();

        }
        p($scheduleMap);

    }

    /**
     *  Exam Result Declare
     *  @Sandeep on 12 March 2019
    **/

    public function exam_result_declare()
    {

        $mapData = [];    
        $scheduleMap = StudentExamResult::where(function($query) use ($request) 
        {
            $query->where('result_date', "=", date('Y-m-d'));
            $query->where('result_time', "<", date('H:i:s'));
            $query->where('cronjob_status', "=", 0);
        })
        ->orderBy('class_id','ASC')
        ->get();

        $message = "Dear Students, Please check your results are declared on the app and web portals.";

        foreach ($scheduleMap as $scheduleMaps) {

            $section_info  = Section::where(function($query) use ($scheduleMaps) 
            {
                $query->where('class_id',$scheduleMaps->class_id);
            })
            ->get();

            foreach ($section_info as $section_infos) {
                $notification = $this->send_push_notification_class($scheduleMaps->exam_id,$scheduleMaps->class_id,$section_infos->section_id,'exam_result_declare',$message);    

                $session = get_current_session();
                $arr_student= StMarksheet::where([['session_id', $session['session_id']],['exam_id',$scheduleMaps->exam_id],['class_id',$scheduleMaps->class_id],['section_id',$section_infos->section_id]])->where('class_rank','<=',10)->with('studentInfo')->orderBy('class_rank', 'ASC')->get()->toArray();

                foreach ($arr_student as $arr_students) {

                    $message = "Congratulation ".$arr_students['student_info']['student_name']." for being in topper list, From Academic Eye Team.";
                    
                    $notification = $this->send_push_notification_class($scheduleMaps->exam_id,$scheduleMaps->class_id,$section_infos->section_id,'exam_pass_congratulations',$message,$arr_students['student_info']['student_id']);   
                }

            }

            $exam  = StudentExamResult::find($scheduleMaps->st_exam_result_id); 
            $exam->cronjob_status   = 1;
            $exam->save();

        }
        p($scheduleMap);

    }





    public function send_push_notification_class($module_id,$class_id,$section_id,$notification_type,$message,$student_id=null){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('class_id',$class_id);
            $query->where('section_id',$section_id);
        })
        ->with('sectionClass')
        ->get()->toArray();

        if($student_id != ""){
            $student_id = $student_id;
        } else {
            $student_id = "";
        }
        
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);
            
            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

            $students = pushnotification_by_topic($module_id,$notification_type,"","",$student_id,$studentTopicName,$message,$section['class_id'],$section['section_id'],1);
            $parents = pushnotification_by_topic($module_id,$notification_type,"","",$student_id,$parentTopicName,$message,$section['class_id'],$section['section_id'],2);
           
        }
        
    }


}
