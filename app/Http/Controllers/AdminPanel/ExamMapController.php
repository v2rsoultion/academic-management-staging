<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Exam\Exam; // Model
use App\Model\Examination\ExamMap; // Model
use Yajra\Datatables\Datatables;

class ExamMapController extends Controller
{
    /**
     *  View page for Map Exam To Class Subjects
     *  @Shree on 22 Oct 2018
    **/
    public function index()
    {
        $map                = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams'] = add_blank_option($arr_exams, 'Select Exam');
        $data = array(
            'page_title'    => trans('language.map_exam_class_subject'),
            'redirect_url'  => url('admin-panel/examination/manage-exam'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.exam-map.index')->with($data);
    }

    /**
     *  Show exam list for class subject
     *  @Shree on 22 Oct 2018
    **/
    public function show_exams(Request $request){
        $exam  = Exam::where('exam_status','=',1)->where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('exam_id')) && $request->get('exam_id') != null)
            {
                $query->where('exam_id', "=", $request->get('exam_id'));
            }
        })->orderBy('exam_id', 'DESC')->get();
        
        return Datatables::of($exam,$request)    
        ->addColumn('map_classes', function ($exam)
        {
            $encrypted_exam_id = get_encrypted_value($exam->exam_id, true);
            $map_classes = '<a href="'.url('/admin-panel/examination/map-classes/'.$encrypted_exam_id.' ').'" class="btn btn-raised btn-primary">Mapped Class</a>';
            return $map_classes;
        })
        ->addColumn('action', function ($exam)
        {
            $encrypted_exam_id = get_encrypted_value($exam->exam_id, true);
            $map_classes = '<a href="'.url('/admin-panel/examination/add-classes/'.$encrypted_exam_id.' ').'" class="btn btn-raised btn-primary ">Add Classes</a>';
            return $map_classes;
        })->rawColumns(['action' => 'action', 'map_classes'=> 'map_classes'])->addIndexColumn()
        ->make(true);
    }

    public function add_classes($id){
        $data    				= [];
        $exam = $listData = $arr_section = [];
        $loginInfo 				= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_exam_id 	= get_decrypted_value($id, true);
            $exam      			= Exam::Find($decrypted_exam_id);
            if (!$exam)
            {
                return redirect('admin-panel/examination/manage-exam')->withError('Exam not found!');
            }
            $page_title             	= trans('language.add_classes');
            $encrypted_exam_id   		= get_encrypted_value($exam->exam_id, true);
            $save_url               	= url('admin-panel/examination/save-classes/' . $encrypted_exam_id);
            $submit_button          	= 'Save';
        }
        else
        {
            return redirect()->back()->withInput()->withErrors("Exam not found");
        }
        $arr_class = get_all_class_for_exam_map($decrypted_exam_id);
        $listData['arr_class'] = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section'] = add_blank_option($arr_section, 'Select Section');
        
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'exam' 				=> $exam,
            'listData'			=> $listData,
            'login_info'    	=> $loginInfo,
            'decrypted_exam_id' => $decrypted_exam_id,
            'redirect_url'  	=> url('admin-panel/examination/add-classes'.$id),
        );
        return view('admin-panel.exam-map.add_classes')->with($data);
    }

    public function get_sections()
    {
        $class_id = Input::get('class_id');
        $exam_id = Input::get('exam_id');
        $section = get_all_class_sections($exam_id,$class_id);
        $data = view('admin-panel.class-teacher-allocation.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    public function get_map_sections()
    {
        $class_id = Input::get('class_id');
        $exam_id = Input::get('exam_id');
        $section = get_all_sections_map_exams($exam_id,$class_id);
        $data = view('admin-panel.class-teacher-allocation.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    public function show_section_subject(Request $request){
        $section_id = $request->get('section_id');
        $counter = $request->get('countVal');
        $subjects = get_all_section_subjects($section_id);
        $data = view('admin-panel.exam-map.ajax-class-subject',compact('counter','subjects'))->render();
        return response()->json(['res'=>$data]);
    }

    public function save_classes(Request $request){
        $loginInfo      = get_loggedin_user_data();
        $addClasses     = $request->get('addClasses');
        $session = get_current_session();
        $exam_id = Input::get('decrypted_exam_id');
        DB::beginTransaction();
        foreach($addClasses as $class){
            if(!empty($class['subjects'])){
                foreach($class['subjects'] as $subject){
                   
                    try
                    {
                        $check_existance = ExamMap::where('session_id', '=',$session['session_id'])->where('class_id', '=',$class['class_id'])->where('section_id', '=',$class['section_id'])->where('subject_id', '=',$subject)->where('exam_id', '=',$exam_id)->get()->toArray();
                        if(empty($check_existance)) {
                            $exammap     		= New ExamMap;
                            $exammap->admin_id      = $loginInfo['admin_id'];
                            $exammap->update_by     = $loginInfo['admin_id'];
                            $exammap->exam_id 	    = $exam_id;
                            $exammap->session_id      = $session['session_id'];
                            $exammap->class_id      = $class['class_id'];
                            $exammap->section_id   = $class['section_id'];
                            $exammap->subject_id   = $subject;
                            $exammap->save();
                        }
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return redirect()->back()->withErrors($error_message);
                    }
                }
            }
        }
        DB::commit();
        $success_msg 	= 'Exam map saved successfully!';
        return redirect('admin-panel/examination/map-exam-class-subjects')->withSuccess($success_msg);
    
    }

    public function view_map_classes($id){
        $session = get_current_session();
        $loginInfo      = get_loggedin_user_data();
        $map_data = [];
        $page_title         = trans('language.map_classes');
        $submit_button      = 'Save';
        $decrypted_exam_id 	= get_decrypted_value($id, true);
        $exam      			= Exam::Find($decrypted_exam_id);
        if (!$exam)
        {
            return redirect('admin-panel/examination/manage-exam')->withError('Exam not found!');
        }
        
        $encrypted_exam_id   		= get_encrypted_value($exam->exam_id, true);
        $save_url               	= url('admin-panel/examination/map-save-classes/' . $encrypted_exam_id);
        $counter = 0;
        $arr_medium     = get_all_mediums();
        $exam_map_class  = ExamMap::where("exam_id",'=',$decrypted_exam_id)->where('session_id','=',$session['session_id'])->groupBy('section_id')->with('classInfo')->with('sectionInfo')->get()->toArray();
        // p($exam_map_class);
        foreach($exam_map_class as $class){
            $exam_map_subjects = '';
            $exam_map_subjects = ExamMap::where("exam_id",'=',$decrypted_exam_id)->where('session_id','=',$session['session_id'])->where('class_id','=',$class['class_id'])->where('section_id','=',$class['section_id'])->get()->toArray();
            
            $subject_ids = [];
            $map_data[$counter]['class_id'] = $class['class_id'];
            $map_data[$counter]['section_id'] = $class['section_id'];
            $map_data[$counter]['class_name'] = $class['class_info']['class_name'].' - '.$arr_medium[$class['class_info']['medium_type']].' - '.$class['section_info']['section_name'];
            $map_data[$counter]['all_subjects'] = get_all_section_subjects($class['section_id']);
            foreach($exam_map_subjects as $subject){
                $subject_ids[] = $subject['subject_id'];
            }
            $map_data[$counter]['subject'] = $subject_ids;
            $counter++;
        }
        // p($map_data);
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'exam' 				=> $exam,
            'map_data'			=> $map_data,
            'login_info'    	=> $loginInfo,
            'decrypted_exam_id' => $decrypted_exam_id,
            'redirect_url'  	=> url('admin-panel/examination/map-classes'.$id),
        );
        return view('admin-panel.exam-map.map_classes')->with($data);
        
    }

    public function map_save_classes(Request $request){
        $loginInfo      = get_loggedin_user_data();
        $mapClasses     = $request->get('map_class');
        $session = get_current_session();
        DB::beginTransaction();
        foreach($mapClasses as $class){
            // p($class);
            if(!empty($class['subjects'])){
                foreach($class['subjects'] as $subject){
                    if($subject['exist'] == 0){
                        // Not exist
                        if(isset($subject['subject_id'])){
                            $exammap  = New ExamMap;
                            $exammap->admin_id      = $loginInfo['admin_id'];
                            $exammap->update_by     = $loginInfo['admin_id'];
                            $exammap->exam_id       =  Input::get('decrypted_exam_id');
                            $exammap->session_id      = $session['session_id'];
                            $exammap->class_id      = $class['class_id'];
                            $exammap->section_id      = $class['section_id'];
                            $exammap->subject_id    = $subject['subject_id'];
                            $exammap->save();
                        }
                    } else {
                        // Already exist
                        if(isset($subject['subject_id'])){
                            // Update case
                            
                        } else {
                            // Delete Case
                            $exammap_delete = ExamMap::where('class_id', '=', $class['class_id'])->where('section_id', '=', $class['section_id'])->where('subject_id', '=', $subject['id'])->where('exam_id', '=', Input::get('decrypted_exam_id'))->where('session_id','=',$session['session_id'])->first();
                            $exammap_delete->delete();
                        }
                    }
                }
            }
        }
        DB::commit();
        $success_msg 	= 'Exam class subject mapping update successfully!';
        return redirect()->back()->withSuccess($success_msg);
        
    }

    public function remove_map_section(Request $request){
        $section_id = $request->get('section_id');
        $exam_id = $request->get('exam_id');
        $session = get_current_session();
        $exammap_delete = ExamMap::where('section_id', '=', $section_id)->where('session_id','=',$session['session_id'])->where('exam_id', '=', $exam_id)->get();
        foreach($exammap_delete as $data){
            $data->delete();
        }
        return "success";
    }

    /**
     *  View page for Exam To Class Subject report
     *  @Shree on 23 Oct 2018
    **/
    public function exam_class_report()
    {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class = get_all_classes_mediums();
        $map['arr_class'] = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.exam_class_subject_report'),
            'redirect_url'  => url('admin-panel/examination/manage-exam'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.exam-map.exam_class_report')->with($data);
    }

    /**
     *  Show exam list for class subject
     *  @Shree on 22 Oct 2018
    **/
    public function show_exam_class_report(Request $request){
        
        $session = get_current_session();
        $exam  = ExamMap::where(function($query) use ($request,$session) 
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
        })->orderBy('exam_id', 'DESC')->groupBy('exam_id')->with('examInfo')->with('classInfo')->with('sectionInfo')->get();
        // p($exam);
        return Datatables::of($exam,$request)    
        ->addColumn('exam_name', function ($exam)
        {
            $exam_name = '----';
            if(isset($exam['examInfo']->exam_name)) {
                $exam_name = $exam['examInfo']->exam_name;
            }
            return $exam_name;
        })
        ->addColumn('subject_count', function ($exam) use ($request)
        {
            $session = get_current_session();
            $totalSubjects = 0;
            $totalSubjects = ExamMap::where(function($query) use ($request,$session,$exam) 
            {
                $query->where('session_id', "=", $session['session_id']);
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $query->where('class_id', "=", $request->get('class_id'));
                }
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $query->where('section_id', "=", $request->get('section_id'));
                }
                $query->where('exam_id', "=", $exam->exam_id);
            })->get()->count();
           
            return $totalSubjects;
        })
        ->addColumn('subject', function ($exam)
        {
            $encrypted_exam_id = get_encrypted_value($exam->exam_id, true);
            $map_classes = '<button class="btn btn-raised btn-primary exams" exam-id='.$encrypted_exam_id.'>Subjects</button>';
            return $map_classes;
        })
        ->addColumn('manage_criteria', function ($exam)
        {
            $encrypted_exam_id = get_encrypted_value($exam->exam_id, true);
            $exam_name = $exam['examInfo']->exam_name;
            $class_name = $exam['classInfo']->class_name;
            $section_name = $exam['sectionInfo']->section_name;
            $map_classes = '<button class="btn btn-raised btn-primary exams" class-name='.$class_name.' section-name='.$section_name.' exam-name='.$exam_name.' exam-id='.$encrypted_exam_id.'>Manage Criteria</button>';
            return $map_classes;
        })
        ->addColumn('manage_grade', function ($exam)
        {
            $encrypted_exam_id = get_encrypted_value($exam->exam_id, true);
            $exam_name = $exam['examInfo']->exam_name;
            $map_classes = '<button class="btn btn-raised btn-primary grades"  exam-name='.$exam_name.' exam-id='.$encrypted_exam_id.'>Manage Grade</button>';
            return $map_classes;
        })
       ->rawColumns(['subject' => 'subject','subject_count'=>'subject_count','manage_criteria'=>'manage_criteria', 'manage_grade'=> 'manage_grade'])->addIndexColumn()
        ->make(true);
    }

    public function show_exam_subjects(Request $request){
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        $session = get_current_session();
        $decrypted_exam_id 	= get_decrypted_value($exam_id, true);
        $exam  = ExamMap::where('exam_id','=',$decrypted_exam_id)->where('session_id','=',$session['session_id'])->where('class_id','=',$class_id)->where('section_id','=',$section_id)->orderBy('class_id', 'DESC')->with('classInfo')->with('sectionInfo')->with('subjectInfo')->get()->toArray();
       // p($exam);
        return Datatables::of($exam,$request)      
        ->addColumn('class_name', function ($exam)
        {
            $arr_medium     = get_all_mediums();
            $className = $exam['class_info']['class_name'].' - '.$arr_medium[$exam['class_info']['medium_type']].' - '.$exam['section_info']['section_name'];
            return $className;
        })
        ->addColumn('subject_name', function ($exam)
        {
            $subjectName = $exam['subject_info']['subject_name'].' - '.$exam['subject_info']['subject_code'];
            return $subjectName;
        })
        ->addColumn('marks_criteria', function ($exam) 
        {
            $all_criteria  = get_all_marks_criteria($exam['marks_criteria_id']);
            $select = '
            <input type="hidden" name="criteria_map['.$exam['exam_map_id'].'][exam_map_id]" value="'.$exam['exam_map_id'].'" >
            
            <label class=" field select" style="width: 80%"><select class="form-control show-tick select_form1 select2 "  name="criteria_map['.$exam['exam_map_id'].'][marks_criteria_id]" >'.$all_criteria.'</select></label>';

            return $select;
        })
        ->addColumn('grade_scheme', function ($exam) 
        {
            $all_schemes  = get_all_grade_schemes($exam['grade_scheme_id']);
            $select = '
            <input type="hidden" name="scheme_map['.$exam['exam_map_id'].'][exam_map_id]" value="'.$exam['exam_map_id'].'" >
            
            <label class=" field select" style="width: 80%"><select class="form-control show-tick select_form1 select2 "  name="scheme_map['.$exam['exam_map_id'].'][grade_scheme_id]" >'.$all_schemes.'</select></label>';

            return $select;
        })
       ->rawColumns(['class_name' => 'class_name','subject_name' => 'subject_name','marks_criteria'=>'marks_criteria','grade_scheme'=>'grade_scheme'])->addIndexColumn()
        ->make(true);
    }
    public function show_exam_class_report_data(Request $request){
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        $session = get_current_session();
        $exam  = ExamMap::where('exam_id','=',$exam_id)->where('session_id','=',$session['session_id'])->where('class_id','=',$class_id)->where('section_id','=',$section_id)->orderBy('class_id', 'DESC')->with('classInfo')->with('subjectInfo')->get()->toArray();
        // p($exam);
        return Datatables::of($exam,$request)      
       
        ->addColumn('subject_name', function ($exam)
        {
            $subjectName = $exam['subject_info']['subject_name'].' - '.$exam['subject_info']['subject_code'];
            return $subjectName;
        })
       
        
       ->rawColumns(['subject_name' => 'subject_name'])->addIndexColumn()
        ->make(true);
    }
    /**
     *  View page for manage marks criteria to subjects
     *  @Shree on 23 Oct 2018
    **/
    public function manage_marks_criteria()
    {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_class = get_all_classes_mediums();
        $map['arr_class'] = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.manage_criteria'),
            'redirect_url'  => url('admin-panel/examination/manage-exam'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.exam-map.manage_markscriteria')->with($data);
    }

    public function save_marks_criteria(Request $request){
        $criteria_map     = array_values($request->get('criteria_map'));
        
        $loginInfo        = get_loggedin_user_data();
        DB::beginTransaction();
        try
        {
            foreach($criteria_map as $criteria){
                if($criteria['marks_criteria_id'] != ""){
                    $examMap = ExamMap::find($criteria['exam_map_id']);
                    $examMap->update_by     = $loginInfo['admin_id'];
                    $examMap->marks_criteria_id  = $criteria['marks_criteria_id'];
                    $examMap->save();
                }   
            }
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return $error_message;
        }
        
        DB::commit();
        return "success";
    }

    public function save_grade_scheme(Request $request){
        $scheme_map     = array_values($request->get('scheme_map'));
        
        $loginInfo        = get_loggedin_user_data();
        DB::beginTransaction();
        try
        {
            foreach($scheme_map as $schemeD){
                if($schemeD['grade_scheme_id'] != ""){
                    $examMap = ExamMap::find($schemeD['exam_map_id']);
                    $examMap->update_by     = $loginInfo['admin_id'];
                    $examMap->grade_scheme_id  = $schemeD['grade_scheme_id'];
                    $examMap->save();
                }   
            }
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return $error_message;
        }
        
        DB::commit();
        return "success";
    }

    /**
     *  View page for manage grade scheme to subjects
     *  @Shree on 23 Oct 2018
    **/
    public function manage_grade_scheme()
    {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_class = get_all_classes_mediums();
        $map['arr_class'] = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.manage_grade_scheme'),
            'redirect_url'  => url('admin-panel/examination/manage-exam'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.exam-map.manage_gradescheme')->with($data);
    }

    public function show_markscriteria_report()
    {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class = get_all_classes_mediums();
        $map['arr_class'] = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.markscriteria_map_report'),
            'redirect_url'  => url('admin-panel/examination/markscriteria-map-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.exam-map.markscriteria_report')->with($data);
    }

    public function show_marks_criteria_report(Request $request){
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        $session = get_current_session();
        $exam  = ExamMap::where('exam_id','=',$exam_id)->where('session_id','=',$session['session_id'])->where('class_id','=',$class_id)->where('section_id','=',$section_id)->orderBy('class_id', 'DESC')->with('classInfo')->with('subjectInfo')->with('marksCriteriaInfo')->get()->toArray();
        // p($exam);
        return Datatables::of($exam,$request)      
       
        ->addColumn('subject_name', function ($exam)
        {
            $subjectName = $exam['subject_info']['subject_name'].' - '.$exam['subject_info']['subject_code'];
            return $subjectName;
        })
        ->addColumn('criteria_name', function ($exam)
        {
            $criteria_name = $exam['marks_criteria_info']['criteria_name'];
            return $criteria_name;
        })
        ->addColumn('max_marks', function ($exam)
        {
            $max_marks = $exam['marks_criteria_info']['max_marks'];
            return $max_marks;
        })
        ->addColumn('passing_marks', function ($exam)
        {
            $passing_marks = $exam['marks_criteria_info']['passing_marks'];
            return $passing_marks;
        })
        
        
       ->rawColumns(['subject_name' => 'subject_name','criteria_name'=>'criteria_name','max_marks'=>'max_marks','passing_marks'=>'passing_marks'])->addIndexColumn()
        ->make(true);
    }

    public function show_gradescheme_report()
    {
        $map = $arr_section = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_exams          = get_all_exam_type();
        $map['arr_exams']   = add_blank_option($arr_exams, 'Select Exam');
        $arr_class = get_all_classes_mediums();
        $map['arr_class'] = add_blank_option($arr_class, "Select Class");
        $map['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.gradescheme_map_report'),
            'redirect_url'  => url('admin-panel/examination/gradescheme-map-report'),
            'login_info'    => $loginInfo,
            'map'           => $map
        );
        return view('admin-panel.exam-map.gradescheme_report')->with($data);
    }
    public function show_grade_scheme_report(Request $request){
        $exam_id = $request->get('exam_id');
        $class_id = $request->get('class_id');
        $section_id = $request->get('section_id');
        $session = get_current_session();
        $exam  = ExamMap::where('exam_id','=',$exam_id)->where('session_id','=',$session['session_id'])->where('class_id','=',$class_id)->where('section_id','=',$section_id)->orderBy('class_id', 'DESC')->with('classInfo')->with('subjectInfo')->with('gradeSchemeInfo')->get()->toArray();
        // p($exam);
        return Datatables::of($exam,$request)      
       
        ->addColumn('subject_name', function ($exam)
        {
            $subjectName = $exam['subject_info']['subject_name'].' - '.$exam['subject_info']['subject_code'];
            return $subjectName;
        })
        ->addColumn('grade_scheme_name', function ($exam)
        {
            $schemeName = '----';
            if(isset($exam['grade_scheme_info']['scheme_name'])) {
                $schemeName = $exam['grade_scheme_info']['scheme_name'];
            }
            return $schemeName;
        })
        ->addColumn('grade_scheme_type', function ($exam)
        {
            $grade_types         = \Config::get('custom.grade_type');
            $grade_type = '----';
            if(isset($exam['grade_scheme_info']['grade_type'])) {
                $grade_type = $grade_types[$exam['grade_scheme_info']['grade_type']];
            }
            return $grade_type;
        })
        
        
       ->rawColumns(['subject_name' => 'subject_name','grade_scheme_name'=>'grade_scheme_name','grade_scheme_type'=>'grade_scheme_type'])->addIndexColumn()
        ->make(true);
    }
}
