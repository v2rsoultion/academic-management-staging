<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Visitor\Visitor; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;

class VisitorController extends Controller
{
    /**
     *  View page for Visitor list
     *  @shree on 4 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_visitor'),
            'redirect_url'  => url('admin-panel/caste/view-visitor'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.visitor.index')->with($data);
    }

    /**
     *  Add page for Visitor
     *  @shree on 4 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $visitor      	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_visitor_id 	    = get_decrypted_value($id, true);
            $visitor      			    = Visitor::Find($decrypted_visitor_id);
            if (!$visitor)
            {
                return redirect('admin-panel/visitor/add-visitor')->withError('Visitor not found!');
            }
            $page_title             	= trans('language.edit_visitor');
            $encrypted_visitor_id         = get_encrypted_value($visitor->visitor_id, true);
            $save_url               	= url('admin-panel/visitor/save/' . $encrypted_visitor_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_visitor');
            $save_url      = url('admin-panel/visitor/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'visitor' 		    => $visitor,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/visitor/view-visitor'),
        );
        return view('admin-panel.visitor.add')->with($data);
    }

    /**
     *  Add and update visitor's data
     *  @Shree on 4 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_visitor_id	        = get_decrypted_value($id, true);
        $visit_date = date('Y-m-d H:i:s');
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $visitor = Visitor::find($decrypted_visitor_id);
            $admin_id = $visitor['admin_id'];
            $visit_date = $visitor['visit_date'];
            if (!$visitor)
            {
                return redirect('/admin-panel/visitor/add-visitor/')->withError('Visitor not found!');
            }
            $success_msg = 'Visitor updated successfully!';
        }
        else
        {
            $visitor     = New Visitor;
            $success_msg = 'Visitor saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'name'              => 'required',
            'purpose'           => 'required',
            'contact'           => 'required',
            'check_in_time'     => 'required',
            'check_out_time'    => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $visitor->admin_id          = $admin_id;
                $visitor->update_by         = $loginInfo['admin_id'];
                $visitor->name 		        = Input::get('name');
                $visitor->purpose 		    = Input::get('purpose');
                $visitor->contact 		    = Input::get('contact');
                $visitor->email 		    = Input::get('email');
                $visitor->visit_date 		= $visit_date;
                $visitor->check_in_time 	= Input::get('check_in_time');
                $visitor->check_out_time    = Input::get('check_out_time');
                $visitor->save();

                $send_notification  = $this->send_push_notification($visitor->visitor_id,"Visitor",$visitor->name,'visit');
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/visitor/view-visitor')->withSuccess($success_msg);
    }

    /**
     *  Get visitor's Data for view page(Datatables)
     *  @Shree on 4 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        
        $loginInfo 			= get_loggedin_user_data();
        $visitor  		    = Visitor::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('visit_date')))
            {
                $query->where('visit_date', "=", "{$request->get('visit_date')}");
            }
        })->orderBy('visitor_id', 'DESC')->get();
        // p($visitor);
        return Datatables::of($visitor)
        ->addColumn('date', function ($visitor)
        {
            $date = date("d M Y", strtotime($visitor->visit_date));
            return $date;
        })
        ->addColumn('time', function ($visitor)
        {
            $time = date("H:i A", strtotime($visitor->check_in_time)).' - '.date("H:i A", strtotime($visitor->check_out_time));
            return $time;
        })
        ->addColumn('action', function ($visitor)
        {

            $encrypted_visitor_id = get_encrypted_value($visitor->visitor_id, true);
            
            return '
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-visitor/' . $encrypted_visitor_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-visitor/' . $encrypted_visitor_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['date' => 'date', 'time' => 'time', 'action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Visitor's data
     *  @Shree on 4 Oct 2018.
    **/
    public function destroy($id)
    {
        $visitor_id 		= get_decrypted_value($id, true);
        $visitor 		  	= Visitor::find($visitor_id);
        
        $success_msg = $error_message =  "";
        if ($visitor)
        {
            DB::beginTransaction();
            try
            {
                $visitor->delete();
                $success_msg = "Visitor deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/visitor/view-visitor')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/visitor/view-visitor')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Visitor not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function send_push_notification($module_id,$module_name,$visitor_name,$notification_type){
        $device_ids = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereRaw('FIND_IN_SET(1,staff_role_id)')->orWhereRaw('FIND_IN_SET(10,staff_role_id)')->orWhereRaw('FIND_IN_SET(11,staff_role_id)')->select('staff_name', 'staff_id','reference_admin_id')->groupBy('staff_id')->with('getStaffAdminInfo')->get();
        foreach ($arr_staff_list as $staff) {
            if($staff['getStaffAdminInfo']->fcm_device_id != ""){
                $device_ids[] = $staff['getStaffAdminInfo']->fcm_device_id;
            }
        }
        $message = "New visitor is recorded, click here to check details.";
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
    }

}
