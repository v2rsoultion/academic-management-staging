<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\ConcessionMap; // Model
use App\Model\FeesCollection\RecurringInst; // Model
use Yajra\Datatables\Datatables;

class ConcessionController extends Controller
{
    /**
     *  View page for manage concession
     *  @shree on 10 Oct 2018
    **/
    public function index()
    {
        $loginInfo  = get_loggedin_user_data();
        $concession = $arr_section =  [];
        $arr_class  = get_all_classes_mediums();
        $concession['arr_class']    = add_blank_option($arr_class, "Select Class");
        $concession['arr_section']  = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.manage_concession'),
            'redirect_url'  => url('admin-panel/concession/manage-concession'),
            'login_info'    => $loginInfo,
            'concession'    => $concession,
        );
        return view('admin-panel.concession.index')->with($data);
    }

    /**
     *  Get student list for manage concession
     *  @Shree on 9 Oct 2018
    **/
    public function get_student_list(Request $request)
    {
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $request->request->add(['student_type', 'rte_apply_status']);
            $request->merge(['student_type' => '0', 'rte_apply_status'=>'0']);
           
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('concession', function ($student)
            {
                return '<button type="button" class="btn btn-raised btn-primary students"  student-id="'.$student->student_id.'" >
                Manage
                </button>';
                
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'concession'=> 'concession'])->addIndexColumn()->make(true);
    }

    /**
     *  Get student list for manage concession
     *  @Shree on 9 Oct 2018
    **/
    public function get_student_fees(Request $request)
    {
        $get_all_fees = get_complete_fees($request);
        
        return Datatables::of($get_all_fees)
            ->setRowId('id')
            ->addColumn('concession_amt_data', function ($get_all_fees)
            {
                $concession_amt = 0;
                if($get_all_fees['concession_amt'] != ''){
                    $concession_amt = $get_all_fees['concession_amt'];
                }
                $readonly = "";
                if($get_all_fees['head_expire'] == 1){
                    $readonly = "readonly";
                }
                if($get_all_fees['head_expire'] == 1){
                    $readonly = "readonly";
                }
                return '<input type="number" name="concession['.$get_all_fees['counter'].'][amt]" onkeyup="calculateAmt('.$get_all_fees['counter'].')" id="concession_amt'.$get_all_fees['counter'].'" class="form-control concession-amt" value="'.$concession_amt.'" style="max-width: 180px;" min="0" '.$readonly.'>
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][head_id]" id="head_id'.$get_all_fees['counter'].'" value="'.$get_all_fees['head_id'].'" >
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][head_inst_id]" id="head_inst_id'.$get_all_fees['counter'].'" value="'.$get_all_fees['head_inst_id'].'" >
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][head_type]" id="head_type'.$get_all_fees['counter'].'" value="'.$get_all_fees['head_type'].'" >
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][head_amt]" id="head_amt'.$get_all_fees['counter'].'" value="'.$get_all_fees['head_amt'].'">
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][concession_map_id]" id="concession_map_id'.$get_all_fees['counter'].'" value="'.$get_all_fees['concession_map_id'].'">
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][class_id]" id="class_id'.$get_all_fees['counter'].'" value="'.$get_all_fees['class_id'].'">
                
                <input type="hidden" name="concession['.$get_all_fees['counter'].'][student_id]" id="student_id'.$get_all_fees['counter'].'" value="'.$get_all_fees['student_id'].'">

                <p id="error_msg'.$get_all_fees['counter'].'" class="red" style="margin-bottom: 0px !important;" ></p>
                ';
            })
            ->addColumn('paid_amt', function ($get_all_fees)
            {
                $concession_amt = 0;
                if($get_all_fees['concession_amt'] != ''){
                    $concession_amt = $get_all_fees['concession_amt'];
                }
                $head_amt = $get_all_fees['head_amt'] - $concession_amt;
                return '<span id="paid_fees_id'.$get_all_fees['counter'].'" >'.$head_amt.'</span>';
            })
            ->addColumn('reason', function ($get_all_fees)
            {
                $readonly = "";
                if($get_all_fees['head_expire'] == 1){
                    $readonly = "readonly";
                }
                if($get_all_fees['head_expire'] == 1){
                    $readonly = "readonly";
                }
                $arr_reasons = get_all_reasons($get_all_fees['reason_id']);
                $select = '<label class=" field select" style="width: 80%"><select class="form-control show-tick select_form1 select2 " id="reason_id"  name="concession['.$get_all_fees['counter'].'][reason_id]" '.$readonly.' >'.$arr_reasons.'</select></label>';
                return $select;
            })
            ->addColumn('fees_date', function ($get_all_fees)
            {
                $date = date("d M Y", strtotime($get_all_fees['head_date']));
                return $date;
            })->rawColumns(['fees_date' => 'fees_date','reason','concession_amt_data'=>'concession_amt_data', 'paid_amt'=>'paid_amt'])->addIndexColumn()->make(true);
    }
    /**
     *  Get student list for manage concession
     *  @Shree on 9 Oct 2018
    **/
    public function concession_map(Request $request)
    {
        $session = get_current_session();
        $loginInfo  = get_loggedin_user_data();
        $head_id = null;
        DB::beginTransaction();
            try
            {
                foreach($request->get('concession') as $concessionData){
                    if($concessionData['amt'] != 0) {
                        if($concessionData['concession_map_id'] != ""){
                            $concession = ConcessionMap::find($concessionData['concession_map_id']);
                            $admin_id = $concession->admin_id;
                        } else {
                            $concession = New ConcessionMap;
                            $admin_id = $loginInfo['admin_id'];
                        }
                        if($concessionData['head_type'] == "ONETIME"){
                            $head_id = $concessionData['head_id'];
                        } else if($concessionData['head_type'] == "RECURRING"){
                            $head_id = $concessionData['head_inst_id'];
                        }
                        $concession->admin_id           = $admin_id;
                        $concession->update_by          = $loginInfo['admin_id'];
                        $concession->session_id         = $session['session_id'];
                        $concession->class_id           = $concessionData['class_id'];
                        $concession->student_id         = $concessionData['student_id'];
                        $concession->reason_id          = $concessionData['reason_id'];
                        $concession->head_id            = $head_id;
                        $concession->head_type          = $concessionData['head_type'];
                        $concession->concession_amt     = $concessionData['amt'];
                        $concession->save();
                    }
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return "fail";
            }
            DB::commit();
            return "success";
    }

}
