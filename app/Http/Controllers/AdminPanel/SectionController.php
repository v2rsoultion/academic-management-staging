<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Section\Section;
use App\Model\HomeworkGroup\HomeworkGroup;

use Yajra\Datatables\Datatables;

class SectionController extends Controller
{
    /**
     *  View page for section
     *  @Shree on 19 July 2018
    **/
    public function index()
    {
        $section = [];
        $loginInfo              = get_loggedin_user_data();
        $arr_class              = get_all_classes_mediums();
        $section['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'   => trans('language.view_section'),
            'redirect_url' => url('admin-panel/section/view-sections'),
            'login_info'    => $loginInfo,
            'section'       => $section
        );
        return view('admin-panel.section.index')->with($data);
    }

    /**
     *  Add page for section
     *  @Shree on 19 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $section    = [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_section_id = get_decrypted_value($id, true);
            $section              = $this->getSectionData($decrypted_section_id,"");
            $section              = isset($section[0]) ? $section[0] : [];
            if (!$section)
            {
                return redirect('admin-panel/section/add-section')->withError('Section not found!');
            }
            $page_title             = trans('language.edit_section');
            $decrypted_section_id   = get_encrypted_value($section['section_id'], true);
            $save_url               = url('admin-panel/section/save/' . $decrypted_section_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_section');
            $save_url      = url('admin-panel/section/save');
            $submit_button = 'Save';
        }
        $arr_class              = get_all_classes_mediums();
        $arr_shift              = get_all_shift();
        $arr_stream             = get_all_streams();
        $section['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $section['arr_shift']   = add_blank_option($arr_shift, 'Select Shift');
        $section['arr_stream']  = add_blank_option($arr_stream, 'Select Stream');
        
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'section'       => $section,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/section/view-sections'),
        );
        return view('admin-panel.section.add')->with($data);
    }

    /**
     *  Destroy section data
     *  @Shree on 19 July 2018
    **/
    public function destroy($id)
    {
        $section_id = get_decrypted_value($id, true);
        $section    = Section::find($section_id);
        $success_msg = $error_message =  "";
        if ($section)
        {
            DB::beginTransaction();
            try
            {
                $homework_group = HomeworkGroup::where('section_id', $section_id)->get();
                // p($homework_group);
                $homework_group[0]->delete();
                $section->delete();
                $success_msg = "Section deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/section/view-sections')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/section/view-sections')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Section not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Save section data
     *  @Shree on 19 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_section_id = get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $section    = Section::find($decrypted_section_id);
            $admin_id   = $section['admin_id'];
            $homework_group = HomeworkGroup::where([['class_id', '=', $section['class_id']],['section_id', '=', $section['section_id']]])->first();
            if (!$section)
            {
                return redirect('admin-panel/section/view-sections/')->withError('Section not found!');
            }
            $success_msg = 'Section update successfully!';
        }
        else
        {
            $section        = New Section;
            $homework_group = New HomeworkGroup;
            $success_msg = 'Section saved successfully!';
        }
        $class_id = null;
        if ($request->has('class_id'))
        {
            $class_id = Input::get('class_id');
        }
       
        $validatior = Validator::make($request->all(), [
                'section_name' => 'required|unique:sections,section_name,' . $decrypted_section_id . ',section_id,class_id,' . $class_id,
                'class_id'     => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                if($request->get('stream_status') == 1){
                    $section->stream_id = Input::get('stream_id');
                } else {
                    $section->stream_id = null;
                }
                $section->admin_id          = $admin_id;
                $section->update_by         = $loginInfo['admin_id'];
                $section->class_id          = Input::get('class_id');
                $section->section_name      = Input::get('section_name');
                $section->shift_id          = Input::get('shift_id');
                $section->section_intake    = Input::get('section_intake');
                $section->section_order     = Input::get('section_order');
                $section->save();

                $class_name = get_class_name(Input::get('class_id'));
                $homework_group->admin_id               = $admin_id;
                $homework_group->update_by              = $loginInfo['admin_id'];
                $homework_group->class_id               = Input::get('class_id');
                $homework_group->section_id             = $section->section_id;
                $homework_group->homework_group_name    = $class_name.' - '.Input::get('section_name').' Group';
                $homework_group->save();
                
            }
            catch (\Exception $e)
            { 
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/section/view-sections')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 19 July 2018
    **/
    public function anyData(Request $request)
    {
        $section     = [];
        $arr_section = $this->getSectionData("",$request);
        foreach ($arr_section as $key => $section_data)
        {
            $section[$key] = (object) $section_data;
        }
        return Datatables::of($section)
            ->addColumn('action', function ($section)
            {
                $encrypted_section_id = get_encrypted_value($section->section_id, true);
                if($section->section_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="View Students" href="view-students/' . $encrypted_section_id . '" "">View Students</a>
                    </li>
                    </ul>
                </div>
                <div class="pull-left"><a href="section-status/'.$status.'/' . $encrypted_section_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-section/' . $encrypted_section_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-section/' . $encrypted_section_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
               })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Section's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $section_id = get_decrypted_value($id, true);
        $section    = Section::find($section_id);
        if ($section)
        {
            $section->section_status  = $status;
            $section->save();
            $success_msg = "Section status update successfully!";
            return redirect('admin-panel/section/view-sections')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Section not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get section data
     *  @Shree on 19 July 2018
    **/
    public function getSectionData($section_id = array(),$request)
    {
        $loginInfo = get_loggedin_user_data();
        $section_return   = [];
        $section          = [];
        $arr_medium             = get_all_mediums();
        $arr_section_data = Section::where(function($query) use ($section_id,$loginInfo,$request) 
            {
                if (!empty($section_id))
                {
                    $query->where('section_id', $section_id);
                }
                if (!empty($request) && !empty($request->get('name')))
                {
                    $query->where('section_name', "like", "%{$request->get('name')}%");
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $query->where('class_id', "=", $request->get('class_id'));
                }
                if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
                {
                    $query->where('medium_type', "=", $request->get('medium_type'));
                }
            })->with('sectionClass')->with('sectionClass.getBoard')->with('sectionShift')->with('sectionStream')->orderBy('section_id', 'ASC')->get();
        
        if (!empty($arr_section_data))
        {
            foreach ($arr_section_data as $key => $section_data)
            {
                $section = array(
                    'section_id'        => $section_data['section_id'],
                    'section_name'      => $section_data['section_name'],
                    'section_intake'    => $section_data['section_intake'],
                    'section_order'     => $section_data['section_order'],
                    'section_status'    => $section_data['section_status'],
                    'class_id'          => $section_data['class_id'],
                    'shift_id'          => $section_data['shift_id'],
                    'stream_id'         => $section_data['stream_id'],
                );
                if (isset($section_data['sectionClass']['class_name']))
                {
                    $section['class_name'] = $section_data['sectionClass']['class_name'].' - '.$arr_medium[$section_data['sectionClass']['medium_type']].' - '.$section_data['sectionClass']['getBoard']['board_name'];
                }
                if (isset($section_data['sectionShift']['shift_name']))
                {
                    $section['shift_name'] = $section_data['sectionShift']['shift_name'];
                } else {
                    $section['shift_name'] = "----";
                }
                if (isset($section_data['sectionStream']['stream_name']))
                {
                    $section['stream_name'] = $section_data['sectionStream']['stream_name'];
                } else {
                    $section['stream_name'] = "----";
                }
                $section_return[] = $section;
            }
        }
        return $section_return;
    }

    /**
     *  Get class section data according class
    **/
    public function getClassSectionData()
    {

        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.section.ajax-class-section-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  View page for view students
     *  @Shree on 4 Oct 2018
    **/
    public function view_students($sectionid = NULL)
    {
        if(!empty($sectionid)) {
            $loginInfo                                  = get_loggedin_user_data();
            $decrypted_section_id                       = get_decrypted_value($sectionid, true);
            $arr_class_section_info                     = get_class_section_info($decrypted_section_id);
            $arr_section                                = get_class_section();
            $studentListData                            = [];
            $studentListData['arr_class_section_info']  = $arr_class_section_info;
            $studentListData['section_id']              = $decrypted_section_id;
            
            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.view_students'),
                'studentListData'       => $studentListData,
            );
            return view('admin-panel.section.view_students')->with($data);
        } else {
            return redirect('admin-panel/section/view-sections'); 
        }
    }

    

}
