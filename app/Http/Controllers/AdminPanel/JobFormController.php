<?php
namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Recruitment\Job;
use App\Model\Recruitment\JobForm;
use App\Model\Recruitment\JobFields;
use App\Model\Recruitment\RecruitmentForm;
use App\Model\Recruitment\RecruitmentFormStatus;
use Yajra\Datatables\Datatables;
use Mail;
use Redirect;

class JobFormController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('18',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Add page for Job Form
     *  @Sandeep on 12 Feb 2019
    **/

    public function add(Request $request, $id = NULL)
    {
        $data               = [];
        $job_form           = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_jobs           = get_all_jobs();
        $arr_message_via    = \Config::get('custom.message_via');
        
        if (!empty($id))
        {
            $decrypted_job_form_id = get_decrypted_value($id, true);
            
            $job_form              = JobForm::Find($decrypted_job_form_id);
            if (!$job_form)
            {
                return redirect('admin-panel/recruitment/forms/add-form')->withError('form not found!');
            }
            $page_title           = trans('language.edit_form');
            $decrypted_job_form_id = get_encrypted_value($job_form->job_form_id, true);
            $save_url             = url('admin-panel/recruitment/forms/save/' . $decrypted_job_form_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_form');
            $save_url      = url('admin-panel/recruitment/forms/save');
            $submit_button = 'Save';
        }

           $job_form['arr_message_via']       = $arr_message_via;
           $job_form['arr_jobs']              = add_blank_option($arr_jobs, 'Select Job');
       
        $data    = array(
            'page_title'        => $page_title,
            'save_url'          => $save_url,
            'submit_button'     => $submit_button,
            'job_form'          => $job_form,
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/recruitment/forms/view-form'),
        );
        return view('admin-panel.job_form.add')->with($data);
    }


    /**
     *  Add and update job form's data
     *  @Sandeep on 12 Feb 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo                      = get_loggedin_user_data();
        $decrypted_job_form_id    = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $jobform = JobForm::find($decrypted_job_form_id);

            if (!$jobform)
            {
                return redirect('/admin-panel/recruitment/forms/add-form/')->withError('Form not found!');
            }
            $success_msg = 'Form updated successfully!';
        }
        else
        {
            $jobform     = New JobForm;
            $success_msg = 'Form saved successfully!';
            $decrypted_job_form_id = null;
        }
        $validatior = Validator::make($request->all(), [
                'form_name'             => 'required|unique:job_forms,form_name,' . $decrypted_job_form_id . ',job_form_id',
                'job_id'                => 'required',
                'form_prefix'           => 'required',
                // 'form_success_message'  => 'required',
                // 'form_failed_message'   => 'required',
                // 'form_message_via'      => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $session       = get_current_session();
                $jobform->admin_id                = $loginInfo['admin_id'];
                $jobform->update_by               = $loginInfo['admin_id'];
                $jobform->form_name               = Input::get('form_name');
                $jobform->session_id              = $session['session_id'];
                $jobform->job_id                  = Input::get('job_id');
                $jobform->form_prefix             = Input::get('form_prefix');
                $jobform->form_message_via        = 0;
                $jobform->form_instruction        = Input::get('form_instruction');
                $jobform->form_information        = Input::get('form_information');
                $jobform->form_success_message    = Input::get('form_success_message');
                $jobform->form_failed_message     = Input::get('form_failed_message');
                $jobform->save();

                
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/recruitment/forms/view-form')->withSuccess($success_msg);
    }


    /**
     *  View page for Job form
     *  @Sandeep on 12 Feb 2019
    **/
    public function index()
    {
        $loginInfo              = get_loggedin_user_data();
        $jobForm                = [];
        $arr_jobs               = get_all_jobs();
        $jobForm['arr_jobs']    = add_blank_option($arr_jobs, 'Select Job');

        $data = array(
            'page_title'    => trans('language.view_forms'),
            'redirect_url'  => url('admin-panel/recruitment/forms/view-form'),
            'login_info'    => $loginInfo,
            'jobForm'       => $jobForm
        );
        return view('admin-panel.job_form.index')->with($data);
    }


    /**
     *  Get Data for view page(Datatables)
     *  @Sandeep on 12 Feb 2019
    **/
    public function anyData(Request $request)
    {
        // p($request->all());
        $loginInfo = get_loggedin_user_data();
        $jobForm = JobForm::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('job_id')) && $request->get('job_id') != null)
            {
                $query->where('job_id', "=", $request->get('job_id'));
            }
            
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('form_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('job_form_id', 'DESC')->with('GetJob')->get();
        
        return Datatables::of($jobForm)
       
        ->addColumn('job_name', function ($jobForm)
        {
           return $jobForm->GetJob['job_name'];
        })  
        ->addColumn('action', function ($jobForm)
        {
            $encrypted_job_form_id = get_encrypted_value($jobForm->job_form_id, true);
            if($jobForm->form_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Manage Fields" href="manage-fields/' . $encrypted_job_form_id . '" "">Manage Fields</a>
                    </li>
                    </ul>
                </div>
                <div class="pull-left"><a href="'.url('admin-panel/recruitment/forms/form-status/'.$status.'/' . $encrypted_job_form_id .'').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/recruitment/forms/add-form/' . $encrypted_job_form_id.'').'"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/recruitment/forms/delete-job-form/' . $encrypted_job_form_id .'').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }


    /**
     *  Destroy job form's data
     *  @Sandeep on 12 Feb 2019
    **/
    public function destroy($id)
    {
        $job_form_id = get_decrypted_value($id, true);
        $jobForm    = JobForm::find($job_form_id);
        $success_msg = $error_message =  "";
        if ($jobForm)
        {
            DB::beginTransaction();
            try
            {
                $jobForm->delete();
                $success_msg = "Form deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/recruitment/forms/view-form')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/recruitment/forms/view-form')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Form not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change job form's status
     *  @Sandeep on 12 Feb 2019
    **/
    public function changeStatus($status,$id)
    {
        $job_form_id = get_decrypted_value($id, true);
        $jobForm    = JobForm::find($job_form_id);
        
        if ($jobForm)
        {
            $jobForm->form_status  = $status;
            $jobForm->save();
            $success_msg = "Form status updated successfully!";
            return redirect('admin-panel/recruitment/forms/view-form')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Form not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for manage fields
     *  @Sandeep on 12 Feb 2019
    **/

    public function manageFieldsView($id){
        $loginInfo  = get_loggedin_user_data();
        $job_form_id = get_decrypted_value($id, true);
        $jobForm    = JobForm::find($job_form_id);
        if ($jobForm)
        {
            $arr_jobs            = get_all_jobs();
            $arr_staff_fields    = \Config::get('custom.staff_fields');
            $jobForm['arr_jobs'] = add_blank_option($arr_jobs, 'Select Job');
            $jobForm['arr_staff_fields'] = $arr_staff_fields;
            
            $fields = JobFields::where(array('job_form_id'=> $job_form_id))->get()->toArray();
            if(!empty($fields[0])){
                if($fields[0]['form_keys'] != ""){
                    $form_keys = explode(',', $fields[0]['form_keys']);
                    $fields[0]['form_keys'] = $form_keys;
                }
                $jobForm['fields'] = $fields[0];
                $save_url             = url('admin-panel/recruitment/forms/save-manage-fields/' . get_encrypted_value($fields[0]['job_form_field_id'],true));
            } else {
                $jobForm['fields'] = [];
                $save_url             = url('admin-panel/recruitment/forms/save-manage-fields');
            }
            $submit_button        = 'Update';
            $data = array(
                'page_title'    => trans('language.manage_form'),
                'redirect_url'  => url('admin-panel/recruitment/forms/view-form'),
                'login_info'    => $loginInfo,
                'jobForm'       => $jobForm,
                'save_url'      => $save_url,
                'submit_button' => $submit_button
            );

            return view('admin-panel.job_form.manage-fields')->with($data);
        }  else {
            $error_message = "Form not found!";
            return redirect('admin-panel/recruitment/forms/view-form')->withErrors($error_message);
        }
    }


    /**
     *  Save job form's fields
     *  @Sandeep on 12 Feb 2019
    **/
    public function saveManageFields(Request $request, $id = NULL)
    {
        $loginInfo                      = get_loggedin_user_data();
        $decrypted_job_form_id          = get_decrypted_value($id, true);
        $fields = null;
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $fields = JobFields::find($decrypted_job_form_id);
            $admin_id = $fields['admin_id'];
            if (!$fields)
            {
                return redirect('/admin-panel/recruitment/forms/add-form/')->withError('Form not found!');
            }
            $success_msg = 'Form fields updated successfully!';
        }
        else
        {
            $fields     = New JobFields;
            $success_msg = 'Form fields saved successfully!';
            $decrypted_job_form_id = null;
        }
        DB::beginTransaction();
        try
        {
            if($request->get('fields') != ""){
                $form_keys = implode(",",Input::get('fields'));
            }
            $fields->admin_id           = $admin_id;
            $fields->update_by          = $loginInfo['admin_id'];
            $fields->job_form_id        = Input::get('job_form_id');
            $fields->form_keys          = $form_keys;
            $fields->save();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();

        return redirect()->back()->withSuccess($success_msg);
    }


    /**
     *  View page for recruitment
     *  @Sandeep on 12 Feb 2019
    **/
   
    public function recruitment_forms()
    {
        $loginInfo                  = get_loggedin_user_data();
        $listData                   = [];
        $arr_jobs                   = get_all_jobs();
        $listData['arr_jobs']       = add_blank_option($arr_jobs, 'Select Job');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/recruitment/job/view-job'),
            'page_title'    => trans('language.recruitment_form'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.job_form.recruitment_forms')->with($data);
    }

    /**
     *  Get Data for view job recruitment page(Datatables)
     *  @Sandeep on 12 Feb 2019
    **/
    public function recruitment_forms_date(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $jobForm = JobForm::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('job_id')) && $request->get('job_id') != null)
            {
                $query->where('job_id', "=", $request->get('job_id'));
            }
            
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('form_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('job_form_id', 'DESC')->with('GetJob')->with('GetJob.get_medium')
        ->with(['GetJob.get_remaining_jobs'=>function($q) use ($request) {
            $q->where('status_type', "=", 2);
        }])
        ->get();
        
        return Datatables::of($jobForm)
       
        ->addColumn('job_name', function ($jobForm)
        {
           return $jobForm->GetJob['job_name'];
        })
        ->addColumn('job_no_of_vacancy', function ($jobForm)
        {
           return $jobForm->GetJob['job_no_of_vacancy'];
        })  
        ->addColumn('job_type', function ($jobForm)
        {
            if($jobForm->GetJob['job_type'] == 0) {
                $job_type = 'Temporary ('.$jobForm->GetJob['job_durations'].')';
            } else {
                $job_type = 'Permanent';
            }
            return $job_type;
        })
        ->addColumn('medium_type', function ($jobForm)
        {
            return $jobForm->GetJob['get_medium']['medium_type'];
        })
        ->addColumn('remaining_vacancy', function ($jobForm)
        {   
            return ' '.$jobForm->GetJob['job_no_of_vacancy'] - count($jobForm->GetJob['get_remaining_jobs']).' ';
        })
        ->addColumn('action', function ($jobForm)
        {
            $encrypted_job_form_id = get_encrypted_value($jobForm->job_form_id, true);
            $encrypted_job_id = get_encrypted_value($jobForm->GetJob['job_id'], true);
            $url = url('/');

            if(count($jobForm->GetJob['get_remaining_jobs']) >= $jobForm->GetJob['job_no_of_vacancy']){
                return '
                <button type="button" class="btn btn-raised btn-primary recruitment_form disabled">
                View Form </button>';
            } else {
                return '
                <a href="'.$url.'/recruitment-form/'.$encrypted_job_id.'/'.$encrypted_job_form_id.'"> <button type="button" class="btn btn-raised btn-primary recruitment_form '.$disabled.'">
                View Form </button> </a>';
            }

        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    
    /**
     *  View page for Applied candidates
     *  @Sandeep on 14 Feb 2019
    **/
   
    public function applied_candidates()
    {
        $loginInfo                  = get_loggedin_user_data();
        $listData                   = [];
        $arr_jobs                   = get_all_jobs();
        $listData['arr_jobs']       = add_blank_option($arr_jobs, 'Select Job');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/recruitment/applied-candidates'),
            'save_url'      => url('admin-panel/recruitment/schedule-approved-disaaproved-save'),
            'page_title'    => trans('language.applied_candidates'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.job_form.applied_candidates')->with($data);
    }

    /**
     *  Get Data for view Applied candidates page(Datatables)
     *  @Sandeep on 14 Feb 2019
    **/
    public function applied_candidates_data(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $applied_candidates = RecruitmentForm::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('job_id')) && $request->get('job_id') != null)
            {
                $query->where('job_id', "=", $request->get('job_id'));
            }
            
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('staff_name', "like", "%{$request->get('name')}%");
            }

            if (!empty($request) && !empty($request->get('email')))
            {
                $query->where('staff_email', "like", "%{$request->get('email')}%");
            }

            if (!empty($request) && !empty($request->get('contact_number')))
            {
                $query->where('staff_mobile_number', "like", "%{$request->get('contact_number')}%");
            }
        })->where('status_type',0)->orwhere('status_type',1)->orderBy('applied_cadidate_id', 'DESC')->with('GetJob')->with('GetJob.get_medium')->get();

        //p($applied_candidates);
        
        return Datatables::of($applied_candidates)
       
        ->addColumn('job_name', function ($applied_candidates)
        {
           return $applied_candidates->GetJob['job_name'];
        })
        ->addColumn('staff_name', function ($applied_candidates)
        {
            if($applied_candidates->staff_name == "" ){
                return "------";
            } else {
                return $applied_candidates->staff_name;
            }
        })
        
        ->addColumn('staff_email', function ($applied_candidates)
        {
           if($applied_candidates->staff_email == "" ){
                return "------";
            } else {
                return $applied_candidates->staff_email;
            }
        })
        
        ->addColumn('staff_mobile_number', function ($applied_candidates)
        {
           if($applied_candidates->staff_mobile_number == "" ){
                return "------";
            } else {
                return $applied_candidates->staff_mobile_number;
            }
        })

        ->addColumn('resume', function ($applied_candidates)
        {
            if (!empty($applied_candidates->staff_resume)) {
                $document_file = check_file_exist($applied_candidates->staff_resume, 'recruitment_form');
                if (!empty($document_file))
                {
                    $staff_resume = '<a href='.url($document_file).'  target="_blank" >View Resume</a>';
                } 
            } else {
                $staff_resume = 'No Document Found';
            }

            return $staff_resume;
        })

        ->addColumn('status', function ($applied_candidates)
        {
           if($applied_candidates->status_type == 0 ){
                return "Not Scheduled";
            } else {
                return "Scheduled";
            }
        })

        ->addColumn('action', function ($applied_candidates)
        {
            $encrypted_job_form_id = get_encrypted_value($applied_candidates->job_form_id, true);
            $encrypted_applied_cadidate_id = get_encrypted_value($applied_candidates->applied_cadidate_id, true);
            $encrypted_job_id = get_encrypted_value($applied_candidates->GetJob['job_id'], true);
            $url = url('/');

            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Schedule" class="schedule" href="#" job-id="'.$applied_candidates->GetJob['job_id'].'" job-form-id="'.$applied_candidates->job_form_id.'" applied-candidates-id="'.$applied_candidates->applied_cadidate_id.'" >Schedule</a>
                    </li>
                    <li>
                        <a title="View Details" class="" href="view-details/'.$encrypted_applied_cadidate_id.'"" > View Details </a>
                    </li>
                    </ul>
                </div>';

        })->rawColumns(['action' => 'action','resume' => 'resume'])->addIndexColumn()->make(true);
    }


// <li>
//                         <a title="Approve" class="approve" href="#" job-id="'.$applied_candidates->GetJob['job_id'].'" job-form-id="'.$applied_candidates->job_form_id.'" applied-candidates-id="'.$applied_candidates->applied_cadidate_id.'" >Approve</a>
//                     </li>
//                     <li>
//                         <a title="Disapprove" class="disapprove" href="#" job-id="'.$applied_candidates->GetJob['job_id'].'" job-form-id="'.$applied_candidates->job_form_id.'" applied-candidates-id="'.$applied_candidates->applied_cadidate_id.'" >Disapprove</a>
//                     </li> 

    public function schedule_approved_disaaproved(Request $request)
    {   

        $loginInfo       = get_loggedin_user_data();
        $listData        = [];
        $current_date    = date('Y-m-d');
        $recruitment     = [];

        // p($request->all());
        $recruitment     = RecruitmentForm::where([ 'job_id' => $request->job_id , 'job_form_id' => $request->job_form_id, 'applied_cadidate_id' => $request->applied_candidate_id  ])->first(); 

        $recruitment_date_time = explode(' ', $recruitment->datetime);
        //p($recruitment_date_time);

        if($recruitment->status_type == 1){
            $scheduled_msg = $recruitment->message; 
        } else {
            $scheduled_msg = "Dear #CandidateName, you applied for #jobname in #schoolname, your interview is scheduled on #datetime, we request you to be on time with all the Education and work experience related documents."; 
        }

        if($recruitment->status_type == 2){
            $approved_msg = $recruitment->message;
        } else {
            $approved_msg = "Dear #CandidateName, you are selected for #jobname in #schoolname, Please contact the Name in school before Date for joining the school."; 
        }

        if($recruitment->status_type == 3){
            $disapproved_msg = $recruitment->message;
        } else {
            $disapproved_msg = "Dear #CandidateName, we are sorry your application for the #jobname is not approved, we will contact you for any other openings."; 
        }

        if($recruitment->form_message_via == 0){
            $scheduled     = "checked";
        }else if($recruitment->form_message_via == 1){
            $approved     = "checked";
        }else if($recruitment->form_message_via == 2){
            $disapproved     = "checked";
        } else {
            $disapproved     = "checked";
        }

        
        if($request->type == 1){
            $table = "
                <div class='row clearfix'>
                    <div class='col-lg-6 col-md-6 col-sm-6'>
                        <lable class='from_one1'> Date :</lable>
                        <div class='form-group'>
                            <input type='date' name='date' class='form-control' min='$current_date' 'placeholder' = 'Date', 'id' = 'date' value='$recruitment_date_time[0]'>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6'>
                        <lable class='from_one1'> Time :</lable>
                        <div class='form-group'>
                            <input type='time' name='time' class='form-control' 'placeholder' = 'Time', 'id' = 'time' value='$recruitment_date_time[1]'>
                        </div>
                    </div>
                </div>
                <div class='row clearfix'>
                    <div class='col-lg-12 col-md-12 col-sm-12 text_area_desc'>
                        <lable class='from_one1'>Message :</lable>
                        <div class='form-group'>
                            <textarea rows='4' name='message' class='form-control no-resize' placeholder=''>$scheduled_msg</textarea>
                        </div>
                    </div>
                </div>
                <div class='row clearfix'>
                    <div class='col-lg-12 col-md-12 col-sm-12'>
                        <lable class='from_one1'>Send Via :</lable>
                        <div class='form-group'>
                            <div class='radio' style='margin:10px 4px !important;'>
                                <input type='radio' name='form_message_via' id='radio1$request->applied_candidate_id' $scheduled value='0'>
                                <label for='radio1$request->applied_candidate_id' class='document_staff'>Email</label>
                                <input type='radio' name='form_message_via' id='radio2$request->applied_candidate_id' $approved value='1'>
                                <label for='radio2$request->applied_candidate_id' class='document_staff'>SMS</label>
                                <input type='radio' name='form_message_via' id='radio3$request->applied_candidate_id' $disapproved value='2'>
                                <label for='radio3$request->applied_candidate_id' class='document_staff'>Both</label>
                            </div>
                        </div>
                    </div>
                </div>
            ";
        }


        if($request->type == 2){
            $table = "
                <div class='row clearfix'>
                    <div class='col-lg-12 col-md-12 col-sm-12 text_area_desc'>
                        <lable class='from_one1'>Message :</lable>
                        <div class='form-group'>
                            <textarea rows='4' name='message' value='' class='form-control no-resize' placeholder=''>$approved_msg</textarea>
                        </div>
                    </div>
                </div>
                <div class='row clearfix'>
                    <div class='col-lg-12 col-md-12 col-sm-12'>
                        <lable class='from_one1'>Send Via :</lable>
                        <div class='form-group'>
                            <div class='radio' style='margin:10px 4px !important;'>
                                <input type='radio' name='form_message_via' id='radio4$request->applied_candidate_id' $scheduled value='0'>
                                <label for='radio4$request->applied_candidate_id' class='document_staff'>Email</label>
                                <input type='radio' name='form_message_via' id='radio5$request->applied_candidate_id' $approved value='1'>
                                <label for='radio5$request->applied_candidate_id' class='document_staff'>SMS</label>
                                <input type='radio' name='form_message_via' id='radio6$request->applied_candidate_id' $disapproved value='2'>
                                <label for='radio6$request->applied_candidate_id' class='document_staff'>Both</label>
                            </div>
                        </div>
                    </div>
                </div>
            ";
        }

        if($request->type == 3){
            $table = "
                <div class='row clearfix'>
                    <div class='col-lg-12 col-md-12 col-sm-12 text_area_desc'>
                        <lable class='from_one1'>Message :</lable>
                        <div class='form-group'>
                            <textarea rows='4' name='message' value='' class='form-control no-resize' placeholder=''>$disapproved_msg</textarea>
                        </div>
                    </div>
                </div>
                <div class='row clearfix'>
                    <div class='col-lg-12 col-md-12 col-sm-12'>
                        <lable class='from_one1'>Send Via :</lable>
                        <div class='form-group'>
                            <div class='radio' style='margin:10px 4px !important;'>
                                <input type='radio' name='form_message_via' id='radio7$request->applied_candidate_id' $scheduled value='0'>
                                <label for='radio7$request->applied_candidate_id' class='document_staff'>Email</label>
                                <input type='radio' name='form_message_via' id='radio8$request->applied_candidate_id' $approved value='1'>
                                <label for='radio8$request->applied_candidate_id' class='document_staff'>SMS</label>
                                <input type='radio' name='form_message_via' id='radio9$request->applied_candidate_id' $disapproved value='2'>
                                <label for='radio9$request->applied_candidate_id' class='document_staff'>Both</label>
                            </div>
                        </div>
                    </div>
                </div>
            ";
        }

        $table .= 
        "
        <input type='hidden' name='job_id' value='$request->job_id'>
        <input type='hidden' name='job_form_id' value='$request->job_form_id'>
        <input type='hidden' name='applied_cadidate_id' value='$request->applied_candidate_id'>
        <input type='hidden' name='status_type' value='$request->type'>
        ";                          

        return response()->json(['status'=>1001,'data'=>$table]);
    }


    public function schedule_approved_disaaproved_save(Request $request)
    {
//        p($request['time']);

        $loginInfo          = get_loggedin_user_data();
        $admin_id           = $loginInfo['admin_id'];


        $arr_input_fields = [
            'message'               => 'required',
            'form_message_via'      => 'required',
            'status_type'           => 'required',
        ];

        $validatior = Validator::make($request->all(), $arr_input_fields);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try
            {

                $recruitment   = RecruitmentForm::Find($request['applied_cadidate_id']); 
                
                $recruitment->message               = $request['message'];
                $recruitment->datetime              = $request['date'].' '.$request['time'];
                $recruitment->form_message_via      = $request['form_message_via'];
                $recruitment->status_type           = $request['status_type'];
                $recruitment->save();
                

                $recuriment_form   = RecruitmentForm::Find($request['applied_cadidate_id']);
                $_REQUEST['name']  = $recuriment_form->staff_name;
                $_REQUEST['email'] = $recuriment_form->staff_email;
                $mobile_no         = $recuriment_form->staff_mobile_number;
                

                if($request['status_type'] == 1){
                    $data=['name'=> $recuriment_form->staff_name ,'rec_message'=> $request->message, 'date'=> date('d M Y',strtotime($request['date'])),'time'=> date("g:i a", strtotime($request['time'])) ];
                    $message           = $request['message'].''.'    Date : '.date('d M Y',strtotime($request['date'])).''.'  Time : '.date("g:i a", strtotime($request['time']));
                    $success_msg = "Scheduled successfully.";
                } else if($request['status_type'] == 2){
                    $data=['name'=> $recuriment_form->staff_name ,'rec_message'=> $request->message ];
                    $message           = $request['message'];
                    $success_msg = "Approved successfully.";
                } else if($request['status_type'] == 3){
                    $data=['name'=> $recuriment_form->staff_name ,'rec_message'=> $request->message ];
                    $message           = $request['message'];
                    $success_msg = "Disapproved successfully.";
                }


                if($request['form_message_via'] == 0){

                    if($_REQUEST['email'] != ""){
                        Mail::send(['html' => 'mails/recuriment_mail'], $data , function ($message) {
                          $message->to($_REQUEST['email'], $_REQUEST['name'])->subject('Recruitment');
                        });
                    }
                }

                if($request['form_message_via'] == 1){
                    $bulkSms           = send_sms_with_parameters($mobile_no, $message);
                }

                if($request['form_message_via'] == 2){
                    
                    if($_REQUEST['email'] != ""){
                        Mail::send(['html' => 'mails/recuriment_mail'], $data , function ($message) {
                          $message->to($_REQUEST['email'], $_REQUEST['name'])->subject('Recruitment');
                        });
                    }

                    $bulkSms           = send_sms_with_parameters($mobile_no, $message);

                }

            }

            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            } 
        }
            
        DB::commit();
        return redirect()->back()->withSuccess($success_msg);

    }
    

    /**
     *  View page for Selected candidates
     *  @Sandeep on 15 Feb 2019
    **/
   
    public function selected_candidates()
    {
        $loginInfo                  = get_loggedin_user_data();
        $listData                   = [];
        $arr_jobs                   = get_all_jobs();
        $listData['arr_jobs']       = add_blank_option($arr_jobs, 'Select Job');
        $data = array(
            'login_info'    => $loginInfo,
            'page_title'    => trans('language.selected_candidates'),
            'save_url'      => url('admin-panel/recruitment/schedule-approved-disaaproved-save'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.job_form.selected_candidates')->with($data);
    }

    /**
     *  Get Data for view Selected candidates page(Datatables)
     *  @Sandeep on 15 Feb 2019
    **/
    public function selected_candidates_data(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $selected_candidates = RecruitmentForm::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('job_id')) && $request->get('job_id') != null)
            {
                $query->where('job_applied_cadidates.job_id', "=", $request->get('job_id'));
            }
            
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('job_applied_cadidates.staff_name', "like", "%{$request->get('name')}%");
            }

            if (!empty($request) && !empty($request->get('email')))
            {
                $query->where('job_applied_cadidates.staff_email', "like", "%{$request->get('email')}%");
            }

            if (!empty($request) && !empty($request->get('contact_number')))
            {
                $query->where('job_applied_cadidates.staff_mobile_number', "like", "%{$request->get('contact_number')}%");
            }
        })->where('status_type','!=',0)->where('datetime','<',date('Y-m-d'))->orderBy('job_applied_cadidates.applied_cadidate_id', 'DESC')->with('GetJob')->with('GetJob.get_medium')->get();
        
        return Datatables::of($selected_candidates)
       
        ->addColumn('job_name', function ($selected_candidates)
        {
           return $selected_candidates->GetJob['job_name'];
        })
        ->addColumn('staff_name', function ($selected_candidates)
        {
            if($selected_candidates->staff_name == "" ){
                return "------";
            } else {
                return $selected_candidates->staff_name;
            }
        })
        
        ->addColumn('staff_email', function ($selected_candidates)
        {
           if($selected_candidates->staff_email == "" ){
                return "------";
            } else {
                return $selected_candidates->staff_email;
            }
        })
        
        ->addColumn('staff_mobile_number', function ($selected_candidates)
        {
           if($selected_candidates->staff_mobile_number == "" ){
                return "------";
            } else {
                return $selected_candidates->staff_mobile_number;
            }
        })

        ->addColumn('resume', function ($selected_candidates)
        {
            if (!empty($selected_candidates->staff_resume)) {
                $document_file = check_file_exist($selected_candidates->staff_resume, 'recruitment_form');

                if (!empty($document_file))
                {
                    $staff_resume = '<a href='.url($document_file).'  target="_blank" >View Resume</a>';
                } 
            } else {
                $staff_resume = 'No Document Found';
            }

            return $staff_resume;
        })

        ->addColumn('status', function ($selected_candidates)
        {
            if($selected_candidates->status_type == 1 ){
                return "Scheduled";
            } else if($selected_candidates->status_type == 2 ){
                return "Approved";
            } else if($selected_candidates->status_type == 3 ){
                return "Disapproved";
            }
        })


         ->addColumn('action', function ($selected_candidates)
        {
            $encrypted_job_form_id = get_encrypted_value($selected_candidates->job_form_id, true);
            $encrypted_applied_cadidate_id = get_encrypted_value($selected_candidates->applied_cadidate_id, true);
            $encrypted_job_id = get_encrypted_value($selected_candidates->job_id, true);
            $url = url('/');

            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Approve" class="approve" href="#" job-id="'.$selected_candidates->job_id.'" job-form-id="'.$selected_candidates->job_form_id.'" applied-candidates-id="'.$selected_candidates->applied_cadidate_id.'" >Approve</a>
                    </li>
                    <li>
                        <a title="Disapprove" class="disapprove" href="#" job-id="'.$selected_candidates->job_id.'" job-form-id="'.$selected_candidates->job_form_id.'" applied-candidates-id="'.$selected_candidates->applied_cadidate_id.'" >Disapprove</a>
                    </li> 
                    <li>
                        <a title="View Details" class="schedule" href="view-details/'.$encrypted_applied_cadidate_id.'"" > View Details </a>
                    </li>
                    </ul>
                </div>';
        })

        ->rawColumns(['action' => 'action','resume' => 'resume'])->addIndexColumn()->make(true);
    }


    /**
     *  View page for reporting
     *  @Sandeep on 15 Feb 2019
    **/
   
    public function reporting()
    {
        $loginInfo                  = get_loggedin_user_data();
        $listData                   = [];
        $arr_medium                 = get_all_mediums();
        $listData['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/recruitment/job/view-job'),
            'page_title'    => trans('language.recruitment_report'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.job_form.reporting')->with($data);
    }

    /**
     *  Get Data for view reporting page(Datatables)
     *  @Sandeep on 15 Feb 2019
    **/
    public function reporting_data(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $job  = Job::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null && $request->get('medium_type') != "Select Medium")
            {
                $query->where('job_medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('job_name')))
            {
                $query->where('job_name', "like", "%{$request->get('job_name')}%");
            }

        })
        ->with('get_medium')
        ->withCount('get_remaining_jobs')
        ->withCount(['approved_jobs' => function($query) { $query->where('status_type', '=', 2); }])
        ->withCount(['disapproved_jobs' => function($query) { $query->where('status_type', '=', 3); }])
        ->orderBy('job_id', 'DESC')->get();

        return Datatables::of($job)
        ->addColumn('job_type', function ($job)
        {
            if($job->job_type == 0) {
                $job_type = 'Temporary ('.$job->job_durations.')';
            } else {
                $job_type = 'Permanent';
            }
            return $job_type;
        })
        ->addColumn('medium_type', function ($job)
        {
            return $job['get_medium']->medium_type;
        })

        ->addColumn('total_approved', function ($job)
        {
            return $job['approved_jobs_count'];
        })

        ->addColumn('total_disapproved', function ($job)
        {
            return $job['disapproved_jobs_count'];
        })

        ->addColumn('total_applied', function ($job)
        {
            return $job['get_remaining_jobs_count'];
        })

        ->addColumn('job_info', function ($job)
        {
            return '<button type="button" class="btn btn-raised btn-primary recruitment"  job-id="'.$job['job_id'].'" >
            Recruitment
            </button> <br/><br/>  
            <button type="button" class="btn btn-raised btn-primary description" style="color:white;" job-id="'.$job['job_id'].'" >
            Description
            </button>
            ';
        })
        
        ->addColumn('action', function ($job)
        {
            $encrypted_job_id = get_encrypted_value($job->job_id, true);
            if($job->job_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="job-status/'.$status.'/' . $encrypted_job_id . '">'.$statusVal.'</a></div>

                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-job/' . $encrypted_job_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-job/' . $encrypted_job_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>
            ';
        })->rawColumns(['action' => 'action','job_info'=> 'job_info'])->addIndexColumn()->make(true);
    }



    /**
     *  View Candidate detail page
     *  @Sandeep on 16 Feb 2019
    **/
    public function canditate_deatils($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                      = get_loggedin_user_data();
            $decrypted_applied_cadidate_id  = get_decrypted_value($id, true);
            $config_upload_path             = \Config::get('custom.recruitment_form');
            //$applied_candidate              = RecruitmentForm::where('applied_cadidate_id' , $decrypted_applied_cadidate_id )->with('GetJob')->with('GetJob.get_medium')->first();

            $applied_candidate                    = [];
            $applied_candidate                    = get_candidate_signle_data($decrypted_applied_cadidate_id);
            $applied_candidate                    = isset($applied_candidate[0]) ? $applied_candidate[0] : [];


            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.candidate_details'),
                'applied_candidate'     => $applied_candidate,
                'config_upload_path'    => $config_upload_path['display_path']
            );
            return view('admin-panel.job_form.candidate_details')->with($data);
        } else {
            return redirect('admin-panel/recruitment'); 
        }
    }







}
