<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\StaffRoles\StaffRoles;

use Yajra\Datatables\Datatables;

class StaffRolesController extends Controller
{
    /**
     *  View page for staff roles
     *  @Shree on 2 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'   => trans('language.view_staff_role'),
            'redirect_url' => url('admin-panel/staff/view-staff-role'),
            'login_info'   => $loginInfo
        );
        return view('admin-panel.staff_roles.index')->with($data);
    }

    /**
     *  Add page for Staff Role
     *  @Shree on 2 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    = [];
        $staff_role = [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_staff_role_id    = get_decrypted_value($id, true);
            $staff_role                 = StaffRoles::Find($decrypted_staff_role_id);
            if (!$staff_role)
            {
                return redirect('admin-panel/staff-role/add-staff-role')->withError('Staff role not found!');
            }
            $page_title                 = trans('language.edit_staff_role');
            $decrypted_staff_role_id    = get_encrypted_value($staff_role->staff_role_id, true);
            $save_url                   = url('admin-panel/staff-role/save/' . $decrypted_staff_role_id);
            $submit_button              = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_staff_role');
            $save_url      = url('admin-panel/staff-role/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'staff_role'    => $staff_role,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff-role/view-staff-roles'),
        );
        return view('admin-panel.staff_roles.add')->with($data);
    }

    /**
     *  Add and update staff role's data
     *  @Shree on 2 Aug 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo                  = get_loggedin_user_data();
        $decrypted_staff_role_id    = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $staff_role = StaffRoles::find($decrypted_staff_role_id);

            if (!$staff_role)
            {
                return redirect('/admin-panel/staff-role/add-staff-role/')->withError('Staff role not found!');
            }
            $success_msg = 'Staff role updated successfully!';
        }
        else
        {
            $staff_role     = New StaffRoles;
            $success_msg = 'Staff role saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'staff_role_name'   => 'required|unique:staff_roles,staff_role_name,' . $decrypted_staff_role_id . ',staff_role_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $staff_role->admin_id              = $loginInfo['admin_id'];
                $staff_role->update_by             = $loginInfo['admin_id'];
                $staff_role->staff_role_name       = Input::get('staff_role_name');
                $staff_role->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin-panel/staff-role/view-staff-roles')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 2 Aug 2018
    **/
    public function anyData()
    {
        $loginInfo = get_loggedin_user_data();
        $staff_role = StaffRoles::orderBy('staff_role_name', 'ASC')->get();
        return Datatables::of($staff_role)  
        ->addColumn('action', function ($staff_role)
        {
            $encrypted_staff_role_id = get_encrypted_value($staff_role->staff_role_id, true);
            if($staff_role->staff_role_status == 0) {
                $status = 1;
                $statusVal = "Deactive";
            } else {
                $status = 0;
                $statusVal = "Active";
            }
            if($staff_role->staff_role_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="staff-role-status/'.$status.'/' . $encrypted_staff_role_id . '">'.$statusVal.'</a></div>

            ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

     /**
     *  Destroy staff role's data
     *  @Shree on 2 Aug 2018
    **/
    public function destroy($id)
    {
        $staff_role_id = get_decrypted_value($id, true);
        $staff_role    = StaffRoles::find($staff_role_id);
        if ($staff_role)
        {
            $holiday->delete();
            $success_msg = "Staff role deleted successfully!";
            return redirect('admin-panel/staff-role/view-staff-roles')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Staff role not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change staff role's status
     *  @Shree on 2 Aug 2018
    **/
    public function changeStatus($status,$id)
    {
        $staff_role_id = get_decrypted_value($id, true);
        $staff_role    = StaffRoles::find($staff_role_id);
        if ($staff_role)
        {
            $staff_role->staff_role_status  = $status;
            $staff_role->save();
            $success_msg = "Staff role status update successfully!";
            return redirect('admin-panel/staff/manage-staff-roles')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Staff role not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
