<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Section\Section;
use App\Model\Student\Student;
use App\Model\Student\StudentParent;
use App\Model\Student\StudentAcademic;
use App\Model\Student\StudentHealth;
use App\Model\Student\StudentDocument;
use App\Model\Student\ImprestAc; // Model
use App\Model\Student\WalletAc; // Model
use App\Model\Student\StudentAttendance; // Model
use App\Model\Student\StudentAttendanceDetails; // Model
use App\Model\HomeworkGroup\HomeworkGroup; // Model
use App\Model\HomeworkGroup\HomeworkConv; // Model
use App\Model\Exam\ExamScheduleMap; // Model
use App\Model\Address\Country;
use App\Model\Address\State;
use App\Model\Address\City;

use App\Model\FeesCollection\OneTime; //OneTime Model
use App\Model\FeesCollection\ConcessionMap; //ConcessionMap Model
use App\Model\FeesCollection\RecurringHead; //RecurringHead Model
use App\Model\FeesCollection\Receipt; //Receipt Model

use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use Excel;


class StudentController extends Controller
{
    /**
     *  View page for students
     *  @Shree on 24 July 2018
    **/
   
    public function index()
    {
        $loginInfo                  = get_loggedin_user_data();
        $arr_medium                 = get_all_mediums();
        $arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/student/view-list'),
            'page_title'    => trans('language.view_student_list'),
            'listData'      => $listData,
        );
        return view('admin-panel.student.index')->with($data);
    }

    /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function view_students($sectionid = NULL)
    {
        if(!empty($sectionid)) {
            $loginInfo                                  = get_loggedin_user_data();
            $decrypted_section_id                       = get_decrypted_value($sectionid, true);
            $arr_class_section_info                     = get_class_section_info($decrypted_section_id);
            $arr_section                                = get_class_section();
            $studentListData                            = [];
            $studentListData['arr_class_section_info']  = $arr_class_section_info;
            $studentListData['section_id']              = $decrypted_section_id;
            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.view_students'),
                'studentListData'       => $studentListData,
            );
            return view('admin-panel.student.view_students')->with($data);
        } else {
            return redirect('admin-panel/student/view-list'); 
        }
    }

    /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function parent_information()
    {
        $loginInfo      = get_loggedin_user_data();
        $data = array(
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/student/parent-information'),
            'page_title'        => trans('language.parent_information'),
        );
        return view('admin-panel.student.parent_information')->with($data);
        
    }

    /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function student_profile($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                  = get_loggedin_user_data();
            $decrypted_student_id       = get_decrypted_value($id, true);
            $updated_student_seesion    = get_current_student_session();
            $student                    = [];
            $student                    = get_student_signle_data($decrypted_student_id);
            $student                    = isset($student[0]) ? $student[0] : [];
            $parent_info                = parent_info($loginInfo['admin_id']);
            $parent_student             = get_parent_student($parent_info['student_parent_id']);
            
            $student_id                 = $student['student_id'];
            $class_id                   = $student['current_class_id'];

            //p($student);

            $upcoming_fees_arr = [];
            $fees_data      = api_get_complete_fees_student($student_id,$class_id);
            $paid_fees_data = api_get_complete_paid_fees_student($student_id,$class_id);
            $due_fees_data  = api_get_complete_due_fees_student($student_id,$class_id);
            $fees_total     = api_get_fees_total_student($student_id,$class_id);
            $fees_summary   = api_get_fees_total_student($student_id,$class_id);

            if( !empty($fees_data) ){
                
                foreach ($fees_data as $fees_data_res) {
                    if( $fees_data_res['head_expire'] !=1 ){
                        $upcoming_fees_arr[] = array(
                            'fees_name'         => $fees_data_res['fees_name'],
                            'head_id'           => $fees_data_res['head_id'],
                            'head_name'         => $fees_data_res['head_name'],
                            'head_amt'          => $fees_data_res['head_amt'],
                            'head_type'         => $fees_data_res['head_type'],
                            'effective_date'    => $fees_data_res['head_date'],
                        );
                    }
                }
            }

            $student_fees_info = array(
                'fees_summary'  => $fees_summary,
                'upcoming_fees' => $upcoming_fees_arr,
                'paid_fees'     => $paid_fees_data,
                'due_fees'      => $due_fees_data,
            );

            $session     = get_current_session();

        //    p($loginInfo);

            $scheduleMap = ExamScheduleMap::where(function($query) use ($request, $loginInfo,$session) 
            {
                if (!empty($loginInfo['class_id']))
                {
                    $query->where('class_id', "=", $loginInfo['class_id']);
                }
                if (!empty($loginInfo['section_id']))
                {
                    $query->where('section_id', "=", $loginInfo['section_id']);
                }
                if (!empty($session['session']))
                {
                    $query->where('session', "=", $session['session']);
                }

            })
            ->with('getClass')
            ->with('getSection')
            ->with('getExam')
            ->with('getExam.getTerm')
            ->orderBy('class_id','ASC')
            ->groupBy('exam_id')
            ->get()
            ->toArray();

            foreach ($scheduleMap as $map) {
                $map['subject_name'] = $map['get_subject_data']['subject_name'].' - '.$map['get_subject_data']['subject_code'];
                $map['exam_date'] = date("d M Y",strtotime($map['exam_date']));
                $map['time_range'] = $map['exam_time_from'].' - '.$map['exam_time_to'];
                $all_staffs = [];
                if(!empty($map['staff_ids'])){
                    $all_staffs = get_multiple_staff(explode(',', $map['staff_ids']));
                }
                $map['teachers'] = implode(',',$all_staffs);

                $get_class_date =  DB::select("SELECT MIN(exam_date) as start_date, MAX(exam_date) as end_date FROM `schedule_map` WHERE class_id = ".$map['class_id']." ");
                $get_class_date = isset($get_class_date[0]) ? $get_class_date[0] : [];

                $map['class_name'] = $map['get_class']['class_name'];
                $map['section_name'] = $map['get_section']['section_name'];
                $map['start_date'] = $get_class_date->start_date;
                $map['end_date'] = $get_class_date->end_date;
            
                unset($map['get_class']);
                unset($map['get_section']);
                unset($map['get_subject_data']);
                $mapData[] = $map;
            }


            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.student_profile'),
                'student'               => $student,
                'exam_marks'            => $mapData,
                'parent_student'        => $parent_student,
                'student_fees_info'     => $student_fees_info
            );
           // p($student);
            return view('admin-panel.student.student_profile')->with($data);
        } else {
            return redirect('admin-panel/student/view-list'); 
        }
    }

     /**
     *  Add page for student
     *  @Shree on 24 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $student        = [];
        $data           = [];
        $arr_city_p     = [];
        $arr_city_t     = [];
        $arr_state_p    = [];
        $arr_state_t    = [];
        $arr_class      = [];
        $arr_stream     = [];
        $class_id = null;
        $admit_class_id = null;
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_student_id = get_decrypted_value($id, true);
            $student              = get_student_signle_data($decrypted_student_id);
            $student              = isset($student[0]) ? $student[0] : [];
            $admit_class_id       = $student['admission_class_id'];
            $class_id             = $student['current_class_id'];
            $arr_state_p          = get_all_states($student['student_permanent_county']);
            $arr_city_p           = get_all_city($student['student_permanent_state']);
            $arr_state_t          = get_all_states($student['student_permanent_county']);
            $arr_city_t           = get_all_city($student['student_permanent_state']);
            $arr_class            = get_all_classes($student['medium_type']);
            $arr_stream           = get_all_streams($student['medium_type']);
            
            if (!$student)
            {
                return redirect('admin-panel/add-student')->withError('Student not found!');
            }
            $encrypted_student_id = get_encrypted_value($student['student_id'], true);
            $page_title           = 'Edit Student';
            $save_url             = url('admin-panel/student/save/' . $encrypted_student_id);
            $submit_button        = 'Update';
        }
        else
        {
            
            $page_title    = trans('language.add_student');
            $save_url      = url('admin-panel/student/save');
            $submit_button = 'Save';
        }
        $student['arr_session'] = [];
        $arr_medium             = get_all_mediums();
        $arr_student_type       = \Config::get('custom.student_type');
        $arr_gender             = \Config::get('custom.student_gender');
        $arr_category           = \Config::get('custom.student_category');
        $arr_annual_salary      = \Config::get('custom.annual_salary');

        $arr_country                   = get_all_country();
        $arr_title                     = get_titles();
        $arr_caste                     = get_caste();
        $arr_religion                  = get_religion();
        $arr_natioanlity               = get_nationality();
        $admit_arr_section             = get_class_section($admit_class_id);
        $arr_section                   = get_class_section($class_id);
        $arr_session                   = get_arr_session();
        $arr_group                     = get_student_groups();
        $arr_document_category         = get_all_document_category(0);
        
        $student['arr_gender']         = $arr_gender;
        $student['arr_category']       = $arr_category;
        $student['arr_annual_salary']  = $arr_annual_salary;
        $student['arr_student_type']   = $arr_student_type;
        $student['arr_title']          = add_blank_option($arr_title, 'Select Title');
        $student['arr_country']        = add_blank_option($arr_country, 'Select Country');
        $student['arr_state_p']        = add_blank_option($arr_state_p, 'Select State');
        $student['arr_city_p']         = add_blank_option($arr_city_p, 'Select City');
        $student['arr_state_t']        = add_blank_option($arr_state_t, 'Select State');
        $student['arr_city_t']         = add_blank_option($arr_city_t, 'Select City');
        $student['arr_medium']         = add_blank_option($arr_medium, 'Select Medium');
        $student['arr_session']        = add_blank_option($arr_session, 'Select session');
        $student['arr_class']          = add_blank_option($arr_class, 'Select class');
        $student['arr_stream']         = add_blank_option($arr_stream, 'Select Stream');
        $student['arr_caste']          = add_blank_option($arr_caste, 'Select caste');
        $student['arr_religion']       = add_blank_option($arr_religion, 'Select religion');
        $student['arr_section']        = add_blank_option($arr_section, 'Select section');
        $student['admit_arr_section']  = add_blank_option($admit_arr_section, 'Select section');
        $student['arr_natioanlity']    = add_blank_option($arr_natioanlity, 'Select nationality');
        $student['arr_group']          = add_blank_option($arr_group, 'Select house/group');
        $student['arr_document_category']   = add_blank_option($arr_document_category, 'Document category');
        
        // p($student);
        if (!empty($student['student_image']))
        {
            $profile = check_file_exist($student['student_image'], 'student_image');
            if (!empty($profile))
            {
                $student['profile'] = $profile;
            }
        }
        // p($student);
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'student'       => $student,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/studebt/student-list'),
        );
        return view('admin-panel.student.add')->with($data);
    }

    /**
     *  Save Student Data
     *  @Shree on 27 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $student_id           = null;
        $decrypted_student_id = null;
        $student_parent_id = null;
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $decrypted_student_id   = get_decrypted_value($id, true);
            $student                = Student::find($decrypted_student_id);
            $student_parent         = StudentParent::Find($student->student_parent_id);
            $parentAdmin            = Admin::Find($student_parent->reference_admin_id);
            $studentAdmin           = Admin::Find($student->reference_admin_id);
            $student_academic       = StudentAcademic::where('student_id', $student->student_id)->first();
            $student_health         = StudentHealth::where('student_id', $student->student_id)->first();
            $imprestAc              = new ImprestAc();
            $walletAc               = new WalletAc();
            $admin_id = $student['admin_id'];
            $student_parent_id = $student['student_parent_id'];
            if (!$student)
            {
                return redirect('/admin-panel/student/add-student')->withError('Student not found!');
            }
            $success_msg = 'Student updated successfully!';
        }
        else
        {
            $student            = New Student;
            $studentAdmin       = new Admin();
            $student_parent     = new StudentParent();
            $student_academic   = new StudentAcademic();
            $student_health     = new StudentHealth();
            $imprestAc          = new ImprestAc();
            $walletAc           = new WalletAc();
            
            $success_msg        = 'Student saved successfully!';
        }
        
        $arr_input_fields = [
            'medium_type'               => 'required',
            'student_enroll_number'     => 'required|unique:students,student_enroll_number,' . $decrypted_student_id . ',student_id',
            'student_name'              => 'required',
            // 'student_email'             => 'required|unique:students,student_email,' . $decrypted_student_id . ',student_id',
            'student_reg_date'          => 'required',
            'student_dob'               => 'required',
            'student_category'          => 'required',
            'student_type'              => 'required',
            'title_id'                  => 'required',
            'caste_id'                  => 'required',
            'religion_id'               => 'required',
            'nationality_id'            => 'required',
            'student_permanent_address' => 'required',
            'student_permanent_city'    => 'required',
            'student_permanent_state'   => 'required',
            'student_permanent_county'  => 'required',
            'student_permanent_pincode' => 'required',
            'admission_session_id'      => 'required',
            'admission_class_id'        => 'required',
            'admission_section_id'      => 'required',
            'current_session_id'        => 'required',
            'current_class_id'          => 'required',
            'current_section_id'        => 'required',
            // 'student_blood_group'       => 'required',
            // 'medical_issues'            => 'required',
            'student_father_name'       => 'required',
            'student_father_mobile_number' => 'required',
            'student_father_occupation'    => 'required',
            'student_mother_name'          => 'required',
            // 'student_mother_mobile_number' => 'required',
            // 'student_privious_school'      => 'required',
            // 'student_privious_class'       => 'required',
            // 'student_privious_tc_no'       => 'required',
            // 'student_privious_tc_date'     => 'required',
            // 'student_privious_result'      => 'required',
            'student_login_name'           => 'required',
            //'student_login_email'          => 'required|unique:student_parents,student_login_email|unique:staff,staff_email|unique:students,student_email,' . $student_parent_id . ',student_parent_id',
            'student_login_contact_no'     => 'required',
        ];
        $validatior = Validator::make($request->all(), $arr_input_fields);
       
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction(); //Start transaction!

            try
            {
                //inserting data in multiples tables
                $email = Input::get('student_login_email');
                $mobile_no = Input::get('student_login_contact_no');
                if($request->get('update_contact_no_status') == 1){
                    $parentAdmin->mobile_no = $mobile_no;
                    $parentAdmin->save();
                }

                $parentAdmin = check_parents_exist($mobile_no);
               // p($parentAdmin);
                if(empty($parentAdmin)) {
                    $arr_input_fields = [
                        'student_login_contact_no' => 'required|unique:admins,mobile_no',
                       // 'student_login_email' => 'required|unique:admins,email',
                    ];
                    $validatior = Validator::make($request->all(), $arr_input_fields);
                    
                    if ($validatior->fails())
                    {
                        return redirect()->back()->withInput()->withErrors($validatior);
                    }
                    $parentAdmin        = new Admin();
                    $parentAdmin->admin_name = Input::get('student_father_name');
                    $parentAdmin->email = $email;
                    $parentAdmin->mobile_no = $mobile_no;
                    $parentPassword = 'parent@123!';
                    $parentAdmin->admin_type = 3;
                    $parentAdmin->password = Hash::make($parentPassword);
                    $parentAdmin->save();
                    $reference_admin_id = $parentAdmin->admin_id;
                    // $message = "";
                    // $subject = "Welcome to ".trans('language.project_title')." | Parent Panel";;
                    // $emailData = [];
                    // $emailData['sender'] = trans('language.email_sender');
                    // $emailData['email'] = $email;
                    // $emailData['password'] = $parentPassword;
                    // $emailData['receiver'] = Input::get('student_father_name');
                    // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                    // {
                    //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                    
                    // });
                } else {
                    $student_parent       = StudentParent::Find($parentAdmin->student_parent_id);
                    $reference_admin_id   = $student_parent->reference_admin_id;
                }
                
                if (!empty($parentAdmin->admin_id))
                {
                    if (empty($id))
                    {

                        if($request->get('student_email') != ''){
                            $arr_input_fields = [
                                'student_email' => 'required|unique:admins,email',
                                'student_enroll_number' => 'required|unique:admins,enroll_no',
                            ];
                        } else {
                            $arr_input_fields = [
                                // 'staff_email'   => 'unique:admins,email',
                                'student_enroll_number'   => 'required|unique:admins,enroll_no',
                            ];
                        }
                        $validatior = Validator::make($request->all(), $arr_input_fields);
                        if ($validatior->fails())
                        {
                            return redirect()->back()->withInput()->withErrors($validatior);
                        }
                        $studentAdmin->admin_name = Input::get('student_name');
                        $studentAdmin->email = Input::get('student_email');
                        $studentAdmin->enroll_no = Input::get('student_enroll_number');
                        $studentPassword = 'student@123!';
                        $studentAdmin->admin_type = 4;
                        $studentAdmin->password = Hash::make($studentPassword);
                        $studentAdmin->save();
                        // $message = "";
                        // $subject = "Welcome to ".trans('language.project_title')." | Student Panel";
                        // $emailData = [];
                        // $emailData['sender'] = trans('language.email_sender');
                        // $emailData['email'] = Input::get('student_email');
                        // $emailData['password'] = $studentPassword;
                        // $emailData['receiver'] = Input::get('student_name');
                        // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                        // {
                        //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                        
                        // });
                    }


                    $studentAdmin->email = Input::get('student_email');
                    $studentAdmin->enroll_no = Input::get('student_enroll_number');
                    $studentAdmin->save();
                    // student parent table data
                    $student_parent->admin_id                        = $admin_id;
                    $student_parent->update_by                       = $loginInfo['admin_id'];
                    $student_parent->reference_admin_id              = $reference_admin_id;
                    $student_parent->student_login_name              = Input::get('student_login_name');
                    $student_parent->student_login_email             = Input::get('student_login_email');
                    $student_parent->student_login_contact_no        = Input::get('student_login_contact_no');
                    $student_parent->student_father_name             = Input::get('student_father_name');
                    $student_parent->student_father_mobile_number    = Input::get('student_father_mobile_number');
                    $student_parent->student_father_email            = Input::get('student_father_email');
                    $student_parent->student_father_occupation       = Input::get('student_father_occupation');
                    $student_parent->student_mother_name             = Input::get('student_mother_name');
                    $student_parent->student_mother_mobile_number    = Input::get('student_mother_mobile_number');
                    $student_parent->student_mother_email            = Input::get('student_mother_email');
                    $student_parent->student_mother_occupation       = Input::get('student_mother_occupation');
                    $student_parent->student_mother_tongue           = Input::get('student_mother_tongue');
                    $student_parent->student_guardian_name           = Input::get('student_guardian_name');
                    $student_parent->student_guardian_email          = Input::get('student_guardian_email');
                    $student_parent->student_guardian_mobile_number  = Input::get('student_guardian_mobile_number');
                    $student_parent->student_father_annual_salary    = Input::get('student_father_annual_salary');
                    $student_parent->student_mother_annual_salary    = Input::get('student_mother_annual_salary');
                    $student_parent->student_guardian_relation    = Input::get('student_guardian_relation');
                    $student_parent->save();
                    // p($student_parent);
                    // student table data
                    $student->admin_id                      = $admin_id;
                    $student->update_by                     = $loginInfo['admin_id'];
                    $student->reference_admin_id            = $studentAdmin->admin_id;
                    $student->student_parent_id             = $student_parent->student_parent_id;
                    $student->student_enroll_number         = Input::get('student_enroll_number');
                    $student->student_type                  = Input::get('student_type');
                    $student->medium_type                   = Input::get('medium_type');
                    $student->student_email                 = Input::get('student_email');
                    $student->student_reg_date              = Input::get('student_reg_date');
                    $student->student_name                  = Input::get('student_name');
                    $student->student_gender                = Input::get('student_gender');
                    $student->student_dob                   = Input::get('student_dob');
                    $student->student_adhar_card_number     = Input::get('student_adhar_card_number');
                    $student->student_category              = Input::get('student_category');
                    $student->title_id                      = Input::get('title_id');
                    $student->caste_id                      = Input::get('caste_id');
                    $student->religion_id                   = Input::get('religion_id');
                    $student->nationality_id                = Input::get('nationality_id');
                    // $student->student_sibling_name          = Input::get('student_sibling_name');
                    // $student->student_sibling_class_id      = Input::get('student_sibling_class_id');
                    $student->student_temporary_address     = Input::get('student_temporary_address');
                    $student->student_temporary_city        = Input::get('student_temporary_city');
                    $student->student_temporary_state       = Input::get('student_temporary_state');
                    $student->student_temporary_county      = Input::get('student_temporary_county');
                    $student->student_temporary_pincode     = Input::get('student_temporary_pincode');
                    $student->student_permanent_address     = Input::get('student_permanent_address');
                    $student->student_permanent_city        = Input::get('student_permanent_city');
                    $student->student_permanent_state       = Input::get('student_permanent_state');
                    $student->student_permanent_county      = Input::get('student_permanent_county');
                    $student->student_permanent_pincode     = Input::get('student_permanent_pincode');
                    
                    $student->student_privious_school       = Input::get('student_privious_school');
                    $student->student_privious_class        = Input::get('student_privious_class');
                    $student->student_privious_tc_no        = Input::get('student_privious_tc_no');
                    $student->student_privious_tc_date      = Input::get('student_privious_tc_date');
                    $student->student_privious_result       = Input::get('student_privious_result');
                   
                    if ($request->hasFile('student_image'))
                    {
                        if (!empty($id)){
                            $profile = check_file_exist($student->student_image, 'student_image');
                            if (!empty($profile))
                            {
                                unlink($profile);
                            } 
                        }
                        $file                          = $request->file('student_image');
                        $config_upload_path            = \Config::get('custom.student_image');
                        $destinationPath               = public_path() . $config_upload_path['upload_path'];
                        $ext                           = substr($file->getClientOriginalName(),-4);
                        $name                          = substr($file->getClientOriginalName(),0,-4);
                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                        $file->move($destinationPath, $filename);
                        $student->student_image = $filename;
                    }

                    $student->save();
                    // student academic table data
                    $student_academic->admin_id               = $admin_id;
                    $student_academic->update_by              = $loginInfo['admin_id'];
                    $student_academic->student_id             = $student->student_id;
                    $student_academic->admission_session_id   = Input::get('admission_session_id');
                    $student_academic->admission_class_id     = Input::get('admission_class_id');
                    $student_academic->admission_section_id   = Input::get('admission_section_id');
                    $student_academic->current_session_id     = Input::get('current_session_id');
                    $student_academic->current_class_id       = Input::get('current_class_id');
                    $student_academic->current_section_id     = Input::get('current_section_id');
                    $student_academic->student_unique_id      = Input::get('student_enroll_number').'_'.$student->student_id;
                    $student_academic->group_id               = Input::get('group_id');
                    $student_academic->stream_id              = Input::get('stream_id');
                    $student_academic->save();
                    // student health table data
                    $student_health->admin_id               = $admin_id;
                    $student_health->update_by              = $loginInfo['admin_id'];
                    $student_health->student_id             = $student->student_id;
                    $student_health->student_height         = Input::get('student_height');
                    $student_health->student_weight         = Input::get('student_weight');
                    $student_health->student_blood_group    = Input::get('student_blood_group');
                    $student_health->student_vision_left    = Input::get('student_vision_left');
                    $student_health->student_vision_right   = Input::get('student_vision_right');
                    $student_health->medical_issues         = Input::get('medical_issues');
                    $student_health->save();
                    $document_list = [];

                    // Student Imprest Account
                    $checkImprestAcExistance = check_account_existance('Imprest',$student->student_id);
                    if($checkImprestAcExistance == 0) {
                        $amt = get_imprest_ac_config_amt();
                        $imprestAc->admin_id               = $admin_id;
                        $imprestAc->update_by              = $loginInfo['admin_id'];
                        $imprestAc->session_id             = Input::get('current_session_id');
                        $imprestAc->class_id               = Input::get('current_class_id');
                        $imprestAc->student_id             = $student->student_id;
                        $imprestAc->imprest_ac_name        = Input::get('student_name');
                        $imprestAc->imprest_ac_amt         = $amt;
                        $imprestAc->save();
                    }

                    // Student Wallet Account
                    $checkWalletAcExistance = check_account_existance('Wallet',$student->student_id);
                    if($checkWalletAcExistance == 0) {
                        $walletAc->admin_id                = $admin_id;
                        $walletAc->update_by               = $loginInfo['admin_id'];
                        $walletAc->session_id              = Input::get('current_session_id');
                        $walletAc->class_id                = Input::get('current_class_id');
                        $walletAc->student_id              = $student->student_id;
                        $walletAc->wallet_ac_name          = Input::get('student_name');
                        $walletAc->wallet_ac_amt           = 0;
                        $walletAc->save();
                    }
                    $documentKey = 0;
                    foreach($request->get('documents') as $documents){ 
                        $fileData = $request->file('documents');
                        if(isset($documents['document_category_id']) && !empty($documents['document_category_id'])) {
                            if(isset($documents['student_document_id']) ) {
                                $student_document_update       = StudentDocument::where('student_document_id', $documents['student_document_id'])->first();
                                $student_document_update->admin_id                 = $loginInfo['admin_id'];
                                $student_document_update->update_by                = $loginInfo['admin_id'];
                                $student_document_update->student_id               = $student->student_id;
                                $student_document_update->document_category_id     = $documents['document_category_id'];
                                $student_document_update->student_document_details = $documents['student_document_details'];
                                if (isset($fileData[$documentKey]['student_document_file']))
                                {
                                    if (!empty($student_document_update->student_document_file)){
                                        $student_document_file = check_file_exist($student_document_update->student_document_file, 'student_document_file');
                                        if (!empty($student_document_file))
                                        {
                                            unlink($student_document_file);
                                        } 
                                    }
                                    $file                          = $fileData[$documentKey]['student_document_file'];
                                    $config_document_upload_path   = \Config::get('custom.student_document_file');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $student_document_update->student_document_file = $filename;
                                }
                                
                                $student_document_update->save();
                            } else {
                                $student_document   = new StudentDocument();
                                $student_document->admin_id                 = $admin_id;
                                $student_document->update_by                = $loginInfo['admin_id'];
                                $student_document->student_id               = $student->student_id;
                                $student_document->document_category_id     = $documents['document_category_id'];
                                $student_document->student_document_details = $documents['student_document_details'];
                                if (isset($fileData[$documentKey]['student_document_file']))
                                {
                                    $file                          = $fileData[$documentKey]['student_document_file'];
                                    $config_document_upload_path   = \Config::get('custom.student_document_file');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $student_document->student_document_file = $filename;
                                }
                                $student_document->save();
                                $document_list[] = $student_document;
                            }
                        $documentKey++;
                        }
                    }
                    
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/student/view-list')->withSuccess($success_msg);
            
    }

    /**
     *  Change Student's status
     *  @Shree on 31 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $student_id = get_decrypted_value($id, true);
        $student    = Student::find($student_id);
        if ($student)
        {
            $student->student_status  = $status;
            $student->save();
            $success_msg = "student status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy student data
     *  @Shree on 31 July 2018
    **/
    public function destroy($id)
    {
        $student_id = get_decrypted_value($id, true);
        $student    = Student::find($student_id)->with('getParent')->get();
       
        if (!empty($student[0]['student_image']))
        {
            $profile = check_file_exist($student[0]['student_image'], 'student_image');
            if (!empty($profile))
            {
                $student[0]['profile'] = $profile;
                if(file_exists($profile)){
                    @unlink($profile);
                }
            }
        }
        $checkSibling = check_sibling($student[0]['getParent']['student_parent_id'],$student_id);
        if ($student)
        {
            if($checkSibling == 'false'){
                $parentAdmin    = Admin::Find($student[0]['getParent']['reference_admin_id']);
                $parentAdmin->delete();
            }
            $studentAdmin         = Admin::Find($student[0]['reference_admin_id']);
            $studentAdmin->delete();
            $success_msg = "Student deleted successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get sections data according class
     *  @Shree on 27 July 2018
    **/
    public function getSectionData()
    {
        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.class-teacher-allocation.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get Data for view list page(Datatables)
     *  @Shree on 28 July 2018
    **/
    public function listData(Request $request)
    {
        $classSectionData     = [];
        $arr_class_section = $this->getListData("",$request);
        foreach ($arr_class_section as $key => $arr_class_section_data)
        {
            $classSectionData[$key] = (object) $arr_class_section_data;
        }
        
        return Datatables::of($classSectionData)
            // ->addColumn('medium_type', function ($classSectionData)
            // {
            //     $arr_medium   = get_all_mediums();
            //     $medium_type  = $arr_medium[$classSectionData->medium_type];
            //     return $medium_type;
            // })
            ->addColumn('action', function ($classSectionData)
            {
                $encrypted_section_id = get_encrypted_value($classSectionData->section_id, true);
                
                return ' 
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="View Students" href="view-students/' . $encrypted_section_id . '" "">View Students</a>
                    </li>
                    <li>
                        <a title="Assign Roll Numbers" onclick="return confirm('."'Are you sure?'".')" href="assign-roll-no/' . $encrypted_section_id . '" "">Assign Roll Numbers</a>
                    </li>
                    </ul>
                </div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Get list data
     *  @Shree on 28 July 2018
    **/
    public function getListData($data = array(),$request)
    {
        $loginInfo = get_loggedin_user_data();
        $data_return   = [];
        $data          = [];
        
        $arr_medium             = get_all_mediums();
        $arr_class_section_data = Section::where(function($query) use ($loginInfo,$request) 
            {
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null && $request->get('section_id') != "Select section")
                {
                    $query->where('section_id', "=", $request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null &&  $request->get('class_id') != "Select class")
                {
                    $query->where('class_id', "=", $request->get('class_id'));
                }
            })->with('sectionClass')->with('sectionClass.getBoard')->get();
        if (!empty($arr_class_section_data))
        {
            foreach ($arr_class_section_data as $key => $class_section_data)
            {
                $totalStudents = 0;
                
                $totalStudents = StudentAcademic::where(function($query) use ($loginInfo,$class_section_data) 
                {
                    $query->where('current_class_id', "=", $class_section_data['class_id']);
                    $query->where('current_section_id', "=", $class_section_data['section_id']);
                   
                })->get()
                ->count();
                $list_arr = array(
                    'section_id'        => $class_section_data['section_id'],
                    'section_name'      => $class_section_data['section_name'],
                    'section_order'     => $class_section_data['section_order'],
                    'section_status'    => $class_section_data['section_status'],
                    'class_id'          => $class_section_data['class_id'],
                    'total_students'    => $totalStudents,
                );
                if (isset($class_section_data['sectionClass']['class_name']))
                {
                    $list_arr['class_name'] = $class_section_data['sectionClass']['class_name'].' - '.$arr_medium[$class_section_data['sectionClass']['medium_type']].' - '.$class_section_data['sectionClass']['getBoard']['board_name'];
                }
                $data_return[] = $list_arr;
            }
            
        }
        return $data_return;
    }
    /**
     *  Get Student List Data for view student page(Datatables)
     *  @Shree on 28 July 2018
    **/
    public function studentListData(Request $request)
    {
        
        $student     = [];
        $arr_students = get_student_list($request);
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('action', function ($student)
            {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                if($student->student_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Leave"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Study"><i class="fas fa-plus-circle"></i></div>';
                }
                // <li><a href="../delete-student/' . $encrypted_student_id . '" onclick="return confirm('."'Are you sure?'".')" >Delete</a></li>
                return ' 
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a href="'.url('admin-panel/student/student-profile/'.$encrypted_student_id.' ').'" >View Profile</a>
                    </li>
                    <li>
                        <a target="_blank" href="'.url('admin-panel/student/change-parent/'.$encrypted_student_id.' ').'">Change Parent</a>
                    </li>
                 </ul>
             </div>
             
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="../add-student/' . $encrypted_student_id . '"><i class="zmdi zmdi-edit"></i></a></div>
            ';
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile'])->addIndexColumn()->make(true);
    }

    /**
     *  Get Data for parent information page(Datatables)
     *  @Shree on 1 Aug 2018
    **/
    public function parentListData(Request $request)
    {
        $parentData     = [];
        $arr_parent = get_parent_list("",$request);
        foreach ($arr_parent as $key => $arr_parent_data)
        {
            $parentData[$key] = (object) $arr_parent_data;
        }
        return Datatables::of($parentData)
            
            ->addColumn('action', function ($parentData)
            {
                $encrypted_student_parent_id = get_encrypted_value($parentData->student_parent_id, true);
                // view-parent-detail/' . $encrypted_student_parent_id . '" "
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li>
                            <a title="View Parent Detail" href="'.url('/admin-panel/student/view-parent-detail/' . $encrypted_student_parent_id.' ').'" >View Profile</a>
                        </li>
                    </ul>
                </div>';
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

     /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function view_parent_detail($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                      = get_loggedin_user_data();
            $decrypted_student_parent_id    = get_decrypted_value($id, true);
            $parent                         = [];
            $parent                         = get_parent_list($decrypted_student_parent_id,"");
            $parent                         = isset($parent[0]) ? $parent[0] : [];
            $child                          = get_child_list($decrypted_student_parent_id);
            $parent_info                    = parent_info($loginInfo['admin_id']);
            $parent_student                 = get_parent_student($parent_info['student_parent_id']);
            $parent['child_info']           = $child;
            $data = array(
                'login_info'      => $loginInfo,
                'page_title'      => trans('language.view_parent_detail'),
                'parent'          => $parent,
                'parent_student'  => $parent_student,
            );
            // p($parent);
            return view('admin-panel.student.parent_detail')->with($data);
        } else {
            return redirect('admin-panel/student/parent-information'); 
        }
    }

    public function check_parent_existance(Request $request){
        if($request->has('mobile_no') ) {
            $parent_info = check_parents_exist($request->get('mobile_no'));
            if(isset($parent_info['student_parent_id'])) {
                $parent_info['exist_status']  = 1;
            } else {
                $parent_info['exist_status']  = 0;
            }
        } else {
            $parent_info['exist_status']  = 0;
        }
        
        return $parent_info;
    }

    /**
     *  Edit page for edit student document 
     *  @Pratyush on 10 Aug 2018
    **/
    public function edit_document(Request $request, $id = NULL)
    {

        $loginInfo  = get_loggedin_user_data();
        $decrypted_document_id  = get_decrypted_value($id, true);
        $document               = StudentDocument::find($decrypted_document_id);
        if (!empty($id))
        {
            if (!$document)
            {
                return redirect('admin-panel/student/view-students')->withError('Student not found!');
            }
                
            $page_title             = 'Edit Document';
            $save_url               = url('admin-panel/student/save-document/' . $id);
            $submit_button          = 'Update';
        }
        $arr_document_category              = get_all_document_category(2);
        $document['arr_document_category']   = add_blank_option($arr_document_category, 'Document category');

       
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'document'      => $document,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/studebt/student-list'),
        );
        return view('admin-panel.student.edit_document')->with($data);
    }

    /**
     *  Save page for edit student document 
     *  @Pratyush on 10 Aug 2018
    **/
    public function save_document(Request $request){
        // dd($request->hasFile('documents'));
        
        $student_document   = StudentDocument::find($request->get('student_document_id'));
        
            if (!$student_document)
            {
                return redirect('/admin-panel/student/view-students/')->withError('Document not found!');
            }
            $success_msg = 'Document updated successfully!';
            
            
            $previous_doc_details = StudentDocument::where('student_id',$student_document->student_id)->where('document_category_id',Input::get('student_document_category'))->where('student_document_id','!=',$request->get('student_document_id'))->count();

            if($previous_doc_details != 0){
                $encrypted_student_document_id                       = get_encrypted_value($request->get('student_document_id'), true);   
                return redirect('/admin-panel/student/save-document/'.$encrypted_student_document_id)->withError('Document not found!');   
            }

        $validatior = Validator::make($request->all(), [

                'documents'                 => 'mimes:pdf,jpg,jpeg,png',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }else{    

            $encrypted_student_id                       = get_encrypted_value($student_document->student_id, true);   
            $student_document->document_category_id     = Input::get('student_document_category');
            $student_document->student_document_details = Input::get('student_document_details');
            if ($request->hasFile('documents'))
            {
                $file                          = $request->File('documents');
                $config_document_upload_path   = \Config::get('custom.student_document_file');
                $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                $ext                           = substr($file->getClientOriginalName(),-4);
                $name                          = substr($file->getClientOriginalName(),0,-4);
                $filename                      = $name.mt_rand(0,100000).time().$ext;
                $file->move($destinationPath, $filename);
                $student_document->student_document_file = $filename;
            }
            $student_document->save();
            return redirect('admin-panel/student/student-profile/'.$encrypted_student_id)->withSuccess($success_msg);
        }
    }

    /**
     *  Student document's status
     *  @Pratyush on 10 Aug 2018.
    **/
    public function changeDocumentStatus($status,$id)
    {
        $student_document_id    = get_decrypted_value($id, true);
        $student_document       = StudentDocument::find($student_document_id);
        if($student_document){
            $student_document->student_document_status  = $status;
            $student_document->save();
            $success_msg = "Document status updated!";
            $encrypted_student_id = get_encrypted_value($student_document->student_id, true);   
            return redirect('admin-panel/student/student-profile/'.$encrypted_student_id)->withSuccess($success_msg);
        }else{
            $error_message = "Document not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Student Document's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function destroyDocument($id,$type)
    {
        if($type == 1){
            $student_document_id    = $id;
        } else {
            $student_document_id    = get_decrypted_value($id,true);
        }
        $student_document       = StudentDocument::find($student_document_id);
        if($student_document){
            if (!empty($student_document['student_document_file'])){
                $student_document_file = check_file_exist($student_document['student_document_file'], 'student_document_file');
                if (!empty($student_document_file))
                {
                    unlink($student_document_file);
                } 
            }
            $student_document->delete();
            $success_msg = "Document deleted successfully!";
            if($type == 1){
                p($success_msg);
            } 
            
            $encrypted_student_id = get_encrypted_value($student_document->student_id, true);   
            return redirect('admin-panel/student/student-profile/'.$encrypted_student_id)->withSuccess($success_msg);
        }else{
            $error_message = "Document not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function checkSectionCapacity(){
        $section_id = Input::get('section_id');
        $status = check_section_capacity($section_id);
        return $status;
    }

    public function assign_roll_numbers($sectionid){
        $section_id     = get_decrypted_value($sectionid,true);
        $status         = assign_roll_no_by_section($section_id);
        if($status == "Success") {
            $success_msg = "Roll No assigned";
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Some error occured!!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function show_attendance(){
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/student/attendance'),
            'page_title'    => trans('language.view_attendance'),
            'listData'      => $listData,
        );
        return view('admin-panel.student.show_attendance')->with($data);
    }

    public function attendance_data(Request $request){
       
        $student = [];
        $attendance_mark_status = $this->check_attendance_exist($request);
        $arr_students = get_all_student_for_attendance($request);
        $holiday_list = get_school_all_holidays(1);
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student,$attendance_mark_status,$holiday_list) 
            ->addColumn('student_profile', function ($student)
            {
                $profile = '';
                if($student->profile != ''){
                    $profile = "<img src=".url($student->profile)." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('student_enroll_number', function ($student)
            {
                $student_enroll_number = "";
                $student_enroll_number = $student->student_enroll_number;
                return $student_enroll_number;
            })
            ->addColumn('attendance', function ($student) use ($attendance_mark_status,$holiday_list,$request)
            {
                if(in_array($request->get('attendance_date'),$holiday_list)){
                    return "Holiday";
                }
                if($attendance_mark_status == 0 && $student->student_attend_d_id == NULL){
                    return "Pending";
                }
                if($attendance_mark_status == 1 && $student->student_attend_d_id == NULL){
                    return "Not Available";
                }
                $present = $absent = '';
                $present = "checked";
                if($student->student_attendance_type == 1){
                    $present = "checked";
                } else if($student->student_attendance_type == '0' ){
                    $absent = "checked";
                } else if($student->student_attendance_type == ''){
                    $present = "checked";
                }
                $attendance = '
                    <input type="hidden" name="attendance['.$student->student_id.'][student_attend_d_id]"  value="'.$student->student_attend_d_id.'" >

                    <input type="hidden" name="attendance['.$student->student_id.'][student_id]"  value="'.$student->student_id.'" >

                    <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status1'.$student->student_id.'" name="attendance['.$student->student_id.'][attendance]" '.$present.' type="radio" value="1" >
                        <label for="attendance_status1'.$student->student_id.'" class="document_staff">Present</label>
                    </div>
                    <div class="red float-left" style="margin-top:6px !important;">
                        <input id="attendance_status2'.$student->student_id.'" name="attendance['.$student->student_id.'][attendance]" '.$absent.' type="radio" value="0" />
                        <label for="attendance_status2'.$student->student_id.'" class="document_staff">Absent</label> 
                    </div>
                    ';
                return $attendance;
            })
            
            ->rawColumns(['student_profile'=>'student_profile', 'attendance'=> 'attendance', 'student_enroll_number'=> 'student_enroll_number'])->addIndexColumn()->make(true);
    }

    public function check_attendance_exist(Request $request){
        $attendance_mark_status = 0;
        $check_status = StudentAttendance::where(function($query) use ($request) 
            {
                if (!empty($request) && !empty($request->get('attendance_date')) && $request->get('attendance_date') != null){
                    $query->where('student_attendance_date',$request->get('attendance_date'));
                }
            })
            ->get()->toArray();
        if(!empty($check_status)){
            $attendance_mark_status = 1;
        }
        return $attendance_mark_status;
    }

    public function change_parent($student_id){
        $student = [];
        $decrypted_student_id = get_decrypted_value($student_id, true);
        $loginInfo = get_loggedin_user_data();
        $student = Student::where(function($query) use ($decrypted_student_id) 
        {
            $query->where('student_id',$decrypted_student_id);
        })
        ->get();
        $student = isset($student[0]) ? $student[0] : [];
        $arr_annual_salary      = \Config::get('custom.annual_salary');
        $student['arr_annual_salary']  = $arr_annual_salary;
        $page_title    = trans('language.change_parent');
        $save_url      = url('admin-panel/student/save-parent/'.$student_id);
        $submit_button = 'Save';
       
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'student'       => $student,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/studebt/student-list'),
        );
        return view('admin-panel.student.change_parent')->with($data);
    }

    public function save_parent(Request $request, $student_id){
        $loginInfo = get_loggedin_user_data();
        $decrypted_student_id = get_decrypted_value($student_id, true);
        $admin_id = $loginInfo['admin_id'];
        $student = Student::find($decrypted_student_id);
        DB::beginTransaction(); //Start transaction!
        try
        {
            if(!empty($request->get('student_parent_id')) && $request->get('student_parent_id') != NULL){
                // Update Case
                $student_parent  = StudentParent::Find($request->get('student_parent_id'));
                $reference_admin_id   = $student_parent->reference_admin_id;
            } else {
                // Add Case
                $arr_input_fields = [
                    'student_login_contact_no' => 'required|unique:admins,email',
                ];
                $validatior = Validator::make($request->all(), $arr_input_fields);
                
                if ($validatior->fails())
                {
                    return redirect()->back()->withInput()->withErrors($validatior);
                }
                $parentAdmin        = new Admin();
                $parentAdmin->admin_name = Input::get('student_father_name');
                $parentAdmin->email = Input::get('student_login_contact_no');
                $parentPassword = 'parent@123!';
                $parentAdmin->admin_type = 3;
                $parentAdmin->password = Hash::make($parentPassword);
                $parentAdmin->save();
                $reference_admin_id = $parentAdmin->admin_id;
                $student_parent  = New StudentParent;
            }
            // student parent table data
            $student_parent->admin_id                        = $admin_id;
            $student_parent->update_by                       = $loginInfo['admin_id'];
            $student_parent->reference_admin_id              = $reference_admin_id;
            $student_parent->student_login_name              = Input::get('student_login_name');
            $student_parent->student_login_email             = Input::get('student_login_email');
            $student_parent->student_login_contact_no        = Input::get('student_login_contact_no');
            $student_parent->student_father_name             = Input::get('student_father_name');
            $student_parent->student_father_mobile_number    = Input::get('student_father_mobile_number');
            $student_parent->student_father_email            = Input::get('student_father_email');
            $student_parent->student_father_occupation       = Input::get('student_father_occupation');
            $student_parent->student_mother_name             = Input::get('student_mother_name');
            $student_parent->student_mother_mobile_number    = Input::get('student_mother_mobile_number');
            $student_parent->student_mother_email            = Input::get('student_mother_email');
            $student_parent->student_mother_occupation       = Input::get('student_mother_occupation');
            $student_parent->student_mother_tongue           = Input::get('student_mother_tongue');
            $student_parent->student_guardian_name           = Input::get('student_guardian_name');
            $student_parent->student_guardian_email          = Input::get('student_guardian_email');
            $student_parent->student_guardian_mobile_number  = Input::get('student_guardian_mobile_number');
            $student_parent->student_father_annual_salary    = Input::get('student_father_annual_salary');
            $student_parent->student_mother_annual_salary    = Input::get('student_mother_annual_salary');
            $student_parent->student_guardian_relation    = Input::get('student_guardian_relation');
            $student_parent->save();

            $student->student_parent_id = $student_parent->student_parent_id;
            $student->save();
            $success_msg = "Parent information changed";
            
        }   
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/student/view-list')->withSuccess($success_msg);
                
    }   

    public function homework_group(){
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes_mediums();
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel//student/homework-group'),
            'page_title'    => trans('language.view_homework_group'),
            'listData'      => $listData,
        );
        return view('admin-panel.student.show_homework_group')->with($data);
    }

    public function get_homework_group_data(Request $request){
        // p($request->all());
        $session                = get_current_session();
        $homework = HomeworkGroup::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != NULL)
            {
                $query->where('class_id',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != NULL)
            {
                $query->where('section_id',$request->get('section_id'));
            }

            if (!empty($request) && !empty($request->has('student_class_id')) && $request->get('student_class_id') != NULL)
            {
                $query->where('class_id',$request->get('student_class_id'));
            }

            if (!empty($request) && !empty($request->has('student_section_id')) && $request->get('student_section_id') != NULL)
            {
                $query->where('section_id',$request->get('student_section_id'));
            }
            
            if (!empty($request) && !empty($request->has('staff_id')) && $request->get('staff_id') != NULL)
            {
                $query->where('staff_id',$request->get('staff_id'));
            }

            if (!empty($request) && !empty($request->has('subject_id')) && $request->get('subject_id') != NULL)
            {
                $query->where('subject_id',$request->get('subject_id'));
            }
        })
        ->join('homework_conversations', function($join) use ($session) {
            $join->on('homework_conversations.homework_group_id', '=', 'homework_groups.homework_group_id');
            $join->where('h_session_id', "=", $session['session_id']);
        })->with('getStaff','getSubject')
        ->select('homework_groups.class_id','homework_groups.section_id','homework_groups.homework_group_name','homework_conversations.*')
        ->get();
        // p($homework);
        return Datatables::of($homework) 
            ->addColumn('subject_name', function ($homework)
            {
                $subject_name = $homework['getSubject']->subject_name;
                return $subject_name;
            })
            ->addColumn('staff_name', function ($homework)
            {
                $staff_name = $homework['getStaff']->staff_name;
                return $staff_name;
            })
            ->addColumn('homework', function ($homework)
            {
                $homework = '<button class="btn btn-raised btn-primary homework" h-conversation-id='.$homework->h_conversation_id.'>Homework</button>';
                return $homework;
            })
            ->addColumn('homework_date', function ($homework)
            {
                $homework_date =  date("d F Y", strtotime($homework->created_at));
                return $homework_date;
            })
            
            ->rawColumns(['subject_name'=>'subject_name', 'staff_name'=> 'staff_name','homework'=> 'homework','homework_date'=> 'homework_date'])->addIndexColumn()->make(true);
    }

    public function get_homework_info(Request $request){
        $session                = get_current_session();
        $info = [];
        $homeworkInfo = HomeworkConv::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->has('h_conversation_id')) && $request->get('h_conversation_id') != NULL)
            {
                $query->where('h_conversation_id',$request->get('h_conversation_id'));
            }
            $query->where('h_session_id', "=", $session['session_id']);
        })
        ->with('getGroup','getGroup.getClass','getStaff','getSubject') 
        ->with('getGroup.getSection')
        ->first();
        if(!empty($homeworkInfo)) {
            $attechUrl = '';
            if($homeworkInfo->h_conversation_attechment != ''){
                $attechUrl = "<a href=".url('admin-panel/download-homework-attechment/'.$homeworkInfo->h_conversation_id)." > Download Attechment</a>";
            }
            $info = array(
                'subject_name' => $homeworkInfo['getSubject']->subject_name.' - '.$homeworkInfo['getSubject']->subject_code,
                'staff_name' => $homeworkInfo['getStaff']->staff_name,
                'class_name' => $homeworkInfo['getGroup']['getClass']->class_name,
                'section_name' => $homeworkInfo['getGroup']['getSection']->section_name,
                'h_conversation_text' => $homeworkInfo->h_conversation_text,
                'date' => date("d M Y",strtotime($homeworkInfo->created_at)),
                'attechUrl' => $attechUrl
            );
        }
        return $info;
    }


    /**
     *  Get My Home Work
     *  @Sandeep on 9 Jan 2019
    **/

    public function student_view_homework(Request $request)
    {
        $loginInfo              = get_loggedin_user_data();
        $session                = get_current_session();
        $arr_teacher            = get_all_student_staffs($loginInfo['class_id'],$loginInfo['section_id']);
        $arr_subject            = get_subject_list_class_section($loginInfo['class_id'],$loginInfo['section_id']);
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

        $listData['arr_class_id']    = $loginInfo['class_id'];
        $listData['arr_section_id']  = $loginInfo['section_id'];
        $listData['arr_teacher']     = add_blank_option($arr_teacher, 'Select Teacher');
        $listData['arr_subject']     = add_blank_option($arr_subject, 'Select Subject');

        $data = array(
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/student/view-homework'),
            'page_title'        => trans('language.student_homework'),
            'listData'          => $listData,
            'parent_student'    => $parent_student,
        );

        return view('admin-panel.student.student_homework')->with($data);
    }


    /**
     *  Get My Attendence
     *  @Sandeep on 9 Jan 2019
    **/

    public function student_attendance(){
        $loginInfo              = get_loggedin_user_data();
        $arr_class              = get_all_classes_mediums();
        $arr_section            = [];
        $listData               = [];
        $arr_studentData        = [];
        $session                = get_current_session();
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

       // p($loginInfo);

        $arr_student_list = StudentAttendance::
            leftJoin('student_attend_details', function($join) use ($request,$session){
                $join->on('student_attend_details.student_attendance_id', '=', 'student_attendance.student_attendance_id');
                $join->where('student_attend_details.session_id', '=',$session['session_id']);
            })
            ->where('student_attend_details.student_id', '=',$loginInfo['student_id'])
            ->get();

        foreach ($arr_student_list as $arr_student)
        {

            if($attendance_mark_status == 0 && $arr_student->student_attend_d_id == NULL){
                $arr_student->student_attendance_type = "Holiday";
            }
            if($attendance_mark_status == 1 && $arr_student->student_attend_d_id == NULL){
                $arr_student->student_attendance_type = "Not Available";
            }

            if($arr_student->student_attendance_type == 1){
                $arr_student->student_attendance_type = "Present";
                $attendence_class = "attendence_green";
            } else if($arr_student->student_attendance_type == '0' ){
                $arr_student->student_attendance_type = "Absent";
                $attendence_class = "attendence_red";
            } else if($arr_student->student_attendance_type == ''){
                $arr_student->student_attendance_type = "Present";
                $attendence_class = "attendence_green";
            }

            $arr_studentData[] = array(
                'student_id'                => $arr_student->student_id,
                'student_name'              => $arr_student->student_name,
                'student_enroll_number'     => $arr_student->student_enroll_number,
                'student_attend_d_id'       => $arr_student->student_attend_d_id,
                'student_attendance_id'     => $arr_student->student_attendance_id,
                'student_attendance_type'   => $arr_student->student_attendance_type,
                'attendence_class'          => $attendence_class,
                'student_attendance_date'   => date("Y-m-d", strtotime($arr_student->student_attendance_date)),
            );
        }

        $data = array(
            'login_info'         => $loginInfo,
            'redirect_url'       => url('admin-panel/student/attendance'),
            'page_title'         => trans('language.view_attendance'),
            'listData'           => $listData,
            'parent_student'     => $parent_student,
            'arr_student_list'   => $arr_studentData,
        );

        return view('admin-panel.student.student_attendance')->with($data);
    }


    public function get_student_attendance(Request $request){
        $student = [];
        $attendance_mark_status = $this->check_attendance_exist($request);
        $arr_students = get_attendance_for_student($request);
        
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student) 
            ->addColumn('attendance_date', function ($student)
            {
                $attendance_date =  date("d F Y", strtotime($student->student_attendance_date));
                return $attendance_date;
            })
            ->addColumn('attendance', function ($student) use ($attendance_mark_status)
            {

                if($attendance_mark_status == 0 && $student->student_attend_d_id == NULL){
                    return "Holiday";
                }
                if($attendance_mark_status == 1 && $student->student_attend_d_id == NULL){
                    return "Not Available";
                }
                if($student->student_attendance_type == 1){
                    $attendance = '<i class="zmdi zmdi-circle" style="color: green;"></i> (Present)';
                } else if($student->student_attendance_type == '0' ){
                    $attendance = '<i class="zmdi zmdi-circle" style="color: red;"></i> (Absent)';
                } else if($student->student_attendance_type == ''){
                    $attendance = '<i class="zmdi zmdi-circle" style="color: green;"></i> (Present)';
                }

                return $attendance;
            })
            
            ->rawColumns(['student_profile'=>'student_profile', 'attendance'=> 'attendance', 'student_enroll_number'=> 'student_enroll_number'])->addIndexColumn()->make(true);
    }


     /**
     *  View Single parent details
     *  @Sandeep on 23 Jan 2019
    **/

    public function student_parent_detail($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                      = get_loggedin_user_data();
            $decrypted_student_parent_id    = get_decrypted_value($id, true);
            $parent                         = [];
            $parent                         = get_parent_list($decrypted_student_parent_id,"");
            $parent                         = isset($parent[0]) ? $parent[0] : [];
            $child                          = get_child_list($decrypted_student_parent_id);
            $parent_info                    = parent_info($loginInfo['admin_id']);
            $parent_student                 = get_parent_student($parent_info['student_parent_id']);
            $parent['child_info']           = $child;
            $data = array(
                'login_info'      => $loginInfo,
                'page_title'      => trans('language.view_parent_detail'),
                'parent'          => $parent,
                'parent_student'  => $parent_student,
            );
            // p($parent);
            return view('admin-panel.student.parent_detail')->with($data);
        } else {
            return redirect('admin-panel/student/parent-information'); 
        }
    }


     /**
     *  View Single student details
     *  @Sandeep on 23 Jan 2019
    **/
    public function view_student_profile($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                  = get_loggedin_user_data();
            $decrypted_student_id       = get_decrypted_value($id, true);
            $updated_student_seesion    = get_current_student_session();
            if($updated_student_seesion != ""){ $decrypted_student_id = $updated_student_seesion; }
            $student                    = [];
            $student                    = get_student_signle_data($decrypted_student_id);
            $student                    = isset($student[0]) ? $student[0] : [];
            $parent_info                = parent_info($loginInfo['admin_id']);
            $parent_student             = get_parent_student($parent_info['student_parent_id']);
            
            $session                    = get_current_session();
            $student_id                 = $student['student_id'];
            $class_id                   = $student['current_class_id'];

            //p($student);

            $upcoming_fees_arr = [];
            $fees_data      = api_get_complete_fees_student($student_id,$class_id);
            $paid_fees_data = api_get_complete_paid_fees_student($student_id,$class_id);
            $due_fees_data  = api_get_complete_due_fees_student($student_id,$class_id);
            $fees_total     = api_get_fees_total_student($student_id,$class_id);
            $fees_summary   = api_get_fees_total_student($student_id,$class_id);

            if( !empty($fees_data) ){
                
                foreach ($fees_data as $fees_data_res) {
                    if( $fees_data_res['head_expire'] !=1 ){
                        $upcoming_fees_arr[] = array(
                            'fees_name'         => $fees_data_res['fees_name'],
                            'head_id'           => $fees_data_res['head_id'],
                            'head_name'         => $fees_data_res['head_name'],
                            'head_amt'          => $fees_data_res['head_amt'],
                            'head_type'         => $fees_data_res['head_type'],
                            'effective_date'    => $fees_data_res['head_date'],
                        );
                    }
                }
            }

            $student_fees_info = array(
                'fees_summary'  => $fees_summary,
                'upcoming_fees' => $upcoming_fees_arr,
                'paid_fees'     => $paid_fees_data,
                'due_fees'      => $due_fees_data,
            );

            $all_info = get_complete_fees_student($student_id,$class_id);
           // p($all_info);

            $scheduleMap = ExamScheduleMap::where(function($query) use ($request, $loginInfo,$session) 
            {
                if (!empty($loginInfo['class_id']))
                {
                    $query->where('class_id', "=", $loginInfo['class_id']);
                }
                if (!empty($loginInfo['section_id']))
                {
                    $query->where('section_id', "=", $loginInfo['section_id']);
                }
                if (!empty($session['session']))
                {
                    $query->where('session', "=", $session['session']);
                }

            })
            ->with('getClass')
            ->with('getSection')
            ->with('getExam')
            ->with('getExam.getTerm')
            ->orderBy('class_id','ASC')
            ->groupBy('exam_id')
            ->get()
            ->toArray();

            foreach ($scheduleMap as $map) {
                $map['subject_name'] = $map['get_subject_data']['subject_name'].' - '.$map['get_subject_data']['subject_code'];
                $map['exam_date'] = date("d M Y",strtotime($map['exam_date']));
                $map['time_range'] = $map['exam_time_from'].' - '.$map['exam_time_to'];
                $all_staffs = [];
                if(!empty($map['staff_ids'])){
                    $all_staffs = get_multiple_staff(explode(',', $map['staff_ids']));
                }
                $map['teachers'] = implode(',',$all_staffs);

                $get_class_date =  DB::select("SELECT MIN(exam_date) as start_date, MAX(exam_date) as end_date FROM `schedule_map` WHERE class_id = ".$map['class_id']." ");
                $get_class_date = isset($get_class_date[0]) ? $get_class_date[0] : [];

                $map['class_name'] = $map['get_class']['class_name'];
                $map['section_name'] = $map['get_section']['section_name'];
                $map['start_date'] = $get_class_date->start_date;
                $map['end_date'] = $get_class_date->end_date;
            
                unset($map['get_class']);
                unset($map['get_section']);
                unset($map['get_subject_data']);
                $mapData[] = $map;
            }


            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.student_profile'),
                'student'               => $student,
                'exam_marks'            => $mapData,
                'parent_student'        => $parent_student,
                'student_fees_info'     => $student_fees_info
            );
            
            return view('admin-panel.student.student_profile')->with($data);
        } else {
            return redirect('admin-panel/student/view-list'); 
        }
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function importView(){
        $loginInfo          = get_loggedin_user_data();
        $info['arr_student_type']       = \Config::get('custom.student_type');
        $info['arr_gender']             = \Config::get('custom.student_gender');
        $info['arr_category']       = \Config::get('custom.student_category');
        $info['arr_annual_salary']  = \Config::get('custom.annual_salary');
        $info['arr_mediums']        = get_all_mediums();

        $info['arr_title']          = get_titles();
        $info['arr_caste']          = get_caste();
        $info['arr_religion']       = get_religion();
        $info['arr_natioanlity']    = get_nationality();
        $info['arr_session']        = get_arr_session();
        $info['arr_group']          = get_student_groups();
        $info['arr_sections']       = $this->class_with_sections();

        $arr_class                  = get_all_classes_mediums();
        $listData  = $arr_section  = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']      = add_blank_option($arr_section, 'Select Section');
        
        $save_url      = url('admin-panel/student/student-import-save');
        $data = array(
            'login_info'    => $loginInfo,
            'page_title'    => trans('language.import_students'),
            'info'          => $info,
            'save_url'      => $save_url,
            'listData'      => $listData
        );
        return view('admin-panel.student.import_students')->with($data);
    }

    public function class_with_sections(){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $arr_sections = [];
        $arr_section_list = Section::where('section_status', 1)->orderBy('class_id', 'ASC')->with('sectionClass')->get();
        
        if (!empty($arr_section_list))
        {
            foreach ($arr_section_list as $key => $arr_section)
            {
                $arr_sections[$key]['class_id'] = $arr_section['sectionClass']['class_id'];
                $arr_sections[$key]['class_name'] = $arr_section['sectionClass']['class_name'];
                $arr_sections[$key]['section_id'] = $arr_section['section_id'];
                $arr_sections[$key]['section_name'] = $arr_section['section_name'];
            }
        }
        return $arr_sections;
    }

    public function student_import_save(Request $request){
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        $admin_id = $loginInfo['admin_id'];
        if($request->hasFile('import_file')){

            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $key => $value) {
                    $country_id = $state_id = $city_id = null;
                    // Get Country
                    $country = Country::where('country_name', "LIKE", "%{$value->country}%")->first();
                    if(isset($country['country_id'])) { $country_id = $country['country_id']; }
                    // Get State
                    $state = State::where('state_name', "LIKE", "%{$value->state}%")->first();
                    if(isset($state['state_id'])) { $state_id = $state['state_id']; }
                    // Get City
                    $city = City::where('city_name', "LIKE", "%{$value->city}%")->first();
                    if(isset($city['city_id'])) { $city_id = $city['city_id']; }

                    if($value->medium_typeid != "") {
                        $arr[] = [  
                            'medium_type'          => $value->medium_typeid,
                            'admission_session_id'         => $value->admission_sessionid,
                            'admission_class_id'             => $value->admission_classid,
                            'admission_section_id'             => $value->admission_sectionid,
                            'current_session_id'             => $session['session_id'],
                            'current_class_id'             => $request->get('class_id'),
                            'current_section_id'             => $request->get('section_id'),
                            'student_enroll_number'             => $value->enrollment_nounique,
                            'student_name'             => $value->student_name,
                            'student_type'             => $value->student_typeid,
                            'student_reg_date'             => $value->date_of_reg,
                            'student_dob'             => $value->dob,
                            'student_email'             => $value->email_address,
                            'student_gender'            => $value->gendergirlboy,
                            'student_privious_school'            => $value->school_name,
                            'student_privious_class'            => $value->class,
                            'student_privious_tc_no'            => $value->tc_no,
                            'student_privious_tc_date'            => $value->tc_date,
                            'student_privious_result'            => $value->result,
                            'student_category'            => $value->categoryid,
                            'caste_id'            => $value->casteid,
                            'religion_id'            => $value->religionid,
                            'nationality_id'            => $value->nationalityid,
                            // 'student_sibling_name'            => $value->sibling_name,
                            // 'student_sibling_class_id'            => $value->sibling_classid,
                            'student_adhar_card_number'            => $value->aadhar_card_number,
                            'housegroup'            => $value->housegroupid,
                            'title_id'            => $value->titleid,
                            'student_login_contact_no'            => $value->contact_nouse_for_parent_login,
                            'student_login_name'            => $value->name,
                            'student_login_email'            => $value->emailuse_for_parent_login,
                            'student_father_name'            => $value->father_name,
                            'student_father_mobile_number'            => $value->fathers_mobile_number,
                            'student_father_email'            => $value->fathers_email_address,
                            'student_father_annual_salary'            => $value->fathers_annual_salaryid,
                            'student_father_occupation'            => $value->fathers_occupation,
                            'student_mother_name'            => $value->mother_name,
                            'student_mother_mobile_number'            => $value->mothers_mobile_number,
                            'student_mother_email'            => $value->mothers_email_address,
                            'student_mother_annual_salary'            => $value->mothers_annual_salaryid,
                            'student_mother_tongue'            => $value->mother_tongue,
                            'student_mother_occupation'            => $value->mothers_occupation,
                            'student_guardian_name'            => $value->guardian_name,
                            'student_guardian_mobile_number'            => $value->mobile_number,
                            'student_guardian_email'            => $value->email_address,
                            'student_guardian_relation'            => $value->relation_with_guardian,
                            'student_permanent_address'            => $value->address,
                            'student_permanent_county'            => $country_id,
                            'student_permanent_state'              => $state_id,
                            'student_permanent_city'             => $city_id,
                            'student_permanent_pincode'             => $value->pincode,
                            'student_height'           => $value->height,
                            'student_weight'             => $value->weight,
                            'student_blood_group'             => $value->blood_group,
                            'student_vision_right'             => $value->vision_right,
                            'student_vision_left'             => $value->vision_left,
                            'medical_issues'             => $value->medical_issue,
                        ];
                    }
                }
                if(!empty($arr)){

                    DB::beginTransaction();
                    try
                    {
                        foreach ($arr as $students) {
                            $student_exist = Student::where('student_enroll_number', $students['student_enroll_number'])->get()->toArray();
                            if(COUNT($student_exist) == 0) {
                                $student            = New Student;
                                $studentAdmin       = new Admin();
                                $student_parent     = new StudentParent();
                                $student_academic   = new StudentAcademic();
                                $student_health     = new StudentHealth();
                                $imprestAc          = new ImprestAc();
                                $walletAc           = new WalletAc();
                                
                                //inserting data in multiples tables
                                $email = $students['student_login_email'];
                                $mobile_no = $students['student_login_contact_no'];
                                
                                $parentAdmin = check_parents_exist($mobile_no);
                        
                                if(empty($parentAdmin)) {
                                    $parentAdmin        = new Admin();
                                    $parentAdmin->admin_name = $students['student_father_name'];
                                    $parentAdmin->email = $email;
                                    $parentAdmin->mobile_no = $mobile_no;
                                    $parentPassword = 'parent@123!';
                                    $parentAdmin->admin_type = 3;
                                    $parentAdmin->password = Hash::make($parentPassword);
                                    $parentAdmin->save();
                                    $reference_admin_id = $parentAdmin->admin_id;
                                    // $message = "";
                                    // $subject = "Welcome to ".trans('language.project_title')." | Parent Panel";;
                                    // $emailData = [];
                                    // $emailData['sender'] = trans('language.email_sender');
                                    // $emailData['email'] = $email;
                                    // $emailData['password'] = $parentPassword;
                                    // $emailData['receiver'] = Input::get('student_father_name');
                                    // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                                    // {
                                    //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                                    
                                    // });
                                } else {
                                    $student_parent       = StudentParent::Find($parentAdmin->student_parent_id);
                                    $reference_admin_id   = $student_parent->reference_admin_id;
                                }
                                
                                if (!empty($parentAdmin->admin_id))
                                {
                                    $studentAdmin->admin_name = $students['student_name'];
                                    $studentAdmin->email = $students['student_email'];
                                    $studentAdmin->enroll_no = $students['student_enroll_number'];
                                    $studentPassword = 'student@123!';
                                    $studentAdmin->admin_type = 4;
                                    $studentAdmin->password = Hash::make($studentPassword);
                                    $studentAdmin->save();
                                    // $message = "";
                                    // $subject = "Welcome to ".trans('language.project_title')." | Student Panel";
                                    // $emailData = [];
                                    // $emailData['sender'] = trans('language.email_sender');
                                    // $emailData['email'] = Input::get('student_email');
                                    // $emailData['password'] = $studentPassword;
                                    // $emailData['receiver'] = Input::get('student_name');
                                    // Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                                    // {
                                    //     $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                                    
                                    // });

                                    $studentAdmin->email = $students['student_email'];
                                    $studentAdmin->enroll_no = $students['student_enroll_number'];
                                    $studentAdmin->save();
                                    // student parent table data
                                    $student_parent->admin_id                        = $admin_id;
                                    $student_parent->update_by                       = $loginInfo['admin_id'];
                                    $student_parent->reference_admin_id              = $reference_admin_id;
                                    $student_parent->student_login_name              = $students['student_login_name'];
                                    $student_parent->student_login_email             = $students['student_login_email'];
                                    $student_parent->student_login_contact_no        = $students['student_login_contact_no'];
                                    $student_parent->student_father_name             = $students['student_father_name'];
                                    $student_parent->student_father_mobile_number    = $students['student_father_mobile_number'];
                                    $student_parent->student_father_email            = $students['student_father_email'];
                                    $student_parent->student_father_occupation       = $students['student_father_occupation'];
                                    $student_parent->student_mother_name             = $students['student_mother_name'];
                                    $student_parent->student_mother_mobile_number    = $students['student_mother_mobile_number'];
                                    $student_parent->student_mother_email            = $students['student_mother_email'];
                                    $student_parent->student_mother_occupation       = $students['student_mother_occupation'];
                                    $student_parent->student_mother_tongue           = $students['student_mother_tongue'];
                                    $student_parent->student_guardian_name           = $students['student_guardian_name'];
                                    $student_parent->student_guardian_email          = $students['student_guardian_email'];
                                    $student_parent->student_guardian_mobile_number  = $students['student_guardian_mobile_number'];
                                    $student_parent->student_father_annual_salary    = $students['student_father_annual_salary'];
                                    $student_parent->student_mother_annual_salary    = $students['student_mother_annual_salary'];
                                    $student_parent->student_guardian_relation    = $students['student_guardian_relation'];
                                    $student_parent->save();
                                    // p($student_parent);
                                    // student table data

                                    if($students['student_gender'] == "Boy"){
                                        $staff_gender = 0;
                                    } else if($students['student_gender'] == "Girl"){
                                        $staff_gender = 1;
                                    }
                                    $student->admin_id                      = $admin_id;
                                    $student->update_by                     = $loginInfo['admin_id'];
                                    $student->reference_admin_id            = $studentAdmin->admin_id;
                                    $student->student_parent_id             = $student_parent->student_parent_id;
                                    $student->student_enroll_number         = $students['student_enroll_number'];
                                    $student->student_type                  = $students['student_type'];
                                    $student->medium_type                   = $students['medium_type'];
                                    $student->student_email                 = $students['student_email'];
                                    $student->student_reg_date              = $students['student_reg_date'];
                                    $student->student_name                  = $students['student_name'];
                                    $student->student_gender                = $staff_gender;
                                    $student->student_dob                   = $students['student_dob'];
                                    $student->student_adhar_card_number     = $students['student_adhar_card_number'];
                                    $student->student_category              = $students['student_category'];
                                    $student->title_id                      = $students['title_id'];
                                    $student->caste_id                      = $students['caste_id'];
                                    $student->religion_id                   = $students['religion_id'];
                                    $student->nationality_id                = $students['nationality_id'];
                                    // $student->student_sibling_name          = $students['student_sibling_name'];
                                    // $student->student_sibling_class_id      = $students['student_sibling_class_id'];
                                    $student->student_temporary_address     = $students['student_temporary_address'];
                                    $student->student_temporary_city        = $students['student_temporary_city'];
                                    $student->student_temporary_state       = $students['student_temporary_state'];
                                    $student->student_temporary_county      = $students['student_temporary_county'];
                                    $student->student_temporary_pincode     = $students['student_temporary_pincode'];
                                    $student->student_permanent_address     = $students['student_permanent_address'];
                                    $student->student_permanent_city        = $students['student_permanent_city'];
                                    $student->student_permanent_state       = $students['student_permanent_state'];
                                    $student->student_permanent_county      = $students['student_permanent_county'];
                                    $student->student_permanent_pincode     = $students['student_permanent_pincode'];
                                    
                                    $student->student_privious_school       = $students['student_privious_school'];
                                    $student->student_privious_class        = $students['student_privious_class'];
                                    $student->student_privious_tc_no        = $students['student_privious_tc_no'];
                                    $student->student_privious_tc_date      = $students['student_privious_tc_date'];
                                    $student->student_privious_result       = $students['student_privious_result'];
                                

                                    $student->save();
                                    // student academic table data
                                    $student_academic->admin_id               = $admin_id;
                                    $student_academic->update_by              = $loginInfo['admin_id'];
                                    $student_academic->student_id             = $student->student_id;
                                    $student_academic->admission_session_id   = $students['admission_session_id'];
                                    $student_academic->admission_class_id     = $students['admission_class_id'];
                                    $student_academic->admission_section_id   = $students['admission_section_id'];
                                    $student_academic->current_session_id     = $students['current_session_id'];
                                    $student_academic->current_class_id       = $students['current_class_id'];
                                    $student_academic->current_section_id     = $students['current_section_id'];
                                    $student_academic->student_unique_id      = $students['student_enroll_number'].'_'.$student->student_id;
                                    $student_academic->group_id               = $students['group_id'];
                                    $student_academic->stream_id              = $students['stream_id'];
                                    $student_academic->save();
                                    // student health table data
                                    $student_health->admin_id               = $admin_id;
                                    $student_health->update_by              = $loginInfo['admin_id'];
                                    $student_health->student_id             = $student->student_id;
                                    $student_health->student_height         = $students['student_height'];
                                    $student_health->student_weight         = $students['student_weight'];
                                    $student_health->student_blood_group    = $students['student_blood_group'];
                                    $student_health->student_vision_left    = $students['student_vision_left'];
                                    $student_health->student_vision_right   = $students['student_vision_right'];
                                    $student_health->medical_issues         = $students['medical_issues'];
                                    $student_health->save();
                                    $document_list = [];

                                    // Student Imprest Account
                                    $checkImprestAcExistance = check_account_existance('Imprest',$student->student_id);
                                    if($checkImprestAcExistance == 0) {
                                        $amt = get_imprest_ac_config_amt();
                                        $imprestAc->admin_id               = $admin_id;
                                        $imprestAc->update_by              = $loginInfo['admin_id'];
                                        $imprestAc->session_id             = $students['current_session_id'];
                                        $imprestAc->class_id               = $students['current_class_id'];
                                        $imprestAc->student_id             = $student->student_id;
                                        $imprestAc->imprest_ac_name        = $students['student_name'];
                                        $imprestAc->imprest_ac_amt         = $amt;
                                        $imprestAc->save();
                                    }

                                    // Student Wallet Account
                                    $checkWalletAcExistance = check_account_existance('Wallet',$student->student_id);
                                    if($checkWalletAcExistance == 0) {
                                        $walletAc->admin_id                = $admin_id;
                                        $walletAc->update_by               = $loginInfo['admin_id'];
                                        $walletAc->session_id              = $students['current_session_id'];
                                        $walletAc->class_id                = $students['current_class_id'];
                                        $walletAc->student_id              = $student->student_id;
                                        $walletAc->wallet_ac_name          = $students['student_name'];
                                        $walletAc->wallet_ac_amt           = 0;
                                        $walletAc->save();
                                    }
                                }
                            }
                        } 
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return redirect()->back()->withErrors($error_message);
                    }
                    DB::commit();
                    return redirect('/admin-panel/student/import-students')->withSuccess('Student import  successfully');
                    
                }
            }


        }
    }
}
