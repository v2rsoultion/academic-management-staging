<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\PrepaidAccount\PrepaidAccount; 
use App\Model\PrepaidAccount\AccountEntry; 
use Yajra\Datatables\Datatables;

class PrepaidAccountController extends Controller
{
    /**
     *  Add page for Prepaid Account
     *  @Shree on 4 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    				= [];
        $account 				= [];
        $loginInfo 				= get_loggedin_user_data();
        $arr_class 				= get_all_classes_mediums();
        $account['arr_class']  = add_blank_option($arr_class, 'Select Class');
        if (!empty($id))
        {
            $decrypted_prepaid_account_id 	= get_decrypted_value($id, true);
            $account      			= PrepaidAccount::Find($decrypted_prepaid_account_id);
            if (!$account)
            {
                return redirect('admin-panel/prepaid-account/manage-account')->withError('Account not found!');
            }
            $page_title             	    = trans('language.manage_account');
            $encrypted_prepaid_account_id   = get_encrypted_value($account->prepaid_account_id, true);
            $save_url               	    = url('admin-panel/prepaid-account/save-account/' . $decrypted_prepaid_account_id);
            $submit_button          	    = 'Update';
        }
        else
        {
            $page_title    = trans('language.manage_account');
            $save_url      = url('admin-panel/prepaid-account/save-account');
            $submit_button = 'Save';
        }
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'account' 			=> $account,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/prepaid-account/manage-account'),
        );
        return view('admin-panel.prepaid-account.add')->with($data);
    }
}
