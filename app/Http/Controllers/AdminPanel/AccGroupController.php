<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Accounts\AccGroup; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class AccGroupController extends Controller
{
    /** 
	 *  Add Page of Acoount Group
     *  @shree on 05 Apr 2019
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$acc_group =  $arr_heads = [];
        $loginInfo = get_loggedin_user_data();
	 	if(!empty($id))
	 	{
	 		$decrypted_acc_group_id = get_decrypted_value($id, true);
        	$acc_group      		= AccGroup::Find($decrypted_acc_group_id);
            $page_title             = trans('language.edit_acc_group');
        	$save_url    			= url('admin-panel/account/manage-accounts-group-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title             = trans('language.acc_group');
	 		$save_url    			= url('admin-panel/account/manage-accounts-group-save');
	 		$submit_button  		= 'Save';
	 	}
        $get_all_account_heads      = get_all_account_heads();
        $acc_group['arr_heads']  	= add_blank_option($get_all_account_heads, 'Select Head');
        $data                   	=  array(
            'page_title'            => $page_title,
            'login_info'        	=> $loginInfo,
            'save_url'          	=> $save_url,  
            'acc_group'		    	=> $acc_group,	
            'submit_button'	    	=> $submit_button
        );
        return view('admin-panel.accounts-group.add')->with($data);
    }

    /**
     *	Add & Update of Acoount Group
     *  @shree on 02 Apr 2019
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      		= get_loggedin_user_data();
        $decrypted_acc_group_id	= get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        if(!empty($id))
        {
            $acc_group          = AccGroup::Find($decrypted_acc_group_id);
            if(!$acc_group) {
                return redirect('admin-panel/account/manage-accounts-group')->withError('Account Group not found!');
            }
            $admin_id    		= $acc_group->admin_id;
            $success_msg 		= 'Account Group updated successfully!';
        } else {
            $acc_group    	 	= New AccGroup;
            $success_msg 		= 'Account Group saved successfully!';
        }

        $acc_sub_head_id = null;
        if ($request->has('acc_sub_head_id'))
        {
            $acc_sub_head_id = Input::get('acc_sub_head_id');
        }
        $validator 				=  Validator::make($request->all(), [
        	'acc_sub_head_id'	=> 'required',
    		'acc_group'  	    => 'required|unique:acc_group,acc_group,' . $decrypted_acc_group_id . ',acc_group_id,acc_sub_head_id,' . $acc_sub_head_id,
    	]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                $acc_group->admin_id        = $admin_id;
                $acc_group->update_by       = $loginInfo['admin_id'];
                $acc_group->acc_sub_head_id = Input::get('acc_sub_head_id');
                $acc_group->acc_group    	= Input::get('acc_group');
                $acc_group->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/account/manage-accounts-group')->withSuccess($success_msg);
    }

    /**
     *	Get Account Group's Data fo view page
     *  @shree on 06 Apr 2019
    **/
    public function anyData(Request $request)
    {
    	$loginInfo 			 = get_loggedin_user_data();
    	$acc_group 			 = AccGroup::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('s_acc_sub_head_id') !=  NULL)
            {
                $query->where('acc_sub_head_id', '=', $request->get('s_acc_sub_head_id'));
            }
            if (!empty($request) && $request->get('s_acc_group') !=  NULL)
            {
                $query->where('acc_group', 'Like', $request->get('s_acc_group').'%');
            }
        })->orderBy('acc_group_id','DESC')->with('getSubHead.getMainHead')->get();
        // p($acc_group);
    	return Datatables::of($acc_group)
        ->addColumn('head_name', function($acc_group) {
            return $acc_group['getSubHead']->acc_sub_head.' ('.$acc_group['getSubHead']['getMainHead']->acc_main_head.')';
        })
    	->addColumn('action', function($acc_group) use($request) {
    		$encrypted_acc_group_id  = get_encrypted_value($acc_group->acc_group_id, true);
    		if($acc_group->acc_group_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/account/manage-accounts-group-status/'.$status .'/'.$encrypted_acc_group_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/account/manage-accounts-group/'.$encrypted_acc_group_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/account/delete-manage-accounts-group/' . $encrypted_acc_group_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';

    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/account/manage-accounts-group');
    } 

    /** 
     *  Change Status of Account Group
     *  @shree on 06 Apr 2019
    **/
    public function changeStatus($status,$id) 
    {
        $acc_group_id   = get_decrypted_value($id, true);
        $acc_group      = AccGroup::find($acc_group_id);
        if ($acc_group)
        {
            $acc_group->acc_group_status  = $status;
            $acc_group->save();
            $success_msg = "Account Group status update successfully!";
            return redirect('admin-panel/account/manage-accounts-group')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Account Group not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Data of Account Group
     *  @shree on 06 Apr 2019
    **/
    public function destroy($id) {
        $acc_group_id   = get_decrypted_value($id, true);
        $acc_group      = AccGroup::find($acc_group_id);

        if ($acc_group) {
            DB::beginTransaction();
            try
            {
                $acc_group->delete();
                $success_msg = "Account Group deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Account Group not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get sub-head data according to main head
     *  @Khushbu on 02 Apr 2019
    **/
    public function getAccGroupData($acc_sub_head_id=null)
    {
        $acc_sub_head_id   = Input::get('acc_sub_head_id');
        $groups        = get_acc_groups($acc_sub_head_id);
        $data           = view('admin-panel.accounts-group.ajax-acc-group-select',compact('groups'))->render();
        return response()->json(['options'=>$data]);
    }
}
