<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Student\Student; // Model
use App\Model\School\School; // Model
use App\Model\Student\StudentCC; // Model
use App\Model\Certificate\Certificate;
use App\Model\Student\StudentAttendanceDetails; // Model
use Yajra\Datatables\Datatables;
use DateTime;

class CcController extends Controller
{
    /**
     *  View page for CC
     *  @shree on 22 Feb 2018
    **/
    public function index()
    {
        $listData                   =  $arr_section  = [];
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes_mediums();
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.view_cc'),
            'redirect_url'  => url('admin-panel/student/view-character-certificate'),
            'login_info'    => $loginInfo,
            'listData'          => $listData
        );
        return view('admin-panel.student-cc.index')->with($data);
    } 	 

    /**
     *  View page for CC
     *  @shree on 22 Feb 2018
    **/
    public function add_cc()
    {
        $listData                   =  $arr_section  = [];
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes_mediums();
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'    => trans('language.issue_character_certificate'),
            'redirect_url'  => url('admin-panel/student/view-issue-transfer-certificate'),
            'login_info'    => $loginInfo,
            'listData'      => $listData
        );
        return view('admin-panel.student-cc.add')->with($data);
    } 
    
    public function get_cc_records(Request $request){
        // p($request->all());
        $arr_student = $student_list = [];
        
        $arr_student        = Student::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('enroll_no')))
            {
                $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
            }
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
            $query->where('student_cc_status',$request->get('cc_status'));
        })->join('student_academic_info', function($join) use ($request){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
        })
        ->with('getStudentAcademic.getCurrentClass','getStudentAcademic.getCurrentSection','getCc')
        ->orderBy('students.student_id', 'DESC')
        ->get();
        // p($arr_student);
        if(!empty($arr_student)){
            foreach ($arr_student as $key => $students) {
                $class_name = $section_name = $student_image = $class_id = $section_id = '';
                if (!empty($students->student_image))
                {
                    $profile = check_file_exist($students->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                }
                if(isset($students['getStudentAcademic']['getCurrentClass']['class_name'])){
                    $class_name = $students['getStudentAcademic']['getCurrentClass']['class_name'];
                    $class_id = $students['getStudentAcademic']['getCurrentClass']['class_id'];
                }
                if(isset($students['getStudentAcademic']['getCurrentSection']['section_name'])){
                    $section_name = $students['getStudentAcademic']['getCurrentSection']['section_name'];
                    $section_id = $students['getStudentAcademic']['getCurrentSection']['section_id'];
                }
                $class_section = $class_name.' - '.$section_name; 
                $cc_no = $cc_date = $cc_reason = '';
                if(isset($students['getCc'])){
                    $cc_no = "CC".$students['getCc']['student_cc_id'];
                    $cc_date = date("d F Y", strtotime($students['getCc']['cc_date']));
                    $cc_reason = $students['getCc']['cc_reason'];
                }
                $student_list[] = array(
                    'student_id'            => $students->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $students->student_enroll_number,
                    'student_roll_no'       => $students->student_roll_no,
                    'student_name'          => $students->student_name,
                    'class_id'              => $class_id,
                    'section_id'            => $section_id,
                    'class_section'         => $class_section,
                    'cc_no'                 => $cc_no,
                    'cc_date'               => $cc_date,
                    'cc_reason'             => $cc_reason,
                );
            }
        }
        return Datatables::of($student_list)
        ->addColumn('student_profile', function ($student_list)
            {
                $profile = "";
                if($student_list['profile'] != ''){
                    $profile = "<img src=".$student_list['profile']." height='30' />";
                }   
                $name = $student_list['student_name'];
                $complete = $profile."  ".$name;
                return $complete;
            })
        ->addColumn('class_section', function ($student_list)
        {
            return $student_list['class_section'];
        })
        ->addColumn('action', function ($student_list)
        {
            $encrypted_student_id = get_encrypted_value($student_list['student_id'], true);
            return ' 
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li>
                    <a title="Generate CC" class="generate-cc" class-id="'.$student_list['class_id'].'" section-id="'.$student_list['section_id'].'" student-name = "'.$student_list['student_name'].'" class-section = "'.$student_list['class_section'].'" student-id="'.$student_list['student_id'].'" >Generate CC</a>
                </li>
                </ul>
            </div>';
        })
        ->addColumn('cc_action', function ($student_list)
        {
            $encrypted_student_id = get_encrypted_value($student_list['student_id'], true);
            return ' 
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li>
                    <li><a title="Cancel CC"  href="'.url('/admin-panel/student/cancel-character-certificate/'.$encrypted_student_id.' ').'" onclick="return confirm('."'Are you sure?'".')" >Cancel CC</a></li>
                    <li>
                        <a title="Character Certificate" href="'.url('/admin-panel/student/character-certificate/'.$encrypted_student_id.' ').'" >Character Certificate</a>
                    </li>
                </ul>
            </div>';
        })
        ->rawColumns(['action' => 'action','cc_action' => 'cc_action'])->addIndexColumn()->make(true);
    }

    public function save_cc(Request $request){
        $loginInfo  = get_loggedin_user_data();
        $session = get_current_session();
        DB::beginTransaction();
            try
            {
                $cc  = New StudentCC;
                $cc->admin_id      = $loginInfo['admin_id'];
                $cc->update_by     = $loginInfo['admin_id'];
                $cc->session_id    =  $session['session_id'];
                $cc->class_id      =  Input::get('s_class_id');
                $cc->section_id    =  Input::get('s_section_id');
                $cc->student_id    =  Input::get('s_student_id');
                $cc->cc_date       =  Input::get('cc_date');
                $cc->cc_reason     =  Input::get('cc_reason');
                $cc->save();
                $student = Student::find($request->get('s_student_id'));
                $student->student_cc_status = 1;
                $student->save();
                $msg = "success";
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                $msg = $error_message;
            }
        DB::commit();
        return $msg;
    }

    public function cancel_cc($id){
        $decrypted_student_id 	= get_decrypted_value($id, true);
        DB::beginTransaction();
        try
        {
            $cc  = StudentCC::where('student_id',$decrypted_student_id)->first();
            $cc->delete();
            DB::table('students') 
            ->where('student_id',$decrypted_student_id)
            ->update([ 'student_cc_status' => 0 ]);
            $msg = "Student's CC successfully cancel.";
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            $msg = $error_message;
            return redirect()->back()->withInput()->withErrors($msg);
        }
        DB::commit();
        return redirect()->back()->withInput()->withSuccess($msg);
        
    }

    public function cc_certificate($id){
        $session = get_current_session();
        $loginInfo   = get_loggedin_user_data();
        $decrypted_student_id 	= get_decrypted_value($id, true);
        $activeCertificate = Certificate::where('template_type',1)->Where('certificate_status',1)->first();
        if(empty($activeCertificate)){
            $activeCertificate = Certificate::where('template_type',1)->orWhere('certificate_status',1)->first();
        }
        $school              = School::with('getCountry','getState','getCity')->first()->toArray();
        
        $school['logo'] = '';
        if (!empty($school['school_logo']))
        {
            $logo = check_file_exist($school['school_logo'], 'school_logo');
            if (!empty($logo))
            {
                $school['logo'] = $logo;
            }
        }
        $student_info = Student::where(function($query) use ($decrypted_student_id) 
        {
            $query->where('students.student_id',$decrypted_student_id);
        })->join('student_cc', function($join) use ($decrypted_student_id){
            $join->on('student_cc.student_id', '=', 'students.student_id');
            $join->where('student_cc.student_id', '=',$decrypted_student_id);
        })
        ->select('students.*','student_cc.*','student_cc.created_at as cc_create_date','students.student_id as student_id')
        ->with('getStudentAcademic.getCurrentClass','getStudentAcademic.getCurrentSection','getParent','getPermanentCity')
        ->first();
        $datetime1 = new DateTime($student_info['student_reg_date']);
        $datetime2 = new DateTime($student_info['cc_date']);
        $interval = $datetime1->diff($datetime2);
        $totalYear = $interval->y;
        $totalMonth = $interval->m;
        $templateName = $activeCertificate->template_link;
        // p($student_info);
        $certificate_info = array(
            'student_name' => $student_info['student_name'],
            'student_enroll_number' => $student_info['student_enroll_number'],
            'student_father_name' => $student_info['getParent']['student_father_name'],
            'student_mother_name' => $student_info['getParent']['student_mother_name'],
            'cc_created_date' => date("d-m-Y",strtotime($student_info['cc_created_date'])),
            'cc_issue_date' => date("d-m-Y",strtotime($student_info['cc_date'])),
            'cc_reason' => $student_info['cc_reason'],
            'school_name' => $school['school_name'],
            'school_description' => $school['school_description'],
            'logo' => $school['logo'],
            'issue_date' => date("d-m-Y"),
            'total_year' => $totalYear,
            'total_month' => $totalMonth,
            'session_year' => $session['session_year'],
            'address'   => $student_info['student_permanent_address'].', '.$student_info['getPermanentCity']['city_name']
        );
       //  p($certificate_info);
        $page_title = "Character Certificate";
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'certificate_info' => $certificate_info,
        );
        // p($certificate_info);
        return view('admin-panel.certificate-templates.'.$templateName)->with($data);
    }
}
