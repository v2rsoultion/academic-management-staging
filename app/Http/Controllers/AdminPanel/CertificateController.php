<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Certificate\Certificate;

use Yajra\Datatables\Datatables;

class CertificateController extends Controller
{
    public function competition_certificates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.competition_certificates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificates.competition_certificates')->with($data);
    }

    public function character_certificates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.character_certificates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificates.character_certificates')->with($data);
    }

    public function transfer_certificates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.transfer_certificates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificates.transfer_certificates')->with($data);
    }

    /**
     *  Add page for Certificate
     *  @Shree on 6 Dec 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data   = [];
        $certificate  = [];
        $loginInfo = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_certificate_id   = get_decrypted_value($id, true);
            $certificate                = Certificate::Find($decrypted_certificate_id);
            if (!$certificate)
            {
                return redirect()->back()->withErrors('Certificate not found!');
            }
            $page_title            = trans('language.edit_certificate');
            $decrypted_certificate_id   = get_encrypted_value($certificate->certificate_id, true);
            $save_url              = url('admin-panel/certificates/save/' . $decrypted_certificate_id);
            $submit_button         = 'Update';
        }
        else
        {
            $page_title    = trans('language.edit_certificate');
            $save_url      = url('admin-panel/certificates/save');
            $submit_button = 'Save';
        }
        $arr_template_for  = \Config::get('custom.template_for');
        if($certificate->template_type == 0){
            $arr_template  = \Config::get('custom.transfer_template');
        } else if($certificate->template_type == 1){
            $arr_template  = \Config::get('custom.character_template');
        } else if($certificate->template_type == 2){
            $arr_template  = \Config::get('custom.competition_template');
        }
        $certificate['arr_template_for']    = add_blank_option($arr_template_for,'Select template for');
        $certificate['arr_template']    = add_blank_option($arr_template,'Select template');
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'certificate'         => $certificate,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/certificates'),
        );
        return view('admin-panel.certificates.add')->with($data);
    }

    /**
     *  Add and update certificate's data
     *  @Shree on 23 Nov 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_certificate_id = get_decrypted_value($id, true);
        $admin_id   = $loginInfo['admin_id'];
        
        $certificate     = Certificate::find($decrypted_certificate_id);
        $admin_id   = $certificate['admin_id'];
        if (!$certificate)
        {
            return redirect()->back()->withErrors("Certificate not found!"); 
        }
        $success_msg = 'Certificate updated successfully!';
        
        DB::beginTransaction();
        try
        {
            $certificate->admin_id          = $admin_id;
            $certificate->update_by         = $loginInfo['admin_id'];
            $certificate->book_no_start     = Input::get('book_no_start');
            $certificate->book_no_end       = Input::get('book_no_end');
            $certificate->serial_no_start   = Input::get('serial_no_start');
            $certificate->serial_no_end     = Input::get('serial_no_end');
            $certificate->save();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        if($certificate->template_type == 0){
            return redirect('admin-panel/certificates/transfer-certificates')->withSuccess($success_msg);
        } else if($certificate->template_type == 1){
            return redirect('admin-panel/certificates/character-certificates')->withSuccess($success_msg);
        } else if($certificate->template_type == 2){
            return redirect('admin-panel/certificates/competition-certificates')->withSuccess($success_msg);
        }
        
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 5 Dec 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $certificates = Certificate::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('template_type') != null)
            {
                $query->where('template_type', "=", $request->get('template_type'));
            }
        })->orderBy('certificate_id', 'DESC')->get();
        return Datatables::of($certificates)
        ->addColumn('template_for', function ($certificates)
        {
            if($certificates->template_for == 0){
                $template_for = "Participation";
            } else {
                $template_for = "Winner";
            }
           return $template_for;
        }) 
        ->addColumn('book_no', function ($certificates)
        {
           return $certificates->book_no_start.' - '.$certificates->book_no_end;
        }) 
        ->addColumn('serial_no', function ($certificates)
        {
           return $certificates->serial_no_start.' - '.$certificates->serial_no_end;
        }) 
        ->addColumn('template_link', function ($certificates)
        {
            $template_link = str_replace('_', '-',$certificates->template_link);
            return '
                <div class="pull-left"><a target="_blank" href="'.url('/admin-panel/certificates/'.$template_link. ' ').'" >Preview Link</a></div>
            ';
            if($certificates->certificate_status == 0){
                $status = "Deactive";
            } else {
                $status = "Active";
            }
           return $status;
        }) 
        ->addColumn('template_status', function ($certificates)
        {
            if($certificates->certificate_status == 0){
                $status = "Deactive";
            } else {
                $status = "Active";
            }
           return $status;
        }) 
        ->addColumn('action', function ($certificates)
        {
            $encrypted_certificate_id = get_encrypted_value($certificates->certificate_id, true);
            if($certificates->certificate_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/certificates/certificate-status/'.$status.'/'. $encrypted_certificate_id. ' ').'" >'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('/admin-panel/certificates/edit-certificate/'. $encrypted_certificate_id. ' ').'" ><i class="zmdi zmdi-edit"></i></a></div>
                ';
        })->rawColumns(['template_for' => 'template_for','book_no' => 'book_no','serial_no' => 'serial_no','template_status' => 'template_status','action' => 'action','template_link'=> 'template_link'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Certificate's status
     *  @Shree on 6 Dec 2018
    **/
    public function changeStatus($status,$id)
    {
        $certificate_id = get_decrypted_value($id, true);
        $certificate    = Certificate::find($certificate_id);
        if ($certificate)
        {
            $certificate->certificate_status  = $status;
            $certificate->save();
            $success_msg = "Certificate status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Certificate not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function participation_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = "Participation Template1";
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.participation_template1')->with($data);
    }

    public function winner_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = "Winner Template1";
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.winner_template1')->with($data);
    }

    public function character_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.character_template1');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.character_template1')->with($data);
    }

    public function character_template2() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.character_template2');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.character_template2')->with($data);
    }

    public function transfer_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.transfer_template1');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.transfer_template1')->with($data);
    }
    public function transfer_template2() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.transfer_template2');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.transfer_template2')->with($data);
    }
    public function transfer_template3() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.transfer_template3');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.certificate-templates.transfer_template3')->with($data);
    }
}
