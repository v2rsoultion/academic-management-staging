<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use DateTime;
use App\Model\Staff\StaffLeaveHistoryInfo; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class SalaryGenerationController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  Manage Salary Generation page
     *  @shree on 4 April 2019
    **/
    public function manageSalaryGeneration() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => trans('language.manage_salary_generation'),
        );
        
        return view('admin-panel.payroll-salary-generation.index')->with($data);
    }

    /**
     *  Salary Generate page
     *  @shree on 4 April 2019
    **/
    public function generate_salary(){
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => trans('language.salary_generate'),
        );
        return view('admin-panel.payroll-salary-generation.generate-salary')->with($data);
    }
}
