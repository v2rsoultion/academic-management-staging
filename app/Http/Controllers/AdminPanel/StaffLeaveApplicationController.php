<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use DateTime;
use App\Model\Staff\StaffLeaveApplication; // Model
use App\Model\Staff\Staff; // Model
use App\Model\Payroll\LeaveScheme; // Model
use App\Model\Payroll\LeaveSchemeMap; // Model
use App\Model\Staff\StaffLeaveHistoryInfo; // Model
use Yajra\Datatables\Datatables;
use DateInterval;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class StaffLeaveApplicationController extends Controller
{
    /**
     *  View page for Leave application
     *  @Shree on 21 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_leave_application'),
            'redirect_url'  => url('admin-panel/staff-leave-application/view-leave-application'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.staff-leave-application.index')->with($data);
    }

    /**
     *  Add page for Staff Leave Application
     *  @Shree on 21 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    			= [];
        $leave_application 	= $arr_leave_scheme = [];
        $loginInfo 		    = get_loggedin_user_data();
        // p($loginInfo);
        $leave_scheme = LeaveSchemeMap::where('staff_id', $loginInfo['staff_id'])->with('getLeaveSchemeMap')->get()->toArray();
        // p($leave_scheme);
        foreach($leave_scheme as $key => $value) {
            $arr_leave_scheme[$value['get_leave_scheme_map']['pay_leave_scheme_id']] = $value['get_leave_scheme_map']['lscheme_name'];
        }
        // p($arr_leave_scheme);
        if (!empty($id))
        {
            $decrypted_staff_leave_id   = get_decrypted_value($id, true);
            $leave_application			= StaffLeaveApplication::Find($decrypted_staff_leave_id);
            if (!$leave_application)
            {
                return redirect('admin-panel/staff-leave-application/add-leave-application')->withError('Leave Application not found!');
            }
            $page_title             	= trans('language.edit_leave_application');
            $encrypted_staff_leave_id   = get_encrypted_value($leave_application->staff_leave_id, true);
            $save_url               	= url('admin-panel/staff-leave-application/save/' . $encrypted_staff_leave_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_leave_application');
            $save_url      = url('admin-panel/staff-leave-application/save');
            $submit_button = 'Save';
            if(!empty($arr_leave_scheme)) {
                $leave_application['leave_scheme'] = add_blank_option($arr_leave_scheme, 'Select Leave Scheme');
            }
        }
        // p(!empty($leave_application['leave_scheme']));
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'leave_application' => $leave_application,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/staff-leave-application/view-leave-application'),
        );
        return view('admin-panel.staff-leave-application.add')->with($data);
    }

    /**
     *  Add and update Leave Application's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_staff_leave_id	= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $leave_application = StaffLeaveApplication::find($decrypted_staff_leave_id);

            if (!$leave_application)
            {
                return redirect('/admin-panel/staff-leave-application/add-leave-application/')->withError('Leave Application not found!');
            }
            $admin_id = $leave_application['admin_id'];
            $success_msg = 'Leave Application updated successfully!';
        }
        else
        {
            $leave_application  = New StaffLeaveApplication;
            $success_msg 		= 'Leave Application saved successfully!';
        }
        $staff_id = $loginInfo['staff_id'];

        $validatior = Validator::make($request->all(), [
                'staff_leave_from_date' => 'required|unique:staff_leaves,staff_leave_from_date,' . $staff_id . ',staff_id',
                'staff_leave_reason'   	=> 'required',
                'staff_leave_to_date'   => 'required',
                // 'pay_leave_scheme_id'   => 'required',
                'documents'   			=> 'mimes:pdf,jpg,jpeg,png',
        ]);
        $checkLeaveExistance = $this->checkLeaveExistance($staff_id,$request);
        if($checkLeaveExistance > 0){
            return redirect()->back()->withInput()->withErrors('Leaves dates already exist!');
        }
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $leave_application->admin_id      			   = $admin_id;
                $leave_application->update_by      			   = $loginInfo['admin_id'];
                $leave_application->staff_id     			   = $loginInfo['staff_id'];
                $leave_application->staff_leave_reason 	       = Input::get('staff_leave_reason');
                $leave_application->staff_leave_from_date      = Input::get('staff_leave_from_date');
                $leave_application->staff_leave_to_date 	   = Input::get('staff_leave_to_date');
                $leave_application->pay_leave_scheme_id 	   = Input::get('pay_leave_scheme_id');
                if ($request->hasFile('documents'))
		        {
                    if (!empty($id)){
                        $attachment = check_file_exist($leave_application->staff_leave_attachment, 'staff_leave_document_file');
                        if (!empty($attachment))
                        {
                            unlink($attachment);
                        } 
                    }
		            $file                          = $request->File('documents');
		            $config_document_upload_path   = \Config::get('custom.staff_leave_document_file');
		            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
		            $ext                           = substr($file->getClientOriginalName(),-4);
		            $name                          = substr($file->getClientOriginalName(),0,-4);
		            $filename                      = $name.mt_rand(0,100000).time().$ext;
		            $file->move($destinationPath, $filename);
		            $leave_application->staff_leave_attachment = $filename;
                }
                // p($leave_application);
                $leave_application->save();
                $staff_name = get_staff_name($loginInfo['staff_id']);
                $send_notification  = $this->send_push_notification($leave_application->staff_leave_id,"staff_leave",$staff_name,$loginInfo['staff_id'],'staff_leave');
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                // p($error_message);
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/staff-leave-application/view-leave-application')->withSuccess($success_msg);
    }

    /**
     *  Get leaves's Data for view page(Datatables)
     *  @Shree on 21 Oct 2018.
    **/
    public function anyData()
    {
        $loginInfo 				= get_loggedin_user_data();
        $leave_application      = StaffLeaveApplication::where('staff_id',$loginInfo['staff_id'])->orderBy('staff_leave_id', 'DESC')->get();    
        return Datatables::of($leave_application)
        ->addColumn('date_range', function ($leave_application)
        {	
            return date('d M Y',strtotime($leave_application->staff_leave_from_date)).' - '.date('d M Y',strtotime($leave_application->staff_leave_to_date));
            
        })
        ->addColumn('leave_days', function ($leave_application)
        {	
            $start    = (new DateTime($leave_application->staff_leave_from_date));
            $end      = (new DateTime($leave_application->staff_leave_to_date));
            $interval = DateInterval::createFromDateString('1 day');
            return $interval->d.' Days';
            
        })
        ->addColumn('staff_leave_status', function ($leave_application)
        {	
            $status = '';
            if($leave_application->staff_leave_status == 0){
                $status = 'Pending';
            }elseif($leave_application->staff_leave_status == 1){
                $status = 'Approved';
            }elseif($leave_application->staff_leave_status == 2){
                $status = 'Disapproved';
            }
            return $status;
            
        })
        ->addColumn('staff_leave_attachment', function ($leave_application)
        {	
            $attachment_link = '';
            if($leave_application->staff_leave_attachment == ''){
                $attachment_link = 'Attachment not available';
            }else{
                $attachment_link = check_file_exist($leave_application['staff_leave_attachment'], 'staff_leave_document_file');
                $attachment_link = '<span class="profile_detail_span_6"><a href="'.url($attachment_link).'" target="_blank">View Attachment</a></span>';
            }
            return $attachment_link;
            
        })
        ->addColumn('action', function ($leave_application)
        {
            if($leave_application->staff_leave_status == 0) {
                $encrypted_staff_leave_id = get_encrypted_value($leave_application->staff_leave_id, true);
                return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-leave-application/' . $encrypted_staff_leave_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete/' . $encrypted_staff_leave_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            } else {
                return '-----';
            }
            
        })
        ->rawColumns(['action' => 'action','staff_leave_attachment' => 'staff_leave_attachment'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Delete Leave Application's data
     *  @Shree on 21 Oct 2018.
    **/
    public function destroy($id)
    {
        $staff_leave_id 		= get_decrypted_value($id, true);
        $leave_application		= StaffLeaveApplication::find($staff_leave_id);
       
        $success_msg = $error_message =  "";
        if ($leave_application)
        {
            DB::beginTransaction();
            try
            {   
                $attachment = check_file_exist($leave_application->staff_leave_attachment, 'staff_leave_document_file');
                if (!empty($attachment))
                {
                    unlink($attachment);
                } 
                $leave_application->delete();
                $success_msg = "Leave application deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/staff-leave-application/view-leave-application')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/staff-leave-application/view-leave-application')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Leave application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change leave application's status
     *  @Shree on 21 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $loginInfo      		= get_loggedin_user_data();
        $admin_id               = $loginInfo['admin_id'];
        $staff_leave_id 		= get_decrypted_value($id, true);
        $leave_application		= StaffLeaveApplication::find($staff_leave_id);
        if ($leave_application)
        {
            $leave_application->staff_leave_status  = $status;
            $leave_application->save();
            $success_msg = "Leave Application status updated!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Leave Application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Leave application
     *  @Shree on 21 Oct 2018
    **/
    public function manage()
    {
        $leave_status = \Config::get('custom.staff_leave_status');
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_leave_application'),
            'redirect_url'  => url('admin-panel/staff-leave-application/manage-leave-application'),
            'login_info'    => $loginInfo,
            'leave_status'  => $leave_status,
        );
        return view('admin-panel.staff-leave-application.manage')->with($data);
    }

    /**
     *  Manage leaves's Data for manage page(Datatables)
     *  @Shree on 21 Oct 2018.
    **/
    public function manage_leaves()
    {
        $loginInfo 				= get_loggedin_user_data();
        $leave_application      = StaffLeaveApplication::with('getLeaveScheme')->orderBy('staff_leave_id', 'DESC')->get();
        // p($leave_application);
        return Datatables::of($leave_application)
        ->addColumn('date_range', function ($leave_application)
        {	
            return date('d M Y',strtotime($leave_application->staff_leave_from_date)).' - '.date('d M Y',strtotime($leave_application->staff_leave_to_date));
        })
        ->addColumn('leave_days', function ($leave_application)
        {	
            $start    = (new DateTime($leave_application->staff_leave_from_date));
            $end      = (new DateTime($leave_application->staff_leave_to_date));
            $interval = DateInterval::createFromDateString('1 day');
            return $interval->d.' Days';
        })
        ->addColumn('staff_name', function ($leave_application)
        {	
            return get_staff_name($leave_application->staff_id);
            
        })
        ->addColumn('staff_leave_status', function ($leave_application)
        {	
            $status = '';
            if($leave_application->staff_leave_status == 0){
                $status = 'Pending';
            }elseif($leave_application->staff_leave_status == 1){
                $status = 'Approved';
            }elseif($leave_application->staff_leave_status == 2){
                $status = 'Disapproved';
            }
            return $status;
            
        })
        ->addColumn('previous_leave_details', function ($leave_application)
        {	
            $datetime1 = new DateTime($leave_application->staff_leave_from_date);
            $datetime2 = new DateTime($leave_application->staff_leave_to_date);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');//now do whatever you like with $days
            $encrypted_leave_id  = get_encrypted_value($leave_application->staff_leave_id, true);
            $map_staff_leave = '<a href="'.url('/admin-panel/staff-leave-application/manage-leave-data/'.$encrypted_leave_id.' ').'" class="btn btn-raised btn-primary staff-leaves" staff_leave_id='.$leave_application->staff_leave_id.'  staff_name='.get_staff_name($leave_application->staff_id).' no_of_leaves="'.$days.'">Manage Leave</a>';
            return $map_staff_leave;
        })
        ->addColumn('staff_leave_attachment', function ($leave_application)
        {	
            $attachment_link = '';
            if($leave_application->staff_leave_attachment == ''){
                $attachment_link = 'Attachment not available';
            }else{
                $attachment_link = check_file_exist($leave_application['staff_leave_attachment'], 'staff_leave_document_file');
                $attachment_link = '<span class="profile_detail_span_6"><a href="'.url($attachment_link).'" target="_blank">View Attachment</a></span>';
            }
            return $attachment_link;
            
        })
        ->rawColumns(['previous_leave_details' => 'previous_leave_details', 'loss_of_pay' => 'loss_of_pay', 'action' => 'action','staff_name' => 'staff_name','staff_leave_attachment' => 'staff_leave_attachment'])->addIndexColumn()
        ->make(true);
    }

    public function send_push_notification($module_id,$module_name,$staff_name,$staff_id,$notification_type){
        $device_ids = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereRaw('FIND_IN_SET(1,staff_role_id)')->select('staff_name', 'staff_id','reference_admin_id')->groupBy('staff_id')->with('getStaffAdminInfo')->get();
        foreach ($arr_staff_list as $staff) {
            if($staff['getStaffAdminInfo']->fcm_device_id != "" && $staff['getStaffAdminInfo']->notification_status == 1){
                $device_ids[] = $staff['getStaffAdminInfo']->fcm_device_id;
                $staff_admin_id = $staff['getStaffAdminInfo']->admin_id;
            }
        }
        $message = $staff_name." applied for leave";
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => Null,
            'parent_admin_id' => Null,
            'staff_admin_id' => $staff_admin_id,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,$staff_id,'','',$device_ids,$message);
    }

     /** 
     *  Get Manage Staff Leave Data 
     *  @Khushbu on 30 March 2019.
    **/
    public function manageLeave($id) {
        $staff_leave  = $arr_leave_scheme  = [];
        $staff_leave_id         = get_decrypted_value($id, true);
        $leave_status           = \Config::get('custom.staff_leave_status');
        $leave_period           = \Config::get('custom.leave_period');
        $staff_leave            = StaffLeaveApplication::where('staff_leave_id', $staff_leave_id)->with('getStaff','getLeaveScheme')->get()->toArray();
        $staff_leave            = isset($staff_leave[0]) ? $staff_leave[0] : [];
        // p($staff_leave);
        $leave_history = StaffLeaveHistoryInfo::where([['staff_id',$staff_leave['staff_id']],['pay_leave_scheme_id',$staff_leave['pay_leave_scheme_id']],[DB::raw("MONTH(created_at)"), date('m', strtotime($staff_leave['staff_leave_from_date']))]])->get()->toArray();
        $leave_history = isset($leave_history[0]) ? $leave_history[0] : [];
        $staff_leave_scheme     = LeaveSchemeMap::where('staff_id', $staff_leave['staff_id'])->with('getLeaveSchemeMap')->get()->toArray();
      
        foreach($staff_leave_scheme as $key => $value) {
            $arr_leave_scheme[$value['get_leave_scheme_map']['pay_leave_scheme_id']] = $value['get_leave_scheme_map']['lscheme_name'];
        }
        $staff_leave['leave_scheme'] = add_blank_option($arr_leave_scheme, 'Select Leave Scheme');
        
       
        $start    = (new DateTime($staff_leave['staff_leave_from_date']));
        $end      = (new DateTime($staff_leave['staff_leave_to_date']));
        $interval = DateInterval::createFromDateString('1 day');
       
        $days = $interval->d;//now do whatever you like with $days
        $staff_leave['leave_period'] = $leave_period[$staff_leave['get_leave_scheme']['lscheme_period']];
        $staff_leave['leave_status'] = $leave_status['staff_leave_status'];  
        $staff_leave['consume_leave']= $leave_history['total_no_of_leaves'] - $leave_history['remaining_leaves'];
        
       //  p($staff_leave);
        $data                   = array(
            'page_title'        => trans('language.manage_leave'),
            'staff_leave'       => $staff_leave,
            'days'              => $days,
            'leave_status'      => $leave_status,
            'arr_leave_scheme'  => $arr_leave_scheme,
        );
        return view('admin-panel.staff-leave-application.manage-leave')->with($data);
    }

    /** 
     *  Get Staff Leave Map Data
     *  @Khushbu on 27 March 2019.
    **/
    public function leaveHistoryData(Request $request) {
        // p($request->all());
        $session           = get_current_session();
        $staff_leave       = StaffLeaveApplication::find($request->get('staff_leave_id'));
        // p($staff_leave);
        $previous_leaves_history   = StaffLeaveApplication::where('staff_id', $staff_leave->staff_id)->where('staff_leave_id', '!=', $request->get('staff_leave_id'))->with('getStaff','getLeaveScheme')->orderBy('staff_leave_id', 'DESC')->get()->toArray();
        return Datatables::of($previous_leaves_history)
        ->addColumn('date_range', function ($previous_leaves_history)
        {	
            return date('d M Y',strtotime($previous_leaves_history['staff_leave_from_date'])).' - '.date('d M Y',strtotime($previous_leaves_history['staff_leave_to_date']));
            
        })
        ->addColumn('leave_days', function ($previous_leaves_history)
        {	
            $start    = (new DateTime($previous_leaves_history['staff_leave_from_date']));
            $end      = (new DateTime($previous_leaves_history['staff_leave_to_date']));
            $interval = DateInterval::createFromDateString('1 day');
            return $interval->d.' Days';
            
        })
        ->addColumn('staff_leave_status', function ($previous_leaves_history)
        {	
            $status = '';
            if($previous_leaves_history['staff_leave_status'] == 0){
                $status = 'Pending';
            }elseif($previous_leaves_history['staff_leave_status'] == 1){
                $status = 'Approved';
            }elseif($previous_leaves_history['staff_leave_status'] == 2){
                $status = 'Disapproved';
            }
            return $status;
            
        })
        ->addColumn('lscheme_name', function($previous_leaves_history){
            $lscheme_name = '----';
            if(isset($previous_leaves_history['get_leave_scheme']['lscheme_name'])){
                $lscheme_name = $previous_leaves_history['get_leave_scheme']['lscheme_name'];
            }
            return $lscheme_name;
        })
        ->addColumn('no_of_leaves', function($previous_leaves_history) {
            $no_of_leaves = 0;
            if(isset($previous_leaves_history['get_leave_scheme']['no_of_leaves'])){
                $no_of_leaves = $previous_leaves_history['get_leave_scheme']['no_of_leaves'];
            }
            return $no_of_leaves;
        })
        ->addColumn('no_of_loss_pay_leave', function($previous_leaves_history) {
            if(!empty($previous_leaves_history['no_of_loss_pay_leave']))
                return $no_of_loss_pay_leave = $previous_leaves_history['no_of_loss_pay_leave'];
            else
                return $no_of_loss_pay_leave = 0;
        })
        ->rawColumns(['date_range' => 'date_range','leave_days' => 'leave_days'])->addIndexColumn()
        ->make(true); 
    } 

    /**
     *  Change paid leave status 
     *  @Khushbu on 28 March 2019.
    **/
    public function changePaidLeaveStatus(Request $request)
    {
        $leave_application		= StaffLeaveApplication::find($request->get('staff_leaves'));
        // p($leave_application);
        if ($leave_application)
        {
            $leave_application->pay_leaves_status  = $request->get('status');
            $leave_application->save();
            $success_msg = "Leave Application status updated!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Leave Application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Save Model Data 
     *  @Khushbu on 29 March 2019.
    **/
    public function saveModelData(Request $request) {
        // p($request->all());
        $loginInfo      		= get_loggedin_user_data();
        $admin_id               = $loginInfo['admin_id'];
        $session                = get_current_session();
        $leave_application		= StaffLeaveApplication::find($request->get('staff_leave_id'));
        // p($leave_application);
        if ($leave_application)
        {
            $leave_application->staff_leave_status  = $request->get('staff_leave_status');
            $leave_application->pay_leaves_status  = $request->get('pay_leave_status');
            $leave_application->no_of_loss_pay_leave  = $request->get('no_of_loss_pay');
            $leave_application->pay_leave_scheme_id  = $request->get('leave_scheme');
            $leave_application->save();
            if($leave_application->staff_leave_status == 1) {
                $leave_history = StaffLeaveHistoryInfo::where([['staff_id',$leave_application->staff_id],['pay_leave_scheme_id',$leave_application->pay_leave_scheme_id],[DB::raw("MONTH(created_at)"),date('m', strtotime($leave_application->staff_leave_from_date))]])->get()->toArray();
                $leave_history = isset($leave_history[0]) ? $leave_history[0] : [];
                if(!empty($leave_history)) {
                    $staff_leave_history = StaffLeaveHistoryInfo::Find($leave_history['staff_leave_history_info_id']);
                } else {
                    $staff_leave_history = New StaffLeaveHistoryInfo;
                }
                
                $start    = (new DateTime($leave_application->staff_leave_from_date));
                $end      = (new DateTime($leave_application->staff_leave_to_date));
                $interval = DateInterval::createFromDateString('1 day');
                $days = $interval->d;

                $leave_scheme_details = LeaveScheme::Find($leave_application->pay_leave_scheme_id);
                $remaining_leaves = 0;
                if($days <= $leave_scheme_details['no_of_leaves']) {
                    $remaining_leaves = $leave_scheme_details['no_of_leaves'] - $days;
                }
                $staff_leave_history->admin_id      	  = $admin_id;
                $staff_leave_history->update_by      	  = $loginInfo['admin_id'];
                $staff_leave_history->session_id          = $session['session_id']; 
                $staff_leave_history->staff_id     		  = $leave_application['staff_id'];
                $staff_leave_history->pay_leave_scheme_id = $leave_application['pay_leave_scheme_id'];
                $staff_leave_history->lscheme_period_type = $leave_scheme_details['lscheme_period'];
                $staff_leave_history->total_no_of_leaves  = $leave_scheme_details['no_of_leaves'];
                $staff_leave_history->remaining_leaves    = $remaining_leaves;
                $staff_leave_history->staff_leave_id      = $leave_application->staff_leave_id;
                $staff_leave_history->save();
            }
            return $success_msg = "Success";
            //return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            return $error_message = "Fail";
           // return redirect()->back()->withErrors($error_message);
        }
    }

    public function getStaffData(Request $request) {
        $staff_leave       = StaffLeaveApplication::find($request->get('staff_leave_id'));
        return response()->json($staff_leave);
    }

    public function checkLeaveExistance($staff_id,Request $request){
        $begin = $request->get('staff_leave_from_date');
        $end = $request->get('staff_leave_to_date');
        $total = 0;
        $total = StaffLeaveApplication::where('staff_id', $staff_id) // probably scoping to a single user??
        ->where(function ($query) use ($begin, $end) {
            $query->where(function ($q) use ($begin, $end) {
                $q->where('staff_leave_from_date', '>=', $begin)
                ->where('staff_leave_from_date', '<', $end);
            })->orWhere(function ($q) use ($begin, $end) {
                $q->where('staff_leave_from_date', '<=', $begin)
                ->where('staff_leave_to_date', '>', $end);
            })->orWhere(function ($q) use ($begin, $end) {
                $q->where('staff_leave_to_date', '>', $begin)
                ->where('staff_leave_to_date', '<=', $end);
            })->orWhere(function ($q) use ($begin, $end) {
                $q->where('staff_leave_from_date', '>=', $begin)
                ->where('staff_leave_to_date', '<=', $end);
            })
            ->orWhere(function ($q) use ($begin, $end) {
                $q->where('staff_leave_status', 0)->orWhere('staff_leave_status', 1);
            });;
        })->count();
        return $total;
    }
}
