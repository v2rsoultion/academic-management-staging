<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\ImprestEntry; // Model
use Yajra\Datatables\Datatables;
use App\Model\FeesCollection\OneTime; // Model
use App\Model\FeesCollection\FeesStMap; // Model
use App\Model\Student\Student;
use App\Model\FeesCollection\Receipt;
use App\Model\FeesCollection\ReceiptDetail;
use App\Model\Student\ImprestAc; // Model

class ImprestAcController extends Controller
{
    /**
     *  Add page for Imprest A/C entry
     *  @Shree on 20 Dec 2018
    **/
    public function add(Request $request,$id = NULL)
    {
        $data = $arr_section = $imprest_entry = [];
        $imprest_entry 	= [];
        $loginInfo 		= get_loggedin_user_data();
        $arr_class = get_all_classes_mediums();
        if (!empty($id))
        {
            $decrypted_ac_entry_id 	= get_decrypted_value($id, true);
            $imprest_entry      	= ImprestEntry::Find($decrypted_ac_entry_id);
            if (!$imprest_entry)
            {
                return redirect('admin-panel/fees-collection/manage-entry')->withError('Entry not found!');
            }
            $page_title             = trans('language.manage_ac_entry');
            $encrypted_ac_entry_id  = get_encrypted_value($imprest_entry->ac_entry_id, true);
            $save_url               = url('admin-panel/fees-collection/save-entry/' . $encrypted_ac_entry_id);
            $submit_button          = 'Update';
            $arr_section    = get_all_sections($imprest_entry->class_id);
        }
        else
        {
            $page_title    = trans('language.manage_ac_entry');
            $save_url      = url('admin-panel/fees-collection/save-entry');
            $submit_button = 'Save';
        }
        $imprest_entry['arr_class'] = add_blank_option($arr_class, "Select Class");
        $imprest_entry['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'imprest_entry' 	=> $imprest_entry,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/fees-collection/manage-entry'),
        );
        return view('admin-panel.imprest-entry.add')->with($data);
    }

    /**
     *  Add and update Imprest's data
     *  @Shree on 20 Dec 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      		= get_loggedin_user_data();
        $decrypted_ac_entry_id  = get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        $session  = get_current_session();
        if (!empty($id))
        {
            $imprest_entry  = ImprestEntry::find($decrypted_ac_entry_id);
            $admin_id       = $imprest_entry['admin_id'];
            if (!$imprest_entry)
            {
                return redirect('/admin-panel/fees-collection/manage-entry')->withError('Entry not found!');
            }
            $success_msg = 'Entry updated successfully!';
        }
        else
        {
            $imprest_entry  = New ImprestEntry;
            $success_msg 	= 'Entry saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'ac_entry_name' => 'required|unique:imprest_ac_entries,ac_entry_name,' . $decrypted_ac_entry_id . ',ac_entry_id',
            'class_id'      => 'required',
            'section_id'    => 'required',
            'ac_entry_amt'  => 'required',        
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else { 
            DB::beginTransaction();
            try
            {
                $imprest_entry->admin_id       	= $admin_id;
                $imprest_entry->update_by      	= $loginInfo['admin_id'];
                $imprest_entry->session_id 		= $session['session_id'];
                $imprest_entry->class_id 		= Input::get('class_id');
                $imprest_entry->section_id 		= Input::get('section_id');
                $imprest_entry->ac_entry_name 	= Input::get('ac_entry_name');
                $imprest_entry->ac_entry_date 	= date('Y-m-d');
                $imprest_entry->ac_entry_amt 	= Input::get('ac_entry_amt');
                $imprest_entry->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/fees-collection/manage-entry')->withSuccess($success_msg);
    }

    /**
     *  Get Entry's Data for view page(Datatables)
     *  @Shree on 21 Dec 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $session    = get_current_session();
        $imprest_entry  = ImprestEntry::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('ac_entry_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', $request->get('section_id'));
            }
            $query->where('session_id', $session['session_id']);
        })->with('getClass')->with('getSection')->orderBy('ac_entry_id', 'DESC')->get();

        
        return Datatables::of($imprest_entry,$request,$session)
        ->addColumn('class_section', function ($imprest_entry) use($request)
        {
            return $imprest_entry['getClass']->class_name.' - '.$imprest_entry['getSection']->section_name;
        })
        ->addColumn('ac_entry_date', function ($imprest_entry) use($request)
        {
            return date("d M Y", strtotime($imprest_entry->ac_entry_date));
        })
        ->addColumn('action', function ($imprest_entry) use($request,$session)
        {
            $imprest_fees = DB::select("SELECT ac_entry_id FROM `ac_entry_map` where `session_id` = '".$session['session_id']."' AND `ac_entry_id` = '".$imprest_entry->ac_entry_id."'
            ");
            if(isset($imprest_fees)){
                if($imprest_fees[0]->ac_entry_id != NULL){
                    return "------";
                }
            }
            
            $encrypted_ac_entry_id = get_encrypted_value($imprest_entry->ac_entry_id, true);
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/fees-collection/manage-entry/'.$encrypted_ac_entry_id . ' ').'"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/fees-collection/delete-entry/' . $encrypted_ac_entry_id . ' ').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }
    /**
     *  Destroy entry's data
     *  @Shree on 21 Dec 2018.
    **/
    public function destroy($id)
    {
        $ac_entry_id    = get_decrypted_value($id, true);
        $imprest_entry  = ImprestEntry::find($ac_entry_id);
        $success_msg = $error_message =  "";
        if ($imprest_entry)
        {
            DB::beginTransaction();
            try
            {
                $imprest_entry->delete();
                $success_msg = "Entry deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/fees-collection/manage-entry')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/fees-collection/manage-entry')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Entry not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for heads listing
     *  @shree on 13 Oct 2018
     **/
    public function counter_index()
    {
        $loginInfo = get_loggedin_user_data();
        $listData = $arr_section = $arr_class = [];
        $arr_fees_head = get_all_imprest_fee_heads();
        $listData['arr_fees'] = add_blank_option($arr_fees_head, "Select Fees Head");
        $listData['arr_class'] = add_blank_option($arr_class, "Select Class");
        $listData['arr_section'] = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title' => trans('language.fees_counter'),
            'redirect_url' => url('admin-panel/fees-collection/imprest-ac-counter'),
            'login_info' => $loginInfo,
            'listData' => $listData,
        );
        return view('admin-panel.imprest-counter.index')->with($data);
    }

    /**
     *  Get classes data according section
     *  @Shree on 12 Jan 2018
    **/
    public function getFeesClasses()
    {
        $fees_id = Input::get('fees_id');
        $fees_info = OneTime::where(function($query) use ($fees_id)
        {
            $query->where('deduct_imprest_status',1);
            $query->where('ot_head_id',$fees_id);
        })
        ->orderBy('ot_head_id', 'ASC')->first();
        $classes = get_all_classes_mediums($fees_info->ot_classes);
        $data = view('admin-panel.class.ajax-class-select',compact('classes'))->render();
        return response()->json(['options'=>$data]);
    }

    public function get_student_list(Request $request){
        $session    = get_current_session();
        $all_students = $map_students = [];
        if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null && !empty($request->get('ot_head_id')) && $request->get('ot_head_id') != null && !empty($request->get('section_id')) && $request->get('section_id') != null){
            $map_students = FeesStMap::where(function($query) use ($request,$session)
            {
                $query->where('head_type', '=','ONETIME');
                $query->where('session_id',$session['session_id']);
                if (!empty($request) && !empty($request->get('ot_head_id')) && $request->get('ot_head_id') != null){
                    $query->where('head_id',$request->get('ot_head_id'));
                }
                if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                    $query->where('class_id',$request->get('class_id'));
                }
                if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                    $query->where('section_id',$request->get('section_id'));
                }
            })
            ->orderBy('fees_st_map_id', 'DESC')->first();
        }
        $fees_info = OneTime::where(function($query) use ($request)
        {
            $query->where('deduct_imprest_status',1);
            if (!empty($request) && !empty($request->get('ot_head_id')) && $request->get('ot_head_id') != null){
                $query->where('ot_head_id',$request->get('ot_head_id'));
            }
        })
        ->orderBy('ot_head_id', 'ASC')->first();
        // $map_students =  (array) $map_students;
        // $fees_info =  (array) $fees_info;
        if(!empty($map_students)){
            // $students = Student::whereIn('students.student_id',explode(",",$map_students['student_ids']))
            // ->join('imprest_ac', function($join) use ($request,$session){
            //     $join->on('imprest_ac.student_id', '=', 'students.student_id');
            //     if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
            //         $join->where('imprest_ac.class_id', '=',$request->get('class_id'));
            //     }
            // })
            // ->select('students.student_id','students.student_enroll_number','students.student_roll_no','students.student_name','imprest_ac.imprest_ac_amt','imprest_ac.imprest_ac_id')
            // ->orderBy('students.student_id', 'DESC')->get()->toArray();
            
            $studentIds = $map_students->student_ids;

            $students = DB::select("SELECT ST.student_id,ST.student_image,ST.student_enroll_number,ST.student_roll_no,ST.student_name ,IMPST.imprest_ac_amt ,IMPST.imprest_ac_id FROM `students` as ST LEFT JOIN `imprest_ac` AS IMPST  ON  IMPST.student_id = ST.student_id WHERE ST.student_id IN (".$studentIds.") AND ST.student_id NOT IN (SELECT DISTINCT student_id FROM `fee_receipt` LEFT JOIN `fee_receipt_details` ON fee_receipt.receipt_id = fee_receipt_details.receipt_id where `head_id` = '".$fees_info->ot_head_id."'  AND `head_type` = 'ONETIME' ) ");
            if(!empty($students)){
                foreach ($students as $student){
                    $student_image = '';
                    if (!empty($student->student_image))
                    {
                        $student_image = check_file_exist($student->student_image, 'student_image');
                    } else {
                        $student_image = "";
                    }
                    $able_to_pay = 0;
                    if($student->imprest_ac_amt >= $fees_info->ot_amount){
                        $able_to_pay = 1;
                    }
                    $all_students[] = array(
                        'student_id'            => $student->student_id,
                        'imprest_ac_id'         => $student->imprest_ac_id,
                        'ot_head_id'            => $fees_info->ot_head_id,
                        'head_type'             => 'ONETIME',
                        'inst_amt'              => $fees_info->ot_amount,
                        'inst_paid_amt'         => $fees_info->ot_amount,
                        'class_id'              => $request->get('class_id'),
                        'section_id'            => $request->get('section_id'),
                        'inst_status'           => 1,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student->student_enroll_number,
                        'student_roll_no'       => $student->student_roll_no,
                        'student_name'          => $student->student_name,
                        'imprest_ac_amt'        => $student->imprest_ac_amt,
                        'able_to_pay'           => $able_to_pay
                    );
                }
            }
        } 
        return Datatables::of($all_students)
        ->addColumn('student_profile', function ($all_students)
        {
            $profile = "";
            if($all_students['profile'] != ''){
                $profile = "<img src=".$all_students['profile']." height='30' />";
            }   
            $name = $all_students['student_name'];
            $complete = $profile."  ".$name;
            return $complete;
        })
        ->addColumn('imprest_ac_amt', function ($all_students)
        {
            $msg = "";
            if($all_students['able_to_pay'] == 0){
                $msg = "(Insufficient balance)";
            } 
            $amount = $all_students['imprest_ac_amt'];
            $complete = $amount."".$msg;
            return $complete;
        })
        ->addColumn('select', function ($all_students)
        {
            $disabled = "disabled";
            $class = "";
            if($all_students['able_to_pay'] == 1){
                $disabled = "";
                $class = "check";
            }
            $checkBox ='
            <input type="hidden" name="students['.$all_students['student_id'].'][imprest_ac_id]" value="'.$all_students['imprest_ac_id'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][ot_head_id]" value="'.$all_students['ot_head_id'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][head_type]" value="'.$all_students['head_type'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][inst_amt]" value="'.$all_students['inst_amt'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][inst_paid_amt]" value="'.$all_students['inst_paid_amt'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][class_id]" value="'.$all_students['class_id'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][section_id]" value="'.$all_students['section_id'].'" />
            <input type="hidden" name="students['.$all_students['student_id'].'][inst_status]" value="'.$all_students['inst_status'].'" />
            <div class="checkbox">
            <input id="checkbox'.$all_students['student_id'].'" type="checkbox" name="students['.$all_students['student_id'].'][student_id]" class="'.$class.'" value="'.$all_students['student_id'].'" '.$disabled.'>
            <label class="from_one1" for="checkbox'.$all_students['student_id'].'" style="margin-bottom: 4px !important;"></label>
            </div>';
            
            return $checkBox;
        })->rawColumns(['action' => 'action','student_profile'=>'student_profile','imprest_ac_amt'=> 'imprest_ac_amt', 'select'=> 'select'])->addIndexColumn()->make(true);
       

    }

    public function fees_deduction(Request $request){
        // p($request->all());
        
        $session = get_current_session();
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $msg = "Please select atleast one student.";
        $status = "select";
        if($request->get('students')){
            foreach($request->get('students') as $students){
                if($students['student_id'] != ""){
                    DB::beginTransaction(); //Start transaction!
                    try
                    {
                        $imprestAc  = ImprestAc::find($students['imprest_ac_id']);
                        $receipt = New Receipt();
                        $receipt->admin_id                      = $admin_id;
                        $receipt->update_by                     = $loginInfo['admin_id'];
                        $receipt->session_id                    = $session['session_id'];
                        $receipt->class_id                      = $students['class_id'];
                        $receipt->student_id                    = $students['student_id'];
                        $receipt->receipt_date                  = date('Y-m-d');
                        $receipt->total_amount                  = $students['inst_amt'];
                        $receipt->net_amount                    = $students['inst_amt'];
                        $receipt->imprest_amount                = $students['inst_amt'];
                        $receipt->save();
                        $receipt->receipt_number                = $receipt->receipt_id;
                        $receipt->save();
                         
                        $receipt_detail = new ReceiptDetail();
                        $receipt_detail->admin_id           = $admin_id;
                        $receipt_detail->update_by          = $loginInfo['admin_id'];
                        $receipt_detail->receipt_id         = $receipt->receipt_id;
                        $receipt_detail->head_id            = $students['ot_head_id'];
                        $receipt_detail->head_type          = $students['head_type'];
                        $receipt_detail->inst_amt           = $students['inst_amt'];
                        $receipt_detail->inst_paid_amt      = $students['inst_amt'];
                        $receipt_detail->inst_status        = 1;
                        $receipt_detail->save();
                        
                        $imprest_ac_amt = $imprestAc->imprest_ac_amt - $students['inst_amt'];
                        $imprestAc->imprest_ac_amt = $imprest_ac_amt;
                        $imprestAc->save();

                        $msg = "Deduction successfully complete";
                        $status = "success";
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $msg = $e->getMessage();
                        $status = "error";
                    }
                    
                    DB::commit();
                }
            }
        }
        $response = array(
            'status'    => $status,
            'msg'       => $msg
        );
        return json_encode($response);
    }

    public function get_student_count(Request $request){
        // p($request->all());
        $session    = get_current_session();
        $all_students = $map_students = $student_ids = [];
        $total_apply_students = $unpay_students = $pay_students =  0;
        $map_students = FeesStMap::where(function($query) use ($request,$session)
        {
            $query->where('head_type', '=','ONETIME');
            $query->where('session_id',$session['session_id']);
            if (!empty($request) && !empty($request->get('head_id')) && $request->get('head_id') != null){
                $query->where('head_id',$request->get('head_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $query->where('class_id',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $query->where('section_id',$request->get('section_id'));
            }
        })->select('student_ids')->orderBy('fees_st_map_id', 'DESC')->first();
        //$map_students =  (array) $map_students;
        // p($map_students->student_ids);
        if(isset($map_students->student_ids) && $map_students->student_ids != "") {
            $student_ids  = explode(",",$map_students->student_ids);
        }
        $total_apply_students = COUNT($student_ids);

        if(!empty($map_students)) {
            $unpay_students_info = DB::select("SELECT ST.student_id FROM `students` as ST  WHERE ST.student_id IN (".$map_students->student_ids.") AND ST.student_id NOT IN (SELECT DISTINCT student_id FROM `fee_receipt` LEFT JOIN `fee_receipt_details` ON fee_receipt.receipt_id = fee_receipt_details.receipt_id where `head_id` = '".$request->get('head_id')."'  AND `head_type` = 'ONETIME' ) ");
            $unpay_students = COUNT($unpay_students_info);
        }
        $pay_students = $total_apply_students - $unpay_students;

        $finalArray = array(
            'total_students' => $total_apply_students,
            'pay_students' => $pay_students,
            'unpay_students'  => $unpay_students
        );

        return json_encode($finalArray);
        p($unpay_students);
    }

}
