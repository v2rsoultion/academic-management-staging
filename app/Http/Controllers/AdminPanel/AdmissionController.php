<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Admission\AdmissionForm;
use App\Model\Admission\AdmissionFields;
use App\Model\Admission\AdmissionData;
use App\Model\Staff\Staff;
use Yajra\Datatables\Datatables;

class AdmissionController extends Controller
{
    /**
     *  View page for Admission form
     *  @Shree on 11 Sept 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $admissionForm = [];
        $arr_medium   = get_all_mediums();
        $admissionForm['arr_session'] = $arr_sessions;
        $admissionForm['arr_medium'] = $arr_medium;
        $data = array(
            'page_title'    => trans('language.view_forms'),
            'redirect_url'  => url('admin-panel/admission/view-forms'),
            'login_info'    => $loginInfo,
            'admissionForm' => $admissionForm
        );
        return view('admin-panel.admission-form.index')->with($data);
    }

    /**
     *  View page for Admission form
     *  @Shree on 11 Sept 2018
    **/
    public function admission_form_index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $admissionForm = [];
        $arr_medium   = get_all_mediums();
        $admissionForm['arr_session'] = $arr_sessions;
        $admissionForm['arr_medium'] = $arr_medium;
        $data = array(
            'page_title'    => trans('language.admission_form'),
            'redirect_url'  => url('admin-panel/admission/view-forms'),
            'login_info'    => $loginInfo,
            'admissionForm' => $admissionForm
        );
        return view('admin-panel.admission-form.admission_form_index')->with($data);
    }
     /**
     *  View page for Admission form
     *  @Shree on 11 Sept 2018
    **/
    public function enquiry_form_index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $admissionForm = [];
        $arr_medium   = get_all_mediums();
        $admissionForm['arr_session'] = $arr_sessions;
        $admissionForm['arr_medium'] = $arr_medium;
        $data = array(
            'page_title'    => trans('language.enquiry_form'),
            'redirect_url'  => url('admin-panel/admission/view-forms'),
            'login_info'    => $loginInfo,
            'admissionForm' => $admissionForm
        );
        return view('admin-panel.admission-form.enquiry_form_index')->with($data);
    }

    /**
     *  Add page for admission
     *  @Shree on 11 Sept 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data               = [];
        $admission          = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_sessions       = get_arr_session();
        $arr_brochure       = [];
        $arr_class          = [];
        
        $admission['form_type'] = 1;
        $arr_medium         = get_all_mediums();
        $arr_form_type      = \Config::get('custom.form_type');
        $arr_email_receipt  = \Config::get('custom.email_receipt');
        $arr_message_via    = \Config::get('custom.message_via');
        $arr_payment_mode    = \Config::get('custom.payment_mode');
        if (!empty($id))
        {
            $decrypted_admission_form_id = get_decrypted_value($id, true);
            
            $admission              = AdmissionForm::Find($decrypted_admission_form_id);
            $arr_class              = get_all_classes($admission->medium_type);
            $arr_brochure           = get_all_brochure($admission->session_id);
            if (!$admission)
            {
                return redirect('admin-panel/admission/create-form')->withError('form not found!');
            }
            $page_title           = trans('language.edit_form');
            $decrypted_admission_form_id = get_encrypted_value($admission->admission_form_id, true);
            $save_url             = url('admin-panel/admission/save/' . $decrypted_admission_form_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.create_form');
            $save_url      = url('admin-panel/admission/save');
            $submit_button = 'Save';
        }
        $admission['arr_session']       = $arr_sessions;
        $admission['arr_class']         = $arr_class;
        $admission['arr_medium']        = add_blank_option($arr_medium, 'Select Medium');
        $admission['arr_form_type']     = add_blank_option($arr_form_type, 'Form Type');
        $admission['arr_email_receipt'] = $arr_email_receipt;
        $admission['arr_message_via']   = $arr_message_via;
        $admission['arr_brochure']      = add_blank_option($arr_brochure, 'Select Brochure');
        $admission['arr_payment_mode']  = $arr_payment_mode;
       
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'admission'     => $admission,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/admission/view-forms'),
        );
        return view('admin-panel.admission-form.add')->with($data);
    }

    
    /**
     *  Add and update Admission form's data
     *  @Shree on 10 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo                      = get_loggedin_user_data();
        $decrypted_admission_form_id    = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $admissionform = AdmissionForm::find($decrypted_admission_form_id);

            if (!$admissionform)
            {
                return redirect('/admin-panel/admission/create-form/')->withError('Form not found!');
            }
            $success_msg = 'Form updated successfully!';
        }
        else
        {
            $admissionform     = New AdmissionForm;
            $success_msg = 'Form saved successfully!';
            $decrypted_admission_form_id = null;
        }
        $validatior = Validator::make($request->all(), [
                'form_name'             => 'required|unique:admission_forms,form_name,' . $decrypted_admission_form_id . ',admission_form_id',
                'session_id'            => 'required',
                'class_id'              => 'required',
                'medium_type'           => 'required',
                'no_of_intake'          => 'required',
                'form_prefix'           => 'required',
                'form_type'             => 'required',
                'form_success_message'  => 'required',
                'form_failed_message'   => 'required',
                'form_message_via'      => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $form_pay_mode = null;
                if($request->get('form_pay_mode') != "") {
                    $form_pay_mode = implode(",",Input::get('form_pay_mode'));
                }
                $admissionform->admin_id                = $loginInfo['admin_id'];
                $admissionform->update_by               = $loginInfo['admin_id'];
                $admissionform->form_name               = Input::get('form_name');
                $admissionform->medium_type             = Input::get('medium_type');
                $admissionform->session_id              = Input::get('session_id');
                $admissionform->class_ids               = implode(",",Input::get('class_id'));
                $admissionform->no_of_intake             = Input::get('no_of_intake1');
                $admissionform->form_prefix             = Input::get('form_prefix');
                $admissionform->form_type               = Input::get('form_type');
                $admissionform->form_message_via        = Input::get('form_message_via');
                $admissionform->brochure_id             = Input::get('brochure_id');
                $admissionform->form_last_date          = Input::get('form_last_date');
                $admissionform->form_fee_amount         = Input::get('form_fee_amount');
                $admissionform->form_pay_mode           = $form_pay_mode;
                $admissionform->form_email_receipt      = Input::get('form_email_receipt');
                $admissionform->form_instruction        = Input::get('form_instruction');
                $admissionform->form_information        = Input::get('form_information');
                $admissionform->form_success_message    = Input::get('form_success_message');
                $admissionform->form_failed_message     = Input::get('form_failed_message');
              
                $admissionform->save();
                $encrypted_admission_form_id = get_encrypted_value($admissionform->admission_form_id, true);
                if($request->get('form_type') == 0) {
                    $admissionform->form_url     = url('/admission-form/'.$encrypted_admission_form_id);
                } else {
                    $admissionform->form_url     = url('/enquiry-form/'.$encrypted_admission_form_id);
                }
                $admissionform->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/admission/view-forms')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 12 Sept 2018
    **/
    public function anyData(Request $request)
    {
        // p($request->all());
        $loginInfo = get_loggedin_user_data();
        $admissionForm = AdmissionForm::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
            {
                $query->where('session_id', "=", $request->get('session_id'));
            }
            if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('form_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request)  && $request->get('form_type') != null)
            {
                $query->where('form_type', "=", $request->get('form_type'));
            }
        })->orderBy('admission_form_id', 'DESC')->with('getSessionInfo')->get();
        // p($admissionForm);
        return Datatables::of($admissionForm)
       
        ->addColumn('session_name', function ($admissionForm)
        {
           return $admissionForm->getSessionInfo['session_name'];
        }) 
        ->addColumn('class_name', function ($admissionForm)
        {
            $class_names = [];
            $class_ids = explode(",",$admissionForm->class_ids);
            if(!empty($class_ids)) {
            foreach($class_ids as $class_id){
                $class_names[] = get_class_name($class_id);
            }}
           return implode(",", $class_names);
        }) 
        ->addColumn('form_type', function ($admissionForm)
        {
           $arr_form_type      = \Config::get('custom.form_type');
           return $arr_form_type[$admissionForm->form_type];
        }) 
        ->addColumn('medium_type', function ($admissionForm)
        {
           $arr_medium      = get_all_mediums();
           return $arr_medium[$admissionForm->medium_type];
        }) 
        ->addColumn('admission_link', function ($admissionForm)
        {
            $encrypted_admission_form_id = get_encrypted_value($admissionForm->admission_form_id, true);
            $formLink = '-----';
            $checkManageFields = check_admission_field_availability($admissionForm->admission_form_id);
            if($checkManageFields == "true"){
                if($admissionForm->form_type == 0) {
                    // For admission
                    $formLink = '
                    <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Manage Fields" href="fill-admission-form/' . $encrypted_admission_form_id . '" "">Admission Form</a>
                    </li> </ul>
                    </div>';
                } else {
                    // For enquiry
                    $formLink = '
                    <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Manage Fields" href="fill-enquiry-form/' . $encrypted_admission_form_id . '" "">Enquiry Form</a>
                    </li> </ul>
                    </div>';
                }
            }
            return $formLink;
        }) 
        ->addColumn('form_link', function ($admissionForm)
        {
            $encrypted_admission_form_id = get_encrypted_value($admissionForm->admission_form_id, true);
            if($admissionForm->form_type == 0) {
                // For admission
                $form_link = '<a target="_blank" href="'.url('admission-form/'.$encrypted_admission_form_id.'').'" class="btn btn-raised btn-primary">Admission Form Link</a>';
            } else {
                // For enquiry
                $form_link = '<a target="_blank" href="'.url('enquiry-form/'.$encrypted_admission_form_id.'').'" class="btn btn-raised btn-primary">Enquiry Form Link</a>';
            }
            return $form_link;
        }) 
        ->addColumn('action', function ($admissionForm)
        {
            $encrypted_admission_form_id = get_encrypted_value($admissionForm->admission_form_id, true);
            if($admissionForm->form_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            $formLink = "";
            $checkManageFields = check_admission_field_availability($admissionForm->admission_form_id);
            if($checkManageFields == "true"){
                if($admissionForm->form_type == 0) {
                    // For admission
                    $formLink = '<li>
                                    <a title="Manage Fields" href="'.url('admin-panel/admission-form/fill-admission-form/'. $encrypted_admission_form_id .'').'" >Admission Form</a>
                                </li>';
                } else {
                    // For enquiry
                    $formLink = '<li>
                                    <a title="Manage Fields" href="'.url('admin-panel/admission-form/fill-enquiry-form/'. $encrypted_admission_form_id .'').'" >Enquiry Form</a>
                                </li>';
                }
            }
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Manage Fields" href="manage-fields/' . $encrypted_admission_form_id . '" "">Manage Fields</a>
                    </li>
                    '.$formLink.'
                    </ul>
                </div>
                <div class="pull-left"><a href="'.url('admin-panel/admission/form-status/'.$status.'/' . $encrypted_admission_form_id .'').'">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/admission/create-form/' . $encrypted_admission_form_id.'').'"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/admission/delete-admission-form/' . $encrypted_admission_form_id .'').'" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['admission_link'=>'admission_link','action' => 'action','form_link' => 'form_link'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy admission form's data
     *  @Shree on 12 Sept 2018
    **/
    public function destroy($id)
    {
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::find($admission_form_id);
        $success_msg = $error_message =  "";
        if ($admissionForm)
        {
            DB::beginTransaction();
            try
            {
                $admissionForm->delete();
                $success_msg = "Form deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/admission/view-forms')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/admission/view-forms')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Form not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change admission form's status
     *  @Shree on 12 Sept 2018
    **/
    public function changeStatus($status,$id)
    {
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::find($admission_form_id);
        
        if ($admissionForm)
        {
            $admissionForm->form_status  = $status;
            $admissionForm->save();
            $success_msg = "Form status updated successfully!";
            return redirect('admin-panel/admission/view-forms')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Form not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function manageFieldsView($id){
        $loginInfo  = get_loggedin_user_data();
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::find($admission_form_id);
        if ($admissionForm)
        {
            $arr_class = get_all_classes($admissionForm['medium_type']);
            $payment_mode       = \Config::get('custom.payment_mode');
            $arr_medium         = get_all_mediums();
            $arr_student_fields = \Config::get('custom.student_fields');
            $arr_sessions       = get_arr_session();
            $admissionForm['arr_session'] = $arr_sessions;
            $admissionForm['arr_class'] = add_blank_option($arr_class, 'Select Class');
            $admissionForm['arr_medium'] = add_blank_option($arr_medium, 'Medium Type');
            $admissionForm['arr_student_fields'] = $arr_student_fields;
            $admissionForm['payment_mode'] = $payment_mode;
            
            $fields = AdmissionFields::where(array('admission_form_id'=> $admission_form_id))->get()->toArray();
            if(!empty($fields[0])){
                if($fields[0]['form_keys'] != ""){
                    $form_keys = explode(',', $fields[0]['form_keys']);
                    $fields[0]['form_keys'] = $form_keys;
                }
                if($fields[0]['form_pay_mode'] != ""){
                    $form_pay_mode = explode(',', $fields[0]['form_pay_mode']);
                    $fields[0]['form_pay_mode'] = $form_pay_mode;
                }
                $admissionForm['fields'] = $fields[0];
                $save_url             = url('admin-panel/admission-form/save-manage-fields/' . get_encrypted_value($fields[0]['admission_field_id'],true));
            } else {
                $admissionForm['fields'] = [];
                $save_url             = url('admin-panel/admission-form/save-manage-fields');
            }
            $submit_button        = 'Update';
            $data = array(
                'page_title'    => trans('language.manage_form'),
                'redirect_url'  => url('admin-panel/admission/view-forms'),
                'login_info'    => $loginInfo,
                'admissionForm' => $admissionForm,
                'save_url'      => $save_url,
                'submit_button' => $submit_button
            );
            // p($admissionForm);
            return view('admin-panel.admission-form.manage-fields')->with($data);
        }  else {
            $error_message = "Form not found!";
            return redirect('admin-panel/admission/view-forms')->withErrors($error_message);
        }
    }

    /**
     *  Save admission Admission form's fields
     *  @Shree on 18 Sept 2018
    **/
    public function saveManageFields(Request $request, $id = NULL)
    {
        $loginInfo                      = get_loggedin_user_data();
        $decrypted_admission_form_id    = get_decrypted_value($id, true);
        $fields = $form_pay_mode = null;
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $fields = AdmissionFields::find($decrypted_admission_form_id);
            $admin_id = $fields['admin_id'];
            if (!$fields)
            {
                return redirect('/admin-panel/admission/create-form/')->withError('Form not found!');
            }
            $success_msg = 'Form fields updated successfully!';
        }
        else
        {
            $fields     = New AdmissionFields;
            $success_msg = 'Form fields saved successfully!';
            $decrypted_admission_form_id = null;
        }
        DB::beginTransaction();
        try
        {
            if($request->get('form_pay_mode') != ""){
                $form_pay_mode = implode(",",Input::get('form_pay_mode'));
            }
            if($request->get('fields') != ""){
                $form_keys = implode(",",Input::get('fields'));
            }
            $fields->admin_id           = $admin_id;
            $fields->update_by          = $loginInfo['admin_id'];
            $fields->admission_form_id  = Input::get('admission_form_id');
            $fields->form_keys          = $form_keys;
            $fields->form_pay_mode      = $form_pay_mode;
            $fields->save();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();

        return redirect()->back()->withSuccess($success_msg);
    }

    public function showForm($id){
        $loginInfo  = get_loggedin_user_data();
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::where(array('admission_form_id'=> $admission_form_id))->with('getFormFieldsInfo')->get();
        $admissionForm              = isset($admissionForm[0]) ? $admissionForm[0] : [];
        $medium_type = $admissionForm->medium_type;

        if ($admissionForm)
        {
            $arr_student_type       = \Config::get('custom.student_type');
            $arr_gender             = \Config::get('custom.student_gender');
            $arr_category           = \Config::get('custom.student_category');
            $arr_annual_salary      = \Config::get('custom.annual_salary');
            $arr_fees               = \Config::get('custom.fees_status');
            $arr_mode               = \Config::get('custom.mode');
            $arr_city_p = $arr_city_t = $arr_state_p = $arr_state_t = $admit_arr_section = $arr_section = [];
            $arr_country                   = get_all_country();
            
            $arr_session                   = get_arr_session();
            $arr_caste                     = get_caste();
            $arr_religion                  = get_religion();
            $arr_natioanlity               = get_nationality();
            $arr_group                     = get_student_groups();
            $arr_document_category         = get_all_document_category(0);
            $arr_class                     = get_all_classes($medium_type);
            $arr_stream                    = get_all_streams($medium_type);
            $arr_title                     = get_titles();
            
            $admissionForm['arr_gender']         = $arr_gender;
            $admissionForm['arr_category']       = $arr_category;
            $admissionForm['arr_annual_salary']  = $arr_annual_salary;
            $admissionForm['arr_student_type']   = $arr_student_type;
            $admissionForm['arr_title']        = add_blank_option($arr_title, 'Select Title');
            $admissionForm['arr_country']        = add_blank_option($arr_country, 'Select Country');
            $admissionForm['arr_session']        = add_blank_option($arr_session, 'Select session');
            $admissionForm['arr_state_p']        = add_blank_option($arr_state_p, 'Select State');
            $admissionForm['arr_city_p']         = add_blank_option($arr_city_p, 'Select City');
            $admissionForm['arr_state_t']        = add_blank_option($arr_state_t, 'Select State');
            $admissionForm['arr_city_t']         = add_blank_option($arr_city_t, 'Select City');
            $admissionForm['arr_class']          = add_blank_option($arr_class, 'Select class');
            $admissionForm['arr_stream']         = add_blank_option($arr_stream, 'Select Stream');
            $admissionForm['arr_caste']          = add_blank_option($arr_caste, 'Select caste');
            $admissionForm['arr_religion']       = add_blank_option($arr_religion, 'Select religion');
            $admissionForm['arr_section']        = add_blank_option($arr_section, 'Select section');
            $admissionForm['admit_arr_section']  = add_blank_option($admit_arr_section, 'Select section');
            $admissionForm['arr_natioanlity']    = add_blank_option($arr_natioanlity, 'Select nationality');
            $admissionForm['arr_group']          = add_blank_option($arr_group, 'Select house/group');
            $admissionForm['arr_fees']           = add_blank_option($arr_fees, 'Fees Status');
            $admissionForm['arr_mode']           = add_blank_option($arr_mode, 'Mode');
            $stadmissionFormudent['arr_document_category']   = add_blank_option($arr_document_category, 'Document category');
            if($admissionForm['getFormFieldsInfo']['form_keys'] != ""){
                $form_keys = explode(',', $admissionForm['getFormFieldsInfo']['form_keys']);
                $admissionForm['form_keys'] = $form_keys;
            }
            if($admissionForm->form_type == 0){
                $page_title = trans('language.admission_form');
            } else {
                $page_title = trans('language.enquiry_form');
            }
            // p($admissionForm);
            $save_url             = url('admin-panel/admission-form/save-data-form');
            $submit_button        = 'Update';
            $data = array(
                'page_title'    => $page_title,
                'redirect_url'  => url('admin-panel/admission/view-forms'),
                'login_info'    => $loginInfo,
                'admissionForm' => $admissionForm,
                'save_url'      => $save_url,
                'submit_button' => $submit_button
            );
            // p($admissionForm);
            return view('admin-panel.admission-form.enquiry-form')->with($data);
        }  else {
            $error_message = "Form not found!";
            return redirect('admin-panel/admission/view-forms')->withErrors($error_message);
        }
    }

    /**
     *  Save admission form data
     *  @Shree on 18 Sept 2018
    **/
    public function saveDataForm(Request $request, $id = NULL)
    {
        // p($request->all());
        $loginInfo                      = get_loggedin_user_data();
        $decrypted_admission_data_id   = get_decrypted_value($id, true);
        $fields = $form_pay_mode = null;
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $admissionData = AdmissionData::find($decrypted_admission_data_id);
            $admin_id = $admissionData['admin_id'];
            if (!$admissionData)
            {
                return redirect()->back()->withErrors('Form not found!');
            }
            if($admissionData->form_type == 0){
                $success_msg = 'Admission Form data updated successfully!';
            } else if($admissionData->form_type == 1){
                $success_msg = 'Enquiry Form data updated successfully!';
            }
           
        }
        else
        {
            $admissionData     = New AdmissionData;
            $success_msg = 'Form data saved successfully!';
            $decrypted_admission_form_id = null;
        }
        $form_number = get_form_number($request->get('admission_form_id'));
                
        DB::beginTransaction();
        try
        {
            $student_gender = 0;
            if($request->get('student_gender') != ""){
                $student_gender = $request->get('student_gender');
            }
            $student_type = 0;
            if($request->get('student_type') != ""){
                $student_type = $request->get('student_type');
            }
            $admission_data_fees = 0;
            if($request->get('admission_data_fees') != ""){
                $admission_data_fees = $request->get('admission_data_fees');
            }
            $admission_data_mode = 0;
            if($request->get('admission_data_mode') != ""){
                $admission_data_mode = $request->get('admission_data_mode');
            }
            if(empty($id)){
                $admissionData->form_number = $form_number;
            }
            $paymodes = '';
            if($request->get('paymodes') != ""){
                $paymodes = implode(',',$request->get('paymodes'));
            }
            $admissionData->admin_id           = $admin_id;
            $admissionData->update_by          = $loginInfo['admin_id'];
            $admissionData->admission_form_id  = Input::get('admission_form_id');
            $admissionData->form_type          = Input::get('form_type');
            $admissionData->student_enroll_number      = Input::get('student_enroll_number');
            $admissionData->student_roll_no      = Input::get('student_roll_no');
            $admissionData->student_name      = Input::get('student_name');
            $admissionData->student_reg_date      = Input::get('student_reg_date');
            $admissionData->student_dob      = Input::get('student_dob');
            $admissionData->student_email      = Input::get('student_email');
            $admissionData->student_gender      = $student_gender;
            $admissionData->student_type      = $student_type;
            $admissionData->medium_type      = Input::get('medium_type');
            $admissionData->student_category      = Input::get('student_category');
            $admissionData->title_id      = Input::get('title_id');
            $admissionData->caste_id      = Input::get('caste_id');
            $admissionData->religion_id      = Input::get('religion_id');
            $admissionData->nationality_id      = Input::get('nationality_id');
            $admissionData->student_sibling_name      = Input::get('student_sibling_name');
            $admissionData->student_sibling_class_id      = Input::get('student_sibling_class_id');
            $admissionData->student_adhar_card_number      = Input::get('student_adhar_card_number');
            $admissionData->student_privious_school      = Input::get('student_privious_school');
            $admissionData->student_privious_class      = Input::get('student_privious_class');
            $admissionData->student_privious_tc_no      = Input::get('student_privious_tc_no');
            $admissionData->student_privious_tc_date      = Input::get('student_privious_tc_date');
            $admissionData->student_privious_result      = Input::get('student_privious_result');
            $admissionData->student_temporary_address      = Input::get('student_temporary_address');
            $admissionData->student_temporary_city      = Input::get('student_temporary_city');
            $admissionData->student_temporary_state      = Input::get('student_temporary_state');
            $admissionData->student_temporary_county      = Input::get('student_temporary_county');
            $admissionData->student_temporary_pincode      = Input::get('student_temporary_pincode');
            $admissionData->student_permanent_address      = Input::get('student_permanent_address');
            $admissionData->student_permanent_city      = Input::get('student_permanent_city');
            $admissionData->student_permanent_state      = Input::get('student_permanent_state');
            $admissionData->student_permanent_county      = Input::get('student_permanent_county');
            $admissionData->student_permanent_pincode      = Input::get('student_permanent_pincode');
            $admissionData->admission_session_id      = Input::get('session_id');
            $admissionData->admission_class_id      = Input::get('current_class_id');
            $admissionData->admission_section_id      = Input::get('admission_section_id');
            $admissionData->current_session_id      = Input::get('session_id');
            $admissionData->current_class_id      = Input::get('current_class_id');
            $admissionData->current_section_id      = Input::get('current_section_id');
            $admissionData->student_unique_id      = Input::get('student_unique_id');
            $admissionData->group_id      = Input::get('group_id');
            $admissionData->stream_id      = Input::get('stream_id');
            $admissionData->student_height      = Input::get('student_height');
            $admissionData->student_weight      = Input::get('student_weight');
            $admissionData->student_blood_group      = Input::get('student_blood_group');
            $admissionData->student_vision_left      = Input::get('student_vision_left');
            $admissionData->student_vision_right      = Input::get('student_vision_right');
            $admissionData->medical_issues      = Input::get('medical_issues');
            $admissionData->student_login_name      = Input::get('student_login_name');
            $admissionData->student_login_email      = Input::get('student_login_email');
            $admissionData->student_login_contact_no      = Input::get('student_login_contact_no');
            $admissionData->student_father_name      = Input::get('student_father_name');
            $admissionData->student_father_mobile_number      = Input::get('student_father_mobile_number');
            $admissionData->student_father_email      = Input::get('student_father_email');
            $admissionData->student_father_occupation      = Input::get('student_father_occupation');
            $admissionData->student_father_annual_salary      = Input::get('student_father_annual_salary');
            $admissionData->student_mother_name      = Input::get('student_mother_name');
            $admissionData->student_mother_mobile_number      = Input::get('student_mother_mobile_number');
            $admissionData->student_mother_email      = Input::get('student_mother_email');
            $admissionData->student_mother_occupation      = Input::get('student_mother_occupation');
            $admissionData->student_mother_annual_salary      = Input::get('student_mother_annual_salary');
            $admissionData->student_mother_tongue      = Input::get('student_mother_tongue');
            $admissionData->student_guardian_name      = Input::get('student_guardian_name');
            $admissionData->student_guardian_email      = Input::get('student_guardian_email');
            $admissionData->student_guardian_mobile_number      = Input::get('student_guardian_mobile_number');
            $admissionData->student_guardian_relation      = Input::get('student_guardian_relation');
            $admissionData->admission_data_fees      = $admission_data_fees;
            $admissionData->admission_data_mode      = $admission_data_mode;
            $admissionData->admission_data_pay_mode      = $paymodes;
            
            if ($request->hasFile('student_image'))
            {
                if (!empty($id)){
                    $profile = check_file_exist($admissionData->student_image, 'admission_image');
                    if (!empty($profile))
                    {
                        unlink($profile);
                    } 
                }
                $file                          = $request->file('student_image');
                $config_upload_path            = \Config::get('custom.admission_image');
                $destinationPath               = public_path() . $config_upload_path['upload_path'];
                $ext                           = substr($file->getClientOriginalName(),-4);
                $name                          = substr($file->getClientOriginalName(),0,-4);
                $filename                      = $name.mt_rand(0,100000).time().$ext;
                $file->move($destinationPath, $filename);
                $admissionData->student_image = $filename;
            }
            $admissionData->save();
            $class_name = get_class_name($request->get('current_class_id'));
            // For Enquiry
            if($request->get('form_type') == 1) {
                $send_notification  = $this->send_push_notification($admissionData->admission_data_id,$admissionData->form_number,$class_name,'admission_new_enquiry');
            } else if($request->get('form_type') == 0){
                // For admission
                $send_notification  = $this->send_push_notification($admissionData->admission_data_id,$admissionData->form_number,$class_name,'admission_new_applicant');
            } 
            
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        if($admissionData->form_type == 0){
            return redirect('admin-panel/admission-form/manage-admissions')->withSuccess($success_msg);
        } else if($admissionData->form_type == 1){
            return redirect('admin-panel/admission-form/manage-enquiries')->withSuccess($success_msg);
        } else {
            return redirect()->back()->withSuccess($success_msg);
        }
    }

    public function showEnquiries()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $formData = $arr_class = [];
        $formData['arr_session'] = $arr_sessions;
        $arr_class = get_all_classes_mediums();
        $formData['arr_class'] = add_blank_option($arr_class,'Select Class');
        $data = array(
            'page_title'    => trans('language.manage_enquiries'),
            'redirect_url'  => url('admin-panel/admission-form/manage-enquiries'),
            'login_info'    => $loginInfo,
            'formData' => $formData
        );
        return view('admin-panel.admission-form.manage-enquiries')->with($data);
    }

    public function showAdmissions()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $formData = $arr_class = [];
        $formData['arr_session'] = $arr_sessions;
        $arr_class = get_all_classes_mediums();
        $formData['arr_class'] = add_blank_option($arr_class,'Select Class');
        $data = array(
            'page_title'    => trans('language.manage_admissions'),
            'redirect_url'  => url('admin-panel/admission-form/manage-admissions'),
            'login_info'    => $loginInfo,
            'formData' => $formData
        );
        return view('admin-panel.admission-form.manage-admissions')->with($data);
    }

    function getEnquiriesData(Request $request){
        $loginInfo = get_loggedin_user_data();
        $formData = AdmissionData::where(function($query) use ($request) 
        {
            $query->where('form_type','1');
            if (!empty($request) && !empty($request->get('form_number')) && $request->get('form_number') != null)
            {
                $query->where('form_number', "=", $request->get('form_number'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null)
            {
                $query->where('current_class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
            {
                $query->where('current_session_id', "=", $request->get('session_id'));
            }
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
        })->orderBy('admission_data_id', 'DESC')->with('getFormInfo')->with('getClassInfo')->with('getSessionInfo')->get();
        return Datatables::of($formData)
       
        ->addColumn('class_session', function ($formData)
        {
           return $formData->getClassInfo['class_name'].' - '.$formData->getSessionInfo['session_name'];
        }) 
        ->addColumn('mobile', function ($formData)
        {
            $mobile = $formData->student_father_mobile_number;
            if($formData->student_father_mobile_number == null){
                $mobile = "---";
            }
           return $mobile;
        }) 
        ->addColumn('father_name', function ($formData)
        {
            $father_name = $formData->student_father_name;
            if($formData->student_father_name == null){
                $father_name = "---";
            }
           return $father_name;
        }) 
        ->addColumn('status', function ($formData)
        {
            if($formData->admission_data_status == 0){
                $status = "Pending";
            } else if($formData->admission_data_status == 1){
                $status = "Contacted";
            } else if($formData->admission_data_status == 2){
                $status = "Admitted";
            }
           return $status;
        }) 
        ->addColumn('action', function ($formData)
        {
            $encrypted_admission_data_id = get_encrypted_value($formData->admission_data_id, true);
            
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="View Details" href="form-data/' . $encrypted_admission_data_id . '">View Details</a>
                    </li>
                   
                    </ul>
                </div>
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-success btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-plus-circle"></i> 
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                   
                    <li>
                        <a title="Pending" href="enquiry-status/0/' . $encrypted_admission_data_id . '" "">Pending</a>
                    </li>
                    <li>
                        <a title="Contacted" href="enquiry-status/1/' . $encrypted_admission_data_id . '" "">Contacted</a>
                    </li>
                    <li>
                        <a title="Admitted" href="enquiry-status/2/' . $encrypted_admission_data_id . '" "">Admitted</a>
                    </li>
                   
                    </ul>
                </div>
              ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    function getAdmissionsData(Request $request){
        $loginInfo = get_loggedin_user_data();
        $formData = AdmissionData::where(function($query) use ($request) 
        {
            $query->where('form_type','!=', '1');
            if (!empty($request) && !empty($request->get('form_number')) && $request->get('form_number') != null)
            {
                $query->where('form_number', "=", $request->get('form_number'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null)
            {
                $query->where('current_class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
            {
                $query->where('current_session_id', "=", $request->get('session_id'));
            }
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
            $query->where('admission_data_status', "=", 0);
            $query->orWhere('admission_data_status', "=", 1);
        })->orderBy('admission_data_id', 'DESC')->with('getFormInfo')->with('getClassInfo')->with('getSessionInfo')->get();
        return Datatables::of($formData)
       
        ->addColumn('class_session', function ($formData)
        {
           return $formData->getClassInfo['class_name'].' - '.$formData->getSessionInfo['session_name'];
        }) 
        ->addColumn('mobile', function ($formData)
        {
            $mobile = $formData->student_father_mobile_number;
            if($formData->student_father_mobile_number == null){
                $mobile = "---";
            }
           return $mobile;
        }) 
        ->addColumn('father_name', function ($formData)
        {
            $father_name = $formData->student_father_name;
            if($formData->student_father_name == null){
                $father_name = "---";
            }
           return $father_name;
        }) 
        ->addColumn('status', function ($formData)
        {
            if($formData->admission_data_status == 0){
                $status = "Pending";
            } else if($formData->admission_data_status == 1){
                $status = "Contacted";
            } else if($formData->admission_data_status == 2){
                $status = "Admitted";
            }
           return $status;
        }) 
        ->addColumn('fees_status', function ($formData)
        {
            if($formData->admission_data_fees == 0){
                $status = "Pending";
            } else if($formData->admission_data_fees == 1){
                $status = "Paid";
            }  else if($formData->admission_data_fees == 2){
                $status = "NA";
            } 
           return $status;
        }) 
        ->addColumn('mode', function ($formData)
        {
            if($formData->admission_data_mode == 0){
                $status = "Offline";
            } else if($formData->admission_data_mode == 1){
                $status = "Online";
            } 
           return $status;
        }) 
        ->addColumn('action', function ($formData)
        {
            $encrypted_admission_data_id = get_encrypted_value($formData->admission_data_id, true);
            
            return '
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="form-data/' . $encrypted_admission_data_id . '"><i class="zmdi zmdi-edit"></i></a></div>

                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Print" href="#">Print</a>
                    </li>
                   
                    </ul>
                </div>

                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-success btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-plus-circle"></i> 
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                   
                    <li>
                        <a title="Pending" href="enquiry-status/0/' . $encrypted_admission_data_id . '" "">Pending</a>
                    </li>
                    <li>
                        <a title="Contacted" href="enquiry-status/1/' . $encrypted_admission_data_id . '" "">Contacted</a>
                    </li>
                    <li>
                        <a title="Admitted" href="enquiry-status/2/' . $encrypted_admission_data_id . '" "">Admitted</a>
                    </li>
                   
                    </ul>
                </div>
              ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }


    /**
     *  Change admission form's status
     *  @Shree on 12 Sept 2018
    **/
    public function changeEnquiryStatus($status,$id)
    {
        $admission_form_id = get_decrypted_value($id, true);
        $admissionData     = AdmissionData::find($admission_form_id);
        
        if ($admissionData)
        {
            if($admissionData->form_type == 0 && $status == 2){
                $totalIntake = get_total_intake($admissionData->current_class_id);
                
                if($totalIntake>0){
                    $section_id = get_available_section($admissionData->current_class_id);
                    if($section_id != 0){
                        $storeData = store_admission_data($admissionData->admission_data_id, $admissionData->current_class_id, $section_id);
                        if($storeData == "Success"){
                            $success_msg = "Student added successfully!";
                            return redirect('admin-panel/admission-form/selected-students')->withSuccess($success_msg);
                        } else {
                            return redirect()->back()->withErrors($storeData);
                        }
                        
                    }
                } else {
                    $error_msg = "Sorry! We have already full all student in this class";
                    return redirect()->back()->withErrors($error_msg);
                }
            } else {
                $admissionData->admission_data_status  = $status;
                $admissionData->save();
                if($admissionData->form_type == 0) {
                    $success_msg = "Admission status updated successfully!";
                } else {
                    $success_msg = "Enquiry status updated successfully!";
                }
            }
            
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Enquiry not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function showFormData($id){
        $loginInfo  = get_loggedin_user_data();
        $admission_form_data_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionData::where(array('admission_data_id'=> $admission_form_data_id))->with('getFormFieldsInfo')->with('getFormInfo')->get();
        $admissionForm              = isset($admissionForm[0]) ? $admissionForm[0] : [];
        $medium_type = $admissionForm->medium_type;
        if ($admissionForm)
        {
            $arr_student_type       = \Config::get('custom.student_type');
            $arr_gender             = \Config::get('custom.student_gender');
            $arr_category           = \Config::get('custom.student_category');
            $arr_annual_salary      = \Config::get('custom.annual_salary');
            $arr_fees               = \Config::get('custom.fees_status');
            $arr_mode               = \Config::get('custom.mode');
            $arr_city_p = $arr_city_t = $arr_state_p = $arr_state_t = $admit_arr_section = $arr_section = [];
            $arr_country                   = get_all_country();
            
            $arr_title                     = get_titles();
            $arr_session                   = get_arr_session();
            $arr_caste                     = get_caste();
            $arr_religion                  = get_religion();
            $arr_natioanlity               = get_nationality();
            $arr_group                     = get_student_groups();
            $arr_document_category         = get_all_document_category(0);
            $arr_class                     = get_all_admission_classes($admissionForm['getFormInfo']->class_ids);
            $arr_stream                    = get_all_streams($medium_type);
            
            $admissionForm['arr_gender']         = $arr_gender;
            $admissionForm['arr_category']       = $arr_category;
            $admissionForm['arr_annual_salary']  = $arr_annual_salary;
            $admissionForm['arr_student_type']   = $arr_student_type;
            $admissionForm['arr_title']          = add_blank_option($arr_title, 'Select Title');
            $admissionForm['arr_country']        = add_blank_option($arr_country, 'Select Country');
            $admissionForm['arr_session']        = add_blank_option($arr_session, 'Select session');
            $admissionForm['arr_state_p']        = add_blank_option($arr_state_p, 'Select State');
            $admissionForm['arr_city_p']         = add_blank_option($arr_city_p, 'Select City');
            $admissionForm['arr_state_t']        = add_blank_option($arr_state_t, 'Select State');
            $admissionForm['arr_city_t']         = add_blank_option($arr_city_t, 'Select City');
            $admissionForm['arr_class']          = add_blank_option($arr_class, 'Select class');
            $admissionForm['arr_stream']         = add_blank_option($arr_stream, 'Select Stream');
            $admissionForm['arr_caste']          = add_blank_option($arr_caste, 'Select caste');
            $admissionForm['arr_religion']       = add_blank_option($arr_religion, 'Select religion');
            $admissionForm['arr_section']        = add_blank_option($arr_section, 'Select section');
            $admissionForm['admit_arr_section']  = add_blank_option($admit_arr_section, 'Select section');
            $admissionForm['arr_natioanlity']    = add_blank_option($arr_natioanlity, 'Select nationality');
            $admissionForm['arr_group']          = add_blank_option($arr_group, 'Select house/group');
            $admissionForm['arr_fees']           = add_blank_option($arr_fees, 'Fees Status');
            $admissionForm['arr_mode']           = add_blank_option($arr_mode, 'Mode');
            $stadmissionFormudent['arr_document_category']   = add_blank_option($arr_document_category, 'Document category');
            if($admissionForm['getFormFieldsInfo']['form_keys'] != ""){
                $form_keys = explode(',', $admissionForm['getFormFieldsInfo']['form_keys']);
                $admissionForm['form_keys'] = $form_keys;
            }
            $admissionForm['form_pay_mode']  = $admissionForm['getFormFieldsInfo']['form_pay_mode'];
            if($admissionForm->form_type == 0){
                $page_title = trans('language.admission_form');
            } else {
                $page_title = trans('language.enquiry_form');
            }
            $admissionForm['class_id']  = $admissionForm['getFormInfo']['class_id'];
            $admissionForm['session_id']  = $admissionForm['getFormInfo']['session_id'];
            $admissionForm['medium_type']  = $admissionForm['getFormInfo']['medium_type'];
            
            $save_url             = url('admin-panel/admission-form/save-data-form/'.$id);
            $submit_button        = 'Update';
            $data = array(
                'page_title'    => $page_title,
                'redirect_url'  => url('admin-panel/admission/view-forms'),
                'login_info'    => $loginInfo,
                'admissionForm' => $admissionForm,
                'save_url'      => $save_url,
                'submit_button' => $submit_button
            );
            // p($admissionForm);
            return view('admin-panel.admission-form.enquiry-form')->with($data);
        }  else {
            $error_message = "Form not found!";
            return redirect('admin-panel/admission/view-forms')->withErrors($error_message);
        }
    }


    /**
     *  Form listing page for selected students
     *  @Shree on 24 Sept 2018
    **/
    public function showSelectedStudents()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $admissionForm = [];
        $arr_medium   = get_all_mediums();
        $admissionForm['arr_session'] = $arr_sessions;
        $admissionForm['arr_medium'] = $arr_medium;
        $data = array(
            'page_title'    => trans('language.selected_students'),
            'redirect_url'  => url('admin-panel/admission-form/selected-students'),
            'login_info'    => $loginInfo,
            'admissionForm' => $admissionForm
        );
        return view('admin-panel.admission-form.selected-students')->with($data);
    }
    
    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 12 Sept 2018
    **/
    public function getSelectedStudents(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $admissionForm = AdmissionForm::where(function($query) use ($request) 
        {
            $query->where('form_type', "=", 0);
            if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
            {
                $query->where('session_id', "=", $request->get('session_id'));
            }
            if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('form_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('admission_form_id', 'DESC')->with('getSessionInfo')->with('getClassInfo')->get();
        
        return Datatables::of($admissionForm)
       
        ->addColumn('session_name', function ($admissionForm)
        {
           return $admissionForm->getSessionInfo['session_name'];
        }) 
        ->addColumn('class_name', function ($admissionForm)
        {
            $class_names = [];
            $class_ids = explode(",",$admissionForm->class_ids);
            if(!empty($class_ids)) {
            foreach($class_ids as $class_id){
                $class_names[] = get_class_name($class_id);
            }}
           return implode(",", $class_names);
        }) 
        ->addColumn('medium_type', function ($admissionForm)
        {
           $arr_medium      = get_all_mediums();
           return $arr_medium[$admissionForm->medium_type];
        }) 
        ->addColumn('total_intake', function ($admissionForm)
        {
           return $admissionForm->no_of_intake;
        }) 
        ->addColumn('total_intake_remaining', function ($admissionForm)
        {
            $total = AdmissionData::where('admission_form_id', $admissionForm->admission_form_id)->get()->count();
            
           return $admissionForm->no_of_intake - $total;
        }) 
        ->addColumn('action', function ($admissionForm)
        {
            $encrypted_admission_form_id = get_encrypted_value($admissionForm->admission_form_id, true);
            
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="View Students" href="view-students/' . $encrypted_admission_form_id . '" "">View Students</a>
                    </li>
                    </ul>
                </div>
               ';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Form listing page for selected students
     *  @Shree on 24 Sept 2018
    **/
    public function showStudents($id)
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $admissionForm = [];
        $decrypted_admission_form_id = get_decrypted_value($id, true);
        $admissionForm   = AdmissionForm::Find($decrypted_admission_form_id);
        
        $data = array(
            'page_title'    => trans('language.students'),
            'redirect_url'  => url('admin-panel/admission-form/selected-students'),
            'login_info'    => $loginInfo,
            'admissionForm' => $admissionForm
        );
        return view('admin-panel.admission-form.show-students')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 24 Sept 2018
    **/
    public function getStudents(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $admissionForm = AdmissionData::where(function($query) use ($request) 
        {
            $query->where('form_type', "=", 0);
           
            if (!empty($request) && !empty($request->get('form_number')))
            {
                $query->where('form_number', "like", "%{$request->get('form_number')}%");
            }
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
        })->orderBy('admission_data_id', 'DESC')->get();
        // p($admissionForm);
        return Datatables::of($admissionForm)
        ->addColumn('father_name', function ($admissionForm)
        {
            $student_father_name = "----";
            if($admissionForm->student_father_name != ""){
                $student_father_name = $admissionForm->student_father_name;
            }
           return $student_father_name;
        }) 
        ->addColumn('mobile', function ($admissionForm)
        {
            $student_father_mobile_number = "----";
            if($admissionForm->student_father_mobile_number != ""){
                $student_father_mobile_number = $admissionForm->student_father_mobile_number;
            }
           return $student_father_mobile_number;
        }) 
        ->addColumn('profile', function ($admissionForm)
        {
            $encrypted_admission_form_id = get_encrypted_value($admissionForm->admission_form_id, true);
            $encrypted_student_id = get_encrypted_value($admissionForm->student_id, true);
            return '
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a href="'.url('admin-panel/student/student-profile/' . $encrypted_student_id .' ').'">View Profile</a>
                    </li>
                </ul>
            </div>
               ';
        })->rawColumns(['profile' => 'profile'])->addIndexColumn()->make(true);
    }

    public function check_enquiry_existance(Request $request){
        if($request->has('enquiry_no') ) {
            $enquiryInfo = AdmissionData::where(function($query) use ($request) 
            {
                $query->where('form_type', "=", 1);
                $query->where('form_number', "=", $request->get('enquiry_no'));
                
            })->orderBy('admission_data_id', 'DESC')->get()->toArray();
            $enquiryInfo              = isset($enquiryInfo[0]) ? $enquiryInfo[0] : [];
            if(isset($enquiryInfo['admission_data_id'])) {
                $enquiryInfo['exist_status']  = 1;
            } else {
                $enquiryInfo['exist_status']  = 0;
            }
        } else {
            $enquiryInfo['exist_status']  = 0;
        }
        
        return $enquiryInfo;
    }

    public function send_push_notification($module_id,$module_name,$class_name,$notification_type){
        $device_ids = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereRaw('FIND_IN_SET(1,staff_role_id)')->orWhereRaw('FIND_IN_SET(10,staff_role_id)')->select('staff_name', 'staff_id','reference_admin_id')->groupBy('staff_id')->with('getStaffAdminInfo')->get();
        foreach ($arr_staff_list as $staff) {
            if($staff['getStaffAdminInfo']->fcm_device_id != "" && $staff['getStaffAdminInfo']->notification_status == 1){
                $device_ids[] = $staff['getStaffAdminInfo']->fcm_device_id;
                $staff_admin_id = $staff['getStaffAdminInfo']->admin_id;
            }
        }
        if($notification_type == 'admission_new_applicant'){
            $message = "New Application for admission in ".$class_name." received";
        } else if($notification_type == 'admission_new_enquiry'){
            $message = "New Enquiry for admission in ".$class_name." received";
        }
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => Null,
            'parent_admin_id' => Null,
            'staff_admin_id' => $staff_admin_id,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
    }

}
