<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Examination\GradeScheme; // Model
use App\Model\Examination\Grades; // Model
use Yajra\Datatables\Datatables;

class GradeSchemeController extends Controller
{
    /**
     *  View page for Grade scheme
     *  @shree on 6 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_grade_scheme'),
            'redirect_url'  => url('admin-panel/grade-scheme/view-grade-scheme'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.grade-scheme.index')->with($data);
    }

    /**
     *  Add page for Grade scheme
     *  @Shree on 6 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $scheme      	= [];
        $loginInfo 		= get_loggedin_user_data();
       
        if (!empty($id))
        {
            $decrypted_grade_scheme_id 	= get_decrypted_value($id, true);
            $scheme      		= GradeScheme::where('grade_scheme_id', $decrypted_grade_scheme_id)->with('getGrades')->get();
            $scheme = isset($scheme[0]) ? $scheme[0] : [];
            if (!$scheme)
            {
                return redirect('admin-panel/grade-scheme/add-grade-scheme')->withError('Grade scheme not found!');
            }
            $gradeData = [];
            
            foreach ($scheme['getGrades'] as $grade)
            {
                $grade_list['grade_id']         = $grade['grade_id'];
                $grade_list['grade_scheme_id']  = $grade['grade_scheme_id'];
                $grade_list['grade_min']        = $grade['grade_min'];
                $grade_list['grade_max']        = $grade['grade_max']; 
                $grade_list['grade_name']       = $grade['grade_name']; 
                $grade_list['grade_point']      = $grade['grade_point']; 
                $grade_list['grade_remark']     = $grade['grade_remark']; 
                $gradeData[] = $grade_list;
            }
            $scheme->gradeData = $gradeData;
            $page_title             	= trans('language.edit_grade_scheme');
            $encrypted_grade_scheme_id  = get_encrypted_value($scheme->grade_scheme_id, true);
            $save_url               	= url('admin-panel/grade-scheme/save/' . $encrypted_grade_scheme_id);
            $submit_button          	= 'Update';
           
        }
        else
        {
            $page_title    = trans('language.add_grade_scheme');
            $save_url      = url('admin-panel/grade-scheme/save');
            $submit_button = 'Save';
        }
        $scheme['arr_grade_type']         = \Config::get('custom.grade_type');
        //p($scheme);
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'login_info'    	=> $loginInfo,
            'scheme'            => $scheme,
            'redirect_url'  	=> url('admin-panel/grade-scheme/view-grade-scheme'),
        );
        return view('admin-panel.grade-scheme.add')->with($data);
    }

    /**
     *  Add and update grade scheme's data
     *  @Shree on 6 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo              = get_loggedin_user_data();
        $decrypted_grade_scheme_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $scheme = GradeScheme::find($decrypted_grade_scheme_id);
            $admin_id = $scheme['admin_id'];
            if (!$scheme)
            {
                return redirect('/admin-panel/grade-scheme/add-grade-scheme/')->withError('Grade scheme not found!');
            }
            $success_msg = 'Grade scheme updated successfully!';
        }
        else
        {
            $scheme     = New GradeScheme;
            $success_msg = 'Grade scheme saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'scheme_name'   => 'required|unique:grade_schemes,scheme_name,' . $decrypted_grade_scheme_id . ',grade_scheme_id',
            'grade_type'   => 'required'
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $scheme->admin_id       = $admin_id;
                $scheme->update_by      = $loginInfo['admin_id'];
                $scheme->scheme_name 	= Input::get('scheme_name');
                $scheme->grade_type 	= Input::get('grade_type');
                $scheme->save();
              
                foreach($request->get('gradeData') as $gradeData){ 
                    if(isset($gradeData['grade_name']) && !empty($gradeData['grade_name'])) {
                        if(isset($gradeData['grade_id']) ) {
                           
                            $grade_update   = Grades::where('grade_id', $gradeData['grade_id'])->first();
                            $validatior     = Validator::make($gradeData, [
                                'grade_name'   => 'required'
                            ]);
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $grade_update->admin_id         = $admin_id;
                            $grade_update->update_by        = $loginInfo['admin_id'];
                            $grade_update->grade_scheme_id  = $grade_update->grade_scheme_id;
                            $grade_update->grade_min        = $gradeData['grade_min'];
                            $grade_update->grade_max        = $gradeData['grade_max'];
                            $grade_update->grade_name       = $gradeData['grade_name'];
                            $grade_update->grade_point      = $gradeData['grade_point'];
                            $grade_update->grade_remark     = $gradeData['grade_remark'];
                            $grade_update->save();
                        } else {
                            
                            $grade   = new Grades();
                            $validatior = Validator::make($gradeData, [
                                'grade_name'   => 'required'
                            ]);
                            if ($validatior->fails())
                            {
                                return redirect()->back()->withInput()->withErrors($validatior);
                            }
                            $grade->admin_id        = $admin_id;
                            $grade->update_by       = $loginInfo['admin_id'];
                            $grade->grade_scheme_id = $scheme->grade_scheme_id;
                            $grade->grade_min       = $gradeData['grade_min'];
                            $grade->grade_max       = $gradeData['grade_max'];
                            $grade->grade_name      = $gradeData['grade_name'];
                            $grade->grade_point     = $gradeData['grade_point'];
                            $grade->grade_remark    = $gradeData['grade_remark'];
                            $grade->save();
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/grade-scheme/view-grade-scheme')->withSuccess($success_msg);
    }

    /**
     *  Get Grade scheme's Data for view page(Datatables)
     *  @shree on 6 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $scheme  	= GradeScheme::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('scheme_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('grade_scheme_id', 'DESC')->get();

        return Datatables::of($scheme)
        
        ->addColumn('grades', function ($scheme)
        {
            $grades = 0;
            $grades = Grades::where(function($query) use ($scheme) 
            {
                $query->where('grade_scheme_id', "=", $scheme->grade_scheme_id);
            })->get()->count();  
            if($grades == 0) {
                return "No grade available.";
            } else {      
                return '<button type="button" class="btn btn-raised btn-primary grades"  grade-id="'.$scheme->grade_scheme_id.'" >
            '.$grades.' Grades
            </button>';
            }
        })
        ->addColumn('grade_type', function ($scheme)
        {
            $grade_types         = \Config::get('custom.grade_type');
            $grade_type = $grade_types[$scheme->grade_type];
            return $grade_type;
        })
        
        ->addColumn('action', function ($scheme)
        {

            $encrypted_grade_scheme_id = get_encrypted_value($scheme->grade_scheme_id, true);
            if($scheme->scheme_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="grade-scheme-status/'.$status.'/' . $encrypted_grade_scheme_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-grade-scheme/' . $encrypted_grade_scheme_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-grade-scheme/' . $encrypted_grade_scheme_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'grades'=> 'grades'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy grade scheme's data
     *  @shree on 6 Oct 2018.
    **/
    public function destroy($id)
    {
        $grade_scheme_id 	= get_decrypted_value($id, true);
        $scheme 		= GradeScheme::find($grade_scheme_id);
        
        $success_msg = $error_message =  "";
        if ($scheme)
        {
            DB::beginTransaction();
            try
            {
                $scheme->delete();
                $success_msg = "Grade scheme deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/grade-scheme/view-grade-scheme')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/grade-scheme/view-grade-scheme')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Grade scheme not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change grade scheme's status
     *  @Shree on 6 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $grade_scheme_id       = get_decrypted_value($id, true);
        $scheme          = GradeScheme::find($grade_scheme_id);
        if ($scheme)
        {
            $scheme->scheme_status  = $status;
            $scheme->save();
            $success_msg = "Grade Scheme status update successfully!";
            return redirect('admin-panel/grade-scheme/view-grade-scheme')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Grade scheme not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Grade's data
     *  @Shree on 6 Oct 2018.
    **/
    public function destroyGrade($id)
    {
        $grade_id    = $id;
        $grade       = Grades::find($grade_id);
        
        $success_msg = $error_message =  "";
        if ($grade)
        {
            DB::beginTransaction();
            try
            {
                $grade->delete();
                $success_msg = "Grade deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        }else{
            $error_message = "Grade not found!";
            return $error_message;
        }
    }

    /**
     *  Get grades's Data for view page(Datatables)
     *  @shree on 6 Oct 2018.
    **/
    public function grades(Request $request)
    {
        $grades  	= Grades::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('grade_scheme_id', "=", $request->get('id'));
            }
        })->orderBy('grade_id', 'DESC')->get();

        return Datatables::of($grades)
        ->addColumn('grade_min_max', function ($grades)
        {
            $grade_min_max  = $grades->grade_min.' - '.$grades->grade_max;
            return $grade_min_max;
        })
        ->addColumn('action', function ($grades)
        {
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini grade-records" data-toggle="tooltip" title="Delete" grade-record="'.$grades->grade_id.'" scheme-record="'.$grades->grade_scheme_id.'" style="margin-top: -1px !important; padding: 0px !important;"><i class="zmdi zmdi-delete"></i></div>';
        })->rawColumns(['action' => 'action', 'grades'=> 'grades'])->addIndexColumn()
        ->make(true);
    }
}
