<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\RecurringHead; // Model
use App\Model\FeesCollection\RecurringInst; // Model
use Yajra\Datatables\Datatables;

class RecurringHeadController extends Controller
{
    /**
     *  View page for Recurring head
     *  @shree on 1 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $recurring = [];
        $arr_class              = get_all_classes_mediums();
        $recurring['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'    => trans('language.view_recurring_h'),
            'redirect_url'  => url('admin-panel/recurring-head/view-recurring-heads'),
            'login_info'    => $loginInfo,
            'recurring'     => $recurring
        );
        return view('admin-panel.recurring-head.index')->with($data);
    }

    /**
     *  Add page for one time
     *  @Shree on 1 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $recurring      	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_rc_head_id 	= get_decrypted_value($id, true);
            $recurring      		= RecurringHead::where('rc_head_id', $decrypted_rc_head_id)->with('getInstallments')->get();
            $recurring = isset($recurring[0]) ? $recurring[0] : [];
            if (!$recurring)
            {
                return redirect('admin-panel/recurring-head/add-recurring-head')->withError('Recurring head not found!');
            }
            if($recurring->rc_classes != ""){
                $recurring->rc_classes = explode(",",$recurring->rc_classes);
            }
            $inst_list = [];
            foreach ($recurring['getInstallments'] as $inst)
            {
                $inst_data['rc_inst_id']                = $inst['rc_inst_id'];
                $inst_data['rc_head_id']                = $inst['rc_head_id'];
                $inst_data['rc_inst_particular_name']   = $inst['rc_inst_particular_name'];
                $inst_data['rc_inst_amount']            = $inst['rc_inst_amount'];
                $inst_data['rc_inst_date']              = $inst['rc_inst_date']; 
                $inst_list[] = $inst_data;
            }
            $recurring->inst_data = $inst_list;
            $page_title             	= trans('language.edit_recurring_h');
            $encrypted_rc_head_id       = get_encrypted_value($recurring->rc_head_id, true);
            $save_url               	= url('admin-panel/recurring-head/save/' . $encrypted_rc_head_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_recurring_h');
            $save_url      = url('admin-panel/recurring-head/save');
            $submit_button = 'Save';
        }
        $arr_class              = get_all_classes_mediums();
        $recurring['arr_class']   = $arr_class;
       
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'recurring' 		=> $recurring,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/recurring-head/view-recurring-heads'),
        );
        return view('admin-panel.recurring-head.add')->with($data);
    }

    /**
     *  Add and update one time's data
     *  @Shree on 1 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo              = get_loggedin_user_data();
        $decrypted_rc_head_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $recurring = RecurringHead::find($decrypted_rc_head_id);
            $admin_id = $recurring['admin_id'];
            if (!$recurring)
            {
                return redirect('/admin-panel/recurring-head/add-recurring-head/')->withError('Recurring head not found!');
            }
            $success_msg = 'Recurring head updated successfully!';
        }
        else
        {
            $recurring     = New RecurringHead;
            $success_msg = 'Recurring head saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'rc_particular_name'   => 'required|unique:recurring_heads,rc_particular_name,' . $decrypted_rc_head_id . ',rc_head_id',
            'rc_classes' => 'required',
            'rc_amount' => 'required',
        ], [
            'rc_classes.required' => 'Please select classes.'
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $classes = null;
                if($request->get('rc_classes') != ""){
                    $classes = implode(",",$request->get('rc_classes'));
                }
                $recurring->admin_id            = $admin_id;
                $recurring->update_by           = $loginInfo['admin_id'];
                $recurring->rc_particular_name 	= Input::get('rc_particular_name');
                $recurring->rc_classes 	        = $classes;
                $recurring->rc_amount 	        = Input::get('rc_amount');
                $recurring->save();
                foreach($request->get('inst') as $inst){ 
                    if(isset($inst['rc_inst_particular_name']) && !empty($inst['rc_inst_particular_name'])) {
                        if(isset($inst['rc_inst_id']) ) {
                            $inst_update                            = RecurringInst::find($inst['rc_inst_id']);
                            $inst_update->admin_id                  = $admin_id;
                            $inst_update->update_by                 = $loginInfo['admin_id'];
                            $inst_update->rc_head_id                = $inst_update->rc_head_id;
                            $inst_update->rc_inst_particular_name   = $inst['rc_inst_particular_name'];
                            $inst_update->rc_inst_amount            = $inst['rc_inst_amount'];
                            $inst_update->rc_inst_date              = $inst['rc_inst_date'];
                            $inst_update->save();
                        } else if(!isset($inst['rc_inst_id']) ){
                            $recurring_inst   = new RecurringInst();
                            $recurring_inst->admin_id                   = $admin_id;
                            $recurring_inst->update_by                  = $loginInfo['admin_id'];
                            $recurring_inst->rc_head_id                 = $recurring->rc_head_id;
                            $recurring_inst->rc_inst_particular_name    = $inst['rc_inst_particular_name'];
                            $recurring_inst->rc_inst_amount             = $inst['rc_inst_amount'];
                            $recurring_inst->rc_inst_date               = $inst['rc_inst_date'];
                            $recurring_inst->save();
                        }
                    }
                }
            }
            
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/recurring-head/view-recurring-heads')->withSuccess($success_msg);
    }

    /**
     *  Get one time's Data for view page(Datatables)
     *  @shree on 1 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $recurring  	= RecurringHead::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('rc_particular_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->whereRaw('FIND_IN_SET('.$request->get('class_id').',rc_classes)');
            }
        })->orderBy('rc_head_id', 'DESC')->get();

        return Datatables::of($recurring)
        ->addColumn('classes', function ($recurring)
        {
            return '<button type="button" class="btn btn-raised btn-primary classes"  head-id="'.$recurring->rc_head_id.'" >
            View Classes
            </button>';
        })
        ->addColumn('no_of_inst', function ($recurring)
        {
            $no_of_inst = 0;
            $no_of_inst = RecurringInst::where(function($query) use ($recurring) 
            {
                $query->where('rc_head_id', "=", $recurring->rc_head_id);
               
            })->get()->count();  
            if($no_of_inst == 0) {
                return "No installment available.";
            } else {      
                return '<button type="button" class="btn btn-raised btn-primary installments"  installment-id="'.$recurring->rc_head_id.'" >
            '.$no_of_inst.' Installments
            </button>';
            }
        })
        ->addColumn('action', function ($recurring)
        {

            $encrypted_rc_head_id = get_encrypted_value($recurring->rc_head_id, true);
            if($recurring->rc_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="recurring-head-status/'.$status.'/' . $encrypted_rc_head_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-recurring-head/' . $encrypted_rc_head_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-recurring-head/' . $encrypted_rc_head_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'no_of_inst'=> 'no_of_inst', 'classes'=>'classes'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy recurring head's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $rc_head_id 	= get_decrypted_value($id, true);
        $recurring 		= RecurringHead::find($rc_head_id);
        
        $success_msg = $error_message =  "";
        if ($recurring)
        {
            DB::beginTransaction();
            try
            {
                $recurring->delete();
                $success_msg = "Recurring head deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/recurring-head/view-recurring-heads')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/recurring-head/view-recurring-heads')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Recurring head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change recurring's status
     *  @Shree on 1 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $rc_head_id       = get_decrypted_value($id, true);
        $recurring          = RecurringHead::find($rc_head_id);
        if ($recurring)
        {
            $recurring->rc_status  = $status;
            $recurring->save();
            $success_msg = "Recurring head status update successfully!";
            return redirect('admin-panel/recurring-head/view-recurring-heads')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Recurring head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Installment's data
     *  @Shree on 1 Oct 2018.
    **/
    public function destroyInst($id)
    {
        $rc_inst_id    = $id;
        $inst       = RecurringInst::find($rc_inst_id);
        
        $success_msg = $error_message =  "";
        if ($inst)
        {
            DB::beginTransaction();
            try
            {
                $inst->delete();
                $success_msg = "Recurring Install deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        }else{
            $error_message = "Installment not found!";
            return $error_message;
        }
    }

    /**
     *  Get installment's Data for view page(Datatables)
     *  @shree on 1 Oct 2018.
    **/
    public function installment(Request $request)
    {
        $recurring  	= RecurringInst::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('rc_head_id', "=", $request->get('id'));
            }
        })->orderBy('rc_inst_id', 'DESC')->get();

        return Datatables::of($recurring)
        
        ->addColumn('action', function ($recurring)
        {
            return '
                    <div class="btn btn-icon btn-neutral btn-icon-mini inst-records" data-toggle="tooltip" title="Delete" inst-record="'.$recurring->rc_inst_id.'" style="margin-top: -1px !important; padding: 0px !important;"><i class="zmdi zmdi-delete"></i></div>';
        })->rawColumns(['action' => 'action', 'no_of_inst'=> 'no_of_inst'])->addIndexColumn()
        ->make(true);
    }

     /**
     *  Get Heads data according type
     *  @Shree on 8 Oct 2018
    **/
    public function getHeadeData()
    {
        $type = Input::get('type');
        $heads = get_all_heads($type);
        $data = view('admin-panel.recurring-head.ajax-head-select',compact('heads'))->render();
        return response()->json(['options'=>$data]);
    }

     /**
     *  Get classes Data for view page(Datatables)
     *  @shree on 12 Oct 2018.
    **/
    public function classes(Request $request)
    {
        $recurring  	= RecurringHead::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('rc_head_id', "=", $request->get('id'));
            }
        })->orderBy('rc_head_id', 'DESC')->get()->toArray();
        
        $class_arr = explode(",",$recurring[0]['rc_classes']);
        $allNames = "";
        $name = [];
        foreach($class_arr as $key => $id){
            $encrypted_id = get_encrypted_value($id,true);
            $name[]['class_name'] = get_class_name_by_id($encrypted_id);
        }
        return Datatables::of($name)
        ->addColumn('class_name', function ($name)
        {
            return $name['class_name'];
        })
        ->addIndexColumn()
        ->make(true);
    }

}
