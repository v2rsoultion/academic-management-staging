<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\InventoryCategory\Category; // Model
use App\Model\InventoryItem\Items; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class InventoryCategoryController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('17',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /** 
	 *  Add Page of Category
     *  @Khushbu on 14 Dec 2018
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$category = $getCategoryData = $getCategoryData11 = [];
	 	$loginInfo           = get_loggedin_user_data();

	 	if(!empty($id))
	 	{
	 		$decrypted_category_id 	= get_decrypted_value($id, true);
        	$category      			= Category::Find($decrypted_category_id);
            $page_title             = trans('language.edit_category');
        	$save_url    			= url('admin-panel/inventory/manage-category-save/'. $id);
        	$submit_button  		= 'Update';
	 	}
	 	else 
	 	{
            $page_title             = trans('language.add_category');
	 		$save_url    	        = url('admin-panel/inventory/manage-category-save');
	 		$submit_button          = 'Save';
	 	}
        $getCategoryData        = get_all_category(0);
        $get_all_categories     = get_all_categories(0);
	 	$getCategoryData11['']  = add_blank_option($get_all_categories, 'Select Category');
        $data                   =  array(
            'page_title'        => $page_title,
            'login_info'        => $loginInfo,
            'save_url'          => $save_url,  
            'category'		    => $category,	
            'submit_button'	    => $submit_button,
            'getCategoryData'   => $getCategoryData,
            'getCategoryData11' => $getCategoryData11
        );
        return view('admin-panel.inventory-category.add')->with($data);
    }   

    /**
     *	Add & Update of Category
     *  @Khushbu on 14 Dec 2018
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_category_id		= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id))
        {
            $category        = Category::Find($decrypted_category_id);
            if(!$category) {
                return redirect('admin-panel/inventory/manage-category')->withError('Category not found!');
            }

            $admin_id    = $category->admin_id;
            $success_msg = 'Category updated successfully!';
        }
        else
        {
            $category    = New Category;
            $success_msg = 'Category saved successfully!';
        }

            $validator             = Validator::make($request->all(), [
                'category_name'    => 'required|unique:inv_category,category_name,' . $decrypted_category_id . ',category_id'
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $category->admin_id         = $admin_id;
                $category->update_by        = $loginInfo['admin_id'];
                $category->category_level   = Input::get('category');
                $category->category_name    = Input::get('category_name');
                $category->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/inventory/manage-category')->withSuccess($success_msg);
    }
     	
    /**
     *	Get Category's Data fo view page
     *  @Khushbu on 14 Dec 2018
    **/
    public function anyData(Request $request)
    {

    	$loginInfo 			= get_loggedin_user_data();
        $getCategoryData    = get_all_category(0);
    	$category 			= Category::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('search_category') !=  NULL)
            {
                $query->where('category_level', '=', $request->get('search_category'));
            }
           if (!empty($request) && $request->get('s_category_name') !=  NULL)
            {
                $query->where('category_name', 'Like', $request->get('s_category_name').'%');
            }
        })->orderBy('category_id','DESC')->get();
        $all_categories = get_all_categories(0);

    	return Datatables::of($category, $all_categories)
        ->addColumn('category_level', function($category) use($all_categories) {
            return $all_categories[$category->category_level];
        })
    	->addColumn('action', function($category) use($request) {
    		$encrypted_category_id  = get_encrypted_value($category->category_id, true);
            
    		if($category->category_status == 0) {

                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/inventory/manage-category-status/'.$status .'/'.$encrypted_category_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/inventory/manage-category/'.$encrypted_category_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/inventory/delete-manage-category/' . $encrypted_category_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';

    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/inventory/manage-category');

    } 

    /** 
     *	Change Status of Category
     *  @Khushbu on 14 Dec 2018
	**/
	public function changeStatus($status,$id) 
	{
		$category_id 		= get_decrypted_value($id, true);
        $category 		  	= Category::find($category_id);
        if($category->category_level == 0){
            $find_category_in_items = Items::whereRaw('FIND_IN_SET('.$category->category_id.',category_id)')->get()->count();
        } else {
            $find_category_in_items = Items::whereRaw('FIND_IN_SET('.$category->category_level.',category_id)')->get()->count();    
        }
        if($find_category_in_items == 0) {
            if ($category)
            {
                $category->category_status  = $status;
                $category->save();
                $success_msg = "Category status update successfully!";
                return redirect('admin-panel/inventory/manage-category')->withSuccess($success_msg);
            }
            else
            {
                $error_message = "Category not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Sorry we can't update status it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Category
     *  @Khushbu on 14 Dec 2018
	**/
	public function destroy($id) {
		$category_id 		= get_decrypted_value($id, true);
        $category 		  	= Category::find($category_id);
        $find_category_in_items = Items::whereRaw('FIND_IN_SET('.$category->category_level.',category_id)')->get()->count();
        if($find_category_in_items == 0) {
            if ($category)
            {
                DB::beginTransaction();
                try
                {
                    $getData     = Category::where('category_level',$category->category_id)->get()->count();
                    if($getData == 0) 
                    {
                        $category->delete();
                        $success_msg = "Category deleted successfully!";
                    }
                    else 
                    {
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                        return redirect()->back()->withErrors($error_message);
                    }
                    
                }  
                catch(\Exception $e)
                {
                    DB::rollback();
                    $error_message = "Sorry we can't delete it because it's already in used!!";
                    return redirect()->back()->withErrors($error_message);
                }  
                DB::commit();
                return redirect()->back()->withSuccess($success_msg);
            }
            else
            {
                $error_message = "Category not found!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Sorry we can't delete it because it's already in used!!";
            return redirect()->back()->withErrors($error_message);
        }
	}

    /**
     *  Get sub-category data according to category
     *  @Khushbu on 15 Dec 2018
    **/
    public function getSubCategoryData($category_id=null)
    {
        $category_id    = Input::get('category_id');
        $subcategory    = get_sub_categories($category_id);
        $data           = view('admin-panel.inventory-category.ajax-sub-category-select',compact('subcategory'))->render();
        return response()->json(['options'=>$data]);
    }
}
