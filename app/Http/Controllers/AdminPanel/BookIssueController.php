<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\LibraryMember\LibraryMember; // Model
use App\Model\Student\Student; // Model
use App\Model\Staff\Staff; // Model 
use App\Model\Book\Book; // Model 
use App\Model\Book\BookCopiesInfo; // Model 
use App\Model\IssueBook\IssueBook; // Model 
use App\Model\BookAllowance\BookAllowance; // Model
use App\Model\IssueBook\IssueBookHistory; // Model 
use Yajra\Datatables\Datatables;
use URL;
use DateTime;
use Redirect;

class BookIssueController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Issue Book
     *  @Pratyush on 21 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $listData  = array(
        	'1' => 'Student',
        	'2'	=> 'Staff'
        );
        $data = array(
            'page_title'    => trans('language.book_issued'),
            'redirect_url'  => url('admin-panel/issue-book'),
            'login_info'    => $loginInfo,
            'listData'		=> $listData
        );
        return view('admin-panel.issue-book.view_member')->with($data);
    }

    /**
     *  Get Member's Data for Issue Book page(Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function anyData(Request $request)
    {
    	// dd($request->input());
    	$loginInfo 	= get_loggedin_user_data();
    	$member 	= [];
    	// For member type student
    	if($request->has('member_type') && $request->get('member_type') == 1){

    			$arr_student 	  = LibraryMember::where(function($query) use ($loginInfo,$request) 
            {
            	$query->where('library_members.library_member_type',1);
            })->join('students', function($join) use ($request){
                $join->on('students.student_id', '=', 'library_members.member_id');
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')))
                {
                    $join->where('students.student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('member_name')))
                {
                    $join->where('students.student_name', "like", "%{$request->get('member_name')}%");
                }
            })->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            })->get();

            if(!empty($arr_student[0]->student_id)){
            	foreach($arr_student as $student) {
                    $image = "";
                    $profileImg = "";
	                if (!empty($student->student_image))
	                {
	                    $profile = check_file_exist($student->student_image, 'student_image');
	                    if (!empty($profile))
	                    {
                            $image = URL::to($profile);
                            $profileImg = "<img src=".$image." height='30' />";
	                    }
	                } else {
	                    $image = "";
                    }
                    $name = $profileImg.' '.$student->student_name;
	                $tot_book_allow = BookAllowance::first();
	                // p($tot_book_allow->allow_for_student);
	                $issued_books 	= IssueBook::where('member_id',$student->library_member_id)->count();
	                $books_left 	= $tot_book_allow->allow_for_student - $issued_books; 

            		$member[] = array(
            		'id'				=> $student->library_member_id,
            		'name' 				=> $name,
            		'father_name' 		=> $student->student_father_name,
            		'profile' 			=> $image,
            		'no_of_book_left' 	=> $books_left,
            		);	
            	}
            	
            }
            
    	}elseif($request->has('member_type') && $request->get('member_type') == 2){
    		$arr_staff 	  = LibraryMember::where(function($query) use ($loginInfo,$request) 
            {
            	$query->where('library_members.library_member_type',2);
            })->join('staff', function($join) use ($request){
                $join->on('staff.staff_id', '=', 'library_members.member_id');
                // For staff name
                if (!empty($request) && !empty($request->has('member_name')))
                {
                    $join->where('staff.staff_name', "like", "%{$request->get('member_name')}%");
                }
            })->join('designations', function($join) use ($request){
                $join->on('designations.designation_id', '=', 'staff.designation_id');
            })->get();

            if(!empty($arr_staff[0]->staff_id)){
            	foreach($arr_staff as $staff) {
                    $image = "";
                    $profileImg = '';
	                if (!empty($staff->staff_profile_img))
	                {
	                    $profile = check_file_exist($staff->staff_profile_img, 'staff_profile');
	                    if (!empty($profile))
	                    {
                            $image = URL::to($profile);
                            $profileImg = "<img src=".$image." height='30' />";
	                    }
	                } else {
	                    $image = "";
	                }
                    $name = $profileImg.' '.$staff->staff_name;
	                $tot_book_allow = BookAllowance::first();
	                // p($tot_book_allow->allow_for_student);
	                $issued_books 	= IssueBook::where('member_id',$staff->library_member_id)->count();
	                $books_left 	= $tot_book_allow->allow_for_staff - $issued_books; 

            		$member[] = array(
            		'id'				=> $staff->library_member_id,
            		'name' 				=> $name,
            		'father_name' 		=> $staff->staff_father_name_husband_name,
            		'profile' 			=> $image,
            		'no_of_book_left' 	=> $books_left,
            		);	
            	}
            	
            }
        }
        return Datatables::of($member)
        ->addColumn('name', function ($member)
        {
            return $member['name'];
        })
        ->addColumn('action', function ($member)
        {
            $encrypted_member_id = get_encrypted_value($member['id'], true);
            if($member['no_of_book_left'] == 0){
                return '-----';
            } else {
                return '
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li><a href="issue-book/books/' . $encrypted_member_id . '"  ">Issue Book</a></li>
                    </ul>
                </div>';
            }
        })->rawColumns(['action' => 'action','name' => 'name'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Books (issue Book)
     *  @Pratyush on 21 Aug 2018
    **/
    public function displayBooks($id = false)
    {
        $memberInfo = get_library_member_info($id);
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.book_issued'),
            'redirect_url'  => url('admin-panel/title/view-titles'),
            'login_info'    => $loginInfo,
            'member_id'		=> $id,
            'memberInfo'	=> $memberInfo,
            'save_url'     	=> url('admin-panel/issue-book/issue-member-book')
        );
        return view('admin-panel.issue-book.view_books')->with($data);
    }

    /**
     *  Get Book's Data for issue book - Book Page(Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function displayBooksData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $book      = Book::where(function($query) use ($loginInfo,$request){
                    if (!empty($request) && $request->has('book_name'));
                    {
                        $query->where('book_name', "like", "%{$request->get('book_name')}%");
                    }
                    if (!empty($request) && $request->has('publisher'));
                    {
                        $query->where('publisher_name', "like", "%{$request->get('publisher')}%");
                    }
                    if (!empty($request) && $request->has('subtitle'));
                    {
                        $query->where('book_subtitle', "like", "%{$request->get('subtitle')}%");
                    }
            })->with('getCategory')->orderBy('book_id', 'ASC')->get();
        return Datatables::of($book)
                ->addColumn('available_copies', function ($book) use ($loginInfo,$request)
                {
                	if($request->get('member_id') != ''){
                		$decrypted_member_id 	= get_decrypted_value($request->get('member_id'), true); // library member primary key

                		$library_member =  LibraryMember::where('library_member_id',$decrypted_member_id)->first();
                	}
                	$bookavailable = BookCopiesInfo::where(function($query) use ($book,$loginInfo,$request,$library_member){
                		$query->where('book_id',$book->book_id);
	               		$query->where('book_copy_status',0);
    					if($library_member->library_member_type == 1){
    						$query->where('exclusive_for_staff',0);
    					}            		
                	})->count();
                 	return $bookavailable;
                })
                ->addColumn('category', function ($book)
                {
                    return $book['getCategory']->book_category_name;
                })
                ->addColumn('action', function ($book) use ($request)
                {
                	$member_id 			= $request->get('member_id');
                    $encrypted_book_id 	= get_encrypted_value($book->book_id, true);
                	if($request->get('member_id') != ''){
                		$decrypted_member_id 	= get_decrypted_value($request->get('member_id'), true); // library member primary key

                		$library_member =  LibraryMember::where('library_member_id',$decrypted_member_id)->first();
                	}

                	$bookavailable = BookCopiesInfo::where(function($query) use ($book,$request,$library_member){
                		$query->where('book_id',$book->book_id);
	               		$query->where('book_copy_status',0);
    					if($library_member->library_member_type == 1){
    						$query->where('exclusive_for_staff',0);
    					}            		
                	})->count();
                	if($bookavailable != 0){
                        $issue_button 	= '<li><a href="#" id="student_pop_id" book-id="'.$encrypted_book_id.'" member-id="'.$member_id.'">Issue</a></li>'; 
                        return '
                        <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="caret"></span>
                            </button>
                                <ul class="dropdown-menu">
                                '.$issue_button.'    
                                </ul>
                            </div>';
                	}else{
                		return "----";
                	}
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Issue book to member
     *  @Pratyush on 22 Aug 2018.
    **/
    public function issueBook(Request $request)
    {
    	
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_book_id			= get_decrypted_value($request->get('book_id'), true);
        $decrypted_member_id		= get_decrypted_value($request->get('member_id'), true);
        if($request->has('book_id') && $request->has('member_id')){   
        	$library_member = LibraryMember::where('library_member_id',$decrypted_member_id)->first();
        	$bookavailable = BookCopiesInfo::where(function($query) use ($decrypted_book_id,$request,$library_member){
        				// Book ID 
                		$query->where('book_id',$decrypted_book_id);
                		// Book Copy Status
	               		$query->where('book_copy_status',0);
    					if($library_member->library_member_type == 1){
    						$query->where('exclusive_for_staff',0);
    					}            		
                	})->first();
        	
        	if(!empty($bookavailable)){
        		$success_msg = 'Book Issued';
	            DB::beginTransaction();
	            try
	            {

	            		$bookavailable->book_copy_status = 1;
	            		$bookavailable->save();


	            	    $book     					= New IssueBook;
		          		$book->admin_id       		= $loginInfo['admin_id'];
		                $book->update_by      		= $loginInfo['admin_id'];
		                $book->book_id 				= $decrypted_book_id; 
		                $book->member_id 			= $decrypted_member_id;
		                $book->member_type 			= $library_member->library_member_type;
		                $book->book_copies_id 		= $bookavailable->book_info_id;
		                $book->issued_from_date 	= Input::get('issue_from');
		                $book->issued_to_date 		= Input::get('issue_to');
		                $book->issued_book_status	= 1;
		                $book->save();	
	            	
	            }
	            catch (\Exception $e)
	            {
	                //failed logic here
	                DB::rollback();
	                $error_message = $e->getMessage();
	                return redirect()->back()->withErrors($error_message);
	            }
	            DB::commit();
	            return redirect('/admin-panel/issue-book/books/'.$request->get('member_id'))->withSuccess($success_msg);
            }else{
            	$error_message = 'No book to issue';
	            return redirect()->back()->withErrors($error_message);
            }
	       	
        }else{
            	$error_message = 'Invalid details';
	            return redirect()->back()->withErrors($error_message);
            }
    
    }

    /**
     *  View page for Return / renew / update (issue Book)
     *  @Pratyush on 23 Aug 2018
    **/
    public function showReturnBookPage($id = false)
    {
        
        $loginInfo = get_loggedin_user_data();
        $listData  = array(
            '1' => 'Student',
            '2' => 'Staff'
        );
        $data = array(
            'page_title'    => trans('language.return_renew_book'),
            'redirect_url'  => url('admin-panel/return-book'),
            'login_info'    => $loginInfo,
            'listData'      => $listData,
            'renew_url'     => url('admin-panel/return-book/renew-book'),
            'update_url'     => url('admin-panel/return-book/update-book')
        );
        return view('admin-panel.issue-book.issued_book_details')->with($data);
    }


    /**
     *  Get Member's Data for Issue Book page(Datatables)
     *  @Pratyush on 21 Aug 2018.
    **/
    public function ReturnBookPageData(Request $request)
    {
        
        $loginInfo      = get_loggedin_user_data();
        $issue_books    = [];
        $final_details  = [];
        // For member type student
        if($request->has('member_type') && $request->get('member_type') == 1){
            $issue_books = IssueBook::where(function($query) use ($loginInfo,$request){
                //For Student
                $query->where('member_type',1);

                //For Book status
                $query->where('issued_book_status',1);
            })
            ->join('library_members', function($join) use ($request){
                $join->on('library_members.library_member_id', '=', 'issued_books.member_id');
            })->join('students', function($join) use ($request){
                $join->on('students.student_id', '=', 'library_members.member_id');
            })->join('books', function($join) use ($request){
                $join->on('books.book_id', '=', 'issued_books.book_id');
            })->join('book_copies_info', function($join) use ($request){
                $join->on('book_copies_info.book_info_id', '=', 'issued_books.book_copies_id');
            })->get();
            if(!empty($issue_books[0]->issued_book_id)){
                foreach($issue_books as $value) {
                    $final_details[] = array(
                        'issue_book_id'         => $value->issued_book_id,
                        'issue_book_name'       => $value->book_name,
                        'member_name'           => $value->student_name,
                        'issue_book_isbn_no'    => $value->book_isbn_no,
                        'issue_book_unique_no'  => $value->book_unique_id,
                        'issue_book_start_date' => $value->issued_from_date,
                        'issue_book_end_date'   => $value->issued_to_date
                    );
                }
            }
            
        }elseif($request->has('member_type') && $request->get('member_type') == 2){
            $issue_books = IssueBook::where(function($query) use ($loginInfo,$request){
                //For Staff
                $query->where('member_type',2);

                //For Book status
                $query->where('issued_book_status',1);
            })
            ->join('library_members', function($join) use ($request){
                $join->on('library_members.library_member_id', '=', 'issued_books.member_id');
            })->join('staff', function($join) use ($request){
                $join->on('staff.staff_id', '=', 'library_members.member_id');
            })->join('books', function($join) use ($request){
                $join->on('books.book_id', '=', 'issued_books.book_id');
            })->join('book_copies_info', function($join) use ($request){
                $join->on('book_copies_info.book_info_id', '=', 'issued_books.book_copies_id');
            })->get();
            if(!empty($issue_books[0]->issued_book_id)){
                foreach($issue_books as $value) {
                    $final_details[] = array(
                        'issue_book_id'         => $value->issued_book_id,
                        'issue_book_name'       => $value->book_name,
                        'member_name'           => $value->staff_name,
                        'issue_book_isbn_no'    => $value->book_isbn_no,
                        'issue_book_unique_no'  => $value->book_unique_id,
                        'issue_book_start_date' => $value->issued_from_date,
                        'issue_book_end_date'   => $value->issued_to_date
                    );
                }
            }

        }
        
        return Datatables::of($final_details)
                ->addColumn('issue_book_duration', function ($final_details)
                {
                    $earlier = new DateTime($final_details['issue_book_start_date']);
                    $later = new DateTime($final_details['issue_book_end_date']);
                    $diff = $later->diff($earlier)->format("%a");
                    return $diff.' Days';
                })
                ->addColumn('action', function ($final_details)
                {
                    
                    $encrypted_issue_book_id = get_encrypted_value($final_details['issue_book_id'], true);
                    
                    return '
                            <div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                   <li><a href="return-book/return-book/' . $encrypted_issue_book_id . '" onclick="return confirm('."'Are you sure?'".')">Return Book</a></li>
                                   <li><a href="#" id="renew_pop_id" book-id="'.$encrypted_issue_book_id.'">Renew</a></li>
                                   <li><a href="#" id="update_pop_id" book-id="'.$encrypted_issue_book_id.'">Update</a></li>
                                 </ul>
                             </div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Renew Book
     *  @Pratyush on 23 Aug 2018.
    **/
    public function renewBook(Request $request)
    {   
        $book_issued_id  = get_decrypted_value($request->get('book_id'), true);
        $book_issued     = IssueBook::find($book_issued_id);
        if ($book_issued)
        {
            DB::beginTransaction();
            try
            {
                $history    = new IssueBookHistory;
                $history->admin_id          = $book_issued->admin_id;
                $history->update_by         = $book_issued->update_by;
                $history->book_id           = $book_issued->book_id;
                $history->member_id         = $book_issued->member_id;
                $history->member_type       = $book_issued->member_type;
                $history->book_copies_id    = $book_issued->book_copies_id;
                $history->issued_from_date  = $book_issued->issued_from_date;
                $history->issued_to_date    = $book_issued->issued_to_date;
                $history->save();

                $book_issued->issued_from_date  = Input::get('issue_from');;
                $book_issued->issued_to_date    = Input::get('issue_to');;
                $book_issued->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            $success_msg = "Issue book's details updated!";
            return redirect('admin-panel/return-book')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Book details not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Update Book Dates
     *  @Pratyush on 23 Aug 2018.
    **/
    public function updateBook(Request $request)
    {   
        $book_issued_id  = get_decrypted_value($request->get('book_id_update'), true);
        $book_issued     = IssueBook::find($book_issued_id);

        if($book_issued){

            $book_issued->issued_from_date  = Input::get('issue_from');
            $book_issued->issued_to_date    = Input::get('issue_to');
            $book_issued->save();
            $success_msg = "Issue book's details updated!";
            return redirect('admin-panel/return-book')->withSuccess($success_msg);
        }else{

            $error_message = "Book details not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Return Issued Book
     *  @Pratyush on 23 Aug 2018.
    **/
    public function returnBook($id = false)
    {   
        $book_issued_id  = get_decrypted_value($id, true);
        $book_issued     = IssueBook::find($book_issued_id);
        
        if($book_issued){
            
            $bookcopy = BookCopiesInfo::where('book_info_id',$book_issued->book_copies_id)->first();

            if($bookcopy){
                $bookcopy->book_copy_status = 0;
                $bookcopy->save();                 
            }

            $book_issued->issued_book_status  = 2;
            $book_issued->save();
            $success_msg = "Issue book's details updated!";
            return redirect('admin-panel/return-book')->withSuccess($success_msg);
        }else{

            $error_message = "Book details not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
