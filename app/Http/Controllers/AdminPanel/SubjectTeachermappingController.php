<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model 
use App\Model\SubjectClassMapping\SubjectClassmapping; // Model 
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model 
use App\Model\Staff\Staff; // Model 
use App\Model\Subject\Subject; // Model 
use App\Model\Classes\Classes; // Model
use App\Model\Section\Section; // Model
use Yajra\Datatables\Datatables;

class SubjectTeachermappingController extends Controller
{
    /**
     *  View page for Subject Teacher mapping
     *  @Pratyush on 08 Aug 2018
    **/
    public function index()
    {

        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_map_subject_teacher'),
            'redirect_url'  => url('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.map-subject-to-teacher.index')->with($data);
    }

    /**
     *  Get Subject Teacher mapping's Data for view page(Datatables)
     *  @Pratyush on 08 Aug 2018.
    **/
    public function anyData()
    {
        $class  			= Section::orderBy('class_id', 'ASC')->with('sectionClass')->with('sectionClass.getBoard')->get();
        // p($class[0]['sectionClass']);
        
        $arr_boards     = get_all_school_boards();
        $arr_medium             = get_all_mediums();
        return Datatables::of($class,$arr_medium,$arr_boards)
        		->addColumn('no_of_subject', function ($class)
                {
                    return SubjectSectionMapping::where('class_id',$class->class_id)->where('section_id',$class->section_id)->count();
                    
                })
                ->addColumn('no_of_map_subject', function ($class)
                {
                    // SubjectTeacherMapping::select('subject_id')->where('class_id',$class->class_id)->where('section_id',$class->section_id)->groupBy('subject_id')->get()->count();
                    return SubjectTeacherMapping::select('subject_id')->where('staff_ids','!=','')->where('class_id',$class->class_id)->where('section_id',$class->section_id)->groupBy('subject_id')->get()->count(); 
                })
                ->addColumn('no_of_remain_subject', function ($class)
                {
                	$tot_sub 	= 0;
                	$map_sub 	= 0;
                	$remaining 	= 0;
                	$tot_sub 	= SubjectSectionMapping::where('class_id',$class->class_id)->where('section_id',$class->section_id)->count();
                    $map_sub    = SubjectTeacherMapping::select('subject_id')->where('staff_ids','!=','')->where('class_id',$class->class_id)->where('section_id',$class->section_id)->groupBy('subject_id')->get()->count();

                	$remaining 	= $tot_sub - $map_sub;
                    return $remaining;
                    
                })
                ->addColumn('class_sections', function ($class) use ($arr_medium,$arr_boards)
                {
                    return $class['sectionClass']['class_name'].' - '.$arr_medium[$class['sectionClass']['medium_type']].' - '.$class['sectionClass']['getBoard']['board_name'].' - '.$class->section_name;
                    
                    
                })
        		->addColumn('action', function ($class)
                {
                    $encrypted_section_id = get_encrypted_value($class->section_id, true);
                    return '<div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                     <li><a href="map-teacher/' . $encrypted_section_id . '" ">Map Teacher</a></li>
                                     <li><a href="view-report/' . $encrypted_section_id . '" ">View Report</a></li>
                                 </ul>
                             </div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  View page for Subject Teacher mapping - Map Subject
     *  @Pratyush on 08 Aug 2018
    **/
    public function getMapSubject($id)
    {
        $loginInfo  = get_loggedin_user_data();
        $section_info = get_section_info_by_id($id); 
        $data = array(
            'page_title'    => trans('language.map_subject_teacher'),
            'redirect_url'  => url('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/teacher-subject-mapping/save'),
            'section_info'    => $section_info
        );
        return view('admin-panel.map-subject-to-teacher.map_teacher')->with($data);
    }

    /**
     *  Get Subject Teacher mapping's Data for view page(Datatables)
     *  @Pratyush on 08 Aug 2018.
    **/
    public function anyMapSubjectData(Request $request)
    {
        $class_id	= $request->get('class_id');
        $section_id	= $request->get('section_id');
        $loginInfo 			= get_loggedin_user_data();
        $subject 			= SubjectSectionMapping::where('class_id',$class_id)->where('section_id',$section_id)->with('getSubjects')->get();
        // p(empty($subject));
        $arr_staff          = get_all_staffs();
        $listData           = [];
        $table = 
        "<table class='table m-b-0 c_list' style='width:100%'>
        <thead>
        <tr>
            <th>Subject Code - Subject Name</th>
            <th>Teacher Name</th>
            
        </tr>
        </thead>
        <tbody>";
        if(!empty($subject)) {
            $success_msg ="No Subject is mapped to this class";
        } 
        foreach ($subject as $key => $value){
        $staff_id_arr = array();
        $teacher_data = SubjectTeacherMapping::where('class_id',$class_id)->where('section_id',$section_id)->where('subject_id',$value->subject_id)->get()->toArray();
        $class_subject_staff_map_id = NULL;
        foreach ($teacher_data as $teacherkey){  
            $staff_id_arr[] = $teacherkey['staff_ids'];
            $class_subject_staff_map_id = $teacherkey['class_subject_staff_map_id'];
        }
        $table .= "<tr>";
        $table .= "<td width='200px;'>".$value['getSubjects']->subject_code.' - '.$value['getSubjects']->subject_name."</td>
                    <td width='50%;'>";
        $table  .= "
        <input type='hidden' name='subjectMap[$key][class_subject_staff_map_id]' value='".$class_subject_staff_map_id."' />
        <input type='hidden' name='subjectMap[$key][subject_id]' value='".$value->subject_id."' />
        <label class='field select' style='width: 50%'><select class='form-control show-tick select_form3 select2' name='subjectMap[$key][teacher_id]'  id='teacher_ids_".$value->subject_id."'>";
        $table .= "<option value='' >Select Teacher</option>";
        foreach($arr_staff as $key2 => $value2){ // For Teacher's details
            $disable = '';

            if($key2 == ''){
                $disable = 'disabled="disabled"';
            }
            $default_select = '';
            if(in_array($key2, $staff_id_arr)){
                $default_select = 'selected';
            }
            $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
        } 
        $table .= "</select></label></td>";
        // $table  .= "<td><button class='btn btn-info' onclick='return assignteacher(".$value->subject_id.");' subject-id = '".$value->subject_id."'>Save</button>";
        $table .= "</tr>";                          

        }
        $table .= "</tbody></table>";
        // echo $table;die;
        return response()->json(['status'=>1001,'data'=>$table, 'success_msg'=>$success_msg]);
    }

    /**
     *  View page for view report
     *  @Pratyush on 08 Aug 2018
    **/
    public function getViewReport($id)
    {
        $loginInfo = get_loggedin_user_data();
        $section_info = get_section_info_by_id($id); 
        $data = array(
            'page_title'    => trans('language.mst_view_report'),
            'redirect_url'  => url('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/teacher-subject-mapping/save'),
            'section_info'    => $section_info
        );
        return view('admin-panel.map-subject-to-teacher.view_report')->with($data);
    }

    /**
     *  Add and update subject class mapping's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function save(Request $request)
    {
        $loginInfo      	= get_loggedin_user_data();
        $class_id			= $request->get('class_id');
        $section_id			= $request->get('section_id');
        $admin_id           = $loginInfo['admin_id'];
        try
            {
            if(!empty($request->get('subjectMap'))) {
            foreach ($request->get('subjectMap') as $key => $map) {
                $success_msg = "Teacher successfully assign";
                if(($map['teacher_id'] != '' && $map['class_subject_staff_map_id'] != '') || ($map['teacher_id'] == '' && $map['class_subject_staff_map_id'] != '')){
                    // edit case
                    $mapping	            = SubjectTeacherMapping::find($map['class_subject_staff_map_id']);		
                    $mapping->admin_id      = $admin_id;
                    $mapping->update_by     = $loginInfo['admin_id'];
                    $mapping->class_id 	 	= $class_id;
                    $mapping->section_id 	= $section_id;
                    $mapping->subject_id 	= $map['subject_id'];
                    $mapping->staff_ids 	= $map['teacher_id'];
                    $mapping->save();
                    // edit case
                } else if($map['teacher_id'] != '' && $map['class_subject_staff_map_id'] == ''){
                    // add case
                    $mapping	            = new SubjectTeacherMapping;		
                    $mapping->admin_id      = $admin_id;
                    $mapping->update_by     = $loginInfo['admin_id'];
                    $mapping->class_id 	 	= $class_id;
                    $mapping->section_id 	= $section_id;
                    $mapping->subject_id 	= $map['subject_id'];
                    $mapping->staff_ids 	= $map['teacher_id'];
                    $mapping->save();
                }
            }
            } else {
                $success_msg = "No Subject is mapped to this class";
            }
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        
        return redirect()->back()->withSuccess($success_msg);
	}

	/**
     *  Get Subject Teacher Mapping view report page's data (Datatables)
     *  @Pratyush on 08 Aug 2018.
    **/
    public function anyViewReportData(Request $request)
    {
        // p($request->all());  
        $loginInfo 		= get_loggedin_user_data();
        $class_id		= $request->get('class_id');
        $section_id		= $request->get('section_id');

        $teacher 	= SubjectTeacherMapping::where('staff_ids','!=','')->where('class_id',$class_id)->where('section_id',$section_id)->with('getStaff')->groupBy('staff_ids')->get();
        // p($teacher);    
        return Datatables::of($teacher)
                ->addColumn('teacher_code', function ($teacher)
                {
                    $staff_attendance_unique_id = '----';
                    if(isset($teacher['getStaff']->staff_attendance_unique_id)){
                        $staff_attendance_unique_id = $teacher['getStaff']->staff_attendance_unique_id;
                    }
        			return $staff_attendance_unique_id;
                })
                ->addColumn('teacher_name', function ($teacher)
                {
                    $staff_name = '----';
                    if(isset($teacher['getStaff']->staff_name)){
                        $staff_name = $teacher['getStaff']->staff_name;
                    }
        			return $staff_name;
                })
                ->addColumn('subjects', function ($teacher)
                {
                	$class_details 	= SubjectTeacherMapping::select('class_id')->where('class_id',$teacher->class_id)->where('section_id',$teacher->section_id)->with('getClass')->groupBy('section_id')->where('staff_ids',$teacher->staff_ids)->get();
                    $sub_class_det = ''; // Subject Detail class wise.

                	foreach($class_details as $value){ // Getting subject details

                        $sub_details = []; // Subjects details
                        $subject_details  = SubjectTeacherMapping::select('subject_id')->with('getSubject')->where('staff_ids',$teacher->staff_ids)->where('section_id',$teacher->section_id)->where('class_id',$value->class_id)->get();
                        // $subject_details  = SubjectTeacherMapping::with('getSubject')->where('staff_ids',$teacher->staff_ids)->where('class_id',$value->class_id)->get();
                        foreach ($subject_details as $sub_det){
                            $sub_details[] = $sub_det['getSubject']->subject_name .'('.$sub_det['getSubject']->subject_code.')';
                        }

                    $sub_class_det .= '<strong>'.$value['getClass']->class_name.'</strong> : '.implode(",",$sub_details) .'<br/>';
                	}
                    
        			return $sub_class_det;
                })->rawColumns(['teacher_code' => 'teacher_code','teacher_name' => 'teacher_name','subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
    }
}