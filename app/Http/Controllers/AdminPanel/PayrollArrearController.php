<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\Arrear; // Model
use App\Model\Payroll\ArrearMap; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollArrearController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Arrear
     * @Khushbu on 15 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $arrear = $staff = $staff_mapped = $staff_ids_arr = []; 
        $loginInfo   = get_loggedin_user_data();
        $staff       = Staff::get(); 
        if(!empty($id)) {
            $decrypted_arrear_id 	= get_decrypted_value($id, true);
        	$arrear      		    = Arrear::Find($decrypted_arrear_id);
            $page_title             = trans('language.edit_arrear');
        	$save_url    			= url('admin-panel/payroll/manage-arrear-save/'. $id);
            $submit_button  		= 'Update';
            $staff_mapped           = ArrearMap::select('staff_id')->where('pay_arrear_id', $decrypted_arrear_id)->get()->toArray();
            foreach($staff_mapped as $key => $value){
                $staff_ids_arr[] = $value['staff_id'];
            }
        } else {
            $page_title             = trans('language.add_arrear');
	 		$save_url    			= url('admin-panel/payroll/manage-arrear-save');
	 		$submit_button  		= 'Save';
	 	}
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'arrear'          => $arrear,
            'staff'           => $staff,
            'staff_ids_arr'   => $staff_ids_arr,
        );
        return view('admin-panel.payroll-arrear.add')->with($data);
    }

    /**
     *  Add & Update of Payroll Arrear
     *  @Khushbu on 15 Feb 2019
     */
    public function save(Request $request, $id = NULL) {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_arrear_id		= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        $session                    = get_current_session();
        if(!empty($id)) {
            $arrear             = Arrear::Find($decrypted_arrear_id);
            if(!$arrear) {
                return redirect('admin-panel/payroll/manage-arrear')->withErrors('Arrear not found!');
            }
            $admin_id    = $arrear->admin_id;
            $success_msg = 'Arrear updated successfully!';
        } else {
            $arrear    	     = New Arrear;
            $success_msg     = 'Arrear saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'arrear_name'    	   => 'required|unique:pay_arrear,arrear_name,' . $decrypted_arrear_id . ',pay_arrear_id'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            if(!empty($request->get('staffs')) && count($request->get('staffs')) != 0){
                $success_msg = 'Staffs are not selected.';
                $error_messages = [];

            DB::beginTransaction();
            try
            {
               
                $arrear->admin_id         = $admin_id;
                $arrear->update_by        = $loginInfo['admin_id'];
                $arrear->arrear_name   	  = Input::get('arrear_name');
                $arrear->arrear_from_date = Input::get('arrear_from_date');
                $arrear->arrear_to_date   = Input::get('arrear_to_date');
                $arrear->arrear_amount    = Input::get('arrear_amount');
                $arrear->arrear_total_amt = Input::get('arrear_total_amt');
                $arrear->round_off        = Input::get('round_off');
                $arrear->save();

                foreach ($request->get('staffs') as $value){
                    if($value['exist'] == 1 && !isset($value['staff_id']))
                    {
                        // Delete Case
                        $success_msg = "Record Updated successfully";
                        $arrear_map  = ArrearMap::where('staff_id',$value['exist_id'])->first();
                        $arrear_map->delete();
                
                    }
                    if($value['exist'] == 0 && isset($value['staff_id']))
                    {
                        //Add Case
                        $arrear_map                   = New ArrearMap;
                        $success_msg = 'Staff Mapped successfully.';
                        $arrear_map->admin_id         = $admin_id;
                        $arrear_map->update_by        = $loginInfo['admin_id'];
                        $arrear_map->session_id 	  = $session['session_id'];
                        $arrear_map->pay_arrear_id 	  = $arrear->pay_arrear_id;
                        $arrear_map->staff_id   	  = $value['staff_id'];
                        $arrear_map->save();
                    }    
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
                DB::commit();
            } 
        }
        return redirect('admin-panel/payroll/manage-arrear')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Arrear's Data fo view page
     *  @Khushbu on 15 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
    	$arrear 		    = Arrear::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_arrear_name') !=  NULL)
            {
                $query->where('arrear_name', 'Like', $request->get('s_arrear_name').'%');
            }
        })->orderBy('pay_arrear_id','DESC')->with('ArrearMap')->get();
        return Datatables::of($arrear)
        ->addColumn('date_range', function($arrear) {
            $date_range = date('d-m-Y', strtotime($arrear['arrear_from_date'])). ' - ' . date('d-m-Y', strtotime($arrear['arrear_to_date']));
            return $date_range;
        })
        ->addColumn('round_off', function($arrear) {
            if($arrear['round_off'] == 0) {
                $round_off = 'Nearest Rupee';
            } else {
                $round_off = 'Highest Rupee';
            }
            return $round_off;
        })
        ->addColumn('staff_mapped', function($arrear) {
            $encrypted_arrear_id  = get_encrypted_value($arrear->pay_arrear_id, true);
            $map_staff = '<a href="" class="btn btn-raised btn-primary arrear-staff" data-toggle="modal" data-target="#viewStaffModel" arrear_id='.$arrear->pay_arrear_id.'>View Staff</a>';
            return $map_staff;
        })
    	->addColumn('action', function($arrear) use($request) {
            $encrypted_arrear_id  = get_encrypted_value($arrear->pay_arrear_id, true);
              return '<div class="text-center">
                    
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-arrear/'.$encrypted_arrear_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-arrear/' . $encrypted_arrear_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['action' => 'action', 'staff_mapped' => 'staff_mapped'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-arrear');
    }  
    /**
	 *	Destroy Data of Payroll Arrear
     *  @Khushbu on 16 Feb 2019
	**/
	public function destroy($id) {
        $arrear_id 		= get_decrypted_value($id, true);
        $arrear_map     = ArrearMap::where('pay_arrear_id',$arrear_id)->get();
        $arrear 	    = Arrear::find($arrear_id);
        if ($arrear)
        {
            DB::beginTransaction();
            try
            {
                foreach ($arrear_map as $key => $value) {
                    $arrear_map[$key]->delete();
                }
                $arrear->delete();
                $success_msg = "Arrear deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Arrear not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page of Staff
     *  @Khushbu on 16 Feb 2019
     */
    // public function viewMapEmployees($id) {
    //     $arr_arrear          = [];
    //     $loginInfo      	 = get_loggedin_user_data();
    //     $arrear_id 		     = get_decrypted_value($id, true);
    //     $page_title          = trans('language.map_employees');
    //     $arr_arrear          = get_all_arrear();
    //     $arr_arrear_list     = add_blank_option($arr_arrear,'Select Arrear');
    //     $data                = array(
    //         'loginInfo'      => $loginInfo,
    //         'page_title'     => $page_title,
    //         'save_url'		 => url('admin-panel/payroll/manage-arrear-map-employees-save/'.$id),
    //         'arrear_id'      => $arrear_id,
    //         'arr_arrear_list' => $arr_arrear_list,
    //     );
    //     return view('admin-panel.payroll-arrear.map-employees')->with($data);
    // } 

    /**
     * Get Employee's Map Data for view page
     *  @Khushbu on 16 Feb 2019 
     */
    // public function anyDataMapEmployees(Request $request) {  
    //     $staff_mapped = ArrearMap::select('staff_id')->where('pay_arrear_id', $request->get('arrear_id'))->get()->toArray();
    //     $staff = Staff::get(); 
    //     $staff_ids_arr 	    = array();
    //     foreach($staff_mapped as $key => $value){
    //         $staff_ids_arr[] = $value['staff_id'];
    //     }
    //     return Datatables::of($staff, $staff_ids_arr)
    //     ->addColumn('checkbox', function($staff) use($staff_ids_arr){
    //         $check = '';
    //         $exist = 0;
    //         if(!empty($staff_ids_arr)){
    //             if(in_array($staff->staff_id, $staff_ids_arr)){
    //                 $check = 'checked';
    //                 $exist = 1;
    //             }
    //         }
    //         return '
    //             <input type="hidden" name="staffs['.$staff->staff_id.'][exist]" value="'.$exist.'" >
    //             <input type="hidden" name="staffs['.$staff->staff_id.'][exist_id]" value="'.$staff->staff_id.'" >

    //             <div class="checkbox" id="customid">
    //                 <input type="checkbox" id="staff'.$staff->staff_id.'" name="staffs['.$staff->staff_id.'][staff_id]" class="check" value="'.$staff->staff_id.'" '.$check.'>
    //                 <label class="from_one1 " style="margin-bottom: 4px !important;"  for="staff'.$staff->staff_id.'"></label>
    //             </div>';
    //     })
    //     ->addColumn('employee_profile', function($staff) {
    //         $profile = '';
    //         if($staff->staff_profile_img != '') {
    //             $staff_profile_path = check_file_exist($staff->staff_profile_img, 'staff_profile');
    //             $profile = "<img src=".url($staff_profile_path)." height='30' />";
    //         }
    //         $staff_name = $staff->staff_name;
    //         $employee_profile = $profile." ".$staff_name;
    //         return $employee_profile;
    //     })
    //     ->addColumn('previous_arrear', function($staff) {
    //         $previous_arrear =  2000;
    //         return $previous_arrear;
    //     })->rawColumns(['checkbox' => 'checkbox', 'employee_profile' => 'employee_profile','previous_arrear' => 'previous_arrear'])->addIndexColumn()
    //     ->make(true); 
    //     return redirect('/payroll/manage-arrear');
    // }

    /**
     *  Add Page of Employee's Map 
     *  @Khushbu on 16 Feb 2019 
     */
    // public function saveMapEmployees(Request $request, $id= NULL) {
    //     $loginInfo      = get_loggedin_user_data();
    //     $session        = get_current_session();
    //     $admin_id       = $loginInfo['admin_id'];
    //     $arrear_id 		= get_decrypted_value($id, true);
    //     if(!empty($request->get('staffs')) && count($request->get('staffs')) != 0){
    //         $success_msg = 'Staffs are not selected.';
    //         $error_messages = [];
    //         DB::beginTransaction();
    //         try
    //         {
    //             foreach ($request->get('staffs') as $value){
    //                 if($value['exist'] == 1 && !isset($value['staff_id']))
    //                 {
    //                     // Delete Case
    //                     $success_msg = "Record Updated successfully";
    //                     $arrear_map  = ArrearMap::where('staff_id',$value['exist_id'])->first();
    //                     $arrear_map->delete();
                
    //                 }
    //                 if($value['exist'] == 0 && isset($value['staff_id']))
    //                 {
    //                     //Add Case
    //                     $arrear_map                   = New ArrearMap;
    //                     $success_msg = 'Staff Mapped successfully.';
    //                     $arrear_map->admin_id         = $admin_id;
    //                     $arrear_map->update_by        = $loginInfo['admin_id'];
    //                     $arrear_map->session_id 	  = $session['session_id'];
    //                     $arrear_map->pay_arrear_id 	  = $arrear_id ;
    //                     $arrear_map->staff_id   	  = $value['staff_id'];
    //                     $arrear_map->save();
    //                 }    
    //             }
    //         }
    //         catch (\Exception $e)
    //         {
    //             //failed logic here
    //             DB::rollback();
    //              return Datatables::of($staff, $staff_ids_arr));
    //              return Datatables::of($staff, $staff_ids_arr)rrors($error_message);
    //         }
    //         DB:: return Datatables::of($staff, $staff_ids_arr)
    //         retu return Datatables::of($staff, $staff_ids_arr)ss($success_msg)->withErrors($error_messages);   
    //     } else { return Datatables::of($staff, $staff_ids_arr)
    //         return redirect()->back();
    //     }         
    // }

    /** 
     *  Get Staff Map Data
     *  @Khushbu on 18 Jan 2019.
    **/
    public function getArrearStaffData(Request $request) {
        $arrear_staff_map        = ArrearMap::where('pay_arrear_id',$request->get('arrear_id'))->with('ArrearStaff')->get();
        return Datatables::of($arrear_staff_map)
        ->addColumn('employee_profile', function($arrear_staff_map) {
        $profile = '';
        if($arrear_staff_map['ArrearStaff']['staff_profile_img'] != '') {
            $staff_profile_path = check_file_exist($arrear_staff_map['ArrearStaff']['staff_profile_img'], 'staff_profile');
            $profile = "<img src=".url($staff_profile_path)." height='30' />";
        }
        $staff_name = $arrear_staff_map['ArrearStaff']['staff_name'];
        $employee_profile = $profile." ".$staff_name;
        return $employee_profile;
        })
        ->addColumn('previous_arrear', function($bonus_staff_map) {
            $previous_arrear =  2000;
            return $previous_arrear;
        })->rawColumns(['employee_profile' => 'employee_profile','previous_arrear' => 'previous_arrear'])->addIndexColumn()
        ->make(true); 
    } 
}
