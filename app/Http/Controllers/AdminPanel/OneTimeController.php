<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\OneTime; // Model
use Yajra\Datatables\Datatables;

class OneTimeController extends Controller
{
    /**
     *  View page for One time head
     *  @shree on 1 Oct 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $onetime = [];
        $arr_class              = get_all_classes_mediums();
        $onetime['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'    => trans('language.view_one_time'),
            'redirect_url'  => url('admin-panel/one-time/view-one-time'),
            'login_info'    => $loginInfo,
            'onetime'       => $onetime
        );
        return view('admin-panel.one-time-head.index')->with($data);
    }

    /**
     *  Add page for one time
     *  @Shree on 1 Oct 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $onetime      	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_ot_head_id 	    = get_decrypted_value($id, true);
            $onetime      			    = OneTime::Find($decrypted_ot_head_id);
            if (!$onetime)
            {
                return redirect('admin-panel/one-time/add-one-time')->withError('One time head not found!');
            }
            if($onetime->ot_classes != ""){
                $onetime->ot_classes = explode(",",$onetime->ot_classes);
            }
            $page_title             	= trans('language.edit_one_time');
            $encrypted_ot_head_id       = get_encrypted_value($onetime->ot_head_id, true);
            $save_url               	= url('admin-panel/one-time/save/' . $encrypted_ot_head_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_one_time');
            $save_url      = url('admin-panel/one-time/save');
            $submit_button = 'Save';
        }
        $arr_refundable = \Config::get('custom.refundable_status');
        $arr_student_for = \Config::get('custom.student_for');
        $arr_deduct_imprest_status = \Config::get('custom.deduct_imprest_status');
        $arr_receipt_status = \Config::get('custom.receipt_status');
        $arr_class              = get_all_classes_mediums();
        $onetime['arr_class']   = $arr_class;
        $onetime['arr_refundable'] = $arr_refundable;
        $onetime['arr_student_for'] = $arr_student_for;
        $onetime['arr_deduct_imprest_status'] = $arr_deduct_imprest_status;
        $onetime['arr_receipt_status'] = $arr_receipt_status;
       
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'onetime' 		    => $onetime,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/one-time/view-one-time'),
        );
        return view('admin-panel.one-time-head.add')->with($data);
    }

    /**
     *  Add and update one time's data
     *  @Shree on 1 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo              = get_loggedin_user_data();
        $decrypted_ot_head_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $onetime = OneTime::find($decrypted_ot_head_id);
            $admin_id = $onetime['admin_id'];
            if (!$onetime)
            {
                return redirect('/admin-panel/one-time/add-one-time/')->withError('One time head not found!');
            }
            $success_msg = 'One time head updated successfully!';
        }
        else
        {
            $onetime     = New OneTime;
            $success_msg = 'One time head saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'ot_particular_name'   => 'required|unique:one_time_heads,ot_particular_name,' . $decrypted_ot_head_id . ',ot_head_id',
            'ot_alias' => 'required',
            'ot_classes' => 'required',
            'ot_amount' => 'required',
            'ot_date' => 'required',
            'ot_refundable' => 'required',
        ], [
            'ot_classes.required' => 'Please select classes.',
            'ot_particular_name.unique' => 'Particular Name has already been taken.'
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $classes = null;
                if($request->get('ot_classes') != ""){
                    $classes = implode(",",$request->get('ot_classes'));
                }
                $onetime->admin_id            	= $admin_id;
                $onetime->update_by           	= $loginInfo['admin_id'];
                $onetime->ot_particular_name 	= Input::get('ot_particular_name');
                $onetime->ot_alias 	            = Input::get('ot_alias');
                $onetime->ot_classes 	        = $classes;
                $onetime->ot_amount 	        = Input::get('ot_amount');
                $onetime->ot_date 	            = Input::get('ot_date');
                $onetime->ot_refundable         = Input::get('ot_refundable');
                $onetime->for_students          = Input::get('for_students');
                $onetime->deduct_imprest_status = Input::get('deduct_imprest_status');
                $onetime->receipt_status        = Input::get('receipt_status');
                $onetime->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/one-time/view-one-time')->withSuccess($success_msg);
    }

    /**
     *  Get one time's Data for view page(Datatables)
     *  @shree on 1 Oct 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	= get_loggedin_user_data();
        $onetime  	= OneTime::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('ot_particular_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->whereRaw('FIND_IN_SET('.$request->get('class_id').',ot_classes)');
            }
        })->orderBy('ot_head_id', 'DESC')->get();

        return Datatables::of($onetime)
        ->addColumn('classes', function ($onetime)
        {
            return '<button type="button" class="btn btn-raised btn-primary classes"  head-id="'.$onetime->ot_head_id.'" >
            View Classes
            </button>';
            // return $allNames; 
        })
        ->addColumn('ot_date', function ($onetime)
        {
            $date = date("d M Y", strtotime($onetime->ot_date));
            return $date; 
        })
        ->addColumn('ot_refundable', function ($onetime)
        {
            $arr_refundable = \Config::get('custom.refundable_status');
            $refundable     = $arr_refundable[$onetime->ot_refundable];
            return $refundable;
        })
        ->addColumn('for_students', function ($onetime)
        {
            $arr_for_students = \Config::get('custom.student_for');
            $for_students     = $arr_for_students[$onetime->for_students];
            return $for_students;
        })
        ->addColumn('deduct_imprest_status', function ($onetime)
        {
            $arr_deduct_imprest_status = \Config::get('custom.deduct_imprest_status');
            $deduct_imprest_status     = $arr_deduct_imprest_status[$onetime->deduct_imprest_status];
            return $deduct_imprest_status;
        })
        ->addColumn('receipt_status', function ($onetime)
        {
            $arr_receipt_status = \Config::get('custom.receipt_status');
            $receipt_status     = $arr_receipt_status[$onetime->receipt_status];
            return $receipt_status;
        })
        ->addColumn('action', function ($onetime)
        {

            $encrypted_ot_head_id = get_encrypted_value($onetime->ot_head_id, true);
            if($onetime->ot_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="one-time-status/'.$status.'/' . $encrypted_ot_head_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-one-time/' . $encrypted_ot_head_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-one-time/' . $encrypted_ot_head_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action', 'classes'=>'classes'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy one time's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $ot_head_id 	= get_decrypted_value($id, true);
        $onetime 		= OneTime::find($ot_head_id);
        
        $success_msg = $error_message =  "";
        if ($onetime)
        {
            DB::beginTransaction();
            try
            {
                $onetime->delete();
                $success_msg = "One time head deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/one-time/view-one-time')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/one-time/view-one-time')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "One time head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change one time's status
     *  @Shree on 1 Oct 2018.
    **/
    public function changeStatus($status,$id)
    {
        $ot_head_id       = get_decrypted_value($id, true);
        $onetime          = OneTime::find($ot_head_id);
        if ($onetime)
        {
            $onetime->ot_status  = $status;
            $onetime->save();
            $success_msg = "One time head status update successfully!";
            return redirect('admin-panel/one-time/view-one-time')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "One time head not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

     /**
     *  Get classes Data for view page(Datatables)
     *  @shree on 12 Oct 2018.
    **/
    public function classes(Request $request)
    {
        $onetime  	= OneTime::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
            {
                $query->where('ot_head_id', "=", $request->get('id'));
            }
        })->orderBy('ot_head_id', 'DESC')->get()->toArray();
        
        $class_arr = explode(",",$onetime[0]['ot_classes']);
        $allNames = "";
        $name = [];
        foreach($class_arr as $key => $id){
            $encrypted_id = get_encrypted_value($id,true);
            $name[]['class_name'] = get_class_name_by_id($encrypted_id);
        }
        return Datatables::of($name)
        ->addColumn('class_name', function ($name)
        {
            return $name['class_name'];
        })
        ->addIndexColumn()
        ->make(true);
    }
}
