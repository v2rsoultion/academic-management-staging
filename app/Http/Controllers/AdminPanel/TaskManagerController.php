<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\TaskManager\Task; // Model
use App\Model\TaskManager\TaskResponse; // Model
use App\Model\Admin\Admin; // Model
use App\Model\Classes\Classes;
use App\Model\Section\Section;
use App\Model\Staff\Staff;
use App\Model\Student\Student;
use Yajra\Datatables\Datatables;
use Mbarwick83\Shorty\Facades\Shorty;
use anlutro\BulkSms\BulkSmsService;
use Redirect;


class TaskManagerController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('12',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /** 
     *  Add Page of Tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function add(Request $request, $id = NULL) 
    {
        $task               = [];
    	$loginInfo          = get_loggedin_user_data();
        $arr_task_priority  = \Config::get('custom.task_priority');
        $arr_task_type      = \Config::get('custom.task_type');
        $arr_task_file      = \Config::get('custom.task_file');
        $arr_staff          = get_all_staffs();
        $arr_class          = get_all_classes_mediums();
        
        if(!empty($id))
        {
            $decrypted_task_id      = get_decrypted_value($id, true);
            $task                   = Task::Find($decrypted_task_id);
            $page_title             = trans('language.edit_task');
            $save_url               = url('admin-panel/task-manager/save/'. $id);
            $submit_button          = 'Update';
            $arr_section            = get_class_section($task->class_id);
            $arr_student            = get_student_list_class_section($task->class_id,$task->section_id);
        }
        else
        {
            $page_title             = trans('language.add_task');
            $save_url               = url('admin-panel/task-manager/save');
            $submit_button          = 'Save';
            $arr_section            = [];
            $arr_student            = [];
        }

        $task['arr_staff']          = add_blank_option($arr_staff, 'Select Staff');
        $arr_task_priority1['']     = add_blank_option($arr_task_priority, 'Select Priority');
        $task['arr_task_priority']  = $arr_task_priority1;
        $task['arr_task_type']      = $arr_task_type;
        $task['arr_task_file']      = $arr_task_file;
        $task['arr_class']          = add_blank_option($arr_class, 'Select Class');
        $task['arr_section']        = add_blank_option($arr_section, 'Select Section');
        $task['arr_student']        = add_blank_option($arr_student, 'Select Student');

        $data                   = array(
            'page_title'        => $page_title,
            'login_info'        => $loginInfo,
            'save_url'          => $save_url,
            'submit_button'     => $submit_button,
            'task'              => $task    

        );
        return view('admin-panel.task-manager.add')->with($data);
    }
    /**
     *  Add & Update of Tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function save(Request $request, $id = NULL) 
    {
        $loginInfo              = get_loggedin_user_data();
        $decrypted_task_id      = get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];

        if(!empty($id))
        {
            $task               = Task::Find($decrypted_task_id);
            if(!$task) {
                return redirect('admin-panel/task-manager/add-task')->withErrors('Task not found');
            }
            $admin_id           = $task->admin_id;
            $success_msg        = "Task updated successfully!"; 
            $title              = Input::get('task_name');
        }
        else
        {
            $task               = New Task;
            $success_msg        = "Task saved successfully!"; 
            $title              = Input::get('task_name');
        }
        $validator              =  Validator::make($request->all(), [
            'task_name'         => 'required',
            'task_priority'     => 'required',
            'task_date'         => 'required',
            'task_description'  => 'required',
            'task_type'         => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $task->admin_id           = $admin_id;
                $task->update_by          = $loginInfo['admin_id'];
                $task->task_name          = Input::get('task_name');
                $task->task_priority      = Input::get('task_priority');
                $task->task_date          = Input::get('task_date');
                $task->task_description   = Input::get('task_description');
                $task->task_type          = Input::get('task_type');
                $task->class_id           = Input::get('class_id');
                $task->section_id         = Input::get('section_id');
                $task->staff_id           = Input::get('staff_id');
                $task->student_id         = Input::get('student_id');


                if ($request->hasFile('task_file'))
                {
                    if (!empty($id)){
                        $task_file = check_file_exist($task->task_file, 'task_file');
                        if (!empty($task_file))
                        {
                            unlink($task_file);
                        } 
                    }

                    $file                          = $request->file('task_file');
                    $config_upload_path            = \Config::get('custom.task_file');
                    $destinationPath               = public_path() . $config_upload_path['upload_path'];
                    $ext                           = $file->getClientOriginalExtension();
                    $name                          = str_replace(' ', '', substr($file->getClientOriginalName(),0,-4));
                    $filename                      = $name.mt_rand(0,100000).time().'.'.$ext;
                    $file->move($destinationPath, $filename);
                    $task->task_file = $filename;
                }

                $task->save();

                if($task->task_type == 1){
                    $notification = $this->send_push_notification_student($task->task_id,$task->student_id,'task_add_student',$admin_id,$task->task_description,$title);
                } else {
                    $notification = $this->send_push_notification_staff($task->task_id,$task->staff_id,'task_add_staff',$admin_id,$task->task_description,$title);
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/task-manager/view-task')->withSuccess($success_msg);
    }
    /**
     *  View page of Tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function index() 
    {
    	$loginInfo  = get_loggedin_user_data();

        $arr_task_type   = \Config::get('custom.task_type');
       
        $page_title = trans('language.view_task');
        $data               =  array(
            'page_title'     => $page_title,
            'arr_task_type'  => $arr_task_type,
            'login_info'     => $loginInfo
        );
        return view('admin-panel.task-manager.index')->with($data);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 08 Jan 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo          = get_loggedin_user_data();
        $arr_task_priority  = \Config::get('custom.task_priority');
        $arr_task_status    = \Config::get('custom.task_status');
        $task               = Task::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('s_task_name') !=  NULL)
            {
                $query->where('task_name', 'Like', $request->get('s_task_name').'%');
            }
            if (!empty($request) && $request->get('task_type') !=  NULL)
            {
                $query->where('task_type', '=', $request->get('task_type'));
            }
            if (!empty($request) && $request->get('s_task_date') !=  NULL)
            {
                $query->where('task_date', '=', $request->get('s_task_date'));
            }
        })->with('getStudent')->with('getSiblingClass')->with('getSections')->with('getStaff')->orderBy('task_id','DESC')->get();        //p($task);

        return Datatables::of($task,$arr_task_priority,$arr_task_status)
        ->addColumn('task_priority', function ($task) use ($arr_task_priority)
            {
                return $arr_task_priority[$task->task_priority];
            })
        ->addColumn('task_type_detail', function ($task) use ($arr_task_priority)
            {   
                if($task->task_type == 1){
                    $task_type_detail = "Type :- Student".'<br>'."Class :-".$task['getSiblingClass']->class_name.' - '.$task['getSections']->section_name.'<br>'."Name :- ".$task['getStudent']->student_name."";
                } else {
                    $task_type_detail = "Type :- Staff".'<br>'."Name :- ".$task['getStaff']->staff_name."";
                }
                return $task_type_detail;
            }) 
        ->addColumn('task_status', function ($task) use ($arr_task_status)
            {
                return $arr_task_status[$task->task_status];
            }) 
        ->addColumn('action', function($task) use($request) {
            $encrypted_task_id  = get_encrypted_value($task->task_id, true);
            
            return '<div class="text-center">
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-top:10px;">
                        <i class="zmdi zmdi-label"></i>
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                            <li>
                                <a title="Open" href="'.url('admin-panel/task-manager/task-status/0/'.$encrypted_task_id.'').'">Open</a>
                            </li>
                            <li>
                            <a title="On Hold" href="'.url('admin-panel/task-manager/task-status/1/'.$encrypted_task_id.'').'">On Hold</a>
                            </li>
                            <li>
                                <a title="Resolved" href="'.url('admin-panel/task-manager/task-status/2/'.$encrypted_task_id.'').'">Resolved</a>
                            </li>
                            <li>
                                <a title="View Response" href="'.url('admin-panel/task-manager/view-response/'.$encrypted_task_id.'').'">View Response</a>
                            </li>
                        </ul>
                    </div>
                    <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/task-manager/add-task/'.$encrypted_task_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
                    <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/task-manager/delete-task/' . $encrypted_task_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
                ';

        })->rawColumns(['action' => 'action','task_type_detail' => 'task_type_detail' ])->addIndexColumn()
        ->make(true); 
        return redirect('/task-manager/view-task');
    }

    /** 
     *  Change Status of Tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function changeStatus($status,$id) 
    {
        $task_id        = get_decrypted_value($id, true);
        $task           = Task::find($task_id);
        if ($task)
        {
            $task->task_status  = $status;
            $task->save();
            $success_msg = "Task status update successfully!";
            return redirect('admin-panel/task-manager/view-task')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Task not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Destroy Data of Tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function destroy($id) {
        $task_id        = get_decrypted_value($id, true);
        $task           = Task::find($task_id);
        if ($task)
        {
            DB::beginTransaction();
            try
            {
                $task->delete();
                $success_msg = "Task deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Task not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  View Response of Tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function viewResponses() 
    {
    	$loginInfo   = get_loggedin_user_data();
        $page_title  = trans('language.view_responses');
        $data               =  array(
            'page_title'    => $page_title,
            'login_info'    => $loginInfo
        );
        return view('admin-panel.task-manager.view-responses')->with($data);
    }
    /**
     *  Mapping With Users to tasks
     *  @Khushbu on 08 Jan 2019
    **/
    public function mapping() 
    {
        $loginInfo   = get_loggedin_user_data();
        $page_title  = trans('language.mapping');
        $data               = array(
            'page_title'    => $page_title,    
            'login_info'    => $loginInfo
        );
        return view('admin-panel.task-manager.mapping')->with($data);
    }

    /**
     *  Get Student Data
     *  @Sandeep on 20 Feb 2019
    **/
    public function get_student_data()
    {
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $student = get_student_list_class_section($class_id,$section_id);
        $data = view('admin-panel.task-manager.ajax-student',compact('student'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get Task Response
     *  @Sandeep on 20 Feb 2019
    **/

    public function  view_task_response($id = NULL){
        
        $loginInfo                 = get_loggedin_user_data();
        $decrypted_task_manager_id = get_decrypted_value($id, true);
        $TaskMnagerData            = [];

        $task_record     = Task::where('task_id',$decrypted_task_manager_id)->with('getStudent')->with('getStaff')->with('TaskResponse')->first();                  

        foreach($task_record['TaskResponse'] as $tm){

            $profile_img = "public/images/default.png"; 
            if($tm->sender_type == 0){  

                $name = $loginInfo['admin_name'];
                if (!empty($loginInfo['admin_profile_img'])){
                    $profile = check_file_exist($loginInfo['admin_profile_img'], 'admin_profile_img');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }

            } else if($tm->sender_type == 1){
                $name = $task_record['getStudent']->student_name;

                if (!empty($task_record['getStudent']->student_image)){
                    $profile = check_file_exist($task_record['getStudent']->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }

            } else {
                $name = $task_record['getStaff']->staff_name;

                if (!empty($task_record['getStudent']->staff_profile_img)){
                    $profile = check_file_exist($task_record['getStudent']->staff_profile_img, 'staff_profile');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }
            } 

            $config_upload_path            = \Config::get('custom.task_file');
            if($tm['response_file'] != ""){
                $destinationPath               = $config_upload_path['display_path'];
            } else {
                $destinationPath   = "";
            }


            $TaskMnagerData[] = array(
                'task_response_id' => $tm['task_response_id'],
                'task_id'          => $tm['task_id'],
                'sender_type'      => $tm['sender_type'],
                'response_text'    => $tm['response_text'],
                'response_file'    => $destinationPath.$tm['response_file'],
                'time'             => time_elapsed_string($tm['created_at']),
                'task_name'        => $task_record['task_name'],
                'profile_img'      => $profile_img,
                'name'             => $name
            );
            
        }

        $data = array(
            'page_title'        => trans('language.task_response'),
            'login_info'        => $loginInfo,
            'task_id'           => $decrypted_task_manager_id,
            // 'staff_student_id'  => $loginInfo['admin_id'],
            'task_record'       => $task_record,
            'taskmanager_data'  => $TaskMnagerData
        );
        return view('admin-panel.task-manager.task_response')->with($data);
    }



    public function  send_task_response(Request $request){

        $loginInfo       = get_loggedin_user_data();
        $task_id         = Input::get('task_id');
        $message_text    = Input::get('message');
        $admindetail     = Task::Find($loginInfo['admin_id']);
        $task_record     = Task::where('task_id',$task_id)->with('getStudent')->with('getStaff')->first();                
        
        $message_date = '';

        if($loginInfo['admin_type'] == 1){
            $sender_type = 0;
        } else if($loginInfo['admin_type'] == 2){
            $sender_type = 2;
        } else {
            $sender_type = 1;
        }

            $taskresponse = New TaskResponse();
            DB::beginTransaction();
            try
            {
                $taskresponse->task_id          = $task_id;
                $taskresponse->sender_type      = $sender_type;
                $taskresponse->admin_id         = $task_record->admin_id;
                $taskresponse->student_id       = $task_record->student_id;
                $taskresponse->staff_id         = $task_record->staff_id;
                $taskresponse->response_text    = $message_text;

                if ($request->hasFile('task_file'))
                {
                    $file                          = $request->file('task_file');
                    $config_upload_path            = \Config::get('custom.task_file');
                    $destinationPath               = public_path() . $config_upload_path['upload_path'];
                    $ext                           = $file->getClientOriginalExtension();
                    $name                          = str_replace(' ', '', substr($file->getClientOriginalName(),0,-4));
                    $filename                      = $name.mt_rand(0,100000).time().'.'.$ext;
                    $file->move($destinationPath, $filename);

                    $response_file = $config_upload_path['display_path'].$filename;  
                    $taskresponse->response_file   = $filename;
                }

                $taskresponse->save();

                if($sender_type == 0){
                    $admin_info = Admin::where('admin_id',$task_record->admin_id)->first();
                    $message = $admin_info->admin_name.' : '.$message;
                } else if($sender_type == 1){
                    $message = $task_record['getStudent']->student_name.' : '.$message;
                } else if($sender_type == 2){
                    $message = $task_record['getStaff']->staff_name.' : '.$message;
                }


                $title = $task_record->task_name;
                    
                //$title = "New Message Received";
                if($sender_type == 1 || $sender_type == 2){
                    $notification = $this->send_push_notification_admin($task_id,$task_record->admin_id,'task_response_add',$task_record->admin_id,$message_text,$title);
                }

                if($sender_type == 0 && $task_record->student_id != ""){
                    $notification = $this->send_push_notification_student($task_id,$task_record->student_id,'task_response_add',$task_record->admin_id,$message_text,$title);
                }
                if($sender_type == 0 && $task_record->staff_id != ""){
                    $notification = $this->send_push_notification_staff($task_id,$task_record->staff_id,'task_response_add',$task_record->admin_id,$message_text,$title);
                }


                $message_date = $taskresponse->created_at;
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return response()->json(['status'=>false, 'error_message'=> $error_message]);
            }
            DB::commit();

            if($response_file != "")  {
                $show_response = '<br/> <br/><a href="'.url($response_file).'"  target="_blank" > View File </a>';
            }

            if(!empty($loginInfo['admin_profile_img'])){
                $profile_img = '<img src="'.url($loginInfo['admin_profile_img']).'" class="float-right rounded-circle" title="preview" alt="preview">';
            } else {
                $default_img = "public/images/default.png"; 
                $profile_img = '<img src="'.url($default_img).'" class="float-right rounded-circle" title="preview" alt="preview">';
            }
            $user_message = '<li class="clearfix message-animation nexclass"> <div class="message my-message float-right"> Task Name: '.$task_record->task_name.' <br /><br /> '.$message_text.' '.$show_response.' <span class="chat-staff-name">'.$loginInfo['admin_name'].'</span> </div><div class="clearfix"></div> '.$profile_img.' <div class="message-data float-right"> <span class="message-data-time"> '.time_elapsed_string($message_date).' </span> &nbsp; </div><div class="clearfix"></div></li>';
            return response()->json(['status'=>true, 'user_message'=> $user_message ]);
    }



    /**
     *  View page of My Tasks
     *  @Sandeep on 21 feb 2019
    **/
    public function my_task() 
    {
        $loginInfo          = get_loggedin_user_data();
        $parent_info        = parent_info($loginInfo['admin_id']);
        $parent_student     = get_parent_student($parent_info['student_parent_id']);
        $arr_task_type      = \Config::get('custom.task_type');
        $page_title         = trans('language.view_task');
        
        $data               =  array(
            'page_title'       => $page_title,
            'arr_task_type'    => $arr_task_type,
            'login_info'       => $loginInfo,
            'parent_student'   => $parent_student,
        );
        return view('admin-panel.task-manager.my_task')->with($data);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Sandeep on 21 feb 2019
    **/
    public function my_taskData(Request $request)
    {
        $loginInfo          = get_loggedin_user_data();

        if($loginInfo['admin_type'] == 2){
            $request['task_type'] = 2;
            $request['staff_id'] = $loginInfo['staff_id'];
        }

        if($loginInfo['admin_type'] == 4 || $loginInfo['admin_type'] == 3){
            $request['task_type'] = 1;
            $request['student_id'] = $loginInfo['student_id'];
        }

        $arr_task_priority  = \Config::get('custom.task_priority');
        $arr_task_status    = \Config::get('custom.task_status');
        $task               = Task::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('s_task_name') !=  NULL)
            {
                $query->where('task_name', 'Like', $request->get('s_task_name').'%');
            }
            if (!empty($request) && $request->get('task_type') !=  NULL)
            {
                $query->where('task_type', '=', $request->get('task_type'));
            }
            if (!empty($request) && $request->get('staff_id') !=  NULL)
            {
                $query->where('staff_id', '=', $request->get('staff_id'));
            }
            if (!empty($request) && $request->get('student_id') !=  NULL)
            {
                $query->where('student_id', '=', $request->get('student_id'));
            }
            if (!empty($request) && $request->get('s_task_date') !=  NULL)
            {
                $query->where('task_date', '=', $request->get('s_task_date'));
            }
        })->with('getStudent')->with('getSiblingClass')->with('getSections')->with('getStaff')->orderBy('task_id','DESC')->get();        //p($task);

        return Datatables::of($task,$arr_task_priority,$arr_task_status)
        ->addColumn('task_priority', function ($task) use ($arr_task_priority)
            {
                return $arr_task_priority[$task->task_priority];
            })
        ->addColumn('task_status', function ($task) use ($arr_task_status)
            {
                return $arr_task_status[$task->task_status];
            }) 
        ->addColumn('action', function($task) use($request) {
            $encrypted_task_id  = get_encrypted_value($task->task_id, true);
            
            return '<div class="text-center">
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-top:10px; padding-left:20px;">
                        <i class="zmdi zmdi-label"></i>
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                            <li>
                                <a title="View Response" href="'.url('admin-panel/view-response/'.$encrypted_task_id.'').'">View Response</a>
                            </li>
                        </ul>
                    </div>
                ';

        })->rawColumns(['action' => 'action','task_type_detail' => 'task_type_detail' ])->addIndexColumn()
        ->make(true); 
        return redirect('/task-manager/view-task');
    }


    /**
     *  Get My Task Response
     *  @Sandeep on 21 Feb 2019
    **/

    public function  my_view_task_response($id = NULL){
        
        $loginInfo                 = get_loggedin_user_data();
        $decrypted_task_manager_id = get_decrypted_value($id, true);
        $TaskMnagerData            = [];
        $parent_info               = parent_info($loginInfo['admin_id']);
        $parent_student            = get_parent_student($parent_info['student_parent_id']);
        $task_record               = Task::where('task_id',$decrypted_task_manager_id)->with('getStudent')->with('getStaff')->with('TaskResponse')->with('getAdmin')->first();                  

        foreach($task_record['TaskResponse'] as $tm){

            $profile_img = "public/images/default.png"; 
            if($tm->sender_type == 0){  

                $name = $task_record['getAdmin']->admin_name;
                if (!empty($task_record['getAdmin']->admin_profile_img)){
                    $profile = check_file_exist($task_record['getAdmin']->admin_profile_img, 'admin_profile_img');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }

            } else if($tm->sender_type == 1){
                $name = $task_record['getStudent']->student_name;

                if (!empty($task_record['getStudent']->student_image)){
                    $profile = check_file_exist($task_record['getStudent']->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }

            } else {
                $name = $task_record['getStaff']->staff_name;

                if (!empty($task_record['getStudent']->staff_profile_img)){
                    $profile = check_file_exist($task_record['getStudent']->staff_profile_img, 'staff_profile');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }
            } 

            $config_upload_path            = \Config::get('custom.task_file');
            if($tm['response_file'] != ""){
                $destinationPath               = $config_upload_path['display_path'];
            } else {
                $destinationPath   = "";
            }


            $TaskMnagerData[] = array(
                'task_response_id' => $tm['task_response_id'],
                'task_id'          => $tm['task_id'],
                'sender_type'      => $tm['sender_type'],
                'response_text'    => $tm['response_text'],
                'response_file'    => $destinationPath.$tm['response_file'],
                'time'             => time_elapsed_string($tm['created_at']),
                'task_name'        => $task_record['task_name'],
                'profile_img'      => $profile_img,
                'name'             => $name
            );
            
        }

//        p($TaskMnagerData);

        $data = array(
            'page_title'        => trans('language.task_response'),
            'login_info'        => $loginInfo,
            'task_id'           => $decrypted_task_manager_id,
            // 'staff_student_id'  => $loginInfo['admin_id'],
            'parent_student'    => $parent_student,
            'task_record'       => $task_record,
            'taskmanager_data'  => $TaskMnagerData
        );
        return view('admin-panel.task-manager.my_task_response')->with($data);
    }



    //Notification function
    public function send_push_notification_staff($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($user_id) 
        {
            $query->whereIn('staff_id',explode(",",$user_id));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $user_id,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }

    //Notification function
    public function send_push_notification_student($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $student_info        = Student::where(function($query) use ($user_id) 
        {
            $query->where('student_status',1);
            $query->where('student_id',$user_id);
        })
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();

        foreach($student_info as $student){
            if($student['get_admin_info']['fcm_device_id'] != "" && $student['get_admin_info']['notification_status'] == 1){
                $device_ids[] = $student['get_admin_info']['fcm_device_id'];
            }
            if($student['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student['get_parent']['get_parent_admin_info']['notification_status'] == 1){
                $device_ids[] = $student['get_parent']['get_parent_admin_info']['fcm_device_id'];
                $parent_admin_id = $student['get_parent']['get_parent_admin_info']['admin_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => $user_id,
                'parent_admin_id' => $parent_admin_id,
                'staff_admin_id' => Null,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


    //Notification function
    public function send_push_notification_admin($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $admin_info    = Admin::where(function($query) use ($user_id) 
        {
            $query->whereIn('admin_id',explode(",",$user_id));
        })
        ->get()->toArray();

        foreach($admin_info as $admin){
            if($admin['fcm_device_id'] != "" && $admin['notification_status'] == 1){
                $device_ids[] = $admin['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => Null,
                'school_admin_id' => $admin_id,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }






}



