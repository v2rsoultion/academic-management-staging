<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Classes\Classes;
use App\Model\Student\StudentAcademic;

use Yajra\Datatables\Datatables;

class ClassesController extends Controller
{
    /**
     *  View page for class
     *  @Shree on 18 July 2018
    **/
    public function index()
    {
        $class = [];
        $loginInfo              = get_loggedin_user_data();
        $arr_medium             = get_all_mediums();
        $class['arr_medium']    = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.view_class'),
            'redirect_url'  => url('admin-panel/class/view-classes'),
            'login_info'    => $loginInfo,
            'class'         => $class
        );
        return view('admin-panel.class.index')->with($data);
    }

    /**
     *  Add page for class
     *  @Shree on 18 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data   = [];
        $class  = [];
        $loginInfo = get_loggedin_user_data();
        $arr_medium         = get_all_mediums();
        $arr_stream_status  = \Config::get('custom.stream_status');
        $arr_board          = get_all_school_boards();
        if (!empty($id))
        {
            $decrypted_class_id = get_decrypted_value($id, true);
            $class              = Classes::Find($decrypted_class_id);
            if (!$class)
            {
                return redirect('admin-panel/class/add-class')->withError('Class not found!');
            }
            $page_title             = trans('language.edit_class');
            $decrypted_class_id     = get_encrypted_value($class->class_id, true);
            $save_url               = url('admin-panel/class/save/' . $decrypted_class_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_class');
            $save_url      = url('admin-panel/class/save');
            $submit_button = 'Save';
        }
        $class['arr_medium']   = add_blank_option($arr_medium, 'Select Medium');
        $class['arr_board']   = add_blank_option($arr_board, 'Select Board');
        $class['arr_stream_status']   = $arr_stream_status;
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'class'         => $class,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/class/view-classes'),
        );
        return view('admin-panel.class.add')->with($data);
    }

    /**
     *  Add and update class's data
     *  @Shree on 18 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_class_id = get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        $medium_type = $board_id = null;
        if (!empty($id))
        {
            $class = Classes::find($decrypted_class_id);
            $admin_id       = $class['admin_id'];
            if (!$class)
            {
                return redirect('/admin-panel/class/add-class/')->withError('Class not found!');
            }
            $success_msg = 'Class updated successfully!';
        }
        else
        {
            $class     = New Classes;
            $success_msg = 'Class saved successfully!';
        }
        if ($request->has('medium_type'))
        {
            $medium_type = Input::get('medium_type');
        }
        if ($request->has('board_id'))
        {
            $board_id = Input::get('board_id');
        }
        $validatior = Validator::make($request->all(), [
                'class_name'        => 'required|unique:classes,class_name,' . $decrypted_class_id . ',class_id,medium_type,' . $medium_type.',board_id,' . $board_id,
                'medium_type'      => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $class->admin_id            = $admin_id;
                $class->update_by           = $loginInfo['admin_id'];
                $class->class_name          = Input::get('class_name');
                $class->medium_type         = Input::get('medium_type');
                $class->board_id            = Input::get('board_id');
                $class->class_stream        = Input::get('class_stream');
                $class->class_order         = Input::get('class_order');
                $class->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/class/view-classes')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 18 July 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $arr_medium     = get_all_mediums();
        $arr_boards     = get_all_school_boards();
        $session = get_current_session();
        $class = Classes::where(function($query) use ($request,$session) 
        {
            if (!empty($request) && !empty($request->get('name')) && $request->get('name') != null)
            {
                $query->where('class_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->orderBy('class_order', 'ASC')->get();
        
        return Datatables::of($class,$arr_medium,$arr_boards,$session)
        ->addColumn('class_name', function ($class) use ($arr_medium,$arr_boards)
        {
            $class_name = $class->class_name." - ".$arr_medium[$class->medium_type]." - ".$arr_boards[$class->board_id];
            return $class_name;
        })
        ->addColumn('class_stream', function ($class)
        {
            $arr_stream_status   = \Config::get('custom.stream_status');
            $class_stream  = $arr_stream_status[$class->class_stream];
            return $class_stream;
        })
        ->addColumn('board_id', function ($class)
        {
            $arr_board   = get_all_school_boards();
            $board_id  = $arr_board[$class->board_id];
            return $board_id;
        })
        ->addColumn('class_students', function($class) use ($session) {
            $total_students = 0;
            $total_students = StudentAcademic::where(function($query) use ($class,$session) 
            {
                $query->where('current_class_id', "=", $class->class_id);
                $query->where('current_session_id', "=", $session['session_id']);
            })->orderBy('student_id', 'ASC')->get()->count();
            return $total_students;
        })
        ->addColumn('action', function ($class)
        {
            $encrypted_class_id = get_encrypted_value($class->class_id, true);
            if($class->class_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="class-status/'.$status.'/' . $encrypted_class_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-class/' . $encrypted_class_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-class/' . $encrypted_class_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            
        })->rawColumns(['action' => 'action','class_name'=>'class_name'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy class's data
     *  @Shree on 18 July 2018
    **/
    public function destroy($id)
    {
        $class_id = get_decrypted_value($id, true);
        $class    = Classes::find($class_id);
       
        $success_msg = $error_message =  "";
        if ($class)
        {
            DB::beginTransaction();
            try
            {
                $class->delete();
                $success_msg = "Class deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/class/view-classes')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/class/view-classes')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change class's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $class_id = get_decrypted_value($id, true);
        $class    = Classes::find($class_id);
        if ($class)
        {
            $class->class_status  = $status;
            $class->save();
            $success_msg = "Class status update successfully!";
            return redirect('admin-panel/class/view-classes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get class data according medium
     *  @Shree on 5 Sept 2018
    **/
    public function getClassData()
    {
        $medium_type = Input::get('medium_type');
        $classes = get_all_classes($medium_type);
        $data = view('admin-panel.class.ajax-class-select',compact('classes'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get total intake data according class
     *  @Shree on 12 Sept 2018
    **/
    public function getIntakeData()
    {
        $totalIntake = 0;
        $class_id = Input::get('class_id');
        $totalIntake = get_total_intake($class_id);
        return $totalIntake;
    }


     /**
     *  Get total intake data according class
     *  @Shree on 29 Sept 2018
    **/
    public function getstreamStatus()
    {
        $class_id = Input::get('class_id');
        $status = get_class_stream_status($class_id);
        return $status;
    }
}
