<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Permissions\Permissions; // Model
use App\Model\Permissions\MasterModule; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PermissionsController extends Controller
{

    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('21',$permissions )){
            $error_message = "Unauthorized Access";
           //  Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Bank
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $all_modules = get_all_master_modules();
        $permissionId = 1;
        $encrypted_module_permission_id  = get_encrypted_value($permissionId, true);
        $permissions = Permissions::find($permissionId);
        // p($all_modules);
        $data = array(
            'page_title'    => trans('language.permissions'),
            'redirect_url'  => url('admin-panel/permissions'),
            'save_url'      => url('admin-panel/save-permissions/'.$encrypted_module_permission_id),
            'login_info'    => $loginInfo,
            'all_permissions'   => $permissions,
            'all_modules'   => $all_modules
        );
        return view('admin-panel.permissions.index')->with($data);
    }

    /**
     *  Add and update bank's data
     *  @Shree on 2 Oct 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        // p($request->all());
        $loginInfo      	= get_loggedin_user_data();
        $decrypted_module_permission_id  = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $permissions       = Permissions::find($decrypted_module_permission_id);
            $success_msg = 'Permissions updated successfully!';
        }
        DB::beginTransaction();
        try
        {
            $permissionData = '';
            if($request->get('permissions') != ''){
                $permissionData = implode(',',$request->get('permissions'));
            }
            $permissions->permissions    = $permissionData;
            $permissions->save();
        }
        catch (\Exception $e)
        {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }

        DB::commit();
        return redirect('admin-panel/permissions')->withSuccess($success_msg);
    }

   
}
