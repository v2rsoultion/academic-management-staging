<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Facility\Facility; // Model
use Yajra\Datatables\Datatables;

class FacilityController extends Controller
{
    /**
     *  View page for facility
     *  @Pratyush on 19 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_facility'),
            'redirect_url'  => url('admin-panel/facilities/view-facilities'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.facility.index')->with($data);
    }


    /**
     *  Add page for facility
     *  @Pratyuhs on 19 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data   	= [];
        $facility   = [];
        $loginInfo 	= get_loggedin_user_data();
        if (!empty($id)) {
            $decrypted_facility_id = get_decrypted_value($id, true);
            $facility              = Facility::Find($decrypted_facility_id);
            if (!$facility)
            {
                return redirect('admin-panel/facilities/add-facility')->withError('Facility not found!');
            }
            $page_title             = trans('language.edit_facility');
            $decrypted_facility_id  = get_encrypted_value($facility->facility_id, true);
            $save_url               = url('admin-panel/facilities/save/' . $decrypted_facility_id);
            $submit_button          = 'Update';
        } else {
            $page_title    = trans('language.add_facility');
            $save_url      = url('admin-panel/facilities/save');
            $submit_button = 'Save';
        }
        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'facility'      => $facility,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/facilities/view-facilities'),
        );
        return view('admin-panel.facility.add')->with($data);
    }

    /**
     *  Add and update facility's data
     *  @Pratyush on 19 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
    	
        $loginInfo      		= get_loggedin_user_data();
        $decrypted_facility_id 	= get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        if (!empty($id)) {
            $facility = Facility::find($decrypted_facility_id);
            $admin_id = $facility['admin_id'];
            if (!$facility)
            {
                return redirect('/admin-panel/facilities/add-facility/')->withError('Facility not found!');
            }
            $success_msg = 'Facility updated successfully!';
        } else {
            $facility     	= New Facility;
            $success_msg 	= 'Facility saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'facility_name'   => 'required|unique:facilities,facility_name,' . $decrypted_facility_id . ',facility_id',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        }  else {
            
            DB::beginTransaction();
            try
            {
                $facility->admin_id                 = $admin_id;;
                $facility->update_by                = $loginInfo['admin_id'];
                $facility->facility_name            = Input::get('facility_name');
                $facility->facility_fees_applied    = Input::get('is_fees_applied');
                $facility->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/facilities/view-facilities')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @by Pratyush on 19 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $temp_facility  = Facility::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('facility_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('facility_id', 'DESC')->get();
		$facility_arr 	= array();
		$facility 		= array();
        foreach ($temp_facility as $temp_key1 => $temp_value){
        	if($temp_value['facility_fees_applied'] == 1){
        		$is_fees_applied = 'Yes';
        	}else{
        		$is_fees_applied = 'No';
        	}
			$facility_arr[$temp_key1]['facility_id']			= $temp_value['facility_id'];
			$facility_arr[$temp_key1]['admin_id']				= $temp_value['admin_id'];
        	$facility_arr[$temp_key1]['update_by']				= $temp_value['update_by'];
        	$facility_arr[$temp_key1]['facility_name']			= $temp_value['facility_name'];
        	$facility_arr[$temp_key1]['facility_fees_applied']	= $is_fees_applied;
        	$facility_arr[$temp_key1]['facility_status']		= $temp_value['facility_status'];
        }
        foreach($facility_arr as $temp_key2 => $temp_value2){
        	$facility[$temp_key2] = (object)$temp_value2;
        }
        return Datatables::of($facility)
        ->addColumn('action', function ($facility)
        {
            $encrypted_facility_id = get_encrypted_value($facility->facility_id, true);
            if($facility->facility_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '<div class="pull-left"><a href="facility-status/'.$status.'/' . $encrypted_facility_id . '">'.$statusVal.'</a></div>
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-facility/' . $encrypted_facility_id . '"><i class="zmdi zmdi-edit"></i></a></div>
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-facility/' . $encrypted_facility_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy facility's data
     *  @Pratyush on 19 July 2018.
    **/
    public function destroy($id)
    {
        $facility_id = get_decrypted_value($id, true);
        $facility    = Facility::find($facility_id);
        
        $success_msg = $error_message =  "";
        if ($facility)
        {
            DB::beginTransaction();
            try
            {
                $facility->delete();
                $success_msg = "Facility deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/facilities/view-facilities')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/facilities/view-facilities')->withErrors($error_message);
            }
        } else {
            $error_message = "Facility not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change facility's status
     *  @Pratyush on 19 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $facility_id = get_decrypted_value($id, true);
        $facility    = Facility::find($facility_id);
        if ($facility)  {
            $facility->facility_status  = $status;
            $facility->save();
            $success_msg = "Facility status updated!";
            return redirect('admin-panel/facilities/view-facilities')->withSuccess($success_msg);
        } else {
            $error_message = "Facility not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
