<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;
use App\Model\FeesCollection\OneTime; // Model
use App\Model\FeesCollection\RecurringHead; // Model
use App\Model\FeesCollection\FeesStMap; // Model
use App\Model\Classes\Classes;
use App\Model\Student\Student;

class MapHeadsController extends Controller
{
    /**
     *  View page for heads listing
     *  @shree on 13 Oct 2018
    **/
    public function index()
    {
        $loginInfo  = get_loggedin_user_data();
        $heads = [];
        $arr_class  = get_all_classes_mediums();
        $heads['arr_class']    = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.map_heads'),
            'redirect_url'  => url('admin-panel/map-head/heads'),
            'login_info'    => $loginInfo,
            'heads'         => $heads,
        );
        return view('admin-panel.map-heads.index')->with($data);
    }

    /**
     *  Get head list 
     *  @Shree on 13 Oct 2018
    **/
    public function get_heads_list(Request $request)
    {
        $arr_heads   = [];
        // For one time head
        $arr_onetime_head_list        = OneTime::where(function($query) use ($request) 
        {
            // For Class ID
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->whereRaw('FIND_IN_SET('.$request->get('class_id').',ot_classes)');
            }
            // For Name
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('ot_particular_name', "like", "%{$request->get('name')}%");
            }
            
        })
        ->where(array('ot_status' => 1))
        ->orderBy('ot_head_id', 'ASC')->get();
        
        if (!empty($arr_onetime_head_list))
        {
            foreach ($arr_onetime_head_list as $arr_head_value)
            {
                $headData = [];
                $headData['head_id'] = $arr_head_value->ot_head_id;
                $headData['head_name'] = $arr_head_value->ot_particular_name;
                $headData['head_type'] = "ONETIME";
                $headData['head_date'] = $arr_head_value->ot_date;
                $arr_heads[] = $headData;
            }
        }
        // For recurring fees head
        $arr_recurring_head_list        = RecurringHead::where(function($query) use ($request) 
        {
            // For Class ID
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->whereRaw('FIND_IN_SET('.$request->get('class_id').',rc_classes)');
            }
            // For Name
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('rc_particular_name', "like", "%{$request->get('name')}%");
            }
            
        })
        ->where(array('rc_status' => 1))
        ->orderBy('rc_head_id', 'ASC')->get();
        
        if (!empty($arr_recurring_head_list))
        {
            foreach ($arr_recurring_head_list as $arr_head_value)
            {
                $headData = [];
                $headData['head_id'] = $arr_head_value->rc_head_id;
                $headData['head_name'] = $arr_head_value->rc_particular_name;
                $headData['head_type'] = "RECURRING";
                $headData['head_date'] = null;
                $arr_heads[] = $headData;
            }
        }
        return Datatables::of($arr_heads)
            ->addColumn('classes', function ($arr_heads)
            {
                return '<button type="button" class="btn btn-raised btn-primary classes" head-type="'.$arr_heads['head_type'].'"  head-id="'.$arr_heads['head_id'].'" >
                View Classes
                </button>';
            })
            ->addColumn('action', function ($arr_heads)
            {
                $encrypted_head_id = get_encrypted_value($arr_heads['head_id'], true);
                $encrypted_head_type = get_encrypted_value($arr_heads['head_type'], true);
                return '
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a href="../../admin-panel/map-heads/map-student/' . $encrypted_head_id . '/'.$encrypted_head_type.'" "">Map Student</a>
                    </li>
                 </ul>
             </div>
              ';
            })
            ->rawColumns(['action' => 'action','classes'=>'classes'])->addIndexColumn()->make(true);
    }

    /**
     *  Get classes Data for view page(Datatables)
     *  @shree on 12 Oct 2018.
    **/
    public function classes(Request $request)
    {
        
        if($request->get('type') == "ONETIME") {
            $onetime  	= OneTime::where(function($query) use ($request) 
            {
                if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
                {
                    $query->where('ot_head_id', "=", $request->get('id'));
                }
            })->orderBy('ot_head_id', 'DESC')->get()->toArray();
            
            $class_arr = explode(",",$onetime[0]['ot_classes']);
        } else if($request->get('type') == "RECURRING") {
            $recurring  	= RecurringHead::where(function($query) use ($request) 
            {
                if (!empty($request) && !empty($request->has('id')) && $request->get('id') != null)
                {
                    $query->where('rc_head_id', "=", $request->get('id'));
                }
            })->orderBy('rc_head_id', 'DESC')->get()->toArray();
            
            $class_arr = explode(",",$recurring[0]['rc_classes']);
        }
        $allNames = "";
        $name = [];
        foreach($class_arr as $key => $id){
            $encrypted_id = get_encrypted_value($id,true);
            $name[]['class_name'] = get_class_name_by_id($encrypted_id);
        }
        return Datatables::of($name)
        ->addColumn('class_name', function ($name)
        {
            return $name['class_name'];
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     *  Get map student
     *  @Shree on 13 Oct 2018
    **/
    public function map_student($head_id,$type)
    {
        $heads = $arr_section = $arr_class = [];
        $loginInfo  = get_loggedin_user_data();
        $head_id    = get_decrypted_value($head_id, true);
        $type       = get_decrypted_value($type, true);
        $arr_class  = get_head_classes($head_id,$type);
        $heads['head_name']     = get_head_name($type,$head_id);
        $heads['head_id']       = $head_id;
        $heads['head_type']     = $type;
        $heads['arr_class']     = add_blank_option($arr_class, "Select Class");
        $heads['arr_section']     = add_blank_option($arr_section, "Select Section");
        $data = array(
            'page_title'    => trans('language.map_student'),
            'redirect_url'  => url('admin-panel/map-head/map-student'),
            'login_info'    => $loginInfo,
            'heads'         => $heads,
        );
        return view('admin-panel.map-heads.map_student')->with($data);
    }

    /**
     *  Get student list for map fees head
     *  @Shree on 9 Oct 2018
    **/
    public function get_student_list(Request $request)
    {
        $session       = get_current_session();
        $student     = [];
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
            $request->request->add(['student_type', 'rte_apply_status']);
            $request->merge(['student_type' => '0', 'rte_apply_status'=>'0']);
           
            $arr_students = get_student_list($request);
        } else {
            $arr_students = [];
        }
        $mapInfo =  FeesStMap::where(function($query) use ($request,$session)
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('head_id')))
            {
                $query->where('head_id', "=", $request->get('head_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('head_type')))
            {
                $query->where('head_type', "=", $request->get('head_type'));
            }
        })->get()->toArray();
        $mapInfo = isset($mapInfo[0]) ? $mapInfo[0] : [];
        $fees_st_map_id = $studentIds = null;
        if(isset($mapInfo['fees_st_map_id'])){
            $fees_st_map_id = $mapInfo['fees_st_map_id'];
            if($mapInfo['student_ids'] != ""){
                $studentIds = $mapInfo['student_ids'];
            } else {
                $studentIds = null;
            }
        }
        foreach ($arr_students as $key => $arr_student)
        {
            $arr_student['student_map'] = 'no';
            $arr_student['fees_st_map_id'] = $fees_st_map_id;
            if($studentIds != "" && in_array($arr_student['student_id'],explode(",",$studentIds))){
                $arr_student['student_map'] = 'yes';
            }
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student)
            ->addColumn('student_profile', function ($student)
            {
                $profile = "";
                if($student->profile != ''){
                    $profile = "<img src=".$student->profile." height='30' />";
                }   
                $name = $student->student_name;
                $complete = $profile."  ".$name;
                return $complete;
            })
            ->addColumn('Select', function ($student)
            {
                $check = "";
                if($student->student_map == 'yes'){
                    $check = "checked";
                }
                $checkBox ='
                <div class="checkbox">
                <input id="checkbox'.$student->student_id.'" type="checkbox" name="students[]" value="'.$student->student_id.'" '.$check.'>
                <label class="from_one1" for="checkbox'.$student->student_id.'" style="margin-bottom: 4px !important;"></label>
                </div>';
              
                return $checkBox;
            })->rawColumns(['action' => 'action','student_profile'=>'student_profile', 'Select'=> 'Select'])->addIndexColumn()->make(true);
    }

    public function get_student_counts(Request $request){
        $session       = get_current_session();
        $total_students =  Student::where(function($query) use ($request)
        {
            $query->where('student_type', "=", '0');
            $query->where('rte_apply_status', "=", '0');
            $query->where('student_status',1);
        })->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            $join->where('current_session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $join->where('current_class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $join->where('current_section_id', "=", $request->get('section_id'));
            }

        })->get()->count();

        $mapInfo =  FeesStMap::where(function($query) use ($request,$session)
        {
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->get('head_id')))
            {
                $query->where('head_id', "=", $request->get('head_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('head_type')))
            {
                $query->where('head_type', "=", $request->get('head_type'));
            }
        })->get()->toArray();
        $mapInfo = isset($mapInfo[0]) ? $mapInfo[0] : [];
        $fees_st_map_id = null;
        $map_students = 0;
        if(isset($mapInfo['fees_st_map_id'])){
            $fees_st_map_id = $mapInfo['fees_st_map_id'];
            if($mapInfo['student_ids'] != ""){
                $students = explode(',',$mapInfo['student_ids']);
                $map_students = COUNT($students);
            } else {
                $map_students = 0;
            }
        }
        $remaining_students = 0;
        $remaining_students = $total_students - $map_students;
        $finalArray = array(
            'total_students' => $total_students,
            'remaining_students' => $remaining_students,
            'map_students'  => $map_students, 
            'fees_st_map_id'    => $fees_st_map_id
        );

        return json_encode($finalArray);
    }

    public function save_map_student(Request $request){
        $loginInfo = get_loggedin_user_data();
        $session   = get_current_session();
        $head_id = $request->get('head_id');
        $head_type = $request->get('head_type');
        $class_id = $request->get('st_class_id');
        $section_id = $request->get('st_section_id');
        $students = implode(",",$request->get('students'));
        $fees_st_map_id = $request->get('fees_st_map_id');
        if($fees_st_map_id != ""){
            $mapInfo   = FeesStMap::find($fees_st_map_id);
            $admin_id = $mapInfo['admin_id'];
        } else {
            $mapInfo   = new FeesStMap();
            $admin_id = $loginInfo['admin_id'];
        }
        $mapInfo->admin_id      = $admin_id;
        $mapInfo->update_by      = $loginInfo['admin_id'];
        $mapInfo->session_id     = $session['session_id'];
        $mapInfo->head_id       = $head_id;
        $mapInfo->head_type       = $head_type;
        $mapInfo->class_id       = $class_id;
        $mapInfo->section_id     = $section_id;
        $mapInfo->student_ids     = $students;
        $mapInfo->save();
        
        return "success";
    }
}
