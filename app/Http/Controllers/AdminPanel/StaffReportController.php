<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Staff\Staff;
use App\Model\Student\StudentAttendanceDetails; // Model
use Yajra\Datatables\Datatables;

class StaffReportController extends Controller
{
    /**
     *  View page for Staff Attendance Data
     *  @Khushbu on 06 March 2019
    **/
    public function staffAttendance() {
        $map                = [];
        $loginInfo          = get_loggedin_user_data();
        // $arr_class          = get_all_classes_mediums();
        // $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_staff_report'),
            'redirect_url'  => url('admin-panel/staff/staff-attendance-report'),
            'login_info'    => $loginInfo,
            // 'map'           => $map,
        );
        return view('admin-panel.staff-report.staff_attendance_report')->with($data);
    }

    public function anyDataStaffAttendance(Request $request){
        $session        = get_current_session();
        $staff          = Staff::with('getDesignation','getStaffAttendence')->get()->toArray();
        // p($staff);
        $total_days = 365 - count(get_school_all_holidays(1));
        return Datatables::of($staff) 
        ->addColumn('staff_profile', function ($staff)
        {
            $profile = "";
            if($staff['staff_profile_img'] != ''){
                $profile = check_file_exist($staff['staff_profile_img'], 'staff_profile');
                    if (!empty($profile))
                    {
                        $staff_image = URL($profile);
                    }
                $staff_profile = "<img src=".$staff_image." height='30' />";
            }   
            $name = $staff['staff_name'];
            $complete = $staff_profile."  ".$name;
            return $complete;
        })  
        ->addColumn('designation_name', function($staff) {
            return $staff['get_designation']['designation_name'];
        })     
        ->addColumn('total_working_days', function($staff) use($total_days) {
            return $total_days;
        })
        ->addColumn('total_present', function($staff) {
            $encrypted_staff_id = get_encrypted_value($staff['staff_id'], true);
            $total_present = '<a href="'.url('admin-panel/staff/view-profile/'.$encrypted_staff_id.'').'">'.count($staff['get_staff_attendence']).'</a>';
            return $total_present;
        })
        ->addColumn('total_paid_leaves', function($staff) {
            return '0';
        })
        ->addColumn('total_loss_pay', function($staff) {
            return '0';
        })
       ->rawColumns(['staff_profile' => 'staff_profile', 'designation_name' => 'designation_name', 'total_working_days' => 'total_working_days', 'total_present' => 'total_present', 'total_paid_leaves' => 'total_paid_leaves', 'total_loss_pay' => 'total_loss_pay'])->addIndexColumn()
        ->make(true);
    }
}
