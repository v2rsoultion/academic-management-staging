<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Title\Title; // Model
use Yajra\Datatables\Datatables;

class TitleController extends Controller
{
    /**
     *  View page for Title
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_title'),
            'redirect_url'  => url('admin-panel/title/view-titles'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.title.index')->with($data);
    }

    /**
     *  Add page for Title
     *  @Pratyush on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $title 			= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_title_id 	= get_decrypted_value($id, true);
            $title      			= Title::Find($decrypted_title_id);
            if (!$title)
            {
                return redirect('admin-panel/title/add-title')->withError('Title not found!');
            }
            $page_title             	= trans('language.edit_title');
            $encrypted_title_id   		= get_encrypted_value($title->title_id, true);
            $save_url               	= url('admin-panel/title/save/' . $encrypted_title_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_title');
            $save_url      = url('admin-panel/title/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'title' 			=> $title,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/title/view-titles'),
        );
        return view('admin-panel.title.add')->with($data);
    }

    /**
     *  Add and update Title's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_title_id			= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $title = Title::find($decrypted_title_id);
            $admin_id = $title['admin_id'];
            if (!$title)
            {
                return redirect('/admin-panel/title/add-title/')->withError('Title not found!');
            }
            $success_msg = 'Title updated successfully!';
        }
        else
        {
            $title     		= New Title;
            $success_msg 	= 'Title saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'title_name'   => 'required|unique:titles,title_name,' . $decrypted_title_id . ',title_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $title->admin_id       = $admin_id;
                $title->update_by      = $loginInfo['admin_id'];
                $title->title_name 	   = Input::get('title_name');
                $title->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/title/view-titles')->withSuccess($success_msg);
    }

    /**
     *  Get Title's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $title  			= Title::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('title_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('title_id', 'DESC')->get();

        return Datatables::of($title)
        ->addColumn('action', function ($title)
        {
            $encrypted_title_id = get_encrypted_value($title->title_id, true);
            if($title->title_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="title-status/'.$status.'/' . $encrypted_title_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-title/' . $encrypted_title_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-title/' . $encrypted_title_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Title's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $title_id 		= get_decrypted_value($id, true);
        $title 		  	= Title::find($title_id);
        
        $success_msg = $error_message =  "";
        if ($title)
        {
            DB::beginTransaction();
            try
            {
                $title->delete();
                $success_msg = "Title deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/title/view-titles')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/title/view-titles')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Title not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Title's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $title_id 		= get_decrypted_value($id, true);
        $title 		  	= Title::find($title_id);
        if ($title)
        {
            $title->title_status  = $status;
            $title->save();
            $success_msg = "Title status update successfully!";
            return redirect('admin-panel/title/view-titles')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Title not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
