<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\FeesCollection\RTECheques;
use App\Model\FeesCollection\ImprestEntry;
use App\Model\FeesCollection\Receipt;
use App\Model\FeesCollection\ReceiptDetail;
use App\Model\FeesCollection\STCheques;
use App\Model\Student\Student;
use App\Model\FeesCollection\ConcessionMap;
use App\Model\Classes\Classes;
use Yajra\Datatables\Datatables;
use DateTime;
use DateInterval;
use DatePeriod;
use Carbon\Carbon;

class FeesReportController extends Controller
{
    /**
     *  View page for RTE Cheque Data
     *  @Khushbu on 06 March 2019
    **/
    public function rteCheques() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/rte-cheques-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.rte_cheque_report')->with($data);
    }

    public function anyDataRteCheques(Request $request){
        $session        = get_current_session();
        $st_cheque_status = \Config::get('custom.st_cheques_status');
        $rte_cheque      = RTECheques::with('getBankInfo')->get()->toArray();
        return Datatables::of($rte_cheque)      
        ->addColumn('bank_name', function($rte_cheque) {
            return $rte_cheque['get_bank_info']['bank_name'];
        })
        ->addColumn('rte_cheque_date', function($rte_cheque) {
            return date("d-m-Y", strtotime($rte_cheque['rte_cheque_date']));
        })
        ->addColumn('rte_cheque_status', function($rte_cheque) use($st_cheque_status) {
            return $st_cheque_status[$rte_cheque['rte_cheque_status']];
        })  
       ->rawColumns(['bank_name' => 'bank_name', 'rte_cheque_date' => 'rte_cheque_date'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Imprest Entries Data
     *  @Khushbu on 06 March 2019
    **/
    public function imprestEntries() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/imprest-entries-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.imprest_entries_report')->with($data);
    }

    public function anyDataImprestEntries(Request $request){
        $session          = get_current_session();
        $imprest_entries  = ImprestEntry::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }  
        })->with('getClass','getSection')->get()->toArray();
        return Datatables::of($imprest_entries)      
        ->addColumn('class_section', function($imprest_entries) {
            return $imprest_entries['get_class']['class_name']. ' - ' .$imprest_entries['get_section']['section_name'];
        })
        ->addColumn('ac_entry_date', function($imprest_entries) {
            return date("d-m-Y", strtotime($imprest_entries['ac_entry_date']));
        })
       ->rawColumns(['class_section' => 'class_section', 'ac_entry_date' => 'ac_entry_date'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Class Monthly Sheet
     *  @Khushbu on 11 March 2019
    **/
    public function classMonthly(Request $request) {
        $map  = $arr_class = $arr_class1 =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class1          = get_all_classes_mediums();
        if($request->get('class_id') != '') {
            $arr_class = get_all_classes_mediums($request->get('class_id'));
        } else {
            $arr_class          = get_all_classes_mediums();
        }
        $map['arr_class']   = add_blank_option($arr_class1, "Select Class");

        $start    = (new DateTime($session['start_date']));
        $end      = (new DateTime($session['end_date']));
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        foreach ($period as $dt) {
            $sessionPeriodArr[] = array(
                'month'    => $dt->format("m"),
                'year'     => $dt->format("Y"),
                'month_year_string' => $dt->format("F-Y")
            );
        }
        foreach($arr_class as $key => $record) {
            $classMonthlyArr = [];
            foreach ($period as $dt) {
                $class_monthly     = Receipt::where([['class_id', $key],[DB::raw("MONTH(receipt_date)"), $dt->format("m")],[DB::raw("YEAR(receipt_date)"), $dt->format("Y")]])->select('class_id', DB::raw("MONTH(receipt_date) as month"), DB::raw("(SUM(total_amount)) as total_paid_amount"), DB::raw("(SUM(total_concession_amount)) as total_concession_amount"), DB::raw("(SUM(net_amount))-(SUM(total_amount)) as total_dues_amount"))->with('getClass')->get()->toArray();
                $classMonthlyArr[] = array(
                    'class_id'    => $key,
                    'class_name'  => $class_monthly[0]['get_class']['class_name'],
                    'month'       => $dt->format("m"),
                    'year'        => $dt->format("Y"),
                    'month_year_string' => $dt->format("F-Y"),
                    'total_paid_amount' => $class_monthly[0]['total_paid_amount'],
                    'total_dues_amount' => $class_monthly[0]['total_dues_amount'],
                    'total_concession_amount' => $class_monthly[0]['total_concession_amount']
                );
            }
            $final_sheet_record[] = array(
                'class_id' => $key,
                'class_name' => $record,
                'fees_data' => $classMonthlyArr
            );
        }      
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'save_url'      => url('admin-panel/fees-collection/class-monthly-sheet'),
            'redirect_url'  => url('admin-panel/fees-collection/class-monthly-sheet'),
            'login_info'    => $loginInfo,
            'map'           => $map,
            'sessionPeriodArr' => $sessionPeriodArr,
            'final_sheet_record' => $final_sheet_record,
        );
        return view('admin-panel.fees-collection-report.class_monthly_sheet')->with($data);
    }

    /**
     *  View page for Student Monthly Sheet
     *  @Khushbu on 12 March 2019
    **/
    public function studentMonthly(Request $request) {
        $map  = $arr_class = $arr_class1 = $arr_student = [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class1         = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class1, "Select Class");
        $map['arr_student'] = add_blank_option($arr_student, "Select Student");   
        $start              = (new DateTime($session['start_date']));
        $end                = (new DateTime($session['end_date']));
        $interval           = DateInterval::createFromDateString('1 month');
        $period             = new DatePeriod($start, $interval, $end);
        foreach ($period as $dt) {
            $sessionPeriodArr[] = array(
                'month'    => $dt->format("m"),
                'year'     => $dt->format("Y"),
                'month_year_string' => $dt->format("F-Y")
            );
        }
        if(!empty($request->get('class_id')) && $request->get('class_id') != null ) {
            $student = Student::with(['getStudentAcademic' => function($query) use($request) {
                $query->where('admission_class_id', $request->get('class_id'));
            }])->get()->toArray();
            if(!empty($request->get('student_id')) && $request->get('student_id') != null ) {
                $student = Student::where('student_id',$request->get('student_id'))->with(['getStudentAcademic' => function($query) use($request) {
                    $query->where('admission_class_id', $request->get('class_id'));
                }])->get()->toArray();
            }
        } else {
            $student = Student::with('getStudentAcademic')->get()->toArray();
        }
       
        foreach($student as $key => $value) {
            if(!empty($value['get_student_academic'])) {
                $arr_student[$value['student_id']]= $value['student_name'];
            }
        }
        foreach($arr_student as $key => $record) {
            $studentMonthlyArr = [];
            foreach($period as $dt) {
                $student_monthly     = Receipt::where([['student_id',$key], [DB::raw("MONTH(receipt_date)"), $dt->format("m")],[DB::raw("YEAR(receipt_date)"), $dt->format("Y")]])->select('class_id', 'student_id', DB::raw("MONTH(receipt_date) as month"), DB::raw("(SUM(total_amount)) as total_paid_amount"), DB::raw("(SUM(total_concession_amount)) as total_concession_amount"), DB::raw("(SUM(net_amount))-(SUM(total_amount)) as total_dues_amount"))->with('getClass','getStudent')->get()->toArray();
                
                $studentMonthlyArr[] = array(
                    'class_id'    => $student_monthly[0]['get_class']['class_id'],
                    'class_name'  => $student_monthly[0]['get_class']['class_name'],
                    'student_id'  => $key,
                    'student_name'  => $record,
                    'month'       => $dt->format("m"),
                    'year'        => $dt->format("Y"),
                    'month_year_string' => $dt->format("F-Y"),
                    'total_paid_amount' => $student_monthly[0]['total_paid_amount'],
                    'total_dues_amount' => $student_monthly[0]['total_dues_amount'],
                    'total_concession_amount' => $student_monthly[0]['total_concession_amount']
                );
            }
            $final_sheet_record[] = array(
                'student_id'  => $key,
                'student_name'  => $record,
                'fees_data' => $studentMonthlyArr
            );
        }    
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'save_url'      => url('admin-panel/fees-collection/student-monthly-sheet'),
            'redirect_url'  => url('admin-panel/fees-collection/student-monthly-sheet'),
            'login_info'    => $loginInfo,
            'map'           => $map,
            'sessionPeriodArr' => $sessionPeriodArr,
            'final_sheet_record' => $final_sheet_record,
        );
        return view('admin-panel.fees-collection-report.student_monthly_sheet')->with($data);
    }

    /**
     *  Get Students data according class
     *  @Khushbu on 13 March 2019
    **/
    public function getStudentsData()
    {
        $arr_student = [];
        $class_id = Input::get('class_id');
        $student = Student::with(['getStudentAcademic' => function($query) use($class_id) {
            $query->where('admission_class_id', $class_id);
        }])->get()->toArray();
        foreach($student as $key => $value) {
            if(!empty($value['get_student_academic'])) {
                $arr_student[$value['student_id']]= $value['student_name'];
            }
        }
        $data = view('admin-panel.fees-collection-report.ajax-select',compact('arr_student'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  View page for Cheque Data
     *  @Khushbu on 13 March 2019
    **/
    public function cheques() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/cheques-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.cheque_report')->with($data);
    }

    public function anyDatacheques(Request $request){
        $session          = get_current_session();
        $st_cheque_status = \Config::get('custom.st_cheques_status');
        $st_cheque        = STCheques::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('class_id') != null)
            {
                $query->where('cheques_class_id', "=", $request->get('class_id'));
            }  
        })->where('cheques_session_id', $session['session_id'])->with('getStudentInfo','getBankInfo', 'getClassInfo')->get()->toArray();
        // p($st_cheque);
        return Datatables::of($st_cheque)   
        ->addColumn('student_profile', function ($st_cheque)
        {
            $profile = "";
            if($st_cheque['get_student_info']['student_image'] != ''){
                $profile = check_file_exist($st_cheque['get_student_info']['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $st_cheque['get_student_info']['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })     
        ->addColumn('class_name', function($st_cheque) {
            return $st_cheque['get_class_info']['class_name'];
        })
        ->addColumn('bank_name', function($st_cheque) {
            return $st_cheque['get_bank_info']['bank_name'];
        })
        ->addColumn('student_cheque_date', function($st_cheque) {
            return date("d-m-Y", strtotime($st_cheque['student_cheque_date']));
        })
        ->addColumn('st_cheque_status', function($st_cheque) use($st_cheque_status) {
            return $st_cheque_status[$st_cheque['student_cheque_status']];
        }) 
       ->rawColumns(['student_profile' => 'student_profile', 'class_name' => 'class_name', 'bank_name' => 'bank_name', 'student_cheque_date' => 'student_cheque_date', 'st_cheque_status' => 'st_cheque_status'])->addIndexColumn()
        ->make(true);
    }
    
    /**
     *  View page for Student Fine Data
     *  @Khushbu on 13 March 2019
    **/
    public function fine() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/student-fine-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.fine_report')->with($data);
    }

    public function anyDatafine(Request $request){
        $session        = get_current_session();
        $student_monthly     = Receipt::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }  
        })->where([['session_id', $session['session_id']],['total_calculate_fine_amount','!=','']])->with('getStudent.getStudentAcademic.getAdmitClass', 'getStudent.getStudentAcademic.getAdmitSection')->get()->toArray();
        // p($student_monthly);
        return Datatables::of($student_monthly)   
        ->addColumn('student_profile', function ($student_monthly)
        {
            $profile = "";
            if($student_monthly['get_student']['student_image'] != ''){
                $profile = check_file_exist($student_monthly['get_student']['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $student_monthly['get_student']['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })   
        ->addColumn('class_section', function($student_monthly) {
            return $student_monthly['get_student']['get_student_academic']['get_admit_class']['class_name']. ' - ' .$student_monthly['get_student']['get_student_academic']['get_admit_section']['section_name'];
        })
       ->rawColumns(['student_profile' => 'student_profile', 'class_section' => 'class_section'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Concession Data
     *  @Khushbu on 15 March 2019
    **/
    public function concession() {
        $map                =  [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/concession-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.concession_report')->with($data);
    }

    public function anyDataConcession(Request $request){
        $arr_student_concession = [];
        $session    = get_current_session();
        $student_concession = Receipt::where(function($query) use ($request) {
            if (!empty($request) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }  
        })->where([['session_id', $session['session_id']],['total_concession_amount','!=','']])->with( 'getStudent','getClass', 'ReceiptDetail.getHeadNameInfo')->get()->groupBy('student_id')->toArray();
        // p($student_concession);
        if(!empty($student_concession)) {
            foreach($student_concession as $key => $value) {
                $total= '';
                foreach($student_concession[$key] as $ckey => $record) {
                    $studentConcessionArr[$ckey] = array(
                        'heads_name'   => $record['receipt_detail']['get_head_name_info']['ot_particular_name'],
                        'concession_amount' => $record['total_concession_amount'],
                    );
                    $total += $record['total_concession_amount'];
                }
                $arr_student_concession[] = array(
                    'student_id'      => $record['student_id'],
                    'student_name'    => $record['get_student']['student_name'],
                    'student_image'   => $record['get_student']['student_image'],
                    'class_id'        => $record['class_id'],
                    'class_name'      => $record['get_class']['class_name'],
                    'heads_concession' => $studentConcessionArr,
                    'head_concession_total'  => $total,
                );
            }
        }
        // p($arr_student_concession);
        return Datatables::of($arr_student_concession)   
        ->addColumn('student_profile', function ($arr_student_concession)
        {
            $profile = "";
            if($arr_student_concession['student_image'] != ''){
                $profile = check_file_exist($arr_student_concession['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $arr_student_concession['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        })
        ->addColumn('heads_concession', function ($arr_student_concession)
        {
            if($arr_student_concession['heads_concession'] != ''){
               foreach($arr_student_concession['heads_concession'] as $fckey => $records) {
                        $heads_concession .= $records['heads_name']. ' - ' .$records['concession_amount']."</br>";
               }
               return $heads_concession;
            }   
        })
       ->rawColumns(['student_profile' => 'student_profile', 'heads_concession' => 'heads_concession'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Student Headwise Report
     *  @Khushbu on 15 March 2019
    **/
    public function studentHeadwise() {
        $map = $arr_student = [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_student'] = add_blank_option($arr_student, "Select Student"); 
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/student-headwise-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.student_headwise_report')->with($data);
    }

    public function anyDataStudentHeadwise(Request $request) {
        $map  = $arr_class = $arr_class1 = $arr_student = [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class1         = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class1, "Select Class");
        $map['arr_student'] = add_blank_option($arr_student, "Select Student");   
        
        if(!empty($request->get('class_id')) && $request->get('class_id') != null ) {
            $student = Student::with(['getStudentAcademic' => function($query) use($request) {
                $query->where('admission_class_id', $request->get('class_id'))->with('getAdmitClass');
            }])->with(['getStudentReceipts' => function($query) {
                $query->where(DB::raw("YEAR(receipt_date)"),date('Y'));
            }])->get()->toArray();
        } else {
            $student = Student::with('getStudentAcademic.getAdmitClass')->with(['getStudentReceipts' => function($query) {
                $query->where(DB::raw("YEAR(receipt_date)"),date('Y'));
            }])->get()->toArray();
        }
        $final_sheet_record = [];
        foreach($student as $key => $record) {
            if(!empty($record['get_student_academic'])) {
            $total_fees = api_get_fees_total_student($record['student_id'],$record['get_student_academic']['admission_class_id']);
          
            $total_concession = $total_paid_fees = $total_remaining_fees = $total_net_amount = $total_fine_paid = 0;
            foreach($student[$key]['get_student_receipts'] as $fkey => $value) {
                $total_paid_fees  += $value['total_amount'];
                $total_concession += $value['total_concession_amount'];
                $total_net_amount += $value['net_amount'];
                $total_fine_paid  += $value['total_fine_amount'];
            }
            $final_sheet_record[] = array(
                'student_enroll'=> $record['student_enroll_number'],
                'student_id'    => $record['student_id'],
                'student_name'  => $record['student_name'],
                'student_image' => $record['student_image'],
                'class_id'      => $record['get_student_academic']['admission_class_id'],
                'class_name'    => $record['get_student_academic']['get_admit_class']['class_name'], 
                'total_fees_applied'      => $total_fees['total_fees'],  
                'total_paid_fees'         => $total_paid_fees,
                'total_concession_amount' => $total_concession,
                'total_remaining_fees'    => $total_fees['total_fees'] - ($total_paid_fees+$total_concession),
                'total_fine_paid'         => $total_fine_paid,
            );
            }
        }  
        return Datatables::of($final_sheet_record)  
        ->addColumn('student_profile', function ($final_sheet_record)
        {
            $profile = "";
            if($final_sheet_record['student_image'] != ''){
                $profile = check_file_exist($final_sheet_record['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL($profile);
                    }
                $st_profile = "<img src=".$student_image." height='30' />";
            }   
            $name = $final_sheet_record['student_name'];
            $complete = $st_profile."  ".$name;
            return $complete;
        }) 
       ->rawColumns(['student_profile' => 'student_profile'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Class Headwise Report
     *  @Khushbu on 18 March 2019
    **/
    public function classHeadwise() {
        $map = $arr_student = [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class          = get_all_classes_mediums();
        $map['arr_class']   = add_blank_option($arr_class, "Select Class");
        $map['arr_student'] = add_blank_option($arr_student, "Select Student"); 
        $data = array(
            'page_title'    => trans('language.menu_fees_collection_report'),
            'redirect_url'  => url('admin-panel/fees-collection/class-headwise-report'),
            'login_info'    => $loginInfo,
            'map'           => $map,
        );
        return view('admin-panel.fees-collection-report.class_headwise_report')->with($data);
    }

    public function anyDataClassHeadwise(Request $request) {
        $map  = $arr_class = $arr_class1 = [];
        $loginInfo          = get_loggedin_user_data();
        $session            = get_current_session();
        $arr_class1          = get_all_classes_mediums();
        if($request->get('class_id') != '') {
            $arr_class = get_all_classes_mediums($request->get('class_id'));
        } else {
            $arr_class          = get_all_classes_mediums();
        }
        $map['arr_class']   = add_blank_option($arr_class1, "Select Class");
        foreach($arr_class as $key => $record) {
            $classHeadwiseArr = [];
            $class_headwise   = Receipt::where([['class_id', $key],[DB::raw("YEAR(receipt_date)"), date("Y")]])->with('getClass')->get()->toArray();
            $total_feesD      = get_total_complete_fees($key); 
            $total_fees_applied = '';
            foreach($total_feesD as $tckey => $total_class_fees) {
                $total_fees_applied += $total_class_fees['head_amt'];
            }
            $total_concession = $total_received_fees = $total_remaining_fees = $total_net_amount = $total_fine_paid = '0';
            foreach($class_headwise as $ckey => $value) {
                $total_received_fees  += $value['total_amount'];
                $total_concession += $value['total_concession_amount'];
                $total_net_amount += $value['net_amount'];
                $total_fine_paid  += $value['total_fine_amount'];
            }
            $final_sheet_record[] = array(
                'class_id' => $key,
                'class_name' => $record,
                'total_fees_applied' => $total_fees_applied,
                'total_received_fees' => $total_received_fees,
                'total_concession_amount' => $total_concession,
                'total_remaining_fees' => $total_fees_applied - ($total_received_fees+$total_concession),
                'total_fine_paid'      =>  $total_fine_paid
            );
        }
        return Datatables::of($final_sheet_record)  
       ->rawColumns([ ])->addIndexColumn()
        ->make(true);
    }

}
