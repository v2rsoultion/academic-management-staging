<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SubjectClassMapping\SubjectClassmapping; // Model 
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model 
use App\Model\StudentSubjectManage\StudentSubjectManage; // Model
use App\Model\SubjectTeacherMapping\SubjectTeachermapping; // Model 
use App\Model\Subject\Subject; // Model 
use App\Model\Classes\Classes; // Model
use Yajra\Datatables\Datatables;

class SubjectClassMappingController extends Controller
{
    /**
     *  View page for Subject Class mapping
     *  @Pratyush on 07 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $mapp  = $arr_class = [];
        $arr_class = get_all_classes_mediums();
        $mapp['arr_class']  = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'    => trans('language.view_map_subject'),
            'redirect_url'  => url('admin-panel/subject-class-mapping/view-subject-class-mapping'),
            'login_info'    => $loginInfo,
            'mapp'          => $mapp
        );
        return view('admin-panel.map-subject-to-class.index')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        
        $class = Classes::where(function($query) use ($request)  {
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
        })->where('class_status',1)->with(['getSections' => function($query) {
            $query->where('section_status',1);
        }])->get();
        return Datatables::of($class)
        ->addColumn('class_name', function ($class)
        {
            $arr_medium             = get_all_mediums();
            $class_name = $class->class_name.' - '.$arr_medium[$class->medium_type];
            return $class_name;
            
        })
        ->addColumn('no_of_subject', function ($class)
        {
            return SubjectClassmapping::where('class_id',$class->class_id)->count();
            
        })
        ->addColumn('sections', function ($class)
        {
            $sections = '';
            foreach($class['getSections'] as $key => $value){
                $sections .= $value->section_name.',';
            }
            $sections = rtrim($sections,',');
            return $sections;
            
        })
        ->addColumn('action', function ($class)
        {
            $encrypted_class_id = get_encrypted_value($class->class_id, true);
                return '<div class="dropdown">
            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="zmdi zmdi-label"></i>
            <span class="caret"></span>
            </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li><a href="map-subject/' . $encrypted_class_id . '" ">Map Subject</a></li>
                <li><a href="view-report/' . $encrypted_class_id . '" ">View Report</a></li>
                </ul>
            </div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Subject Class mapping - Map Subject
     *  @Pratyush on 07 Aug 2018
    **/
    public function getMapSubject($id)
    {
        $loginInfo = get_loggedin_user_data();
        $class_name = get_class_name_by_id($id); 
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/subject-class-mapping/view-subject-class-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/class-subject-mapping/save'),
            'class_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-class.map_subject')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyMapSubjectData(Request $request)
    {
    	$session = get_current_session();
        $decrypted_class_id	= get_decrypted_value($request->get('class_id'), true);
        $class    = Classes::find($decrypted_class_id);
        if ($class)
        {
            $subject  			= Subject::orderBy('subject_id', 'ASC')->get();
            $subject_ids 		= SubjectClassmapping::select('subject_id')->where('class_id',$decrypted_class_id)->where('session_id', '=', $session['session_id'])->get();
            $subjectArr 		= array(); 
            $subject_ids_Arr 	= array();
            
            foreach($subject_ids as $subject_id){
                $subject_ids_Arr[] = $subject_id->subject_id;
            }
            
            foreach($subject as $key => $value){
                $subjectArr[$key] = $value;
                $subjectArr[$key]['subject_arr'] = $subject_ids_Arr;
            }
            return Datatables::of($subjectArr)
        		->addColumn('checkbox', function ($subjectArr)
                {
                    $check = '';
                    $exist = 0;
                	if(!empty($subjectArr['subject_arr'])){

                		if(in_array($subjectArr->subject_id, $subjectArr['subject_arr'])){
                            $check = 'checked';
                            $exist = 1;
                		}
                	}
                    return '
                    <input type="hidden" name="subjects['.$subjectArr->subject_id.'][exist]" value="'.$exist.'" >
                    <input type="hidden" name="subjects['.$subjectArr->subject_id.'][exist_id]" value="'.$subjectArr->subject_id.'" >

                    <div class="checkbox" id="customid">
                        <input type="checkbox" id="subject'.$subjectArr->subject_id.'" name="subjects['.$subjectArr->subject_id.'][subject_id]" class="check" value="'.$subjectArr->subject_id.'" '.$check.'>
                        <label  class="from_one1 " style="margin-bottom: 4px !important;"  for="subject'.$subjectArr->subject_id.'"></label>
                    </div>
                    ';
                    // return '<div class="checkbox"><input name="check[]" type="checkbox" class="check" value="'.$subjectArr->subject_id.'" '.$check.'><label for="checkbox10"></label></div>';
                    
                })
                ->addColumn('subjects', function ($subjectArr)
                {
        			return $subjectArr->subject_code.' - '.$subjectArr->subject_name;
                })->rawColumns(['checkbox' => 'checkbox','subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
        }
        else
        {
            $error_message = "Class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add and update subject class mapping's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function save(Request $request)
    {
        // p($request->all());
        $loginInfo      			= get_loggedin_user_data();
        $session = get_current_session();
        $decrypted_class_id			= get_decrypted_value($request->get('class_id'), true);
        $class    = Classes::find($decrypted_class_id);
        $admin_id = $loginInfo['admin_id'];
		if(!empty($request->get('class_id'))){
            // if(empty(Input::has('check'))){
            //    SubjectClassmapping::where('class_id',$decrypted_class_id)->delete(); 
            //    $success_msg = 'Subjects removed successfully.';
            //    return redirect('admin-panel/class-subject-mapping/map-subject/'.$request->get('class_id'))->withSuccess($success_msg);
            // }
			if(!empty(Input::has('subjects')) && count(Input::get('subjects')) != 0){
                $success_msg = 'Subjects are not selected.';
                $error_messages = [];
				try
	            {
	            	foreach (Input::get('subjects') as $value){
                        if($value['exist'] == 1 && !isset($value['subject_id']))
                        {
                            // Delete case
                            $get_section_subject = [];
                            $get_section_subject = SubjectSectionmapping::where('class_id', '=', $decrypted_class_id)->where('subject_id', '=', $value['exist_id'])->where('session_id', '=', $session['session_id'])->get()->toArray();
                           
                            $get_student_subject = [];
                            $get_student_subject = StudentSubjectManage::where('class_id', '=', $decrypted_class_id)->where('subject_id', '=', $value['exist_id'])->where('session_id', '=', $session['session_id'])->get()->toArray();
                           
                            $get_staff_subject = [];
                            $get_staff_subject = SubjectTeachermapping::where('class_id', '=', $decrypted_class_id)->where('subject_id', '=', $value['exist_id'])->get()->toArray();
                            
                            if(empty($get_section_subject) && empty($get_student_subject) && empty($get_staff_subject)){
                                $exammap_delete = SubjectClassmapping::where('class_id', '=', $decrypted_class_id)->where('subject_id', '=', $value['exist_id'])->where('session_id', '=', $session['session_id'])->first();
                                $exammap_delete->delete();
                            } else {
                                $subjectInfo = Subject::FIND($value['exist_id']);
                                $error_messages[] = "'".$subjectInfo->subject_name.'-'.$subjectInfo->subject_code."' subject already use in further mapping so we can't delete this mapping.";
                            }
                            
                        }
                        if($value['exist'] == 0 && isset($value['subject_id']))
                        {
                            $success_msg = 'You have successfully assigned Subjects to class. To assign it to each section, go to Map Subject to Section module.';	
                            // Add case
                            $subject_mapping = new SubjectClassmapping;		
                            $subject_mapping->admin_id       = $admin_id;
                            $subject_mapping->update_by      = $loginInfo['admin_id'];
                            $subject_mapping->class_id 	 	 = $decrypted_class_id;
                            $subject_mapping->subject_id 	 = $value['subject_id'];
                            $subject_mapping->session_id 	 = $session['session_id'];
                            $subject_mapping->save();
                        }
	            	}
	                
	            }
	            catch (\Exception $e)
	            {
	                //failed logic here
	                DB::rollback();
	                $error_message = $e->getMessage();
	                return redirect()->back()->withErrors($error_message);
	            }
                DB::commit();
				return redirect('admin-panel/class-subject-mapping/map-subject/'.$request->get('class_id'))->withSuccess($success_msg)->withErrors($error_messages);
    		}else{
    			return redirect()->back();
    		}
    	}else{
    		return redirect()->back();
    	}
	}

	/**
     *  View page for view report
     *  @Pratyush on 07 Aug 2018
    **/
    public function getViewReport($id)
    {
        $loginInfo = get_loggedin_user_data();
        $class_name = get_class_name_by_id($id); 
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/subject-class-mapping/view-subject-class-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/class-subject-mapping/save'),
            'class_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-class.view_report')->with($data);
    }

    /**
     *  Get Subject for view report page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyViewReportData(Request $request)
    {
        $session = get_current_session();
        $loginInfo 				= get_loggedin_user_data();
        $decrypted_class_id		= get_decrypted_value($request->get('class_id'), true);
        $subject  				= SubjectClassmapping::where('class_id',$decrypted_class_id)->where('session_id', '=', $session['session_id'])->with('getSubjects')->orderBy('subject_class_map_id', 'ASC')->get();
        
        return Datatables::of($subject)
                ->addColumn('subjects', function ($subject)
                {
        			return $subject['getSubjects']->subject_code.' - '.$subject['getSubjects']->subject_name;
                })->rawColumns(['subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
    }
}