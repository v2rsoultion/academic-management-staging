<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BookCategory\BookCategory; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class BookCategoryController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('15',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     *  View page for Book Document
     *  @Pratyush on 13 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_book_category'),
            'redirect_url'  => url('admin-panel/book-category/view-book-categories'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.book-category.index')->with($data);
    }

    /**
     *  Add page for Book Document
     *  @Pratyush on 13 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $book_category 	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_book_category_id = get_decrypted_value($id, true);
            $book_category 				= BookCategory::Find($decrypted_book_category_id);
            if (!$book_category)
            {
                return redirect('admin-panel/book-category/add-book-category')->withError('Book Category not found!');
            }
            $page_title             	= trans('language.edit_book_category');
            $encrypted_book_category_id   		= get_encrypted_value($book_category->book_category_id, true);
            $save_url               	= url('admin-panel/book-category/save/' . $encrypted_book_category_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_book_category');
            $save_url      = url('admin-panel/book-category/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'book_category' 	=> $book_category,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/book-category/view-book-categories'),
        );
        return view('admin-panel.book-category.add')->with($data);
    }

    /**
     *  Add and update Book category's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_book_category_id	= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $book_category = BookCategory::find($decrypted_book_category_id);

            if (!$book_category)
            {
                return redirect('/admin-panel/book-category/add-book-category/')->withError('Book Category not found!');
            }
            $admin_id = $book_category['admin_id'];
            $success_msg = 'Book Category updated successfully!';
        }
        else
        {
            $book_category     		= New BookCategory;
            $success_msg 	= 'Book Category saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'book_category_name'   => 'required|unique:book_categories,book_category_name,' . $decrypted_book_category_id . ',book_category_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $book_category->admin_id       		= $admin_id;
                $book_category->update_by      		= $loginInfo['admin_id'];
                $book_category->book_category_name 	= Input::get('book_category_name');
                $book_category->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/book-category/view-book-categories')->withSuccess($success_msg);
    }

    /**
     *  Get Book category's Data for view page(Datatables)
     *  @Pratyush on 13 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $book_category      = BookCategory::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('book_category_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('book_category_id', 'DESC')->get();
        return Datatables::of($book_category)
        ->addColumn('action', function ($book_category)
        {
            $encrypted_book_category_id = get_encrypted_value($book_category->book_category_id, true);
            if($book_category->book_category_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="book-category-status/'.$status.'/' . $encrypted_book_category_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-book-category/' . $encrypted_book_category_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-book-category/' . $encrypted_book_category_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Book category's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $book_category_id 	= get_decrypted_value($id, true);
        $book_category 		= BookCategory::find($book_category_id);
        
        $success_msg = $error_message =  "";
        if ($book_category)
        {
            DB::beginTransaction();
            try
            {
                $book_category->delete();
                $success_msg = "Book Category deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect('admin-panel/book-category/view-book-categories')->withSuccess($success_msg);
            } else {
                return redirect('admin-panel/book-category/view-book-categories')->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Book Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Book category's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $book_category_id 	= get_decrypted_value($id, true);
        $book_category 		= BookCategory::find($book_category_id);
        if ($book_category)
        {
            $book_category->book_category_status  = $status;
            $book_category->save();
            $success_msg = "Book Category status update successfully!";
            return redirect('admin-panel/book-category/view-book-categories')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Book Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
