<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Communication\Communication;
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model 
use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // Model
use App\Model\Communication\CommunicationMessage; // CommunicationMessage Model
use App\Model\Student\Student;
use App\Model\Staff\Staff;
use App\Model\Section\Section;
use Yajra\Datatables\Datatables;

class CommunicationController extends Controller
{
    /**
     *  View page for Communication
     *  @Sandeep on 23 Feb 2019
    **/
    public function index()
    {
        $communication = [];
        $loginInfo              = get_loggedin_user_data();
        $parent_info            = parent_info($loginInfo['admin_id']);
        $parent_student         = get_parent_student($parent_info['student_parent_id']);

        $data = array(
            'page_title'        => trans('language.communication'),
            // 'redirect_url'  => url('admin-panel/notice-board/view-notice'),
            'login_info'        => $loginInfo,
            'parent_student'    => $parent_student,
            'communication'     => $communication
        );
        return view('admin-panel.communication.index')->with($data);
    }


    /**
     *  View page for Communication staff
     *  @Sandeep on 23 Feb 2019
    **/
    public function communication_staff_data()
    {

        $loginInfo     = get_loggedin_user_data();
        $subject_id    = SubjectSectionMapping::where('class_id',$loginInfo['class_id'])->where('section_id',$loginInfo['section_id'])->get();

        $class_allocation = ClassTeacherAllocation::where('class_id',$loginInfo['class_id'])->where('section_id',$loginInfo['section_id'])->first();
        $staff_teacher = Staff::where('staff_id',$class_allocation['staff_id'])->first();
        $staff_data_subject = $staff_data = [];
        $staff_data_subject[] = array(
            'staff_id'          => $staff_teacher->staff_id,
            'staff_name'        => $staff_teacher->staff_name,
            'designation_id'    => $staff_teacher->designation_id,
            'staff_role_id'     => $staff_teacher->staff_role_id,
            'staff_type'        => "Class Teacher",
        );

        foreach ($subject_id as $subject_ids) {

            $staff_id = SubjectTeacherMapping::where(function($query) use ($subject_ids, $staff_teacher) 
            {
                $query->where('class_id',$subject_ids['class_id'])->where('section_id',$subject_ids['section_id'])->where('subject_id',$subject_ids['subject_id']);                
                if (!empty($staff_teacher->staff_id)) {
                    $query->where('staff_ids', "!=", $staff_teacher->staff_id);
                }
            })->first(); 

            if($staff_id != ""){
          
                $staff = Staff::where('staff_id',$staff_id['staff_ids'])->first()->toArray();
                $staff_data[] = array(
                    'staff_id'          => $staff['staff_id'],
                    'staff_name'        => $staff['staff_name'],
                    'designation_id'    => $staff['designation_id'],
                    'staff_role_id'     => $staff['staff_role_id'],
                    'staff_type'        => "Subject Teacher",
                );
            }
        }

        $staff_data = array_unique($staff_data);
        $staff_data = array_merge($staff_data_subject,$staff_data);
        
        return Datatables::of($staff_data)
        ->addColumn('action', function ($staff_data)
        {
            $encrypted_staff_id    = get_encrypted_value($staff_data['staff_id'], true);
            $encrypted_sender_type = get_encrypted_value(2, true);
            return '
                    <div class="pull-left"><a href="'.url('/admin-panel/communication/'.$encrypted_staff_id.'/'.$encrypted_sender_type.' ').'" > Staff Communication</a></div>
                    ';
            
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    
    /**
     *  Get Communication Response
     *  @Sandeep on 24 Feb 2019
    **/

    public function  communication($id = NULL,$sender_type = NULL){

        $decrypted_server_type    = get_decrypted_value($sender_type, true);


        $loginInfo            = get_loggedin_user_data();

        if($loginInfo['admin_type'] == 2){
            $staff_id             = $loginInfo['staff_id'];
            $parent_info          = parent_info($loginInfo['admin_id']);
            $parent_student       = get_parent_student($parent_info['student_parent_id']);
            $parent_info['student_parent_id']   = get_decrypted_value($id, true);
            $sender_id            = $loginInfo['staff_id'];
        } else {
            $staff_id             = get_decrypted_value($id, true);
            $parent_info          = parent_info($loginInfo['admin_id']);
            $parent_student       = get_parent_student($parent_info['student_parent_id']);    
            $sender_id            = $parent_info['student_parent_id'];
        }

        
        $CommunicationData    = [];

        $comm_message = CommunicationMessage::where('student_parent_id', $parent_info['student_parent_id'])->where('staff_id', $staff_id)->orderBy('comm_message_id','ASC')->with('getParent')->with('getStaff')->get();

        foreach($comm_message as $comm_messages){

            $profile_img = "public/images/default.png"; 
            if($comm_messages->sender_type == 1){  

                $name = $comm_messages['getStaff']->staff_name;
                if (!empty($comm_messages['getStaff']->staff_profile_img)){
                    $profile = check_file_exist($comm_messages['getStaff']->staff_profile_img, 'staff_profile');
                    if (!empty($profile))
                    {
                        $profile_img = $profile;
                    }
                }
            } else if($comm_messages->sender_type == 2){
                $name = $comm_messages['getParent']->student_login_name;
            }

            $CommunicationData[] = array(
                'comm_message_id'       => $comm_messages['comm_message_id'],
                'student_parent_id'     => $comm_messages['student_parent_id'],
                'staff_id'              => $comm_messages['staff_id'],
                'sender_id'             => $comm_messages['sender_id'],
                'sender_type'           => $comm_messages['sender_type'],
                'message'               => $comm_messages['message'],
                'time'                  => time_elapsed_string($comm_messages['created_at']),
                'profile_img'           => $profile_img,
                'name'                  => $name
            );   
        }

        //p($CommunicationData);

        $data = array(
            'page_title'          => trans('language.communication'),
            'login_info'          => $loginInfo,
            'student_parent_id'   => $parent_info['student_parent_id'],
            'staff_id'            => $staff_id,
            'sender_id'           => $sender_id,
            'sender_type'         => $decrypted_server_type,
            'parent_student'      => $parent_student,
            'communication_data'  => $CommunicationData
        );
        return view('admin-panel.communication.communication')->with($data);
    }


    public function  send_communication_response(Request $request){

        $loginInfo       = get_loggedin_user_data();

        //p($request->all()); 

        $student_parent_id    = Input::get('student_parent_id');
        $message_text         = Input::get('message');
        $staff_id             = Input::get('staff_id');
        $sender_id            = Input::get('sender_id');
        $sender_type          = Input::get('sender_type');
        $message_date         = '';


        $communication = New CommunicationMessage();
            DB::beginTransaction();
            try
            {
                $communication->student_parent_id     = $student_parent_id;
                $communication->staff_id              = $staff_id;
                $communication->sender_id             = $sender_id;
                $communication->sender_type           = $sender_type;
                $communication->message               = $message_text;
                $communication->save();

                $title = "New Message Received";
                if($sender_type == 1){
                    $notification = $this->send_push_notification_parent($communication->comm_message_id,$loginInfo['student_id'],'communication',$loginInfo['admin_id'],$message_text,$title);
                }

                if($sender_type == 2){
                    $notification = $this->send_push_notification_staff($communication->comm_message_id,$staff_id,'communication',$loginInfo['admin_id'],$message_text,$title);
                }

                $message_date = $taskresponse->created_at;
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return response()->json(['status'=>false, 'error_message'=> $error_message]);
            }
            DB::commit();

            if(!empty($loginInfo['admin_profile_img'])){
                $profile_img = '<img src="'.url($loginInfo['admin_profile_img']).'" class="float-right rounded-circle" title="preview" alt="preview">';
            } else {
                $default_img = "public/images/default.png"; 
                $profile_img = '<img src="'.url($default_img).'" class="float-right rounded-circle" title="preview" alt="preview">';
            }
            $user_message = '<li class="clearfix message-animation nexclass"> <div class="message my-message float-right"> <br /> '.$message_text.' <span class="chat-staff-name">'.$loginInfo['admin_name'].'</span> </div><div class="clearfix"></div> '.$profile_img.' <div class="message-data float-right"> <span class="message-data-time"> '.time_elapsed_string($message_date).' </span> &nbsp; </div><div class="clearfix"></div></li>';
            return response()->json(['status'=>true, 'user_message'=> $user_message ]);
    }


    //Notification function
    public function send_push_notification_staff($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $staff_info        = Staff::where(function($query) use ($user_id) 
        {
            $query->whereIn('staff_id',explode(",",$user_id));
        })
        ->with('getStaffAdminInfo')
        ->get()->toArray();
        foreach($staff_info as $staff){
            if($staff['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff_admin_info']['fcm_device_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => Null,
                'staff_admin_id' => $user_id,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


    //Notification function
    public function send_push_notification_parent($module_id,$user_id,$notification_type,$admin_id,$message,$title){
        $device_ids = [];
        $session = get_current_session();
        $student_info        = Student::where(function($query) use ($user_id) 
        {
            $query->whereIn('student_id',explode(",",$user_id));
        })
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();

        foreach($student_info as $student){
            // if($student['get_admin_info']['fcm_device_id'] != "" && $student['get_admin_info']['notification_status'] == 1){
            //     $device_ids[] = $student['get_admin_info']['fcm_device_id'];
            // }
            if($student['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student['get_parent']['get_parent_admin_info']['notification_status'] == 1){
                $device_ids[] = $student['get_parent']['get_parent_admin_info']['fcm_device_id'];
                $parent_admin_id = $student['get_parent']['get_parent_admin_info']['admin_id'];
            }
            $info = array(
                'admin_id' => $admin_id,
                'update_by' => $admin_id,
                'notification_via' => 0,
                'notification_type' => $notification_type,
                'session_id' => $session['session_id'],
                'class_id' => Null,
                'section_id' => Null,
                'module_id' => $module_id,
                'student_admin_id' => Null,
                'parent_admin_id' => $parent_admin_id,
                'staff_admin_id' => Null,
                'school_admin_id' => Null,
                'notification_text' => $message,
            );
            $save_notification = api_save_notification($info);
        }
        
        $send_notification = api_pushnotification($module_id,$notification_type,'','','',$device_ids,$message,$title);
    }


    public function get_staff_class_info(){
        $loginInfo  = get_loggedin_user_data();
        $staff_id = $loginInfo['staff_id'];
        $class_info = ClassTeacherAllocation::where('staff_id', '=', $staff_id)->get()->toArray();
        return $class_info;
    }


    public function view_parents($section_id = false,$subject_id = false,$class_id = false){
        $loginInfo  = get_loggedin_user_data();
        $staff_class_info = $this->get_staff_class_info();
        $staff_class_info = isset($staff_class_info[0]) ? $staff_class_info[0] : [];

        $decrypted_section_id = get_decrypted_value($section_id, true);
        $decrypted_subject_id = get_decrypted_value($subject_id, true);
        $decrypted_class_id   = get_decrypted_value($class_id, true);

        $section_id = isset($decrypted_section_id) ? $decrypted_section_id : $staff_class_info['section_id'];
        $subject_id = isset($decrypted_subject_id) ? $decrypted_subject_id : [];
        $class_id = isset($decrypted_class_id) ? $decrypted_class_id : $staff_class_info['class_id'];


        $data = array(
            'page_title'    => trans('language.view_parents'),
            'redirect_url'  => url('admin-panel/my-class/view-students'),
            'login_info'    => $loginInfo,
            'staff_class_info'    => $staff_class_info,
            'section_id'    => $section_id,
            'subject_id'    => $subject_id,
            'class_id'      => $class_id,
        );
        return view('admin-panel.communication.view_parents')->with($data);
    }

    
    public function get_parents_list(Request $request)
    {
        $student     = [];
        $arr_student_list = [];
        $session     = get_current_session();
        if (!empty($request) && ($request->get('class_id') != null || $request->get('section_id') != null) )
        {
        
        $arr_students  = Student::where(function($query) use ($request) 
            {
                $query->where('student_status',1);
            })
        ->join('student_academic_info', function($join) use ($request,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('current_section_id', '=',$request->get('section_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('current_class_id', '=',$request->get('class_id'));
            }
            $join->where('current_session_id', $session['session_id']);
        })->with('getStudentAcademic')
        ->join('student_parents', function($join) use ($request){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            if (!empty($request) && !empty($request->get('father_name')))
            {
                $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
            }
        })->orderBy('students.student_id', 'DESC')
        ->with('getParent')->with(['getRTEInfo' => function($query) use ($request,$session){
            
            $query->where('session_id', "=", $session['session_id']);
            
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }
        }])->groupby('student_parents.student_parent_id')->get();
       
        if (!empty($arr_students[0]->student_id))
        {
            foreach ($arr_students as $student)
            {
                $arr_student_list[] = array(
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student->student_name,
                    'student_status'        => $student->student_status,
                    'medium_type'           => $arr_medium[$student->medium_type],
                    'rte_status'            => $arr_rte_status[$student->rte_apply_status],
                    'rte_apply_status'      => $student->rte_apply_status,
                    'section_id'            => $request->get('section_id'),
                    'student_parent_id'     => $student['getParent']->student_parent_id,
                    'student_father_name'   => $student['getParent']->student_father_name,
                    'student_father_mobile_number'   => $student['getParent']->student_father_mobile_number,
                );
            }
        }

        //p($arr_student_list);
        

        } else {
            $arr_student_list = [];
        }
        // foreach ($arr_student_list as $key => $arr_student)
        // {
        //     //p($arr_student);
        //     $student[$key] = (object) $arr_student;
        // }
        // p();
        return Datatables::of($arr_student_list)
            
            ->addColumn('parent_communication', function ($arr_student_list)
            {
                
                $encrypted_parent_id   = get_encrypted_value($arr_student_list['student_parent_id'], true);
                $encrypted_sender_type = get_encrypted_value(1, true);
                return '
                    <div class="pull-left"><a href="'.url('/admin-panel/communication/'.$encrypted_parent_id.'/'.$encrypted_sender_type.' ').'" > Parent Communication</a></div>
                    ';
            })
            ->rawColumns(['parent_communication'=>'parent_communication'])->addIndexColumn()->make(true);
    }



}
