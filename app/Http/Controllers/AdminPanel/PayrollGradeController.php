<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Payroll\Grade; // Model
use App\Model\Payroll\PayrollMapping; // Model
use App\Model\Staff\Staff; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class PayrollGradeController extends Controller
{
    public function __construct()
    {
        $permissions = get_permissions();
        if(!in_array('16',$permissions )){
            $error_message = "Unauthorized Access";
            Redirect::to('admin-panel/unauthorized')->send();
        }
    }
    /**
     * Add Page of Payroll Grade
     * @Khushbu on 05 Feb 2019
    **/
    public function add(Request $request, $id = NULL) {
        $grade       = []; 
        $loginInfo   = get_loggedin_user_data();
        if(!empty($id))
	 	{
	 		$decrypted_grade_id 	= get_decrypted_value($id, true);
        	$grade      		    = Grade::Find($decrypted_grade_id);
            $page_title             = trans('language.edit_grade');
        	$save_url    			= url('admin-panel/payroll/manage-grade-save/'. $id);
        	$submit_button  		= 'Update';
	 	}
	 	else 
	 	{
            $page_title             = trans('language.add_grade');
	 		$save_url    			= url('admin-panel/payroll/manage-grade-save');
	 		$submit_button  		= 'Save';
	 	}
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title,
            'submit_button'   => $submit_button,
            'save_url'        => $save_url,
            'grade'           => $grade    
        );
        return view('admin-panel.payroll-grade.add')->with($data);
    }

    /**
     *	Add & Update of Payroll Grade
     *  @Khushbu on 05 Feb 2019
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_grade_id			= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        if(!empty($id))
        {
            $grade        = Grade::Find($decrypted_grade_id);
            if(!$grade) {
                return redirect('admin-panel/payroll/manage-grade')->withErrors('Grade not found!');
            }
            $admin_id       = $grade->admin_id;
            $success_msg    = 'Grade updated successfully!';
        } else {
            $grade    	    = New Grade;
            $success_msg    = 'Grade saved successfully!';
        }
            $validator             =  Validator::make($request->all(), [
                'grade_name'       => 'required|unique:pay_grade,grade_name,' . $decrypted_grade_id . ',pay_grade_id'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                $grade->admin_id          = $admin_id;
                $grade->update_by         = $loginInfo['admin_id'];
                $grade->grade_name   	  = Input::get('grade_name');
                $grade->grade_description = Input::get('grade_desc');
                $grade->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/payroll/manage-grade')->withSuccess($success_msg);
    }

    /**
     *	Get Payroll Grade's Data fo view page
     *  @Khushbu on 05 Feb 2019
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $total_staff        = Staff::count();
    	$grade 		        = Grade::where(function($query) use ($request) 
        {
           if (!empty($request) && $request->get('s_grade_name') !=  NULL)
            {
                $query->where('grade_name', 'Like', $request->get('s_grade_name').'%');
            }
        })->orderBy('pay_grade_id','DESC')->with('gradeMap')->get();
        // p($grade);
        return Datatables::of($grade)
        ->addColumn('no_of_staff_mapped', function($grade) {
            return count($grade['gradeMap']);
        })
        ->addColumn('mapped', function($grade) {
            $encrypted_grade_id  = get_encrypted_value($grade->pay_grade_id, true);
            $map_staff = '<a href="'.url('admin-panel/payroll/manage-grade/view-map-employees/'. $encrypted_grade_id. ' ').'" class="btn btn-raised btn-primary">Map Employees</a>';
            return $map_staff;
        })
    	->addColumn('action', function($grade) use($request) {
            $encrypted_grade_id  = get_encrypted_value($grade->pay_grade_id, true);
    		if($grade->grade_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="'.url('admin-panel/payroll/manage-grade-status/'.$status .'/'.$encrypted_grade_id.'').'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/payroll/manage-grade/'.$encrypted_grade_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/payroll/delete-manage-grade/' . $encrypted_grade_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';
    	})->rawColumns(['action' => 'action', 'mapped' => 'mapped'])->addIndexColumn()
    	->make(true); 
    	return redirect('/payroll/manage-grade');
    } 
    /** 
     *	Change Status of Payroll Grade
     *  @Khushbu on 05 Feb 2019
	**/
	public function changeStatus($status,$id) 
	{
		$grade_id 		= get_decrypted_value($id, true);
        $grade 	        = Grade::find($grade_id);
        if($grade) {
            $grade->grade_status  = $status;
            $grade->save();
            $success_msg = "Grade status update successfully!";
            return redirect('admin-panel/payroll/manage-grade')->withSuccess($success_msg);
        } else {
            $error_message = "Grade not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Payroll Grade
     *  @Khushbu on 05 Feb 2019
	**/
	public function destroy($id) {
		$grade_id 		= get_decrypted_value($id, true);
        $grade 	        = Grade::find($grade_id);
        if ($grade) {
            DB::beginTransaction();
            try
            {
                $grade->delete();
                $success_msg = "Grade deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Grade not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page of Staff
     *  @Khushbu on 05 Feb 2019
     */
    public function viewMapEmployees($id) {
        $loginInfo      	 = get_loggedin_user_data();
        $grade_id 		     = get_decrypted_value($id, true);
        $page_title          = trans('language.map_employees');
        $data                = array(
            'loginInfo'      => $loginInfo,
            'page_title'     => $page_title,
            'save_url'		 => url('admin-panel/payroll/manage-grade-map-employees-save/'.$id),
            'grade_id'       => $grade_id,
        );
        return view('admin-panel.payroll-grade.map-employees')->with($data);
    } 

    /**
     *  Get Employee's Map Data for view page
     *  @Khushbu on 05 Feb 2019 
     */
    public function anyDataMapEmployees(Request $request) {  
        $staff_mapped = PayrollMapping::select('staff_id')->where('pay_grade_id', $request->get('grade_id'))->get()->toArray();
        $staff = Staff::whereNotIn('staff_id', function($query) use($request) {
            $query->select('staff_id')->from('payroll_mapping')->where('pay_grade_id', '!=' ,$request->get('grade_id'));
        })->select('staff_id','staff_name','staff_profile_img')->get(); 
        $staff_ids_arr 	    = array();
        foreach($staff_mapped as $key => $value){
            $staff_ids_arr[] = $value['staff_id'];
        }
        return Datatables::of($staff,$staff_ids_arr)
        ->addColumn('checkbox', function($staff) use($staff_ids_arr) {
            $check = '';
            $exist = 0;
            if(!empty($staff_ids_arr)){
                if(in_array($staff->staff_id, $staff_ids_arr)){
                    $check = 'checked';
                    $exist = 1;
                }
            }
            return '
                <input type="hidden" name="staffs['.$staff->staff_id.'][exist]" value="'.$exist.'" >
                <input type="hidden" name="staffs['.$staff->staff_id.'][exist_id]" value="'.$staff->staff_id.'" >

                <div class="checkbox" id="customid">
                    <input type="checkbox" id="staff'.$staff->staff_id.'" name="staffs['.$staff->staff_id.'][staff_id]" class="check" value="'.$staff->staff_id.'" '.$check.'>
                    <label class="from_one1 " style="margin-bottom: 4px !important;"  for="staff'.$staff->staff_id.'"></label>
                </div>';
        })
        ->addColumn('employee_profile', function($staff) {
            $profile = '';
            if($staff->staff_profile_img != '') {
                $staff_profile_path = check_file_exist($staff->staff_profile_img, 'staff_profile');
                $profile = "<img src=".url($staff_profile_path)." height='30' />";
            }
            $staff_name = $staff->staff_name;
            $employee_profile = $profile." ".$staff_name;
            return $employee_profile;
        })->rawColumns(['checkbox' => 'checkbox', 'employee_profile' => 'employee_profile'])->addIndexColumn()
        ->make(true); 
        return redirect('/payroll/manage-grade');
    }

    /**
     *  Add Page of Employee's Map 
     *  @Khushbu on 05 Feb 2019 
     */
    public function saveMapEmployees(Request $request, $id= NULL) {
        $loginInfo      = get_loggedin_user_data();
        $session        = get_current_session();
        $admin_id       = $loginInfo['admin_id'];
        $grade_id 		= get_decrypted_value($id, true);
        if(!empty($request->get('staffs')) && count($request->get('staffs')) != 0){
            $success_msg = 'Staffs are not selected.';
            $error_messages = [];
            DB::beginTransaction();
            try
            {
                foreach ($request->get('staffs') as $value){
                    if($value['exist'] == 1 && !isset($value['staff_id']))
                    {
                        // Delete Case
                        $success_msg = "Record Updated successfully";
                        $department_map = PayrollMapping::where('staff_id',$value['exist_id'])->first();
                        // p($department_map);
                        if($department_map['pay_dept_id'] == '') 
                        {
                            $department_map->delete();
                        } else {
                            $department_map->pay_grade_id = NULL;
                            $department_map->save();
                        }
                    }
                    if($value['exist'] == 0 && isset($value['staff_id']))
                    {
                        $department_map = PayrollMapping::where('staff_id',$value['staff_id'])->first();
                        if(!isset($department_map['pay_dept_id']) && empty($department_map['pay_dept_id'])) {
                            //Add Case
                            $department_map      = New PayrollMapping;
                        }
                        $success_msg = 'Staff Mapped successfully.';
                        $department_map->admin_id         = $admin_id;
                        $department_map->update_by        = $loginInfo['admin_id'];
                        $department_map->session_id 	  = $session['session_id'];
                        $department_map->pay_grade_id 	  = $grade_id ;
                        $department_map->staff_id   	  = $value['staff_id'];
                        
                        $department_map->save();
                    }    
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            return redirect()->back()->withSuccess($success_msg)->withErrors($error_messages);   
        } else {
            return redirect()->back();
        }         
    }
}
