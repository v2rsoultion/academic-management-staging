<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\MarksheetTemplate\MarksheetTemplate;

use Yajra\Datatables\Datatables;

class MarksheetTemplateController extends Controller
{
    public function examwise_templates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.examwise_templates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.marksheets.examwise_templates')->with($data);
    }

    public function termwise_templates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.termwise_templates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.marksheets.termwise_templates')->with($data);
    }

    public function annual_templates() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.annual_templates');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.marksheets.annual_templates')->with($data);
    }

    
    /**
     *  Get Data for view page(Datatables)
     *  @Shree
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $templates = MarksheetTemplate::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('template_type') != null)
            {
                $query->where('template_type','=',$request->get('template_type'));
            }
        })->orderBy('marksheet_template_id', 'DESC')->get();
        return Datatables::of($templates)
        ->addColumn('template_link', function ($templates)
        {
            $template_link = str_replace('_', '-',$templates->template_link);
            return '
                <div class="pull-left"><a target="_blank" href="'.url('/admin-panel/marksheet-templates/'.$template_link. ' ').'" >Preview Link</a></div>
            ';
            
        }) 
        ->addColumn('template_status', function ($templates)
        {
            if($templates->template_status == 0){
                $status = "Deactive";
            } else {
                $status = "Active";
            }
           return $status;
        }) 
        ->addColumn('action', function ($templates)
        {
            $encrypted_marksheet_template_id = get_encrypted_value($templates->marksheet_template_id, true);
            if($templates->template_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="pull-left"><a href="'.url('/admin-panel/marksheet-templates/template-status/'.$status.'/'. $encrypted_marksheet_template_id. ' ').'" >'.$statusVal.'</a></div>
                ';
        })->rawColumns(['template_status' => 'template_status','action' => 'action','template_link'=> 'template_link'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Template's status
     *  @Shree 
    **/
    public function changeStatus($status,$id)
    {
        $marksheet_template_id = get_decrypted_value($id, true);
        $template    = MarksheetTemplate::find($marksheet_template_id);
        if ($template)
        {
            $template->template_status  = $status;
            $template->save();
            $success_msg = "Template status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Template not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function examwise_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.examwise_template1');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.marksheet-templates.examwise_template1')->with($data);
    }

    public function termwise_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.termwise_template1');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.marksheet-templates.termwise_template1')->with($data);
    }

    public function annual_template1() {
        $loginInfo   = get_loggedin_user_data();
        $page_title = trans('language.annual_template1');
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'      => $page_title
        );
        return view('admin-panel.marksheet-templates.annual_template1')->with($data);
    }

    
}
