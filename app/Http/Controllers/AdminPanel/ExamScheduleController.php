<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Examination\ExamMap; // Model
use App\Model\Examination\StudentExamResult; // Model
use App\Model\Examination\StMarksheet; // Model
use App\Model\SubjectSectionMapping\SubjectSectionMapping;
use App\Model\Exam\ExamSchedules; // Model
use App\Model\Exam\ExamScheduleMap; // Model
use App\Model\Exam\ExamScheduleRoomMap; // Model
use App\Model\Examination\ExamMarks; // Model
use App\Model\Examination\Grades; // Model
use App\Model\Exam\Exam;     // Model
use App\Model\Student\Student;
use App\Model\Section\Section; // Model
use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model 

use Yajra\Datatables\Datatables;

class ExamScheduleController extends Controller
{
    /**
     *  View page for exam schedules
     *  @Shree on 8 Dec 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_exam = get_all_exam_type();
        $schedule['arr_exam']   = add_blank_option($arr_exam, 'Select Exam');
        // p($schedule['arr_exam']);
        $data = array(
            'page_title'    => trans('language.schedules'),
            'redirect_url'  => url('admin-panel/examination/schedules'),
            'login_info'    => $loginInfo,
            'schedule'      => $schedule
        );
        return view('admin-panel.exam-schedule.index')->with($data);
    }

    /**
     *  Add page for exam schedules
     *  @Shree on 8 Dec 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $schedule 		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_exam_schedule_id 	= get_decrypted_value($id, true);
            $schedule       			    = ExamSchedules::Find($decrypted_exam_schedule_id);
            if (!$schedule)
            {
                return redirect('admin-panel/examinations/schedules')->withError('Exam schedule not found!');
            }
            $page_title             	= trans('language.edit_schedule');
            $encrypted_exam_schedule_id = get_encrypted_value($schedule->exam_schedule_id, true);
            $save_url               	= url('admin-panel/examination/schedule/save/' . $encrypted_exam_schedule_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_schedule');
            $save_url      = url('admin-panel/examination/schedule/save');
            $submit_button = 'Save';
        }
        
        $arr_exam = get_all_exam_type();
        $schedule['arr_exam']   = add_blank_option($arr_exam, 'Select Exam');
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'schedule' 			=> $schedule,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/examination/schedules'),
        );
        return view('admin-panel.exam-schedule.add')->with($data);
    }

    /**
     *  Add and update schedule's data
     *  @Shree on 10 Dec 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        // p($request->all());
        $decrypted_exam_schedule_id = null;
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_exam_schedule_id	= get_decrypted_value($id, true);
        $session = get_current_session();
        $admin_id       = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $schedule = ExamSchedules::find($decrypted_exam_schedule_id);
            $admin_id   = $schedule['admin_id'];
            
            if (!$schedule)
            {
                return redirect('/admin-panel/examination/add-schedule/')->withError('Schedule not found!');
            }
            $success_msg = 'Schedule updated successfully!';
        }
        else
        {
            $schedule     	= New ExamSchedules;
            $success_msg 	= 'Schedule saved successfully!';
        }
        $exam_id = null;
        if ($request->has('exam_id'))
        {
            $exam_id = Input::get('exam_id');
        }
        
        $validatior = Validator::make($request->all(), [
                'schedule_name' => 'required|unique:exam_schedules,schedule_name,' . $decrypted_exam_schedule_id . ',exam_schedule_id,exam_id,' . $exam_id,
                'exam_id'     => 'required',
        ]);
        

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $schedule->admin_id         = $admin_id;
                $schedule->update_by        = $loginInfo['admin_id'];
                $schedule->schedule_name 	= Input::get('schedule_name');
                $schedule->session_id 	    = $session['session_id'];
                $schedule->exam_id 	        = Input::get('exam_id');
                $schedule->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/examination/schedules')->withSuccess($success_msg);
    }

    /**
     *  Get schedule's Data for view page(Datatables)
     *  @Shree on 10 Dec 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $schedule  	= ExamSchedules::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('schedule_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->get('exam_id')) && $request->get('exam_id') != NULL)
            {
                $query->where('exam_id', "=", $request->get('exam_id'));
            }
        })
        ->with('getExam')
        ->with(['getExam.getExamClasses'=>function($q) use ($request) {
            $q->with('classInfo');
            // $q->groupBy('exam_map.class_id');
        }])
        ->orderBy('exam_schedule_id', 'DESC')
        ->get()->toArray();
        $schedules = [];
        foreach ($schedule as $scheduleData) {
            $classNames = [];
            $classIds = [];
            foreach ($scheduleData['get_exam']['get_exam_classes'] as $classes) {
                if(!in_array($classes['class_id'],$classIds))
                {
                    $classNames[] = $classes['class_info']['class_name'];
                    $classIds[] = $classes['class_id'];
                }
            }
            $scheduleData['exam_name'] = $scheduleData['get_exam']['exam_name'];
            $scheduleData['class_names'] = implode(',',$classNames);
            unset($scheduleData['get_exam']);
            $schedules[] = $scheduleData;
            
        }
        // p($schedules);
        return Datatables::of($schedules)
        ->addColumn('action', function ($schedules)
        {
            $encrypted_exam_schedule_id = get_encrypted_value($schedules['exam_schedule_id'], true);
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li><a  href="'.url('/admin-panel/examination/schedules/manage-classes/'.$encrypted_exam_schedule_id.' ').'"  >Manage Classes</a></li>
                        <li><a  href="'.url('/admin-panel/examination/schedules/view-exam-schedules/'.$encrypted_exam_schedule_id.' ').'"  >View Schedules</a></li>
                    </ul>
                </div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/examination/add-schedule/'.$encrypted_exam_schedule_id).'"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a  href="'.url('admin-panel/examination/delete-schedule/'.$encrypted_exam_schedule_id).'"  onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy exam schedule data
     *  @Shree on 19 July 2018
    **/
    public function destroy($id)
    {
        $exam_schedule_id = get_decrypted_value($id, true);
        $schedule    = ExamSchedules::find($exam_schedule_id);
        
        $success_msg = $error_message =  "";
        if ($schedule)
        {
            DB::beginTransaction();
            try
            {
                $schedule->delete();
                $success_msg = "Schedule deleted successfully!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return redirect()->back()->withSuccess($success_msg);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }
        else
        {
            $error_message = "Schedule not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Manage Class page for exam schedules
     *  @Shree on 10 Dec 2018
    **/
    public function manage_classes($id,Request $request)
    {
        $data  = $schedule = $arr_section = [];
        $loginInfo 		= get_loggedin_user_data();
        $decrypted_exam_schedule_id = get_decrypted_value($id, true);
        $schedule       = ExamSchedules::Find($decrypted_exam_schedule_id);
        if (!$schedule)
        {
            return redirect()->back()->withError('Exam schedule not found!');
        }
        
        $page_title    = trans('language.manage_classes');
        $save_url      = url('admin-panel/examination/schedule/manage-class/save');
        $submit_button = 'Save';
        $arr_exam = get_all_exam_type();
        $arr_class = get_all_exam_class($schedule->exam_id);
        $schedule['arr_exam']   = add_blank_option($arr_exam, 'Select Exam');
        $schedule['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $schedule['arr_section']   = add_blank_option($arr_section, 'Select Section');
        // p($schedule);
        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'schedule' 			=> $schedule,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/examination/schedules'),
        );
        return view('admin-panel.exam-schedule.manage_class')->with($data);
    }
  
    function get_subjects(Request $request){
        // p($request->all());
        $all_subjects = [];
        $all_subject_data = ExamMap::where('exam_map.exam_id','=', $request->get('exam_id'))
        ->where('exam_map.class_id','=', $request->get('class_id'))
        ->where('exam_map.section_id','=', $request->get('section_id'))
        ->leftJoin('schedule_map', function($join) use ($request){
            $join->on('schedule_map.subject_id', '=', 'exam_map.subject_id');
            if (!empty($request) && !empty($request->get('exam_schedule_id')) && $request->get('exam_schedule_id') != null){
                $join->where('exam_schedule_id', '=',$request->get('exam_schedule_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                $join->where('schedule_map.class_id', '=',$request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')) && $request->get('section_id') != null){
                $join->where('schedule_map.section_id', '=',$request->get('section_id'));
            }
        })
        ->with(['subjectInfo' => function($query){
        }])
        ->select('schedule_map.schedule_map_id','schedule_map.exam_schedule_id','exam_map.exam_id','exam_map.class_id','exam_map.section_id','exam_map.subject_id','schedule_map.staff_ids','schedule_map.exam_date','schedule_map.exam_time_from','schedule_map.exam_time_to')
        ->get()->toArray();
        foreach ($all_subject_data as $subject_data) {
            $get_all_staff = get_teaching_staff();
            $staffIds = [];
            foreach($get_all_staff as $staff_id){
                $exist_staff_id = [];
                if($subject_data['staff_ids'] != ''){
                    $exist_staff_id = explode(',',$subject_data['staff_ids']);
                }
                
                if(!in_array($staff_id,$exist_staff_id)){
                    $all_staff_map = check_staff_availability_for_examschedule($request,$staff_id);
                } 
                if(!empty($all_staff_map)){
                    foreach($all_staff_map as $map){
                        if( ( ( (strtotime($request->get('exam_time_from')) > strtotime($map['exam_time_from'])) && (strtotime($request->get('exam_time_from')) >= strtotime($map['exam_time_to']))  ) || ( (strtotime($request->get('exam_time_from')) < strtotime($map['exam_time_from'])) &&  (strtotime($request->get('exam_time_to')) >= strtotime($map['exam_time_from'])) ) ) || ( (strtotime($request->get('exam_time_from')) < strtotime($map['exam_time_from'])) && (strtotime($request->get('exam_time_to')) < strtotime($map['exam_time_from']))  )  ||  ( (strtotime($request->get('exam_time_from')) < strtotime($map['exam_time_from'])) && (strtotime($request->get('exam_time_to')) > strtotime($map['exam_time_from'])) && ( (strtotime($request->get('exam_time_to')) > strtotime($map['exam_time_to'])) &&  (strtotime($request->get('exam_time_to')) == strtotime($map['exam_time_from'])) )  )  ){

                            if( ( strtotime($request->get('exam_time_from')) <  strtotime($map['exam_time_from']))  && ( strtotime($request->get('exam_time_to')) > strtotime($map['exam_time_from'])) && ( strtotime($request->get('exam_time_from')) <  strtotime($map['exam_time_to'])) &&  ( strtotime($request->get('exam_time_to')) <=  strtotime($map['exam_time_to']))){
                                /// 
                            } else {
                                $staffIds[] = $map['staff_id'];
                            }
                        } else {
                            // Faill case
                            $staffIds[] = '';
                        }
                    }
                } else {
                    $staffIds[] = $staff_id;
                }
            }
            $staff = get_multiple_staff($staffIds);
            $staffIdsData = $subject_data['staff_ids'];
            // p($staffIdsData);
            $staff_data = view('admin-panel.staff.ajax-staff-select',compact('staff','staffIdsData'))->render();
            $subject_data['staff_data'] = $staff_data;
            // p($staff_data);
            $all_subjects[] = $subject_data;
        }
        // p($all_subjects);
        return Datatables::of($all_subjects)
            ->setRowId('id')
            ->addColumn('subject_name', function ($all_subjects)
            {
                return '
                <input type="hidden" name="map['.$all_subjects['subject_id'].'][schedule_map_id]"  id="schedule_map_id'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['schedule_map_id'].'"  >

                <input type="hidden" name="map['.$all_subjects['subject_id'].'][exam_schedule_id]"  id="exam_schedule_id'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['exam_schedule_id'].'"  >
                
                <input type="hidden" name="map['.$all_subjects['subject_id'].'][exam_id]"  id="exam_id'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['exam_id'].'"  >

                <input type="hidden" name="map['.$all_subjects['subject_id'].'][class_id]"  id="class_id'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['class_id'].'"  >

                <input type="hidden" name="map['.$all_subjects['subject_id'].'][section_id]"  id="section_id'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['section_id'].'"  >

                <input type="hidden" name="map['.$all_subjects['subject_id'].'][subject_id]"  id="subject_id'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['subject_id'].'"  >

                '.$all_subjects['subject_info']['subject_name'].' - '.$all_subjects['subject_info']['subject_code'];
            })
            ->addColumn('exam_date', function ($all_subjects)
            {
                return '<input type="date" name="map['.$all_subjects['subject_id'].'][exam_date]"  id="exam_date'.$all_subjects['subject_id'].'" class="form-control dates" onChange="CheckValid('.$all_subjects['subject_id'].')" value="'.$all_subjects['exam_date'].'" style="max-width: 150px;" >';
                
            })
            ->addColumn('exam_time', function ($all_subjects)
            {
                return '<input type="time" counter= "'.$all_subjects['subject_id'].'" name="map['.$all_subjects['subject_id'].'][exam_time_from]"  id="exam_time_from'.$all_subjects['subject_id'].'" class="form-control times " value="'.$all_subjects['exam_time_from'].'" style="max-width: 150px; float:left" onChange="CheckValid('.$all_subjects['subject_id'].')" >
               
                <input type="time" counter= "'.$all_subjects['subject_id'].'" name="map['.$all_subjects['subject_id'].'][exam_time_to]"  id="exam_time_to'.$all_subjects['subject_id'].'" class="form-control times" onChange="CheckValid('.$all_subjects['subject_id'].')" value="'.$all_subjects['exam_time_to'].'" style="max-width: 150px; float:left; margin-left: 2%;" >
                <p class="red" id="exam_time_to_err'.$all_subjects['subject_id'].'"></p>
                ';
            })
            
            ->addColumn('staff_ids', function ($all_subjects)
            {
                $arr_staff = [];
                $arr_staffs = '';
                
                $select = '
                <input type="hidden" name="map['.$all_subjects['subject_id'].'][exist_staff_ids]"  id="exist_staff_ids'.$all_subjects['subject_id'].'" class="form-control " value="'.$all_subjects['staff_ids'].'"  >
                <label class=" field select" style="width: 80%"><select class="form-control show-tick select_form1 select22" id="staff_ids'.$all_subjects['subject_id'].'" multiple  name="map['.$all_subjects['subject_id'].'][staff_ids][]" >'.$all_subjects['staff_data'].'</select></label>';
                return $select;
            })
           ->rawColumns(['subject_name' => 'subject_name','exam_date'=>'exam_date', 'exam_time'=>'exam_time','staff_ids'=>'staff_ids'])->addIndexColumn()->make(true);
    }

    public function get_available_staff(Request $request){
        // p($request->all());
        $get_all_staff = get_teaching_staff();
        $staffIds = [];
        foreach($get_all_staff as $staff_id){
            $exist_staff_id = [];
            if($request->get('exist_staff_ids') != ''){
                $exist_staff_id = explode(',',$request->get('exist_staff_ids'));
            }
            
            if(!in_array($staff_id,$exist_staff_id)){
                $all_staff_map = check_staff_availability_for_examschedule($request,$staff_id);
            } 
            if(!empty($all_staff_map)){
                foreach($all_staff_map as $map){
                    if( ( ( (strtotime($request->get('exam_time_from')) > strtotime($map['exam_time_from'])) && (strtotime($request->get('exam_time_from')) >= strtotime($map['exam_time_to']))  ) || ( (strtotime($request->get('exam_time_from')) < strtotime($map['exam_time_from'])) &&  (strtotime($request->get('exam_time_to')) >= strtotime($map['exam_time_from'])) ) ) || ( (strtotime($request->get('exam_time_from')) < strtotime($map['exam_time_from'])) && (strtotime($request->get('exam_time_to')) < strtotime($map['exam_time_from']))  )  ||  ( (strtotime($request->get('exam_time_from')) < strtotime($map['exam_time_from'])) && (strtotime($request->get('exam_time_to')) > strtotime($map['exam_time_from'])) && ( (strtotime($request->get('exam_time_to')) > strtotime($map['exam_time_to'])) &&  (strtotime($request->get('exam_time_to')) == strtotime($map['exam_time_from'])) )  )  ){

                        if( ( strtotime($request->get('exam_time_from')) <  strtotime($map['exam_time_from']))  && ( strtotime($request->get('exam_time_to')) > strtotime($map['exam_time_from'])) && ( strtotime($request->get('exam_time_from')) <  strtotime($map['exam_time_to'])) &&  ( strtotime($request->get('exam_time_to')) <=  strtotime($map['exam_time_to']))){
                            /// 
                        } else {
                            $staffIds[] = $map['staff_id'];
                        }
                    } else {
                        // Faill case
                        $staffIds[] = '';
                    }
                }
            } else {
                $staffIds[] = $staff_id;
            }
        }
        $staff = get_multiple_staff($staffIds);
        $data = view('admin-panel.staff.ajax-staff-select',compact('staff'))->render();
        return response()->json(['options'=>$data]);
        
    }

    public function save_schedule_map(Request $request){
        
        $loginInfo  = get_loggedin_user_data();
        $session    = get_current_session();
        // p($request->all());
        if($request->get('map') != ''){
            foreach ($request->get('map') as $map) {
                if($map['exam_date'] != '' && $map['exam_time_from'] != '' && $map['exam_time_to'] != ''){
                    try
                    {
                        if(!empty($map['schedule_map_id']) && $map['schedule_map_id'] != ''){
                            $mapping = ExamScheduleMap::find($map['schedule_map_id']);
                        } else {
                            $mapping = new ExamScheduleMap;
                        }
                        $staff_ids = null;
                        if(isset($map['staff_ids']) && $map['staff_ids'] != ''){
                            $staff_ids = implode(',',$map['staff_ids']);
                        }
                        $mapping->admin_id          = $loginInfo['admin_id'];
                        $mapping->update_by         = $loginInfo['admin_id'];
                        $mapping->session_id 	    = $session['session_id'];
                        $mapping->exam_schedule_id  = $map['exam_schedule_id'];
                        $mapping->exam_id           = $map['exam_id'];
                        $mapping->class_id          = $map['class_id'];
                        $mapping->section_id        = $map['section_id'];
                        $mapping->subject_id        = $map['subject_id'];
                        $mapping->staff_ids         = $staff_ids;
                        $mapping->exam_date         = $map['exam_date'];
                        $mapping->exam_time_from    = $map['exam_time_from'];
                        $mapping->exam_time_to      = $map['exam_time_to'];
                        $mapping->save();
                        
                        $success_msg = 'Schedule successfully Mapped.';
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return $error_message;
                    }
                }
            }
            
            DB::commit();
        }
        return "success";
        
    }

    public function view_exam_schedule($id){
        $schedule = $arr_section = [];
        $loginInfo = get_loggedin_user_data();
        $decrypted_exam_schedule_id 	= get_decrypted_value($id, true);
        $schedule       			    = ExamSchedules::where("exam_schedule_id",$decrypted_exam_schedule_id)->with('getExam')->first();
        

        $arr_class = get_all_exam_class($schedule->exam_id);
        $schedule['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $schedule['arr_section']   = add_blank_option($arr_section, 'Select Section');
        
        $data = array(
            'page_title'    => trans('language.view_exam_schedules'),
            'redirect_url'  => url('admin-panel/examination/view-exam-schedules/'.$id),
            'login_info'    => $loginInfo,
            'schedule'      => $schedule
        );
        return view('admin-panel.exam-schedule.view_exam_schedules')->with($data);
    }

    public function exam_schedule_subjects(Request $request){
        $mapData = [];    
        $scheduleMap = ExamScheduleMap::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('exam_id')))
            {
                $query->where('exam_id', "=", $request->get('exam_id'));
            }
            if (!empty($request) && !empty($request->get('exam_schedule_id')))
            {
                $query->where('exam_schedule_id', "=", $request->get('exam_schedule_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }

        })
        ->with('getClass')
        ->with('getSection')
        ->with('getSubjectData')
        ->orderBy('subject_id','ASC')
        ->get()
        ->toArray();

        foreach ($scheduleMap as $map) {
            $map['subject_name'] = $map['get_subject_data']['subject_name'].' - '.$map['get_subject_data']['subject_code'];
            $map['exam_date'] = date("d M Y",strtotime($map['exam_date']));
            $map['time_range'] = $map['exam_time_from'].' - '.$map['exam_time_to'];
            $all_staffs = [];
            if(!empty($map['staff_ids'])){
                $all_staffs = get_multiple_staff(explode(',', $map['staff_ids']));
            }
            $map['teachers'] = implode(',',$all_staffs);

            unset($map['get_class']);
            unset($map['get_section']);
            unset($map['get_subject_data']);
            $mapData[] = $map;
        }
        return Datatables::of($mapData)
            
            ->addColumn('teachers', function ($mapData)
            {
                return $mapData['teachers'];
            })
            ->rawColumns(['teachers' => 'teachers'])->addIndexColumn()->make(true);
    }

    public function view_exam_schedule_data(Request $request){
        // p($request->all());    
        $mapData = [];    
        $scheduleMap = ExamScheduleMap::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('exam_id')))
            {
                $query->where('exam_id', "=", $request->get('exam_id'));
            }
            if (!empty($request) && !empty($request->get('exam_schedule_id')))
            {
                $query->where('exam_schedule_id', "=", $request->get('exam_schedule_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }

        })
        ->with('getClass')
        ->with('getSection')
        ->orderBy('class_id','ASC')
        ->groupBy('section_id')
        ->get()
        ->toArray();
        foreach ($scheduleMap as $map) {
            $get_class_date =  DB::select("SELECT MIN(exam_date) as start_date, MAX(exam_date) as end_date FROM `schedule_map` WHERE class_id = ".$map['class_id']." AND section_id = ".$map['section_id']." ");
            $get_class_date = isset($get_class_date[0]) ? $get_class_date[0] : [];

            $map['class_name'] = $map['get_class']['class_name'];
            $map['section_name'] = $map['get_section']['section_name'];
            $map['start_date'] = $get_class_date->start_date;
            $map['end_date'] = $get_class_date->end_date;

            unset($map['get_class']);
            unset($map['get_section']);
            
            $mapData[] = $map;
        }

        return Datatables::of($mapData)
       
            ->addColumn('date_range', function ($mapData)
            {
                return date("d M Y", strtotime($mapData['start_date'])).' - '.date("d M Y", strtotime($mapData['end_date']));
            })
            
            ->addColumn('details', function ($mapData)
            {
                return '<button type="button" class="btn btn-raised btn-primary subjects"  exam-schedule-id="'.$mapData['exam_schedule_id'].'"  class-id="'.$mapData['class_id'].'" section-id="'.$mapData['section_id'].'" >
                Subjects
                </button>';
                
            })
            ->addColumn('publish_notify', function ($mapData)
            {
                if($mapData['publish_status'] == 0){
                    return '<button type="button" class="btn btn-raised btn-primary publish"  exam-schedule-id="'.$mapData['exam_schedule_id'].'"  class-id="'.$mapData['class_id'].'" section-id="'.$mapData['section_id'].'" >
                    Publish & Notify
                    </button>';
                } else {
                    return '<button type="button" class="btn btn-raised btn-primary"  exam-schedule-id="'.$mapData['exam_schedule_id'].'" style="background: #F8A31B !important" class-id="'.$mapData['class_id'].'" section-id="'.$mapData['section_id'].'" >
                    Published
                    </button>';
                }
            })
            ->addColumn('room_allocation', function ($mapData)
            {
                $encrypted_schedule_map_id = get_encrypted_value($mapData['schedule_map_id'], true);
                return '<a target="_blank" href="'.url('/admin-panel/examination/schedules/room-allocation/'.$encrypted_schedule_map_id.' ').'" class="btn btn-raised btn-primary rooms" >
                Room Allocation
                </a>';
                
            })
           ->rawColumns(['date_range' => 'date_range','details'=>'details','room_allocation'=>'room_allocation', 'publish_notify'=>'publish_notify'])->addIndexColumn()->make(true);
    }

    public function room_allocation($id){
        $scheduleMap = [];
        $loginInfo 		= get_loggedin_user_data();
        $decrypted_schedule_map_id 	= get_decrypted_value($id, true);
        $scheduleMap   = ExamScheduleMap::where("schedule_map_id",$decrypted_schedule_map_id)->with('getExam')->with('getSchedule')->get()->toArray();
        $scheduleMap = isset($scheduleMap[0]) ? $scheduleMap[0] : [];
        // p($scheduleMap);
        $room_map      		= ExamScheduleRoomMap::where(function($query) use ($scheduleMap) 
        {
            if ( !empty($scheduleMap['exam_schedule_id']) )
            {
                $query->where('exam_schedule_id', "=", $scheduleMap['exam_schedule_id']);
            }    
            if ( !empty($scheduleMap['exam_id']) )
            {
                $query->where('exam_id', "=", $scheduleMap['exam_id']);
            }   
            if ( !empty($scheduleMap['class_id']) )
            {
                $query->where('class_id', "=", $scheduleMap['class_id']);
            } 
            if ( !empty($scheduleMap['section_id']) )
            {
                $query->where('section_id', "=", $scheduleMap['section_id']);
            }   
            
        })
        // ->toSql();
        ->get()
        ->toArray();
        $scheduleMap['room_map'] = $room_map;
        $arr_room_no = get_all_room_no();
        $scheduleMap['arr_room_no']   = add_blank_option($arr_room_no, 'Select Room No');
        $page_title             	= trans('language.room_allocation');
        $save_url               	= url('admin-panel/examination/schedules/room-save/');
        $submit_button          	= 'Update';

        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'scheduleMap' 		=> $scheduleMap,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> '',
        );
        return view('admin-panel.exam-schedule.room_allocation')->with($data);
    }

    /**
     *  Add and update fine's data
     *  @Shree on 8 Oct 2018.
    **/
    public function room_save(Request $request)
    {
        // p($request->all());
        $loginInfo      = get_loggedin_user_data();
        $session        = get_current_session();
        DB::beginTransaction();
            try
            {
              
                foreach($request->get('room') as $room){ 
                    if(isset($room['roll_no_from']) && !empty($room['roll_no_from']) && isset($room['roll_no_to']) && !empty($room['roll_no_to']) && isset($room['room_no_id']) && !empty($room['room_no_id'])) {
                        if(isset($room['schedule_room_map_id']) ) {
                           
                            $room_update  = ExamScheduleRoomMap::where('schedule_room_map_id', $room['schedule_room_map_id'])->first();
                            $room_update->admin_id          = $loginInfo['admin_id'];
                            $room_update->update_by         = $loginInfo['admin_id'];
                            $room_update->session_id        = $session['session_id'];
                            $room_update->exam_schedule_id  = Input::get('exam_schedule_id');
                            $room_update->exam_id           = Input::get('exam_id');
                            $room_update->class_id          = Input::get('class_id');
                            $room_update->section_id        = Input::get('section_id');
                            $room_update->roll_no_from      = $room['roll_no_from'];
                            $room_update->roll_no_to        = $room['roll_no_to'];
                            $room_update->room_no_id        = $room['room_no_id'];
                            $room_update->save();
                        } else {
                            $room_update   = new ExamScheduleRoomMap();
                            $room_update->admin_id          = $loginInfo['admin_id'];
                            $room_update->update_by         = $loginInfo['admin_id'];
                            $room_update->session_id        = $session['session_id'];
                            $room_update->exam_schedule_id  = Input::get('exam_schedule_id');
                            $room_update->exam_id           = Input::get('exam_id');
                            $room_update->class_id          = Input::get('class_id');
                            $room_update->section_id        = Input::get('section_id');
                            $room_update->room_no_id        = $room['room_no_id'];
                            $room_update->roll_no_from      = $room['roll_no_from'];
                            $room_update->roll_no_to        = $room['roll_no_to'];
                            
                            $room_update->save();
                            // p($room_update);
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        return redirect()->back()->withSuccess("Room Allocation Successfully Complete");
    }

    /**
     *  Destroy Fine detail's data
     *  @Shree on 8 Oct 2018.
    **/
    public function destroyAllocateRoom($id)
    {
        $schedule_room_map_id    = $id;
        $room       = ExamScheduleRoomMap::find($schedule_room_map_id);
        $success_msg = $error_message =  "";
        if ($room)
        {
            DB::beginTransaction();
            try
            {
                $room->delete();
                $success_msg = "Allocate room successfully removed!";
            }
            catch (\Exception $e)
            {  
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
            }
            DB::commit();
            if($success_msg != ""){
                return $success_msg;
            } else {
                return $error_message;
            }
            
        }else{
            $error_message = "Allocate room information not found!";
            return $error_message;
        }
    }


    /**
     *  Student Examination
     *  @Sandeep on 10 Jan 2019.
    **/

    public function student_examination(){
        
        $loginInfo = get_loggedin_user_data();        
        $parent_info        = parent_info($loginInfo['admin_id']);
        $parent_student     = get_parent_student($parent_info['student_parent_id']);

        $data = array(
            'page_title'    => trans('language.view_exam_schedules'),
            'redirect_url'  => url('admin-panel/examination/view-exam-schedules/'.$id),
            'login_info'    => $loginInfo,
            'parent_student'    => $parent_student,
        );
        return view('admin-panel.exam-schedule.view_exam_schedule_student')->with($data);
    }

    /**
     *  Student Examination Data
     *  @Sandeep on 14 Jan 2019.
    **/

    public function view_exam_schedule_data_student(Request $request){
        // p($request->all());    
        $mapData = [];    
        $loginInfo = get_loggedin_user_data();

        $session       = get_current_session();

        $scheduleMap = ExamScheduleMap::where(function($query) use ($request, $loginInfo,$session) 
        {
            if (!empty($loginInfo['class_id']))
            {
                $query->where('class_id', "=", $loginInfo['class_id']);
            }
            if (!empty($loginInfo['section_id']))
            {
                $query->where('section_id', "=", $loginInfo['section_id']);
            }
            if (!empty($session['session']))
            {
                $query->where('session', "=", $session['session']);
            }
            $query->where('publish_status', 1);
        })
        ->with('getClass')
        ->with('getSection')
        ->with('getExam')
        ->with('getExam.getTerm')
        ->orderBy('class_id','ASC')
        ->groupBy('exam_id')
        ->get()
        ->toArray();

        foreach ($scheduleMap as $map) {
            $get_class_date =  DB::select("SELECT MIN(exam_date) as start_date, MAX(exam_date) as end_date FROM `schedule_map` WHERE class_id = ".$map['class_id']." ");
            $get_class_date = isset($get_class_date[0]) ? $get_class_date[0] : [];

            $result_date_time = StudentExamResult::where([ 'exam_id' => $map['exam_id'] , 'class_id' => $map['class_id'] , 'session_id' => $map['session_id'] ])->first();

            $map['class_name'] = $map['get_class']['class_name'];
            $map['section_name'] = $map['get_section']['section_name'];
            $map['start_date'] = $get_class_date->start_date;
            $map['end_date'] = $get_class_date->end_date;
            $map['result_date'] = $result_date_time->result_date;
            $map['result_time'] = $result_date_time->result_time;
            $map['current_date'] = date('Y-m-d');
            $map['current_time'] = date('H:i:s');

            unset($map['get_class']);
            unset($map['get_section']);
            
            $mapData[] = $map;
        }

        return Datatables::of($mapData)
            ->addColumn('term_exam_name', function ($mapData)
            {
                return $mapData['get_exam']['get_term']['term_exam_name'];
            })
            ->addColumn('exam_name', function ($mapData)
            {
                return $mapData['get_exam']['exam_name'];
            })
            ->addColumn('date_range', function ($mapData)
            {
                return date("d M Y", strtotime($mapData['start_date'])).' - '.date("d M Y", strtotime($mapData['end_date']));
            })
            
            ->addColumn('schedule', function ($mapData)
            {
                return '<button type="button" class="btn btn-raised btn-primary subjects" exam-id="'.$mapData['exam_id'].'"  exam-schedule-id="'.$mapData['exam_schedule_id'].'"  class-id="'.$mapData['class_id'].'" section-id="'.$mapData['section_id'].'" >
                Schedule
                </button>';
                
            })
            ->addColumn('marks', function ($mapData)
            {

                if($mapData['current_date'].' '.$mapData['current_time'] >= $mapData['result_date'].' '.$mapData['result_time']){
                    
                        return '<button type="button" class="btn btn-raised btn-primary marks" exam-id="'.$mapData['exam_id'].'"  exam-schedule-id="'.$mapData['exam_schedule_id'].'"  class-id="'.$mapData['class_id'].'" section-id="'.$mapData['section_id'].'" >  Marks </button>';
                    
                } else {
                    return 'Your resuld will be declared on '.date("d M Y",strtotime($mapData['result_date'])).' at '.date("h:i A",strtotime($mapData['result_time'])).'';
                }
                
            })
           ->rawColumns(['date_range' => 'date_range','schedule'=>'schedule','marks'=>'marks'])->addIndexColumn()->make(true);
    }


    /**
     *  Student Examination Subject Data
     *  @Sandeep on 14 Jan 2019.
    **/


    public function exam_schedule_subjects_student(Request $request){
        $mapData = [];    
        $scheduleMap = ExamScheduleMap::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('exam_id')))
            {
                $query->where('exam_id', "=", $request->get('exam_id'));
            }
            if (!empty($request) && !empty($request->get('exam_schedule_id')))
            {
                $query->where('exam_schedule_id', "=", $request->get('exam_schedule_id'));
            }
            if (!empty($request) && !empty($request->get('class_id')))
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->get('section_id')))
            {
                $query->where('section_id', "=", $request->get('section_id'));
            }

        })
        ->with('getClass')
        ->with('getSection')
        ->with('getSubjectData')
        ->with('getRoomMap')
        ->with('getRoomMap.getRoom')
        ->orderBy('exam_date','ASC')
        ->get()
        ->toArray();

        foreach ($scheduleMap as $map) {
            $map['subject_name'] = $map['get_subject_data']['subject_name'].' - '.$map['get_subject_data']['subject_code'];
            $map['exam_date'] = date("d M Y",strtotime($map['exam_date']));
            $map['time_range'] = $map['exam_time_from'].' - '.$map['exam_time_to'];
            $all_staffs = [];
            if(!empty($map['staff_ids'])){
                $all_staffs = get_multiple_staff(explode(',', $map['staff_ids']));
            }
            $map['teachers'] = implode(',',$all_staffs);

            unset($map['get_class']);
            unset($map['get_section']);
            unset($map['get_subject_data']);
            $mapData[] = $map;
        }

        return Datatables::of($mapData)
            
            ->addColumn('room_no', function ($mapData)
            {
                return $mapData['get_room_map']['get_room']['room_no'];
            })
            ->rawColumns(['room_no' => 'room_no'])->addIndexColumn()->make(true);
    }


    /**
     *  Student Examination Marks Data
     *  @Sandeep on 14 Jan 2019.
    **/
    
    public function exam_marks_student(Request $request){
        $mapData = [];    

        $loginInfo = get_loggedin_user_data();
        $scheduleMap = ExamMarks::where(function($query) use ($request,$loginInfo) {
            $query->where('exam_marks.exam_id','=', $request->get('exam_id'));
            $query->where('exam_marks.class_id','=', $request->get('class_id'));
            $query->where('exam_marks.section_id','=', $request->get('section_id'));
            $query->where('exam_marks.student_id','=', $loginInfo['student_id']);
        })            
        ->leftJoin('exam_map', function($join) use ($request){
            $join->on('exam_map.subject_id', '=', 'exam_marks.subject_id');
            $join->where('exam_map.exam_id', '=', $request->get('exam_id'));
            $join->where('exam_marks.class_id','=', $request->get('class_id'));
            $join->where('exam_marks.section_id','=', $request->get('section_id'));
        })
        ->select('exam_marks.*', 'exam_map.marks_criteria_id','exam_map.grade_scheme_id')
        ->with('marksCriteriaInfo')
        ->with('subjectInfo')
        ->groupBy('exam_mark_id')
        ->get()->sortBy('subjectInfo.subject_is_coscholastic')
        ->toArray();

        foreach ($scheduleMap as $map) {
            $gradedata = Grades::where('grades.grade_min', "<=", $map['marks'])->where('grades.grade_max', ">=", $map['marks'])->first();

            if($map['subject_info']['subject_is_coscholastic'] == 1){
                $map['subject_name'] = $map['subject_info']['subject_name'].' - '.$map['subject_info']['subject_code'].' ( Coscholastic )';
            } else {
                $map['subject_name'] = $map['subject_info']['subject_name'].' - '.$map['subject_info']['subject_code'];
            }
            

            $map['max_marks'] = $map['marks_criteria_info']['max_marks'];
            $map['grade_name'] = $gradedata['grade_name'];
            if($map['grade_name'] == ""){
                $map['grade_name'] = "-";
            }
            $all_staffs = [];
          
            unset($map['get_class']);
            unset($map['get_section']);
            unset($map['subject_info']);
            unset($map['marks_criteria_info']);
            $mapData[] = $map;
        }


        return Datatables::of($mapData)
        ->rawColumns([])->addIndexColumn()->make(true);
    }
    public function publish_schedule(Request $request){
        $session       = get_current_session();
        $examInfo = Exam::find($request->get('exam_id'));
        $send_notification_by_topic  = $this->send_push_notification_class($request->get('exam_schedule_id'),$examInfo->exam_name,$request->get('class_id'),$request->get('section_id'),'exam_schedule_create');
        
        $send_notification  = $this->send_push_notification($request->get('exam_schedule_id'),$examInfo->exam_name,$request->get('class_id'),$request->get('section_id'),'exam_schedule_create');

        DB::table('schedule_map') 
        ->where('session_id',$session['session_id'])
        ->where('exam_schedule_id',$request->get('exam_schedule_id'))
        ->where('exam_id',$request->get('exam_id'))
        ->where('class_id',$request->get('class_id'))
        ->where('section_id',$request->get('section_id'))
        ->update([ 'publish_status' => 1 ]);
        return "success";
    }

    public function student_list($class_id,$section_id){
        $session       = get_current_session();
        $arr_student      = Student::where(array('students.student_status'=> 1))
        ->join('student_academic_info', function($join) use ($class_id,$section_id,$session){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            $join->where('current_class_id', '=',$class_id);
            $join->where('current_section_id', '=',$section_id);
            $join->where('current_session_id', '=',$session['session_id']);
        })
        ->join('student_parents', function($join) use ($class_id,$section_id){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->orderBy('student_name', 'ASC')->with('getAdminInfo')->get()->toArray();
        p($arr_student);
    }

    public function send_push_notification_class($module_id,$exam_name,$class_id,$section_id,$notification_type){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('class_id',$class_id);
            $query->where('section_id',$section_id);
        })
        ->with('sectionClass')
        ->get()->toArray();
        $message = $exam_name." Scheduled is published.";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);
            
            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

            $students = pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['class_id'],$section['section_id'],1);
            $parents = pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['class_id'],$section['section_id'],2);
           
        }
        
    }

    public function send_push_notification($module_id,$exam_name,$class_id,$section_id,$notification_type){
        $device_ids = [];
        $teachingStaff = SubjectTeacherMapping::where(function($query) use ($class_id,$section_id) 
        {
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $query->where('class_id', $class_id);
            }
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $query->where('section_id', $section_id);
            }
        })
        ->with('getStaff.getStaffAdminInfo')
        ->get()->toArray();
        $message = $exam_name." Scheduled is published.";

        $loginInfo  = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $session = get_current_session();

        foreach ($teachingStaff as $key => $staff) {
            if(isset($staff['get_staff']['reference_admin_id'])  && $staff['get_staff']['get_staff_admin_info']['notification_status'] == 1){
                $info = array(
                    'admin_id' => $admin_id,
                    'update_by' => $admin_id,
                    'notification_via' => 0,
                    'notification_type' => $notification_type,
                    'session_id' => $session['session_id'],
                    'class_id' => Null,
                    'section_id' => Null,
                    'module_id' => $module_id,
                    'student_admin_id' => Null,
                    'parent_admin_id' => Null,
                    'staff_admin_id' => $staff['get_staff']['reference_admin_id'],
                    'school_admin_id' => Null,
                    'notification_text' => $message,
                );
                $save_notification = save_notification($info);
            }
            if($staff['get_staff']['get_staff_admin_info']['fcm_device_id'] != "" && $staff['get_staff']['get_staff_admin_info']['notification_status'] == 1){
                $device_ids[] = $staff['get_staff']['get_staff_admin_info']['fcm_device_id'];
            }
        }
        
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
    }


    /**
     *  Student Examination Data
     *  @Sandeep on 6 march 2019.
    **/

    public function exam_marks_student_total(Request $request){

        $resultData       = [];    
        $loginInfo        = get_loggedin_user_data();
        $session          = get_current_session();

        $result_date_time = StMarksheet::where([ 'student_id' => $loginInfo['student_id'] , 'exam_id' => $request->get('exam_id') , 'class_id' => $request->get('class_id') , 'session_id' => $session['session_id'] , 'section_id' => $request->get('section_id') ])->first();

        //p($result_date_time);
        $data = array(
            'result_date_time'    => $result_date_time,
        );
        return view('admin-panel.exam-schedule.result_data_exam')->with($data);
    }


}
