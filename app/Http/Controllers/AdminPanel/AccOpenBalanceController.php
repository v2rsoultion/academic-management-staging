<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Accounts\AccOpenBalance; // Model
use Yajra\Datatables\Datatables;
use Redirect;

class AccOpenBalanceController extends Controller
{
    /** 
	 *  Add Page of Acoount Group Head
     *  @Shree on 05 Apr 2019
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$arr_groups = $arr_heads = $open_balance = $arr_group_h = [];
        $loginInfo               = get_loggedin_user_data();
	 	if(!empty($id))
	 	{
	 		$decrypted_acc_open_balance_id = get_decrypted_value($id, true);
        	$open_balance      		= AccOpenBalance::Find($decrypted_acc_open_balance_id);
            $page_title             = trans('language.edit_open_balance');
        	$arr_groups             = get_all_group_heads($open_balance->acc_sub_head_id);
        	$save_url    			= url('admin-panel/account/manage-opening-balance-save/'. $id);
        	$submit_button  		= 'Update';
	 	} else {
            $page_title             = trans('language.open_balance');
	 		$save_url    			= url('admin-panel/account/manage-opening-balance-save');
	 		$submit_button  		= 'Save';
	 	}
        $get_all_account_heads      = get_all_account_heads();
        $open_balance['arr_heads']  = add_blank_option($get_all_account_heads, 'Select Head');
        $open_balance['arr_groups'] = add_blank_option($arr_groups, 'Select Group Heads');
        $data                   	=  array(
            'page_title'            => $page_title,
            'login_info'        	=> $loginInfo,
            'save_url'          	=> $save_url,  
            'open_balance'		    => $open_balance,	
            'submit_button'	    	=> $submit_button
        );
        return view('admin-panel.account-opening-balance.add')->with($data);
    }

    /**
     *	Add & Update of Opening Balance
     *  @Shree on 08 Apr 2019
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      = get_loggedin_user_data();
        $decrypted_acc_open_balance_id	= get_decrypted_value($id, true);
        $admin_id       = $loginInfo['admin_id'];
        if(!empty($id))
        {
            $open_balance   = AccOpenBalance::Find($decrypted_acc_open_balance_id);
            if(!$open_balance) {
                return redirect('admin-panel/account/manage-opening-balance')->withError('Opening balance not found!');
            }
            $admin_id       = $open_balance->admin_id;
            $success_msg 	= 'Opening Balance updated successfully!';
        } else {
            $open_balance   = New AccOpenBalance;
            $success_msg 	= 'Opening Balance saved successfully!';
        }

        $acc_sub_head_id = null;
        if ($request->has('acc_sub_head_id'))
        {
            $acc_sub_head_id = Input::get('acc_sub_head_id');
        }
        $acc_group_head_id = null;
        if ($request->has('acc_group_head_id'))
        {
            $acc_group_head_id = Input::get('acc_group_head_id');
        }
        $balance_date = null;
        if ($request->has('balance_date'))
        {
            $balance_date = Input::get('balance_date');
        }
        $validator  =  Validator::make($request->all(), [
        	'acc_sub_head_id'   => 'required',
        	'acc_group_head_id'	=> 'required',
    		'balance_amount'  	    => 'required|unique:acc_open_balance,balance_amount,' . $decrypted_acc_open_balance_id . ',acc_open_balance_id,acc_sub_head_id,' . $acc_sub_head_id.',acc_group_head_id,'.$acc_group_head_id.',balance_date,'.$balance_date,
    	]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            DB::beginTransaction();
            try
            {
                $open_balance->admin_id         = $admin_id;
                $open_balance->update_by       	= $loginInfo['admin_id'];
                $open_balance->acc_sub_head_id  = Input::get('acc_sub_head_id');
                $open_balance->acc_group_head_id    = Input::get('acc_group_head_id');
                $open_balance->balance_amount   = Input::get('balance_amount');
                $open_balance->balance_date    	= Input::get('balance_date');
                $open_balance->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/account/manage-opening-balance')->withSuccess($success_msg);
    }

    /**
     *	Get Account Group's Data fo view page
     *  @Khushbu on 02 Apr 2019
    **/
    public function anyData(Request $request)
    {
    	$loginInfo 		= get_loggedin_user_data();
    	$open_balance   = AccOpenBalance::where(function($query) use ($request) 
        {
            if (!empty($request) && $request->get('s_acc_sub_head_id') !=  NULL)
            {
                $query->where('acc_sub_head_id', $request->get('s_acc_sub_head_id'));
            }
            if (!empty($request) && $request->get('s_acc_group_head_id') !=  NULL)
            {
                $query->where('acc_group_head_id', $request->get('s_acc_group_head_id'));
            }
        })->orderBy('acc_open_balance_id','DESC')->with('getSubHead.getMainHead','getAccGroupHead.getAccGroup')->get();
    	return Datatables::of($open_balance)
        ->addColumn('head_name', function($open_balance) {
            return $open_balance['getSubHead']->acc_sub_head.' ('.$open_balance['getSubHead']['getMainHead']->acc_main_head.')';
        })
        ->addColumn('group_name', function($open_balance) {
            return $open_balance['getAccGroupHead']->acc_group_head.' ('.$open_balance['getAccGroupHead']['getAccGroup']->acc_group.')';
        })
        ->addColumn('balance_date', function($open_balance) {
            return date("d F, Y", strtotime($open_balance->balance_date));
        })
        ->addColumn('balance_amount', function($open_balance) {
            return "Rs. ".$open_balance->balance_amount;
        })
    	->addColumn('action', function($open_balance) use($request) {
    		$encrypted_acc_open_balance_id  = get_encrypted_value($open_balance->acc_open_balance_id, true);
      		return '<div class="text-center">
      				
                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/account/manage-opening-balance/'.$encrypted_acc_open_balance_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="'.url('admin-panel/account/delete-manage-opening-balance/' . $encrypted_acc_open_balance_id .''). '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
            ';

    	})->rawColumns(['head_name'=> 'head_name','group_name'=> 'group_name','balance_date'=>'balance_date','balance_amount'=>'balance_amount','action' => 'action'])->addIndexColumn()
    	->make(true); 
    	
    } 

    /**
     *  Destroy Data of Opening Balance
     *  @shree on 08 Apr 2019
    **/
    public function destroy($id) {
        $acc_open_balance_id   = get_decrypted_value($id, true);
        $open_balance      = AccOpenBalance::find($acc_open_balance_id);

        if ($open_balance) {
            DB::beginTransaction();
            try
            {
                $open_balance->delete();
                $success_msg = "Opening Balance deleted successfully!";
            }  
            catch(\Exception $e)
            {
                DB::rollback();
                $error_message = "Sorry we can't delete it because it's already in used!!";
                return redirect()->back()->withErrors($error_message);
            }  
            DB::commit();
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Opening Balance not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

   
}
