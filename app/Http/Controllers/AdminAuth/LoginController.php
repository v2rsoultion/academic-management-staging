<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Admin; // Model

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/admin-panel/dashboard';

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest', ['except' => 'logout']);
        $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $admin_type = request()->input('admin_type');
        if($admin_type == 1 || $admin_type == 2 || $admin_type == 3) 
        {
            $username = request()->input('username');
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
        if($admin_type == 4)
        {
            $username = request()->input('username');
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'enroll_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
    }
 
    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $arr_admin_type = \Config::get('custom.admin_type');
        $admin_type['arr_admin_type']  = $arr_admin_type;
        $data = array(
            'admin_type'    => $admin_type
        );
        return view('admin-panel.auth.login')->with($data);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        // Load user from database
        $user = \App\Admin::where('admin_type', $request->get('admin_type'))
        ->where(function ($query) use ($request) { 
            $query->where('email', $request->get('username'))
                  ->orWhere('enroll_no', $request->get('username'))
                  ->orWhere('mobile_no', $request->get('username'));
        })->first();
        // Check if user was successfully loaded, that the password matches
        if(!empty($user)){
            if (Hash::check($request->get('password'), $user->password)) {
                authenticated($user);
            }else{
                return redirect('admin-panel')->withErrors('Invalid User name or Password');    
            }
        }else{
            return redirect('admin-panel')->withErrors('Invalid User name or Password');  
        }
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {   
        if ($request->admin_type != $user->admin_type) {
            $this->logout($request);
            return redirect('admin-panel')->withErrors('Unauthorized User!');  
        }
    }
    
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/admin-panel');
    }
}
