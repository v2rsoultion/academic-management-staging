<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Admin; // Model
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest');
        $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $admin_type = request()->input('admin_type');
        if($admin_type == 1 || $admin_type == 2 || $admin_type == 3) 
        {
            $username = request()->input('username');
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
        if($admin_type == 4)
        {
            $username = request()->input('username');
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'enroll_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
    }
 
    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        $arr_admin_type = \Config::get('custom.admin_type');
        $admin_type['arr_admin_type']  = $arr_admin_type;

        $data = array(
            'admin_type'    => $admin_type
        );
        return view('admin-panel.auth.passwords.email')->with($data);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        // Load user from database
        $user = \App\Admin::where('admin_type', $request->get('admin_type'))
        ->where(function ($query) use ($request) { 
            $query->where('email', $request->get('username'))
                  ->orWhere('enroll_no', $request->get('username'))
                  ->orWhere('mobile_no', $request->get('username'));
        })->with('adminStudent.getParent')->first();
        if(!empty($user)){
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );
            if($response === Password::RESET_LINK_SENT) {
                return redirect('admin-panel/password/reset')->withSuccess('Mail has been send successfully! Please check your email');    
            } else {
                $token = str_random(64);
                if($this->username === 'mobile_no')
                {
                    $username = $user['mobile_no'];
                    DB::table('admin_password_resets')->insert(
                    ['mobile_no' => $request->get('username'), 'token' => Hash::make($token), 'created_at' => date('Y-m-d H:i:s')]
                    );
                }
                if($this->username === 'enroll_no')
                {
                    $username = $user['enroll_no'];
                    DB::table('admin_password_resets')->insert(
                    ['enroll_no' => $request->get('username'), 'token' => Hash::make($token), 'created_at' => date('Y-m-d H:i:s')]
                    );
                }
                // Short URL
                $base_url  = env('APP_URL');
                $reset_url = $base_url."/academic-management/admin-panel/password/reset/".$token."/".$username;
                // when website is real hosting name then create short url  
                $reset_short_url = shortenUrl($reset_url);
                if($user['admin_type'] == 4)
                {
                    $mobile_no = $user['adminStudent']['getParent']['student_login_contact_no'];
                } else {
                    $mobile_no = $user['mobile_no'];
                }
                $message   = "Reset Your Password Click here >>".$reset_short_url['short_url'];
                // $message   = "Click here to Reset Your Password >>".$reset_url;
                $bulkSms = send_sms_with_parameters($mobile_no, $message);
                return redirect('admin-panel/password/reset')->withSuccess('SMS has been Send successfully!');
            }
           
            return back()->withErrors(
                ['email' => trans($response)]
            );
        } else {
            return redirect('admin-panel/password/reset')->withErrors('Invalid Username and type!');
        }
    }
}
