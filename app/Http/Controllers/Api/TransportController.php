<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use DateTime;
use Validator;

use App\Model\Transport\Vehicle; // Vehicle Model
use App\Model\Transport\VehicleAttendence; // VehicleAttendence Model
use App\Model\Transport\VehicleAttendDetails; // VehicleAttendDetails Model
use App\Model\Student\Student; // Student Model
use App\Model\Staff\Staff; // Staff Model
use App\Model\Transport\MapStudentStaffVehicle; // MapStudentStaffVehicle Model


class TransportController extends Controller
{
    /* Function name : getVehicleList
     *  To get vehicle list
     *  @Sumit on 16 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getVehicleList(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id              = $request->input('device_id');
        $csrf_token             = $request->input('csrf_token');
        $vehicle_name           = $request->input('vehicle_name');
        $registration_number    = $request->input('registration_number');
        $ownership              = $request->input('ownership');
        $contact_person_name    = $request->input('contact_person_name');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $vehicle = Vehicle::where(function($query) use ($vehicle_name,$registration_number,$ownership,$contact_person_name) 
                { 
                    $query->where('vehicle_status', 1); 
                    
                    if ( $vehicle_name !='' ) 
                    {
                        $query->where('vehicle_name', "like", "%{$vehicle_name}%");
                    }

                    if ( $registration_number !='' ) 
                    {
                        $query->where('vehicle_registration_no', $registration_number); 
                    }

                    if ( $ownership !='' ) 
                    {
                        $query->where('vehicle_ownership', $ownership); 
                    }

                    if ( $contact_person_name !='' ) 
                    {
                        $query->where('contact_person', "like", "%{$contact_person_name}%");
                    }
                })
                ->with('get_driver_conductor.getDriver')
                ->with('get_driver_conductor.getConductor')
                ->orderBy('vehicle_id','DESC')
                ->get();

                if( count($vehicle) != 0 ){

                    $vehicle_info = [];
                    $driver_info = [];
                    $conductor_info = [];
                    foreach ($vehicle as $vehicle_res) {

                        if($vehicle_res['vehicle_ownership'] == 0) {
                            $ownership = 'Owner';
                        } else {
                            $ownership = 'Contract';
                        }

                        if( $vehicle_res['get_driver_conductor']['driver_id'] !='' ){
                            $driver_info[] = array(
                                'name'          => $vehicle_res['get_driver_conductor']['getDriver']['staff_name'],
                                'email'         => $vehicle_res['get_driver_conductor']['getDriver']['staff_email'],
                                'mobile_number' => $vehicle_res['get_driver_conductor']['getDriver']['staff_mobile_number'],
                            );
                        } else {
                            $driver_info = [];
                        }
                        
                        if( $vehicle_res['get_driver_conductor']['conductor_id'] !='' ){
                            $conductor_info[] = array(
                                'name'          => $vehicle_res['get_driver_conductor']['getConductor']['staff_name'],
                                'email'         => $vehicle_res['get_driver_conductor']['getConductor']['staff_email'],
                                'mobile_number' => $vehicle_res['get_driver_conductor']['getConductor']['staff_mobile_number'],
                            );
                        } else {
                            $conductor_info = [];
                        }

                        $vehicle_info[] = array(
                            'vehicle_id'                => check_string_empty($vehicle_res['vehicle_id']),
                            'vehicle_name'              => check_string_empty($vehicle_res['vehicle_name']),
                            'vehicle_registration_no'   => check_string_empty($vehicle_res['vehicle_registration_no']),
                            'vehicle_ownership'         => check_string_empty($vehicle_res['vehicle_ownership']),
                            'vehicle_capacity'          => check_string_empty($vehicle_res['vehicle_capacity']),
                            'contact_person'            => check_string_empty($vehicle_res['contact_person']),
                            'contact_number'            => check_string_empty($vehicle_res['contact_number']),
                            'driver_info'               => $driver_info,
                            'conductor_info'            => $conductor_info,
                        );
                    }

                    $result_data['vehicle_info'] = $vehicle_info;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /* Function name : getVehicleAttendance
     *  To get vehicle Attendance
     *  @Sumit on 16 FEB 2019    
     *  @parameters : device_id,csrf_token,vehicle_id,date
     *  @V2R by Sumit
    */
    public function getVehicleAttendance(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $date           = $request->input('date');
        $vehicle_id     = $request->input('vehicle_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($date) && !empty($vehicle_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $vehicle  = Vehicle::where(function($query) use ($request,$vehicle_id) 
                {
                    $query->where('vehicles.vehicle_id','=',$vehicle_id);
                })
                ->join('assign_driver_conductor_routes', function($join) {
                    $join->on('assign_driver_conductor_routes.vehicle_id', '=', 'vehicles.vehicle_id');
                })
                ->with('getDriver')
                ->with('getConductor')
                ->first();

                if( !empty($vehicle) ){

                    $vehicle_info = array(
                        'vehicle_id'        => check_string_empty($vehicle['vehicle_id']),
                        'vehicle_name'      => check_string_empty($vehicle['vehicle_name']),
                        'driver_name'       => check_string_empty($vehicle['getDriver']['staff_name']),
                        'conductor_name'    => check_string_empty($vehicle['getConductor']['staff_name']),
                    );

                    $vehicle_attendance = VehicleAttendence::where(function($query) use ($date,$vehicle_id,$session) 
                    { 
                        $query->where([ ['session_id', $session['session_id']], ['vehicle_id', $vehicle_id], ['vehicle_attendence_date', $date] ]);
                        
                    })
                    ->with('attendence_details')
                    ->first();

                    if( !empty($vehicle_attendance) ){
                        $attendance_res_arr = [];
                        foreach($vehicle_attendance['attendence_details'] as $attendance_res){

                            if( $attendance_res['student_staff_type'] == 1 ){

                                $student = Student::where('student_id', $attendance_res['student_staff_id'])->select('student_id','student_name')->first();
                                $name = $student['student_name'];

                            } else if($attendance_res['student_staff_type'] == 2){

                                $staff = Staff::where('staff_id', $attendance_res['student_staff_id'])->select('staff_id','staff_name')->first();
                                $name = $staff['staff_name'];
                            }

                            $attendance_res_arr[] =  array(
                                'vehicle_attendence_id'         => check_string_empty($attendance_res['vehicle_attendence_id']),
                                'vehicle_attend_d_id'           => check_string_empty($attendance_res['vehicle_attend_d_id']),
                                'vehicle_attendence_type'       => $attendance_res['vehicle_attendence_type'],
                                'member_id'                     => check_string_empty($attendance_res['student_staff_id']),
                                'member_type'                   => check_string_empty($attendance_res['student_staff_type']),
                                'member_name'                   => check_string_empty($name),
                                'attendence_pick_up'            => check_string_empty($attendance_res['attendence_pick_up']),
                                'attendence_drop'               => check_string_empty($attendance_res['attendence_drop']),
                                'attendance_flag'               => check_string_empty($attendance_res['vehicle_attendence_status']),
                            );
                        }

                        $result_data['vehicle_info']    = $vehicle_info;
                        $result_data['attendance_info'] = $attendance_res_arr;
                        $status = true;
                        $message = 'success';

                    } else {

                        $attendance_data = MapStudentStaffVehicle::where('vehicle_id',$vehicle_id)->get();
                        $attendance_res_arr = [];
                        foreach($attendance_data as $attendance_res){

                            if( $attendance_res['student_staff_type'] == 1 ){

                                $student = Student::join('student_academic_info', function($join) {
                                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                                })->where('students.student_id', $attendance_res['student_staff_id'])->select('students.student_id','students.student_name','students.student_image','current_class_id','current_section_id')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->first();

                                $member_name     = $student['student_name'];
                                $member_id       = $student['student_id'];
                                $member_image    = check_string_empty($student['student_image']);
                                $member_class    = $student['getStudentAcademic']['getCurrentClass']['class_name'];
                                $member_section  = $student['getStudentAcademic']['getCurrentSection']['section_name'];
                                $member_type     = $attendance_res['student_staff_type'];

                            } else if($attendance_res['student_staff_type'] == 2){

                                $staff = Staff::where('staff_id', $attendance_res['student_staff_id'])->select('staff_id','staff_name')->first();
                                $member_name     = $staff['staff_name'];
                                $member_id       = $staff['staff_id'];
                                $member_type     = $attendance_res['student_staff_type'];
                                $member_image    = "";
                                $member_class    = "";
                                $member_section  = "";
                            }

                            $attendance_res_arr[] =  array(
                                'member_id'         => $member_id,
                                'member_name'       => $member_name,
                                'member_type'       => $member_type,
                                'member_image'      => $member_image,
                                'member_class'      => $member_class,
                                'member_section'    => $member_section,
                            );


                        }

                        $result = usort($attendance_res_arr, function($a, $b) {
                            return strcmp($a['member_name'], $b['member_name']);
                        });

                        $result_data['vehicle_info']    = $vehicle_info;
                        $result_data['attendance_info'] = $attendance_res_arr;
                        $status = true;
                        $message = 'success';
                    }
                    
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /* Function name : markVehicleAttendance
     *  To mark vehicle Attendance
     *  @Sumit on 16 FEB 2019    
     *  @parameters : device_id,csrf_token,vehicle_id,date,attendance_data
     *  @V2R by Sumit
    */
    public function markVehicleAttendance(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $admin_id           = $request->input('admin_id');
        $vehicle_id         = $request->input('vehicle_id');
        $date               = $request->input('date');
        $attendance_data    = $request->input('attendance_data');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($vehicle_id) && !empty($date) && !empty($attendance_data) && !empty($admin_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $session = get_current_session();

                $total_present_staff = $total_present_student = $total_strength = 0;

                $attendance = VehicleAttendence::where([ ['vehicle_id',$vehicle_id], ['vehicle_attendence_date',$date] ])->first();

                if( count($attendance) != 0 ){

                    $attendance->update_by                  = $admin_id;
                    $attendance->save();

                    $attendance_user_arr = json_decode($attendance_data, true);

                    foreach ($attendance_user_arr as $attendance_user_res) {

                        if($attendance_user_res['member_type'] == 1 && $attendance_user_res['attendance_flag'] == 1){
                            $total_present_student++;
                        }
        
                        if($attendance_user_res['member_type'] == 2 && $attendance_user_res['attendance_flag'] == 1){
                            $total_present_staff++;
                        }

                        $total_strength++;
                        if( $attendance_user_res['vehicle_attend_d_id'] == '' ) {

                            $attend_details = New VehicleAttendDetails();

                            $attend_details->admin_id                 = $admin_id;
                            $attend_details->update_by                = $admin_id;
                            $attend_details->session_id               = $session['session_id'];
                            $attend_details->vehicle_id               = $vehicle_id;
                            $attend_details->vehicle_attendence_id    = $attendance->vehicle_attendence_id;
                            $attend_details->vehicle_attendence_type  = $attendance_user_res['attendance_flag'];
                            $attend_details->student_staff_id         = $attendance_user_res['member_id'];
                            $attend_details->student_staff_type       = $attendance_user_res['member_type'];
                            $attend_details->attendence_pick_up       = $attendance_user_res['attendence_pick_up'];
                            $attend_details->attendence_drop          = $attendance_user_res['attendence_drop'];
                            $attend_details->vehicle_attendence_date  = $date; 
                            $attend_details->save();
                        } else {
                            $attend_details = VehicleAttendDetails::find($attendance_user_res['vehicle_attend_d_id']);

                            $attend_details->update_by                = $admin_id;
                            $attend_details->vehicle_attendence_id    = $attendance->vehicle_attendence_id;
                            $attend_details->vehicle_attendence_type  = $attendance_user_res['attendance_flag'];
                            $attend_details->student_staff_id         = $attendance_user_res['member_id'];
                            $attend_details->student_staff_type       = $attendance_user_res['member_type'];
                            $attend_details->attendence_pick_up       = check_string_empty($attendance_user_res['attendence_pick_up']);
                            $attend_details->attendence_drop          = check_string_empty($attendance_user_res['attendence_drop']);
                            $attend_details->save();

                        }
                        
                    }

                    $attendance->total_present_student     = $total_present_student;
                    $attendance->total_present_staff       = $total_present_staff;
                    $attendance->total_strength            = $total_strength;
                    $attendance->save();

                    $status = true;
                    $message = 'Attendance update successfully!';
                    
                } else {
                    $attendance = new VehicleAttendence;

                    $attendance->admin_id       = $admin_id;
                    $attendance->update_by      = $admin_id;
                    $attendance->session_id     = $session['session_id'];
                    $attendance->vehicle_id     = $vehicle_id;
                    $attendance->vehicle_attendence_date     = $date;
                    $attendance->save();

                    $attendance_user_arr = json_decode($attendance_data, true);

                    foreach ($attendance_user_arr as $attendance_user_res) {

                        if($attendance_user_res['member_type'] == 1 && $attendance_user_res['attendance_flag'] == 1){
                            $total_present_student++;
                        }
        
                        if($attendance_user_res['member_type'] == 2 && $attendance_user_res['attendance_flag'] == 1){
                            $total_present_staff++;
                        }

                        $total_strength++;
            
                        $attend_details = New VehicleAttendDetails();

                        $attend_details->admin_id                 = $admin_id;
                        $attend_details->update_by                = $admin_id;
                        $attend_details->session_id               = $session['session_id'];
                        $attend_details->vehicle_id               = $vehicle_id;
                        $attend_details->vehicle_attendence_id    = $attendance->vehicle_attendence_id;
                        $attend_details->vehicle_attendence_type  = $attendance_user_res['attendance_flag'];
                        $attend_details->student_staff_id         = $attendance_user_res['member_id'];
                        $attend_details->student_staff_type       = $attendance_user_res['member_type'];
                        $attend_details->attendence_pick_up       = $attendance_user_res['attendence_pick_up'];
                        $attend_details->attendence_drop          = $attendance_user_res['attendence_drop'];
                        $attend_details->vehicle_attendence_date  = $date; 
                        $attend_details->save();
                    }

                    $attendance->total_present_student     = $total_present_student;
                    $attendance->total_present_staff       = $total_present_staff;
                    $attendance->total_strength            = $total_strength;
                    $attendance->save();

                    $status = true;
                    $message = 'Attendance saved successfully!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }
}
