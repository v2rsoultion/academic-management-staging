<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Hash;
use DateTime;

use App\Admin; // Admin Model
use App\Model\HomeworkGroup\HomeworkGroup; // HomeworkGroup Model
use App\Model\HomeworkGroup\HomeworkConv; // HomeworkConv Model

class HomeworkGroupController extends Controller
{

    /*  Function name : getHomeworkGroup
     *  To get homework group
     *  @Sumit on 04 DEC 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getHomeworkGroup(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $get_homework_group = HomeworkGroup::where(function($query) use($request){

                    if (!empty($request) && !empty($request->get('class_id')))
                    {
                        $query->where('class_id', "=", $request->get('class_id'));
                    }

                    if (!empty($request) && !empty($request->get('section_id')))
                    {
                        $query->where('section_id', "=", $request->get('section_id'));
                    }

                })->with('getClass')->with('getSection')->orderby('homework_group_id','desc')->paginate(20);

                if(count($get_homework_group) != 0){

                    $homework_group_info = [];

                    foreach($get_homework_group as $homework_group_res){

                        $homework_group_info[] = array(
                            'homework_group_id' => $homework_group_res->homework_group_id,
                            'homework_group_name' => $homework_group_res->homework_group_name,
                            'class_id' => $homework_group_res->getClass->class_id,
                            'class_name' => $homework_group_res->getClass->class_name,
                            'section_id' => $homework_group_res->getSection->section_id,
                            'section_name' => $homework_group_res->getSection->section_name,
                        ); 
                    }

                    $result_data = $homework_group_info;
                    $lastPage = $get_homework_group->lastPage($homework_group_res);   
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }
            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getHomeworkGroupDetails
     *  To get homework group details
     *  @Sumit on 04 DEC 2018    
     *  @parameters : device_id,csrf_token,homework_group_id
     *  @V2R by Sumit
    */
    public function getHomeworkGroupDetails(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $group_id           = $request->input('homework_group_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if( $group_id =='' ) {
                    $get_homework_group_data = HomeworkGroup::where(function($query) use($request){

                        if (!empty($request) && !empty($request->get('class_id')))
                        {
                            $query->where('class_id', "=", $request->get('class_id'));
                        }
    
                        if (!empty($request) && !empty($request->get('section_id')))
                        {
                            $query->where('section_id', "=", $request->get('section_id'));
                        }
    
                    })->with('getClass')->with('getSection')->orderby('homework_group_id','DESC')->first();
                    
                    $homework_group_id = $get_homework_group_data['homework_group_id'];
                } else {
                    $homework_group_id = $group_id;
                }
                
                $get_homework_group = HomeworkConv::where(function($query) use($request,$homework_group_id){
                    if (!empty($homework_group_id) )
                    {
                        $query->where('homework_group_id', "=", $homework_group_id);
                    }
                    if (!empty($request) && !empty($request->get('staff_id')))
                    {
                        $query->where('staff_id', "=", $request->get('staff_id'));
                    }
                    if (!empty($request) && !empty($request->get('subject_id')))
                    {
                        $query->where('subject_id', "=", $request->get('subject_id'));
                    }
                    if (!empty($request) && !empty($request->get('date')))
                    {
                        $query->whereDate('created_at', "=", $request->get('date'));
                    }
                })->with('getStaff')->with('getSubject')->orderby('h_conversation_id','DESC')->paginate(20);

                if(count($get_homework_group) != 0){

                    $homework_group_info = [];

                    foreach($get_homework_group as $homework_group_res){

                        $attachment = check_file_exist($homework_group_res->h_conversation_attechment, 'conversation_attechment');

                        $homework_group_details_info[] = array(
                            'h_conversation_id' => $homework_group_res->h_conversation_id,
                            'homework_group_id' => $homework_group_res->homework_group_id,
                            'staff_id'      => $homework_group_res->getStaff->staff_id,
                            'staff_name'    => $homework_group_res->getStaff->staff_name,
                            'subject_id'    => $homework_group_res->getSubject->subject_id,
                            'subject_name'  => $homework_group_res->getSubject->subject_name,
                            'details'       => $homework_group_res->h_conversation_text,
                            'attachment'       => $attachment,
                            'submited_date' => date('d F Y',strtotime($homework_group_res->created_at)),
                        ); 
                    }

                    $lastPage = $get_homework_group->lastPage($homework_group_res);  
                    $result_data = $homework_group_details_info;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : getHomeworkWithDetails
     *  To get homework details
     *  @Sumit on 23 JAN 2019    
     *  @parameters : device_id,csrf_token,class_id,section_id
     *  @V2R by Sumit
    */
    public function getHomeworkWithDetails(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $staff_id       = $request->input('staff_id');
        $subject_id     = $request->input('subject_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) && !empty($section_id) && !empty($staff_id) && !empty($subject_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $get_homework_group = HomeworkGroup::where(function($query) use($request){                    

                    if (!empty($request) && !empty($request->get('class_id')))
                    {
                        $query->where('class_id', "=", $request->get('class_id'));
                    }

                    if (!empty($request) && !empty($request->get('section_id')))
                    {
                        $query->where('section_id', "=", $request->get('section_id'));
                    }

                })->with('getClass')->with('getSection')
                ->with(['getMessages'=>function($q1) use ($request) {

                    if ( $request->get('subject_id') !='' )
                    {
                        $q1->where('subject_id', "=", $request->get('subject_id'));
                    }

                    if ( $request->get('staff_id') !='' )
                    {
                        $q1->where('staff_id', "=", $request->get('staff_id'));
                    }

                }])->orderby('homework_group_id','desc')->first();

                if(count($get_homework_group['getMessages']) != 0){

                    $homework_group_details_info = [];
                    foreach($get_homework_group['getMessages'] as $homework_group_details_res){
                        
                        $homework_group_details_info[] = array(
                            'h_conversation_id' => $homework_group_details_res->h_conversation_id,
                            'homework_group_id' => $homework_group_details_res->homework_group_id,
                            'staff_id'      => $homework_group_details_res->getStaff->staff_id,
                            'staff_name'    => $homework_group_details_res->getStaff->staff_name,
                            'subject_id'    => $homework_group_details_res->getSubject->subject_id,
                            'subject_name'  => $homework_group_details_res->getSubject->subject_name,
                            'details'       => $homework_group_details_res->h_conversation_text,
                            'submited_date' => date('d F Y',strtotime($homework_group_details_res->created_at)),
                        );
                    }

                    $result_data = $homework_group_details_info;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            
            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : saveStaffHomework
     *  To get homework details
     *  @Sumit on 23 JAN 2019    
     *  @parameters : device_id,csrf_token,staff_id,staff_admin_id,class_id,section_id,subject_id,homework_text
     *  @V2R by Sumit
    */
    public function saveStaffHomework(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $staff_id       = $request->input('staff_id');
        $subject_id     = $request->input('subject_id');
        $staff_admin_id = $request->input('admin_staff_id');
        $homework_text  = $request->input('homework_text');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) && !empty($section_id) && !empty($staff_id) && !empty($subject_id) && !empty($staff_admin_id) && !empty($homework_text) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $get_homework_group = HomeworkGroup::where(function($query) use($request){

                    if (!empty($request) && !empty($request->get('class_id')))
                    {
                        $query->where('class_id', "=", $request->get('class_id'));
                    }

                    if (!empty($request) && !empty($request->get('section_id')))
                    {
                        $query->where('section_id', "=", $request->get('section_id'));
                    }

                })->with('getClass')->with('getSection')->first();

                $homework_group_id = $get_homework_group->homework_group_id;

                $homework = New HomeworkConv();

                DB::beginTransaction();
                try
                {
                    $homework->admin_id             = $staff_admin_id;
                    $homework->update_by            = $staff_admin_id;
                    $homework->h_conversation_text  = $homework_text;
                    $homework->staff_id             = $staff_id;
                    $homework->subject_id           = $subject_id;
                    $homework->homework_group_id    = $homework_group_id;
                    $homework->save();
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return response()->json(['status'=>false, 'error_message'=> $error_message]);
                }
                DB::commit();

                $status = true;
                $message = 'success';

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

}

