<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Visitor\Visitor; // Visitor Model

use DB;
use Hash;
use DateTime;
use Validator;

class VisitorsController extends Controller
{

    /*  Function name : getVisitors
     *  To get visitors
     *  @Sumit on 01 JAN 2019    
     *  @parameters : device_id,csrf_token,student_id,flag
     *  @V2R by Sumit
    */
    public function getVisitors(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $name           = $request->input('name');
        $visit_date     = $request->input('visit_date');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $visitor = Visitor::where(function($query) use ($request) 
                {
                    if ( $request->get('name') !='' )
                    {
                        $query->where('name', "like", "%{$request->get('name')}%");
                    }

                    if ( $request->get('visit_date') !='' )
                    {
                        $query->where('visit_date', "=", "{$request->get('visit_date')}");
                    }

                })->orderBy('visitor_id', 'DESC')->paginate(20);

                if( count($visitor) !=0 ){

                    $visitor_info = [];

                    foreach ($visitor as $visitor_res) {

                        $visitor_info[] = array(
                            'visitor_id'    => check_string_empty($visitor_res['visitor_id']),
                            'name'          => check_string_empty($visitor_res['name']),
                            'purpose'       => check_string_empty($visitor_res['purpose']),
                            'contact'       => check_string_empty($visitor_res['contact']),
                            'email'         => check_string_empty($visitor_res['email']),
                            'visit_date'    => check_string_empty(date("d M Y", strtotime($visitor_res['visit_date']))),
                            'check_in_time' => check_string_empty(date("h:i A", strtotime($visitor_res['check_in_time']))),
                            'check_out_time' => check_string_empty(date("h:i A", strtotime($visitor_res['check_out_time']))),
                        );
                    }

                    $result_data['visitor_info'] = $visitor_info;
                    $lastPage = $visitor->lastPage($visitor_res);  
                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;

        return response()->json($result_response, 200);

    }

}

