<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\QuestionPaper\QuestionPaper; // QuestionPaper Model

use DB;
use Hash;
use DateTime;
use Validator;

class QuestionPaperController extends Controller
{

    /*  Function name : getQuestionPaper
     *  To get all question paper
     *  @Sumit on 28 DEC 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getQuestionPaper(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $subject_id     = $request->input('subject_id');
        $section_id     = $request->input('section_id');
        $flag     = $request->input('flag');
        $question_paper_id  = $request->input('question_paper_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $question_paper = QuestionPaper::where(function($query) use ($class_id,$subject_id,$question_paper_id,$section_id,$flag) 
                { 
                    if ( $class_id !='' ) 
                    {
                        $query->where('class_id', $class_id); 
                    }

                    if ( $section_id !='' ) 
                    {
                        $query->where('section_id', $section_id); 
                    }

                    if ( $subject_id !='' ) 
                    {
                        $query->where('subject_id', $subject_id); 
                    }

                    if ( $flag == 1 ) 
                    {
                        $query->where('question_paper_status', 1); 
                    }

                    if ( $question_paper_id !='' ) 
                    {
                        $query->where('question_paper_id', $question_paper_id); 
                    }
                })
                ->with('getClass')
                ->with('getSection')
                ->with('getSubjects')
                ->with('getExamType')
                ->orderBy('question_paper_id','DESC')
                ->paginate(20);

                if( count($question_paper) != 0 ){

                    $question_paper_info = [];

                    foreach ($question_paper as $question_res) {

                        $question_paper_file = '';

                        if($question_res['question_paper_file'] == ''){
                            $question_paper_file = '';
                        }else{
                            $config_document_upload_path   = \Config::get('custom.question_paper_document');
                            $doc_path   = $config_document_upload_path['display_path'].$question_res['question_paper_file'];
                            $question_paper_file      = url($doc_path);
                        }

                        $question_paper_info[] = array(
                            'question_paper_id'         => check_string_empty($question_res['question_paper_id']),
                            'question_paper_name'       => check_string_empty($question_res['question_paper_name']),
                            'class_id'                  => check_string_empty($question_res['class_id']),
                            'class_name'                => check_string_empty($question_res['getClass']['class_name']),
                            'section_id'                => check_string_empty($question_res['section_id']),
                            'section_name'              => check_string_empty($question_res['getSection']['section_name']),
                            'subject_id'                => check_string_empty($question_res['subject_id']),
                            'subject_name'              => check_string_empty($question_res['getSubjects']['subject_name']),
                            'exam_id'                   => check_string_empty($question_res['getExamType']['exam_id']),
                            'exam_type'                 => check_string_empty($question_res['getExamType']['exam_name']),
                            'question_paper_file'       => check_string_empty($question_paper_file),
                            'status'                    => check_string_empty($question_res['question_paper_status']),
                        );
                    }

                    $result_data['question_paper_info'] = $question_paper_info;
                    $lastPage = $question_paper->lastPage($question_res);  
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : saveQuestionPaper
     *  To add/edit question paper
     *  @Sumit on 28 DEC 2018    
     *  @parameters : device_id,csrf_token,question_paper_id,admin_id,paper_name,class_id,section_id,subject_id,exam_type_id,file
     *  @V2R by Sumit
    */
    public function saveQuestionPaper(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $question_paper_id  = $request->input('question_paper_id');
        $paper_name         = $request->input('paper_name');
        $class_id           = $request->input('class_id');
        $section_id         = $request->input('section_id');
        $subject_id         = $request->input('subject_id');
        $exam_type_id       = $request->input('exam_type_id');
        $file               = $request->input('file');
        $admin_id           = $request->input('admin_id');
        $staff_id           = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_id) && !empty($paper_name) && !empty($class_id) && !empty($section_id) && !empty($subject_id) && !empty($exam_type_id)  ){
            
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if (!empty($question_paper_id))
                {
                    $question_paper = QuestionPaper::find($question_paper_id);
                    $admin_id = $question_paper['admin_id'];

                    if (!$question_paper)
                    {
                        $status = false;
                        $message = 'Question Paper not found!';

                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;
                    }
                    $success_msg = 'Question Paper updated successfully!';
                }
                else
                {
                    $question_paper  = New QuestionPaper;
                    $success_msg     = 'Question Paper saved successfully!';
                }

                $validatior = Validator::make($request->all(), [
                        'paper_name'   => 'required|unique:question_papers,question_paper_name,' . $question_paper_id . ',question_paper_id',
                        'class_id'     => 'required',
                        'section_id'   => 'required',
                        'subject_id'   => 'required',
                        'exam_type_id' => 'required',
                ]);

                if ($validatior->fails())
                {   
                    //p($validatior->fails());
                    $status = false;
                    $message = 'Question Paper name already exists';

                    $result_response['status'] 	    = $status;
                    $result_response['message']     = $message;
                    $result_response['data'] 	    = $result_data;
                }
                else
                {
                    DB::beginTransaction();
                    try
                    {
                        $save_staff_id = null;
                        if($staff_id  != ""){
                            $save_staff_id = $staff_id;
                        }
                        $question_paper->admin_id                = $admin_id;
                        $question_paper->update_by               = $admin_id;
                        $question_paper->staff_id                = $save_staff_id;
                        $question_paper->question_paper_name     = $paper_name;
                        $question_paper->exam_id 	             = $exam_type_id;
                        $question_paper->class_id 	             = $class_id;
                        $question_paper->section_id 	         = $section_id;
                        $question_paper->subject_id 	         = $subject_id;

                        if ($request->hasFile('file'))
                        {
                            if (!empty($question_paper->question_paper_file))
                                {
                                    $profile = check_file_exist($question_paper->question_paper_file, 'question_paper_document');
                                    if (!empty($profile))
                                    {
                                        if(file_exists($profile)){
                                            @unlink($profile);
                                        }
                                    }
                                }

                            $file                          = $request->File('file');
                            $config_document_upload_path   = \Config::get('custom.question_paper_document');
                            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                            $ext                           = substr($file->getClientOriginalName(),-4);
                            $name                          = substr($file->getClientOriginalName(),0,-4);
                            $filename                      = $name.mt_rand(0,100000).time().$ext;
                            $file->move($destinationPath, $filename);
                            $question_paper->question_paper_file = $filename;
                        }
                        $question_paper->save();

                        $status = true;
                        $message = $success_msg;
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;
                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;
                    }
                    DB::commit();
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : deleteQuestionPaper
     *  To delete question paper
     *  @Sumit on 28 DEC 2018    
     *  @parameters : device_id,csrf_token,question_paper_id
     *  @V2R by Sumit
    */
    public function deleteQuestionPaper(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $question_paper_id  = $request->input('question_paper_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($question_paper_id) ){

        
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $question_paper = QuestionPaper::find($question_paper_id);
                if (!empty($question_paper))
                {
                    DB::beginTransaction();
                    try
                    {
                        $question_paper->delete();
                        $success_msg = "Question paper deleted successfully!";
                    }
                    catch (\Exception $e)
                    {  
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();

                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = 'No record found';
                }                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }

    /*  Function name : changeStatusQuestionPaper
     *  To change status question paper
     *  @Sumit on 01 DEC 2018    
     *  @parameters : device_id,csrf_token,id,status
     *  @V2R by Sumit
    */
    public function changeStatusQuestionPaper(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $status             = $request->input('status');
        $id                 = $request->input('id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($id) && $status !='' ){
            
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $question_paper 		= QuestionPaper::find($id);
                if ($question_paper)
                {
                    $question_paper->question_paper_status  = $status;
                    $question_paper->save();
                    $success_msg = "Question paper status update successfully!";

                    $status = true;
                    $message = $success_msg;
                }
                else
                {
                    $error_message = "Question paper not found!";
                    $status = false;
                    $message = $error_message;
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }

    

}

