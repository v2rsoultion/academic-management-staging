<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin; // Admin Model
use App\Model\School\School; // School Model
use App\Model\Student\Student; // Student Model
use App\Model\Staff\Staff; // Staff Model
use App\Model\StaffRoles\StaffRoles; // StaffRoles Model
use App\Model\StudentParentGroup\StudentParentGroup; //Model
use App\Model\StudentParentGroup\MapStudentParentGroup; //Model
use App\Model\Student\StudentParent;
use App\Model\ApiToken\ApiToken; // ApiToken Model
use App\Model\School\SchoolBoard; // SchoolBoard Model
use App\Model\Student\StudentAcademic; // StudentAcademic Model
use App\Model\Student\StudentAcademicHistory; // StudentAcademicHistory Model
use App\Model\Classes\Classes; // Classes Model
use App\Model\Section\Section; // Section Model
use App\Model\Exam\Exam; // Exam Model
use App\Model\Subject\Subject; // Subject Model
use App\Model\Designation\Designation; // Designation Model
use App\Model\Mediums\Mediums; // Mediums Model
use App\Model\Exam\ExamSchedules; // ExamSchedules Model
use App\Model\App\AppSettings; // AppSettings Model
use App\Model\Session\Session; // Session Model
use App\Model\Caste\Caste;
use App\Model\Religion\Religion;
use App\Model\Nationality\Nationality;
use App\Model\Remarks\Remarks; // Remarks Model
use App\Model\Hostel\Hostel; // Hostel Model
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // ClassTeacherAllocation Model
use App\Model\Recruitment\Job; // Job Model
use App\Model\Permissions\Permissions; // Remarks Model
use App\Model\Permissions\MasterModule; // Remarks Model

use DB;
use Hash;
use DateTime;
use Validator;

class ApiController extends Controller
{
    private function findUsername($admin_type,$username){
        if($admin_type == 1 || $admin_type == 2 || $admin_type == 3) 
        {
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
        if($admin_type == 4)
        {
            $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'enroll_no';
            request()->merge([$fieldType => $username]);
            return $fieldType;
        }
    }

    /*  Function name : generateToken
     *  To generate token with device unique ID
     *  @Pratyush on 27 NOV 2018    
     *  @parameters : device_id
     *  @V2R by Pratyush
    */
    public function generateToken(Request $request) {
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        /*
         * input variables
        */
        $device_id    = $request->input('device_id');

        if (!empty($request->input()) && !empty($device_id)){

            $token          = ApiToken::where('device_id',$device_id)->first();
            $app_settings   = AppSettings::select('android_app_version','ios_app_version','update_mandatory')->first();
            
            $app_info = array(
                    'android_app_version'   =>  check_string_empty($app_settings['android_app_version']),
                    'ios_app_version'       =>  check_string_empty($app_settings['ios_app_version']),
                    'update_mandatory'      =>  check_string_empty($app_settings['update_mandatory'])
            );
            
            if(empty($token)){

                $created_token = bcrypt(md5(md5($device_id)));
                $Add = new ApiToken;
                $Add->device_id     =  $device_id;
                $Add->csrf_token    =  $created_token;
                $Add->save();

            }else{

                $created_token = bcrypt(md5(md5($token['token_created'])));
                ApiToken::where('token_id', $token['token_id'])->update(['csrf_token'=>$created_token]);
            }
            
            $data_arr = array(
                'token'     => $created_token,
                'app_info'  => $app_info,
            );

            $result_data    = $data_arr;
            $status         = true;
            $message        = 'Success';

        }else{
            $status = false;
            $message = 'Parameter missing';
        }
        $result_response['data']    = $result_data;
        $result_response['status']  = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*  Function name : log in
     *  To login  
     *  @Pratyush on 12 Oct 2018	
     *  @parameters : device_id,csrf_token,username,password,admin_flag
    */  
    public function login(Request $request) {
        /*  
         * Variable initia  lization
         */
        $result_data = [];
        $arr_response = [];
        /*
         * input variables
         */

        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $username  	= $request->input('username');
        $password  	= $request->input('password');
        $admin_flag = $request->input('admin_flag');

        $student_id = $class_id = $section_id = $parent_id = $staff_id = $staff_role_id = $staff_role_ids = NULL ;
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($username) && !empty($password) && $admin_flag != ''){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $user_login_from = $this->findUsername($admin_flag,$username);

                if($admin_flag == 1 || $admin_flag == 2 || $admin_flag == 3){
                    if( $user_login_from == 'mobile_no' ){
                        $admin = DB::table('admins')->where('mobile_no', $username)->where('admin_type',$admin_flag)->first();
                        $error = 'Entered mobile number is not registered with us';
                    } else {
                        $admin = DB::table('admins')->where('email', $username)->where('admin_type',$admin_flag)->first();
                        $error = 'Entered email address is not registered with us';
                    }
                }

                if($admin_flag == 4){
                    if( $user_login_from == 'enroll_no' ){
                        $admin = DB::table('admins')->where('enroll_no', $username)->where('admin_type',$admin_flag)->first();
                        $error = 'There is no user registered with these details.';
                    } else {
                        $admin = DB::table('admins')->where('email', $username)->where('admin_type',$admin_flag)->first();
                        $error = 'Entered email address is not registered with us';
                    }
                }

                if (!empty($admin)) {
                    if (Hash::check($password, $admin->password)) {
                        if($admin_flag == 4){
                            if(COUNT(Student::where('reference_admin_id',$admin->admin_id)->get())) {
                                    $student    = Student::where('reference_admin_id',$admin->admin_id)
                                        ->join('student_academic_info', function($join) {
                                            $join->on('student_academic_info.student_id', '=', 'students.student_id');
                                        })->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->first();
                                    
                                    $student_id = $student->student_id;  
                                    $class_id   = $student->current_class_id; 
                                    $section_id = $student->current_section_id;
                                    $class_name   = $student['getStudentAcademic']['getCurrentClass']->class_name; 
                                    $section_name = $student['getStudentAcademic']['getCurrentSection']->section_name;
                                }
                            }
                            if($admin_flag == 2){
                                $staff    = Staff::where('reference_admin_id',$admin->admin_id)->first();
                                $staff_id = $staff['staff_id'];
                                $staff_role_id  = explode(',',$staff['staff_role_id']);
                                $staff_role_ids = $staff['staff_role_id'];
                                $profile_image  = check_file_exist($staff['staff_profile_img'], 'staff_profile');
                            }
                            if($admin_flag == 3){
                                $parent    = StudentParent::where('reference_admin_id',$admin->admin_id)->first();
                                // p($parent);
                                $parent_id = $parent['student_parent_id'];
                            }

                             // -- Notification Information start here
                            if($admin->notification_status == 0){
                                $notification_status = false;
                            } else {
                                $notification_status = true;
                            }
                            // -- Notification Information end here

                            $result_data = array(
                                'admin_id'		=> $admin->admin_id, 
                                'admin_type'	=> check_string_empty($admin->admin_type),
                                'admin_name'	=> check_string_empty($admin->admin_name),
                                'email'			=> check_string_empty($admin->email),
                                'mobile_no'		=> check_string_empty($admin->mobile_no),
                                'profile_image'	        => check_string_empty($profile_image),
                                'notification_status'	=> $notification_status,
                                'parent_id'     => $parent_id,
                                'staff_id'      => $staff_id,
                                'student_id'    => $student_id,
                                'class_id'      => $class_id,
                                'section_id'    => $section_id,
                                'class_name'      => check_string_empty($class_name),
                                'section_name'    => check_string_empty($section_name),
                                'staff_role_ids' => $staff_role_ids,
                            );
                            
                            $status = true;
                            $message = 'Record Found';
                    } else {
                        $status = false;
                        $message = 'Invalid Password';
                    }
                } else {
                    $status = false;
                    $message = $error;
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        }else{
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 		= $status;
        $result_response['message'] 	= $message;
        $result_response['data'] 		= $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getSchoolDetails
     *  To School details
     *  @Pratyush on 11 Oct 2018	
     *  @parameters : device_id,csrf_token
    */
    public function getSchoolDetails(Request $request) {
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        /*
         * input variables
        */

        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $school = School::with('getCountry')->with('getState')->with('getCity')->first();
                
            	if(!empty($school)){

                    $school_logo = check_file_exist($school['school_logo'], 'school_logo');

                    if($school['school_medium'] != ""){
                        $medium = get_medium_info($school['school_medium']);
                    } else {
                        $medium = '';
                    }
                    
                    $board_of_education   = get_boards_info($school['school_board_of_exams']);

            		// Basic Information
	            	$basic_info = array(
	            		'school_name'			=> check_string_empty($school->school_name),
	            		'reg_no'				=> check_string_empty($school->school_registration_no),
	            		'sr_no'					=> check_string_empty($school->school_sno_numbers),
	            		'no_of_student'			=> check_string_empty($school->school_total_students),
	            		'no_of_staff'			=> check_string_empty($school->school_total_staff),
	            		'class_from'			=> check_string_empty($school->school_class_from),
	            		'class_to'				=> check_string_empty($school->school_class_to),
	            		'fee_range_from'		=> check_string_empty($school->school_fee_range_from),
                        'fee_range_to'			=> check_string_empty($school->school_fee_range_to),
                        'medium'                => $medium,
                        'board_of_education'    => $board_of_education,
	            	);

			        // Branch Informatiom
			        $branch_info = array(
			        	'facilities'	=> check_string_empty($school->school_facilities),
			        	'branch_url'	=> check_string_empty($school->school_url)
			        );

			        // Other Information
			        $other_info = array(
			        	'email'			=> check_string_empty($school->school_email),
			        	'mobile'		=> check_string_empty($school->school_mobile_number),
			        	'telephone'		=> check_string_empty($school->school_telephone),
			        	'fax'			=> check_string_empty($school->school_fax_number),
			        	'description'	=> check_string_empty($school->school_description)
			        );

			        // Address Information
			        $address_info = array(
			        	'address'		=> check_string_empty($school->school_address), 		
			        	'district'		=> check_string_empty($school->school_district),
			        	'city'			=> check_string_empty($school['getCity']['city_name']),
			        	'country'	    => check_string_empty($school['getCountry']['country_name']),
			        	'pincode'		=> check_string_empty($school->school_pincode),
			        	'state'			=> check_string_empty($school['getState']['state_name']),
                    );

                    // -- Images Information start here
                    $school_img1 = check_file_exist($school['school_img1'], 'school_img');
                    $school_img2 = check_file_exist($school['school_img2'], 'school_img');
                    $school_img3 = check_file_exist($school['school_img3'], 'school_img');
                    $school_img4 = check_file_exist($school['school_img4'], 'school_img');
                    $school_img5 = check_file_exist($school['school_img5'], 'school_img');
                    $school_img6 = check_file_exist($school['school_img6'], 'school_img');

                    $images_info = array(
                        'school_img1'   => $school_img1,
                        'school_img2'   => $school_img2,
                        'school_img3'   => $school_img3,
                        'school_img4'   => $school_img4,
                        'school_img5'   => $school_img5,
                        'school_img6'   => $school_img6,
                    );
                    // -- Images Information end here
                    
                    $result_data['school_logo']     = $school_logo;
	            	$result_data['basic_info'] 		= $basic_info;
	            	$result_data['branch_info'] 	= $branch_info;
	            	$result_data['other_info'] 		= $other_info;
                    $result_data['address_info'] 	= $address_info;
                    $result_data['images_info'] 	= $images_info;
	            	$status = true;
                	$message = 'success';	
            	}else{
            		$status = false;
                	$message = 'No record found';	
            	}
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getProfileDetails
     *  To get login profile details
     *  @Pratyush on 28 NOV 2018    
     *  @parameters : device_id,csrf_token,profile_type,profile_id
     *  @V2R by Sumit
    */
    public function getProfileDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $profile_type   = $request->input('profile_type');
        $profile_id     = $request->input('profile_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // School Admin Data
                if( $profile_type == 1 ){
                    $school = Admin::where([ ['admin_type', 1], ['admin_id', $profile_id] ])->with('adminSchool')->with('adminSchool.getCountry')->with('adminSchool.getState')->with('adminSchool.getCity')->first();

                    if(!empty($school)){
                        // -- Basic Information start here
                            $school_logo = check_file_exist($school['adminSchool']['school_logo'], 'school_logo');

                            
                            $basic_info = array(
                                'admin_name'           => check_string_empty($school['adminSchool']['school_name']),
                                'admin_email'          => check_string_empty($school['adminSchool']['school_email']),
                                'admin_mobile'         => check_string_empty($school['adminSchool']['school_mobile_number']),
                                'admin_profile_image'  => $school_logo,
                            );
                        // -- Basic Information end here
                        
                        // -- Notification Information start here
                            if($school->notification_status == 0){
                                $notification_status = false;
                            } else {
                                $notification_status = true;
                            }
                            $notification_info = array(
                                'notification_status' => $notification_status
                            );
                        // -- Notification Information end here

                        $result_data  = $basic_info;
                        $result_data['notification_info']   = $notification_info;

                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else if($profile_type == 2){

                    $staff = Staff::where('reference_admin_id',$profile_id)->first();
                    $staff_admin = Admin::where('admin_id',$profile_id)->first();
                    if(!empty($staff)){

                        $staff_profile = check_file_exist($staff['staff_profile_img'], 'staff_profile');

                        $basic_info = array(
                            'admin_name'           => check_string_empty($staff['staff_name']),
                            'admin_email'          => check_string_empty($staff['staff_email']),
                            'admin_mobile'         => check_string_empty($staff['staff_mobile_number']),
                            'staff_roll'           => check_string_empty($staff['staff_role_id']),
                            'admin_profile_image'  => $staff_profile,
                        );

                        // -- Notification Information start here
                        if($staff_admin->notification_status == 0){
                            $notification_status = false;
                        } else {
                            $notification_status = true;
                        }
                        $notification_info = array(
                            'notification_status' => $notification_status
                        );
                        // -- Notification Information end here

                        $result_data  = $basic_info;
                        $result_data['notification_info']   = $notification_info;

                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else if($profile_type == 3) {
                    
                    $parent = StudentParent::where('reference_admin_id',$profile_id)->first();
                    $parent_admin = Admin::where('admin_id',$profile_id)->first();
                    if(!empty($parent)){

                        $basic_info = array(
                            'admin_name'           => check_string_empty($parent['student_login_name']),
                            'admin_email'          => check_string_empty($parent['student_login_email']),
                            'admin_mobile'         => check_string_empty($parent['student_login_contact_no']),
                            'admin_profile_image'  => '',
                        );

                        
                        // -- Notification Information start here
                        if($parent_admin->notification_status == 0){
                            $notification_status = false;
                        } else {
                            $notification_status = true;
                        }
                        $notification_info = array(
                            'notification_status' => $notification_status
                        );
                        // -- Notification Information end here

                        $result_data  = $basic_info;
                        $result_data['notification_info']   = $notification_info;

                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else if($profile_type == 4){

                    $student = Student::where('reference_admin_id',$profile_id)->first();
                    $student_admin = Admin::where('admin_id',$profile_id)->first();
                    if(!empty($student)){

                        $student_image = check_file_exist($student['student_image'], 'student_image');

                        $basic_info = array(
                            'admin_name'              => check_string_empty($student['student_name']),
                            'admin_email'              => check_string_empty($student['student_login_email']),
                            'admin_enroll_number'     => check_string_empty($student['student_enroll_number']),
                            'admin_mobile'            => '',
                            'admin_profile_image'     => $student_image,
                        );

                        // -- Notification Information start here
                        if($student_admin->notification_status == 0){
                            $notification_status = false;
                        } else {
                            $notification_status = true;
                        }
                        $notification_info = array(
                            'notification_status' => $notification_status
                        );
                        // -- Notification Information end here

                        $result_data  = $basic_info;
                        $result_data['notification_info']   = $notification_info;

                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'Incorrect Type';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getAllData
     *  To get all exam, class, section, designation, medium details
     *  @Pratyush on 29 NOV 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getAllData(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id              = $request->input('device_id');
        $csrf_token             = $request->input('csrf_token');
        $user_id                = $request->input('user_id');
        $parent_id              = $request->input('parent_id');
        $android_app_version    = $request->input('android_app_version');
        $ios_app_version        = $request->input('ios_app_version');
        $staff_id               = $request->input('staff_id');
        $fcm_device_id          = $request->input('fcm_device_id');
        $user_type              = $request->input('user_type');
        $device_type            = $request->input('device_type');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($user_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // Get all class data
                $class_data = Classes::select('class_id','class_name')->get();
                if( count($class_data) != 0 ){
                    $result_data['class_info'] = $class_data;
                } else {
                    $result_data['class_info'] = [];
                }

                // Get all section data
                $section_data = Section::select('section_id','section_name','class_id','section_intake')->get();
                if( count($section_data) != 0 ){
                    $result_data['section_info'] = $section_data;
                } else {
                    $result_data['section_info'] = [];
                }

                // Get all exam data
                $exam_data = Exam::select('exam_id','exam_name')->get();
                if( count($exam_data) != 0 ){
                    $result_data['exam_info'] = $exam_data;
                } else {
                    $result_data['exam_info'] = [];
                }

                // Get all designation data
                $designation_data = Designation::select('designation_id','designation_name')->get();
                if( count($designation_data) != 0 ){
                    $result_data['designation_info'] = $designation_data;
                } else {
                    $result_data['designation_info'] = [];
                }

                // Get all medium data
                $medium_data = Mediums::select('medium_type_id','medium_type')->get();
                if( count($medium_data) != 0 ){
                    $result_data['medium_info'] = $medium_data;
                } else {
                    $result_data['medium_data'] = [];
                }

                // Get all subjects data
                $subjects_data = Subject::select('subject_id','subject_name')->get();
                if( count($subjects_data) != 0 ){
                    $result_data['subject_info'] = $subjects_data;
                } else {
                    $result_data['subject_info'] = [];
                }

                // Get all staff data
                $staff_data = Staff::select('staff_id','staff_name')->get();
                if( count($staff_data) != 0 ){
                    $result_data['staff_info'] = $staff_data;
                } else {
                    $result_data['staff_info'] = [];
                }

                // Get all exam schedules data
                $exam_schedules_data = ExamSchedules::select('exam_schedule_id','schedule_name')->get();
                if( count($exam_schedules_data) != 0 ){
                    $result_data['exam_schedules_info'] = $exam_schedules_data;
                } else {
                    $result_data['exam_schedules_info'] = [];
                }

                // Update App Version

                if( $android_app_version !='' ){
                    Admin::where('admin_id', $user_id)->update(['android_current_app_version' => $android_app_version]);
                }

                if( $ios_app_version !='' ){
                    Admin::where('admin_id', $user_id)->update(['ios_current_app_version' => $ios_app_version]);
                }

                if( $fcm_device_id !='' ){
                    Admin::where('admin_id', $user_id)->update(['fcm_device_id' => $fcm_device_id]);
                }
                
                if( $device_type !='' ){
                    Admin::where('admin_id', $user_id)->update(['device_type' => $device_type]);
                }
                
                // Get Pages URL
                $app_settings   = AppSettings::select('comapny_page_url','privacy_policy_url','terms_condition_url')->first();
                $page_url_arr = array(
                    'comapny_page_url'      => check_string_empty($app_settings['comapny_page_url']),
                    'privacy_policy_url'    => check_string_empty($app_settings['privacy_policy_url']),
                    'terms_condition_url'   => check_string_empty($app_settings['terms_condition_url']),
                );
                $result_data['pages_urls'] = $page_url_arr;

                // Get all session data
                $session_data = Session::select('session_id','session_name','session_start_date','session_end_date')->get();
                if( count($session_data) != 0 ){
                    $result_data['session_info'] = $session_data;
                } else {
                    $result_data['session_info'] = [];
                }

                // Get all student by parent_id
                $student_data = StudentParent::where('student_parent_id', '=', $parent_id)
                ->with('getStudents')
                ->with('getStudents.getStudentAcademic.getCurrentClass')
                ->with('getStudents.getStudentAcademic.getCurrentSection')
                ->first();
                
                if($parent_id != '' &&  !empty($student_data['getStudents']) ){
                    $student_info = [];
                    foreach($student_data['getStudents'] as $student_res){

                        $profile_image = check_file_exist($student_res['student_image'], 'student_image');

                        $student_info[] = array(
                            'student_id'            => $student_res['student_id'],
                            'student_name'          => $student_res['student_name'],
                            'student_image'         => $profile_image,
                            'student_class_id'      => $student_res['getStudentAcademic']['getCurrentClass']['class_id'],
                            'student_class'         => $student_res['getStudentAcademic']['getCurrentClass']['class_name'],
                            'student_section_id'    => $student_res['getStudentAcademic']['getCurrentSection']['section_id'],
                            'student_section'       => $student_res['getStudentAcademic']['getCurrentSection']['section_name'],
                        );
                    }
                    $result_data['student_info'] = $student_info;
                } else {
                    $result_data['student_info'] = [];
                }

                // Get school basic data
                $school_data = School::where('admin_id',2)->select('school_name','school_logo','school_email','school_mobile_number','school_telephone')->first();
                if( !empty($school_data) ){

                    $school_info = array(
                        'school_name'               => check_string_empty($school_data['school_name']),
                        'school_logo'               => check_file_exist($school_data['school_logo'], 'school_logo'),
                        'school_email'              => check_string_empty($school_data['school_email']),
                        'school_mobile_number'      => check_string_empty($school_data['school_mobile_number']),
                        'school_telephone'          => check_string_empty($school_data['school_telephone']),
                        'school_description'        => check_string_empty($school_data['school_description']),
                    );

                    $result_data['school_info'] = $school_info;
                } else {
                    $result_data['school_info'] = [];
                }

                // Get all hostel data
                $hostel_data = Hostel::select('hostel_id','hostel_name','hostel_type')->where('hostel_status',1)->get();
                if( count($hostel_data) != 0 ){
                    $result_data['hostel_info'] = $hostel_data;
                } else {
                    $result_data['hostel_info'] = [];
                }

                // Get staff class & section by staff_id
                $staff_class_data = ClassTeacherAllocation::where('staff_id',$staff_id)->with('getCurrentClass')->with('getCurrentSection')->first();
                
                if( count($staff_class_data) != 0 ){
                    $staff_class_info = array(
                        'class_id'   => $staff_class_data['getCurrentClass']['class_id'],
                        'class_name' => $staff_class_data['getCurrentClass']['class_name'], 
                        'section_id' => $staff_class_data['getCurrentSection']['section_id'], 
                        'section_name' => $staff_class_data['getCurrentSection']['section_name'], 
                    );
                    $result_data['staff_class_info'] = $staff_class_info;
                } else {
                    $result_data['staff_class_info'] = [];
                }

                // Get all Job
                $job_data = Job::select('job_id','job_name')->get();
                if( count($job_data) != 0 ){
                    $result_data['job_info'] = $job_data;
                } else {
                    $result_data['job_info'] = [];
                }

                $arr_caste  = Caste::where([['caste_status', '=', 1]])->select('caste_id', 'caste_name')->get();
                if( count($arr_caste) != 0 ){
                    $result_data['arr_caste'] = $arr_caste;
                } else {
                    $result_data['arr_caste'] = [];
                }

                $arr_religion = Religion::where([['religion_status', '=', 1]])->select('religion_id', 'religion_name')->get();
                if( count($arr_religion) != 0 ){
                    $result_data['arr_religion'] = $arr_religion;
                } else {
                    $result_data['arr_religion'] = [];
                }

                $arr_natioanlity = Nationality::where([['nationality_status', '=', 1]])->select('nationality_id', 'nationality_name')->get();
                if( count($arr_natioanlity) != 0 ){
                    $result_data['arr_natioanlity'] = $arr_natioanlity;
                } else {
                    $result_data['arr_natioanlity'] = [];
                }

                $arr_permission = MasterModule::get();
                foreach ($arr_permission as $arr_permissions) {
                    
                    $modules = Permissions::whereRaw('FIND_IN_SET('.$arr_permissions->master_module_id.',permissions)')->count();

                    $arr_modules[] = array(
                        'master_module_id'         => $arr_permissions->master_module_id,
                        'module_name'         => $arr_permissions->module_name,
                        'mandatory'         => $modules,
                    );


                    if( count($arr_modules) != 0 ){
                        $result_data['arr_modules'] = $arr_modules;
                    } else {
                        $result_data['arr_modules'] = [];
                    }

                }

                


                $status = true;
                $message = 'success';

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : changePassword
     *  To get all exam, class, section, designation, medium details
     *  @Pratyush on 11 Dec 2018    
     *  @parameters : device_id,csrf_token,current_password,new_password
     *  @V2R by Sumit
    */
    public function changePassword(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id        = $request->input('device_id');
        $csrf_token       = $request->input('csrf_token');
        $current_password = $request->input('current_password');
        $new_password     = $request->input('new_password');
        $user_id          = $request->input('user_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  && !empty($current_password) && !empty($new_password) && !empty($user_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $admin  = Admin::find($user_id);

                if( !empty($admin) ){

                    if(Hash::check($request->get('current_password'), $admin['password'])){
                        $values=array(
                              'password'          => Hash::make($request->get('new_password')),
                        );
            
                        $validatior = Validator::make($request->all(), [
                            'current_password'          => 'required',
                            'new_password'              => 'required|different:current_password',
                        ], [
                            'new_password.different'    => "New Password can't be same as current password",
                        ]);
            
                        if ($validatior->fails())
                        {
                            $status  = false;
                            $message = "New Password can't be same as current password";

                        } else {
                            Admin::where('admin_id', $user_id)->update($values);
                            $status = true;
                            $message = 'Password changed successfully!';
                        }
            
                   } else {
                        $status  = false;
                        $message = 'Incorrect current password. Please try again.';
                   }

                } else {
                    $status = false;
                    $message = 'No record found';
                }
                
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : getAllRemarks
     *  To get all remarks details
     *  @Pratyush on 26 Dec 2018    
     *  @parameters : device_id,csrf_token,class_id,section_id,teacher_id,enroll_number
     *  @V2R by Sumit
    */
    public function getAllRemarks(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $remarks_info = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $subject_id     = $request->input('subject_id');
        $teacher_id     = $request->input('teacher_id');
        $enroll_number  = $request->input('enroll_number');
        $student_name   = $request->input('student_name');
        $remark_date    = $request->input('remark_date');

        if (!empty($request->input()) && !empty($csrf_token) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $remarks = Remarks::leftJoin('students', 'remarks.student_id', '=', 'students.student_id')->where(function($query) use($request, $teacher_id, $class_id, $section_id, $enroll_number, $subject_id, $student_name, $remark_date){

                    if ( !empty($teacher_id) )
                    {
                        $query->where('staff_id', $teacher_id);
                    }

                    if ( !empty($class_id) )
                    {
                        $query->where('class_id', $class_id);
                    }

                    if ( !empty($section_id) )
                    {
                        $query->where('section_id', $section_id);
                    }
                    if ( !empty($remark_date) )
                    {
                        $query->where('remark_date', $remark_date);
                    }
                    if ( !empty($subject_id) )
                    {
                        $query->where('subject_id', $subject_id);
                    }

                    if (!empty($request) && !empty($enroll_number))
                    {
                        $query->where('student_enroll_number', "like", "%{$enroll_number}%");
                    }

                    if (!empty($request) && !empty($student_name))
                    {
                        $query->where('student_name', "like", "%{$student_name}%");
                    }


                })
                ->with(['getStudent'=>function($query_student) use ($request,$enroll_number, $student_name) {

                }])
                ->with('getClass')
                ->with('getSection')
                ->with('getSubject')
                ->with('getStaff')
                ->orderBy('remark_id','DESC')
                ->paginate(20);

                if( count($remarks) !=0 ){
                    $remarks_info = [];
                    foreach ($remarks as $remarks_res) {
                        
                        $profile_image          = check_file_exist($remarks_res['getStaff']['staff_profile_img'], 'staff_profile');
                        $student_profile_image = check_file_exist($remarks_res['getStudent']['student_image'], 'student_image');
                        
                        if( !empty($remarks_res['getStudent']) ){
                            $remarks_info[] =  array(
                                "remark_id"                 => $remarks_res['remark_id'],
                                "staff_id"                  => $remarks_res['getStaff']['staff_id'],
                                "staff_name"                => $remarks_res['getStaff']['staff_name'],
                                'staff_image'               => $profile_image,
                                "student_id"                => $remarks_res['getStudent']['student_id'],
                                "student_name"              => $remarks_res['getStudent']['student_name'],
                                "student_image"             => $student_profile_image,
                                "student_roll_no"           => $remarks_res['getStudent']['student_roll_no'],
                                "student_enroll_number"     => $remarks_res['getStudent']['student_enroll_number'],
                                "subject_id"                => $remarks_res['getSubject']['subject_id'],
                                "subject_name"              => $remarks_res['getSubject']['subject_name'],
                                "class"                     => $remarks_res['getClass']['class_name'],
                                "section"                   => $remarks_res['getSection']['section_name'],
                                "remark"                    => $remarks_res['remark_text'],
                                "remark_date"               => date('d F Y',strtotime($remarks_res['remark_date'])),
                            );
                        } else {
                            $remarks_info = [];
                        }
                        
                    }

                    if( !empty($remarks_info) ){
                        $result_data['remarks_info']  = $remarks_info;
                        $lastPage = $remarks->lastPage($remarks_res);  
                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }
                } else {
                    $status = false;
                    $message = 'No record found';
                }

                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }
    
}
