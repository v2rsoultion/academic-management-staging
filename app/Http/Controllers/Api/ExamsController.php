<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Hash;
use DateTime;
use URL;
use App\Model\Student\StudentAcademic; // ExamScheduleMap Model
use App\Model\Exam\ExamSchedules; // ExamSchedules Model
use App\Model\Exam\ExamScheduleMap; // ExamScheduleMap Model
use App\Model\Staff\Staff; // Staff Model
use App\Model\Examination\MarksCriteria; // MarksCriteria Model
use App\Model\Examination\ExamMarks; // ExamMarks Model
use App\Model\Examination\ExamMap; // ExamMap Model
use App\Model\StudentSubjectManage\StudentSubjectManage; // StudentSubjectManage Model
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // SubjectSectionMapping Model
use App\Model\Student\Student;
use App\Model\Exam\ExamScheduleRoomMap;

class ExamsController extends Controller
{
    /*  Function name : getExamSchedule
     *  To get exam schedule
     *  @Sumit on 06 Dec 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getExamSchedule(Request $request){

        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $exam_schedules = ExamSchedules::where(function($query) use ($request) 
                {
                    if (!empty($request) && !empty($request->get('schedule_id'))) {
                        $query->where('exam_schedule_id', "=", $request->get('schedule_id'));
                    }

                })->with(['getExam'=>function($q1) use ($request) {

                    if (!empty($request) && !empty($request->get('exam_name'))) {
                        $q1->where('exam_name', "like", "%{$request->get('exam_name')}%");
                    }

                }])->with(['getExamScheduleMap'=>function($q) use ($request) {

                    $q->orderBy('schedule_map.class_id');

                    if (!empty($request) && !empty($request->get('class_id'))) {
                        $q->where('class_id', "=", $request->get('class_id'));
                    }
                    $q->with('getClass');

                }])->orderby('exam_schedule_id', 'DESC')->paginate(20);

                //p($exam_schedules);

                if( count($exam_schedules) !=0 ){

                    $exam_schedules_arr = [];

                    foreach($exam_schedules as $exam_schedules_res){

                        $class_arra = [];
                        $class_exist_arr = [];

                        foreach( $exam_schedules_res['getExamScheduleMap'] as $class_res ){

                            if(!in_array($class_res['getClass']['class_id'],$class_exist_arr)){

                                $class_arra[] =  array(
                                    'class_id' => $class_res['getClass']['class_id'],
                                    'class_name' => $class_res['getClass']['class_name'],
                                );
                            }

                            $class_exist_arr[] = $class_res['getClass']['class_id'];
                        }

                        if(!empty($exam_schedules_res['getExam'])) {

                            if(!empty($class_arra)) {

                                $exam_schedules_arr[] =  array(
                                    'schedule_id' => $exam_schedules_res['exam_schedule_id'],
                                    'schedule_name' => $exam_schedules_res['schedule_name'],
                                    'exam_id' => $exam_schedules_res['getExam']['exam_id'],
                                    'exam_name' => $exam_schedules_res['getExam']['exam_name'],
                                    'classes' => $class_arra,
                                );
                            }
                        }
                    }

                    if( !empty($exam_schedules_arr) ){

                        $result_data['exam_schedules_info'] = $exam_schedules_arr;
                        $lastPage = $exam_schedules->lastPage($exam_schedules_res);
                        $status = true;
                        $message = 'success';

                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {

                $status = false;
                $message = 'Invalid token';
            }

        } else {

            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getSchedule
     *  To get exam schedule
     *  @Sumit on 08 Dec 2018    
     *  @parameters : device_id,csrf_token,schedule_id
     *  @V2R by Sumit
    */
    public function getSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $getroomdata = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $schedule_id    = $request->input('schedule_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $exam_schedules = ExamScheduleMap::where(function($query) use ($request) 
                {
                    if (!empty($request) && !empty($request->get('exam_id')))
                    {
                        $query->where('exam_id', "=", $request->get('exam_id'));
                    }

                    if (!empty($request) && !empty($request->get('schedule_id')))
                    {
                        $query->where('exam_schedule_id', "=", $request->get('schedule_id'));
                    }

                    if (!empty($request) && !empty($request->get('class_id')))
                    {
                        $query->where('class_id', "=", $request->get('class_id'));
                    }

                    if (!empty($request) && !empty($request->get('section_id')))
                    {
                        $query->where('section_id', "=", $request->get('section_id'));
                    }
                })
                ->with('getClass')
                ->with('getSection')
                ->with('getExam')
                ->with('getSchedule')
                //->with('getRoomMapAll.getRoom')
                ->orderBy('class_id','ASC')
                ->groupBy('section_id')
                ->paginate(20);
                //p($exam_schedules);

                if( count($exam_schedules) !=0 ){

                    $exam_schedules_arr = [];
                    foreach($exam_schedules as $exam_schedules_res){

                        $get_class_date =  DB::select("SELECT MIN(exam_date) as start_date, MAX(exam_date) as end_date FROM `schedule_map` WHERE class_id = ".$exam_schedules_res->class_id." AND section_id = ".$exam_schedules_res->section_id." AND exam_schedule_id = ".$exam_schedules_res->exam_schedule_id." AND exam_id = ".$exam_schedules_res->exam_id." ");

                       // p($get_class_date);

                        $get_class_date = isset($get_class_date[0]) ? $get_class_date[0] : [];

                        //p($exam_schedules_res);
                        $getRoomMapAll = ExamScheduleRoomMap::where(['session_id' => $exam_schedules_res->session_id ,'exam_schedule_id' => $exam_schedules_res->exam_schedule_id, 'exam_id' => $exam_schedules_res->exam_id, 'class_id' => $exam_schedules_res->class_id, 'section_id' => $exam_schedules_res->section_id ])->get();

                        foreach ($getRoomMapAll as $getrooms) {
                            
                            $getroomdata[] = array(
                                'schedule_room_map_id'   => $getrooms->schedule_room_map_id,
                                'roll_no_from'   => $getrooms->roll_no_from,
                                'roll_no_to'   => $getrooms->roll_no_to,
                                'room_no'   => $getrooms['getRoom']->room_no,
                            );
                        }

                        $class_arra[] =  array(
                            'schedule_id'   => $exam_schedules_res->exam_schedule_id,
                            'schedule_name' => $exam_schedules_res['getSchedule']->schedule_name,
                            'exam_id'       => $exam_schedules_res->exam_id,
                            'exam_name'     => $exam_schedules_res['getExam']->exam_name,
                            'class_id'      => $exam_schedules_res->class_id,
                            'class_name'    => $exam_schedules_res['getClass']->class_name,
                            'section_id'      => $exam_schedules_res->section_id,
                            'section_name'  => $exam_schedules_res['getSection']->section_name,
                            'start_date'    => $get_class_date->start_date,
                            'end_date'      => $get_class_date->end_date,
                            'roll_number_data'      => $getroomdata,
                        );

                    }

                    $result_data['schedules_info'] = $class_arra;
                    $lastPage = $exam_schedules->lastPage($exam_schedules_res);
                    
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getSubjectOfSchedule
     *  To get exam schedule
     *  @Sumit on 08 Dec 2018    
     *  @parameters : device_id,csrf_token,schedule_id
     *  @V2R by Sumit
    */
    public function getSubjectOfSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $schedule_id    = $request->input('schedule_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($schedule_id) && !empty($class_id) && !empty($section_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $get_subjects = ExamScheduleMap::where(function($query) use ($request,$schedule_id,$class_id,$section_id) 
                {
                    if ( !empty($schedule_id) ) {
                        $query->where('exam_schedule_id', "=", $schedule_id);
                    }

                    if ( !empty($class_id) ) {
                        $query->where('class_id', "=", $class_id);
                    }

                    if ( !empty($section_id) ) {
                        $query->where('section_id', "=", $section_id);
                    }

                })->with('getSubject')->with(array('getRoomMap'=>function($q) use ($request,$schedule_id,$class_id,$section_id) {

                    $q->with(array('getRoom' => function($query) {
                        $query->select('room_no_id','room_no');
                    }));

                }))->paginate(20);

                if( count($get_subjects) !=0 ){

                    $subject_info = [];
                    $class_arra = [];

                    foreach($get_subjects as $get_subjects_res){

                        $staff_ids = explode(',',$get_subjects_res['staff_ids']);

                        $get_staff = Staff::whereIn('staff_id', $staff_ids)->select('staff_name')->get()->toArray();

                        $get_staff_name_arr = [];

                        foreach ($get_staff as $value) {
                            $get_staff_name_arr[] = $value['staff_name'];
                        }

                        $staff_name = implode(', ', $get_staff_name_arr);

                        $subject_info[] = array(
                            'schedule_id'       => $get_subjects_res['exam_schedule_id'],
                            'subject'           => $get_subjects_res['getSubject']->subject_name.' - '.$get_subjects_res['getSubject']->subject_code,
                            'staff_name'        => $staff_name,
                            'exam_date'         => date('d F Y',strtotime($get_subjects_res['exam_date'])),
                            'exam_time_from'    =>  date("g:i a", strtotime($get_subjects_res['exam_time_from'])),
                            'exam_time_to'      =>  date("g:i a", strtotime($get_subjects_res['exam_time_to'])),
                            'roll_no_from'      => $get_subjects_res['getRoomMap']['roll_no_from'],
                            'roll_no_to'        => $get_subjects_res['getRoomMap']['roll_no_to'],
                            'room_no'           => $get_subjects_res['getRoomMap']['getRoom']['room_no'],
                        ); 
                    }

                    $result_data['subject_info'] = $subject_info;
                    $lastPage = $get_subjects->lastPage($get_subjects_res);   
                    
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : getMarksOfStudents
     *  To get exam schedule
     *  @Sumit on 08 Dec 2018    
     *  @parameters : device_id,csrf_token,class_id,section_id,exam_id,subject_id
     *  @V2R by Sumit
    */
    public function getMarksOfStudents(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $exam_id        = $request->input('exam_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $student_id     = $request->input('student_id');
        $subject_id     = $request->input('subject_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($exam_id) && !empty($class_id) && !empty($section_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $get_student_list = StudentAcademic::where(function($query) use ($request,$class_id,$student_id,$section_id) 
                {
                    if ( !empty($student_id) ) {
                        $query->where('student_id', "=", $student_id);
                    }

                    if ( !empty($class_id) ) {
                        $query->where('current_class_id', "=", $class_id);
                    }

                    if ( !empty($section_id) ) {
                        $query->where('current_section_id', "=", $section_id);
                    }

                })->with('getCurrentClass')->with('getCurrentSection')->with(['getStudent' => function($query) {

                    $query->select('student_id', 'student_name', 'student_enroll_number', 'student_roll_no', 'student_parent_id', 'title_id');

                }])->with(['getStudent.getParent' => function($parent_query) {

                    $parent_query->select('student_parent_id', 'student_father_name');

                }])->with(['getStudent.getTitle' => function($parent_query) {

                    $parent_query->select('title_id', 'title_name');

                }])->paginate(20);


                $full_student_arr = []; 
                $subject_arr = [];
                $total_marks = 0;
                $obtained_marks = 0;

                foreach($get_student_list as $key => $get_student_res){

                    $st_exclude_sub = StudentSubjectManage::where([ ['class_id', '=', $class_id], ['section_id', '=', $section_id], ['student_id', '=', $get_student_res['student_id']] ])->select('subject_id')->get()->toArray();

                    $st_exclude_sub_arr = []; 

                    if( count($st_exclude_sub) !=0 ){

                        foreach($st_exclude_sub as $st_exclude_sub_res){
                            $st_exclude_sub_arr[] = $st_exclude_sub_res['subject_id'];
                        }

                    }

                    $get_student_subjects = SubjectSectionMapping::where(function($query) use ($request,$exam_id,$class_id,$section_id,$subject_id,$st_exclude_sub_arr) 
                    {

                        if ( !empty($class_id) ){
                            $query->where('subject_section_map.class_id', "=", $class_id);
                        }    

                        if ( !empty($section_id) ){
                            $query->where('subject_section_map.section_id', "=", $section_id);
                        }

                        if ( !empty($subject_id) ){
                            $query->where('subject_section_map.subject_id', "=", $subject_id);
                        }

                        $query->whereNotIn('subject_section_map.subject_id', $st_exclude_sub_arr);

                    })->with('getSubjects')

                    ->leftJoin('exam_marks', function($join) use ($class_id,$section_id,$get_student_res,$exam_id){
                        $join->on('exam_marks.subject_id', '=', 'subject_section_map.subject_id');
                        $join->where('exam_marks.exam_id', '=',$exam_id);
                        $join->where('exam_marks.class_id', '=',$class_id);
                        $join->where('exam_marks.section_id', '=',$section_id);
                        $join->where('exam_marks.student_id', '=',$get_student_res['student_id']);
                    })
                    ->leftJoin('exam_map', function($join) use ($class_id,$section_id,$get_student_res,$exam_id){
                        $join->on('exam_map.subject_id', '=', 'subject_section_map.subject_id');
                        $join->where('exam_map.exam_id', '=',$exam_id);
                        $join->where('exam_map.class_id', '=',$class_id);
                        $join->where('exam_map.section_id', '=',$section_id);
                        
                    })->leftJoin('marks_criteria', function($join) use ($class_id,$section_id,$get_student_res,$exam_id){
                        $join->on('marks_criteria.marks_criteria_id', '=', 'exam_map.marks_criteria_id');
                    })
                    ->leftJoin('grade_schemes', function($join) use ($class_id,$section_id,$get_student_res,$exam_id){
                        $join->on('grade_schemes.grade_scheme_id', '=', 'exam_map.grade_scheme_id');
                    })
                    ->get()->toArray();

                    $subject_arr= [];
                    $total_marks = 0;
                    $obtained_marks = 0;

                    foreach ($get_student_subjects as $get_student_subjects_res) {

                        if( $get_student_subjects_res['exam_map_id'] !='' && $get_student_subjects_res['marks'] !='' ) {
                            $subject_arr[] = array(
                                'subject_id'   => $get_student_subjects_res['get_subjects']['subject_id'],
                                'subject_name' => $get_student_subjects_res['get_subjects']['subject_name'],
                                'marks'        => check_string_empty($get_student_subjects_res['marks']),
                                'max_marks'    => check_string_empty($get_student_subjects_res['max_marks']),
                                'passing_marks'=> $get_student_subjects_res['passing_marks'],
                            );
    
                            if( !empty(check_string_empty($get_student_subjects_res['marks'])) ){
                                $obtained_marks    += (int)$get_student_subjects_res['marks'];
                            }
    
                            if( !empty(check_string_empty($get_student_subjects_res['max_marks'])) ){
                                $total_marks += (int)$get_student_subjects_res['max_marks'];
                            }
                        }
                        
                    }

                    if( !empty($subject_arr) ){

                        $profile_image = check_file_exist($get_student_res['getStudent']['student_image'], 'student_image');

                        $full_student_arr[] = array(
                            'student_id'            => $get_student_res['getStudent']['student_id'],
                            'student_title'         => $get_student_res['getStudent']['getTitle']['title_name'],
                            'student_name'          => $get_student_res['getStudent']['student_name'],
                            'student_image'         => $profile_image,
                            'student_enroll_number' => $get_student_res['getStudent']['student_enroll_number'],
                            'student_roll_no'       => $get_student_res['getStudent']['student_roll_no'],
                            'class'                 => $get_student_res['getCurrentClass']['class_name'],
                            'section'               => $get_student_res['getCurrentSection']['section_name'],
                            'student_father_name'   => $get_student_res['getStudent']['getParent']['student_father_name'],
                            'total_marks'           => $total_marks,
                            'obtained_marks'        => $obtained_marks,
                            'subject_info'          => $subject_arr,
                        );
                    }
                }

                if( count($full_student_arr) !=0 ){
                    $status = true;
                    $message = 'success';
                    $result_data['student_info'] =  $full_student_arr;
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }

    /*  Function name : saveStudentMarks
     *  To add/edit student marks
     *  @Sumit on 27 Dec 2018    
     *  @parameters : device_id,csrf_token,class_id,section_id,exam_id,student_id,subject_json_array
     *  @V2R by Sumit
    */
    public function saveStudentMarks(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $exam_id        = $request->input('exam_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $subject_json   = $request->input('subject_json');
        $student_id     = $request->input('student_id');
        $edit_flag      = $request->input('edit_flag');
        $admin_id       = $request->input('admin_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_id) && !empty($exam_id) && !empty($class_id) && !empty($section_id) && !empty($subject_json) && !empty($student_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $exam_schedule = ExamSchedules::where('exam_id',$exam_id)->select('exam_schedule_id','session_id')->first();

                if( $edit_flag == 0 ){

                    $subjects = json_decode($subject_json, true);

                    foreach ($subjects as $subjects_res) {

                        $exam_marks = New ExamMarks;

                        DB::beginTransaction();
                        try
                        {
                            $exam_marks->admin_id	            = $admin_id;
                            $exam_marks->update_by	            = $admin_id;
                            $exam_marks->exam_id                = $exam_id;
                            $exam_marks->class_id               = $class_id;
                            $exam_marks->section_id             = $section_id;
                            $exam_marks->student_id	            = $student_id;
                            $exam_marks->exam_schedule_id	    = $exam_schedule['exam_schedule_id'];
                            $exam_marks->session_id	            = $exam_schedule['session_id'];
                            $exam_marks->subject_id	            = $subjects_res['subject_id'];
                            $exam_marks->marks	                = $subjects_res['marks'];
                            $exam_marks->save();
                        }
                        catch (\Exception $e)
                        {
                            DB::rollback();
                            $error_message = $e->getMessage();
                            $status  = false;
                            $message = $error_message;
                        }
                        DB::commit();
                    }

                    $status = true;
                    $message = 'Marks saved successfully!';

                } else {
                    $subjects = json_decode($subject_json, true);

                    foreach ($subjects as $subjects_res) {

                        $exam_marks = ExamMarks::where([ ['exam_schedule_id', $exam_schedule['exam_schedule_id']], ['exam_id', $exam_id], ['class_id', $class_id], ['section_id', $section_id], ['student_id', $student_id], ['subject_id', $subjects_res['subject_id']] ])->first();

                        if( !empty($exam_marks) ){
                            DB::beginTransaction();
                            try
                            {
                                $exam_marks->admin_id	            = $admin_id;
                                $exam_marks->update_by	            = $admin_id;
                                $exam_marks->exam_id                = $exam_id;
                                $exam_marks->class_id               = $class_id;
                                $exam_marks->section_id             = $section_id;
                                $exam_marks->student_id	            = $student_id;
                                $exam_marks->exam_schedule_id	    = $exam_schedule['exam_schedule_id'];
                                $exam_marks->session_id	            = $exam_schedule['session_id'];
                                $exam_marks->subject_id	            = $subjects_res['subject_id'];
                                $exam_marks->marks	                = $subjects_res['marks'];
                                $exam_marks->save();
                            }
                            catch (\Exception $e)
                            {
                                DB::rollback();
                                $error_message = $e->getMessage();
                                $status  = false;
                                $message = $error_message;
                                
                                $result_response['status'] 	    = $status;
                                $result_response['message']     = $message;
                                $result_response['data'] 	    = $result_data;
                                return response()->json($result_response, 200); 
                            }
                            DB::commit();
                        } else {
                            
                            $status = false;
                            $message = 'No record found.';
                            $result_response['status'] 	    = $status;
                            $result_response['message']     = $message;
                            $result_response['data'] 	    = $result_data;
                            return response()->json($result_response, 200); 
                        }
                        
                    }

                    $status = true;
                    $message = 'Marks updated successfully!';
                }

            } else {

                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200); 

    }



    public function view_tabsheet(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $type           = $request->input('type');
        $exam_id        = $request->input('exam_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $student_id     = $request->input('student_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($type)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $exam_ids = [];
                $arr_exams  = get_all_exam_type();
                if($request->get('type') == 1){
                    $exam_ids[] = $request->get('exam_id');
                } else {
                    foreach ($arr_exams as $examKey => $exam) {
                        $exam_ids[] = $examKey;
                    }
                }
                $class_id = $request->get('class_id');
                $session    = get_current_session();

                $arr_student        = Student::where(function($query) use ($exam_id,$class_id) 
                {   
                    $query->where('student_status', 1);
                })->join('student_academic_info', function($join) use ($exam_id,$class_id,$session){
                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                    
                    if (!empty($class_id) && !empty($class_id) && $class_id != null){
                        $join->where('current_class_id', '=',$class_id);
                    }
                    $join->where('current_session_id', '=',$session['session_id']);
                })
                ->with('get_subject_with_marks')->get()->toArray();
               
                $common_exam_list = [];
                foreach ($arr_student as $student) {
                    $section_id = $student['current_section_id'];
                    $student_exam_list = [];
                    foreach ($exam_ids as $examId) {
                        $arr_subject_list = ExamMap::where(function($query) use ($request,$examId) 
                        {
                            if ( !empty($request) && $request->get('class_id') != null )
                            {
                                $query->where('exam_map.class_id', "=", $request->get('class_id'));
                            }  
                            $query->where('exam_map.exam_id', "=", $examId);
                            $query->where('marks_criteria_id', "!=", '');
                        })
                        ->join('schedule_map', function($join) use ($request,$examId){
                            $join->on('schedule_map.subject_id', '=', 'exam_map.subject_id');
                            if (!empty($request) && !empty($request->get('class_id')) && $request->get('class_id') != null){
                                $join->where('schedule_map.class_id', '=',$request->get('class_id'));
                            }
                            $join->where('schedule_map.exam_id', '=',$examId);
                            $join->where('schedule_map.publish_status', 1);
                        })
                        ->orderBy('exam_map.subject_id', 'ASC')->groupBy('exam_map.subject_id')
                        ->with('subjectInfo')
                        ->get()->toArray();

                        $get_excluded_subjects = get_excluded_subjects($class_id,$section_id,$student['student_id']);
                        
                        $arr_subjects = [];
                       
                        foreach($arr_subject_list as $subject){
                            $subjectD = [];
                            $subjectD['subject_id'] = $subject['subject_id'];
                            $subjectD['subject_name'] =  $subject['subject_info']['subject_name'].' - '.$subject['subject_info']['subject_code'];
                            $subjectD['exclude'] = 0;
                            $subjectD['marks'] = '';
                            $subjectD['attend_flag'] = 1;
                            $subjectD['marks_status'] = 0;
                            if(in_array($subject['subject_id'],$get_excluded_subjects)){
                                $subjectD['exclude'] = 1;
                            }
                            if(!empty($student['get_subject_with_marks'])) {
                                foreach ($student['get_subject_with_marks'] as $keyMarks => $marksData) {
                                    if($subject['subject_id'] == $marksData['subject_id'] && $student['student_id'] == $marksData['student_id'] && $examId == $marksData['exam_id']){
                                        
                                        $subjectD['marks'] = $marksData['marks'];
                                        $subjectD['attend_flag'] = $marksData['attend_flag'];
                                        $subjectD['marks_status'] = $marksData['marks_status'];
                                    }
                                }
                            }
                            $arr_subjects[] = $subjectD;
                        }
                        $student_exam_list[] =  array(
                            'exam_id'      => $examId,
                            'exam_name'    => get_exam_name($examId),
                            'exam_subjects'    => $arr_subjects,
                        );
                        if(COUNT($common_exam_list) < COUNT($exam_ids)){
                            $common_exam_list[] =  array(
                                'exam_id'      => $examId,
                                'exam_name'    => get_exam_name($examId),
                                'exam_subjects'    => $arr_subjects,
                            );
                        }
                        
                    }
                    $student_image = '';
                    if (!empty($student['student_image']))
                    {
                        $profile = check_file_exist($student['student_image'], 'student_image');
                        if (!empty($profile))
                        {
                            $student_image = URL::to($profile);
                        }
                    } else {
                        $student_image = "";
                    }
                    if(!empty($student['student_roll_no'])){
                        $rollNo = $student['student_roll_no'];
                    } else {
                        $rollNo = "----";
                    }
                    $arr_student_list[] = array(
                        'student_id'            => $student['student_id'],
                        'profile'               => $student_image,
                        'student_enroll_number' => $student['student_enroll_number'],
                        'student_roll_no'       => $rollNo,
                        'student_name'          => $student['student_name'],
                        'student_status'        => $student['student_status'],
                        'student_exam_list'      => $student_exam_list
                    );
                }




                if( count($arr_student_list) != 0 ){
                    $result_data = $arr_student_list;
                    $status = true;
                    $message = 'Record Found !!';
                } else {
                    $status = false;
                    $message = 'No Record Found !!';
                }

            } else {

                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']      = $status;
        $result_response['message']     = $message;
        $result_response['data']        = $result_data;
        return response()->json($result_response, 200); 

    }






    public function dfd(Request $request){
       
       
       
        
        //p($common_exam_list);
        p($arr_student_list);
        $data = array(
            'common_exam_list'    => $common_exam_list,
            'all_students'    => $arr_student_list
        );
        return view('admin-panel.exam-report-card.tabsheet-grid')->with($data);
    }



}

