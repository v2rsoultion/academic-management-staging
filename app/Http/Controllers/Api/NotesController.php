<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notes\Notes; // Notes Model
use App\Model\Section\Section; // Model

use DB;
use Hash;
use DateTime;
use Validator;

class NotesController extends Controller
{

    /*  Function name : getNotes
     *  To get all notes
     *  @Sumit on 1 JAN 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getNotes(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $class_id       = $request->input('class_id');
        $subject_id     = $request->input('subject_id');
        $note_id        = $request->input('note_id');
        $section_id     = $request->input('section_id');
        $flag           = $request->input('flag');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $notes = Notes::where(function($query) use ($class_id,$subject_id,$note_id,$section_id,$flag) 
                {
                    if ( $class_id !='' ){
                        $query->where('class_id', $class_id); 
                    }
                    
                    if ( $section_id !='' ){
                        $query->where('section_id', $section_id); 
                    }

                    if ( $subject_id !='' ){
                        $query->where('subject_id', $subject_id); 
                    }

                    if ( $flag == 1 ) 
                    {
                        $query->where('online_note_status', 1); 
                    }

                    if ( $note_id !='' ){
                        $query->where('online_note_id', $note_id); 
                    }
                })
                ->with('getClass')
                ->with('getSection')
                ->with('getSubject')
                ->orderBy('online_note_id','DESC')
                ->paginate(20);

                if( count($notes) != 0 ){

                    $notes_info = [];
                    foreach ($notes as $notes_res) {

                        $online_note_url = '';

                        if($notes_res['online_note_url'] == ''){

                            $online_note_url = '';

                        } else {

                            $config_document_upload_path   = \Config::get('custom.online_notes');
                            $doc_path   = $config_document_upload_path['display_path'].$notes_res['online_note_url'];
                            $online_note_url      = url($doc_path);
                        }

                        $notes_info[] = array(
                            'online_note_id'            => check_string_empty($notes_res['online_note_id']),
                            'online_note_name'          => check_string_empty($notes_res['online_note_name']),
                            'online_note_unit'          => check_string_empty($notes_res['online_note_unit']),
                            'online_note_topic'         => check_string_empty($notes_res['online_note_topic']),
                            'class_id'                  => check_string_empty($notes_res['class_id']),
                            'class_name'                => check_string_empty($notes_res['getClass']['class_name']),
                            'section_id'                => check_string_empty($notes_res['section_id']),
                            'section_name'              => check_string_empty($notes_res['getSection']['section_name']),
                            'subject_id'                => check_string_empty($notes_res['subject_id']),
                            'subject_name'              => check_string_empty($notes_res['getSubject']['subject_name']),
                            'online_note_url'           => check_string_empty($online_note_url),
                            'status'                    => check_string_empty($notes_res['online_note_status']),
                        );
                    }

                    $result_data['notes_info'] = $notes_info;
                    $lastPage = $notes->lastPage($notes_res);

                    $status = true;
                    $message = 'success';
                } else {
                    $status = false;
                    $message = 'No record found';
                }

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : saveNotes
     *  To add/edit online notes
     *  @Sumit on 1 JAN 2019    
     *  @parameters : device_id,csrf_token,note_id,admin_id,notes_name,class_id,section_id,subject_id,unit,file,topic
     *  @V2R by Sumit
    */

    public function saveNotes(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $note_id            = $request->input('note_id');
        $notes_name         = $request->input('notes_name');
        $class_id           = $request->input('class_id');
        $section_id         = $request->input('section_id');
        $subject_id         = $request->input('subject_id');
        $unit               = $request->input('unit');
        $topic              = $request->input('topic');
        $file               = $request->input('file');
        $admin_id           = $request->input('admin_id');
        $staff_id           = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_id) && !empty($notes_name) && !empty($class_id) && !empty($section_id) && !empty($subject_id) && !empty($topic) && !empty($unit)  ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if (!empty($note_id)){

                    $notes = Notes::find($note_id);
                    $admin_id = $notes['admin_id'];

                    if (!$notes){

                        $status = false;
                        $message = 'Notes not found!';

                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;

                        return response()->json($result_response, 200);
                    }
                    $success_msg = 'Notes updated successfully!';
                } else {
                    $notes           = New Notes;
                    $success_msg     = 'Notes saved successfully!';
                }

                $validatior = Validator::make($request->all(), [
                    'notes_name'   => 'required|unique:online_notes,online_note_name,'.$note_id.',online_note_id',
                    'class_id'     => 'required',
                    'section_id'   => 'required',
                    'subject_id'   => 'required',
                    'unit'         => 'required',
                    'topic'        => 'required',
                ]);

                if ($validatior->fails()) {

                    $status = false;
                    $message = 'Notes name already exists';

                    $result_response['status'] 	    = $status;
                    $result_response['message']     = $message;
                    $result_response['data'] 	    = $result_data;

                    return response()->json($result_response, 200);
                } else {

                    DB::beginTransaction();
                    try
                    {
                        $save_staff_id = null;

                        if($staff_id  != ""){
                            $save_staff_id = $staff_id;
                        }

                        $notes->admin_id                = $admin_id;
                        $notes->update_by               = $admin_id;
                        $notes->staff_id                = $save_staff_id;
                        $notes->online_note_name        = $notes_name;
                        $notes->online_note_unit 	    = $unit;
                        $notes->online_note_topic 	    = $topic;
                        $notes->class_id 	            = $class_id;
                        $notes->section_id 	            = $section_id;
                        $notes->subject_id 	            = $subject_id;

                        if ($request->hasFile('file')) {

                            if (!empty($notes->online_note_url)) {
                                $note_file = check_file_exist($notes->online_note_url, 'online_notes');
                                
                                if (!empty($note_file)) {
                                    if(file_exists($note_file)){
                                        @unlink($note_file);
                                    }
                                }
                            }

                            $file                          = $request->File('file');
                            $config_document_upload_path   = \Config::get('custom.online_notes');
                            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                            $ext                           = substr($file->getClientOriginalName(),-4);
                            $name                          = substr($file->getClientOriginalName(),0,-4);
                            $filename                      = $name.mt_rand(0,100000).time().$ext;

                            $file->move($destinationPath, $filename);
                            $notes->online_note_url = $filename;
                        }

                        $notes->save();

                        if( empty($note_id) ){
                            $notification = $this->send_push_notification_class($notes->notes_id,$class_id,$section_id,'notes_add',$admin_id);
                        }
                        
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;

                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;

                        return response()->json($result_response, 200);
                    }
                    DB::commit();
                }

                $status = true;
                $message = $success_msg;

            } else {
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }


    /*  Function name : deleteNotes
     *  To delete online notes
     *  @Sumit on 1 JAN 2019
     *  @parameters : device_id,csrf_token,note_id
     *  @V2R by Sumit
    */
    public function deleteNotes(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $note_id            = $request->input('note_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($note_id) ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $notes = Notes::find($note_id);
                if (!empty($notes))
                {
                    DB::beginTransaction();
                    try
                    {
                        $notes->delete();
                        $success_msg = "Notes deleted successfully!";
                    }
                    catch (\Exception $e)
                    {  
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();
                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        return response()->json($result_response, 200);
    }


    /*  Function name : changeStatusNotes
     *  To change status notes
     *  @Sumit on 01 DEC 2018    
     *  @parameters : device_id,csrf_token,id,status
     *  @V2R by Sumit
    */
    public function changeStatusNotes(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $status             = $request->input('status');
        $id                 = $request->input('id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($id) && $status !='' ){

            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                $notes 		= Notes::find($id);

                if ($notes)
                {
                    $notes->online_note_status  = $status;
                    $notes->save();
                    $success_msg = "Notes status update successfully!";

                    $status = true;
                    $message = $success_msg;
                } else {

                    $error_message = "Notes not found!";
                    $status = false;
                    $message = $error_message;
                }

            } else {

                $status = false;
                $message = 'Invalid token';
            }

        } else {
            $status = false;
            $essage = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;

        return response()->json($result_response, 200);
    }


    // Notification function
    public function send_push_notification_class($module_id,$class_id,$section_id,$notification_type,$admin_id){
        $device_ids = [];
        $section_info        = Section::where(function($query) use ($class_id,$section_id) 
        {
            $query->where('class_id',$class_id);
            $query->where('section_id',$section_id);
        })
        ->with('sectionClass')
        ->get()->toArray();
        $message = "New online content is published, click here to check.";
        foreach($section_info as $section){
            $studentTopicName = "student-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $studentTopicName = preg_replace('/\s+/', '_', $studentTopicName);
            
            $parentTopicName = "parent-".$section['section_class']['class_name']."-".$section['section_name']."_".$section['section_class']['class_id']."-".$section['section_id'];
            $parentTopicName = preg_replace('/\s+/', '_', $parentTopicName);

            $students = api_pushnotification_by_topic($module_id,$notification_type,"","","",$studentTopicName,$message,$section['class_id'],$section['section_id'],$admin_id);
            $parents = api_pushnotification_by_topic($module_id,$notification_type,"","","",$parentTopicName,$message,$section['class_id'],$section['section_id'],$admin_id);
           
        }
        
    }
}

