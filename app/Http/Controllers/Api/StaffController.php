<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use DateTime;
use Validator;

use App\Model\Staff\Staff; // Staff Model
use App\Model\StaffRoles\StaffRoles; // StaffRoles Model
use App\Model\Designation\Designation; // Designation Model
use App\Model\Staff\StaffDocument; // StaffDocument Model
use App\Model\Shift\Shift; // Shift Model
use App\Model\TimeTable\TimeTable; // TimeTable Model
use App\Model\TimeTable\TimeTableMap; // TimeTableMap Model
use App\Model\Staff\StaffLeaveApplication; // StaffLeaveApplication Model
use App\Model\Student\StudentAttendance; // StudentAttendance Model
use App\Model\Student\StudentAttendanceDetails; // StudentAttendanceDetails Model
use App\Model\Remarks\Remarks; // Remarks Model
use App\Model\HomeworkGroup\HomeworkGroup; // HomeworkGroup Model
use App\Model\HomeworkGroup\HomeworkConv; // HomeworkConv Model
use App\Model\Student\Student; // Model
use App\Model\Exam\ExamSchedules; // ExamSchedules Model
use App\Model\Exam\ExamScheduleMap; // ExamScheduleMap Model
use App\Model\Admin\Admin; // Model
use App\Model\Notice\Notice; // Notice Model
use App\Model\SubjectSectionMapping\SubjectSectionMapping; // Model 
use App\Model\SubjectTeacherMapping\SubjectTeacherMapping; // Model
use App\Model\ClassTeacherAllocation\ClassTeacherAllocation; // Model

class StaffController extends Controller
{
    /*  Function name : getStaffDetails
     *  To generate token with device unique ID
     *  @Sumit on 27 NOV 2018    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sumit
    */
    public function getStaffDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id)){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $staff = Staff::leftJoin('designations', 'staff.designation_id', '=', 'designations.designation_id')->where(function($query) use($request){

                    if (!empty($request) && !empty($request->has('staff_name')))
                    {
                        $query->where('staff.staff_name', "like", "%{$request->get('staff_name')}%");
                    }

                    if (!empty($request) && !empty($request->has('designation_id')))
                    {
                        $query->where('designations.designation_id', $request->get('designation_id'));
                    }

                })->select('staff.staff_id', 'staff.staff_name', 'staff.staff_profile_img', 'designations.designation_name')->orderBy('staff.staff_name','asc')->paginate(20);
                
                
                if(count($staff) != 0){

                    foreach($staff as $staff_res){

                        $profile_image = check_file_exist($staff_res->staff_profile_img, 'staff_profile');

                        $staff_info[] = array(
                            'staff_id'          => $staff_res->staff_id,
                            'staff_name'        => $staff_res->staff_name,
                            'staff_profile'     => $profile_image,
                            'designation_name'  => $staff_res['designation_name'],
                        );
                    }

                    $result_data = $staff_info;
                    $lastPage = $staff->lastPage($staff_res);

	            	$status = true;
                	$message = 'success';	
            	}else{
            		$status = false;
                	$message = 'No record found';
            	}
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);

    }

    /* Function name : getSingleStaffDetails
     *  To get single staff details
     *  @Sumit on 27 NOV 2018    
     *  @parameters : device_id,csrf_token,staff_id
     *  @V2R by Sumit
    */ 
    public function getSingleStaffDetails(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $staff_id   = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $staff = Staff::where('staff_id', $staff_id)->with('getTitle')->with('getDesignation')->with('getCaste')->with('getNationality')->with('getReligion')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')->with('getPermanentCity')->with('getShift')->with('getPermanentState')->with('getPermanentCountry')->first();
                
                if(!empty($staff)){
                    // -- Basic Information start here

                        // Getting gender
                        if( $staff->staff_gender == 0 ){
                            $staff_gender = 'Male';
                        } else {
                            $staff_gender = 'Female';
                        }

                        if( $staff->teaching_status == 0 ){
                            $teaching_status = 'Non Teaching';
                        } else {
                            $teaching_status = 'Teaching';
                        }

                        // Getting medium
                        // $arr_medium   = \Config::get('custom.medium_type');
                        // $medium_type = '';
                        if($staff->medium_type == 0){
                            $medium_type = "Hindi";
                        } else {
                            $medium_type = "English";
                        }

                        // Getting marital
                        // $arr_marital   = \Config::get('custom.staff_marital');
                        // $staff_marital = '';
                        if($staff->staff_marital_status == 0 ){
                            $staff_marital = "Unmarried";
                        } else {
                            $staff_marital = "Married";
                        }

                        if(check_string_empty($staff->staff_experience) == "" ){
                            $staff_experience = [];
                        } else {
                            $staff_experience = json_decode($staff->staff_experience);
                        }


                        // Getting Roles
                        $staff_role = $staff->staff_role_id;
                        if(!empty($staff_role) && $staff_role != NULL){
                            $roles_ids = explode(',', $staff_role);
                            $staff_role = StaffRoles::whereIn('staff_role_id', $roles_ids)->select('staff_role_name')->get()->toArray();

                            $staff_role_arr = [];
                            foreach($staff_role as $staff_role_value){
                                $staff_role_arr[] = $staff_role_value['staff_role_name'];
                            }
                            $staff_roles = implode(', ', $staff_role_arr);
                        }
                        
                        $profile_image = check_file_exist($staff->staff_profile_img, 'staff_profile');
                        
                        $basic_info = array(
                            'staff_id'          => check_string_empty($staff->staff_id),
                            'staff_title'       => check_string_empty($staff['getTitle']['title_name']),
                            'staff_name'        => check_string_empty($staff->staff_name),
                            'profile_image'     => $profile_image,
                            'dob'               => date('d F Y',strtotime($staff->staff_dob)),
                            'email'             => check_string_empty($staff->staff_email),
                            'mobile'            => check_string_empty($staff->staff_mobile_number),
                            'gender'            => $staff_gender,
                            'designation'       => check_string_empty($staff['getDesignation']['designation_name']),
                            'staff_roles'       => check_string_empty($staff_roles),
                            'medium_type'       => check_string_empty($medium_type),
                            'marital_status'    => check_string_empty($staff_marital),
                            'teaching_status'   => $teaching_status,
                            'shift_name'        => check_string_empty($staff['getShift']['shift_name']),
                        ); 
                    // -- Basic Information end here

                    // -- Other Information end here
                        $other_info = array(
                            'staff_father_name_husband_name'  => check_string_empty($staff->staff_father_name_husband_name),
                            'staff_father_husband_mobile_no'  => check_string_empty($staff->staff_father_husband_mobile_no),
                            'staff_mother_name'  => check_string_empty($staff->staff_mother_name),
                            'staff_blood_group'  => check_string_empty($staff->staff_blood_group),
                            'gender'            => $staff->staff_gender,
                            'marital_status'     => $staff->staff_marital_status,
                            'designation_id'  => check_string_empty($staff->designation_id),
                            'caste_id'           => check_string_empty($staff['getCaste']['caste_id']),
                            'staff_caste'        => check_string_empty($staff['getCaste']['caste_name']),
                            'nationality_id'     => check_string_empty($staff['getNationality']['nationality_id']),
                            'staff_nationality'  => check_string_empty($staff['getNationality']['nationality_name']),
                            'religion_id'        => check_string_empty($staff['getReligion']['religion_id']),
                            'staff_religions'    => check_string_empty($staff['getReligion']['religion_name']),
                            'staff_attendance_unique_id' => check_string_empty($staff->staff_attendance_unique_id),
                        );
                    // -- Other Information end here

                    // -- Temporary address Information end here
                        $temporary_address = array(
                            'staff_temporary_address'    => check_string_empty($staff->staff_temporary_address),
                            'staff_temporary_city_id'    => check_string_empty($staff['getTemporaryCity']['city_id']),
                            'staff_temporary_city'       => check_string_empty($staff['getTemporaryCity']['city_name']),
                            'staff_temporary_state_id'   => check_string_empty($staff['getTemporaryState']['state_id']),
                            'staff_temporary_state'      => check_string_empty($staff['getTemporaryState']['state_name']),
                            'staff_temporary_country_id' => check_string_empty($staff['getTemporaryCountry']['country_id']),
                            'staff_temporary_country'    => check_string_empty($staff['getTemporaryCountry']['country_name']),
                            'staff_temporary_pincode'    => check_string_empty($staff->staff_temporary_pincode),
                        );
                    // -- Temporary address Information end here

                    // -- Permanent address Information end here
                    $permanent_address = array(
                        'staff_permanent_address'    => check_string_empty($staff->staff_permanent_address),
                        'staff_permanent_city_id'    => check_string_empty($staff['getPermanentCity']['city_id']),
                        'staff_permanent_city'       => check_string_empty($staff['getPermanentCity']['city_name']),
                        'staff_permanent_state_id'   => check_string_empty($staff['getPermanentState']['state_id']),
                        'staff_permanent_state'      => check_string_empty($staff['getPermanentState']['state_name']),
                        'staff_permanent_country_id' => check_string_empty($staff['getPermanentCountry']['country_id']),
                        'staff_permanent_country'    => check_string_empty($staff['getPermanentCountry']['country_name']),
                        'staff_permanent_pincode'    => check_string_empty($staff->staff_permanent_pincode),
                    );
                    // -- Permanent address Information end here

                    $professional_info =  array(
                        'staff_aadhar'      => check_string_empty($staff->staff_aadhar),
                        'staff_UAN'         => check_string_empty($staff->staff_UAN),
                        'staff_ESIN'        => check_string_empty($staff->staff_ESIN),
                        'staff_PAN'         => check_string_empty($staff->staff_PAN),
                        'staff_qualification' => check_string_empty($staff->staff_qualification),
                        'staff_specialization' => check_string_empty($staff->staff_specialization),
                        'staff_reference' => check_string_empty($staff->staff_reference),
                        'staff_experience' => $staff_experience,
                    );

                    $result_data['basic_info'] = $basic_info;
                    $result_data['other_info'] = $other_info;
                    $result_data['professional_info'] = $professional_info;
                    $result_data['temporary_address'] = $temporary_address;
                    $result_data['permanent_address'] = $permanent_address;

	            	$status = true;
                	$message = 'success';	
            	}else{
            		$status = false;
                	$message = 'No record found';
            	}
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : getStaffShifts
     *  To get staff shifts
     *  @Sumit on 13 DEC 2018    
     *  @parameters : device_id,csrf_token,staff_id
     *  @V2R by Sumit
    */
    public function getStaffShifts(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $staff_id   = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $staff = Staff::where('staff_id', $staff_id)->select('shift_ids')->first();
                
                if(!empty($staff)){

                    $shift_ids = explode(',',$staff->shift_ids);

                    $shifts = Shift::whereIn('shift_id', $shift_ids)->get();
                    if( count($shifts) !=0 ){
                        
                        $shifts_arr = [];
                        $counter = 1;
                        foreach($shifts as $shifts_res){
                            $shifts_arr[] = array(
                                's_no'              => $counter,
                                'shift_name'        => $shifts_res->shift_name,
                                'shift_start_time'  => date('H:i A',strtotime($shifts_res->shift_start_time)),
                                'shift_end_time'    => date('H:i A',strtotime($shifts_res->shift_end_time)),
                            );
                            $counter++;
                        }

                        $result_data['shift_info'] = $shifts_arr;

                        $status = true;
                        $message = 'success';
                    } else {
                        $status = false;
                        $message = 'No record found';
                    }

            	}else{
            		$status = false;
                	$message = 'No record found';
            	}
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : getStaffSchedule
     *  To get staff schedule
     *  @Sumit on 14 DEC 2018    
     *  @parameters : device_id,csrf_token,staff_id
     *  @V2R by Sumit
    */

    public function getStaffSchedule(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $staff_id   = $request->input('staff_id');
        $arr_week_days                  = \Config::get('custom.week_days');
        
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                $time_table_map = TimeTableMap::
                join('time_tables', function($join) use ($class_id,$section_id){
                    $join->on('time_table_map.time_table_id', '=', 'time_tables.time_table_id');
                })->where([ ['staff_id', '=', $staff_id] ])
                ->select('time_table_map.*','time_tables.time_table_week_days','time_tables.class_id','time_tables.lecture_no as total_lecture','time_tables.section_id', 'time_table_map.lecture_no as map_lecture_no')
                ->orderBy('map_lecture_no', 'ASC')
                ->with('getSubject')->with('getStaff')
                ->with('getClass')
                ->with('getSection')
                ->get()
                ->groupBy('map_lecture_no')->toArray();

                $final_arr = [];

                if( !empty($time_table_map) ){
                    foreach($time_table_map as $lecture_key => $time_table){

                        $total_lecture = $time_table['total_lecture'];
                        $arr_days = [];

                        if(!empty($time_table)){

                            foreach ($time_table as $key => $time_table_data) {

                                $lecture = $time_table_data['lecture_no'];
                                $total_lecture = $time_table_data['total_lecture'];
                                $start_time = date("g:i a", strtotime($time_table_data['start_time']));
                                $end_time = date("g:i a", strtotime($time_table_data['end_time']));
                                $class = $time_table_data['get_class']['class_name'];
                                $section = $time_table_data['get_section']['section_name'];

                                $subject_name = '';

                                $staff_name = '';

                                $lunch_break = '';

                                if($time_table_data['lunch_break'] == '1'){
                                    $lunch_break = "Lunch Break";
                                } else {
                                    $subject_name   = $time_table_data['get_subject']['subject_name'];
                                    $staff_name   = $time_table_data['get_staff']['staff_name'];
                                }

                                $arr_days[] = array(
                                    'day'           => $arr_week_days[$time_table_data['day']],
                                    'day_key'       => $time_table_data['day'],
                                    'lecture_no'    => $lecture,
                                    'start_time'    => $start_time,
                                    'end_time'      => $end_time,
                                    'class'         => $class,
                                    'section'       => $section,
                                    'subject_name'  => $subject_name,
                                    'staff_name'    => $staff_name,
                                    'lunch_break'   => $lunch_break
                                );
                            }
                        }

                        usort($arr_days, function($a, $b) {
                            return $a['day_key'] <=> $b['day_key'];
                        });

                        $final_arr[] = array(
                            'day_keys'      => $time_table[0]['time_table_week_days'],
                            'for_lecture'   => $lecture_key,
                            'day_info'      => $arr_days
                        );
                    }

                    $result_data['staff_schedule'] = $final_arr;
                    $status = true;
                    $message = 'success';

                } else {
                    $status = false;
                    $message = 'No record found';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']  = $status;
        $result_response['message'] = $message;
        $result_response['total_lecture'] = $total_lecture;
        $result_response['data']    = $result_data;
        
        return response()->json($result_response, 200);
    }


    public function getStaffSchedule_old(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $staff_id   = $request->input('staff_id');
        $arr_week_days                  = \Config::get('custom.week_days');
        
        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                $time_table_map = TimeTableMap::where([ ['staff_id', '=', $staff_id] ])->with('getSubject')->with('getTimeTable.timeTableClass')->with('getTimeTable.timeTableSection')->get()->toArray();
                
                if( !empty($time_table_map) ){

                    $final_arr = [];
                    foreach ($arr_week_days as $dayKey => $days) {
                    
                        $arr_lecture = [];
                            for ($lecture = 1; $lecture <= 8; $lecture++) {
                                
                                $lecture_info = '';
                                $start_time = '';
                                $end_time = '';
                                $class = '';
                                $section = '';
                                $subject_name = '';
                                foreach ($time_table_map as $time_table_map_res) {
                                    if($time_table_map_res['lecture_no'] == $lecture && $time_table_map_res['day'] == $dayKey){
                                        $start_time     = $time_table_map_res['start_time'];
                                        $end_time       = $time_table_map_res['end_time'];
                                        $class          = $time_table_map_res['get_time_table']['time_table_class']['class_name'];
                                        $section        = $time_table_map_res['get_time_table']['time_table_section']['section_name'];
                                        $subject_name   = $time_table_map_res['get_subject']['subject_name'];
                                    } 
                                }
                                $arr_lecture[] = array(
                                    'lecture_no'    => $lecture,
                                    'start_time'    => date("g:i a", strtotime($start_time)),
                                    'end_time'      => date("g:i a", strtotime($end_time)),
                                    'class'         => $class,
                                    'section'       => $section,
                                    'subject_name'  => $subject_name
                                );
                            }

                        $final_arr[] = array(
                            'day' => $days,
                            'arr_lecture' => $arr_lecture
                        );
                    }
                    $result_data['staff_schedule'] = $final_arr;
                    $status = true;
                    $message = 'success';

                }   else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);
    }

    /* All Staff App API
    ------------------------------------ */

    /* Function name : staffProfile
     *  To get staff profile
     *  @Sumit on 12 JAN 2019    
     *  @parameters : device_id,csrf_token,staff_id
     *  @V2R by Sumit
    */
    public function staffProfile(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $staff_id   = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $staff = Staff::where('staff_id', $staff_id)->with('getTitle')->with('getDesignation')->with('getCaste')->with('getNationality')->with('getReligion')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')->with('getPermanentCity')->with('getPermanentState')->with('getPermanentCountry')->first();
                
                if(!empty($staff)){
                    // -- Basic Information start here

                        // Getting gender
                        if( $staff->staff_gender == 0 ){
                            $staff_gender = 'Male';
                        } else {
                            $staff_gender = 'Female';
                        }

                        // Getting medium
                        $arr_medium   = \Config::get('custom.medium_type');
                        $medium_type = '';
                        if($staff->medium_type === 0 || $staff->medium_type !='' ){
                            $medium_type = $arr_medium[$staff->medium_type];
                        }

                        // Getting marital
                        $arr_marital   = \Config::get('custom.staff_marital');
                        $staff_marital = '';
                        if($staff->staff_marital_status === 0 || !empty($staff->staff_marital_status)){
                            $staff_marital = $arr_marital[$staff->staff_marital_status];
                        }

                        // Getting Roles
                        $staff_role = $staff->staff_role_id;
                        if(!empty($staff_role) && $staff_role != NULL){
                            $roles_ids = explode(',', $staff_role);
                            $staff_role = StaffRoles::whereIn('staff_role_id', $roles_ids)->select('staff_role_name')->get()->toArray();

                            $staff_role_arr = [];
                            foreach($staff_role as $staff_role_value){
                                $staff_role_arr[] = $staff_role_value['staff_role_name'];
                            }
                            $staff_roles = implode(', ', $staff_role_arr);
                        }


                        $arr_medium      = get_all_mediums();
                        
                        $profile_image = check_file_exist($staff->staff_profile_img, 'staff_profile');
                        
                        $basic_info = array(
                            'staff_id'          => check_string_empty($staff->staff_id),
                            'staff_title'       => check_string_empty($staff['getTitle']['title_name']),
                            'staff_name'        => check_string_empty($staff->staff_name),
                            'profile_image'     => $profile_image,
                            'dob'               => date('d F Y',strtotime($staff->staff_dob)),
                            'email'             => check_string_empty($staff->staff_email),
                            'mobile'            => check_string_empty($staff->staff_mobile_number),
                            'gender'            => $staff_gender,
                            'designation'       => check_string_empty($staff['getDesignation']['designation_name']),
                            'staff_roles'       => check_string_empty($staff_roles),
                            'medium_type'       => check_string_empty($arr_medium[$staff['medium_type']]),
                            'marital_status'    => check_string_empty($staff_marital),
                        ); 
                    // -- Basic Information end here

                    // -- Other Information end here
                        $other_info = array(
                            'staff_father_name_husband_name'  => check_string_empty($staff->staff_father_name_husband_name),
                            'staff_father_husband_mobile_no'  => check_string_empty($staff->staff_father_husband_mobile_no),
                            'staff_mother_name'  => check_string_empty($staff->staff_mother_name),
                            'staff_blood_group'  => check_string_empty($staff->staff_blood_group),
                            'staff_caste'        => check_string_empty($staff['getCaste']['caste_name']),
                            'staff_nationality'  => check_string_empty($staff['getNationality']['nationality_name']),
                            'staff_religions'    => check_string_empty($staff['getReligion']['religion_name']),
                            'staff_attendance_unique_id' => check_string_empty($staff->staff_attendance_unique_id),
                        );
                    // -- Other Information end here

                    // -- Temporary address Information end here
                        $temporary_address = array(
                            'staff_temporary_address' => check_string_empty($staff->staff_temporary_address),
                            'staff_temporary_city'    => check_string_empty($staff['getTemporaryCity']['city_name']),
                            'staff_temporary_state'   => check_string_empty($staff['getTemporaryState']['state_name']),
                            'staff_temporary_country' => check_string_empty($staff['getTemporaryCountry']['staff_temporary_county']),
                            'staff_temporary_pincode' => check_string_empty($staff->staff_temporary_pincode),
                        );
                    // -- Temporary address Information end here

                    // -- Permanent address Information end here
                    $permanent_address = array(
                        'staff_permanent_address' => check_string_empty($staff->staff_permanent_address),
                        'staff_permanent_city'    => check_string_empty($staff['getPermanentCity']['city_name']),
                        'staff_permanent_state'   => check_string_empty($staff['getPermanentState']['state_name']),
                        'staff_permanent_country' => check_string_empty($staff['getPermanentCountry']['country_name']),
                        'staff_permanent_pincode' => check_string_empty($staff->staff_permanent_pincode),
                    );
                    // -- Permanent address Information end here

                    $professional_info =  array(
                        'staff_aadhar'      => check_string_empty($staff->staff_aadhar),
                        'staff_UAN'         => check_string_empty($staff->staff_UAN),
                        'staff_ESIN'        => check_string_empty($staff->staff_ESIN),
                        'staff_PAN'         => check_string_empty($staff->staff_PAN),
                        'staff_qualification' => check_string_empty($staff->staff_qualification),
                        'staff_specialization' => check_string_empty($staff->staff_specialization),
                        'staff_reference' => check_string_empty($staff->staff_reference),
                        'staff_experience' => check_string_empty($staff->staff_experience),
                    );

                    $result_data['basic_info'] = $basic_info;
                    $result_data['other_info'] = $other_info;
                    $result_data['professional_info'] = $professional_info;
                    $result_data['temporary_address'] = $temporary_address;
                    $result_data['permanent_address'] = $permanent_address;

	            	$status = true;
                	$message = 'success';	
            	}else{
            		$status = false;
                	$message = 'No record found';
            	}
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	= $status;
        $result_response['message'] = $message;
        $result_response['data'] 	= $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : changeStaffLeaveStatus
     *  To get staff profile
     *  @Sumit on 12 JAN 2019    
     *  @parameters : device_id,csrf_token,staff_id,leave_id
     *  @V2R by Sumit
    */
    public function changeStaffLeaveStatus(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $leave_id       = $request->input('leave_id');
        $status         = $request->input('status');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($leave_id) && $status !='' ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                // Staff Leaves Data
                $staff_leave  = StaffLeaveApplication::find($leave_id);

                if( !empty($staff_leave) ){
                    DB::beginTransaction();
                    try
                    {
                        $staff_leave->staff_leave_status  = $status;
                        $staff_leave->save();
                        $status = true;
                        $message = 'success';
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();

                        $status = false;
                        $message = $error_message;
                    }
                    DB::commit();
                } else {
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }
    
    /* Function name : marksAttendace
     *  To marks student attendace
     *  @Sumit on 12 JAN 2019    
     *  @parameters : device_id,csrf_token,admin_staff_id,class_id,section_id,date,student_data
     *  @V2R by Sumit
    */
    public function marksAttendace(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $admin_staff_id = $request->input('admin_staff_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $date           = $request->input('date');
        $attendance_data   = $request->input('attendance_data');
        $student_attendance_id = $request->input('student_attendance_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_staff_id) && !empty($class_id) && !empty($section_id) && !empty($date) && !empty($attendance_data)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $total_present_students = $total_absent_students = 0;
                $admin_id  = $admin_staff_id;

                $attendance_data_arr = json_decode($attendance_data, true);

                $session    = get_current_session();
                
                if (!empty($student_attendance_id)) {

                    $attendance = StudentAttendance::find($student_attendance_id);
                    $admin_id   = $attendance['admin_id'];
                    if (!$attendance)
                    {
                        $status = false;
                        $message = 'Attendance not found!';

                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;
                    }
                    $success_msg = 'Attendancee updated successfully!';
                }
                else {
                    $attendance     = New StudentAttendance;
                    $success_msg    = 'Attendance saved successfully!';
                }
                DB::beginTransaction();
                try
                {
                    $attendance = StudentAttendance::where('student_attendance_date', $date)->first();
                    
                    if(empty($attendance)){
                        $attendance     = New StudentAttendance;
                    }

                    $attendance->admin_id       = $admin_id;
                    $attendance->update_by      = $admin_id;
                    $attendance->session_id 	= $session['session_id'];
                    $attendance->class_id 	    = $class_id;
                    $attendance->section_id 	= $section_id;
                    $attendance->student_attendance_date 	= $date;
                    $attendance->save();
                    
                    foreach($attendance_data_arr as $details){ 
                        if($details['attendance'] == 0){
                            $total_absent_students++;
                        }
                        if($details['attendance'] == 1){
                            $total_present_students++;
                        }
                        if(isset($details['student_attend_d_id']) ) {
                            
                            $student_attend = StudentAttendanceDetails::where('student_attend_d_id', $details['student_attend_d_id'])->first();

                            if( !empty($student_attend) ){
                                $student_attend->admin_id               = $admin_id;
                                $student_attend->update_by              = $admin_id;
                                $student_attend->student_attendance_id  = $attendance->student_attendance_id;
                                $student_attend->session_id             = $session['session_id'];
                                $student_attend->class_id 	            = $class_id;
                                $student_attend->section_id 	        = $section_id;
                                $student_attend->student_id             = $details['student_id'];
                                $student_attend->student_attendance_date    = $date;
                                $student_attend->student_attendance_type    = $details['attendance'];
                                $student_attend->save();

                                $success_msg = 'Attendancee updated successfully!';
                            } else {

                                $status = false;
                                $message = 'Student Attendance not found!';

                                $result_response['status'] 	    = $status;
                                $result_response['message']     = $message;
                                $result_response['data'] 	    = $result_data;
                            }
                            
                        } else {
                            $student_attend = New StudentAttendanceDetails();
                            $student_attend->admin_id               = $admin_id;
                            $student_attend->update_by              = $admin_id;
                            $student_attend->student_attendance_id  = $attendance->student_attendance_id;
                            $student_attend->session_id             = $session['session_id'];
                            $student_attend->class_id 	            = $class_id;
                            $student_attend->section_id 	        = $section_id;
                            $student_attend->student_id             = $details['student_id'];
                            $student_attend->student_attendance_date = $date;
                            $student_attend->student_attendance_type = $details['attendance'];
                            $student_attend->save();
                        
                            if($details['attendance'] == 0){
                                $type = "Absent";
                            } else {
                                $type = "Present";
                            }

                            $send_notification  = $this->send_push_notification_attendence($student_attend->student_attend_d_id,"",$details['student_id'],"","","",'student_attendance',$type,$student_attend->student_attendance_date);


                        }

                    }
                    $attendance->total_present_student 	= $total_present_students;
                    $attendance->total_absent_student 	= $total_absent_students;
                    $attendance->save();
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $status = false;
                    $message = $error_message;
                }
                DB::commit();

                $status = true;
                $message = $success_msg;

                $result_response['status'] 	    = $status;
                $result_response['message']     = $message;
                $result_response['data'] 	    = $result_data;
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : staffAddRemarks
     *  To add remarks
     *  @Sumit on 12 JAN 2019    
     *  @parameters : device_id,csrf_token,admin_staff_id,class_id,section_id,subject_id,staff_id,remark_text,student_id
     *  @V2R by Sumit
    */
    public function staffAddRemarks(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $admin_staff_id = $request->input('admin_staff_id');
        $staff_id       = $request->input('staff_id');
        $subject_id     = $request->input('subject_id');
        $student_id     = $request->input('student_id');
        $class_id       = $request->input('class_id');
        $section_id     = $request->input('section_id');
        $remark_text    = $request->input('remark_text');
        $remark_id      = $request->input('remark_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($admin_staff_id) && !empty($staff_id) && !empty($subject_id) && !empty($student_id) && !empty($remark_text) && !empty($class_id) && !empty($section_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $admin_id  = $admin_staff_id;
                $session   = get_current_session();

                if (!empty($remark_id))
                {
                    $remark = Remarks::find($remark_id);
                    $admin_id = $remark['admin_id'];
                    if (!$remark)
                    {
                        $status = false;
                        $message = 'Remark not found!';

                        $result_response['status'] 	    = $status;
                        $result_response['message']     = $message;
                        $result_response['data'] 	    = $result_data;
                    }
                    $success_msg = 'Remark updated successfully!';
                }
                else
                {
                    $remark     	= New Remarks;
                    $success_msg 	= 'Remark saved successfully!';
                }
                $validatior = Validator::make($request->all(), [
                    'student_id'   => 'required',
                    'subject_id'   => 'required',
                    'remark_text'   => 'required',
                ]);
                if ($validatior->fails())
                {
                    $status = false;
                    $message = 'Parameters missing';
                }
                else
                {
                    DB::beginTransaction();
                    try
                    {
                        $remark->admin_id       = $admin_id;
                        $remark->update_by      = $admin_id;
                        $remark->session_id 	= $session['session_id'];
                        $remark->class_id 	    = $class_id;
                        $remark->section_id 	= $section_id;
                        $remark->staff_id 	    = $staff_id;
                        $remark->student_id 	= $student_id;
                        $remark->subject_id 	= $subject_id;
                        $remark->remark_text 	= $remark_text;
                        $remark->remark_date 	= date('Y-m-d');
                        $remark->save();
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;
                    }
                    DB::commit();
                    $status = true;
                    $message = $success_msg;
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : getStaffSubjects
     *  To get staff subjects
     *  @Sumit on 12 JAN 2019    
     *  @parameters : device_id,csrf_token,staff_id
     *  @V2R by Sumit
    */
    public function getStaffSubjects(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $staff_id       = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $subjects = get_staff_subjects($request,$staff_id);
                
                if( !empty($subjects) ){

                    $status = true;
                    $message = 'success';
                    $result_data['subjec_info'] = $subjects;

                } else {

                    $status = false;
                    $message = 'No Record found!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : saveStaffHomework
     *  To add remarks
     *  @Sumit on 12 JAN 2019    
     *  @parameters : device_id,csrf_token,staff_id,subject_id,homework_message,admin_staff_id,class_id,section_id
     *  @V2R by Sumit
    */
    public function saveStaffHomework(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $admin_staff_id     = $request->input('admin_staff_id');
        $staff_id           = $request->input('staff_id');
        $subject_id         = $request->input('subject_id');
        $class_id           = $request->input('class_id');
        $section_id         = $request->input('section_id');
        $homework_message   = $request->input('homework_message');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id)  && !empty($admin_staff_id)  && !empty($subject_id)  && !empty($class_id)  && !empty($section_id) && !empty($homework_message)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $group = HomeworkGroup::where([ ['class_id', "=", $class_id], ['section_id', "=", $section_id] ])->with('getClass')->with('getSection')->first();
                
                if ( !empty($group) ){

                    $homework = New HomeworkConv();
                    DB::beginTransaction();
                    try
                    {
                        $homework->admin_id             = $admin_staff_id;
                        $homework->update_by            = $admin_staff_id;
                        $homework->h_conversation_text  = $homework_message;
                        $homework->staff_id             = $staff_id;
                        $homework->subject_id           = $subject_id;
                        $homework->homework_group_id    = $group['homework_group_id'];
                        $homework->save();

                        if ($request->hasFile('h_conversation_attechment'))
                        {
                            $file                          = $request->File('h_conversation_attechment');
                            $config_document_upload_path   = \Config::get('custom.conversation_attechment');
                            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                            $ext                           = substr($file->getClientOriginalName(),-4);
                            $name                          = substr($file->getClientOriginalName(),0,-4);
                            if($ext == 'docx'){
                                $ext = ".docx";
                            }
                            if($ext == 'jpeg'){
                                $ext = ".jpeg";
                            }
                            if($ext == 'xlsx'){
                                $ext = ".xlsx";
                            }
                            $filename          = $name.$ext;
                            $file->move($destinationPath, $filename);
                            $homework->h_conversation_attechment = $filename;
                            $homework->save();
                        }
                
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;
                    }
                    DB::commit();
                    $status = true;
                    $message = 'success';
                }

            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /*  Function name : saveStaffLeave
     *  To add & edit staff leave
     *  @Sumit on 17 Jan 2018    
     *  @parameters : device_id,csrf_token,staff_id,reason,date_from,date_to,attachment
     *  @V2R by Sumit
    */
    public function saveStaffLeave(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $leave_id       = $request->input('leave_id');
        $staff_id       = $request->input('staff_id');
        $reason         = $request->input('reason');
        $date_from      = $request->input('date_from');
        $date_to        = $request->input('date_to');
        $attachment     = $request->input('attachment');
        $admin_id       = $request->input('admin_id');
        
        if (!empty($request->input()) && !empty($csrf_token)  && !empty($admin_id) && !empty($staff_id) && !empty($reason) && !empty($date_from) && !empty($date_to) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                if (!empty($leave_id))
                {
                    $leave_application = StaffLeaveApplication::find($leave_id);

                    $admin_id = $leave_application['admin_id'];

                    if (!$leave_application)
                    {
                        $status = false;
                        $message = 'Leave Application not found!';
                    }
                    $success_msg = 'Leave Application updated successfully!';
                }
                else
                {
                    $leave_application  = New StaffLeaveApplication;
                    $success_msg 		= 'Leave Application saved successfully!';
                }

                $validatior = Validator::make($request->all(), [
                        'reason'   	    => 'required',
                        'date_from'     => 'required|unique:staff_leaves,staff_leave_from_date,' . $staff_id . ',staff_id',
                        'date_to'   	=> 'required',
                ]);

                if ($validatior->fails())
                {
                    $status = false;
                    $message = $validatior;
                }
                else
                {
                    DB::beginTransaction();
                    try
                    {
                        $leave_application->admin_id      			   = $admin_id;
                        $leave_application->update_by      			   = $admin_id;
                        $leave_application->staff_id     			   = $staff_id;
                        $leave_application->staff_leave_reason 	       = $reason;
                        $leave_application->staff_leave_from_date      = $date_from;
                        $leave_application->staff_leave_to_date 	   = $date_to;
                        if ($request->hasFile('attachment'))
                        {
                            if (!empty($leave_id)){
                                $attachment = check_file_exist($leave_application->staff_leave_attachment, 'staff_leave_attachment');
                                if (!empty($attachment))
                                {
                                    unlink($attachment);
                                } 
                            }
                            $file                          = $request->File('attachment');
                            $config_document_upload_path   = \Config::get('custom.staff_leave_document_file');
                            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                            $ext                           = substr($file->getClientOriginalName(),-4);
                            $name                          = substr($file->getClientOriginalName(),0,-4);
                            $filename                      = $name.mt_rand(0,100000).time().$ext;
                            $file->move($destinationPath, $filename);
                            $leave_application->staff_leave_attachment = $filename;
                        }
                        $leave_application->save();

                        if( empty($leave_id) ){
                            $staff_name = get_staff_name($staff_id);
                            $send_notification  = $this->send_push_notification_admin($leave_application->staff_leave_id,"staff_leave",$staff_name,$staff_id,'staff_leave',$admin_id);
                        }
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        $status = false;
                        $message = $error_message;
                    }
                    DB::commit();
                }
                $status = true;
                $message = $success_msg;
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
        
    }

    /* Function name : staffDeleteRemarks
     *  To delete remarks
     *  @Sumit on 23 JAN 2019    
     *  @parameters : device_id,csrf_token,remarks_id
     *  @V2R by Sumit
    */
    public function staffDeleteRemarks(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];

        /*
         * input variables
        */
        $device_id          = $request->input('device_id');
        $csrf_token         = $request->input('csrf_token');
        $remark_id          = $request->input('remark_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($remark_id) ){
            
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $remarks = Remarks::find($remark_id);
                if (!empty($remarks))
                {
                    DB::beginTransaction();
                    try
                    {
                        $remarks->delete();
                        $success_msg = "Remark deleted successfully!";
                    }
                    catch (\Exception $e)
                    {  
                        DB::rollback();
                        $error_message = "Sorry we can't delete it because it's already in used!!";
                    }
                    DB::commit();
                    $status = true;
                    $message = $success_msg;
                } else {
                    $status = false;
                    $message = 'No record found';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }

    /* Function name : staffExamDuty
     *  To get exam duty
     *  @Sumit on 23 JAN 2019    
     *  @parameters : device_id,csrf_token,staff_id
     *  @V2R by Sumit
    */
    public function staffExamDuty(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id      = $request->input('device_id');
        $csrf_token     = $request->input('csrf_token');
        $staff_id       = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id)  ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $exam = ExamScheduleMap::where(function($query) use ($request,$staff_id) 
                {
                    if (!empty($request) && !empty($request->get('exam_date')))
                    {
                        $query->where('exam_date', "=", "{$request->get('exam_date')}");
                    }

                    $query->whereRaw('FIND_IN_SET('.$staff_id.',staff_ids)');

                })
                ->whereHas('getExam', function($query_student) use ($request) {

                    if (!empty($request) && !empty($request->get('exam_id')))
                    {
                        $query_student->where('exam_id', "=", "{$request->get('exam_id')}");
                    }

                })

                ->whereHas('getClass', function($query_student) use ($request) {

                    if (!empty($request) && !empty($request->get('class_id')))
                    {
                        $query_student->where('class_id', "=", "{$request->get('class_id')}");
                    }

                })

                ->with('getExam')->with('getSubject')->with('getClass')->with('getSection')->with('getRoomMap.getRoom')->paginate(20);

                if( count($exam) != 0 ){

                    $exam_info  = [];
                    foreach($exam as $exam_res){

                        $exam_info[] = array(
                            'exam_id'           =>  $exam_res['getExam']['exam_id'],
                            'exam_name'         =>  $exam_res['getExam']['exam_name'],
                            'subject_id'        =>  $exam_res['getSubject']['subject_id'],
                            'subject_name'      =>  $exam_res['getSubject']['subject_name'],
                            'class_id'          =>  $exam_res['getClass']['class_id'],
                            'class_name'        =>  $exam_res['getClass']['class_name'],
                            'section_id'        =>  $exam_res['getSection']['section_id'],
                            'section_name'      =>  $exam_res['getSection']['section_name'],
                            'room_no'           =>  $exam_res['getRoomMap']['getRoom']['room_no'],
                            'section_name'      =>  $exam_res['getSection']['section_name'],
                            'section_name'      =>  $exam_res['getSection']['section_name'],
                            'exam_date'         =>  $exam_res['exam_date'],
                            'exam_time_from'    =>  date("g:i a", strtotime($exam_res['exam_time_to'])),
                            'exam_time_to'      =>  date("g:i a", strtotime($exam_res['exam_time_to'])),
                        );
                    }

                    $status = true;
                    $message = 'success';
                    $result_data['exam_info'] = $exam_info;
                    $lastPage = $exam->lastPage($exam_res);  

                } else {

                    $status = false;
                    $message = 'No Record found!';
                }
                
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status'] 	    = $status;
        $result_response['message']     = $message;
        $result_response['data'] 	    = $result_data;
        
        return response()->json($result_response, 200);
    }
    
    // Notification function
    public function send_push_notification_admin($module_id,$module_name,$staff_name,$staff_id,$notification_type,$admin_id){
        $device_ids = [];
        
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereRaw('FIND_IN_SET(1,staff_role_id)')->select('staff_name', 'staff_id','reference_admin_id')->groupBy('staff_id')->with('getStaffAdminInfo')->get();
        foreach ($arr_staff_list as $staff) {
            if($staff['getStaffAdminInfo']->fcm_device_id != "" && $staff['getStaffAdminInfo']->notification_status == 1){
             //   $device_ids[] = $staff['getStaffAdminInfo']->fcm_device_id;
                $staff_admin_id = $staff['getStaffAdminInfo']->admin_id;
            }
        }

        $admin_info    = Admin::where(function($query) use ($user_id) 
        {
            $query->where('admin_type',1);
        })
        ->get()->toArray();

        foreach($admin_info as $admin){
            if($admin['fcm_device_id'] != "" && $admin['notification_status'] == 1){
                $device_ids[] = $admin['fcm_device_id'];
            }
        }

       // p($device_ids);
        $message = $staff_name." applied for leave";
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => Null,
            'parent_admin_id' => Null,
            'staff_admin_id' => $staff_admin_id,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = api_save_notification($info);
        $send_notification = api_pushnotification($module_id,$notification_type,$staff_id,'','',$device_ids,$message);
    }


    // Notification function
    public function send_push_notification($module_id,$module_name,$staff_name,$staff_id,$notification_type,$admin_id){
        $device_ids = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->whereRaw('FIND_IN_SET(1,staff_role_id)')->select('staff_name', 'staff_id','reference_admin_id')->groupBy('staff_id')->with('getStaffAdminInfo')->get();
        foreach ($arr_staff_list as $staff) {
            if($staff['getStaffAdminInfo']->fcm_device_id != "" && $staff['getStaffAdminInfo']->notification_status == 1){
                $device_ids[] = $staff['getStaffAdminInfo']->fcm_device_id;
                $staff_admin_id = $staff['getStaffAdminInfo']->admin_id;
            }
        }

        //p($admin_id);
        $message = $staff_name." applied for leave";
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => Null,
            'parent_admin_id' => Null,
            'staff_admin_id' => $staff_admin_id,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = api_save_notification($info);
        $send_notification = api_pushnotification($module_id,$notification_type,$staff_id,'','',$device_ids,$message);
    }

    // Attendence Notification function
    public function send_push_notification_attendence($module_id,$staff_id,$student_id,$subject_name,$class_id,$section_id,$notification_type,$attendance_type,$attendance_date){
        $device_ids = [];
        $student_info        = Student::where(function($query) use ($student_id) 
        {
            if (!empty($student_id) && !empty($student_id) && $student_id != null){
                $query->where('students.student_id', $student_id);
            }
            $query->where('students.student_status', 1);
        })
        ->join('student_academic_info', function($join) use ($class_id,$section_id){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            if (!empty($section_id) && !empty($section_id) && $section_id != null){
                $join->where('current_section_id', '=',$section_id);
            }
            if (!empty($class_id) && !empty($class_id) && $class_id != null){
                $join->where('current_class_id', '=',$class_id);
            }
        })
        ->join('student_parents', function($join){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
        })
        ->select('students.student_id','students.student_name','students.student_parent_id','students.reference_admin_id')
        ->with('getAdminInfo')->with('getParent.getParentAdminInfo')
        ->get()->toArray();
        $student_info = isset($student_info[0]) ? $student_info[0] : [];

        if($student_info['get_admin_info']['fcm_device_id'] != ""  && $student_info['get_admin_info']['notification_status'] == 1){
            if($notification_type != 'student_attendance'){
                $student_admin_id = $student_info['get_admin_info']['admin_id'];
                $device_ids[] = $student_info['get_admin_info']['fcm_device_id'];
            }
        }
        if($student_info['get_parent']['get_parent_admin_info']['fcm_device_id'] != "" && $student_info['get_parent']['get_parent_admin_info']['notification_status'] == 1){
            $device_ids[] = $student_info['get_parent']['get_parent_admin_info']['fcm_device_id'];
            $parent_admin_id = $student_info['get_parent']['get_parent_admin_info']['admin_id'];
        }
        if($notification_type == 'student_dairy_remark_by_staff'){
            $message = $student_info['student_name']." received remark in ".$subject_name;
        }  else if($notification_type == 'student_attendance'){
            $message = $student_info['student_name']." is ".$attendance_type." in school on ".date("d F Y", strtotime($attendance_date)).".";
        }
        $info = array(
            'admin_id' => $admin_id,
            'update_by' => $admin_id,
            'notification_via' => 0,
            'notification_type' => $notification_type,
            'session_id' => $session['session_id'],
            'class_id' => Null,
            'section_id' => Null,
            'module_id' => $module_id,
            'student_admin_id' => $student_admin_id,
            'parent_admin_id' => $parent_admin_id,
            'staff_admin_id' => Null,
            'school_admin_id' => Null,
            'notification_text' => $message,
        );
        $save_notification = save_notification($info);
        $send_notification = pushnotification($module_id,$notification_type,'','','',$device_ids,$message);
    }


    /*  Function name : getCommunicationStaff
     *  To generate token with device unique ID
     *  @Sandeep on 25 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function getCommunicationStaff(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id   = $request->input('device_id');
        $csrf_token  = $request->input('csrf_token');
        $class_id    = $request->input('class_id');
        $section_id  = $request->input('section_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) && !empty($section_id)){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){

                $subject_id          = SubjectSectionMapping::where('class_id',$class_id)->where('section_id',$section_id)->get();
                $class_allocation    = ClassTeacherAllocation::where('class_id',$class_id)->where('section_id',$section_id)->first();

                $staff_teacher   = Staff::leftJoin('designations', 'staff.designation_id', '=', 'designations.designation_id')->where(function($query) use($request){

                    if (!empty($request) && !empty($request->get('staff_name')))
                    {
                        $query->where('staff.staff_name', "like", "%{$request->get('staff_name')}%");
                    }

                    if (!empty($request) && !empty($request->get('designation_id')))
                    {
                        $query->where('designations.designation_id', $request->get('designation_id'));
                    }
                })->where('staff.staff_id',$class_allocation['staff_id'])->first();

                $profile_image = check_file_exist($staff_teacher->staff_profile_img, 'staff_profile');

                $staff_data_subject  = $staff_data = [];

                if(!empty($staff_teacher)){
                    $staff_data_subject[] = array(
                        'staff_id'          => $staff_teacher->staff_id,
                        'staff_name'        => $staff_teacher->staff_name,
                        'designation_id'    => $staff_teacher->designation_id,
                        'designation_name'  => $staff_teacher->designation_name,
                        'staff_profile'     => $profile_image,
                        'staff_type'        => "Class Teacher",
                    );
                }

                foreach ($subject_id as $subject_ids) {


                    $staff_id = SubjectTeacherMapping::where(function($query) use ($subject_ids, $staff_teacher) 
                    {
                        $query->where('class_id',$subject_ids['class_id'])->where('section_id',$subject_ids['section_id'])->where('subject_id',$subject_ids['subject_id']);                
                        if (!empty($staff_teacher->staff_id)) {
                            $query->where('staff_ids', "!=", $staff_teacher->staff_id);
                        }
                    })->first(); 

                    if($staff_id != ""){
                        
                        $staff   = Staff::leftJoin('designations', 'staff.designation_id', '=', 'designations.designation_id')->where(function($query) use($request){

                            if (!empty($request) && !empty($request->get('staff_name')))
                            {
                                $query->where('staff.staff_name', "like", "%{$request->get('staff_name')}%");
                            }

                            if (!empty($request) && !empty($request->get('designation_id')))
                            {
                                $query->where('designations.designation_id', $request->get('designation_id'));
                            }
                        })->where('staff_id',$staff_id['staff_ids'])->first();

//                        $staff = Staff::where('staff_id',$staff_id['staff_ids'])->with('getDesignation')->first()->toArray();

                        $profile_image = check_file_exist($staff['staff_profile_img'], 'staff_profile');

                        if(!empty($staff)){
                            $staff_data[] = array(
                                'staff_id'          => check_string_empty($staff['staff_id']),
                                'staff_name'        => check_string_empty($staff['staff_name']),
                                'designation_id'    => check_string_empty($staff['designation_id']),
                                'designation_name'  => $staff['designation_name'],
                                'staff_role_id'     => check_string_empty($staff['staff_role_id']),
                                'staff_profile'     => $profile_image,
                                'staff_type'        => "Subject Teacher",
                            );
                        }
                    }
                }

                $staff_data = array_unique($staff_data);
                $staff_data = array_merge($staff_data_subject,$staff_data);

                if(count($staff_data) != 0){

                    $result_data = $staff_data;
                    $lastPage = 1;

                    $status = true;
                    $message = 'success';   
                }else{
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']  = $status;
        $result_response['message'] = $message;
        $result_response['data']    = $result_data;
        
        return response()->json($result_response, 200);

    }


    /*  Function name : getCommunicationParent
     *  To generate token with device unique ID
     *  @Sandeep on 25 FEB 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function getCommunicationParent(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id   = $request->input('device_id');
        $csrf_token  = $request->input('csrf_token');
        $class_id    = $request->input('class_id');
        $section_id  = $request->input('section_id');
        $enroll_number    = $request->input('enroll_number');
        $roll_no  = $request->input('roll_no');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($class_id) && !empty($section_id)){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){


                $arr_students  = Student::join('student_academic_info', function($join) use ($request,$class_id,$section_id,$enroll_number,$roll_no){
                    $join->on('student_academic_info.student_id', '=', 'students.student_id');
                    if (!empty($request) && !empty($section_id) && $section_id != null){
                        $join->where('current_section_id', '=',$section_id);
                    }
                    if (!empty($request) && !empty($enroll_number) && $enroll_number != null){
                        $join->where('student_enroll_number', '=',$enroll_number);
                    }
                    if (!empty($request) && !empty($roll_no) && $roll_no != null){
                        $join->where('student_roll_no', '=',$roll_no);
                    }
                    if (!empty($request) && !empty($class_id) && $class_id != null){
                        $join->where('current_class_id', '=',$class_id);
                    }
                })->with('getStudentAcademic')
                ->join('student_parents', function($join) use ($request){
                    $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                    if (!empty($request) && !empty($request->get('father_name')))
                    {
                        $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                    }
                })->orderBy('students.student_id', 'DESC')
                ->with('getParent')->with(['getRTEInfo' => function($query) use ($request,$session){
                    
                    $query->where('session_id', "=", $session['session_id']);
                    
                    if (!empty($request) && !empty($class_id))
                    {
                        $query->where('class_id', "=", $class_id);
                    }
                    if (!empty($request) && !empty($section_id))
                    {
                        $query->where('section_id', "=", $section_id);
                    }
                }])->groupby('student_parents.student_parent_id')->paginate(20);
               
//                p($arr_students);
                if (!empty($arr_students[0]->student_id))
                {
                    foreach ($arr_students as $student)
                    {
                        $arr_student_list[] = array(
                            'student_id'            => check_string_empty($student->student_id),
                            'profile'               => check_string_empty($student_image),
                            'student_enroll_number' => check_string_empty($student->student_enroll_number),
                            'student_roll_no'       => check_string_empty($student->student_roll_no),
                            'student_name'          => check_string_empty($student->student_name),
                            'student_status'        => check_string_empty($student->student_status),
                            'medium_type'           => check_string_empty($arr_medium[$student->medium_type]),
                            'rte_status'            => check_string_empty($arr_rte_status[$student->rte_apply_status]),
                            'rte_apply_status'      => check_string_empty($student->rte_apply_status),
                            'section_id'            => check_string_empty($section_id),
                            'student_parent_id'     => check_string_empty($student['getParent']->student_parent_id),
                            'student_father_name'   => check_string_empty($student['getParent']->student_father_name),
                            'student_father_mobile_number'   => check_string_empty($student['getParent']->student_father_mobile_number),
                        );
                    }
                }


                


                if(count($arr_student_list) != 0){

                    $result_data = $arr_student_list;
                    $lastPage = $arr_students->lastPage($student);

                    $status = true;
                    $message = 'success';   
                }else{
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['last_page']   = $lastPage;
        $result_response['status']  = $status;
        $result_response['message'] = $message;
        $result_response['data']    = $result_data;
        
        return response()->json($result_response, 200);

    }


    /*  Function name : updateStaff
     *  To generate token with device unique ID
     *  @Sandeep on 27 March 2019    
     *  @parameters : device_id,csrf_token
     *  @V2R by Sandeep
    */
    public function update_staff(Request $request){
        /*
         * Variable initialization
        */
        $result_data = [];
        $arr_response = [];
        $lastPage = 0;

        /*
         * input variables
        */
        $device_id  = $request->input('device_id');
        $csrf_token = $request->input('csrf_token');
        $staff_id   = $request->input('staff_id');

        if (!empty($request->input()) && !empty($csrf_token) && !empty($device_id) && !empty($staff_id) ){
            // API Auth
            if(validate_token($device_id,$csrf_token) == true){
                
                $staff  = Staff::find($request->input('staff_id'));
                
                if(count($staff) != 0){

                    $staff->staff_name                      = $request->input('staff_name');
                    $staff->designation_id                  = $request->input('designation_id');
                    $staff->staff_email                     = $request->input('staff_email');
                    $staff->staff_gender                    = $request->input('staff_gender');
                    $staff->staff_dob                       = $request->input('staff_dob');
                    $staff->staff_mobile_number             = $request->input('staff_mobile_number');
                    $staff->staff_marital_status            = $request->input('staff_marital_status');
                    $staff->staff_father_name_husband_name  = $request->input('staff_father_name_husband_name');
                    $staff->staff_mother_name               = $request->input('staff_mother_name');
                    $staff->staff_father_husband_mobile_no  = $request->input('staff_father_husband_mobile_no');
                    $staff->staff_blood_group               = $request->input('staff_blood_group');
                    $staff->caste_id                        = $request->input('caste_id');
                    $staff->nationality_id                  = $request->input('nationality_id');
                    $staff->religion_id                     = $request->input('religion_id');
                    $staff->staff_temporary_address         = $request->input('staff_temporary_address');
                    $staff->staff_temporary_county          = $request->input('staff_temporary_county');
                    $staff->staff_temporary_state           = $request->input('staff_temporary_state');
                    $staff->staff_temporary_city            = $request->input('staff_temporary_city');
                    $staff->staff_temporary_pincode         = $request->input('staff_temporary_pincode');
                    $staff->staff_permanent_address         = $request->input('staff_permanent_address');
                    $staff->staff_permanent_county          = $request->input('staff_permanent_county');
                    $staff->staff_permanent_state           = $request->input('staff_permanent_state');
                    $staff->staff_permanent_city            = $request->input('staff_permanent_city');
                    $staff->staff_permanent_pincode         = $request->input('staff_permanent_pincode');
                    $staff->save();

                    $staff = Staff::where('staff_id', $staff_id)->with('getTitle')->with('getDesignation')->with('getCaste')->with('getNationality')->with('getReligion')->with('getTemporaryCity')->with('getTemporaryState')->with('getTemporaryCountry')->with('getPermanentCity')->with('getPermanentState')->with('getPermanentCountry')->first();

                    if(!empty($staff)){
                    // -- Basic Information start here

                        // Getting gender
                        if( $staff->staff_gender == 0 ){
                            $staff_gender = 'Male';
                        } else {
                            $staff_gender = 'Female';
                        }

                        // Getting medium
                        $arr_medium   = \Config::get('custom.medium_type');
                        $medium_type = '';
                        if($staff->medium_type === 0 || $staff->medium_type !='' ){
                            $medium_type = $arr_medium[$staff->medium_type];
                        }

                        // Getting marital
                        $arr_marital   = \Config::get('custom.staff_marital');
                        $staff_marital = '';
                        if($staff->staff_marital_status === 0 || !empty($staff->staff_marital_status)){
                            $staff_marital = $arr_marital[$staff->staff_marital_status];
                        }

                        // Getting Roles
                        $staff_role = $staff->staff_role_id;
                        if(!empty($staff_role) && $staff_role != NULL){
                            $roles_ids = explode(',', $staff_role);
                            $staff_role = StaffRoles::whereIn('staff_role_id', $roles_ids)->select('staff_role_name')->get()->toArray();

                            $staff_role_arr = [];
                            foreach($staff_role as $staff_role_value){
                                $staff_role_arr[] = $staff_role_value['staff_role_name'];
                            }
                            $staff_roles = implode(', ', $staff_role_arr);
                        }


                        $arr_medium      = get_all_mediums();
                        
                        $profile_image = check_file_exist($staff->staff_profile_img, 'staff_profile');
                        
                        $basic_info = array(
                            'staff_id'          => check_string_empty($staff->staff_id),
                            'staff_title'       => check_string_empty($staff['getTitle']['title_name']),
                            'staff_name'        => check_string_empty($staff->staff_name),
                            'profile_image'     => $profile_image,
                            'dob'               => date('d F Y',strtotime($staff->staff_dob)),
                            'email'             => check_string_empty($staff->staff_email),
                            'mobile'            => check_string_empty($staff->staff_mobile_number),
                            'gender'            => $staff_gender,
                            'designation'       => check_string_empty($staff['getDesignation']['designation_name']),
                            'staff_roles'       => check_string_empty($staff_roles),
                            'medium_type'       => check_string_empty($arr_medium[$staff['medium_type']]),
                            'marital_status'    => check_string_empty($staff_marital),
                        ); 
                    // -- Basic Information end here

                    // -- Other Information end here
                        $other_info = array(
                            'staff_father_name_husband_name'  => check_string_empty($staff->staff_father_name_husband_name),
                            'staff_father_husband_mobile_no'  => check_string_empty($staff->staff_father_husband_mobile_no),
                            'staff_mother_name'  => check_string_empty($staff->staff_mother_name),
                            'staff_blood_group'  => check_string_empty($staff->staff_blood_group),
                            'staff_caste'        => check_string_empty($staff['getCaste']['caste_name']),
                            'staff_nationality'  => check_string_empty($staff['getNationality']['nationality_name']),
                            'staff_religions'    => check_string_empty($staff['getReligion']['religion_name']),
                            'staff_attendance_unique_id' => check_string_empty($staff->staff_attendance_unique_id),
                        );
                    // -- Other Information end here

                    // -- Temporary address Information end here
                        $temporary_address = array(
                            'staff_temporary_address' => check_string_empty($staff->staff_temporary_address),
                            'staff_temporary_city'    => check_string_empty($staff['getTemporaryCity']['city_name']),
                            'staff_temporary_state'   => check_string_empty($staff['getTemporaryState']['state_name']),
                            'staff_temporary_country' => check_string_empty($staff['getTemporaryCountry']['staff_temporary_county']),
                            'staff_temporary_pincode' => check_string_empty($staff->staff_temporary_pincode),
                        );
                    // -- Temporary address Information end here

                    // -- Permanent address Information end here
                        $permanent_address = array(
                            'staff_permanent_address' => check_string_empty($staff->staff_permanent_address),
                            'staff_permanent_city'    => check_string_empty($staff['getPermanentCity']['city_name']),
                            'staff_permanent_state'   => check_string_empty($staff['getPermanentState']['state_name']),
                            'staff_permanent_country' => check_string_empty($staff['getPermanentCountry']['country_name']),
                            'staff_permanent_pincode' => check_string_empty($staff->staff_permanent_pincode),
                        );
                        // -- Permanent address Information end here

                        $professional_info =  array(
                            'staff_aadhar'      => check_string_empty($staff->staff_aadhar),
                            'staff_UAN'         => check_string_empty($staff->staff_UAN),
                            'staff_ESIN'        => check_string_empty($staff->staff_ESIN),
                            'staff_PAN'         => check_string_empty($staff->staff_PAN),
                            'staff_qualification' => check_string_empty($staff->staff_qualification),
                            'staff_specialization' => check_string_empty($staff->staff_specialization),
                            'staff_reference' => check_string_empty($staff->staff_reference),
                            'staff_experience' => check_string_empty($staff->staff_experience),
                        );

                        $result_data['basic_info'] = $basic_info;
                        $result_data['other_info'] = $other_info;
                        $result_data['professional_info'] = $professional_info;
                        $result_data['temporary_address'] = $temporary_address;
                        $result_data['permanent_address'] = $permanent_address;

                        $status = true;
                        $message = 'success';   
                    }else{
                        $status = false;
                        $message = 'No record found';
                    }
  
                }else{
                    $status = false;
                    $message = 'No record found';
                }
            }else{
                $status = false;
                $message = 'Invalid token';
            }
        } else {
            $status = false;
            $message = 'Parameters missing';
        }

        $result_response['status']  = $status;
        $result_response['message'] = $message;
        $result_response['data']    = $result_data;
        
        return response()->json($result_response, 200);

    }




}
