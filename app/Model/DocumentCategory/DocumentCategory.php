<?php

namespace App\Model\DocumentCategory;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    protected $table      = 'document_category';
    protected $primaryKey = 'document_category_id';
}
