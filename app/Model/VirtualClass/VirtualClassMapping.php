<?php

namespace App\Model\VirtualClass;

use Illuminate\Database\Eloquent\Model;

class VirtualClassMapping extends Model
{
    protected $table      = 'virtual_class_map';
    protected $primaryKey = 'virtual_class_map_id';
}
