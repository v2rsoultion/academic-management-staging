<?php

namespace App\Model\School;

use Illuminate\Database\Eloquent\Model;

class SchoolBoard extends Model
{
    protected $table      = 'school_boards';
    protected $primaryKey = 'board_id';
}
