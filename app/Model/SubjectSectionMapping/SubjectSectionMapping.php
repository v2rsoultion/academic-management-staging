<?php

namespace App\Model\SubjectSectionMapping;

use Illuminate\Database\Eloquent\Model;

class SubjectSectionMapping extends Model
{
    protected $table      = 'subject_section_map';
    protected $primaryKey = 'subject_section_map_id';

    public function getSubjects()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }

    public function getExamMap()
    {
        return $this->belongsTo('App\Model\Examination\ExamMap', 'subject_id');
    }
}
