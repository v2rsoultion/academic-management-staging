<?php

namespace App\Model\Facility;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $table      = 'facilities';
    protected $primaryKey = 'facility_id';
}
