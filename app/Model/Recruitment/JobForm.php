<?php

namespace App\Model\Recruitment;

use Illuminate\Database\Eloquent\Model;

class JobForm extends Model
{
    protected $table      = 'job_forms';
    protected $primaryKey = 'job_form_id';

    public function GetJob()
    {
        return $this->belongsTo('App\Model\Recruitment\Job','job_id');
    }
}
