<?php

namespace App\Model\Recruitment;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table      = 'job';
    protected $primaryKey = 'job_id';

    public function get_medium()
    {
        return $this->belongsTo('App\Model\Mediums\Mediums','job_medium_type');
    }

    public function get_remaining_jobs()
    {
        return $this->hasMany('App\Model\Recruitment\RecruitmentForm','job_id');
    }

    public function approved_jobs()
    {
        return $this->hasMany('App\Model\Recruitment\RecruitmentForm','job_id');
    }

    public function disapproved_jobs()
    {
        return $this->hasMany('App\Model\Recruitment\RecruitmentForm','job_id');
    }

    public function getForm()
    {
        return $this->belongsTo('App\Model\Recruitment\JobForm','job_id');
    }

    public function getAppliedCadidates()
    {
        return $this->hasMany('App\Model\Recruitment\RecruitmentForm','job_id');
    }
}
