<?php

namespace App\Model\Certificate;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table = "certificates";
    protected $primaryKey = "certificate_id";
}
