<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class StaffDocument extends Model
{
    protected $table      = 'staff_documents';
    protected $primaryKey = 'staff_document_id';

    public function getDocumentCategory()
    {
        return $this->belongsTo('App\Model\DocumentCategory\DocumentCategory','document_category_id');
    }

}
