<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class StaffAttendance extends Model
{
    protected $table      = 'staff_attendance';
    protected $primaryKey = 'staff_attendance_id';
}
