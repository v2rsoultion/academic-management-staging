<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class StaffAttendanceDetails extends Model
{
    protected $table      = 'staff_attend_details';
    protected $primaryKey = 'staff_attend_d_id';
}
