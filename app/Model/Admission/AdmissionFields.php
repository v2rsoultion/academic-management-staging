<?php

namespace App\Model\Admission;

use Illuminate\Database\Eloquent\Model;

class AdmissionFields extends Model
{
    protected $table      = 'admission_fields';
    protected $primaryKey = 'admission_field_id';
}
