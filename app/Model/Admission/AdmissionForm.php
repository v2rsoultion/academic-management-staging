<?php

namespace App\Model\Admission;

use Illuminate\Database\Eloquent\Model;

class AdmissionForm extends Model
{
    protected $table      = 'admission_forms';
    protected $primaryKey = 'admission_form_id';

    public function getSessionInfo()
    {
        return $this->belongsTo('App\Model\Session\Session','session_id');
    }

    public function getClassInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }

    public function getBrochureInfo()
    {
        return $this->belongsTo('App\Model\Brochure\Brochure','brochure_id');
    }

    public function getFormFieldsInfo()
    {
        return $this->belongsTo('App\Model\Admission\AdmissionFields','admission_form_id');
    }

    public function AdmissionDataInfo()
    {
        return $this->hasMany('App\Model\Admission\AdmissionData', 'admission_form_id');
    }
    
}
