<?php

namespace App\Model\Examination;

use Illuminate\Database\Eloquent\Model;

class MarksCriteria extends Model
{
    protected $table      = 'marks_criteria';
    protected $primaryKey = 'marks_criteria_id';
}
