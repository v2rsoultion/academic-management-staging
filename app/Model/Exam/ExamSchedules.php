<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;

class ExamSchedules extends Model
{
    protected $table      = 'exam_schedules';
    protected $primaryKey = 'exam_schedule_id';

    public function getExam()
    {
        return $this->belongsTo('App\Model\Exam\Exam', 'exam_id');
    }

    public function getExamScheduleMap()
    {
        return $this->hasMany('App\Model\Exam\ExamScheduleMap', 'exam_schedule_id');
    }

    

}
