<?php

namespace App\Model\Competition;

use Illuminate\Database\Eloquent\Model;

class CompetitionMapping extends Model
{
    protected $table      = 'competition_map';
    protected $primaryKey = 'student_competition_map_id';

    // For student Info
    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }

    // For competition Info
    public function getCompetition()
    {
        return $this->belongsTo('App\Model\Competition\Competition','competition_id');
    }

    // For student Academic Info
    public function getStudentAcademic()
    {
        return $this->belongsTo('App\Model\Student\StudentAcademic','student_id');
    }

}
