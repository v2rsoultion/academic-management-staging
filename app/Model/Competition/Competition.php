<?php

namespace App\Model\Competition;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $table      = 'competitions';
    protected $primaryKey = 'competition_id';
}
