<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentCC extends Model
{
    protected $table      = 'student_cc';
    protected $primaryKey = 'student_cc_id';
    // For Class
    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }
    // For Section
    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }
    // For Session
    public function getSession()
    {
        return $this->belongsTo('App\Model\Session\Session', 'session_id');
    }
    // For Student
    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }
}
