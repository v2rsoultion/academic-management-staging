<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentGroupMap extends Model
{
    protected $table      = 'st_parent_group_map';
    protected $primaryKey = 'st_parent_group_map_id';
}
