<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentAttendanceDetails extends Model
{
    protected $table      = 'student_attend_details';
    protected $primaryKey = 'student_attend_d_id';

}
