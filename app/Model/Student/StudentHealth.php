<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentHealth extends Model
{
    protected $table      = 'student_health_info';
    protected $primaryKey = 'student_health_info_id';
    
    public function getStudent()
    {
        return $this->hasOne('App\Model\Student\Student','student_id');
    }
}
