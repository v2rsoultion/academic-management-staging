<?php

namespace App\Model\PrepaidAccount;

use Illuminate\Database\Eloquent\Model;

class PrepaidAccount extends Model
{
    protected $table = "prepaid_accounts";
    protected $primaryKey = "prepaid_account_id";

    public function getClassInfo()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSectionInfo()
    {
    	return $this->hasMany('App\Model\Section\Section','section_id');
    }

    public function getStudentInfo()
    {
    	return $this->hasMany('App\Model\Student\Student','student_id');
    }
}
