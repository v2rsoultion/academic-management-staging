<?php

namespace App\Model\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccSubHead extends Model
{
    protected $table      = 'acc_sub_heads';
    protected $primaryKey = 'acc_sub_head_id';

    public function getMainHead() {
        return $this->belongsTo('App\Model\Accounts\AccMainHead', 'acc_main_head_id');
    }
}
