<?php

namespace App\Model\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccGroupHead extends Model
{
    protected $table      = 'acc_group_heads';
    protected $primaryKey = 'acc_group_head_id';

    public function getSubHead() {
        return $this->belongsTo('App\Model\Accounts\AccSubHead', 'acc_sub_head_id');
    }

    public function getAccGroup() {
        return $this->belongsTo('App\Model\Accounts\AccGroup', 'acc_group_id');
    }
}
