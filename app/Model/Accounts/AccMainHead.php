<?php

namespace App\Model\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccMainHead extends Model
{
    protected $table      = 'acc_main_heads';
    protected $primaryKey = 'acc_main_head_id';

    public function getSubHeads() {
        return $this->hasMany('App\Model\Accounts\AccSubHead', 'acc_main_head_id');
    }
}
