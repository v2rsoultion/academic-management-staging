<?php

namespace App\Model\Session;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table      = 'sessions';
    protected $primaryKey = 'session_id';
    //
}
