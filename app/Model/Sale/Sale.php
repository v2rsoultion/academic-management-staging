<?php

namespace App\Model\Sale;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table      = 'sales';
    protected $primaryKey = 'sale_id';

    public function purchaseItems() {
        return $this->hasMany('App\Model\Sale\SaleItems','purchase_id');
    }
    public function category() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'category_id');
    }

    public function subCategory() {
        return $this->belongsTo('App\Model\InventoryCategory\Category', 'subcategory_id');
    }

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student', 'student_id');
    }

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

    public function getSiblingClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSections()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }

    public function bank() {
        return $this->belongsTo('App\Model\FeesCollection\Bank', 'bank_id');
    }


}
