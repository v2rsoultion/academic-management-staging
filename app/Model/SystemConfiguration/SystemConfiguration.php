<?php

namespace App\Model\SystemConfiguration;

use Illuminate\Database\Eloquent\Model;

class SystemConfiguration extends Model
{
    protected $table      = 'system_config';
    protected $primaryKey = 'system_config_id';
}
