<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class VehicleDocument extends Model
{
    protected $table      = 'vehicle_documents';
    protected $primaryKey = 'vehicle_document_id';

    public function getDocumentCategory()
    {
        return $this->belongsTo('App\Model\DocumentCategory\DocumentCategory','document_category_id');
    }

}
