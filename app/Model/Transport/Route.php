<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table      = 'routes';
    protected $primaryKey = 'route_id';

    public function get_route_locations()
    {
        return $this->hasMany('App\Model\Transport\RouteLocation', 'route_id');
    }
}
