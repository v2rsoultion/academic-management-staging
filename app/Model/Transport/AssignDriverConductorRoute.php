<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class AssignDriverConductorRoute extends Model
{
    protected $table      = 'assign_driver_conductor_routes';
    protected $primaryKey = 'assign_id';

    public function getDriver()
    {
        return $this->belongsTo('App\Model\Staff\Staff','driver_id');
    }

    public function getConductor()
    {
        return $this->belongsTo('App\Model\Staff\Staff','conductor_id');
    }
    
}
