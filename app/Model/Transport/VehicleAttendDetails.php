<?php

namespace App\Model\Transport;

use Illuminate\Database\Eloquent\Model;

class VehicleAttendDetails extends Model
{
    protected $table      = 'vehicle_attend_details';
    protected $primaryKey = 'vehicle_attend_d_id';
 	
}
