<?php

namespace App\Model\Notice;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $table      = 'notice';
    protected $primaryKey = 'notice_id';

    // For Session
    public function getSession()
    {
        return $this->belongsTo('App\Model\Session\Session','session_id');
    }
}
