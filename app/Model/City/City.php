<?php

namespace App\Model\City;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "city";
    protected $primaryKey = "city_id";

    public function stateCity()
    {
        return $this->belongsTo('App\Model\State\State', 'state_id');
    }
    public function countryState()
    {
        return $this->belongsTo('App\Model\Country\Country', 'country_id');
    }
}
