<?php

namespace App\Model\BookCupboard;

use Illuminate\Database\Eloquent\Model;

class BookCupboard extends Model
{
    protected $table      = 'book_cupboards';
    protected $primaryKey = 'book_cupboard_id';
    
    public function getCupBoardShelf()
    {
        return $this->hasMany('App\Model\BookCupboardshelf\BookCupboardshelf', 'book_cupboard_id');
    }
}
