<?php

namespace App\Model\PlanSchedule;

use Illuminate\Database\Eloquent\Model;

class PlanSchedule extends Model
{
    protected $table      = 'plan_schedule';
    protected $primaryKey = 'plan_schedule_id';


    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

}
