<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class FeesStMap extends Model
{
    protected $table      = 'fees_st_map';
    protected $primaryKey = 'fees_st_map_id';
}
