<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class RTEHead extends Model
{
    protected $table      = 'rte_heads';
    protected $primaryKey = 'rte_head_id';
}
