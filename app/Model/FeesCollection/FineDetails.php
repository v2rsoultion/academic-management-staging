<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class FineDetails extends Model
{
    protected $table      = 'fine_details';
    protected $primaryKey = 'fine_detail_id';

}
