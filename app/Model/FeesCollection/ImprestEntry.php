<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class ImprestEntry extends Model
{
    protected $table      = 'imprest_ac_entries';
    protected $primaryKey = 'ac_entry_id';

    public function getSession()
    {
        return $this->belongsTo('App\Model\Session\Session','session_id');
    }

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section','section_id');
    }
}
