<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class RTEHeadMap extends Model
{
    protected $table      = 'rte_head_map';
    protected $primaryKey = 'rte_head_map_id';
}
