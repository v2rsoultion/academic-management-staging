<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class RecurringInst extends Model
{
    protected $table      = 'recurring_inst';
    protected $primaryKey = 'rc_inst_id';
}
