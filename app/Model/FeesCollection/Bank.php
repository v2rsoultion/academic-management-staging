<?php

namespace App\Model\FeesCollection;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table      = 'banks';
    protected $primaryKey = 'bank_id';
}
