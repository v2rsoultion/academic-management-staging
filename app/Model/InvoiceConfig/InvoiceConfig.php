<?php

namespace App\Model\InvoiceConfig;

use Illuminate\Database\Eloquent\Model;

class InvoiceConfig extends Model
{
    protected $table      = 'invoice_config';
    protected $primaryKey = 'invoice_config_id';
}
