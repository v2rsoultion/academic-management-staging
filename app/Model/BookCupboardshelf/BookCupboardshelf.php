<?php

namespace App\Model\BookCupboardshelf;

use Illuminate\Database\Eloquent\Model;

class BookCupboardshelf extends Model
{
    protected $table      = 'book_cupboardshelfs';
    protected $primaryKey = 'book_cupboardshelf_id';

    // For CupBoard
    public function getCupBoard()
    {
        return $this->belongsTo('App\Model\BookCupboard\BookCupboard','book_cupboard_id');
    }
}
