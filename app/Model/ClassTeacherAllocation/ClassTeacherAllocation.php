<?php

namespace App\Model\ClassTeacherAllocation;

use Illuminate\Database\Eloquent\Model;

class ClassTeacherAllocation extends Model
{
    protected $table      = 'staff_class_allocations';
    protected $primaryKey = 'staff_class_allocation_id';

    public function getCurrentClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getCurrentSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'section_id');
    }

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_id');
    }

}