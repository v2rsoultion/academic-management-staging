<?php

namespace App\Model\Remarks;

use Illuminate\Database\Eloquent\Model;

class Remarks extends Model
{
    protected $table      = 'remarks';
    protected $primaryKey = 'remark_id';
    protected $dates = ['remark_date'];
    
    public function getSession()
    {
        return $this->belongsTo('App\Model\Session\Session','session_id');
    }
    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }
    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section','section_id');
    }
    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }
    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject','subject_id');
    }
}
