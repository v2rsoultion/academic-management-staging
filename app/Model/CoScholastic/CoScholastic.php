<?php

namespace App\Model\CoScholastic;

use Illuminate\Database\Eloquent\Model;

class CoScholastic extends Model
{
    protected $table      = 'co_scholastic_types';
    protected $primaryKey = 'co_scholastic_type_id';
}
