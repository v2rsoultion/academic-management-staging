<?php

namespace App\Model\InventoryUnit;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table      = 'inv_unit';
    protected $primaryKey = 'unit_id';

    public function unitdetails() {
        return $this->hasOne('App\Model\InventoryItem\Items','unit_id');
    }

    public function purchaseunitdetails() {
        return $this->hasOne('App\Model\Purchase\PurchaseItems','unit_id');
    }
}
