<?php

namespace App\Model\Address;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table      = 'state';
    protected $primaryKey = 'state_id';

    public function countryInfo()
    {
        return $this->belongsTo('App\Model\Address\Country','country_id');
    }
}
