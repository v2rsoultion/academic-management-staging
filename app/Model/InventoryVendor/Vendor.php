<?php

namespace App\Model\InventoryVendor;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'inv_vendor';
    protected $primaryKey = 'vendor_id';

    public function vendordetails() {
        return $this->hasOne('App\Model\Purchase\Purchase','vendor_id');
    }
}
