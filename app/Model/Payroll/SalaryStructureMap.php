<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class SalaryStructureMap extends Model
{
    protected $table      = 'salary_structure_map';
    protected $primaryKey = 'salary_structure_map_id';

    public function getSalaryStructureMap() {
        return $this->belongsTo('App\Model\Payroll\SalaryStructure','salary_structure_id');
    }
    public function SalarysHead() {
        return $this->belongsTo('App\Model\Payroll\SalaryHead','pay_salary_head_id');
    }
}
