<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table      = 'pay_config';
    protected $primaryKey = 'pay_config_id';
}
