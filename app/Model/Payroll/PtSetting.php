<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class PtSetting extends Model
{
    protected $table = 'pt_setting';
    protected $primaryKey = 'pt_setting_id';
}
