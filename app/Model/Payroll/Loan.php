<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table      = 'pay_loan';
    protected $primaryKey = 'pay_loan_id';

    public function LoanStaff() {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
