<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class SalaryStructure extends Model
{
    protected $table      = 'salary_structure';
    protected $primaryKey = 'salary_structure_id';

    public function SalaryStructureMap() {
        return $this->hasMany('App\Model\Payroll\SalaryStructureMap','salary_structure_id');
    }
}
