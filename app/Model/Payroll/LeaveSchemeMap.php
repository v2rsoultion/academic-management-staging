<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class LeaveSchemeMap extends Model
{
    protected $table      = 'pay_leave_scheme_map';
    protected $primaryKey = 'pay_leave_scheme_map_id';

    public function getLeaveSchemeMap() {
        return $this->belongsTo('App\Model\Payroll\LeaveScheme','pay_leave_scheme_id');
    }

    public function LeaveSchemeStaff() {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
