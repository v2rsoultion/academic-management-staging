<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class TdsSetting extends Model
{
    protected $table = 'tds_setting';
    protected $primaryKey = 'tds_setting_id';
}
