<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Arrear extends Model
{
    protected $table      = 'pay_arrear';
    protected $primaryKey = 'pay_arrear_id';

    public function ArrearMap() {
        return $this->hasMany('App\Model\Payroll\ArrearMap','pay_arrear_id');
    }
}
