<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class SalaryHead extends Model
{
    protected $table      = 'pay_salary_head';
    protected $primaryKey = 'pay_salary_head_id';
}
