<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class SalaryStructureMapStaff extends Model
{
    protected $table      = 'sal_struct_map_staff';
    protected $primaryKey = 'sal_struct_map_staff_id';

    public function getSalStructureMapStaff() {
        return $this->belongsTo('App\Model\Staff\Staff','staff_id');
    }
}
