<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class PayrollMapping extends Model
{
    protected $table      = 'payroll_mapping';
    protected $primaryKey = 'payroll_mapping_id';

    public function getDepartmentMap()
    {
        return $this->belongsTo('App\Model\Payroll\Department','pay_dept_id');
    }

    public function getGradeMap()
    {
        return $this->belongsTo('App\Model\Payroll\Grade','pay_grade_id');
    }
}
