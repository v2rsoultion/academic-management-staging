<?php

namespace App\Model\StudentSubjectManage;

use Illuminate\Database\Eloquent\Model;

class StudentSubjectManage extends Model
{
    protected $table      = 'st_exclude_sub';
    protected $primaryKey = 'st_exclude_sub_id';

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject','subject_id');
    }
}
