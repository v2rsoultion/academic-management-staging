<?php

namespace App\Model\Stream;

use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    protected $table      = 'streams';
    protected $primaryKey = 'stream_id';
}