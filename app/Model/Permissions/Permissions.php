<?php

namespace App\Model\Permissions;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    protected $table      = 'module_permissions';
    protected $primaryKey = 'module_permission_id';
}
