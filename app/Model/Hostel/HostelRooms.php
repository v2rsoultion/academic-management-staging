<?php

namespace App\Model\Hostel;

use Illuminate\Database\Eloquent\Model;

class HostelRooms extends Model
{
    protected $table      = 'hostel_rooms';
    protected $primaryKey = 'h_room_id';

    public function getHostel()
    {
        return $this->belongsTo('App\Model\Hostel\Hostel', 'hostel_id');
    }

    public function getBlock()
    {
        return $this->belongsTo('App\Model\Hostel\HostelBlock', 'h_block_id');
    }

    public function getFloor()
    {
        return $this->belongsTo('App\Model\Hostel\HostelFloor', 'h_floor_id');
    }

    public function getRoomCate()
    {
        return $this->belongsTo('App\Model\Hostel\HostelRoomCategory', 'h_room_cate_id');
    }

    public function getStudentsMap()
    {
        return $this->hasMany('App\Model\Hostel\HostelStudentMap', 'h_room_id');
    }
}
