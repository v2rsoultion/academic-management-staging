Hello {!! $name !!},
<br />
<br />
Please find below information regarding your recruitment.
<br />
@if($date != "") Date: {!! $date !!} <br /> @endif
@if($time != "") Time: {!! $time !!} <br /> @endif
Message: {!! $rec_message !!}
<br />
<br />
Thank You, <br />