@if(isset($subject['subject_id']) && !empty($subject['subject_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('subject_id',old('subject_id',isset($subject['subject_id']) ? $subject['subject_id'] : ''),['class' => 'gui-input', 'id' => 'subject_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.subject_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('subject_name', old('subject_name',isset($subject['subject_name']) ? $subject['subject_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.subject_name'), 'id' => 'subject_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.subject_code') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('subject_code', old('subject_code',isset($subject['subject_code']) ? $subject['subject_code']: ''), ['class' => 'form-control','placeholder'=>trans('language.subject_code'), 'id' => 'subject_code']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Is_Coscholastic :</lable>
        <div class="form-group">
            <div class="radio" style="margin-top:6px !important;">
                @if(isset($subject['subject_is_coscholastic'])) 
                    {{ Form::radio('subject_is_coscholastic', '1',  $subject['subject_is_coscholastic'] == '1', array('id'=>'radio12')) }}
                    {{ Form::label('radio12', 'Yes', array('onclick'=>'checkForScholastic("1")', 'class' => 'document_staff')) }}

                    {{ Form::radio('subject_is_coscholastic', '0',  $subject['subject_is_coscholastic'] == '0', array('id'=>'radio2')) }}
                {{ Form::label('radio2', 'No', array('onclick'=>'checkForScholastic("0")', 'class' => 'document_staff')) }}
                @else
                {{ Form::radio('subject_is_coscholastic', '1',  false, array('id'=>'radio12')) }}
                {{ Form::label('radio12', 'Yes', array('onclick'=>'checkForScholastic("1")', 'class' => 'document_staff')) }}
                {{ Form::radio('subject_is_coscholastic', '0',  true, array('id'=>'radio2')) }}
                {{ Form::label('radio2', 'No', array('onclick'=>'checkForScholastic("0")', 'class' => 'document_staff')) }}
                @endif
                
            </div>
        </div>
    </div>
</div>
<div style="@if(isset($subject['subject_is_coscholastic']) && $subject['subject_is_coscholastic'] == '1') display:block; @else  display:none; @endif" id="scholasticBlock">
    <div class="row clearfix"> 
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.subject_class_ids') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('class_ids[]', $subject['arr_class'],  isset( $subject['class_ids']) ?  $subject['class_ids'] : "", ['class' => 'form-control show-tick select_form1 select2','id'=>'class_ids', 'multiple'=>'multiple', 'required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
            <lable class="from_one1">{!! trans('language.co_scholastic_type_id') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('co_scholastic_type_id', $subject['arr_co_scholastic'],isset($subject['co_scholastic_type_id']) ? $subject['co_scholastic_type_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'co_scholastic_type_id', 'required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div> 
        <div class="col-lg-6 col-md-6">
            <lable class="from_one1">{!! trans('language.description') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::textarea('co_scholastic_subject_des', old('co_scholastic_subject_des',isset($subject['co_scholastic_subject_des']) ? $subject['co_scholastic_subject_des']: ''), ['class' => 'form-control','placeholder'=>trans('language.description'), 'id' => 'co_scholastic_subject_des', 'rows'=> 2]) !!}
            </div>
        </div>   
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/academic') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#subject-form").validate({

            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                subject_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                subject_code: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                co_scholastic_subject_des: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    //error.insertAfter(element.parent());
                    element.closest('.form-group').after(error);
                }
            }
        });
    });

    function checkForScholastic(val) {
        var x = document.getElementById("scholasticBlock");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

</script>