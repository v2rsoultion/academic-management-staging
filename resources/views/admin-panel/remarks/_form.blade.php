@if(isset($remark['remark_id']) && !empty($remark['remark_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('remark_id',old('remark_id',isset($remark['remark_id']) ? $remark['remark_id'] : ''),['class' => 'gui-input', 'id' => 'remark_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.students') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('student_id', $remark['arr_students'],isset($remark['student_id']) ? $remark['student_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.subject') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('subject_id', $remark['arr_subjects'],isset($remark['subject_id']) ? $remark['subject_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.remark_text') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('remark_text', old('remark_text',isset($remark['remark_text']) ? $remark['remark_text']: ''), ['class' => 'form-control','placeholder'=>trans('language.remark_text'), 'id' => 'remark_text', 'rows'=> '3']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/my-class') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#remark-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                student_id: {
                    required: true,
                },
                subject_id: {
                    required: true,
                },
                remark_text: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });

</script>