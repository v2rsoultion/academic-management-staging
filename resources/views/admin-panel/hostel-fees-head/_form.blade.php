@if(isset($hostel['hostel_id']) && !empty($hostel['hostel_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('hostel_id',old('hostel_id',isset($hostel['hostel_id']) ? $hostel['hostel_id'] : ''),['class' => 'gui-input', 'id' => 'hostel_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_fees_head_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('h_fees_head_name', old('h_fees_head_name',isset($h_fees_head['h_fees_head_name']) ? $h_fees_head['h_fees_head_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_fees_head_name'), 'id' => 'h_fees_head_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_fees_head_price') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('h_fees_head_price', old('h_fees_head_price',isset($h_fees_head['h_fees_head_price']) ? $h_fees_head['h_fees_head_price']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_fees_head_price'), 'id' => 'h_fees_head_price']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/hostel') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#h-fees-head-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                h_fees_head_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                h_fees_head_price: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });
</script>