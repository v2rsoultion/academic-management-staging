@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.fees_due_report') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.fees_due_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal', 'url' =>$save_url]) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $report['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                            {!!Form::select('hostel_id', $report['arr_hostels'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'hostel_id','onChange' => 'getBlock(this.value)'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <input type="hidden" name="hostel_block_id" id="hostel_block_id" value="{{$hostel_block_id}}">
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                            {!!Form::select('h_block_id', $report['arr_block'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_block_id','onChange' => 'getFloor(this.value)'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <input type="hidden" name="hostel_floor_id" id="hostel_floor_id" value="{{$hostel_floor_id}}">
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                            {!!Form::select('h_floor_id', $report['arr_floor'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_floor_id', 'onChange' => 'getRoom(this.value)'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div> 
                                    <input type="hidden" name="hostel_room_id" id="hostel_room_id" value="{{$hostel_room_id}}">
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                            {!!Form::select('h_room_id', $report['arr_room'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_room_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div> 
                                    <div class="col-lg-1 col-md-1 padding-0">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1 padding-0">
                                        <a href="fees-due-report" class="btn btn-raised btn-round btn-primary" name="Clear">Clear</a>
                                        
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <div id="grid-block"> 
                                <div class="table-responsive" style="margin-top: 30px;" >
                                    @if(!empty($final_sheet_record) && !empty($sessionPeriodArr))
                                    <table class="table m-b-0 c_list table-bordered" id="class-monthly-table" style="width:100%;">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th rowspan=2>{{trans('language.s_no')}}</th>
                                                <th rowspan=2>{{trans('language.student')}}</th>
                                                <th rowspan=2>{{trans('language.hostel_block')}} - {{trans('language.floor_room_no')}}</th>
                                                @if(!empty($sessionPeriodArr))
                                                @foreach($sessionPeriodArr as $session_list)
                                                    <th class="text-center" colspan="3">{!! $session_list['month_year_string'] !!} </th>
                                                @endforeach
                                                @endif
                                            </tr>
                                            <tr>
                                                @foreach($sessionPeriodArr as $session_list)
                                                    <td>Total Paid</td>
                                                    <td>Total Due</td>
                                                    <td>Concession</td>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($final_sheet_record))
                                            @php $counter= 0; @endphp
                                                @foreach($final_sheet_record as $key => $record)
                                            <tr>
                                                <td style="width: 30px !important;">@php echo ++$counter; @endphp</td>
                                                <td>{!! $record['student_name'] !!}</td>
                                                <td>{!! $record['hostel_block_floor_room'] !!}</td>
                                                @foreach($final_sheet_record[$key]['fees_data'] as $value)
                                                    @if($value['total_paid_amount'] != "")
                                                        <td>{!! $value['total_paid_amount'] !!}</td>
                                                    @else
                                                        <td>0.00</td>
                                                    @endif
                                                    @if($value['total_dues_amount'] != "")
                                                        <td>{!! $value['total_dues_amount'] !!}</td>
                                                    @else
                                                        <td>0.00</td>
                                                    @endif
                                                    @if($value['total_concession_amount'] != "")
                                                        <td>{!! $value['total_concession_amount'] !!}</td>
                                                    @else
                                                        <td>0.00</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                        <div class="alert alert-danger" role="alert">
                                            No Record found.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    
                                    @endif
                                    
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

     $(document).ready(function() {
        var hostel_id = $("#hostel_id").val();
        var hostel_block_id = $("#hostel_block_id").val();
        if(hostel_id != '') {
            window.onload = getBlock(hostel_id,hostel_block_id);
        }

        var h_block_id = $("#h_block_id").val();
        var hostel_floor_id = $("#hostel_floor_id").val();
        if(h_block_id != '') {
            window.onload = getFloor(h_block_id,hostel_floor_id);
        }
        if(hostel_block_id != '') {
            window.onload = getFloor(hostel_block_id,hostel_floor_id);
        }

        var h_floor_id = $("#h_floor_id").val();
        var hostel_room_id = $("#hostel_room_id").val();
        if(h_floor_id != '') {
            window.onload = getRoom(h_floor_id,hostel_room_id);
        }
        if(hostel_floor_id != '') {
            window.onload = getRoom(hostel_floor_id,hostel_room_id);
        }
    });
    
    function getBlock(hostel_id,hostel_block_id)
    {
        
        if(hostel_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-block-data')}}",
                type: 'GET',
                data: {
                    'hostel_id': hostel_id,
                    'hostel_block_id': hostel_block_id
                },
                success: function (data) {
                    $("select[name='h_block_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='h_block_id'").html('');
            $(".mycustloading").hide();
        }
    }
    function getFloor(h_block_id,hostel_floor_id)
    {
        if(h_block_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-floor-data')}}",
                type: 'GET',
                data: {
                    'h_block_id': h_block_id,
                    'hostel_floor_id': hostel_floor_id
                },
                success: function (data) {
                    $("select[name='h_floor_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_floor_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function getRoom(h_floor_id,hostel_room_id)
    {
        if(h_floor_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/rooms/get-room-data')}}",
                type: 'GET',
                data: {
                    'h_floor_id': h_floor_id,
                    'hostel_room_id': hostel_room_id
                },
                success: function (data) {
                    $("select[name='h_room_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_room_id'").html('');
            $(".mycustloading").hide();
        }
    }
</script>
@endsection