@extends('admin-panel.layout.header')

@section('content')
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Registered</strong> Student Report <small></small> </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger" role="alert">
                                {{$errors->first()}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                            
                            <div class="alert alert-danger" role="alert" id="searchError" style="display: none;"></div>
                            <div class="clearfix"></div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('hostel_id', $report['arr_hostels'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'hostel_id','onChange' => 'getBlock(this.value)'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('h_block_id', $report['arr_block'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_block_id','onChange' => 'getFloor(this.value)'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('h_floor_id', $report['arr_floor'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_floor_id', 'onChange' => 'getRoom(this.value)'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div> 
                                <div class="col-lg-2 col-md-2">
                                    <label class=" field select" style="width: 100%">
                                        {!!Form::select('h_room_id', $report['arr_room'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_room_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                </div> 
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                </div>
                                <div class="col-lg-1 col-md-1">
                                    {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: block">
                            <hr>
                           
                            <div class="table-responsive" >    
                                <table class="table m-b-0 c_list " id="student-table" style="width:100%;">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.hostel_block')}}</th>
                                            <th>{{trans('language.h_floor_no')}}</th>
                                            <th>{{trans('language.h_room_no')}}</th>
                                            <th>{{trans('language.h_room_alias')}}</th>
                                            <th>{{trans('language.room_category')}}</th>
                                            <th>{{trans('language.h_room_capacity')}}</th>
                                            <th>{{trans('language.registered_students')}}</th>
                                            <th>{{trans('language.room_available')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ 
            $('#searchError').html('');
            $("#searchError").hide();
        });
       
    });
    $(document).ready(function () {
      
        var table = $('#student-table').DataTable({
            dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bInfo : false,
            bFilter: false,
            buttons: [
                {
                    extend: 'csvHtml5',
                }
            ],
            ajax: {
                url: "{{url('admin-panel/hostel/allocation-report/room-data/')}}",
                data: function (d) {
                    d.hostel_id = $('select[name=hostel_id]').val();
                    d.h_block_id = $('select[name=h_block_id]').val();
                    d.h_floor_id = $('select[name=h_floor_id]').val();
                    d.h_room_id  = $('select[name=h_room_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'hostel_block', name: 'hostel_block'},
                {data: 'h_floor_no', name: 'h_floor_no'},
                {data: 'h_room_no', name: 'h_room_no'},
                {data: 'h_room_alias', name: 'h_room_alias'},
                {data: 'h_room_cate_name', name: 'h_room_cate_name'},
                {data: 'h_room_capacity', name: 'h_room_capacity'},
                {data: 'student_name', name: 'student_name'},
                {data: 'room_available', name: 'room_available'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "3%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 8, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6, 7, 8],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
       
    });
    
    function getBlock(hostel_id)
    {
        if(hostel_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-block-data')}}",
                type: 'GET',
                data: {
                    'hostel_id': hostel_id
                },
                success: function (data) {
                    $("select[name='h_block_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_block_id'").html('');
            $(".mycustloading").hide();
        }
    }
    function getFloor(h_block_id)
    {
        if(h_block_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-floor-data')}}",
                type: 'GET',
                data: {
                    'h_block_id': h_block_id
                },
                success: function (data) {
                    $("select[name='h_floor_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_floor_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function getRoom(h_floor_id)
    {
        if(h_floor_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/rooms/get-room-data')}}",
                type: 'GET',
                data: {
                    'h_floor_id': h_floor_id
                },
                success: function (data) {
                    $("select[name='h_room_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_room_id'").html('');
            $(".mycustloading").hide();
        }
    }
    

    
</script>
@endsection

