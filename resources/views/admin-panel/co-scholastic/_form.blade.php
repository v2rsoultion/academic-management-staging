@if(isset($scholastic['co_scholastic_type_id']) && !empty($scholastic['co_scholastic_type_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('co_scholastic_type_id',old('co_scholastic_type_id',isset($scholastic['co_scholastic_type_id']) ? $scholastic['co_scholastic_type_id'] : ''),['class' => 'gui-input', 'id' => 'co_scholastic_type_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.co_scholastic_type_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('co_scholastic_type_name', old('co_scholastic_type_name',isset($scholastic['co_scholastic_type_name']) ? $scholastic['co_scholastic_type_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.co_scholastic_type_name'), 'id' => 'co_scholastic_type_name']) !!}
        </div>
    </div>

    <div class="col-lg-8 col-md-8">
        <lable class="from_one1">{!! trans('language.co_scholastic_type_description') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('co_scholastic_type_description', old('co_scholastic_type_description',isset($scholastic['co_scholastic_type_description']) ? $scholastic['co_scholastic_type_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.co_scholastic_type_description'), 'id' => 'co_scholastic_type_description', 'rows'=> 3]) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/academic') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $("#scholastic-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                co_scholastic_type_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                co_scholastic_type_description: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        
    });

    

</script>