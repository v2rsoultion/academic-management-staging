<option value="">Select Sub Category</option>
@if(!empty($subcategory))
  @foreach($subcategory as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif