
{!! Form::open(['files'=>TRUE,'id' => 'schedule-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
@if(isset($schedule['exam_schedule_id']) && !empty($schedule['exam_schedule_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('s_exam_schedule_id',old('exam_schedule_id',isset($schedule['exam_schedule_id']) ? $schedule['exam_schedule_id'] : ''),['class' => 'gui-input', 'id' => 's_exam_schedule_id', 'readonly' => 'true']) !!}
{!! Form::hidden('s_exam_id',old('exam_id',isset($schedule['exam_id']) ? $schedule['exam_id'] : ''),['class' => 'gui-input', 'id' => 's_exam_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info Schedule -->
<div class="row clearfix" id="top-block">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.schedule_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('schedule_name', old('schedule_name',isset($schedule['schedule_name']) ? $schedule['schedule_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.schedule_name'), 'id' => 'schedule_name','disabled']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.exam_name') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('exam_id', $schedule['arr_exam'],isset($schedule['exam_id']) ? $schedule['exam_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'s_exam_id','disabled'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('s_class_id', $schedule['arr_class'],isset($schedule['class_id']) ? $schedule['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'s_class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.section_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('s_section_id', $schedule['arr_section'],isset($schedule['section_id']) ? $schedule['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'s_section_id','onChange' => 'checkSearch()'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
   
</div>
{!! Form::close() !!}
<div class="row clearfix" id="subjectsBlock" style="display: none">
    <div class="col-lg-12">
        {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
        <div class="table-responsive">
            <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
            <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
            <table class="table m-b-0 c_list" id="map-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.subject_name') !!}</th>
                        <th>{!! trans('language.exam_date') !!}  <span class="red-text">*</span></th>
                        <th>{!! trans('language.exam_time') !!}  <span class="red-text">*</span></th>
                        <th>{!! trans('language.staffs') !!}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="row clearfix">
            <div class="col-lg-1 col-md-2">
                {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit']) !!}
            </div>
            <div class="col-lg-2 col-md-3">
                <a href="{!! url('admin-panel/examination/schedules') !!}" class="btn btn-raised" >Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
        
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#class-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                schedule_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                exam_id: {
                    required: true
                }
            },
            /* @validation highlighting + error placement---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    function getSection(class_id)
    {
        var exam_id = $('#s_exam_id').val();
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/get-map-sections')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'exam_id': exam_id
                },
                success: function (data) {
                    $("select[name='s_section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='s_section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function checkSearch(){
        var class_id = $("#s_class_id").val();
        var section_id = $("#s_section_id").val();
        var exam_schedule_id = $('#s_exam_schedule_id').val();
        var exam_id = $('#s_exam_id').val();
        var class_id = $('#s_class_id').val();
        var section_id = $('#s_section_id').val();
        if(class_id != '' && section_id != '' && exam_schedule_id != '' ){
                $("#subjectsBlock").show();
                var table1 = $('#map-table').DataTable({
                    //dom: 'Blfrtip',
                    destroy: true,
                    pageLength: 30,
                    processing: true,
                    serverSide: true,
                    bLengthChange: false,
                    searching: false,
                    paging: false,
                    info: false,
                    // buttons: [
                    //     'copy', 'csv', 'excel', 'pdf', 'print'
                    // ],
                    ajax: {
                        url: '{{url('admin-panel/examination/schedules/get-subjects')}}',
                        data: {
                            'class_id': class_id,
                            'section_id': section_id,
                            'exam_schedule_id': exam_schedule_id,
                            'exam_id': exam_id
                        },
                    },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        {data: 'subject_name', name: 'subject_name'},
                        {data: 'exam_date', name: 'exam_date'},
                        {data: 'exam_time', name: 'exam_time'},
                        {data: 'staff_ids', name: 'staff_ids'},
                    ],
                    columnDefs: [
                        {
                            "targets": 0, // your case first column
                            "width": "8%"
                        },
                        {
                            "targets": 1, // your case first column
                            "width": "15%"
                        },
                        {
                            "targets": 2, // your case first column
                            "width": "15%"
                        },
                        {
                            "targets": 3, // your case first column
                            "width": "40%"
                        },
                        {
                            "targets": 4, // your case first column
                            "width": "15%"
                        },
                        {
                            targets: [ 0, 1, 2 ],
                            className: 'mdl-data-table__cell--non-numeric'
                        }
                    ]
                });
                $(document).on("click", ".dates", function () {
                    // $(".dates").bootstrapMaterialDatePicker({ weekStart : 0,time: false,minDate : new Date() }).on('change', function(e, date){ $(this).valid();  });
                    // $(this).closest(".dates").bootstrapMaterialDatePicker('show');
                });
                $(document).on("focus", ".dates", function () {
                    // $(".dates").bootstrapMaterialDatePicker({ weekStart : 0,time: false,minDate : new Date() }).on('change', function(e, date){ $(this).valid();  });
                    // $(this).closest(".dates").bootstrapMaterialDatePicker('show');
                });
                $(document).on("click", ".times", function () {
                    counter = $(this).attr('counter');
                    var start_time = $('#exam_time_from'+counter).val(); //start time
                    var end_time = $('#exam_time_to'+counter).val();  //end time
                    
                    //convert both time into timestamp
                    var stt = new Date("November 30, 2018 " + start_time);
                    stt = stt.getTime();
                    var endt = new Date("November 30, 2018 " + end_time);
                    endt = endt.getTime();
                    if(stt > endt && end_time != '') {
                        $("#exam_time_to_err"+counter).html('Start time should be less than end time');
                        $('#exam_time_to'+counter).val('')
                    }
                    // $(".times").bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  });
                    // $(this).closest(".times").bootstrapMaterialDatePicker('show');
                    
                });
                $(document).on("focus", ".times", function () {
                    counter = $(this).attr('counter');
                    var start_time = $('#exam_time_from'+counter).val(); //start time
                    var end_time = $('#exam_time_to'+counter).val();  //end time
                    //convert both time into timestamp
                    var stt = new Date("November 30, 2018 " + start_time);
                    stt = stt.getTime();
                    var endt = new Date("November 30, 2018 " + end_time);
                    endt = endt.getTime();
                    if(stt > endt && end_time != '') {
                        $("#exam_time_to_err"+counter).html('Start time should be less than end time');
                        $('#exam_time_to'+counter).val('')
                    }
                    // $(".times").bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  });
                    // $(this).closest(".times").bootstrapMaterialDatePicker('show');
                });
                $(document).on("keyup", ".times", function () {
                    console.log('keyup');
                    counter = $(this).attr('counter');
                    var start_time = $('#exam_time_from'+counter).val(); //start time
                    var end_time = $('#exam_time_to'+counter).val();  //end time
                    //convert both time into timestamp
                    var stt = new Date("November 30, 2018 " + start_time);
                    stt = stt.getTime();
                    var endt = new Date("November 30, 2018 " + end_time);
                    endt = endt.getTime();
                    if(stt > endt && end_time != '') {
                        $("#exam_time_to_err"+counter).html('Start time should be less than end time');
                        $('#exam_time_to'+counter).val('')
                    }
                    // $(".times").bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid();  });
                    // $(this).closest(".times").bootstrapMaterialDatePicker('show');
                });
                $('.select22').select2();
                
            
        } else {
            $("#subjectsBlock").hide();
            
        }
        
    }

    function CheckValid(id){
        var subject_id = $('#subject_id'+id).val();
        var exam_date = $('#exam_date'+id).val();
        var exam_time_from = $('#exam_time_from'+id).val();
        var exam_time_to = $('#exam_time_to'+id).val();
        var exist_staff_ids = $('#exist_staff_ids'+id).val();
        var exam_schedule_id = $('#s_exam_schedule_id').val();
        var exam_id = $('#s_exam_id').val();
        var class_id = $('#s_class_id').val();
        var section_id = $('#s_section_id').val();
        $('#section_id'+id).val(section_id);
        $('#class_id'+id).val(class_id);
        $('#exam_id'+id).val(exam_id);
        $('#exam_schedule_id'+id).val(exam_schedule_id);
        if(subject_id != "" && exam_date != "" && exam_time_from != "" && exam_time_to != ''){
            
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/schedules/get-available-staff')}}",
                type: 'GET',
                data: {
                    'exam_date': exam_date,
                    'exam_time_from' : exam_time_from,
                    'exam_time_to': exam_time_to,
                    'exist_staff_ids': exist_staff_ids
                },
                success: function (data) {
                    $('#staff_ids'+id).select2();
                    $("#staff_ids"+id).html(data.options);
                   $(".mycustloading").hide();
                }
            });
        } else {
            // $("#staff_ids").prop('disabled', true);
        }
    }

    $('#list-form').on('submit', function(e) {
       
        var formData = $(this).serialize();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/examination/schedules/save-schedule')}}",
            data     : formData,
            cache    : false,

            success  : function(data) {
                data = $.trim(data);
                console.log(data);
                if(data == "success"){
                    $("#success-block").html("Schedule successfully updated.");
                    $("#success-block").show();
                    $("#error-block").html("");
                    $("#error-block").hide();
                    $('html, body').animate({
                        scrollTop: $("#top-block").offset().top
                    }, 1000);
                    checkSearch();
                } else {
                    $("#success-block").html("");
                    $("#success-block").hide();
                    $("#error-block").html("Something goes wrong.");
                    $("#error-block").show();
                    $('html, body').animate({
                        scrollTop: $("#top-block").offset().top
                    }, 1000);
                }
            }
        })
        
        e.preventDefault();
    });

    

</script>