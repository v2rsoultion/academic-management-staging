@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_examination') !!}</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.menu_examination') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                             
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="schedule-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{!! trans('language.terms_name') !!}</th>
                                            <th>{!! trans('language.exam_name') !!}</th>
                                            <th>{{trans('language.schedule_date_range')}}</th>
                                            <th>{{trans('language.view_exam_schedules')}} </th>
                                            <th>{{trans('language.my_marks')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>


<div class="modal fade" id="subjectsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
           
            <table class="table m-b-0 c_list" id="subjects-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.exam_date') !!}</th>
                        <th>{!! trans('language.subject_name') !!}</th>
                        <th>{!! trans('language.schedule_time_range') !!}</th>
                        <th>{!! trans('language.room_no') !!}</th>
                    </tr>
                </thead>
            </table>
            
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="marksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Marks Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 0px !important;">
            <div id="marks-table-total"></div>
            <table class="table m-b-0 c_list" id="marks-table" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('language.s_no')}}</th>
                        <th>{!! trans('language.subject_name') !!}</th>
                        <th>{!! trans('language.exam_total_mark') !!}</th>
                        <th>{!! trans('language.exam_obtain_mark') !!}</th>
                        <th>{!! trans('language.grades') !!}</th>
                    </tr>
                </thead>
            </table>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var exam_id = $("#exam_id").val();
        var exam_schedule_id = $("#exam_schedule_id").val();
        var table = $('#schedule-table').DataTable({
            
            // dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'pdf','csvHtml5'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-schedule-data-student')}}',
                data: function (d) {
                    d.class_id = $('select[name="class_id"]').val();
                    d.section_id = $('select[name="section_id"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'term_exam_name', name: 'term_exam_name'},
                {data: 'exam_name', name: 'exam_name'},
                {data: 'date_range', name: 'date_range'},
                {data: 'schedule', name: 'schedule'},
                {data: 'marks', name: 'marks'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    $(document).on('click','.subjects',function(e){
        var exam_id = $(this).attr('exam-id');
        var exam_schedule_id = $(this).attr('exam-schedule-id');
        var class_id = $(this).attr('class-id');
        var section_id = $(this).attr('section-id');
        $('#subjectsModel').modal('show');
        var table1 = $('#subjects-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-schedule-subjects-student')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.exam_schedule_id = exam_schedule_id;
                    d.class_id = class_id;
                    d.section_id = section_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'exam_date', name: 'exam_date'},
                {data: 'subject_name', name: 'subject_name'},
                {data: 'time_range', name: 'time_range'},
                {data: 'room_no', name: 'room_no'},
            ],
            columnDefs: [
                
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

    })



    $(document).on('click','.marks',function(e){
        var exam_id = $(this).attr('exam-id');
        var exam_schedule_id = $(this).attr('exam-schedule-id');
        var class_id = $(this).attr('class-id');
        var section_id = $(this).attr('section-id');
        $('#marksModel').modal('show');

        var table1 = $('#marks-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 30,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-marks-student')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.exam_schedule_id = exam_schedule_id;
                    d.class_id = class_id;
                    d.section_id = section_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'subject_name', name: 'subject_name'},
                {data: 'max_marks', name: 'max_marks'},
                {data: 'marks', name: 'marks'},
                {data: 'grade_name', name: 'grade_name'},
            ],
            columnDefs: [
                
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ],
            drawCallback: function (row, data, start, end, display) {
                var api = this.api();

                $(api.column(0).footer()).html('');
                $(api.column(1).footer()).html('');
                $(api.column(2).footer()).html('Total');
                $(api.column(3).footer()).html('');
                $(api.column(4).footer()).html('');
                // $( api.column(3).footer() ).html(
                //     round(api.column( 3, {page:'current'} ).data().sum(),2)
                // );
                // $( api.column(4).footer() ).html(
                //     round(api.column( 4, {page:'current'} ).data().sum(),2)
                // );
            },
        });



        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/examination/exam-marks-student-total')}}",
            type: 'GET',
            data: {
                'exam_id': exam_id,
                'exam_schedule_id': exam_schedule_id,
                'class_id': class_id,
                'section_id': section_id,
            },
            success: function (data) {
                 $("#marks-table-total").html(data);
            }
        });

    })


</script>
@endsection




