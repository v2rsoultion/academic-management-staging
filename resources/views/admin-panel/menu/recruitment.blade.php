@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_recruitment') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/recruitment') !!}">{!! trans('language.menu_recruitment') !!}</a></li>
        
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Recruitment/Job.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Job</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="recruitment/job/add-job" class="float-left" title="Add ">
                        <i class="fas fa-plus"></i> Add 
                      </a>
                      <a href="recruitment/job/view-job" class="float-right" title="View ">
                        <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                      <!-- <ul class="header-dropdown opendiv">
                        <li class="dropdown">
                          <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right slideUp">
                            <li>
                              <a href="#" title="">View Candidate</a>
                            </li>
                            
                          </ul>
                        </li>
                      </ul>
                      <div class="clearfix"></div> -->
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/admission/admission_form.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Create Forms</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/recruitment/forms/add-form') }}" class="float-left" title="Add ">
                        <i class="fas fa-plus"></i> Add 
                      </a>
                      <a href="{{ url('/admin-panel/recruitment/forms/view-form') }}" class="float-right" title="View ">
                        <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                     
                      <div class="clearfix"></div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/admission/manage_enquires.svg') !!}" alt="Recruitment Forms">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Recruitment Forms</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/recruitment/recruitment-forms') }}" class="cusa" title="Form">
                        <i class="fas fa-eye"></i> Form 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Recruitment/InterviewSchedule.svg') !!}" alt="Applied Candidates">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Applied Candidates</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/recruitment/applied-candidates') }}" class="cusa" title="View">
                        <i class="fas fa-eye"></i> View 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>

                        <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Recruitment/Select Candidates.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Select Candidates</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="{{ url('/admin-panel/recruitment/selected-candidates') }}" class="cusa" title="View " style="width: 48%; margin: 16px auto;">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Recruitment/Report.svg') !!}" alt="Reports">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Reports</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="{{ url('/admin-panel/recruitment/reporting') }}" class="cusa" title="View " style="width: 48%; margin: 16px auto;">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </section>
@endsection
