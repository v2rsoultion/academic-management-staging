@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_library') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item">{!! trans('language.menu_library') !!}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Library/Configurations.svg') !!}" alt="Book Category">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Book Category</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="book-category/add-book-category" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="book-category/view-book-categories" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        
                      </div>
                    </div>

                    <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Library/Book Allowance.svg') !!}" alt="Book Allowance" >
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Book Allowance</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="" class="cusa allowance" title="Manage " book-allowance-id ="{{$book_allowance_id}}" data-toggle="modal" data-target="#bookAllowanceModel">
                              <i class="fas fa-plus"></i> Manage 
                            </a>
                         
                          <div class="clearfix"></div>
                        </div>
                      </div>

                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Library/Book Vendor.svg') !!}" alt="Book Vendor" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Book Vendor</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="book-vendor/add-book-vendor" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="book-vendor/view-book-vendors" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Library/Book CupBorads.svg') !!}" alt="Book CupBoards">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Book CupBoards</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="book-cupboard/add-book-cupboard" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="book-cupboard/view-book-cupboard" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <!-- <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Library/CupBorad Shelf.svg') !!}" alt="CupBoard Shelf">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Book CupBoard Shelf</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="cupboard-shelf/add-cupboard-shelf" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="cupboard-shelf/view-cupboard-shelf" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div> -->
                        
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Library/Book.svg') !!}" alt="Book">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Book</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="book/add-book" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a>
                                <a href="book/view-books" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Library/Members.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Members</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="member/register-student" class="float-left" title="Student ">
                              <i class="fas fa-plus"></i> Student 
                            </a>
                            <a href="member/register-staff" class="float-right" title="Staff ">
                              <i class="fas fa-plus"></i>Staff 
                            </a>
                            <div class="clearfix"></div>
                            <ul class="header-dropdown opendiv">
                              <li class="dropdown">
                                <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-v"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                  <li>
                                    <a href="{{ url('/admin-panel/member/view-students') }}" title="">View Students</a>
                                  </li>
                                  <li>
                                    <a href="{{ url('/admin-panel/member/view-staff') }}" title="">View Staff</a>
                                  </li>
                                  
                                </ul>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Library/Issue Book.svg') !!}" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Manage Book</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="{{ url('/admin-panel/issue-book') }}" class="float-left" title="Issue ">
                                <i class="fas fa-plus"></i> Issue 
                              </a>
                              <a href="{{ url('/admin-panel/return-book') }}" class="float-right" title="Return/Renew ">
                                <!-- <i class="fas fa-eye"></i>  -->Return/Renew
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Library/Fine.svg') !!}" alt="Fine">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Fine</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="library-fine" class="cusa" title="Manage ">
                                  <i class="fas fa-plus"></i> Manage 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Library/Reports.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Reports</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  
                                  <a href="{{ url('admin-panel/library-report') }}" class="cusa" title="View ">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </section>
<div class="modal fade" id="bookAllowanceModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Book Allowance </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="padding-top: 15px !important;padding-bottom: 25px !important;">
            {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal']) !!}
            <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
            <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
            {!! Form::hidden('book_allowance_id',old('book_allowance_id',isset($book_allowance['book_allowance_id']) ? $book_allowance['book_allowance_id'] : ''),['class' => 'gui-input', 'id' => 'book_allowance_id', 'readonly' => 'true']) !!}
            <div class="row clearfix">
              <div class="col-lg-6 col-md-6">
                <lable class="from_one1">{!! trans('language.edit_book_staff') !!} :</lable>
                <div class="form-group">
                  {!! Form::number('allow_for_staff', old('allow_for_staff',isset($book_allowance['allow_for_staff']) ? $book_allowance['allow_for_staff']: ''), ['class' => 'form-control','placeholder'=>trans('language.edit_book_staff'), 'id' => 'allow_for_staff']) !!}
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <lable class="from_one1">{!! trans('language.edit_book_student') !!} :</lable>
                <div class="form-group">
                    {!! Form::number('allow_for_student', old('allow_for_student',isset($book_allowance['allow_for_student']) ? $book_allowance['allow_for_student']: ''), ['class' => 'form-control','placeholder'=>trans('language.edit_book_student'), 'id' => 'allow_for_student']) !!}
                </div>
              </div>
            </div>
            <div class="row clearfix">
              <div class="col-lg-2 col-md-2">
                <lable class="from_one1">&nbsp;</lable> <br />
                {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit', 'id'=>'list-submit']) !!}
              </div>
              <div class="col-lg-2 col-md-2">
                <lable class="from_one1">&nbsp;</lable> <br />
                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
              </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
  $(document).on('click','.allowance',function(e){
    $("#error-block").hide();
    $("#success-block").hide();
    var book_allowance_id = $(this).attr('book-allowance-id');
    $('#book_allowance_id').val(book_allowance_id);
    if(book_allowance_id != ''){
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{url('admin-panel/book-allowance/manage/')}}",
        type: 'GET',
        data: {
          'book_allowance_id': book_allowance_id,
        },
        success: function (data) {
          $("#allow_for_staff").val(data.book_allowance.allow_for_staff);
          $("#allow_for_student").val(data.book_allowance.allow_for_student);
        }
      });
    }
    $('#bookAllowanceModel').modal('show');
  });

  $('#list-form').on('submit', function(e) { 
    var allow_for_staff   = $('#allow_for_staff').val();
    var allow_for_student = $('#allow_for_student').val();
    if(allow_for_staff != '' && allow_for_student != '') {
        var formData = $(this).serialize();
        console.log();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/book-allowance/save')}}",
            data     : formData,
            cache    : false,

            success  : function(data) {
                data = $.trim(data);
                if(data == "success"){
                    $("#success-block").html('Book Allowance updated successfully! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                    $("#success-block").show();
                    $("#error-block").hide();

                } else {
                    $("#error-block").html("No record found !!");
                    $("#error-block").show();
                    $("#success-block").hide();
                }
            }
        })
    } else {
        $("#error-block").html('Required field missing !! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $("#error-block").show();
    }
    e.preventDefault();
  });
</script>
@endsection