@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_student_report') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_student_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="tab-content">
                <div class="tab-pane active" id="classlist">
                    <div class="card">
                        <div class="body">
                            <!--  All Content here for any pages -->
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Examination/Exam-wise mark-sheet template_new.svg') !!}" alt="Certificates">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Attendance Report</div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{!! URL::to('admin-panel/student-report/student-attendance-report') !!}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Examination/Exam-wise mark-sheet template_new.svg') !!}" alt="Competition Certificates">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Exam Wise Marks Report </div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{{ url('/admin-panel/student-report/examwise-marks-report') }}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Examination/Subject-wise.svg') !!}" alt="Character Certificates">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Subject Wise Marks Report</div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{{ url('/admin-panel/student-report/subjectwise-marks-report') }}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection