@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card{
    background: transparent !important;
    }
    section.content{
    background: #f0f2f5 !important;
    }
</style>
<!--  Main content here -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.menu_student') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_student') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body">
                                <!--  All Content here for any pages -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/ViewList.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Student</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/student/add-student') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="{{ url('/admin-panel/student/view-list') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/ParentInformation.svg') !!}" alt="Student" >
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Parent Information</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="student/parent-information" class="cusa" title="Parents ">
                                            <i class="fas fa-eye"></i>Parents 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/Attendence.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Attendance</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/student/attendance') }}" class="cusa" title="View " style="width: 38%; margin: 20px auto;">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/LeaveManagement.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Leave Management</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="student-leave-application/manage-leave-application" class="cusa" title="Manage ">
                                            <i class="fas fa-eye"></i>Manage 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                
                                    <!-- <div class="col-md-3">
                                        <div class="dashboard_div" >
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/Group.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Groups</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/groups/add-group') }}" class="cusa" title="Add" style="width: 50%; margin: 20px auto;">
                                            <i class="fas fa-plus"></i>Add Group 
                                            </a>
                                            <ul class="header-dropdown opendiv">
                                                <li class="dropdown">
                                                    <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                                        <li>
                                                            <a href="{{ url('admin-panel/groups/view-student-groups') }}" title="">Student Group </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('admin-panel/groups/view-parent-groups') }}" title="">Parent Group  </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <div class="row clearfix"></div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/DairyRemarks.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Dairy Remarks</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/dairy-remarks/remarks') }}" class="cusa" title="View " style="width: 38%; margin: 20px auto;">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/homework_group.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">HomeWork Group</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('admin-panel/student/homework-group') }}" class="cusa" title="View " style="width: 38%; margin: 20px auto;">
                                            <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Student/homework_group.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Import Students</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/student/import-students') }}" class="float-left" title="Import ">
                                            <i class="fas fa-plus"></i> Import 
                                            </a>
                                            <a href="{{ url('/public/Students.xls') }}" target="_blank" class="float-right" title="Sample File ">
                                            <i class="fas fa-eye"></i>Sample file 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/certificates/transfer_certificate.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Transfer Certificate</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/student/issue-transfer-certificate') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Issue 
                                            </a>
                                            <a href="{{ url('/admin-panel/student/view-issue-transfer-certificate') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View Issed TC 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/certificates/character_certificate.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Character Certificate</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/student/issue-character-certificate') }}" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Issue 
                                            </a>
                                            <a href="{{ url('/admin-panel/student/view-issue-character-certificate') }}" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View Issed CC 
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="dashboard_div">
                                            <div class="imgdash">
                                                <img src="{!! URL::to('public/assets/images/Examination/Report.svg') !!}" alt="Student">
                                            </div>
                                            <h4 class="">
                                                <div class="tableCell" style="height: 64px;">
                                                    <div class="insidetable">Report</div>
                                                </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ url('/admin-panel/student-report') }}" class="cusa" title="View " style="width: 48%; margin: 20px auto;">
                                            <i class="fas fa-eye"></i> Manage
                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection