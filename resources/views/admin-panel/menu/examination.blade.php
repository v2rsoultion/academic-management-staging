@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Examination And Report Cards</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <!-- <li class="breadcrumb-item">{!! trans('language.menu_examination') !!}</li> -->
          <li class="breadcrumb-item">Examination </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Examination/Terms.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Terms</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="examination/manage-term" class="cusa" title="Manage Term ">
                          <i class="fas fa-plus"></i> Manage  
                        </a>
                      
                        <div class="clearfix"></div>
                        
                      </div>
                    </div>
                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Examination/exam.svg') !!}" alt="Exam">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Exam</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="examination/manage-exam" class="cusa" title="Manage Exam ">
                          <i class="fas fa-plus"></i> Manage 
                        </a>
                      
                        <div class="clearfix"></div>
                        
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Examination/GradeScheme.svg') !!}" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Grade Scheme</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="grade-scheme/add-grade-scheme" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="grade-scheme/view-grade-scheme" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Examination/Markscriteria.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Marks criteria</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="examination/manage-marks-criteria" class="cusa" title="Manage Exam ">
                              <i class="fas fa-plus"></i> Manage 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Examination/MapExamTClassSubjects.svg') !!}" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Map Exam To Class Subjects</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="{{ url('/admin-panel/examination/map-exam-class-subjects') }}" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Manage 
                              </a>
                              <a href="{{ url('/admin-panel/examination/exam-class-subject-report') }}" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>Mapping Report
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        
                          <div class="col-md-4">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Examination/MapMarkCriteriatoSubjects.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Map Mark Criteria to Subjects</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="{{ url('/admin-panel/examination/manage-markcriteria-subject') }}" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Manage 
                                </a>
                                <a href="{{ url('/admin-panel/examination/markscriteria-map-report') }}" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>Mapping Report 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Examination/MapGradeSchemetoSubjects.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Map Grade Scheme to Subjects</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('/admin-panel/examination/manage-grade-scheme-subject') }}" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Manage 
                                  </a>
                                  <a href="{{ url('/admin-panel/examination/gradescheme-map-report') }}" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>Mapping Report 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                                
                                <div class="col-md-3">
                                  <div class="dashboard_div">
                                    <div class="imgdash">
                                      <img src="{!! URL::to('public/assets/images/Examination/ExamSchedule.svg') !!}" alt="Student">
                                      </div>
                                      <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                          <div class="insidetable">Exam Schedule</div>
                                        </div>
                                      </h4>
                                      <div class="clearfix"></div>
                                      <a href="{{ url('/admin-panel/examination/schedules') }}" class="cusa" title="Schedules">
                                        <i class="fas fa-plus"></i> Schedules  
                                      </a>
                                    
                                      <div class="clearfix"></div>
                                    </div>
                                  </div>
                               
                                  <div class="col-md-3">
                                    <div class="dashboard_div">
                                      <div class="imgdash">
                                        <img src="{!! URL::to('public/assets/images/Examination/Marks.svg') !!}" alt="Marks">
                                        </div>
                                        <h4 class="">
                                          <div class="tableCell" style="height: 64px;">
                                            <div class="insidetable">Marks</div>
                                          </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{{ url('/admin-panel/examination/manage-marks') }}" class="cusa" title="Manage Marks">
                                        <i class="fas fa-plus"></i> Manage Marks  
                                        </a>
                                      </div>
                                    </div>
                                  <div class="col-md-3">
                                    <div class="dashboard_div">
                                      <div class="imgdash">
                                        <img src="{!! URL::to('public/assets/images/Examination/GenerateReportCard.svg') !!}" alt="Student">
                                        </div>
                                        <h4 class="">
                                          <div class="tableCell" style="height: 64px;">
                                            <div class="insidetable">Generate Report Card</div>
                                          </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                      
                                        <a href="{{ url('/admin-panel/examination/manage-report-card') }}" class="cusa" title="Manage ">
                                          <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                  </div>

                                  <div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Examination/Marksheet11.svg') !!}" alt="Certificates">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Marksheet Templates</div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{!! URL::to('admin-panel/marksheet-templates') !!}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                  </div>

                                  <div class="col-md-3">
                                    <div class="dashboard_div">
                                        <div class="imgdash">
                                            <img src="{!! URL::to('public/assets/images/Examination/Tabsheet.svg') !!}" alt="Certificates">
                                        </div>
                                        <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Tabsheet</div>
                                            </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{!! URL::to('admin-panel/examination/tabsheet') !!}" class="cusa" title="Manage ">
                                        <i class="fas fa-eye"></i>Manage 
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                  </div>                                  
                                  <div class="col-md-3">
                                  <div class="dashboard_div">
                                    <div class="imgdash">
                                      <img src="{!! URL::to('public/assets/images/Examination/Report.svg') !!}" alt="Student">
                                      </div>
                                      <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                          <div class="insidetable">Report</div>
                                        </div>
                                      </h4>
                                      <div class="clearfix"></div>
                                      <a href="{!! URL::to('admin-panel/examination-report') !!}" class="cusa" title="View ">
                                      <i class="fas fa-eye"></i>Manage 
                                      </a>
                                      <div class="clearfix"></div>
                                    </div>
                                  </div>
                                </div>
                                  
                                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
@endsection

