@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_staff') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item">{!! trans('language.menu_staff') !!}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Staff/information.svg') !!}" alt="Staff">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Staff</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="staff/add-staff" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="staff/view-staff" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Staff/Attendence.svg') !!}" alt="Attendance" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Attendance</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="{{ url('/admin-panel/staff/attendance/add-attendance') }}" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="{{ url('/admin-panel/staff/attendance/view-attendance') }}" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Staff/LeaveManagement.svg') !!}" alt="Leave Management">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Leave Management</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="staff-leave-application/manage-leave-application" class="cusa" title="Manage ">
                              <i class="fas fa-eye"></i>Manage 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Student/staff_roles.svg') !!}" alt="Staff Roles">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Staff Roles</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="staff/manage-staff-roles" class="cusa" title="Manage ">
                                <i class="fas fa-plus"></i> Manage 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                        <div class="row">

                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/schedule_icon.svg') !!}" alt="Plan Schedule">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Plan Schedule</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="staff/plan-schedule/add-plan-schedule" class="float-left" title="Add">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="staff/plan-schedule/view-plan-schedule" class="float-right" title="View">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Staff/Performance.svg') !!}" alt="Performance">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Performance</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="" class="cusa" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dashboard_div">
                                    <div class="imgdash">
                                        <img src="{!! URL::to('public/assets/images/Student/homework_group.svg') !!}" alt="Student">
                                    </div>
                                    <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                            <div class="insidetable">Import Staff</div>
                                        </div>
                                    </h4>
                                    <div class="clearfix"></div>
                                    <a href="{{ url('/admin-panel/staff/import-staff') }}" class="float-left" title="Import ">
                                    <i class="fas fa-plus"></i> Import 
                                    </a>
                                    <a href="{{ url('/public/Staff.xls') }}" target="_blank" class="float-right" title="Sample File ">
                                    <i class="fas fa-eye"></i>Sample file 
                                    </a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Examination/Report.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Report</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="{{ url('/admin-panel/staff/staff-attendance-report') }}" class="cusa" title="View " style="width: 68%; margin: 20px auto;">
                                <i class="fas fa-eye"></i> Attendance Report
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>

                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </section>
@endsection