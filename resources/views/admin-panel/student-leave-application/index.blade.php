@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_leave_application') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/student-leave-application/add-leave-application') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student-leave-management') !!}">{!! trans('language.menu_leave_management') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_leave_application') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body form-gap">
                            @if(session()->has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session()->get('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="leaves-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.leave_date_range')}}</th>
                                            <th>{{trans('language.leave_days')}}</th>
                                            <th>{{trans('language.leave_app_attachment')}}</th>
                                            <th>{{trans('language.leave_app_reason')}}</th>
                                            <th>{{trans('language.leave_app_status')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#leaves-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "{{url('admin-panel/student-leave-application/data')}}",
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'date_range', name: 'date_range'},
                {data: 'leave_days', name: 'leave_days'},
                {data: 'student_leave_attachment', name: 'student_leave_attachment'},
                {data: 'student_leave_reason', name: 'student_leave_reason'},
                {data: 'student_leave_status', name: 'student_leave_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




