@if(isset($notice['notice_id']) && !empty($notice['notice_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('notice_id',old('notice_id',isset($notice['notice_id']) ? $notice['notice_id'] : ''),['class' => 'gui-input', 'id' => 'notice_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.notice_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('notice_name', old('notice_name',isset($notice['notice_name']) ? $notice['notice_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.notice_name'), 'id' => 'notice_name']) !!}
        </div>
    </div>
    
    
    <div class="col-lg-9 col-md-9">
        <lable class="from_one1">{!! trans('language.notice_text') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('notice_text',old('notice_text',isset($notice['notice_text']) ? $notice['notice_text']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.notice_text'),'rows' => 5, 'cols' => 50)) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.for_class') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('for_class', $notice['arr_selection'],isset($notice['for_class']) ? $notice['for_class'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'for_class', 'onChange'=>'Show_data(this.value,0)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @php
    if(isset($notice['class_ids']) && $notice['class_ids'] != '') {
        $classStatus = "style='display: block'"; 
    } else {
        $classStatus = "style='display: none'"; 
    }
    @endphp
    <div class="col-lg-3 col-md-3" id="class_block" {!! $classStatus !!}>
        <lable class="from_one1">{!! trans('language.class_ids') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_ids[]', $notice['arr_class'],isset($notice['class_ids']) ? $notice['class_ids'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_ids','required','multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.for_staff') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('for_staff', $notice['arr_selection'],isset($notice['for_staff']) ? $notice['for_staff'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'for_staff', 'onChange'=>'Show_data1(this.value,1)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @php
    if(isset($notice['staff_ids']) && $notice['staff_ids'] != '') {
        $staffStatus = "style='display: block'"; 
    } else {
        $staffStatus = "style='display: none'"; 
    }
    @endphp
    <div class="col-lg-3 col-md-3" id="staff_block" {!! $staffStatus !!}>
        <lable class="from_one1">{!! trans('language.staff_ids') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_ids[]', $notice['arr_staff'],isset($notice['staff_ids']) ? $notice['staff_ids'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_ids','required','multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/notice-board') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#group-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                notice_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                class_ids: {
                    required: true,
                },
                staff_ids: {
                    required: true,
                },
                notice_text: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    function Show_data(typeVal,type){
        
        if(typeVal == 1){
            if(type == 0){
                $('#class_block').show();
            } else {
                $('#class_block').hide();
                $('#class_ids').val('');
            }
        } else {
            $('#class_block').hide();
            $('#class_ids').val('');
        }
    }
    function Show_data1(typeVal,type){
        
        if(typeVal == 1){
            
            if(type == 1){
                $('#staff_block').show();
            } else {
                $('#staff_block').hide();
                $('#staff_ids').val('');
            }
        } else {
            $('#staff_block').hide();
            $('#staff_ids').val('');
        }
    }

    

</script>