<option value="">Select Stream</option>
@if(!empty($stream))
  @foreach($stream as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif