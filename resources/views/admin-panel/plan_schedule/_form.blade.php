@if(isset($plan_schedule['plan_schedule_id']) && !empty($plan_schedule['plan_schedule_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('plan_schedule_id',old('plan_schedule_id',isset($plan_schedule['plan_schedule_id']) ? $plan_schedule['plan_schedule_id'] : ''),['class' => 'gui-input', 'id' => 'plan_schedule_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_id', $plan_schedule['arr_staffs'],isset($plan_schedule['staff_id']) ? $plan_schedule['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.plan_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::date('plan_date', old('plan_date',isset($plan_schedule['plan_date']) ? $plan_schedule['plan_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.plan_date'), 'id' => 'plan_date']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.plan_time') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::time('plan_time', old('plan_time',isset($plan_schedule['plan_time']) ? $plan_schedule['plan_time']: ''), ['class' => 'form-control','placeholder'=>trans('language.plan_time'), 'id' => 'plan_time']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">
    <div class="col-md-12 col-lg-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.plan_description') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('plan_description', old('plan_description',isset($plan_schedule['plan_description']) ? $plan_schedule['plan_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.plan_description'), 'id' => 'plan_description', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/staff') !!}" class="btn btn-raised" >Cancel</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#plan-schedule-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_id: {
                    required: true
                },
                plan_date: {
                    required: true
                },
                plan_time: {
                    required: true
                },
                plan_description: {
                    required: true
                }
            },
            messages: {
                staff_role_id:{
                    required: 'The staff role field is required.'
                } 
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });

</script>