@if(isset($question_paper['question_paper_id']) && !empty($question_paper['question_paper_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('question_paper',old('question_paper',isset($question_paper['question_paper_id']) ? $question_paper['question_paper_id'] : ''),['class' => 'gui-input', 'id' => 'question_paper_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info Question Paper -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('name', old('question_paper_name',isset($question_paper['question_paper_name']) ? $question_paper['question_paper_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.question_paper_name'), 'id' => 'qp_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.question_paper_class_id') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id',$question_paper['arr_class'],isset($question_paper['class_id']) ? $question_paper['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value) || getSubject(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.question_paper_section_id') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('qp_section_id', $question_paper['arr_section'],isset($question_paper['section_id']) ? $question_paper['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'qp_section_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.question_paper_subject_id') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%" >
                {!!Form::select('qp_subject_id', $question_paper['arr_subject_mapping'],isset($question_paper['subject_id']) ? $question_paper['subject_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'qp_subject_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.question_paper_exam_type_id') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('exam_id', $question_paper['arr_exam_type'],isset($question_paper['exam_id']) ? $question_paper['exam_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_file_upload') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="documents" id="question_paper_document" >
                <input type="hidden" class="gui-input" id="question_paper_document" placeholder="Upload Photo" readonly>
            </label>
        </div>
    </div>
    @if($doc_path != '')
    <div class="col-lg-3 col-md-3 document_file1">
        <a href="{{url($doc_path)}}" target="_blank">View Document</a>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/online-content') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<style type="text/css">

.field file state-error{
    width: 100% !important;
}
.state-error{
    width: 100% !important;
}
.document_file1 {
    margin-top: 20px;
}
.document_file1 a{
    text-decoration: none !important;
    color: #1f2f60 !important;
}


</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#question-paper-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                class_id: {
                    required: true
                },
                qp_section_id: {
                    required: true
                },
                qp_subject_id: {
                    required: true
                },
                exam_id: {
                    required: true
                },
                documents: {
                    required: function (e) {
                        if($('#question_paper_id').val() != ''){
                            return false;
                        }else{
                            return true;
                        }
                    }
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>

<script type="text/javascript">


function getSection(class_id)
    {
        if(class_id != "") {
        $('.mycustloading').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/question-paper/get-section-data')}}",
            type: 'GET',
            data: {
                'class_id': class_id
            },
            success: function (data) {
                    $("select[name='qp_section_id'").html('');
                    $("select[name='qp_section_id'").html(data.options);
                    $("select[name='qp_section_id'").removeAttr("disabled");
                    $("select[name='qp_section_id'").selectpicker('refresh');
                    $('.mycustloading').hide();
            }
        });
    } else {
            $("select[name='qp_section_id'").html(''); 
            $("select[name='qp_section_id'").selectpicker('refresh');
        }
    }


</script>
<script type="text/javascript">

function getSubject(class_id)
    {
        if(class_id != "") {
        $('.mycustloading').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/question-paper/get-subject-data')}}",
            type: 'GET',
            data: {
                'class_id': class_id
            },
            success: function (data) {
                    $("select[name='qp_subject_id'").html('');
                    $("select[name='qp_subject_id'").html(data.options);
                    $("select[name='qp_subject_id'").removeAttr("disabled");
                    $("select[name='qp_subject_id'").selectpicker('refresh');
                    $('.mycustloading').hide();
            }
        });
    } else {
            $("select[name='qp_subject_id'").html(''); 
            $("select[name='qp_subject_id'").selectpicker('refresh');
        }
    }




</script>