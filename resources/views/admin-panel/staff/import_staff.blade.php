@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.import_staff') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
               
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff') !!}">{!! trans('language.menu_staff') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.import_staff') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'import-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <lable class="from_one1">{!! trans('language.import_file') !!} <span class="red-text">*</span> :</lable>
                                            <div class="form-group">
                                                <label for="file1" class="field file">
                                                    <input type="file" class="gui-file" name="import_file" id="import_file" >
                                                </label>
                                            </div>
                                           <p>Please upload only xls file format.</p>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Import', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'import']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                               
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" style="margin-top: 30px;">
            <div class="pull-left" style="width: 24%; margin-left: 2%">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                        <div class="card ">
                            <ul class="ul_underline_1">
                                <li class="nav-item imgclass">
                                    <a class="nav-link  active active123" data-toggle="tab" href="#designation" onClick="toggleBlock('designation');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Designation_Category.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Designation </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#staff_roles" onClick="toggleBlock('staff_roles');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Student/staff_roles.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Staff Roles </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#caste" onClick="toggleBlock('caste');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Caste.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Caste </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#religion" onClick="toggleBlock('religion');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Religion.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Religion </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#nationality" onClick="toggleBlock('nationality');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Nationality.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Nationality </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#title" onClick="toggleBlock('title');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Title.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Title </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item imgclass">
                                    <a class="nav-link active123 " data-toggle="tab" href="#shift" onClick="toggleBlock('shift');">
                                        <div class="pro_image_tabs1">
                                            <div class="tab_images2123">
                                                <img src="{!! URL::to('public/assets/images/Configuration/Shift.svg') !!}" class="gap" width="35px">
                                            </div>
                                            <div class="tab_images3123"> Shift </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <!--  <div class="ui_marks1"></div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="pull-right" style="width: 72%;">
                <div class="">
                    <div class="tab-content">
                        <div class="tab-pane body active" id="designation" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Designation</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.designation')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_designations'] as $designationKey => $designation)
                                                                                        <tr>
                                                                                            <td>{{$designationKey}}</td>
                                                                                            <td>{{$designation}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="staff_roles" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Staff Roles</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.staff_role')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_staff_roles'] as $roleKey => $role)
                                                                                        <tr>
                                                                                            <td>{{$roleKey}}</td>
                                                                                            <td>{{$role}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="shift" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Shift</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.shift')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_shift'] as $shiftKey => $shift)
                                                                                        <tr>
                                                                                            <td>{{$shiftKey}}</td>
                                                                                            <td>{{$shift}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="caste" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Caste</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.catse')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_caste'] as $casteKey => $catse)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$casteKey}}</td>
                                                                                        <td>{{$catse}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="religion" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Religion</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.religion')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_religion'] as $religionKey => $religion)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$religionKey}}</td>
                                                                                        <td>{{$religion}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="title" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Title</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.ID')}}</th>
                                                                                        <th>{{trans('language.title')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($info['arr_title'] as $titleKey => $title)
                                                                                        <tr>
                                                                                            <td>{{$titleKey}}</td>
                                                                                            <td>{{$title}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane body" id="nationality" style="margin-left: 20px !important;">
                            <div class="">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 tab2">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="classlist">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="card border_info_tabs_1 card_top_border1" id="">
                                                            <div class="body" >
                                                                <div class="tab-content" style="background-color: #fff;">
                                                                    <div class="nav_item7">
                                                                        <div class="nav_item1">
                                                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Nationality</a>
                                                                        </div>
                                                                        <div class="nav_item2">
                                                                            <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                                                            {{ csrf_field() }}
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>{{trans('language.s_no')}}</th>
                                                                                        <th>{{trans('language.natioanlity')}}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @foreach($info['arr_natioanlity'] as $natioanlityKey => $natioanlity)
                                                                                
                                                                                    <tr>
                                                                                        <td>{{$natioanlityKey}}</td>
                                                                                        <td>{{$natioanlity}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
     jQuery(document).ready(function () {
        $("#import-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                class_id: {
                    required: true
                },
                section_id: {
                    required: true
                },
                import_file: {
                    extension: 'xls',
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
        $("#import_file").change(function() {
            var val = $(this).val();
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                case 'xls':
                    $('#error-block').hide();
                    $('#error-block').html('');
                    $(':input[type="submit"]').prop('disabled', false);
                    break;
                default:
                    $(this).val('');
                    // error message here
                    $('#error-block').show();
                    $('#error-block').html('Please select only xlsx file for import');
                    $(':input[type="submit"]').prop('disabled', true);
                    break;
            }
        });
    });

    function toggleBlock(id) {
        $('html, body').animate({
            scrollTop: $("#"+id).offset().top
        }, 1000);
    }

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }
</script>
@endsection




