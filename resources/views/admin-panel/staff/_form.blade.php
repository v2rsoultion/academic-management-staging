@if(isset($staff['staff_id']) && !empty($staff['staff_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('staff_id',old('staff_id',isset($staff['staff_id']) ? $staff['staff_id'] : ''),['class' => 'gui-input', 'id' => 'staff_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_name', old('staff_name',isset($staff['staff_name']) ? $staff['staff_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_name'), 'id' => 'staff_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_designation_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('designation_id', $staff['arr_designations'],isset($staff['designation_id']) ? $staff['designation_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'designation_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_role_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_role_id[]', $staff['arr_staff_roles'],isset($staff['staff_role_id']) ? $staff['staff_role_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_role_id', 'multiple','required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
    <span class="radioBtnpan">{{trans('language.staff_profile_img')}}</span>
        <label for="file1" class="field file">
            
            <input type="file" class="gui-file" name="staff_profile_img" id="file1" onChange="document.getElementById('staff_profile_img').value = this.value;">
            <input type="hidden" class="gui-input" id="staff_profile_img" placeholder="Upload Photo" readonly>
        </label>
        <br />
        @if(isset($staff['staff_profile_img']))<a href="{{url($staff['staff_profile_img'])}}" target="_blank" >View Image</a>@endif
    </div>

</div>


<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_email') !!}  :</lable>
        <div class="form-group">
            {!! Form::text('staff_email', old('staff_email',isset($staff['staff_email']) ? $staff['staff_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_email'), 'id' => 'staff_email']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_mobile_number') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_mobile_number', old('staff_mobile_number',isset($staff['staff_mobile_number']) ? $staff['staff_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_mobile_number'), 'id' => 'staff_mobile_number','minlength'=> '10', 'maxlength'=> '10']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_dob') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::date('staff_dob', old('staff_dob',isset($staff['staff_dob']) ? $staff['staff_dob']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_dob'), 'id' => 'staff_dob']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_gender') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_gender', $staff['arr_gender'],isset($staff['staff_gender']) ? $staff['staff_gender'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_gender'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        
    </div>

</div>

<!-- Other Information -->
<div class="header">
    <h2><strong>Official</strong> Information</h2>
</div>

<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_aadhar') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_aadhar', old('staff_aadhar',isset($staff['staff_aadhar']) ? $staff['staff_aadhar']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_aadhar'), 'minlength'=> '12','maxlength'=> '12','id' => 'staff_aadhar']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_UAN') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_UAN', old('staff_UAN',isset($staff['staff_UAN']) ? $staff['staff_UAN']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_UAN'), 'id' => 'staff_UAN','minlength'=> '12','maxlength'=> '12']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_ESIN') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_ESIN', old('staff_ESIN',isset($staff['staff_ESIN']) ? $staff['staff_ESIN']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_ESIN'), 'id' => 'staff_ESIN']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_PAN') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_PAN', old('staff_PAN',isset($staff['staff_PAN']) ? $staff['staff_PAN']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_PAN'), 'id' => 'staff_PAN']) !!}
        </div>
    </div>

</div>

<!-- Other Information -->
<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_father_name_husband_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_father_name_husband_name', old('staff_father_name_husband_name',isset($staff['staff_father_name_husband_name']) ? $staff['staff_father_name_husband_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_father_name_husband_name'), 'id' => 'staff_father_name_husband_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_father_husband_mobile_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_father_husband_mobile_no', old('staff_father_husband_mobile_no',isset($staff['staff_father_husband_mobile_no']) ? $staff['staff_father_husband_mobile_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_father_husband_mobile_no'), 'id' => 'staff_father_husband_mobile_no', 'maxlength' => '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_mother_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_mother_name', old('staff_mother_name',isset($staff['staff_mother_name']) ? $staff['staff_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_mother_name'), 'id' => 'staff_mother_name']) !!}
        </div>
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_blood_group') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_blood_group', old('staff_blood_group',isset($staff['staff_blood_group']) ? $staff['staff_blood_group']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_blood_group'), 'id' => 'staff_blood_group']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.caste_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('caste_id', $staff['arr_caste'],isset($staff['caste_id']) ? $staff['caste_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'caste_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.religion_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('religion_id', $staff['arr_religion'],isset($staff['religion_id']) ? $staff['religion_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'religion_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.nationality_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('nationality_id', $staff['arr_natioanlity'],isset($staff['nationality_id']) ? $staff['nationality_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'nationality_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_marital_status') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_marital_status', $staff['arr_marital'],isset($staff['staff_marital_status']) ? $staff['staff_marital_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_marital_status'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.title') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('title_id', $staff['arr_title'],isset($staff['title_id']) ? $staff['title_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'title_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.shift') !!} :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('shift_ids[]', $staff['arr_shift'],isset($staff['shift_ids']) ? $staff['shift_ids'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'shift_ids','multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.teaching_status') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('teaching_status', $staff['arr_teaching_status'],isset($staff['teaching_status']) ? $staff['teaching_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'teaching_status'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>

<!-- Temporary Address Information -->
<div class="header">
    <h2><strong>Qualification and Experience</strong> Information</h2>
</div>

<!-- Specialization section -->
<div class="row clearfix">
    <div class="col-md-4 col-lg-4 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_specialization') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_specialization', old('staff_specialization',isset($staff['staff_specialization']) ? $staff['staff_specialization']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_specialization'), 'id' => 'staff_specialization', 'rows'=> 2]) !!}
        </div>
    </div>

    <div class="col-md-4 col-lg-4 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_qualification') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_qualification', old('staff_qualification',isset($staff['staff_qualification']) ? $staff['staff_qualification']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_qualification'), 'id' => 'staff_qualification', 'rows'=> 2]) !!}
        </div>
    </div>

    <div class="col-md-4 col-lg-4 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_reference') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_reference', old('staff_reference',isset($staff['staff_reference']) ? $staff['staff_reference']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_reference'), 'id' => 'staff_reference', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- Experience section -->

<div id="exp-body">
@php $experienceCount = 1;$exp_counter = 0; @endphp

@if(isset($staff['staff_experience']) && $staff['staff_experience'] != '')
    @php
        $experience = json_decode($staff['staff_experience']);
        $experienceCount = COUNT($experience);
    @endphp
    @foreach($experience as $exp)
    <div id="exp_block{{$exp_counter}}">
        <div class="row clearfix">
            <div class="col-lg-10 col-md-10 text_area_desc">
                <lable class="from_one1">{!! trans('language.staff_experience') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::textarea('staff_experience[]', $exp, ['class' => 'form-control','placeholder'=>trans('language.staff_experience'),  'rows'=> 1, 'required']) !!}
                </div>
            </div>
            <div class="col-lg-2 col-md-2 text-right">
                @if($exp_counter == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addExpBlock()" style="margin-top: 30px !important;" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" style="margin-top: 30px !important;" onclick="remove_exp_block({{$exp_counter}})" >Remove</button>
                @endif
            </div>
        </div>
    </div>
    @php $exp_counter++ @endphp
    @endforeach
@else
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 text_area_desc">
            <lable class="from_one1">{!! trans('language.staff_experience') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::textarea('staff_experience[]', old('staff_experience',isset($staff['staff_experience']) ? $staff['staff_experience']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_experience'),  'rows'=> 1, 'required']) !!}
            </div>
        </div>
        <div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addExpBlock()"  style="margin-top: 30px !important;" >Add More</button></div>
    </div>

@endif

{!! Form::hidden('exp_hide',$experienceCount,['class' => 'gui-input', 'id' => 'exp_hide']) !!}
</div>

<!-- Temporary Address Information -->
<div class="header">
    <h2><strong>Temporary Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_temporary_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_temporary_address', old('staff_temporary_address',isset($staff['staff_temporary_address']) ? $staff['staff_temporary_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_temporary_address'), 'id' => 'staff_temporary_address', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_county') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_temporary_county', $staff['arr_country'],isset($staff['staff_temporary_county']) ? $staff['staff_temporary_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_temporary_county' , 'onChange'=> 'showStates(this.value,"staff_temporary_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_state') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_temporary_state', $staff['arr_state_t'],isset($staff['staff_temporary_state']) ? $staff['staff_temporary_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_temporary_state', 'onChange'=> 'showCity(this.value,"staff_temporary_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_city') !!} :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_temporary_city', $staff['arr_city_t'],isset($staff['staff_temporary_city']) ? $staff['staff_temporary_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_temporary_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>


    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_temporary_pincode', old('staff_temporary_pincode',isset($staff['staff_temporary_pincode']) ? $staff['staff_temporary_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_temporary_pincode'), 'id' => 'staff_temporary_pincode', 'maxlength'=> '6']) !!}
        </div>
    </div>

</div>

<!-- Permanent Address Information -->
<div class="header">
    <h2><strong>Permanent Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_permanent_address') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_permanent_address', old('staff_permanent_address',isset($staff['staff_permanent_address']) ? $staff['staff_permanent_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_permanent_address'), 'id' => 'staff_permanent_address', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- City section -->
<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_permanent_county') !!}  <span class="red-text">*</span>  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_permanent_county', $staff['arr_country'],isset($staff['staff_permanent_county']) ? $staff['staff_permanent_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_permanent_county' , 'onChange'=> 'showStates(this.value,"staff_permanent_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_permanent_state') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_permanent_state', $staff['arr_state_p'],isset($staff['staff_permanent_state']) ? $staff['staff_permanent_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_permanent_state', 'onChange'=> 'showCity(this.value,"staff_permanent_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_permanent_city') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group  ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_permanent_city', $staff['arr_city_p'],isset($staff['staff_permanent_city']) ? $staff['staff_permanent_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_permanent_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_permanent_pincode') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('staff_permanent_pincode', old('staff_permanent_pincode',isset($staff['staff_permanent_pincode']) ? $staff['staff_permanent_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_permanent_pincode'), 'id' => 'staff_permanent_pincode', 'maxlength'=> '6']) !!}
        </div>
    </div>

</div>

<!-- Document Information -->
<div class="header">
    <h2><strong>Document</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($staff['documents']) ? COUNT($staff['documents']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    @if(isset($staff['documents']) && !empty($staff['documents']))
    @php $key = 0; @endphp
    @foreach($staff['documents'] as $documentKey => $documents)
    @php 
        $staff_document_category = "documents[".$key."][document_category_id]";  
        $staff_document_id = "documents[".$key."][staff_document_id]"; 
    @endphp
    <div id="document_block{{$documentKey}}">
        {!! Form::hidden($staff_document_id,$documents['staff_document_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.staff_document_category') !!} :</lable>
                <div class="form-group ">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select($staff_document_category, $staff['arr_document_category'],$documents['document_category_id'], ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            
            </div>
            <div class="col-lg-5 col-md-5">
            <span class="radioBtnpan">{{trans('language.staff_document_file')}}</span><br />
                <label for="file1" class="field file">
                    
                    <input type="file" class="gui-file" name="documents[{{$key}}][staff_document_file]" id="staff_document_file{{$key}}" >
                    <input type="hidden" class="gui-input" id="staff_document_file{{$key}}" placeholder="Upload Photo" readonly>
                    
                    @if(isset($documents['staff_document_file']) && $documents['staff_document_file'] != "")<a href="{{url($documents['staff_document_file'])}}" target="_blank" >View Document</a>@endif
                </label>
            
            </div>
            <div class="col-lg-1 col-md-1">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($documentKey == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$documentKey}})" >Remove</button>
                @endif
            </div>
        </div>

        <!-- Document detail section -->
        <div class="row clearfix">
            <div class="col-sm-12 text_area_desc">
                <lable class="from_one1">{!! trans('language.staff_document_details') !!} :</lable>
                <div class="form-group">
                    <textarea name="documents[{{$key}}][staff_document_details]" id="" class="form-control" placeholder="{{trans('language.staff_document_details')}}" rows="2">{!! $documents['staff_document_details'] !!}</textarea>
                   
                </div>
                
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.staff_document_category') !!} :</lable>
            <div class="form-group ">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('documents[0][document_category_id]', $staff['arr_document_category'],isset($staff['document_category_id']) ? $staff['document_category_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.staff_document_file')}}</span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="documents[0][staff_document_file]" id="staff_document_file" onChange="document.getElementById('staff_document_file').value = this.value;">
                <input type="hidden" class="gui-input" id="staff_document_file" placeholder="Upload Photo" readonly>
               
            </label>
           
        </div>
        <div class="col-lg-3 col-md-3">&nbsp;</div>
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button></div>
    </div>

    <!-- Document detail section -->
    <div class="row clearfix">
        <div class="col-sm-12 text_area_desc">
            <lable class="from_one1">{!! trans('language.staff_document_details') !!} :</lable>
            <div class="form-group">
                {!! Form::textarea('documents[0][staff_document_details]', old('staff_document_details',isset($staff['staff_document_details']) ? $staff['staff_document_details']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_document_details'), 'id' => 'staff_document_details', 'rows'=> 2]) !!}
            </div>
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/staff') !!}" class="btn btn-raised" >Cancel</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#staff-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_profile_img: {
                    extension: 'jpg,png,jpeg,gif'
                },
                staff_aadhar: {
                    required: true,
                    minlength: 12,
                    digits: true
                },
                staff_UAN: {
                    minlength: 12,
                },
                designation_id: {
                    required: true
                },
                staff_role_id: {
                    required: true
                },
                staff_email: {
                    // required: true,
                    email: true,
                    emailvalid: true
                },
                staff_mobile_number: {
                    required: true,
                    digits: true
                },
                staff_dob: {
                    required: true,
                    date: true,
                    minDate: true,
                    maxlength:10
                },
                staff_father_name_husband_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_father_husband_mobile_no: {
                    required: true,
                    digits: true
                },
                staff_mother_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_marital_status: {
                    required: true
                },
                caste_id: {
                    required: true
                },
                nationality_id: {
                    required: true
                },
                religion_id: {
                    required: true
                },
                staff_qualification: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_specialization: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_permanent_address: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                staff_permanent_county: {
                    required: true
                },
                staff_permanent_state: {
                    required: true
                },
                staff_permanent_city: {
                    required: true
                },
                staff_permanent_pincode: {
                    digits: true,
                    required: true
                },
                staff_temporary_pincode: {
                    digits: true
                },
                staff_gender: {
                    required: true
                },
                title_id: {
                    required: true
                },
                shift_ids: {
                    // required: true
                },
                teaching_status: {
                    required: true
                }
                
            },
            messages: {
                staff_role_id:{
                    required: 'The staff role field is required.'
                } 
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

        // d = new Date();
        // d.setFullYear(d.getFullYear() - 10);
        // $('#staff_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false,maxDate : d }).on('change', function(ev) {
        //     $(this).valid();  
        // });
        
    });


    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b);
        
        $("#sectiontable-body").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans("language.student_document_category") !!} :</lable><div class="form-group "><label class=" field select" style="width: 100%">  <select class="form-control show-tick select_form1 select2" id="document_category_id'+arr_key+'" name="documents['+arr_key+'][document_category_id]">@foreach ($staff["arr_document_category"] as $document_key => $document_category) <option value="{{ $document_key}}">{!! $document_category !!}</option>@endforeach</select><i class="arrow double"></i></label></div></div><div class="col-lg-3 col-md-3"><span class="radioBtnpan">{{trans("language.staff_document_file")}}</span><label for="file1" class="field file"><input type="file" class="gui-file" name="documents['+arr_key+'][staff_document_file]" id="file'+arr_key+'" ><input type="hidden" class="gui-input" id="staff_document_file'+arr_key+'" placeholder="Upload Photo" readonly></label></div><div class="col-lg-3 col-md-3">&nbsp;</div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div><div class="row clearfix"><div class="col-sm-12 text_area_desc"><lable class="from_one1">{!! trans("language.staff_document_details") !!} :</lable><div class="form-group"><textarea name="documents['+arr_key+'][staff_document_details]" class="form-control" placeholder="{{trans("language.staff_document_details")}}" id="staff_document_details'+arr_key+'" rows="2" ></textarea></div> </div></div></div>');
        $("#document_category_id"+arr_key).select2();
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function addExpBlock() {
        var a = parseInt(document.getElementById('exp_hide').value);
        document.getElementById('exp_hide').value = a+1;
        var b = document.getElementById('exp_hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#exp-body").append('<div id="exp_block'+arr_key+'" ><div class="row clearfix"> <div class="col-lg-10 col-md-10 text_area_desc"><lable class="from_one1">{!! trans('language.staff_experience') !!} <span class="red-text">*</span> :</lable><div class="form-group"> {!! Form::textarea('staff_experience[]', '', ['class' => 'form-control','placeholder'=>trans('language.staff_experience'),  'rows'=> 1, 'required']) !!}</div> </div><div class="col-lg-2 col-md-2 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_exp_block('+arr_key+')" style="margin-top: 30px !important;">Remove</button></div></div></div>');
        
    }
    function remove_exp_block(id){
        $("#exp_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/state/show-state')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/city/show-city')}}",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

</script>