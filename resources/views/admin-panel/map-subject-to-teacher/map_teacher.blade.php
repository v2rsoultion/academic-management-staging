@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
.search_button4{
    margin-left: 20px !important;
}
/*.teacher_ids{
    color: red;
}*/

</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <h2>{!! trans('language.map_subject') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping') !!}">{!! trans('language.map_subject_to_teacher') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.map_subject') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                                
                            <div class="body form-gap">
                                <span class="classname_padding">Name : <strong>{{$section_info['section_name']}}</strong></span><br/>
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'class-teacher-allocation-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}

                                {!! Form::hidden('class_id',$section_info['class_id'],array('readonly' => 'true','id'=>'class_id')) !!}
                                {!! Form::hidden('section_id',$section_info['section_id'],array('readonly' => 'true','id'=>'section_id')) !!}
                                    <div class="table m-b-0 c_list" id="map-data">
                                        {{ csrf_field() }}
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row ">
                                            {!! Form::submit('Apply', ['class' => 'btn btn-raised btn-round btn-primary float-right search_button4','name'=>'save']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/teacher-subject-mapping/map-teacher-data')}}",
            type: 'GET',
            data: {class_id:class_id, section_id:section_id},
            success: function (data) {

                $('#map-data').html(data.data);
                $('.select2').select2();
            }
        });
           
        
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

    function assignteacher(id){
        var teacher_ids = [];
        var class_id    = $('#class_id').val();
        var section_id    = $('#section_id').val();
        $('#teacher_ids_'+id+' :selected').each(function(i, selectedElement) {
         teacher_ids[i]   = $(selectedElement).val();
        });
        
            if(confirm("Are you sure?")){
                $('.mycustloading').css('display','block');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/teacher-subject-mapping/save')}}",
                    type: 'GET',
                    data: {
                        'class_id': class_id,
                        'section_id': section_id,
                        'subject_id': id,
                        'teacher_ids': teacher_ids
                    },
                    success: function (data) {
                        $('.mycustloading').css('display','none');
                        if(data.status = 1001){
                            $('#display_cust_msg').html(data.message);
                        }else if(data.status = 1002){
                            $('#display_cust_msg2').html(data.message);
                        }   //window.location.reload(true);     
                    }
                });
            }
    }

</script>
@endsection




