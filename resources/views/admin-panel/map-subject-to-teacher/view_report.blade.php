@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <h2>{!! trans('language.mst_view_report') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping') !!}">{!! trans('language.map_subject_to_teacher') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.mst_view_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                                
                                <div class="body form-gap">
                                   
                                    <span class="classname_padding">Class Name : <strong>{!! $section_info['section_name']; !!}</strong></span>
                                    {!! Form::hidden('class_id',$section_info['class_id'],array('readonly' => 'true')) !!}
                                    {!! Form::hidden('section_id',$section_info['section_id'],array('readonly' => 'true')) !!}
                                    {!! Form::hidden('class_name',$section_info['section_name'],array('readonly' => 'true','id'=>'mycustomclassdet2')) !!}
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="view-report-table" style="width:100%">
                                        {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.ms_teacher_code')}}</th>
                                                    <th>{{trans('language.ms_teacher_name')}}</th>
                                                    <th>{{trans('language.mst_subject_code_name')}}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var class_details = $('#mycustomclassdet2').val();
        var table = $('#view-report-table').DataTable({
            dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            buttons: [
                {
                    extend: 'csvHtml5',
                    messageTop: 'Class : ' + class_details,
                    customize: function(doc){
                        return "Class : \t" + class_details + "\n\n" + doc;
                        }
                },
                {
                extend: 'print',
                messageTop: 'Class : ' + class_details,
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                    $(win.document.body).find('h1').css('font-size', '20px');                        
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                    }
                }
            ],
            ajax: {
                url: "{{url('admin-panel/teacher-subject-mapping/view-report-data')}}",
                data: function (d) {
                    d.class_id = $('input[name=class_id]').val();
                    d.section_id = $('input[name=section_id]').val();
                }
            },
            // ajax: "{{url('admin-panel/class-subject-mapping/view-report-data')}}",
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'teacher_code', name: 'teacher_code'},
                {data: 'teacher_name', name: 'teacher_name'},
                {data: 'subjects', name: 'subjects'},
                
            ],
             columnDefs: [
                {
                "targets": 0,
                "orderable": false
                },
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

</script>
@endsection




