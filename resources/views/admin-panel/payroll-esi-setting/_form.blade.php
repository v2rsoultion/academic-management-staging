@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row">
    <div class="headingcommon  col-lg-12">Employee State Insurance</div>
    <div class="row col-lg-12">
        <div class="col-lg-4">
            <lable class="from_one1">{!! trans('language.employee') !!}</lable>
            <div class="form-group">
                {!! Form::number('employee',old('employee',isset($esi_setting['employee']) ? $esi_setting['employee'] : ''),['class' => 'form-control','placeholder' => trans('language.employee'), 'id' => 'employee', 'step' => 0.1, 'min' => 0, 'max' => 100]) !!}
            </div>
        </div> 
        <div class="col-lg-4">
            <lable class="from_one1">{!! trans('language.employer') !!}</lable>
            <div class="form-group">
            {!! Form::number('employer',old('employer',isset($esi_setting['employer']) ? $esi_setting['employer'] : ''),['class' => 'form-control','placeholder' => trans('language.employer'), 'id' => 'employer', 'step' => 0.1, 'min' => 0, 'max' => 100]) !!}
            </div>
        </div> 
        <div class="col-lg-4">
            <lable class="from_one1">{!! trans('language.esi_cut_off') !!}</lable>
            <div class="form-group">
            {!! Form::number('esi_cut_off',old('esi_cut_off',isset($esi_setting['esi_cut_off']) ? $esi_setting['esi_cut_off'] : ''),['class' => 'form-control','placeholder' => trans('language.esi_cut_off'), 'id' => 'esi_cut_off', 'step' => 1, 'min' => 0]) !!}
            </div>
        </div> 
    </div>
    <div class="col-lg-12">
        <lable class="from_one1">Round Off</lable>
        <div class="form-group">
            <div class="radio" style="margin-top:6px !important;">
                <input id="radio34" name="round_off" type="radio" value=1 checked="true">
                <label for="radio34" class="document_staff">Highest Rupee</label>
                <input id="radio35" name="round_off" type="radio" value=0>
                <label for="radio35" class="document_staff">Nearest Rupee</label>
            </div>
        </div>
    </div>
</div>
<div class="row col-lg-12 padding-0">
    <div class="col-lg-1">
        <button class="btn btn-raised btn-primary">Save</button>
    </div>
    <div class="col-lg-1 text-right">
    <a href="{{ url('admin-panel/payroll/manage-esi-setting') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_esi_setting").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                employee: {
                    required: true
                },
                employer: {
                    required: true
                },
                esi_cut_off: {
                    required: true
                }, 
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
    });
</script>