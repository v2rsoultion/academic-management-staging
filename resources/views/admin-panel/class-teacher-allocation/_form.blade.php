@if(isset($allocation['staff_class_allocation_id']) && !empty($allocation['staff_class_allocation_id']))
<?php  $readonly = true; $disabled = ''; ?>
@else
<?php $readonly = false; $disabled='disabled'; ?>
@endif

{!! Form::hidden('staff_class_allocation_id',old('staff_class_allocation_id',isset($allocation['staff_class_allocation_id']) ? $allocation['staff_class_allocation_id'] : ''),['class' => 'gui-input', 'id' => 'staff_class_allocation_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.allot_class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $allocation['arr_class'],isset($allocation['class_id']) ? $allocation['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>        
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.allot_section_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('section_id', $allocation['arr_section'],isset($allocation['section_id']) ? $allocation['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','required',$disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.allot_teacher_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('teacher_id', $allocation['arr_staff'],isset($allocation['staff_id']) ? $allocation['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'teacher_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/academic') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        $("#class-teacher-allocation-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                
                class_id: {
                    required: true
                },
                section_id:{
                    required: true,
                },
                teacher_id:{
                    required: true
                }
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $("select[name='class_id'").removeAttr("disabled");
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }
    
    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $('.mycustloading').css('display','none');
                }
            });
        } else {
            $("select[name='section_id'").html('');
        }
    }
    

</script>