<option value="">Select Room</option>
@if(!empty($room))
  @foreach($room as $key => $value)
    <option value="{{ $key }}" {{ ($key == $hostel_room_id) ? 'selected' : '' }}>{{ $value }}</option>
  @endforeach
@endif