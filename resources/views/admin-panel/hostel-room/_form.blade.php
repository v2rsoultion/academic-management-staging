@if(isset($room['h_room_id']) && !empty($room['h_room_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('h_room_id',old('h_room_id',isset($room['h_room_id']) ? $room['h_room_id'] : ''),['class' => 'gui-input', 'id' => 'hostel_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.hostel_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('hostel_id', $room['arr_hostels'],isset($room['hostel_id']) ? $room['hostel_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'hostel_id','onChange' => 'getBlock(this.value)'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_block_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('h_block_id', $room['arr_block'],isset($room['h_block_id']) ? $room['h_block_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_block_id','onChange' => 'getFloor(this.value)'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_floor_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('h_floor_id', $room['arr_floor'],isset($room['h_floor_id']) ? $room['h_floor_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_floor_id'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_room_cate_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group ">
            <label class=" field select" style="width: 100%">
                {!!Form::select('h_room_cate_id', $room['arr_room_cate'],isset($room['h_room_cate_id']) ? $room['h_room_cate_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'h_room_cate_id'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_room_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('h_room_no', old('h_room_no',isset($room['h_room_no']) ? $room['h_room_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_room_no'), 'id' => 'h_room_no']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_room_alias') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('h_room_alias', old('h_room_alias',isset($room['h_room_alias']) ? $room['h_room_alias']: ''), ['class' => 'form-control','placeholder'=>trans('language.h_room_alias'), 'id' => 'h_room_alias']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.h_room_capacity') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('h_room_capacity', old('h_room_capacity',isset($room['h_room_capacity']) ? $room['h_room_capacity']: '0'), ['class' => 'form-control','placeholder'=>trans('language.h_room_capacity'), 'id' => 'h_room_capacity', 'min'=> 0]) !!}
        </div>
    </div>
    

    <div class="col-lg-3 col-md-3">
        <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/hostel') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#room-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                h_room_no: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                h_room_alias: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                h_room_capacity: {
                    required: true,
                    digits:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                hostel_id: {
                    required: true,
                },
                h_block_id: {
                    required: true,
                },
                h_floor_id: {
                    required: true,
                },
                h_room_cate_id: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });
    function getBlock(hostel_id)
    {
        if(hostel_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-block-data')}}",
                type: 'GET',
                data: {
                    'hostel_id': hostel_id
                },
                success: function (data) {
                    $("select[name='h_block_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_block_id'").html('');
            $(".mycustloading").hide();
        }
    }
    function getFloor(h_block_id)
    {
        if(h_block_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/hostel/configuration/get-floor-data')}}",
                type: 'GET',
                data: {
                    'h_block_id': h_block_id
                },
                success: function (data) {
                    $("select[name='h_floor_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='h_floor_id'").html('');
            $(".mycustloading").hide();
        }
    }
    

</script>