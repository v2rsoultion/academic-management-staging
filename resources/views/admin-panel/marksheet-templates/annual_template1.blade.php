<!DOCTYPE html>
<html>
<head>
	<title>Report-Card-02 Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		margin-bottom: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 14px;
		/*font-style: italic;*/
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border: 1px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 14px;
		/*font-style: italic;*/
	}
	.dotted {
		/*border-bottom: 2px dotted #000;*/
		font-size: 14px;
		/*font-style: italic;*/
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	/*.sub_heading{
		margin-top:0px;
		margin-bottom: 1px;
		font-size: 20px;
		color:#1f2f60;
		font-weight: bold;
		font-style: italic;
	}*/
	.affiliate {
		font-size:14px; 
		width: 230px;
		text-align: center; 
		font-weight: bold;
	}
	.bold {
		font-weight: bold;
	}
	tr{
		height: 25px;
	}
	#border tr {
		border: 1px solid #000;
	} 
	#border td {
		border: 1px solid #000;
		
	}

	</style>
</head>
<body>
	
	<div class="header">
		<table>
			<tr>
				<td style="width: 150px;" class="align"><img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px" style="margin-top: 15px;"></td>
				<td style="width: 800px">
					<h3 class="align header_gap" style="margin-top:10px;margin-bottom: 0px;">Golden Valley School</h3>
					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> Affiliated to CBSE,  Affiliation No.&nbsp; 124568</p>	
					<p class="align" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> Hanuman Gali, Pushkar, Rajasthan <br> Ph. +91 999 999 9999, &nbsp;&nbsp; E-mail : demo@demomail.com</p>	
				</td>
  			</tr>
		</table>
		<h6 class="align" style="margin-top:10px;margin-bottom: 0px;">Academic Session: 2017-2018</h6>
		<h6 class="align" style="margin-top:5px;margin-bottom: 0px;">Report card</h6>
		
		

		<table style="margin: 0px 60px;">
			<tr>
				<td style="width: 110px;" class="content_size">Student's Name</td>
				<td style="width: 300px;" class="content_size">&nbsp; Amit Kumar</td>
				<td style="width: 200px;"></td>
				<td style="width: 120px;" class="content_size">Roll No. </td>
				<td style="width: 90px;" class="content_size">1</td>
			</tr>
			<tr>
				<td class="content_size">Father's Name</td>
				<td class="content_size">&nbsp; Naimesh Mehta</td>
			</tr>
			<tr>
				<td class="content_size">Mother's Name</td>
				<td class="content_size">&nbsp; Sheetal Rani</td>
			</tr>
			<tr>
				<td class="content_size">Date of Birth</td>
				<td class="content_size">&nbsp; 11/11/2004</td>
			</tr>
			<tr>
				<td class="content_size">Class/Section</td>
				<td class="content_size">&nbsp; XI A</td>
				
			</tr>
		</table>
		<table style="margin: 0px 60px;font-size: 14px;width: 88%;" class="align" id="border" cellspacing="0px";>
			<tr style="background-color: #f8e503de;" class="bold">	
				<td>Scholastic Areas</td>
				<td colspan="6">Term-1 (100 marks)</td>
				<td colspan="6">Term-2 (100 marks)</td>
			</tr>
			<tr style="background-color: #75e5db;" class="bold">
				<td rowspan="2">Sub Name</td>
				<td>Per. Test</td>
				<td>Note Book</td>
				<td>SEA</td>
				<td>Half Yearly</td>
				<td>Total</td>
				<td>Grade</td>
				<td>Per. Test</td>
				<td>Note Book</td>
				<td>SEA</td>
				<td>Half Yearly</td>
				<td>Total</td>
				<td>Grade</td>
			</tr>
			<tr  style="background-color: #75e5db;" class="bold">
				<td>10</td>
				<td>5</td>
				<td>5</td>
				<td>80</td>
				<td>100</td>
				<td> </td>
				<td>10</td>
				<td>5</td>
				<td>5</td>
				<td>80</td>
				<td>100</td>
				<td> </td>
			</tr>
			<tr>
				<td> Language 1 </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<td> Language 2 </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<td> Language 3 </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<td> Mathematics </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<td> Science </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<td> So. Science </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
			<tr>
				<td> Any other Sub </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
			</tr>
		</table>
		<div style="float: left;">
		<table style="margin: 10px 60px;font-size: 14px;width: 100%;" id="border" cellspacing="0px";>
			<tr class="align bold" style="background-color: #f8e503de;"> 
				<td>Co-Scholastic Areas: Terms-1<br>[on a 3 point (A-C) grading scale]</td>
				<td>Grade</td>
			</tr>
			<tr>
				<td>Work Education (or Pre-vocational Education)</td>
				<td> </td>
			</tr>
			<tr>
				<td>Art Education</td>
				<td> </td>
			</tr>
			<tr>
				<td>Health & Physical Education</td>
				<td> </td>
			</tr>
		</table>
		<table style="margin: 10px 60px;font-size: 14px;width: 100%;" id="border" cellspacing="0px"; class="bold">
			<tr class="align_right">
				<td colspan="2">Grade</td>
			</tr>
			<tr class="align">
				<td>Discipline: Term-1<br>[on a 3 point (A-C) grading scale]</td>
				<td style="width: 50px;"> </td>
			</tr>
		</table>
		</div>
		<div style="float: left;">
		<table style="margin: 10px 73px;font-size: 14px;width: 100%;" id="border" cellspacing="0px";>
			<tr class="align bold" style="background-color: #f8e503de;"> 
				<td>Co-Scholastic Areas: Terms-1<br>[on a 3 point (A-C) grading scale]</td>
				<td>Grade</td>
			</tr>
			<tr>
				<td>Work Education (or Pre-vocational Education)</td>
				<td> </td>
			</tr>
			<tr>
				<td>Art Education</td>
				<td> </td>
			</tr>
			<tr>
				<td>Health & Physical Education</td>
				<td> </td>
			</tr>
		</table>
		<table style="margin: 10px 73px;font-size: 14px;width: 100%;" id="border" cellspacing="0px"; class="bold">
			<tr class="align_right">
				<td colspan="2">Grade</td>
			</tr>
			<tr class="align">
				<td>Discipline: Term-2<br>[on a 3 point (A-C) grading scale]</td>
				<td style="width: 50px;"> </td>
			</tr>
		</table>
		</div>
		
		<br><br><br><br><br><br>
		<table style="margin: 20px 60px;height: 70px;">
			<tr>
				<td style="width: 220px;font-size:16px;" class="bold">Class Teacher's Remarks : </td>
				<td style="width: 700px;" class="text content_size">&nbsp; </td>
			</tr>
			<tr>
				<td style="width: 200px;font-size:16px;"  class="bold">Promoted to Class </td>
				<td style="width: 700px;" class="text content_size">&nbsp; </td>
			</tr>
		</table>

		<table style="margin: 0px 60px;width: 88%; ">
			<tr>
				<td style="width: 115px" class="align_left content_size sign">Place :  _________</td>
			</tr>
			<tr>
				<td style="width: 115px" class="align_right content_size sign">Date : 18/12/2017</td>
				<td  rowspan="2" style="width: 712px" class="align content_size sign">Signature of Class Teacher</td>
				<td  rowspan="2" style="width: 145px" class="align content_size sign">Signature of Principal</td>
			</tr>
		</table>
		<hr style="margin: 0px 60px;">
		<h6 class="align" style="margin: 10px 0px;">Instructions</h6>
		<p style="font-size: 15px;" class="align bold">Grading scale for scholastic areas: Grades are awarded on a 8-point grading scale as follow-</p>
		<table style="margin: 0px 60px;font-size: 14px;width: 88%;" class="align" id="border" cellspacing="0px";>
				<tr class="bold">
					<td>Marks Range</td>
					<td>Grades</td>
				</tr>
				<tr>
					<td>91-100</td>
					<td>A1</td>
				</tr>
				<tr>
					<td>81-90</td>
					<td>A2</td>
				</tr>
				<tr>
					<td>71-80</td>
					<td>B1</td>
				</tr>
				<tr>
					<td>61-70</td>
					<td>B2</td>
				</tr>
				<tr>
					<td>51-60</td>
					<td>C1</td>
				</tr>
				<tr>
					<td>41-50</td>
					<td>C2</td>
				</tr>
				<tr>
					<td>33-40</td>
					<td>D</td>
				</tr>
				<tr>
					<td> 32 & Below </td>
					<td> E (Needs Enviromenrt)</td>
				</tr>
		</table>
		<br>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	// window.print();
</script>