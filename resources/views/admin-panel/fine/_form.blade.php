@if(isset($fine['fine_id']) && !empty($fine['fine_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('fine_id',old('fine_id',isset($fine['fine_id']) ? $fine['fine_id'] : ''),['class' => 'gui-input', 'id' => 'fine_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info onetime -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fine_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('fine_name', old('fine_name',isset($fine['fine_name']) ? $fine['fine_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.fine_name'), 'id' => 'fine_name']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fine_for') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('fine_for', $fine['arr_head_type'],isset($fine['fine_for']) ? $fine['fine_for'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'fine_for','onChange'=>'getHeads(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.fees_head') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('head_id', $fine['arr_heads'],isset($fine['head_id']) ? $fine['head_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'head_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>


<!-- Document Information -->
<div class="header">
    <h2><strong>Detail </strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($fine['fineDetails']) ? COUNT($fine['fineDetails']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    <p class="green" id="detail_success"></p>
    @if(isset($fine['fineDetails']) && !empty($fine['fineDetails']))
    @php  $key = 0; @endphp
    @foreach($fine['fineDetails'] as $fineKey => $fineVal)
    @php  
        $fine_detail_id = "fineDetails[".$key."][fine_detail_id]"; 
    @endphp
    <div id="detail_block{{$key}}">
        {!! Form::hidden($fine_detail_id,$fineVal['fine_detail_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.fine_detail_days') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('fineDetails['.$key.'][fine_detail_days]', $fineVal['fine_detail_days'], ['class' => 'form-control','placeholder'=>trans('language.fine_detail_days'), 'id' => 'fine_detail_days'.$key.'', 'required', 'min'=>'1']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.fine_detail_amt') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('fineDetails['.$key.'][fine_detail_amt]',$fineVal['fine_detail_amt'], ['class' => 'form-control','placeholder'=>trans('language.fine_detail_amt'), 'id' => 'fine_detail_amt'.$key.'', 'required','min'=>'1']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3 text-right">
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDetailBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$key}}),remove_record({{$fineVal['fine_detail_id']}})" >Remove</button>
                @endif
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.fine_detail_days') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('fineDetails[0][fine_detail_days]', '', ['class' => 'form-control','placeholder'=>trans('language.fine_detail_days'), 'id' => 'fine_detail_days0', 'required', 'min'=>'1']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.fine_detail_amt') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('fineDetails[0][fine_detail_amt]', '', ['class' => 'form-control ','placeholder'=>trans('language.fine_detail_amt'), 'id' => 'fine_detail_amt0', 'required', 'min'=> '1']) !!}
            </div>
        </div>
        
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDetailBlock()" >Add More</button></div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/fees-collection') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#fine-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                fine_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                fine_for: {
                    required: true
                },
                head_id: {
                    required: true
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        

    });
    function addDetailBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#sectiontable-body").append('<div id="detail_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.fine_detail_days') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"><input type="number" name="fineDetails['+arr_key+'][fine_detail_days]" id="fine_detail_days'+arr_key+'" class="form-control" placeholder="{{trans('language.fine_detail_days')}}" required min="1" /></div></div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.fine_detail_amt') !!}  <span class="red-text">*</span>  :</lable><div class="form-group"> <input type="number" name="fineDetails['+arr_key+'][fine_detail_amt]" id="rc_inst_date'+arr_key+'" class="form-control" placeholder="{{trans('language.fine_detail_amt')}}" required min="1" /></div></div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div></div>');
        
    }
    function remove_block(id){
        $("#detail_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function remove_record(fine_detail_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/fine/delete-detail/')}}/"+fine_detail_id,
                success: function (data) {
                    $("#detail_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }
   
    function getHeads(type)
    {
        if(type !== "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recurring-head/get-heads')}}",
                type: 'GET',
                data: {
                    'type': type
                },
                success: function (data) {
                    $("select[name='head_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='head_id'").html('<option value="">Select Head</option>');
            $(".mycustloading").hide();
        }
    }
</script>

