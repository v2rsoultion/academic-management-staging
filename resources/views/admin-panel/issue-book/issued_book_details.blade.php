@extends('admin-panel.layout.header')
@section('content')
{!! Html::script('public/admin/assets/js/jquery.modal.min.js') !!}
{!! Html::style('public/admin/assets/css/jquery.modal.min.css') !!}

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.return_renew_book') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.book_issued') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.return_renew_book') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('member_type', $listData,isset($listData) ? $listData : '1', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('member_name', old('member_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.book_issued_name'), 'id' => 'member_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="member-staff-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('language.book_issued_book_name')}}</th>
                                                <th>{{trans('language.book_issued_member_name')}}</th>
                                                <th>{{trans('language.book_issued_isbn_no')}}</th>
                                                <th>{{trans('language.book_issued_unique_no')}}</th>
                                                <th>{{trans('language.book_issued_start')}}</th>
                                                <th>{{trans('language.book_issued_end')}}</th>
                                                <th>{{trans('language.book_issued_duration')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<!-- Model code start here -->

<p><a href="#renew-book" id="openLink1" rel="modal:open"></a></p>
<p><a href="#update-book" id="openLink2" rel="modal:open"></a></p>

<!-- Modal HTML embedded directly into document -->
<div id="renew-book" class="modal">    
  <div class="container">
  <p><strong>Renew</strong></p>
    {!! Form::open(['files'=>TRUE,'id' => 'book-renew-date-form' , 'class'=>'form-horizontal','url' => $renew_url]) !!}
    <div id='displaymsg'></div>
    <div class="row">
        <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.book_issued_from') !!} :</lable>
        <div class="form-group">
            {!! Form::date('issue_from','',['class' => 'form-control','placeholder'=>trans('language.book_issued_from'), 'id' => 'issue_from_renew']) !!}
        </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.book_issued_to') !!} :</lable>
            <div class="form-group">
                {!! Form::date('issue_to','',['class' => 'form-control','placeholder'=>trans('language.book_issued_to'), 'id' => 'issue_to_renew']) !!}
            </div>
        </div>
        {!! Form::hidden('book_id','',['id' => 'book_id']) !!}
        <div class="col-sm-2 book-issue-button">
            {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        </div>

      </div>
      {!! Form::close() !!}      
    </div>
    <a href="#" id="closemypopup" rel="modal:close"></a>
</div>

<!-- Modal HTML embedded directly into document -->
<div id="update-book" class="modal">    
  <div class="container">
  <p><strong>Update Date</strong></p>
    {!! Form::open(['files'=>TRUE,'id' => 'book-update-date-form' , 'class'=>'form-horizontal','url' => $update_url]) !!}
    <div id='displaymsg'></div>
    <div class="row">
        <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.book_issued_from') !!} :</lable>
        <div class="form-group">
            {!! Form::date('issue_from','',['class' => 'form-control','placeholder'=>trans('language.book_issued_from'), 'id' => 'issue_from_update']) !!}
        </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.book_issued_to') !!} :</lable>
            <div class="form-group">
                {!! Form::date('issue_to','',['class' => 'form-control ','placeholder'=>trans('language.book_issued_to'), 'id' => 'issue_to_update']) !!}
            </div>
        </div>
        {!! Form::hidden('book_id_update','',['id' => 'book_id_update']) !!}
        <div class="col-sm-2 book-issue-button">
            {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        </div>

      </div>
      {!! Form::close() !!}      
    </div>
    <a href="#" id="closemypopup" rel="modal:close"></a>
</div>

<script>
     $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#member-staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: "{{url('admin-panel/return-book-data')}}",
                data: function (d) {
                    d.member_name   = $('input[name=member_name]').val();
                    d.member_type   = $('select[name="member_type"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'issue_book_name', name: 'issue_book_name'},
                {data: 'member_name', name: 'member_name'},
                {data: 'issue_book_isbn_no', name: 'issue_book_isbn_no'},
                {data: 'issue_book_unique_no', name: 'issue_book_unique_no'},
                {data: 'issue_book_start_date', name: 'issue_book_start_date'},
                {data: 'issue_book_end_date', name: 'issue_book_end_date'},
                {data: 'issue_book_duration', name: 'issue_book_duration'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0,
                    "orderable": false,
                    "width": "12%"
                },
                {
                    "targets": 8, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='member_type'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })

        $(document).on("click","#renew_pop_id", function (e) {
            book_id     = $(this).attr("book-id");
            $("#book_id").val(book_id);
            $('#openLink1').trigger('click');
        })       

        $(document).on("click","#update_pop_id", function (e) {
            book_id     = $(this).attr("book-id");
            $("#book_id_update").val(book_id);
            $('#openLink2').trigger('click');
        })

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End Date must be greater than start date.');

        jQuery.validator.addMethod("greaterThanUpdate", 
        function(value, element, params) {

            if (!/Invalid|NaN/.test(value)) {
                return value >= $('#issue_from_update').val();
            }

            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End date must be greater than start date.');

        //  Book Renew Form
        $("#book-renew-date-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                issue_from: {
                    required: true,  
                },
                issue_to: {
                    required: true,  
                    date: true,
                    greaterThan: "#issue_from_renew"
                   
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        //  Book update Form
        $("#book-update-date-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                issue_from: {
                    required: true,  
                    date: true,
                },
                issue_to: {
                    required: true,  
                    date: true,
                    greaterThanUpdate: $('#issue_from_update').val(),
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        // Renew
       
        // $('#issue_to_renew').bootstrapMaterialDatePicker({ weekStart : 0,time: false,minDate : new Date() }).on('change', function(e, date){ $(this).valid(); });
        // $('#issue_from_renew').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false,minDate : new Date()}).on('change', function(e, date) {  
        //     $('#issue_to_renew').val(''); 
        //     $(this).valid();  
        //     $('#issue_to_renew').bootstrapMaterialDatePicker('setMinDate', date);  
        // });
        // // Update
        
        // $('#issue_to_update').bootstrapMaterialDatePicker({ weekStart : 0,time: false,minDate : new Date() }).on('change', function(e, date){ $(this).valid(); });
        // $('#issue_from_update').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false,minDate : new Date()}).on('change', function(e, date) {  
        //     $('#issue_to_update').val(''); 
        //     $(this).valid();  
        //     $('#issue_to_update').bootstrapMaterialDatePicker('setMinDate', date);  
        // });
    });
  
</script>
@endsection