@if(isset($scheme['grade_scheme_id']) && !empty($scheme['grade_scheme_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('grade_scheme_id',old('grade_scheme_id',isset($scheme['grade_scheme_id']) ? $scheme['grade_scheme_id'] : ''),['class' => 'gui-input', 'id' => 'grade_scheme_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info onetime -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.scheme_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('scheme_name', old('scheme_name',isset($scheme['scheme_name']) ? $scheme['scheme_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.scheme_name'), 'id' => 'scheme_name']) !!}
        </div>
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.grade_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('grade_type', $scheme['arr_grade_type'],isset($scheme['grade_type']) ? $scheme['grade_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'grade_type', 'required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

</div>


<!-- Document Information -->
<div class="header">
    <h2><strong>Grade</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($scheme['gradeData']) ? COUNT($scheme['gradeData']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="gardetable-body">
    <p class="green" id="grade_success"></p>
    @if(isset($scheme['gradeData']) && !empty($scheme['gradeData']))
    @php  $key = 0; @endphp
    @foreach($scheme['gradeData'] as $gradeKey => $gradeData)
    @php  
        $grade_id = "gradeData[".$key."][grade_id]"; 
    @endphp
    <div id="inst_block{{$key}}">
        {!! Form::hidden($grade_id,$gradeData['grade_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.grade_min') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('gradeData['.$key.'][grade_min]', $gradeData['grade_min'], ['class' => 'form-control','placeholder'=>trans('language.grade_min'), 'id' => 'grade_min'.$key.'', 'required', 'min' => '1']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.grade_max') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('gradeData['.$key.'][grade_max]',$gradeData['grade_max'], ['class' => 'form-control date','placeholder'=>trans('language.grade_max'), 'id' => 'grade_max'.$key.'', 'required', 'min' => '1']) !!}
                </div>
            </div>

            
            <div class="col-lg-3 col-md-3 text-right"><br />
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addGradeBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$key}}),remove_record({{$gradeData['grade_id']}})" >Remove</button>
                @endif
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.grade_name') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::text('gradeData['.$key.'][grade_name]',$gradeData['grade_name'], ['class' => 'form-control','placeholder'=>trans('language.grade_name'), 'id' => 'grade_name'.$key.'', 'required']) !!}
                </div>
            </div>

            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.grade_point') !!} :</lable>
                <div class="form-group">
                    {!! Form::number('gradeData['.$key.'][grade_point]',$gradeData['grade_point'], ['class' => 'form-control','placeholder'=>trans('language.grade_point'), 'id' => 'grade_point'.$key.'']) !!}
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <lable class="from_one1">{!! trans('language.grade_remark') !!} :</lable>
                <div class="form-group">
                    {!! Form::textarea('gradeData['.$key.'][grade_remark]',$gradeData['grade_remark'], ['class' => 'form-control','placeholder'=>trans('language.grade_remark'), 'id' => 'grade_remark'.$key.'', 'rows' => '2']) !!}
                </div>
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.grade_min') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('gradeData[0][grade_min]', '', ['class' => 'form-control','placeholder'=>trans('language.grade_min'), 'id' => 'grade_min0', 'required', 'min'=> '1','onKeyUp'=>'checkGrades(0)']) !!}
            </div>
            <p id="error0" class="red"></p>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.grade_max') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('gradeData[0][grade_max]', '', ['class' => 'form-control','placeholder'=>trans('language.grade_max'), 'id' => 'grade_max0', 'required','onKeyUp'=>'checkGrades(0)']) !!}
            </div>
        </div>
        <div class="col-lg-3 col-md-3 text-right"><br /><button class="btn btn-primary custom_btn" type="button" onclick="addGradeBlock()" >Add More</button></div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.grade_name') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('gradeData[0][grade_name]', '', ['class' => 'form-control','placeholder'=>trans('language.grade_name'), 'id' => 'grade_name0', 'required']) !!}
            </div>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.grade_point') !!} :</lable>
            <div class="form-group">
                {!! Form::number('gradeData[0][grade_point]', '', ['class' => 'form-control','placeholder'=>trans('language.grade_point'), 'id' => 'grade_point0']) !!}
            </div>
        </div>

        <div class="col-lg-6 col-md-6">
            <lable class="from_one1">{!! trans('language.grade_remark') !!} :</lable>
            <div class="form-group">
                {!! Form::textarea('gradeData[0][grade_remark]', '', ['class' => 'form-control','placeholder'=>trans('language.grade_remark'), 'id' => 'grade_remark0', 'rows' => '2']) !!}
            </div>
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) >= parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) <= parseFloat($otherElement.val(), 10);
        },"The value  must be less");

        $("#recurring-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                scheme_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                grade_type: {
                    required: true,
                }
            },

            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        

    });

    $(document).on('click','.inst-records',function(e){
        var installment_id = $(this).attr('inst-record');
    });

    function checkGrades(id){

        var minval = $('#grade_min'+id).val();
        var maxval = $('#grade_max'+id).val();
        if(minval == ''){
            minval = 0;
        }
        if(maxval == ''){
            maxval = 0;
        }
        console.log('minval',minval);
        console.log('maxval',maxval);
        if(parseFloat(maxval) < parseFloat(minval)){
            $('#error'+id).html('Minimum should be less or equal to maximum');
            return false;
        } else {
            $('#error'+id).html('');
            return true;
        }
    }
    function addGradeBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#gardetable-body").append('<div id="grade_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.grade_min') !!} <span class="red-text">*</span> :</lable><div class="form-group"><input type="number" name="gradeData['+arr_key+'][grade_min]" id="grade_min'+arr_key+'" class="form-control" placeholder="{{trans('language.grade_min')}}" min="1" required onKeyUp=checkGrades('+arr_key+') /> </div> <p id="error'+arr_key+'" class="red"></p></div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.grade_max') !!} <span class="red-text">*</span> :</lable><div class="form-group"> <input type="number" name="gradeData['+arr_key+'][grade_max]" id="grade_max'+arr_key+'" class="form-control" placeholder="{{trans('language.grade_max')}}" min="1" required  onKeyUp=checkGrades('+arr_key+') /> </div> </div><div class="col-lg-3 col-md-3 text-right"><br /><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div> <div class="row clearfix"><div class="col-lg-3 col-md-3"> <lable class="from_one1">{!! trans('language.grade_name') !!} <span class="red-text">*</span> :</lable><div class="form-group"> <input type="text" name="gradeData['+arr_key+'][grade_name]" id="grade_name'+arr_key+'" class="form-control" placeholder="{{trans('language.grade_name')}}" required /></div> </div><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans('language.grade_point') !!} :</lable><div class="form-group"><input type="number" name="gradeData['+arr_key+'][grade_point]" id="grade_point'+arr_key+'" class="form-control" placeholder="{{trans('language.grade_point')}}" /> </div> </div><div class="col-lg-6 col-md-6"><lable class="from_one1">{!! trans('language.grade_remark') !!} :</lable><div class="form-group"> <textarea class="form-control" placeholder="{{trans('language.grade_remark')}}" id="grade_remark'+arr_key+'" rows="2" name="gradeData['+arr_key+'][grade_remark]" cols="50"></textarea></div></div> </div></div>');
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    }
    function remove_block(id){
        $("#grade_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function remove_record(rc_inst_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recurring-head/delete-inst/')}}/"+rc_inst_id,
                success: function (data) {
                    $("#document_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }
   

</script>

