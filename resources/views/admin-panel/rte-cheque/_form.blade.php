@if(isset($cheque['rte_cheque_id']) && !empty($cheque['rte_cheque_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('rte_cheque_id',old('rte_cheque_id',isset($cheque['rte_cheque_id']) ? $cheque['rte_cheque_id'] : ''),['class' => 'gui-input', 'id' => 'rte_cheque_id', 'readonly' => 'true']) !!}

@if ($errors->any())
    <div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.bank_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('bank_id', $cheque['arr_bank'],isset($cheque['bank_id']) ? $cheque['bank_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'bank_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.rte_cheque_no') !!} :</lable>
        <div class="form-group">
            {!! Form::text('rte_cheque_no', old('rte_cheque_no',isset($cheque['rte_cheque_no']) ? $cheque['rte_cheque_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.rte_cheque_no'), 'id' => 'rte_cheque_no', 'maxlength'=>'5']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.rte_cheque_date') !!} :</lable>
        <div class="form-group">
            {!! Form::text('rte_cheque_date', old('rte_cheque_date',isset($cheque['rte_cheque_date']) ? $cheque['rte_cheque_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.rte_cheque_date'), 'id' => 'rte_cheque_date']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.rte_cheque_amt') !!} :</lable>
        <div class="form-group">
            {!! Form::text('rte_cheque_amt', old('rte_cheque_amt',isset($cheque['rte_cheque_amt']) ? $cheque['rte_cheque_amt']: ''), ['class' => 'form-control','placeholder'=>trans('language.rte_cheque_amt'), 'id' => 'rte_cheque_amt', 'min'=>'0']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/fees-collection') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#cheque-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                bank_id: {
                    required: true,
                },
                rte_cheque_no: {
                    required: true,
                    digits:true
                },
                rte_cheque_date: {
                    required: true
                },
                rte_cheque_amt: {
                    required: true,
                    digits:true,
                    min: 1
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });  
        $('#rte_cheque_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}) .on('change', function(ev) {
            $(this).valid();  
        }); 
    });
</script>