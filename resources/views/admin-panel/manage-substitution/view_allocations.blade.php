@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <a href="{!! url('admin-panel/substitute-management/allocate/'.$id) !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/substitute-management') !!}">{!! trans('language.menu_substitute_management') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/substitute-management/manage-substitution') !!}">{!! trans('language.manage_substitution') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2><strong>Basic</strong> Information <small><br />
                                Teacher Name: {!! $leave_application['getStaff']->staff_name !!} <br />
                                Leave Dates: {!! date("d F Y", strtotime($leave_application->staff_leave_from_date)).' - '.date("d F Y", strtotime($leave_application->staff_leave_to_date)) !!} <br />
                                </small> </h2>
                            </div>
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                
                                {!! Form::hidden('ref_leave_id',old('ref_leave_id',isset($ref_leave_id) ? $ref_leave_id : ''),['class' => 'gui-input', 'id' => 'ref_leave_id', 'readonly' => 'true']) !!}

                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="allocation-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.class_lecture')}}</th>
                                            <th>{{trans('language.date_range')}}</th>
                                            <th>{{trans('language.staff_name')}}</th>
                                            <th>{{trans('language.pay_extra')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#allocation-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: "{{url('admin-panel/substitute-management/allocation-data')}}",
                data: function (d) {
                    d.ref_leave_id = $('input[name=ref_leave_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'class_lecture', name: 'class_lecture'},
                {data: 'date_range', name: 'date_range'},
                {data: 'staff_name', name: 'staff_name'},
                {data: 'pay_extra', name: 'pay_extra'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




