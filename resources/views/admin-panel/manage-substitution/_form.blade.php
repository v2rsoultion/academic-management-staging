{!! Form::hidden('substitute_id',old('substitute_id',isset($substitute['substitute_id']) ? $substitute['substitute_id'] : ''),['class' => 'gui-input', 'id' => 'substitute_id', 'readonly' => 'true']) !!}

{!! Form::hidden('leave_staff_id',old('leave_staff_id',isset($listData['staff_id']) ? $listData['staff_id'] : ''),['class' => 'gui-input', 'id' => 'leave_staff_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->

<div class="row clearfix"> 
    
    @if(!empty($leave_application['getStaff']['getClassAllocation']))
    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.as_class_teacher') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('as_class_teacher', $listData['staff_apoint'],isset($substitute['as_class_teacher']) ? $substitute['as_class_teacher'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'as_class_teacher'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif
    
    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.date_from') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('from_date', $listData['dateRange'],isset($substitute['from_date']) ? $substitute['from_date'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'from_date','onChange' => 'getDate(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.date_to') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('to_date', $listData['dateRange'],isset($substitute['to_date']) ? $substitute['to_date'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'to_date','onChange' => 'getDate(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $listData['class_arr'],isset($substitute['class_id']) ? $substitute['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.section_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('section_id', $listData['arr_section'],isset($substitute['section_id']) ? $substitute['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'getLecture(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.lecture_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('lecture_no', $listData['arr_lecture'],isset($substitute['lecture_no1']) ? $substitute['lecture_no1'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'lecture_no','onChange' => 'getStaff(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.staff_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('staff_id', $listData['arr_staff'],isset($substitute['staff_id']) ? $substitute['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.pay_extra') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('pay_extra', $listData['pay_extra'],isset($substitute['pay_extra']) ? $substitute['pay_extra'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'pay_extra'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>


<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/substitute-management/manage-substitution') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) >= Number($(params).val())); 
        },'End Date must be greater than start date.');


        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $("#substitution-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules   ------------------------------------------ */
            rules: {
                from_date: {
                    required: true
                },
                to_date: {
                    required: true,
                    greaterThan: "#from_date"
                },
                class_id: {
                    required: true
                },
                section_id: {
                    required: true
                },
                lecture_no: {
                    required: true
                },
                staff_id: {
                    required: true
                },
                pay_extra: {
                    required: true
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        
    });

    function getSection(class_id)
    {
        var staff_id = $("#leave_staff_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/substitute-management/get-sections')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'staff_id': staff_id,
                    'to_date' : to_date,
                    'from_date': from_date
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }

    function getLecture(section_id)
    {
        var staff_id = $("#leave_staff_id").val();
        var class_id = $("#class_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/substitute-management/get-lectures')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'section_id': section_id,
                    'staff_id': staff_id,
                    'to_date' : to_date,
                    'from_date': from_date
                },
                success: function (data) {
                    $("select[name='lecture_no'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='lecture_no'").html('<option value="">Select Lecture</option>');
            $(".mycustloading").hide();
        }
    }

    function getStaff(lecture_no)
    {
        var staff_id = $("#leave_staff_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(lecture_no != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/substitute-management/get-staff')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'section_id': section_id,
                    'staff_id': staff_id,
                    'lecture_no': lecture_no
                },
                success: function (data) {
                    $("select[name='staff_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='lecture_no'").html('<option value="">Select Lecture</option>');
            $(".mycustloading").hide();
        }
    }

    function getDate(date)
    {
        if(date == "") {
            $("select[name='lecture_no'").html('<option value="">Select Lecture</option>');
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $("select[name='staff_id'").html('<option value="">Select Staff</option>');
        }
    }

</script>