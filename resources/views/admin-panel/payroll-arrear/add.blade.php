@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
   <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_arrear') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-arrear') !!}">{!! trans('language.manage_arrear') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_arrear' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-arrear._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12" style="margin-left:-13px;">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_arrear_name','',['class' => 'form-control','placeholder' => trans('language.arrear_name'), 'id' => 's_arrear_name']) !!}
            </div>
          </div>
          
          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="arrear-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.arrear_s_no') !!}</th>
              <th>{!! trans('language.arrear_name') !!}</th>
              <th>{!! trans('language.date_range') !!}</th>
              <th>{!! trans('language.arrear_amount') !!}</th>
              <th>{!! trans('language.arrear_total_amt') !!}</th>
              <th>{!! trans('language.arrear_round_off') !!}</th>
              <th>{!! trans('language.no_staff_mapped') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#arrear-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-arrear-view')}}",
              data: function (f) {
                  f.s_arrear_name = $('#s_arrear_name').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'arrear_name', name: 'arrear_name'},
            {data: 'date_range', name: 'date_range'},
            {data: 'arrear_amount', name: 'arrear_amount'},
            {data: 'arrear_total_amt', name: 'arrear_total_amt'},
            {data: 'round_off', name: 'round_off'},
            {data: 'staff_mapped', name: 'staff_mapped'},
            {data: 'action', name: 'action'}
        ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "3%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "12%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "11%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "16%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
    
    $(document).on('click', '.arrear-staff', function(e) {
        var arrear_id = $(this).attr('arrear_id'); 
        var table = $('#arrear-staff-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-arrear-employees-data')}}",
              type: 'GET',
              data: {
                'arrear_id': arrear_id
              }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'employee_profile', name: 'employee_profile'},
            {data: 'previous_arrear', name: 'previous_arrear'}
        ],
            columnDefs: [
                {
                  "targets": 0, // your case first column
                  "orderable": false
                },
                {
                  targets: [ 0, 1],
                  className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#viewStaffModel').on('hidden.bs.modal', function () {
        location.reload();
        });
    }); 
</script>
<div class="modal fade" id="viewStaffModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Staffs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table m-b-0 c_list" id="arrear-staff-table" width="100%">
            <thead>
              <tr>
              <th>S.No</th>
              <th>{!! trans('language.emp_name') !!}</th>
              <th>Previous Arrear</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>   
@endsection