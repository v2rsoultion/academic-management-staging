<!DOCTYPE html>
<html>
<head>
	<title>Print Invoice</title>
  <link rel="stylesheet" href="{{url("/public/assets/plugins/bootstrap/css/bootstrap.min.css")}}">
	<style type="text/css">
  body{
    margin: 0px;
    padding:0px;
  }
		table
		{
			width: 720px;
		}
		.align {
			text-align: right;
		}
		.footer {
			width: 720px;
			padding: 8px 0px ;
			background-color: #1f2f60;
			text-align: center;
			color: #fff;
			font-size: 10px;
		}
		.clear {
			clear: both;
		} 
    th{
      padding-bottom: 7px;
    }
    td{
      padding-bottom: 7px;
    }
    .border_bottom {
      border-bottom:1px solid #000;
    }
    .border_top {
      border-top:1px solid #000;
    }
    .border_right {
      border-right: 1px solid #000;
    }
    .border_left {
      border-left:1px solid #000;
    }
    .gap {
      margin-right: 10px !important;
    }
	</style>
</head>
<body>
<table>
  <tr>
    <td><img src="{{url($logo)}}" alt="" width="80px"></td>
    <td style="width: 640px;text-align: right;"><h2 style="color: #1f2f60;margin: 5px 0px;width: 640px;text-align: right; background-color: #fff;">INVOICE</h2><p class="align_right" style="font-size: 13px;margin: 0px;"> {{$school['school_name']}}<br> E-mail : {{$school['school_email']}}<br> Ph. {{$school['school_telephone']}}</p></td>
  </tr>
</table>
<div style="width: 720px; border-bottom: solid #e6e6e6; margin-top:10px!important;height: 1px; margin-bottom: 5px;"></div>
<div class="clear"></div>
<table>
  <tr>
    <td>System Invoice No</td>
    <td> : &nbsp; {{$invoice_details['sys_invoice_no']}} </td>
    <td style="text-align: right;">Invoice No : &nbsp;</td>
    <td style="text-align: right;">  {{$invoice_details['invoice_no']}} </td>
  </tr>
  <tr>
  <td>Payment mode  </td>
    <td> : &nbsp; {{$invoice_details['payment_mode']}} </td>
    <td style="text-align: right;">Date  : &nbsp;</td>
    <td style="text-align: right;">  {{$invoice_details['date']}}</td>
  </tr>
</table>
<!-- <div style="width: 720px; border-bottom: solid #e6e6e6; margin-top:10px!important;height: 1px; margin-bottom: 10px;"></div> -->
<table style="width: 720px; margin: 10px 0px;">
  <thead>
    <tr>
     <th style="text-align: center;" class="border_top border_right border_left border_bottom">S No</th>
     <th style="text-align: center;" class="border_top border_right border_bottom">Item Name</th>
     <th style="text-align: center;" class="border_top border_right border_bottom">Amount</th>
   </tr>
  </thead>
  <tbody>
  @foreach($purchase_items as $key => $record)
  <tr>
  <td style="text-align: center;" class="border_bottom border_right border_left">{{$key+1}}</td>
  <td style="text-align: center;" class="border_bottom border_right">{{$record['items']['item_name']}}</td>
  <td style="text-align: center;" class="border_bottom border_right">{{$record['amount']}}</td>
  @endforeach
  </tbody>
  <tr>
    <td> </td>
    <td style="text-align: right;" class="gap"> Total</td>
    <td style="text-align: center;" class="border_left border_right">{{$purchase['total_amount']}}</td>
  </tr>
  <tr>
    <td> </td>
    <td style="text-align: right;" class="gap"> Tax({{$purchase['tax']}}%)</td>
    @php
      $tax_amount = ($purchase['total_amount']*$purchase['tax'])/100; 
      $total_tax_amount = ($purchase['total_amount']+$tax_amount);
      $discount_amount = ($total_tax_amount*$purchase['discount'])/100; 
    @endphp
    <td style="text-align: center;" class="border_left border_right"> &nbsp; {{$tax_amount}}</td>
  </tr>
  <tr>
    <td> </td>
    <td style="text-align: right;" class="gap">Discount({{$purchase['discount']}}%)</td>
    <td style="text-align: center;" class="border_left border_right"> &nbsp;&nbsp; {{$discount_amount}}</td>
  </tr>
  <tr>
    <td> </td>
    <td style="text-align: right;" class="gap">Gross Amount</td>
    <td style="text-align: center;" class="border_left border_right"> &nbsp;&nbsp;&nbsp; {{$purchase['gross_amount']}}</td>
  </tr>
  <tr>
    <td> </td>
    <td style="text-align: right;" class="gap">Net Total</td>
    <td style="text-align: center;" class="border_left border_right border_bottom border_top">{{$purchase['net_amount']}} /-</td>
  </tr>
</table>
<footer><div class="footer">Academic Management {{ date('Y') }} © Copyright All Rights Reserved</div></footer>
</body>

</html>