@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_purchase') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/inventory/purchase/add-purchase') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_purchase') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                              {!! Form::text('s_system_invoice_no','',
                                              ['class' => 'form-control', 'placeholder'=>trans('language.system_invoice_no'), 'id' => 's_system_invoice_no']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                              {!! Form::text('s_ref_invoice_no','',
                                              ['class' => 'form-control', 'placeholder'=>trans('language.ref_invoice_no'), 'id' => 's_ref_invoice_no']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                              {!! Form::date('s_invoice_date','',
                                              ['class' => 'form-control', 'placeholder'=>trans('language.invoice_date'), 'id' => 's_invoice_date']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group m-bottom-0">
                                            <label class=" field select" style="width: 100%">
                                            {!!Form::select('s_vendor_id',$purchase['vendor'],'',['class' => 'form-control show-tick select_form1 select2','id'=>'s_vendor_id'])!!}
                                            <i class="arrow double"></i>
                                            </label>
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-2">
                                            <div class="form-group m-bottom-0">
                                            <label class=" field select" style="width: 100%">
                                            {!!Form::select('s_category_id',$purchase['category'],'',['class' => 'form-control show-tick select_form1 select2','id'=>'s_category_id'])!!}
                                            <i class="arrow double"></i>
                                            </label>
                                            </div>
                                        </div> -->
                                        <div class="col-lg-1 padding-0">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 padding-0">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="purchase-table" style="width:100%">{{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>{{trans('language.invoice_s_no')}}</th>
                        <th>{{trans('language.system_invoice_no')}}</th>
                        <th>{{trans('language.ref_invoice_no')}}</th>
                        <th>{{trans('language.invoice_date')}}</th>
                        <th>{{trans('language.calculation_amount')}}</th>
                        <th>{{trans('language.view_details')}}</th>
                        <th style="width: 150px;" class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                  </table>
            
                </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#purchase-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            ajax: {
                url: '{{url('admin-panel/inventory/purchase/view-purchase-data')}}',
                data: function (d) {
                    d.s_system_invoice_no = $('input[name=s_system_invoice_no]').val();
                    d.s_ref_invoice_no    = $('input[name="s_ref_invoice_no"]').val();
                    d.s_invoice_date      = $('input[name="s_invoice_date"]').val();
                    d.s_vendor_id         = $('select[name="s_vendor_id"]').val();
                    d.s_category_id       = $('select[name="s_category_id"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'system_invoice_no', name: 'system_invoice_no'},
                {data: 'ref_invoice_no', name: 'ref_invoice_no'},
                {data: 'date', name: 'date'},
                {data: 'calculation_amount', name: 'calculation_amount'},
                {data: 'view_details', name: 'view_details'},
                {data: 'action', name: 'action'}
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "3%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "17%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "20%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    $(document).on('click', '.purchase', function(e) {
        var purchase_id = $(this).attr('purchase_id'); 
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/inventory/purchase/view-purchase-model-data')}}",
                type: 'GET',
                data: {
                    'purchase_id': purchase_id
                },
                success: function (data) {

                    //alert(obj.payment_mode);
                    // console.log(obj.category.category_name)
                    $("#show_response").html(data);
                    $(".mycustloading").hide();   
                }
            });
    });

</script>

<div class="modal fade" id="MoreDetailsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">More Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
           <div id="show_response"></div>
      
      </div>
    </div>
  </div>
</div>
@endsection




