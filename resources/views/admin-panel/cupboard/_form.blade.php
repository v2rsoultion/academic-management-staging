@if(isset($cupboard['book_cupboard_id']) && !empty($cupboard['book_cupboard_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('book_cupboard_id',old('book_cupboard_id',isset($cupboard['book_cupboard_id']) ? $cupboard['book_cupboard_id'] : ''),['class' => 'gui-input', 'id' => 'book_cupboard_id', 'readonly' => 'true']) !!}
@if ($errors->any())
    <div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.cupboard_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('book_cupboard_name', old('book_cupboard_name',isset($cupboard['book_cupboard_name']) ? $cupboard['book_cupboard_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_name'), 'id' => 'book_cupboard_name']) !!}
        </div>
    </div>
</div>
<!-- Document Information -->
<div class="header">
    <h2><strong>CupBoard Shelf</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($scheme['gradeData']) ? COUNT($scheme['gradeData']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="gardetable-body">
    <p class="green" id="grade_success"></p>
    @if(isset($scheme['gradeData']) && !empty($scheme['gradeData']))
    @php  $key = 0; @endphp
    @foreach($scheme['gradeData'] as $gradeKey => $gradeData)
    @php  
        $grade_id = "gradeData[".$key."][grade_id]"; 
    @endphp
    <div id="inst_block{{$key}}">
        {!! Form::hidden($grade_id,$gradeData['grade_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.cupboard_shelf_name') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::text('cupboardshelf_name', old('cupboardshelf_name',isset($cupboard_shelf['book_cupboardshelf_name']) ? $cupboard_shelf['book_cupboardshelf_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_name'), 'id' => 'book_cupboardshelf_name']) !!}
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <lable class="from_one1">{!! trans('language.cupboard_shelf_capacity') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    {!! Form::number('book_cupboard_capacity', old('book_cupboard_capacity',isset($cupboard_shelf['book_cupboard_capacity']) ? $cupboard_shelf['book_cupboard_capacity']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_capacity'), 'id' => 'book_cupboard_capacity']) !!}
                </div>
            </div>
            
            <div class="col-lg-3 col-md-3 text-right"><br />
                @if($key == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addGradeBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$key}}),remove_record({{$gradeData['grade_id']}})" >Remove</button>
                @endif
            </div>
            <div class="col-lg-12 col-md-12">
                <lable class="from_one1">{!! trans('language.cupboard_shelf_detail') !!} :</lable>
                <div class="form-group">
                    
                    {!! Form::textarea('book_cupboard_shelf_detail',old('book_cupboard_shelf_detail',isset($cupboard_shelf['book_cupboard_shelf_detail']) ? $cupboard_shelf['book_cupboard_shelf_detail']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.cupboard_shelf_detail'),'rows' => 3, 'cols' => 50)) !!}
                </div>
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.cupboard_shelf_name') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::text('cupboardshelf_name', old('cupboardshelf_name',isset($cupboard_shelf['book_cupboardshelf_name']) ? $cupboard_shelf['book_cupboardshelf_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_name'), 'id' => 'book_cupboardshelf_name']) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <lable class="from_one1">{!! trans('language.cupboard_shelf_capacity') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::number('book_cupboard_capacity', old('book_cupboard_capacity',isset($cupboard_shelf['book_cupboard_capacity']) ? $cupboard_shelf['book_cupboard_capacity']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_capacity'), 'id' => 'book_cupboard_capacity']) !!}
            </div>
        </div>
        <div class="col-lg-3 col-md-3 text-right"><br /><button class="btn btn-primary custom_btn" type="button" onclick="addGradeBlock()" >Add More</button></div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <lable class="from_one1">{!! trans('language.cupboard_shelf_detail') !!} :</lable>
            <div class="form-group">
                
                {!! Form::textarea('book_cupboard_shelf_detail',old('book_cupboard_shelf_detail',isset($cupboard_shelf['book_cupboard_shelf_detail']) ? $cupboard_shelf['book_cupboard_shelf_detail']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.cupboard_shelf_detail'),'rows' => 3, 'cols' => 50)) !!}
            </div>
        </div>
    </div>
    @endif
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/library') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#cupboard-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                book_cupboard_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });

    });

    function addGradeBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#gardetable-body").append('<div id="grade_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.cupboard_shelf_name') !!} <span class="red-text">*</span> :</lable><div class="form-group"><input type="text" name="cupboardshelfData['+arr_key+'][cupboardshelf_name]" id="cupboardshelf_name'+arr_key+'" class="form-control" placeholder="{{trans('language.cupboard_shelf_name')}}" required /></div></div><div class="col-lg-4 col-md-4"><lable class="from_one1">{!! trans('language.cupboard_shelf_capacity') !!} <span class="red-text">*</span> :</lable><div class="form-group"><input type="number" name="cupboardshelfData['+arr_key+'][book_cupboard_capacity]" id="book_cupboard_capacity'+arr_key+'" class="form-control" placeholder="{{trans('language.cupboard_shelf_capacity')}}" min="0" required /></div></div> <div class="col-lg-3 col-md-3 text-right"><br /><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div> <div class="row clearfix"><div class="col-lg-12 col-md-12"><lable class="from_one1">{!! trans('language.cupboard_shelf_detail') !!} :</lable><div class="form-group"><textarea class="form-control no-resize" placeholder="{{trans('language.cupboard_shelf_detail')}}" id="book_cupboard_shelf_detail'+arr_key+'" rows="3" name="cupboardshelfData['+arr_key+'][book_cupboard_shelf_detail]" cols="50"></textarea></div></div> </div> </div>');
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    }
    function remove_block(id){
        $("#grade_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }
    function remove_record(rc_inst_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/recurring-head/delete-inst/')}}/"+rc_inst_id,
                success: function (data) {
                    $("#document_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }
</script>