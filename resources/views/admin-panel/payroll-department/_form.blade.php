@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-4">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.dept_name') !!}</lable>
      {!! Form::text('dept_name',old('dept_name',isset($department['dept_name']) ? $department['dept_name']: ''),['class' => 'form-control','placeholder' => trans('language.dept_name'), 'id' => 'dept_name']) !!}
    </div>
  </div>
  <div class="col-lg-8">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.dept_desc') !!}</lable>
      {!! Form::textarea('dept_desc',old('dept_desc',isset($department['dept_description']) ? $department['dept_description']: ''),['class' => 'form-control','placeholder'=>trans('language.dept_desc'), 'id' => 'dept_desc', 'rows' => '2']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/payroll/manage-department') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_department").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                dept_name: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    },
                    lettersonly: true
                },
                dept_desc: {
                    required: true,
                    normalizer: function(value) {
                      return $.trim(value);
                    }
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>