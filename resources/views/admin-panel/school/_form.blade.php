@if(isset($school['school_id']) && !empty($school['school_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('school_id',old('school_id',isset($school['school_id']) ? $school['school_id'] : ''),['class' => 'gui-input', 'id' => 'school_id', 'readonly' => 'true']) !!}


@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_name', old('school_name',isset($school['school_name']) ? $school['school_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_name'), 'id' => 'school_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_registration_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_registration_no', old('school_registration_no',isset($school['school_registration_no']) ? $school['school_registration_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_registration_no'), 'id' => 'school_registration_no']) !!}
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.school_sno_numbers') !!} :</lable>
        <div class="form-group bs-example">
            {!! Form::text('school_sno_numbers', old('school_sno_numbers',isset($school['school_sno_numbers']) ? $school['school_sno_numbers']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_sno_numbers'), 'id' => 'school_sno_numbers','data-role'=>'tagsinput']) !!}
        </div>
    </div>

</div>

<!-- Staff and board section -->
<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_board_of_exams') !!} <span class="red-text">*</span>:</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('school_board_of_exams[]', $school['arr_board'],isset($school['school_board_of_exams']) ? $school['school_board_of_exams'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'school_board_of_exams','multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_medium') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('school_medium[]', $school['arr_medium'],isset($school['school_medium']) ? $school['school_medium'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'school_medium','multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_total_students') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_total_students', old('school_total_students',isset($school['school_total_students']) ? $school['school_total_students']: 0), ['class' => 'form-control','placeholder'=>trans('language.school_total_students'), 'id' => 'school_total_students', 'min'=> 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_total_staff') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_total_staff', old('school_total_staff',isset($school['school_total_staff']) ? $school['school_total_staff']: 0), ['class' => 'form-control','placeholder'=>trans('language.school_total_staff'), 'id' => 'school_total_staff', 'min'=> 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.imprest_ac_status') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('imprest_ac_status', $school['arr_status'],isset($school['imprest_ac_status']) ? $school['imprest_ac_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'imprest_ac_status'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_latitude') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_latitude', old('school_latitude',isset($school['school_latitude']) ? $school['school_latitude']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_latitude'), 'id' => 'school_latitude']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_longitude') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_longitude', old('school_longitude',isset($school['school_longitude']) ? $school['school_longitude']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_longitude'), 'id' => 'school_longitude']) !!}
        </div>
    </div>

</div>

<div class="header">
    <h2><strong>Branch</strong> Information</h2>
</div>

<!-- Class & Fees section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_class_from') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_class_from', old('school_class_from',isset($school['school_class_from']) ? $school['school_class_from']: ''), ['class' => 'form-control class','placeholder'=>trans('language.school_class_from'), 'id' => 'school_class_from']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_class_to') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_class_to', old('school_class_to',isset($school['school_class_to']) ? $school['school_class_to']: ''), ['class' => 'form-control class','placeholder'=>trans('language.school_class_to'), 'id' => 'school_class_to']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_fee_range_from') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('school_fee_range_from', old('school_fee_range_from',isset($school['school_fee_range_from']) ? $school['school_fee_range_from']: ''), ['class' => 'form-control fee','placeholder'=>trans('language.school_fee_range_from'), 'id' => 'school_fee_range_from' , 'min'=> 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_fee_range_to') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('school_fee_range_to', old('school_fee_range_to',isset($school['school_fee_range_to']) ? $school['school_fee_range_to']: ''), ['class' => 'form-control fee','placeholder'=>trans('language.school_fee_range_to'), 'id' => 'school_fee_range_to', 'min'=> 0]) !!}
        </div>
    </div>
</div>

<!-- Facilities section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.school_facilities') !!} :</lable>
        <div class="form-group bs-example">
            {!! Form::text('school_facilities', old('school_facilities',isset($school['school_facilities']) ? $school['school_facilities']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_facilities'), 'id' => 'school_facilities','data-role'=>'tagsinput']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_url') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_url', old('school_url',isset($school['school_url']) ? $school['school_url']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_url'), 'id' => 'school_url']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
    <span class="radioBtnpan">{{trans('language.school_logo')}}</span>
        <label for="file1" class="field file">
            
            <input type="file" class="gui-file" name="school_logo" id="file1" onChange="document.getElementById('school_logo').value = this.value;">
            <input type="hidden" class="gui-input" id="school_logo" placeholder="Upload Photo" readonly>
        </label>
        @if(isset($school['logo']))<img src="{{url($school['logo'])}}"  height="100px" width="100px">@endif
    </div>
</div>

<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<!-- Email section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_email') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_email', old('school_email',isset($school['school_email']) ? $school['school_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_email'), 'id' => 'school_email']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_mobile_number') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('school_mobile_number', old('school_mobile_number',isset($school['school_mobile_number']) ? $school['school_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_mobile_number'), 'id' => 'school_mobile_number', 'maxlength'=> '10','minlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_telephone') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_telephone', old('school_telephone',isset($school['school_telephone']) ? $school['school_telephone']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_telephone'), 'id' => 'school_telephone' , 'maxlength'=> '11','minlength'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_fax_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_fax_number', old('school_fax_number',isset($school['school_fax_number']) ? $school['school_fax_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fax_number'), 'id' => 'school_fax_number', 'maxlength'=> '10','minlength'=> '10']) !!}
        </div>
    </div>
</div>

<!-- Description section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.school_description') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('school_description', old('school_description',isset($school['school_description']) ? $school['school_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_description'), 'id' => 'school_description', 'rows'=> 3]) !!}
        </div>
    </div>
</div>

<div class="header">
    <h2><strong>Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.school_address') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('school_address', old('school_address',isset($school['school_address']) ? $school['school_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_address'), 'id' => 'school_address', 'rows'=> 2]) !!}
        </div>
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.country') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('country_id', $school['arr_country'],isset($school['country_id']) ? $school['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=> 'showStates(this.value,"state_id")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.state') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('state_id', $school['arr_state'],isset($school['state_id']) ? $school['state_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id' , 'onChange'=> 'showCity(this.value,"city_id")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.city') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('city_id', $school['arr_city'],isset($school['city_id']) ? $school['city_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'city_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_pincode', old('school_pincode',isset($school['school_pincode']) ? $school['school_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_pincode'), 'id' => 'school_pincode', 'maxlength'=> '6', 'minlength'=> '6']) !!}
        </div>
    </div>

</div>


<div class="header">
    <h2><strong>Images</strong> </h2>
</div>

<div class="row clearfix">
    @php 
        for($i = 1; $i <= 6; $i++) {
    @endphp
    <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.school_image')}}</span>
        <label for="file1" class="field file">
            
            <input type="file" class="gui-file" name="school_img{{$i}}" id="school_file{{$i}}" >
            <input type="hidden" class="gui-input" id="school_img{{$i}}" placeholder="Upload Photo" readonly>
        </label>
        @if(isset($school['school_img'.$i]))<img src="{{url($school['school_img'.$i])}}"  height="100px" width="100px">@endif
    </div>
    @if($i == 4)

    </div><br /><br /><div class="row clearfix">
    @endif
    @php 
        }
    @endphp
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });

        $(".fee").keyup(function(){
            $("#school_fee_range_to").valid();
            $("#school_fee_range_from").valid();
        });

        $(".class").keyup(function(){
            $("#school_class_to").valid();
            $("#school_class_from").valid();
        });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        
       
        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) >= parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) <= parseFloat($otherElement.val(), 10);
        },"The value  must be less");
       
        $("#school-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                school_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_registration_no: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_sno_numbers: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_board_of_exams: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_medium: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_class_from: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                    // digits: true,
                    // lessThan: '#school_class_to'
                },
                school_class_to: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                    // digits: true,
                    // greaterThan: '#school_class_from'
                },
                school_fee_range_from: {
                    required: true,
                    number: true,
                    lessThan: '#school_fee_range_to'
                    
                },
                school_fee_range_to: {
                    required: true,
                    number: true,
                    greaterThan: '#school_fee_range_from'
                },
                school_email: {
                    required: true,
                    email: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_mobile_number: {
                    required: true,
                    digits: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_telephone: {
                    digits: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_fax_number: {
                    digits: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_url: {
                    url: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_logo: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_address: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_pincode: {
                    digits: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_img1: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_img2: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_img3: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_img4: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_img5: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_img6: {
                    extension: 'jpg,png,jpeg,gif'
                },
                school_latitude: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                school_longitude: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                }
            }
        });
        
    });

    function checkValidation(){
        alert('Hello');
        $("#school-form").valid();
    }
    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/state/show-state')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/city/show-city')}}",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

</script>