@if(isset($setting['imprest_ac_confi_id']) && !empty($setting['imprest_ac_confi_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('imprest_ac_confi_id',old('imprest_ac_confi_id',isset($setting['imprest_ac_confi_id']) ? $setting['imprest_ac_confi_id'] : ''),['class' => 'gui-input', 'id' => 'imprest_ac_confi_id', 'readonly' => 'true']) !!}

@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.imprest_ac_confi_amt') !!} :</lable>
        <div class="form-group">
            {!! Form::number('imprest_ac_confi_amt', old('imprest_ac_confi_amt',isset($setting['imprest_ac_confi_amt']) ? $setting['imprest_ac_confi_amt']: ''), ['class' => 'form-control','placeholder'=>trans('language.imprest_ac_confi_amt'), 'id' => 'imprest_ac_confi_amt']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/configuration') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9][\s0-9a-zA-Z]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#setting-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                imprest_ac_confi_amt: {
                    required: true,
                    min:0,
                    number: true
                }
            },

            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                }
            }
        });

    });

    

</script>