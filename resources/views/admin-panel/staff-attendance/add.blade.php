@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
    overflow-x: visible;
    }
    input[type='radio']:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    border: 1px solid #d1d3d1 !important;
    background-color: #fff;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .red input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #e80b15;
    border: 1px solid #e80b15 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .green input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 15px;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #235409;
    border: 1px solid #235409 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .document_staff {
    padding-left: 12px;
    margin-right: 30px;
    }
    .red{
    background: none !important;
    }
    .dataTables_filter{ display: none; }
</style>
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{{$page_title}}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="{!! url('admin-panel/staff/attendance/view-attendance') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff') !!}">{!! trans('language.menu_staff') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff') !!}">Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/add-attendance') !!}">{{$page_title}}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap ">
                                @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <div class="row clearfix">
                                    <div class="col-lg-2">
                                        @if(isset($attendance['staff_attendance']))
                                            @php
                                            $currentDate = $attendance['staff_attendance'];
                                            $staff_attendance_id = $attendance['staff_attendance_id'];
                                            @endphp
                                        @else 
                                            @php
                                            $currentDate = $attendance['currentDate'];
                                            $staff_attendance_id = null;
                                            @endphp
                                        @endif
                                        
                                    </div>
                                    
                                </div>
                                @php $msg = ''; $display = "none"; $displayBlock = "block"; @endphp
                                @if($attendance['holiday'] == 1)
                                        @php $msg = date('Y-m-d').' is holiday'; $display = "block"; $displayBlock = "none"; @endphp
                                       
                                @endif
                                
                               
                                {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                <div class="headingcommon col-lg-4 show" style="padding: 15px 0px;">Date :- 
                                    <!-- <span id="show_date">{!! $currentDate !!}</span> -->
                                    <input type="date" name="attendance_date" id="attendance_date" class="form-control date" value="{!! $currentDate !!}" placeholder="Date">
                                    <input type="hidden" name="holiday" id="holiday" class="form-control" value="{!! $attendance['holiday'] !!}" placeholder="Date">
                                </div>
                                <div class="alert alert-success" role="alert" id="messageH" style=" margin-top: 10px; display: {!! $display !!}">
                                    {!! $msg !!}
                                </div>
                                <div id="attendance-block" style="display: {!! $displayBlock !!}">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-raised btn-primary" style="margin-top: -68px !important;" title="Save" >{{$submit_button}}
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- <input type="hidden" name="attendance_date" id="attendance_date" class="form-control" value="{!! $currentDate !!}" placeholder="Date"> -->
                                    {!! Form::hidden('staff_attendance_id', old('staff_attendance_id', $staff_attendance_id), ['id' => 'staff_attendance_id']) !!}
                                    
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="attendance-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.student_unique_id')}}</th>
                                                    <th>{{trans('language.staff_name')}}</th>
                                                    <th>Time Range</th>
                                                    <th>Attendance</th>
                                                    <th>Overtime Type</th>
                                                    <th>Overtime Value</th>
                                                </tr>
                                            </thead>
                                            
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save" onclick="showMessage()">{{$submit_button}}
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                {!! Form::close() !!}
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        var table = $('#attendance-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 200,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/staff/attendance/attendance-data')}}',
                data: function (d) {
                    d.staff_attendance_id = $('input[name="staff_attendance_id"]').val();
                    d.attendance_date = $('input[name="attendance_date"]').val();
                    d.holiday_status = $('input[name="holiday"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'attendance_id', name: 'attendance_id'},
                {data: 'staff_profile', name: 'staff_profile'},
                {data: 'time_range', name: 'time_range'},
                {data: 'attendance', name: 'attendance'},
                {data: 'overtime_type', name: 'overtime_type'},
                {data: 'overtime_value', name: 'overtime_value'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        
        $(document).on("keyup", ".times", function () {
            counter = $(this).attr('counter');
            
            var start_time = $('#in_time'+counter).val(); //start time
            var end_time = $('#out_time'+counter).val();  //end time
            //convert both time into timestamp
            var stt = new Date("November 30, 2018 " + start_time);
            stt = stt.getTime();
            var endt = new Date("November 30, 2018 " + end_time);
            endt = endt.getTime();
            if(stt > endt && end_time != '') {
                $("#out_time_err"+counter).html('Start time should be less than end time');
                $('#out_time'+counter).val('')
            } else {
                $("#out_time_err"+counter).html('');
            }
        });
        $(document).on("click", ".overtime_type", function () {
            counter = $(this).attr('counter');
            if($("#overtime_type_status0"+counter).prop("checked") == true){
                $("#overtime_value"+counter).val('');
                $("#overtime_value"+counter).prop('disabled', true);
            } else {
                $("#overtime_value"+counter).prop('disabled', false);
            }
        });

        $(document).on("keyup", ".date", function (e) {
            var attendance_date = $("#attendance_date").val();
            var CurrentDate = new Date();
            var SelectedDate = new Date(attendance_date);
            
            var type = 2;
            $(".mycustloading").show();
            $("#messageH").css("display",'none');
            if(CurrentDate > SelectedDate || CurrentDate == SelectedDate ){
                
                console.log('true');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/holiday/check-holidays')}}",
                    type: 'GET',
                    data: {
                        'attendance_date': attendance_date,
                        'type': type
                    },
                    success: function (res) {
                        if(res == '1') {
                            $("#messageH").html(attendance_date+' is holiday');
                            $(".mycustloading").hide();
                            $("#messageH").css("display",'block');
                            $("#attendance-block").css("display",'none');
                        } else if(res == '0'){
                            $("#attendance-block").css("display",'block');
                            $("#messageH").css("display",'none');
                            $("#holiday").val(0);
                            table.draw();
                            e.preventDefault();
                            $(".mycustloading").hide();
                        }
                    }
                });
            }
            else{
                $("#messageH").html("Sorry ! You can't add attendance for future date.");
                $("#messageH").css("display",'block');
                //SelectedDate is more than CurrentDate
                console.log('future date');
                $(".mycustloading").hide();
                $("#attendance-block").css("display",'none');
            }
            
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    
    
    });
    
    
</script> 
@endsection