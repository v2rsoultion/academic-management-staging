{!! Form::hidden('vehicle_id',old('vehicle_id',isset($vehicle['vehicle_id']) ? $vehicle['vehicle_id'] : ''),['class' => 'gui-input', 'id' => 'vehicle_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->

<div class="row clearfix"> 
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.vehicle_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('vehicle_name', old('vehicle_name',isset($vehicle['vehicle_name']) ? $vehicle['vehicle_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.vehicle_name'), 'id' => 'vehicle_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.vehicle_registration_number') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('vehicle_registration_no', old('vehicle_registration_no',isset($vehicle['vehicle_registration_no']) ? $vehicle['vehicle_registration_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.vehicle_registration_number'), 'id' => 'vehicle_registration_no']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.vehicle_ownership') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <label class=" field select" style="width: 100%">
                {!!Form::select('vehicle_ownership', $vehicle['arr_vehicle'],isset($vehicle['vehicle_ownership']) ? $vehicle['vehicle_ownership'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'vehicle_ownership'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">{!! trans('language.vehicle_capacity') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('vehicle_capacity', old('vehicle_capacity',isset($vehicle['vehicle_capacity']) ? $vehicle['vehicle_capacity']: ''), ['class' => 'form-control','placeholder'=>trans('language.vehicle_capacity'), 'id' => 'vehicle_capacity']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1"> {!! trans('language.contact_person') !!} :</lable>
        <div class="form-group">
            {!! Form::text('contact_person', old('contact_person',isset($vehicle['contact_person']) ? $vehicle['contact_person']: ''), ['class' => 'form-control','placeholder'=>trans('language.contact_person'), 'id' => 'contact_person']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1"> {!! trans('language.contact_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('contact_number', old('contact_number',isset($vehicle['contact_number']) ? $vehicle['contact_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.contact_number'), 'id' => 'contact_number','minlength'=> '10', 'maxlength'=> '10']) !!}
        </div>
    </div>
</div>

<!-- Document Information -->
<div class="header">
    <h2><strong>Document</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($vehicle['documents']) ? COUNT($vehicle['documents']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    @if(isset($vehicle['documents']) && !empty($vehicle['documents']))
    @php $key = 0; @endphp
    @foreach($vehicle['documents'] as $documentKey => $documents)
    @php 
        $vehicle_document_category = "documents[".$key."][document_category_id]";  
        $vehicle_document_id = "documents[".$key."][vehicle_document_id]"; 
    @endphp
    <div id="document_block{{$documentKey}}">
        {!! Form::hidden($vehicle_document_id,$documents['vehicle_document_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.vehicle_document_category') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select($vehicle_document_category, $vehicle['arr_document_category'],$documents['document_category_id'], ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id','required'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            
            </div>
            <div class="col-lg-5 col-md-5">
            <span class="radioBtnpan">{{trans('language.vehicle_document_file')}}</span> <span class="red-text">*</span><br />
                <label for="file1" class="field file">
                    
                    <input type="file" class="gui-file" name="documents[{{$key}}][vehicle_document_file]" id="vehicle_document_file{{$key}}" accept="image/*">
                    <input type="hidden" class="gui-input" id="vehicle_document_file{{$key}}" placeholder="Upload Photo" readonly>
                    
                    @if(isset($documents['vehicle_document_file']) && $documents['vehicle_document_file'] != "")<a href="{{url($documents['vehicle_document_file'])}}" target="_blank" >View Document</a>@endif
                </label>
            
            </div>
            <div class="col-lg-1 col-md-1">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($documentKey == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$documentKey}})" >Remove</button>
                @endif
            </div>
        </div>

        <!-- Document detail section -->
        <div class="row clearfix">
            <div class="col-sm-12 text_area_desc">
                <lable class="from_one1">{!! trans('language.vehicle_document_details') !!} <span class="red-text">*</span> :</lable>
                <div class="form-group">
                    <textarea name="documents[{{$key}}][vehicle_document_details]" id="" class="form-control" placeholder="{{trans('language.vehicle_document_details')}}" rows="2" required>{!! $documents['vehicle_document_details'] !!}</textarea>
                   
                </div>
                
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.vehicle_document_category') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group ">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('documents[0][document_category_id]', $vehicle['arr_document_category'],isset($vehicle['document_category_id']) ? $vehicle['document_category_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id','required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.vehicle_document_file')}}</span> <span class="red-text">*</span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="documents[0][vehicle_document_file]" id="vehicle_document_file" onChange="document.getElementById('vehicle_document_file').value = this.value;" accept="image/*" required>
                <input type="hidden" class="gui-input" id="vehicle_document_file" placeholder="Upload Photo" readonly>
               
            </label>
           
        </div>
        <div class="col-lg-3 col-md-3">&nbsp;</div>
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button></div>
    </div>

    <!-- Document detail section -->
    <div class="row clearfix">
        <div class="col-sm-12 text_area_desc">
            <lable class="from_one1">{!! trans('language.vehicle_document_details') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group">
                {!! Form::textarea('documents[0][vehicle_document_details]', old('vehicle_document_details',isset($vehicle['vehicle_document_details']) ? $vehicle['vehicle_document_details']: ''), ['class' => 'form-control','placeholder'=>trans('language.vehicle_document_details'), 'id' => 'vehicle_document_details', 'rows'=> 2,'required']) !!}
            </div>
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/transport') !!}" class="btn btn-raised" >Cancel</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });


    // $(document).on('click','.gui-file',function(e){
    //     //var ext = $(this.value).val().split('.').pop().toLowerCase();
    //     // if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    //     //     alert('invalid extension!');
    //     // }
    //     alert();
    // });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 

        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#vehicle-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                vehicle_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                // staff_profile_img: {
                //     extension: 'jpg,png,jpeg,gif'
                // },
                vehicle_registration_no: {
                    required: true
                },
                vehicle_ownership: {
                    required: true
                },
                vehicle_capacity: {
                    required: true,
                    digits: true
                },
                contact_person: {
                    lettersonly: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                contact_number: {
                    digits: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });


    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b);
        
        $("#sectiontable-body").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans("language.student_document_category") !!} <span class="red-text">*</span> :</lable><div class="form-group "><label class=" field select" style="width: 100%">  <select class="form-control show-tick select_form1 select2" id="document_category_id'+arr_key+'" name="documents['+arr_key+'][document_category_id]" required>@foreach ($vehicle["arr_document_category"] as $document_key => $document_category) <option value="{{ $document_key}}">{!! $document_category !!}</option>@endforeach</select><i class="arrow double"></i></label></div></div><div class="col-lg-3 col-md-3"><span class="radioBtnpan">{{trans("language.vehicle_document_file")}}</span> <span class="red-text">*</span> <label for="file1" class="field file"><input type="file" class="gui-file" name="documents['+arr_key+'][vehicle_document_file]" id="file'+arr_key+'" accept="image/*" required><input type="hidden" class="gui-input" id="vehicle_document_file'+arr_key+'" placeholder="Upload Photo" readonly></label></div><div class="col-lg-3 col-md-3">&nbsp;</div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn red" type="button" onclick="remove_block('+arr_key+')" >Remove</button></div></div><div class="row clearfix"><div class="col-sm-12 text_area_desc"><lable class="from_one1">{!! trans("language.vehicle_document_details") !!} <span class="red-text">*</span> :</lable><div class="form-group"><textarea name="documents['+arr_key+'][vehicle_document_details]" class="form-control" placeholder="{{trans("language.vehicle_document_details")}}" id="vehicle_document_details'+arr_key+'" rows="2" required></textarea></div> </div></div></div>');
        $("#document_category_id"+arr_key).select2();
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

</script>