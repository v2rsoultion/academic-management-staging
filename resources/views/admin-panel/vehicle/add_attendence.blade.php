@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
    overflow-x: visible;
    }
    input[type='radio']:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    border: 1px solid #d1d3d1 !important;
    background-color: #fff;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .red input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #e80b15;
    border: 1px solid #e80b15 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .green input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 15px;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #235409;
    border: 1px solid #235409 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .document_staff {
    padding-left: 12px;
    margin-right: 30px;
    }
    .red{
    background: none !important;
    }
    .dataTables_filter{ display: none; }
</style>
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{{$page_title}}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="{!! url('admin-panel/transport/vehicle/view-attendance/'.$decrypted_vehicle_id) !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport/vehicle/vehicle-attendance') !!}">Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="#">{{$page_title}}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>Vehicle</strong> Information 
                                </h2>

                                <div class="std1" style="margin-bottom: -45px;">
                                    <div class="profile_detail_3">
                                        <label> <span>Vehicle Name : </span> {{ $vehicle->vehicle_name }}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        @if($vehicle['getDriver'] != "")
                                            <label> <span>Driver: </span> {{ $vehicle['getDriver']->staff_name }}</label>
                                        @else 
                                            <label> <span>Driver: </span> Not Available </label>
                                        @endif
                                    </div>
                                    <div class="profile_detail_3">
                                        @if($vehicle['getConductor'] != "")
                                            <label> <span>Conductor: </span> {{ $vehicle['getConductor']->staff_name }}</label>
                                        @else 
                                            <label> <span>Conductor: </span> Not Available </label>
                                        @endif
                                    </div>
                                </div> 

                            </div>
                            <div class="clearfix"></div>

                            <div class="body form-gap ">
                                @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <div class="row clearfix">
                                    <div class="col-lg-2">
                                        @if(isset($attendance['staff_attendance']))
                                            @php
                                            $currentDate = $attendance['staff_attendance'];
                                            $staff_attendance_id = $attendance['staff_attendance_id'];
                                            @endphp
                                        @else 
                                            @php
                                            $currentDate = $attendance['currentDate'];
                                            $staff_attendance_id = null;
                                            @endphp
                                        @endif
                                        
                                    </div>
                                    
                                </div>
                                @php $msg = ''; $display = "none"; $displayBlock = "block"; @endphp
                                @if($attendance['holiday'] == 1)
                                        @php $msg = date('Y-m-d').' is holiday'; $display = "block"; $displayBlock = "none"; @endphp
                                       
                                @endif
                                
                               
                                {!! Form::open(['files'=>TRUE,'id' => 'list-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                <div class="headingcommon col-lg-4 show" style="padding: 15px 0px;">Date :- 
                                    <!-- <span id="show_date">{!! $currentDate !!}</span> -->
                                    <input type="date" name="attendance_date" id="attendance_date" class="form-control date" value="{!! $currentDate !!}" placeholder="Date">
                                    <input type="hidden" name="vehicle_id" id="vehicle_id" class="form-control" value="{!! $vehicle['vehicle_id'] !!}" placeholder="Date">

                                    <input type="hidden" name="vehicle_attendence_id" id="vehicle_attendence_id" class="form-control" value="{!! $attendance['vehicle_attendence_id'] !!}" placeholder="Date">
                                </div>
                                <div class="alert alert-success" role="alert" id="messageH" style=" margin-top: 10px; display: {!! $display !!}">
                                    {!! $msg !!}
                                </div>
                                <div id="attendance-block" style="display: {!! $displayBlock !!}">
                                    <div class="float-right">
                                        <button type="submit" id ="upper_save" class="btn btn-raised btn-primary" style="margin-top: -68px !important;" title="Save" >{{$submit_button}}
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="attendance-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.student_staff_name')}}</th>
                                                    <th>{{trans('language.student_staff_type')}}</th>
                                                    <th>Attendance</th>
                                                    <th>Attendence Type</th>
                                                </tr>
                                            </thead>
                                            
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="float-right">
                                        <button type="submit" id ="lower_save" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save" onclick="showMessage()">{{$submit_button}}
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                {!! Form::close() !!}
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        var table = $('#attendance-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 200,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/transport/vehicle/attendance-data')}}',
                data: function (d) {
                    d.vehicle_id = $('input[name="vehicle_id"]').val();
                    d.vehicle_attendence_id = $('input[name="vehicle_attendence_id"]').val();
                    d.attendance_date = $('input[name="attendance_date"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_staff_name', name: 'student_staff_name'},
                {data: 'student_staff_type_new', name: 'student_staff_type_new'},
                {data: 'attendance', name: 'attendance'},
                {data: 'overtime_type', name: 'overtime_type'},
            ],
            //  columnDefs: [
            //     {
            //         "targets": 0, // your case first column
            //         "width": "10%"
            //     },
            //     {
            //         "targets": 1, // your case first column
            //         "width": "10%"
            //     },
            //     {
            //         "targets": 2, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         "targets": 3, // your case first column
            //         "width": "25%"
            //     },
            //     {
            //         "targets": 4, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         "targets": 5, // your case first column
            //         "width": "15%"
            //     },
            //     {
            //         targets: [ 0, 1, 2, 3 ],
            //         className: 'mdl-data-table__cell--non-numeric'
            //     }
            // ]
        });
        
        $(document).on("click", ".attendence_type", function () {
            counter = $(this).attr('counter');

            if($("#attendance_status2"+counter).prop("checked") == true){
               // $("#overtime_type_status0"+counter).val('');
                $("#overtime_type_status1"+counter).prop('disabled', true);
                $("#overtime_type_status2"+counter).prop('disabled', true);
            } else {
                $("#overtime_type_status1"+counter).prop('disabled', false);
                $("#overtime_type_status2"+counter).prop('disabled', false);
            }
        });



        $(document).on("keyup", ".date", function (e) {
            var attendance_date = $("#attendance_date").val();
            var CurrentDate = new Date();
            var SelectedDate = new Date(attendance_date);
            
            var type = 2;
            $(".mycustloading").show();
            $("#messageH").css("display",'none');
            if(CurrentDate > SelectedDate || CurrentDate == SelectedDate ){
                
                $("#lower_save").removeAttr("disabled");
                $("#upper_save").removeAttr("disabled");
                console.log('true');    
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/holiday/check-holidays')}}",
                    type: 'GET',
                    data: {
                        'attendance_date': attendance_date,
                        'type': type
                    },
                    success: function (res) {
                        if(res == '1') {
                            $("#messageH").html(attendance_date+' is holiday');
                            $(".mycustloading").hide();
                            $("#messageH").css("display",'block');
                            $("#attendance-block").css("display",'none');
                        } else if(res == '0'){
                            $("#attendance-block").css("display",'block');
                            $("#messageH").css("display",'none');
                            $("#holiday").val(0);
                            table.draw();
                            e.preventDefault();
                            $(".mycustloading").hide();
                        }
                    }
                });
            }
            else{
                $("#lower_save").attr("disabled", "disabled");
                $("#upper_save").attr("disabled", "disabled");
                $("#messageH").html("Sorry ! You can't add attendance for future date.");
                $("#messageH").css("display",'block');
                //SelectedDate is more than CurrentDate
                console.log('future date');
                $(".mycustloading").hide();
                $("#attendance-block").css("display",'none');
            }
            
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    
    
    });
    
    
</script> 
@endsection