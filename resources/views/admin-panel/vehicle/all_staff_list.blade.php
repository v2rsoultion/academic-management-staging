@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
.search_button4{
    margin-left: 20px !important;
}
</style>

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <h2>{!! trans('language.staff_list') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport/vehicle/manage-student-staff') !!}">{!! trans('language.manage_student_staff') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.staff_list') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>Vehicle</strong> Information 
                                </h2>

                                <div class="std1" style="margin-bottom: -30px;">
                                    <div class="profile_detail_3">
                                        <label> <span>Vehicle Name : </span> {{ $vehicle->vehicle_name }}</label>
                                    </div>

                                    <div class="profile_detail_3">

                                        <label> <span>Driver - Conductor : </span>
                                            @if($vehicle['getDriver']->staff_name != "")
                                                {{ $vehicle['getDriver']->staff_name }}
                                            @else 
                                                Not Available
                                            @endif

                                            -

                                            @if($vehicle['getConductor']->staff_name != "")
                                                {{ $vehicle['getConductor']->staff_name }}
                                            @else 
                                                Not Available
                                            @endif
                                        </label>
                                    </div>

                                    <!-- <div class="profile_detail_3">
                                        @if($vehicle['getDriver'] != "")
                                            <label> <span>Driver: </span> {{ $vehicle['getDriver']->staff_name }}</label>
                                        @else 
                                            <label> <span>Driver: </span> Not Available </label>
                                        @endif
                                    </div>
                                    <div class="profile_detail_3">
                                        @if($vehicle['getConductor'] != "")
                                            <label> <span>Conductor: </span> {{ $vehicle['getConductor']->staff_name }}</label>
                                        @else 
                                            <label> <span>Conductor: </span> Not Available </label>
                                        @endif
                                    </div> -->
                                </div> 

                            </div>
                            <div class="clearfix"></div>
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.staff_name'), 'id' => 'name']) !!}

                                                
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        
                                        {!! Form::hidden('vehicle_id',old('vehicle_id',isset($vehicle['vehicle_id']) ? $vehicle['vehicle_id'] : ''),['class' => 'gui-input', 'id' => 'vehicle_id', 'readonly' => 'true']) !!}
                                        
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="staff-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.staff_name')}}</th>
                                                <th>{{trans('language.staff_designation_name')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </div>
    </div>
</section>


<script>

    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/transport/vehicle/view-all-staff-data')}}',
                data: function (d) {
                    d.name = $('input[name="name"]').val();
                    d.vehicle_id = $('input[name="vehicle_id"]').val();
                    d.type = 2;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
@endsection



