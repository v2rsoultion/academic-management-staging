@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.attendence_report') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.attendence_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>Attendance</strong> Report 
                                </h2>
                            </div>
                            <div class="clearfix"></div>

                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('vehicle_id', $list['arr_vehicle'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'vehicle_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        

                                        <div class="col-md-3">
                                            {!! Form::date('vehicle_attendance', old('vehicle_attendance', ''), ['class' => 'form-control ','placeholder'=>trans('language.vehicle_attendance'), 'id' => 'vehicle_attendance']) !!}
                                        </div>

                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="attendance-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.vehicle_name')}}</th>
                                                <th>{{trans('language.date')}}</th>
                                                <th>{{trans('language.total_present_staff')}}</th>
                                                <th>{{trans('language.total_present_student')}}</th>
                                                <th>{{trans('language.total_strength')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
        $('#student_attendance').bootstrapMaterialDatePicker({ time: false,shortTime: true}).on('change', function(e, date){ $(this).valid(); });
    });
    $(document).ready(function () {
        var table = $('#attendance-table').DataTable({
            dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            buttons: [
                'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/transport/vehicle/attendance/report-attendance-records')}}',
                data: function (d) {
                    d.vehicle_attendance = $('input[name="vehicle_attendance"]').val();
                    d.vehicle_id = $('input[name="vehicle_id"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'vehicle_name', name: 'vehicle_name'},
                {data: 'vehicle_attendence_date', name: 'vehicle_attendence_date'},
                {data: 'total_present_staff', name: 'total_present_staff'},
                {data: 'total_present_student', name: 'total_present_student'},
                {data: 'total_strength', name: 'total_strength'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });
   
    


</script>
@endsection




