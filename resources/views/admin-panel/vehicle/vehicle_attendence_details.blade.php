@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
    overflow-x: visible;
    }
    input[type='radio']:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    border: 1px solid #d1d3d1 !important;
    background-color: #fff;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .red input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #e80b15;
    border: 1px solid #e80b15 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .green input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 15px;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #235409;
    border: 1px solid #235409 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .document_staff {
    padding-left: 12px;
    margin-right: 30px;
    }
    .red{
    background: none !important;
    }
    .dataTables_filter{ display: none; }
</style>
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{{$page_title}}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport') !!}">{!! trans('language.menu_transport') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/transport/vehicle/vehicle-attendance') !!}">Attendance</a></li>
                    <li class="breadcrumb-item active">{{$page_title}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>Vehicle</strong> Information 
                                </h2>

                                <div class="std1" style="margin-bottom: -30px;">
                                    <div class="profile_detail_3">
                                        <label> <span>Vehicle Name : </span> {{ $vehicle->vehicle_name }}</label>
                                    </div>
                                    <div class="profile_detail_3">
                                        @if($vehicle['getDriver'] != "")
                                            <label> <span>Driver: </span> {{ $vehicle['getDriver']->staff_name }}</label>
                                        @else 
                                            <label> <span>Driver: </span> Not Available </label>
                                        @endif
                                    </div>
                                    <div class="profile_detail_3">
                                        @if($vehicle['getConductor'] != "")
                                            <label> <span>Conductor: </span> {{ $vehicle['getConductor']->staff_name }}</label>
                                        @else 
                                            <label> <span>Conductor: </span> Not Available </label>
                                        @endif
                                    </div>
                                </div> 

                            </div>
                            <div class="clearfix"></div>

                            <div class="body form-gap ">
                                @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif

                                <input type="hidden" name="vehicle_id" id="vehicle_id" class="form-control" value="{!! $vehicle_id !!}" placeholder="Date">
                                <input type="hidden" name="vehicle_attendence_id" id="vehicle_attendence_id" class="form-control" value="{!! $vehicle_attendence_id !!}" placeholder="Date">
                                
                                <div id="attendance-block"> 
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="attendance-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.student_staff_name')}}</th>
                                                    <th>{{trans('language.student_staff_type')}}</th>
                                                    <th>Attendance</th>
                                                </tr>
                                            </thead>
                                            
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="clearfix"></div>
                                </div>
                                {!! Form::close() !!}
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        var table = $('#attendance-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 200,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            info: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/transport/vehicle/view-attendance-details-data')}}',
                data: function (d) {
                    d.vehicle_id = $('input[name="vehicle_id"]').val();
                    d.vehicle_attendence_id = $('input[name="vehicle_attendence_id"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_staff_name', name: 'student_staff_name'},
                {data: 'student_staff_type_new', name: 'student_staff_type_new'},
                {data: 'attendance', name: 'attendance'},
            ],

        });
        
        
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    
    
    });
    
    
</script> 
@endsection