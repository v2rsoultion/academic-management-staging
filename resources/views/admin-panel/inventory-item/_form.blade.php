@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row" >
  
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.item_category_name') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('category_id', $items['arr_category'] ,isset($items['category_id']) ? $items['category_id']: '', ['class' => 'form-control show-tick select_form1 select2','id'=>'category_id', 'onChange'=>'getSubCategory(this.value)'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.item_sub_category_name') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('sub_category_id',$items['arr_subcategory_data'] , isset($items['sub_category_id']) ? $items['sub_category_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'sub_category_id'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1">{!! trans('language.item_name') !!}</lable>
      {!! Form::text('item_name',isset($items->item_name) ? $items->item_name : '',['class' => 'form-control','placeholder'=> trans('language.item_name'), 'id' => 'item_name']) !!}
    </div>
  </div>
  <div class="col-lg-3 col-md-3">
    <lable class="from_one1">{!! trans('language.item_measuring_unit') !!}</lable>
    <div class="form-group m-bottom-0">
      <label class=" field select size">
        {!!Form::select('unit_id', $items['arr_unit_data'], isset($items['unit_id']) ? $items['unit_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'unit_id'])!!}
        <i class="arrow double"></i>
      </label>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="{{ url('admin-panel/inventory/manage-items') }}" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#manage_item").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                category_id: {
                    required: true
                },
                sub_category_id: {
                    required:true
                },
                item_name: {
                    required:true
                },
                unit_id: {
                    required:true
                }

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

  
 function getSubCategory(category_id)
    {
        if(category_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/inventory/get-subcategory-data')}}",
                type: 'GET',
                data: {
                    'category_id': category_id
                },
                success: function (data) {
                    $("select[name='sub_category_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='sub_category_id'").html('<option value="">Select Sub-Category</option>');
            $(".mycustloading").hide();
        }
    }

</script>