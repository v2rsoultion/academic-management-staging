@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
    overflow-x: visible;
    }
    input[type='radio']:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    border: 1px solid #d1d3d1 !important;
    background-color: #fff;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .red input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 100%;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #e80b15;
    border: 1px solid #e80b15 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .green input[type='radio']:checked:after {
    width: 18px;
    height: 18px;
    border-radius: 15px;
    top: -2px;
    left: -3px;
    position: relative;
    background-color: #235409;
    border: 1px solid #235409 !important;
    content: '';
    display: inline-block;
    visibility: visible;
    border: 2px solid white;
    }
    .document_staff {
    padding-left: 12px;
    margin-right: 30px;
    }
    .red{
    background: none !important;
    }
</style>
<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! trans('language.my_attendance') !!}</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.my_attendance') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">

                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group ">
                                                {!! Form::date('student_attendance_date', old('student_attendance_date', ''), ['class' => 'form-control ','placeholder'=>trans('language.student_attendance_date'), 'id' => 'student_attendance_date']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div>
                                <hr>
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                        {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.date')}}</th>
                                                    <th>{{trans('language.attendance')}}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
                //dom: 'Blfrtip',
                pageLength: 20,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ],
                ajax: {
                    url: '{{url('admin-panel/student/get-student-attendance')}}',
                    data: function (d) {
                        d.class_id = $('select[name="class_id"]').val();
                        d.section_id = $('select[name="section_id"]').val();
                        d.attendance_date = $('#student_attendance_date').val();
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    {data: 'attendance_date', name: 'attendance_date'},
                    {data: 'attendance', name: 'attendance'},
                ],
                // columnDefs: [
                //     {
                //         "targets": 0, // your case first column
                //         "width": "10%"
                //     },
                //     {
                //         "targets": 3, // your case first column
                //         "width": "20%"
                //     },
                //     {
                //         targets: [ 0, 1, 2, 3 ],
                //         className: 'mdl-data-table__cell--non-numeric'
                //     }
                // ]
            });
        $('#search-form').on('submit', function(e) {
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            var student_attendance_date = $("#student_attendance_date").val();
            if(class_id != "" && section_id != "" && student_attendance_date !=""){
                $("#list-block").show();
                table.draw();
                e.preventDefault();
            } else {
                $("#list-block").hide();
            }
            e.preventDefault();
        });
          
        $('#clearBtn').click(function(){
           location.reload();
        })


    });

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }

    jQuery(document).ready(function () {
        $.validator.addMethod("minDate", function(value, element) { 
            var inputDate = new Date(value);
            var inputDate1 = new Date(inputDate.getMonth() + 1 + '/' + inputDate.getDate() + '/' + inputDate.getFullYear()).getTime(); 
            var date = new Date();  
            var curDate = new Date(date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear()).getTime();  
            
            if (curDate > inputDate1 && curDate != inputDate1) {
                return true;
            } else {
                return false;
            }
        }, "Please select past date");

        $("#search-form").validate({
            /* @validation states + elements   ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                class_id: {
                    required: true,
                },
                section_id: {
                    required: true,
                },
                student_attendance_date: {
                    required: true,
                    date: true,
                    minDate: true
                }
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
    });

</script>
@endsection




