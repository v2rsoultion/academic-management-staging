@extends('admin-panel.layout.header')

@section('content')
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <!-- <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div> -->
                    <div class="body  form-gap">
                    {!! Form::open(['files'=>TRUE,'id' => 'student-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    @include('admin-panel.student._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="updateContactNoModel" tabindex="-1" style="margin-top: 100px;" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Update Contact No  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body" style="min-height: 200px; padding-top: 0px;">
            <div id="form-block" style="display: block">
            {!! Form::open(['files'=>TRUE,'id' => 'info-form' , 'class'=>'form-horizontal']) !!}
                
                <p class="green" id="grade_success"></p>
                <div class="alert alert-danger" role="alert" id="error-block1" style="display: none;"></div>
                <div class="alert alert-success" role="alert" id="success-block1" style="display: none;"></div>
                
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <lable class="from_one1">{!! trans('language.student_login_contact_no') !!} :</lable>
                    <div class="form-group">
                        {!! Form::text('update_student_login_contact_no', old('update_student_login_contact_no', ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_contact_no'), 'id' => 'update_student_login_contact_no', 'maxlength'=> '10','onkeyup' => "check_parent_existance1()"]) !!}
                        
                        {!! Form::hidden('error_status','0', ['id' => 'error_status']) !!}
                    </div>
                </div>
                    <div class="col-lg-4 col-md-4">
                        {!! Form::button('Update', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Update','onClick' => "update_contact_no()"]) !!}
                    </div>
                    <div class="col-lg-6 col-md-6">
                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

