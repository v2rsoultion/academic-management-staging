@extends('admin-panel.layout.header')
@section('content')
<style>
p{
    margin-bottom: 5px;
}
</style>
<section class="content contact">

    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/student/view-homework') !!}"> {!! $page_title !!} </a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('staff_id', $listData['arr_teacher'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('subject_id', $listData['arr_subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        
                                        {!! Form::hidden('student_class_id',$listData['arr_class_id']) !!}
                                        {!! Form::hidden('student_section_id',$listData['arr_section_id']) !!}


                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div>
                                <hr>
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="homework-table" style="width:100%">
                                        {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.subject_name')}}</th>
                                                    <th>{{trans('language.teacher_name')}}</th>
                                                    <th>{{trans('language.homework')}}</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade" id="infoModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Homework Information </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body" style="min-height: 200px; padding-top: 20px;">
            <div class="info_block">
                <div><b>Staff:</b> <span id="staff_name"></span></div>
                <div><b>Date:</b> <span id="date"></span></div>
                <div><b>Subject:</b> <span id="subject_name"></span> </div>
                <div><b>Class - Section:</b>  <span id="class_name"></span> <span id="class_name"></span></div>
                <div><b>Homework:</b> <span id="h_conversation_text"></span></div>
                <div><b>Attechment:</b> <span id="attechUrl"></span></div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#homework-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/student/get-homework-data')}}',
                data: function (d) {
                    d.staff_id = $('select[name="staff_id"]').val();
                    d.subject_id = $('select[name="subject_id"]').val();
                    d.student_class_id = $('input[name=student_class_id]').val();
                    d.student_section_id = $('input[name=student_section_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'subject_name', name: 'subject_name'},
                {data: 'staff_name', name: 'staff_name'},
                {data: 'homework', name: 'homework'},
                {data: 'homework_date', name: 'homework_date'},
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            var student_attendance_date = $("#student_attendance_date").val();
            if(class_id != "" && section_id != "" && student_attendance_date !=""){
                $("#list-block").show();
                table.draw();
                e.preventDefault();
            } else {
                $("#list-block").hide();
            }
            e.preventDefault();
        });
          
        $('#clearBtn').click(function(){
           location.reload();
        })

        $(document).on('click','.homework',function(e){
            var h_conversation_id = $(this).attr('h-conversation-id');
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-homework-info')}}",
                type: 'GET',
                data: {
                    'h_conversation_id': h_conversation_id
                },
                success: function (data) {
                    if(data != ''){
                        $("#subject_name").html(data.subject_name);
                        $("#staff_name").html(data.staff_name);
                        $("#class_name").html(data.class_name);
                        $("#section_name").html(data.section_name);
                        $("#h_conversation_text").html(data.h_conversation_text);
                        $("#date").html(data.date);
                        $("#attechUrl").html(data.attechUrl);
                        $('#infoModel').modal('show');
                    }
                    $(".mycustloading").hide();
                }
            });
            $('#subjectModel').modal('show');
        });

    });

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }

    jQuery(document).ready(function () {
        
        $("#search-form").validate({
            /* @validation states + elements   ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                class_id: {
                    required: true,
                },
                section_id: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
    });

</script>
@endsection




