@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_notes') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/notes/add-notes') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/online-content') !!}">{!! trans('language.menu_online_content') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_notes') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">     
                                                                      
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSubject(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('subject_id', $listData['arr_subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('subject_id')) <p class="help-block">{{ $errors->first('subject_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="">
                                <table class="table m-b-0 c_list" id="notes-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.t_name')}}</th>
                                            <th>{{trans('language.class_section')}}</th>
                                            <th>{{trans('language.subject')}}</th>
                                            <th>{{trans('language.notes_unit')}}</th>
                                            <th>{{trans('language.file_url')}}</th>
                                            <th>{{trans('language.notes_topic')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    $(document).ready(function () {
        var table = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
        
            ajax: {
                url: '{{url('admin-panel/notes/data')}}',
                data: function (d) {
                    d.subject_id = $('select[name=subject_id]').val();
                    d.class_id = $('select[name="class_id"]').val();
                }
            },
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'online_note_name', name: 'online_note_name'},
                {data: 'class_section', name: 'class_section'},
                {data: 'class_subject', name: 'class_subject'},
                {data: 'online_note_unit', name: 'online_note_unit'},
                {data: 'file_url', name: 'file_url'},
                {data: 'online_note_topic', name: 'online_note_topic'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "12%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "12%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }
    function getSubject(class_id)
    {
        if(class_id != "") {
            var medium_type = $('#medium_type').val();
            $('.mycustloading').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/subject/get-class-subject-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'medium_type': medium_type
                },
                success: function (data) {
                        $("select[name='subject_id'").html(data.options);
                        $('.mycustloading').hide();
                }
            });
        } else {
            
            $("select[name='subject_id'").html('<option value="">Select Subject</option>');
        }
    }
</script>
@endsection




