{!! Form::hidden('job_id',old('job_id',isset($job['job_id']) ? $job['job_id'] : ''),['class' => 'gui-input', 'id' => 'job_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->

<div class="row clearfix"> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.job_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('job_name', old('job_name',isset($job['job_name']) ? $job['job_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.job_name'), 'id' => 'job_name']) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1">{!! trans('language.medium_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('medium_type', $job['arr_medium'],isset($job['job_medium_type']) ? $job['job_medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.job_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <div class="radio" style="margin-top:6px !important;">
                @if(isset($job['job_type'])) 
                    {{ Form::radio('job_type', '0',  $job['job_type'] == '0', array('id'=>'radio12')) }}
                    {{ Form::label('radio12', 'Temporary', array('onclick'=>'checkForTemporary("0")', 'class' => 'document_staff')) }}

                    {{ Form::radio('job_type', '1',  $job['job_type'] == '1', array('id'=>'radio2')) }}
                {{ Form::label('radio2', 'Permament', array('onclick'=>'checkForTemporary("1")', 'class' => 'document_staff')) }}
                @else
                {{ Form::radio('job_type', '0',  false, array('id'=>'radio12')) }}
                {{ Form::label('radio12', 'Temporary', array('onclick'=>'checkForTemporary("0")', 'class' => 'document_staff')) }}
                {{ Form::radio('job_type', '1',  true, array('id'=>'radio2')) }}
                {{ Form::label('radio2', 'Permament', array('onclick'=>'checkForTemporary("1")', 'class' => 'document_staff')) }}
                @endif
                
            </div>
        </div>
    </div>   
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3" style="@if(isset($job['job_type']) && $job['job_type'] == '0') display:block; @else  display:none; @endif" id="jobTemporaryBlock">
            <lable class="from_one1">{!! trans('language.job_durations') !!} <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">

                    {!! Form::text('job_durations', old('job_durations',isset($job['job_durations']) ? $job['job_durations']: ''), ['class' => 'form-control ','placeholder'=>trans('language.job_durations'), 'id' => 'job_durations']) !!}

<!--                     {!!Form::select('job_durations', $job['arr_duration'],isset($job['job_durations']) ? $job['job_durations'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'job_durations', 'required'])!!} -->

                    <i class="arrow double"></i>
                </label>
            </div>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.job_no_of_vacancy') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('job_no_of_vacancy', old('job_no_of_vacancy',isset($job['job_no_of_vacancy']) ? $job['job_no_of_vacancy']: ''), ['class' => 'form-control ','placeholder'=>trans('language.job_no_of_vacancy'), 'id' => 'job_no_of_vacancy', 'min' => 1]) !!}
        </div>
    </div>
</div>

<div class="row clearfix">
    <!-- <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.job_recruitment') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('job_recruitment', old('job_recruitment',isset($job['job_recruitment']) ? $job['job_recruitment']: ''), ['class' => 'form-control','placeholder'=>trans('language.job_recruitment'), 'id' => 'job_recruitment', 'rows'=> 3]) !!}
        </div>
    </div> -->
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.job_description') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::textarea('job_description', old('job_description',isset($job['job_description']) ? $job['job_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.job_description'), 'id' => 'job_description', 'rows'=> 3]) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/recruitment') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

         jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $("#job-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules   ------------------------------------------ */
            rules: {
                job_name: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                job_no_of_vacancy: {
                    required: true
                },
                job_durations: {
                    required: true
                },
                 job_recruitment: {
                    required: true
                },
                 job_description: {
                    required: true
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        //Added later by Pratyush on 18 July 2018
        $('#shift_start_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        $('#shift_end_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        
    });

    function checkForTemporary(val) {
        var x = document.getElementById("jobTemporaryBlock");
        if(val == 1) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

</script>