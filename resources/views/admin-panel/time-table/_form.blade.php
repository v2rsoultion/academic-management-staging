@if(isset($time_table['class_id']) && !empty($time_table['class_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif
{!! Form::hidden('time_table_id',old('time_table_id',isset($shift['time_table_id']) ? $shift['time_table_id'] : ''),['class' => 'gui-input', 'id' => 'time_table_id', 'readonly' => 'true']) !!}
<!-- {!! Form::hidden('class_id',old('class_id',isset($time_table['class_id']) ? $time_table['class_id'] : ''),['class' => 'gui-input', 'id' => 'class_id', 'readonly' => 'true']) !!} -->

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.time_table_class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('name', old('name',isset($time_table['time_table_name']) ? $time_table['time_table_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.time_table_class_name'), 'id' => 'name']) !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.class') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $time_table['arr_class'],isset($time_table['class_id']) ? $time_table['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange'=>'getClassSection(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.time_table_section') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('section_id', $time_table['arr_section'],isset($time_table['section_id']) ? $time_table['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.lecture_no') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('lecture_no', old('lecture_no',isset($time_table['lecture_no']) ? $time_table['lecture_no']: '1'), ['class' => 'form-control','placeholder'=>trans('language.lecture_no'), 'id' => 'lecture_no', 'min'=> '0', 'max'=> '10']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.time_table_week_day') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('week_days[]', $time_table['arr_week_days'],isset($time_table['week_days']) ? $time_table['week_days'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'week_days','multiple' => 'multiple', 'required'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>


</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/academic') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#class-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                class_id: {
                    required: true 
                },
                section_id: {
                    required: true
                },
                lecture_no: {
                    required: true,
                    digits: true
                },
                week_days: {
                    required: true
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    function getClassSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/section/get-class-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }

</script>