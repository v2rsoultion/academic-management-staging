@if(isset($exam['exam_id']) && !empty($exam['exam_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('exam_id',old('exam_id',isset($exam['exam_id']) ? $exam['exam_id'] : ''),['class' => 'gui-input', 'id' => 'exam_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!}  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $account['arr_class'],isset($account['class_id']) ? $account['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange'=>'getTerms(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
    </div>
     
    <div class="col-lg-3 col-md-3"> <br />
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/examination') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[A-Za-z0-9- ]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
        }, "please use only alphanumeric or alphabetic characters");

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#exam-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                medium_type: {
                    required: true,
                },
                exam_name: {
                    required: true,
                    specialChars:true,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });
    function getTerms(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/get-term-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='term_exam_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='term_exam_id'").html('<option value="">Select Term</option>');
            $(".mycustloading").hide();
        }
    }

    

</script>