@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.pf_setting') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-pf-setting') !!}">{!! trans('language.pf_setting') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_pf_setting' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-pf-setting._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
    <!--  DataTable for view Records  -->
  <div class="container-fluid">
    <div class="card">
      <div class="body form-gap">
        @if(session()->has('success'))
          <div class="alert alert-success" role="alert">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
        <div class="table-responsive">
          <table class="table m-b-0 c_list" id="pf_setting_table" style="width:100%">
            {{ csrf_field() }}
            <thead>
              <tr>
                <th>{!! trans('language.month_year') !!}</th>
                <th>{!! trans('language.epf_a') !!}</th>
                <th>{!! trans('language.pension_fund_b') !!}</th>
                <th>{!! trans('language.epf_ab') !!}</th>
                <th>{!! trans('language.pf_cut_off') !!}</th>
                <th>{!! trans('language.acc_no_02') !!}</th>
                <th>{!! trans('language.acc_no_21') !!}</th>
                <th>{!! trans('language.acc_no_22') !!}</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody> </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#pf_setting_table').DataTable({
      serverSide: true,
      searching: false, 
      paging: false, 
      info: false,
      ajax: {
          url:"{{ url('admin-panel/payroll/manage-pf-setting-view')}}"
          // data: function (f) {
          //     f.s_dept_name = $('#s_dept_name').val();
          // }
      },
      columns: [

        {data: 'month_year', name: 'month_year' },
        {data: 'epf_a', name: 'epf_a'},
        {data: 'pension_fund_b', name: 'pension_fund_b'},
        {data: 'epf_a_b', name: 'epf_a_b'},
        {data: 'pf_cuff_off', name: 'pf_cuff_off'},
        {data: 'acc_no_02', name: 'acc_no_02'},
        {data: 'acc_no_21', name: 'acc_no_21'},
        {data: 'acc_no_22', name: 'acc_no_22'},
        {data: 'action', name: 'action'}
    ],
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "13%"
            },
            {
                "targets": 1, // your case first column
                "width": "7%"
            },
            {
                "targets": 2, // your case first column
                "width": "13%"
            },
            {
                "targets": 3, // your case first column
                "width": "5%"
            },
            {
                "targets": 4, // your case first column
                "width": "5%"
            },
            {
                "targets": 5, // your case first column
                "width": "12%"
            },
            {
                "targets": 6, // your case first column
                "width": "12%"
            },
            {
                "targets": 7, // your case first column
                "width": "12%"
            },
            {
                "targets": 8, // your case first column
                "width": "20%"
            },
            {
                targets: [ 0, 1, 2, 3, 4, 5, 6, 7, 8],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });

    $('#clearBtn').click(function(){
        location.reload();
    });
  });
</script>
@endsection