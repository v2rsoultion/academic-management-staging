@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.subjectwise_mark') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    @if(Request::segment(2) == "student-report")
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student-report') !!}">{!! trans('language.menu_student_report') !!}</a></li>
                    @endif
                    @if(Request::segment(2) == "examination-report")
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination-report') !!}">{!! trans('language.menu_examination_report') !!}</a></li>
                    @endif
                    <li class="breadcrumb-item">{!! trans('language.subjectwise_mark') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('exam_id', $map['arr_exams'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $map['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'getSubject(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-2 col-md-2">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('subject_id',$map['arr_subject'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'subject_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search','disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}

                                <div id="show_table" style="display:none;">
                                <div class="table-responsive" style="margin-top:10px;">
                                    <table class="table m-b-0 c_list" id="map-table" style="width:100%">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.exam_roll_no')}}</th>
                                                <th>{!! trans('language.exam_student_name') !!}</th>
                                                <th>{!! trans('language.exam_total_mark') !!}</th>
                                                <th>{!! trans('language.exam_obtain_mark') !!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#map-table').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                'csv', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/examination-report/show-subjectwise-marks-report')}}',
                data: function (d) {
                    d.exam_id = $('select[name=exam_id]').val();
                    d.class_id = $('select[name=class_id]').val();
                    d.section_id = $('select[name=section_id]').val();
                    d.subject_id = $('select[name=subject_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_enroll_number', name: 'student_enroll_number' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'total_marks', name: 'total_marks'},
                {data: 'marks', name: 'marks'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            var class_id = $("#class_id").val();
            var exam_id = $("#exam_id").val();
            if(exam_id == "" ){
                $("#show_table").hide();
            } else {
                table.draw();
                $("#show_table").show();
            }
            e.preventDefault();
        });
    
        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function getSubject(section_id)
    {
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination-report/get-subject-data')}}",
                type: 'GET',
                data: {
                    'section_id': section_id
                },
                success: function (data) {
                    $("select[name='subject_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='subject_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function checkSearch(){
        var exam_id  = $("#exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        var subject_id = $("#subject_id").val();
        if(class_id != '' && section_id != '' && subject_id != '' && exam_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
    
</script>
@endsection