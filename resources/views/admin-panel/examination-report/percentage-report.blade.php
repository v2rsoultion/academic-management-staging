@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.percentage_report') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination-report') !!}">{!! trans('language.menu_examination_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.percentage_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" id="top-block">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <!-- <div class="header">
                        <h2><strong>Tabsheet</strong> </h2>
                    </div> -->
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('type', $manage['arr_type'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'type','onChange' => 'checkSearch(),showBlock(this.value)'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3" id="exam-block"  style="display: none">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('exam_id', $manage['arr_exams'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3" id="class-block" style="display: none">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('class_id', $manage['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="show_table" style="display:none;">
                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table m-b-0 c_list" id="map-table" style="width:100%">
                                {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>{{trans('language.exam_roll_no')}}</th>
                                        <th>{!! trans('language.exam_student_name') !!}</th>
                                        <th>{!! trans('language.exam_total_mark') !!}</th>
                                        <th>{!! trans('language.exam_obtain_mark') !!}</th>
                                        <th>{!! trans('language.exam_percentage') !!}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function () {
        var table = $('#map-table').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                'csv', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/examination-report/percentage-report-data')}}',
                data: function (d) {
                    d.type = $('select[name=type]').val();
                    d.exam_id = $('select[name=exam_id]').val();
                    d.class_id = $('select[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_enroll_number', name: 'student_enroll_number' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'total_marks', name: 'total_marks'},
                {data: 'obtain_marks', name: 'obtain_marks'},
                {data: 'percentage', name: 'percentage'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            var class_id = $("#class_id").val();
            var exam_id = $("#exam_id").val();
            if(exam_id == "" && class_id == ""){
                $("#show_table").hide();
            } else {
                table.draw();
                $("#show_table").show();
            }
            e.preventDefault();
        });
    
        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    function showBlock(type){
        if(type == 2){
            $("#exam-block").hide();
            $("#class-block").show();
            $("#exam_id").val('');
        } else if(type == 1){
            $("#exam-block").show();
            $("#class-block").show();
        } else {
            $("#exam-block").hide();
            $("#class-block").hide();
            $("#exam_id").val('');
        }
    }
    function checkSearch(){
        var exam_id = $("#exam_id").val();
        var term_id = $("#term_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(exam_id == "" && term_id == ''){
            $(':input[name="Search"]').prop('disabled', true);
            $("#marks-grid").css("display", "none");
        } else {
            
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }


</script>
@endsection