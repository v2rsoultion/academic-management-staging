@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.mst_view_report') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/academic') !!}">{!! trans('language.menu_academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/section-subject-mapping/view-subject-section-mapping') !!}">{!! trans('language.map_subject_to_section') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.mst_view_report') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                                <div class="body form-gap">
                                    <span class="classname_padding">Class Name : <strong>{{$class_name}}</strong></span>
                                    {!! Form::hidden('class_name',$class_name,array('readonly' => 'true','id'=>"mainclassname")) !!}
                                    {!! Form::hidden('section_id',$section_id,array('readonly' => 'true')) !!}
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="view-report-table" style="width:100%">
                                        {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.ms_subject_name')}}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var class_details = $('#mainclassname').val();
        
        var table = $('#view-report-table').DataTable({
            dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            buttons: [
                {
                    extend: 'csvHtml5',
                    messageTop: 'Class : ' + class_details,
                    customize: function(doc){
                        return "Class : \t" + class_details + "\n\n" + doc;
                        }
                },
                {
                extend: 'print',
                messageTop: 'Class : ' + class_details,
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                    $(win.document.body).find('h1').css('font-size', '20px');                        
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                    }
                }
            ],
            ajax: {
                url: "{{url('admin-panel/section-subject-mapping/view-report-data')}}",
                data: function (d) {
                    d.section_id = $('input[name=section_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'subjects', name: 'subjects'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "15%"
                },
                {
                "targets": 0,
                "orderable": false
                },
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

</script>
@endsection




