@if(isset($leave_application['student_leave_id']) && !empty($leave_application['student_leave_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('leave_application',old('student_leave_id',isset($leave_application['student_leave_id']) ? $leave_application['student_leave_id'] : ''),['class' => 'gui-input', 'id' => 'student_leave_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<!-- Basic Info section -->
<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.leave_app_date_to') !!} :</lable>
        <div class="form-group">
            {!! Form::date('staff_leave_from_date', old('staff_leave_from_date',isset($leave_application['staff_leave_from_date']) ? $leave_application['staff_leave_from_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.leave_app_date_to'), 'id' => 'staff_leave_from_date']) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.leave_app_date_from') !!} :</lable>
        <div class="form-group">
            {!! Form::date('staff_leave_to_date', old('staff_leave_to_date',isset($leave_application['staff_leave_to_date']) ? $leave_application['staff_leave_to_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.leave_app_date_from'), 'id' => 'staff_leave_to_date'] ) !!}
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.leave_app_attachment') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="documents" id="student_document_file" >
                <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
            </label>
        </div>
    </div>
    @if(!empty($leave_application['leave_scheme'])) 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.leave_scheme') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                {!!Form::select('pay_leave_scheme_id', $leave_application['leave_scheme'],isset($leave_application['pay_leave_scheme_id']) ? $leave_application['pay_leave_scheme_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'pay_leave_scheme_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.leave_app_reason') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_leave_reason',old('staff_leave_reason',isset($leave_application['staff_leave_reason']) ? $leave_application['staff_leave_reason']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.leave_app_reason'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/staff-leave-management') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("greaterThan",  function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) >= Number($(params).val())); 
        },'End Date must be greater than start date.');

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Only alphabetical characters");

        jQuery.validator.addMethod("extension", function(a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif|pdf", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only pdf,jpg,png format allowed."));

        $("#staff-leave-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_leave_reason: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                pay_leave_scheme_id: {
                    required: true,
                },
                documents:{
                    extension: true,
                },
                staff_leave_from_date: {
                    required: true,
                    date: true,
                    maxlength:10
                },
                staff_leave_to_date: {
                    required: true,
                    date: true,
                    maxlength:10,
                    greaterThan: "#staff_leave_from_date"
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    //error.insertAfter(element.parent());
                    element.closest('.form-group').after(error);
                }
            }
        });

        // $('#staff_leave_to_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false ,minDate : new Date()}).on('change', function(ev) {
        //     $(this).valid();  
        // });
        // $('#staff_leave_from_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false ,minDate : new Date() }).on('change', function(e, date)
        // {
        //     $('#staff_leave_to_date').bootstrapMaterialDatePicker('setMinDate', date);
        //     $('#staff_leave_to_date').val('');
        //     $(this).valid();  
        // });
    });

</script>