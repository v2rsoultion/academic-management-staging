@extends('admin-panel.layout.header')
@section('content')
<style>
.modal-dialog {
    max-width: 730px !important;
}
.theme-blush .btn-primary {
    margin-top: 25px !important;
}
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12">
                <h2>{!! trans('language.manage_leave') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-leave-management') !!}">{!! trans('language.menu_leave_management') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-leave-application/manage-leave-application') !!}">{!! trans('language.manage_leave_application') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.manage_leave') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <div class="alert alert-success" id="success_msg" style="display:none;">
                                </div>
                                <div class="alert alert-danger" id="error_msg" style="display:none;">
                                </div>
                                <div style="width:100%;border: 1px solid #ccc;border-radius: 5px;font-size:13px;padding: 5px 10px;margin-left: 0px;margin-bottom: 10px;">
                                    <h5>Leave Summary</h5>
                                    <div><b>Staff Name:</b> <span>{{$staff_leave['get_staff']['staff_name']}}</span> </div>
                                    <div><b>Leave days:</b> <span>{{$days}}</span> </div>
                                    @if(isset($staff_leave['get_leave_scheme']['lscheme_name']))
                                    <div><b>Leave Scheme:</b> <span>{{$staff_leave['get_leave_scheme']['lscheme_name']}}</span> </div>
                                    <div><b>Scheme Leaves Limit:</b> <span>{{$staff_leave['get_leave_scheme']['no_of_leaves']}}</span> </div>
                                    <div><b>Leave Period Type:</b> <span>{{$staff_leave['leave_period']}}</span> </div>
                                    <!-- <div><b>Total Leaves:</b> <span>{{$days}}</span> </div> -->
                                    <div><b>Consume Leaves:</b> <span>{{$staff_leave['consume_leave']}}</span> </div>
                                    @endif
                                    <input type="hidden" id="staff_id" name="staff_id" value="{{$staff_leave['staff_id']}}">
                                    <input type="hidden" id="staff_leave_id" name="staff_leave_id" value="{{$staff_leave['staff_leave_id']}}">
                                    <input type="hidden" id="no_of_leaves" name="no_of_leaves" value="{{$days}}">
                                    
                                </div>
                                <div class=row>
                                    <div class="col-lg-3">
                                        <lable class="from_one1">{!! trans('language.loss_of_pay') !!} :</lable>
                                        <div class="row col-lg-12 paddign-0">
                                            <div class="col-lg-3 padding-0">
                                                <div class="form-group"> 
                                                    @php
                                                    $check = "";
                                                    if($staff_leave['pay_leaves_status'] == 2)
                                                        $check = "checked";
                                                    @endphp  
                                                    <div class="checkbox">
                                                        <input id="checkbox" type="checkbox" name="" value="" onclick="showField()"  <?php echo $check; ?>>
                                                        <label class="from_one1" for="checkbox" style="margin-bottom: 4px !important;"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 padding-0">
                                                <div class="form-group">
                                                    <input type="number" class="form-control" value="{{$staff_leave['no_of_loss_pay_leave']}}"  id="no_of_loss_pay" name="no_of_loss_pay" disabled onkeyup="leaveValidate(this.value)" min='0'>
                                                    <div class="help-block" id="leave-error"> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!empty($staff_leave['get_leave_scheme']))
                                    <div class="col-lg-3">
                                        <lable class="from_one1">{!! trans('language.leave_scheme') !!}</lable>
                                        <div class="form-group m-bottom-0">
                                            <label class="form-group field select" style="width: 100%">
                                            {!!Form::select('leave_scheme', $staff_leave['leave_scheme'], isset($staff_leave['pay_leave_scheme_id']) ? $staff_leave['pay_leave_scheme_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'leave_scheme'])!!}
                                                
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-lg-3">
                                        <lable class="from_one1">{!! trans('language.action') !!}</lable>
                                        <div class="form-group m-bottom-0">
                                            <label class="form-group field select" style="width: 100%">
                                            {!!Form::select('leave_status', $leave_status, isset($staff_leave['staff_leave_status']) ? $staff_leave['staff_leave_status'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'leave_status'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 padding-0">
                                        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary saveBtn','name'=>'save', 'onclick' => 'saveModelData()']) !!}
                                        <a href="{!! url('admin-panel/staff-leave-application/manage-leave-application') !!}" class="btn btn-round btn-primary saveBtn" >Cancel</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="staff-leave-table" width="100%">
                                    <thead>
                                        <tr>
                                        <th>{!! trans('language.s_no') !!}</th> 
                                        <th>{!! trans('language.leave_date_range') !!}</th>
                                        <th>{!! trans('language.leave_days') !!}</th>
                                        <th>{{trans('language.leave_app_status')}}</th>
                                        <th>{!! trans('language.lscheme_name') !!}</th>
                                        <th>{!! trans('language.no_of_leaves') !!}</th>
                                        <th>{!! trans('language.loss_of_pay') !!}<th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function() {
        window.onload=showField;
    });
    $(document).ready(function () {
        var table = $('#staff-leave-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/staff-leave-application/staff_leave-history-data')}}',
                data: function (d) {
                    d.staff_id = $('input[name=staff_id]').val();
                    d.staff_leave_id = $('input[name=staff_leave_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'date_range', name: 'date_range'},
                {data: 'leave_days', name: 'leave_days'},
                {data: 'staff_leave_status', name: 'staff_leave_status'},
                {data: 'lscheme_name', name: 'lscheme_name'},
                {data: 'no_of_leaves', name: 'no_of_leaves'},
                {data: 'no_of_loss_pay_leave', name: 'no_of_loss_pay_leave'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "18%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });

    function showField() {
        if($('#checkbox').prop("checked") == true){
            $('#no_of_loss_pay').prop('disabled', false);
        }
        else if($('#checkbox').prop("checked") == false){
            $('#no_of_loss_pay').prop('disabled', true);
            $('#no_of_loss_pay').val(0);
        }
    }
    function saveModelData() {
        var staff_leave_id = $("#staff_leave_id").val();
        console.log(staff_leave_id);
        var staff_leave_status = $("#leave_status").val();
        console.log(staff_leave_status);
        if($('#checkbox').prop("checked") == true){
            pay_leave_status = 2;
        }
        else if($('#checkbox').prop("checked") == false){
            pay_leave_status = 1;
        }
        var no_of_loss_pay = $("#no_of_loss_pay").val();
        console.log(no_of_loss_pay);
        var leave_scheme = $("#leave_scheme").val();
        console.log(leave_scheme);
        $(".mycustloading").show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/staff-leave-application/get-model-data')}}",
            type: 'GET',
            data: {
                'staff_leave_id': staff_leave_id,
                'staff_leave_status': staff_leave_status,
                'pay_leave_status': pay_leave_status,
                'no_of_loss_pay': no_of_loss_pay,
                'leave_scheme': leave_scheme
            },
            success: function (data) {
                if(data == "Success") {
                    $("#success_msg").show();
                    $("#error_msg").hide();
                    $("#success_msg").html('Leave Application status updated!');
                    $(".mycustloading").hide();
                } else {
                    $("#error_msg").show();
                    $("#success_msg").hide();
                    $("#error_msg").html('Something goes wrong!');
                    $(".mycustloading").hide();
                }
                
            }
        });
    }

    function leaveValidate() {
        var no_of_loss_pay = document.getElementById('no_of_loss_pay').value;
        var no_of_leaves = document.getElementById('no_of_leaves').value;
            if(parseInt(no_of_loss_pay) <= parseInt(no_of_leaves)) {
                document.getElementById("leave-error").innerHTML = '';
            } else {
                $errors = 'less than leave days!!';
                document.getElementById("leave-error").innerHTML = $errors;
            }
    }
</script>
@endsection




