@extends('admin-panel.layout.header')

@section('content')
<div class="chat-launcher"></div>
<div class="chat-wrapper">
    <div class="card">
        <div class="header">
            <ul class="list-unstyled team-info margin-0">
                <li class="m-r-15"><h2>Pro. Team</h2></li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar2.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar3.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar4.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar6.jpg') !!}" alt="Avatar">
                </li>
                <li>
                    <a href="javascript:void(0);" title="Add Member"><i class="zmdi zmdi-plus-circle"></i></a>
                </li>
            </ul>                       
        </div>
        <div class="body form-gap">
            <div class="chat-widget">
            <ul class="chat-scroll-list clearfix">
                <li class="left float-left">
                    <img src="{!! URL::to('public/admin/assets/images/xs/avatar3.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info">
                        <a class="name" href="javascript:void(0);">Alexander</a>
                        <span class="datetime">6:12</span>                            
                        <span class="message">Hello, John </span>
                    </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:15</span> <span class="message">Hi, Alexander<br> How are you!</span> </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:16</span> <span class="message">There are many variations of passages of Lorem Ipsum available</span> </div>
                </li>
                <li class="left float-left"> <img src="{!! URL::to('public/admin/assets/images/xs/avatar2.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="javascript:void(0);">Elizabeth</a> <span class="datetime">6:25</span> <span class="message">Hi, Alexander,<br> John <br> What are you doing?</span> </div>
                </li>
                <li class="left float-left"> <img src="{!! URL::to('public/admin/assets/images/xs/avatar1.jpg') !!}" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="javascript:void(0);">Michael</a> <span class="datetime">6:28</span> <span class="message">I would love to join the team.</span> </div>
                </li>
                    <li class="right">
                    <div class="chat-info"><span class="datetime">7:02</span> <span class="message">Hello, <br>Michael</span> </div>
                </li>
            </ul>
            </div>
            <div class="input-group p-t-15">
                <input type="text" class="form-control" placeholder="Enter text here...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-mail-send"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Dashboard
                <small>Welcome to {!!trans('language.project_title') !!}</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="row clearfix">
                
                @if($login_info['admin_type'] == 2)
                @php $pcounter= 0; @endphp
                @if(in_array(1, $login_info['staff_role_id']))
                @php $pcounter = $pcounter + 1; @endphp
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body form-gap">
                            <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                            <div class="content">
                                <div class="text">Present Student</div>
                                <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['student']}}</h5>
                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body form-gap">
                            <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                            <div class="content">
                                <div class="text">Parent Login No</div>
                                <h5 class="number count-to" data-from="0" data-to="{{$dashboard['parent_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['parent_login']}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body form-gap">
                            <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                            <div class="content">
                                <div class="text">Student Login No</div>
                                <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['student_login']}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body form-gap">
                            <div class="icon xl-slategray"><i class="zmdi zmdi-account-circle"></i> </div>
                            <div class="content">
                                <div class="text">Present Staff</div>
                                <h5 class="number count-to" data-from="0" data-to="{{$dashboard['staff']}}" data-speed="4000" data-fresh-interval="700">{{$dashboard['staff']}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body form-gap">
                            <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                            <div class="content">
                                <div class="text">Total Admission</div>
                                <h5 class="number count-to" data-from="0" data-to="{{$dashboard['total_admission']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_admission']}}</h5>
                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter">
                        <div class="body form-gap">
                            <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                            <div class="content">
                                <div class="text">Staff Login No</div>
                                <h5 class="number count-to" data-from="0" data-to="{{$dashboard['staff_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['staff_login']}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(in_array(1, $login_info['staff_role_id']))   
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance-wallet"></i> </div>
                                <div class="content">
                                    <div class="text">Expenses</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$dashboard['total_expense']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_expense']}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Collected</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$fees_collected}}" data-speed="2500" data-fresh-interval="700">{{$fees_collected}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div> 
                @endif
                    @if(in_array(2,$login_info['staff_role_id']) && $pcounter == 0)
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Present Student</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['student']}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Parent Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['parent_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['parent_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Student Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['student_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Total Admission</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['total_admission']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_admission']}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div> -->
                    @endif
                    @if(in_array(7,$login_info['staff_role_id']) )
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-home"></i> </div>
                                <div class="content">
                                    <div class="text">Hostel</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$hostel}}" data-speed="2500" data-fresh-interval="700">{{$hostel}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-home"></i> </div>
                                <div class="content">
                                    <div class="text">Hostel Capacity</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$h_room_capacity}}" data-speed="2500" data-fresh-interval="700">{{$h_room_capacity}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-hotel"></i> </div>
                                <div class="content">
                                    <div class="text">Occupied Rooms</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$occupied_rooms}}" data-speed="2500" data-fresh-interval="700">{{$occupied_rooms}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Collected</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$h_fees_collected}}" data-speed="2500" data-fresh-interval="700">{{$h_fees_collected}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance-wallet"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Due</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="1000" data-speed="2500" data-fresh-interval="700">1000</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(in_array(9,$login_info['staff_role_id']) && $pcounter == 0)
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Collected</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$fees_collected}}" data-speed="2500" data-fresh-interval="700">{{$fees_collected}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(in_array(9,$login_info['staff_role_id']))
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Total Fees</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$dashboard['total_fees']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_fees']}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Pending</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$fees_pending_total}}" data-speed="2500" data-fresh-interval="700">{{$fees_pending_total}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Concession Applied</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$total_concession_applied}}" data-speed="2500" data-fresh-interval="700">{{$total_concession_applied}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-balance"></i> </div>
                                <div class="content">
                                    <div class="text">Fees Collected Today</div>
                                    <h5 class="m-b-0">Rs. <span class="number count-to" data-from="0" data-to="{{$fees_collected_today}}" data-speed="2500" data-fresh-interval="700">{{$fees_collected_today}}</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    @endif
                    @if(in_array(10,$login_info['staff_role_id']) && $pcounter == 0)
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Present Student</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['student']}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-circle"></i> </div>
                                <div class="content">
                                    <div class="text">Present Staff</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['staff']}}" data-speed="4000" data-fresh-interval="700">{{$dashboard['staff']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Total Admission</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['total_admission']}}" data-speed="2500" data-fresh-interval="700">{{$dashboard['total_admission']}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Parent Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['parent_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['parent_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Student Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['student_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['student_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Staff Login No</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$dashboard['staff_login']}}" data-speed="3000" data-fresh-interval="700">{{$dashboard['staff_login']}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(in_array(12,$login_info['staff_role_id']))
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Books Issued</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$book_issued}}" data-speed="2500" data-fresh-interval="700">{{$book_issued}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-circle"></i> </div>
                                <div class="content">
                                    <div class="text">Books Issued By Student </div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$book_issued_student}}" data-speed="4000" data-fresh-interval="700">{{$book_issued_student}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-account-o"></i> </div>
                                <div class="content">
                                    <div class="text">Books Issued By Staff</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$book_issued_staff}}" data-speed="2500" data-fresh-interval="700">{{$book_issued_staff}}</h5>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Renew date expire today</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$renew_date_expire_today}}" data-speed="3000" data-fresh-interval="700">{{$renew_date_expire_today}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body form-gap">
                                <div class="icon xl-slategray"><i class="zmdi zmdi-label"></i> </div>
                                <div class="content">
                                    <div class="text">Books Renew date expired</div>
                                    <h5 class="number count-to" data-from="0" data-to="{{$renew_date_expired}}" data-speed="3000" data-fresh-interval="700">{{$renew_date_expired}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif
                </div>
                @if($login_info['admin_type'] == 2)
                <div class="row col-lg-12 padding-0" style="margin:0px;">
                @php $chartcounter = 0; @endphp
                    @if(in_array(1,$login_info['staff_role_id']) )
                    @php $chartcounter = $chartcounter + 1; @endphp
                        <div class="col-lg-6" style="padding-left:0px">
                            <div id="feesLineChart" style="height: 300px; width: 100%;"></div>
                        </div>
                        <div class="col-lg-6" style="padding-right:0px;margin-bottom:20px;">
                            <div id="classFeesPieChart" style="height: 300px; width: 100%;"></div>
                        </div>
                    @endif
                    @if(in_array(2,$login_info['staff_role_id']) )
                        <div class="col-lg-6" style="padding-left:0px;margin-bottom:20px;">
                            <div id="attendanceChart" style="height: 300px; width: 100%;"></div>
                        </div>
                    @endif 
                    @php $piechartcounter = 0; @endphp
                    @if(in_array(10,$login_info['staff_role_id']) && $chartcounter == 0)
                    @php $piechartcounter = $piechartcounter + 1; @endphp
                    <div class="col-lg-6" style="padding-left:0px;margin-bottom:20px;">
                        <div id="classFeesPieChart" style="height: 300px; width: 100%;"></div>
                    </div>
                    @endif
                    @if(in_array(9,$login_info['staff_role_id']) && $chartcounter == 0)
                    <div class="col-lg-6" style="padding-left:0px">
                        <div id="feesLineChart" style="height: 300px; width: 100%;"></div>
                    </div>
                    @endif
                    @if(in_array(9,$login_info['staff_role_id']) && $chartcounter == 0 && $piechartcounter == 0)
                    <div class="col-lg-6" style="padding-right:0px;margin-bottom:20px;">
                        <div id="classFeesPieChart" style="height: 300px; width: 100%;"></div>
                    </div>
                    @endif
                </div>
                @endif
                @if($login_info['admin_type'] == 2)
                <div class="col-lg-12 padding-0">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Schedule List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="notice-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($plan_schedule as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['getStaff']['staff_name']}}</span></td>
                                            <td><div class="comment more">{{$record['plan_description']}}</div></td>
                                            <td>{{$record['plan_date']}}</td>
                                            <td>{{$record['plan_time']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Notice List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="notice-table">
                                    <thead>
                                        <tr>
                                            <th>Notice Name</th>
                                            <th>Notice Text</th>
                                            <th>For Class</th>
                                            <th>For Staff</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($notice_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['notice_name']}}</span></td>
                                            <td><div class="comment more">{{$record['notice_text']}}</div></td>
                                            @if($record->for_class == 0)
                                            <td>For All Class</td>
                                            @else
                                            <td>{{get_selected_classes($record->class_ids)}}</td>
                                            @endif
                                            @if($record->for_staff == 0)
                                            <td>For All Staff</td>
                                            @else
                                            <td>{{get_selected_staff($record->staff_ids)}}</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @php $plistcounter= 0; @endphp
                @if(in_array(1, $login_info['staff_role_id']))
                @php $plistcounter = $plistcounter + 1; @endphp
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Visitor List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="visitor-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Purpose</th>
                                            <th>Contact No</th>
                                            <th>Visit Date</th>
                                            <th>Timing</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($visitor_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['name']}}</span></td>
                                            <td>{{$record['purpose']}}</td>
                                            <td>{{$record['contact']}}</td>
                                            <td>{{$record['visit_date']}}</td>
                                            <td><span class="badge badge-primary">{{$record['check_in_time']. '-' .$record['check_out_time']}}</span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Admission List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="admission-table">
                                    <thead>
                                        <tr>
                                            <th>Enroll No</th>
                                            <th>Name</th>
                                            <th>Class</th>
                                            <th>Father Name</th>
                                            <th>Contact No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($admission_list as $record)
                                        <tr> 
                                            <td><span class="list-name">{{$record['student_roll_no']}}</span></td>
                                            <td>{{$record['student_name']}}</td>
                                            <td><span class="badge badge-primary">{{$record['current_class_id']}}</span></td>
                                            <td>{{$record['student_father_name']}}</td>
                                            <td>{{$record['student_father_mobile_numbera']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Enquiry List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="enquiry-table">
                                    <thead>
                                        <tr>
                                            <th>Enroll No</th>
                                            <th>Name</th>
                                            <th>Class</th>
                                            <th>Father Name</th>
                                            <th>Contact No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($enquiry_list as $record)
                                        <tr> 
                                            <td><span class="list-name">{{$record['student_roll_no']}}</span></td>
                                            <td>{{$record['student_name']}}</td>
                                            <td><span class="badge badge-primary">{{$record['current_class_id']}}</span></td>
                                            <td>{{$record['student_father_name']}}</td>
                                            <td>{{$record['student_father_mobile_numbera']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                
                @if(in_array(7,$login_info['staff_role_id']) )
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Occupied Rooms List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="occupied-rooms-table">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Hostel-Block</th>
                                            <th>Floor-Room No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($student_allocate_room as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['getStudent']['student_name']}}</span></td>
                                            <td>{{$record['getHostel']['hostel_name']. ' - ' . $record['getBlock']['h_block_name']}}</td>
                                            <td>{{$record['getFloor']['h_floor_no']. ' - ' . $record['getRoom']['h_room_no']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Leave Rooms List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="leave-rooms-table">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Hostel-Block</th>
                                            <th>Floor-Room No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($student_leave_room as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['getStudent']['student_name']}}</span></td>
                                            <td>{{$record['getHostel']['hostel_name']. ' - ' . $record['getBlock']['h_block_name']}}</td>
                                            <td>{{$record['getFloor']['h_floor_no']. ' - ' . $record['getRoom']['h_room_no']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Fees Pending List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="fees-pending-table">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Hostel-Block</th>
                                            <th>Floor-Room No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($h_student_fees_pending as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['getStudent']['student_name']}}</span></td>
                                            <td>{{$record['getHostel']['hostel_name']. ' - ' . $record['getBlock']['h_block_name']}}</td>
                                            <td>{{$record['getFloor']['h_floor_no']. ' - ' . $record['getRoom']['h_room_no']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(in_array(9,$login_info['staff_role_id']) )
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Fees Paid Today List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="fees-paid-table">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Class</th>
                                            <th>Receipt Number</th>
                                            <th>Amount</th>
                                            <th>Imprest Amount</th>
                                            <th>Payment Mode</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($fees_paid_today_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['getStudent']['student_name']}}</span></td>
                                            <td>{{$record['getClass']['class_name']}}</td>
                                            <td>{{$record['receipt_number']}}</td>
                                            <td>{{$record['net_amount']}}</td>
                                            <td>{{$record['imprest_amount']}}</td>
                                            <td>{{$record['pay_mode']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Fees Due Student List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="fees-due-student-table">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Class</th>
                                            <th>Receipt Number</th>
                                            <th>Amount</th>
                                            <th>Imprest Amount</th>
                                            <th>Payment Mode</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($fees_due_student_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['getStudent']['student_name']}}</span></td>
                                            <td>{{$record['getClass']['class_name']}}</td>
                                            <td>{{$record['receipt_number']}}</td>
                                            <td>{{$record['net_amount']}}</td>
                                            <td>{{$record['imprest_amount']}}</td>
                                            <td>{{$record['pay_mode']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(in_array(10,$login_info['staff_role_id']) && $plistcounter == 0)
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Visitor List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="visitor-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Purpose</th>
                                            <th>Contact No</th>
                                            <th>Visit Date</th>
                                            <th>Timing</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($visitor_list as $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['name']}}</span></td>
                                            <td>{{$record['purpose']}}</td>
                                            <td>{{$record['contact']}}</td>
                                            <td>{{$record['visit_date']}}</td>
                                            <td><span class="badge badge-primary">{{$record['check_in_time']. '-' .$record['check_out_time']}}</span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Admission List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="admission-table">
                                    <thead>
                                        <tr>
                                            <th>Enroll No</th>
                                            <th>Name</th>
                                            <th>Class</th>
                                            <th>Father Name</th>
                                            <th>Contact No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($admission_list as $record)
                                        <tr> 
                                            <td><span class="list-name">{{$record['student_roll_no']}}</span></td>
                                            <td>{{$record['student_name']}}</td>
                                            <td><span class="badge badge-primary">{{$record['current_class_id']}}</span></td>
                                            <td>{{$record['student_father_name']}}</td>
                                            <td>{{$record['student_father_mobile_numbera']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Enquiry List <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="enquiry-table">
                                    <thead>
                                        <tr>
                                            <th>Enroll No</th>
                                            <th>Name</th>
                                            <th>Class</th>
                                            <th>Father Name</th>
                                            <th>Contact No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($enquiry_list as $record)
                                        <tr> 
                                            <td><span class="list-name">{{$record['student_roll_no']}}</span></td>
                                            <td>{{$record['student_name']}}</td>
                                            <td><span class="badge badge-primary">{{$record['current_class_id']}}</span></td>
                                            <td>{{$record['student_father_name']}}</td>
                                            <td>{{$record['student_father_mobile_numbera']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(in_array(12,$login_info['staff_role_id']) )
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Books Expired <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="book-expired-table">
                                    <thead>
                                        <tr>
                                            <th>Book Name</th>
                                            <th>Book Unique Id</th>
                                            <th>Book Category</th>
                                            <th>ISBN No</th>
                                            <th>Author Name</th>
                                            <th>Member Type</th>
                                            <th>Member Name</th>
                                            <th>Expired date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($arr_book_expired_list as $key =>  $record)
                                        <tr>
                                            <td><span class="list-name">{{$record['book_name']}}</span></td>
                                            <td>{{$record['book_unique_id']}}</td>
                                            <td>{{$record['book_category']}}</td>
                                            <td>{{$record['book_isbn_no']}}</td>
                                            <td>{{$record['author_name']}}</td> 
                                            <td>{{$record['member_type']}}</td> 
                                            <td>{{$record['member_name']}}</td>
                                            <td>{{$record['expire_date']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padding-0" style="margin-top:20px;">
                    <div class="card student-list">
                        <div class="header">
                            <h2><strong>New</strong> Books Issued Today <small>Description text here...</small></h2>
                        </div> 
                        <div class="body form-gap">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0" id="book-issued-table">
                                    <thead>
                                        <tr>
                                            <th>Book Name</th>
                                            <th>Book Unique Id</th>
                                            <th>Book Category</th>
                                            <th>ISBN No</th>
                                            <th>Author Name</th>
                                            <th>Member Type</th>
                                            <th>Member Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($arr_book_issued_list as $record)
                                        <tr>
                                        <td><span class="list-name">{{$record['book_name']}}</span></td>
                                            <td>{{$record['book_unique_id']}}</td>
                                            <td>{{$record['book_category']}}</td>
                                            <td>{{$record['book_isbn_no']}}</td>
                                            <td>{{$record['author_name']}}</td> 
                                            <td>{{$record['member_type']}}</td> 
                                            <td>{{$record['member_name']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endif
            </div>
            
        </div>        
    </div>
</section>
<script>
window.onload = function () {
    <?php if(in_array(1,$login_info['staff_role_id']) ) { ?>
    var abc = [];
    var options1 = {
            title: {
                text: "Class Wise Fees Graph",
                padding:20
            },
            data: [{
                    type: "pie",
                    startAngle: 45,
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabel: "{label} ({y})",
                    yValueFormatString:"#,##0.#"%"",
                    dataPoints: [
                        { label: "Class-I", y: 36 },
                        { label: "Class-II", y: 31 },
                        { label: "Class-III", y: 7 },
                        { label: "Class-IV", y: 7 },
                        { label: "Class-V", y: 6 },
                        { label: "Class-VI", y: 10 },
                        { label: "Class-VII", y: 3 }
                    ]
            }]
        };
$("#classFeesPieChart").CanvasJSChart(options1);

    var options = {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Fees Graph",
        padding:20
	},
	axisX:{
		valueFormatString: "MMM"
	},
	axisY: {
		title: "Number of Student",
		suffix: "K",
		minimum: 30
	},
	toolTip:{
		shared:true
	},  
	legend:{
		cursor:"pointer",
		verticalAlign: "bottom",
		horizontalAlign: "left",
		dockInsidePlotArea: true,
		itemclick: toogleDataSeries
	},
	data: [{
		type: "line",
		showInLegend: true,
		name: "Month Fees",
		markerType: "square",
		xValueFormatString: "MMM, YYYY",
		color: "#F08080",
		yValueFormatString: "#,##0K",
		dataPoints: [
			{ x: new Date(2017, 01, 1), y: 63 },
			{ x: new Date(2017, 02, 1), y: 69 },
			{ x: new Date(2017, 03, 1), y: 65 },
			{ x: new Date(2017, 04, 1), y: 70 },
			{ x: new Date(2017, 05, 1), y: 71 },
			{ x: new Date(2017, 06, 1), y: 65 },
			{ x: new Date(2017, 07, 1), y: 73 },
			{ x: new Date(2017, 08, 1), y: 96 },
			{ x: new Date(2017, 09, 1), y: 84 },
			{ x: new Date(2017, 10, 1), y: 85 },
			{ x: new Date(2017, 11, 1), y: 86 },
			{ x: new Date(2017, 12, 1), y: 94 }
		]
	}]
};
$("#feesLineChart").CanvasJSChart(options);

function toogleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else{
		e.dataSeries.visible = true;
	}
	e.chart.render();
}
<?php } ?>
<?php if(in_array(10,$login_info['staff_role_id']) ) { ?>
    var options1 = {
            title: {
                text: "Class Wise Fees Graph",
                padding:20
            },
            data: [{
                    type: "pie",
                    startAngle: 45,
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabel: "{label} ({y})",
                    yValueFormatString:"#,##0.#"%"",
                    dataPoints: [
                        { label: "Class-I", y: 36 },
                        { label: "Class-II", y: 31 },
                        { label: "Class-III", y: 7 },
                        { label: "Class-IV", y: 7 },
                        { label: "Class-V", y: 6 },
                        { label: "Class-VI", y: 10 },
                        { label: "Class-VII", y: 3 }
                    ]
            }]
        };
$("#classFeesPieChart").CanvasJSChart(options1);
<?php } ?>
<?php if(in_array(9,$login_info['staff_role_id']) ) { ?>
    var options = {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Fees Graph",
        padding:20
	},
	axisX:{
		valueFormatString: "MMM"
	},
	axisY: {
		title: "Number of Student",
		suffix: "K",
		minimum: 30
	},
	toolTip:{
		shared:true
	},  
	legend:{
		cursor:"pointer",
		verticalAlign: "bottom",
		horizontalAlign: "left",
		dockInsidePlotArea: true,
		itemclick: toogleDataSeries
	},
	data: [{
		type: "line",
		showInLegend: true,
		name: "Month Fees",
		markerType: "square",
		xValueFormatString: "MMM, YYYY",
		color: "#F08080",
		yValueFormatString: "#,##0K",
		dataPoints: [
			{ x: new Date(2017, 01, 1), y: 63 },
			{ x: new Date(2017, 02, 1), y: 69 },
			{ x: new Date(2017, 03, 1), y: 65 },
			{ x: new Date(2017, 04, 1), y: 70 },
			{ x: new Date(2017, 05, 1), y: 71 },
			{ x: new Date(2017, 06, 1), y: 65 },
			{ x: new Date(2017, 07, 1), y: 73 },
			{ x: new Date(2017, 08, 1), y: 96 },
			{ x: new Date(2017, 09, 1), y: 84 },
			{ x: new Date(2017, 10, 1), y: 85 },
			{ x: new Date(2017, 11, 1), y: 86 },
			{ x: new Date(2017, 12, 1), y: 94 }
		]
	}]
};
$("#feesLineChart").CanvasJSChart(options);

function toogleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else{
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

var options1 = {
	title: {
		text: "Class Wise Fees Graph",
        padding:20
	},
	data: [{
			type: "pie",
			startAngle: 45,
			showInLegend: "true",
			legendText: "{label}",
			indexLabel: "{label} ({y})",
			yValueFormatString:"#,##0.#"%"",
			dataPoints: [
				{ label: "Class-I", y: 36 },
				{ label: "Class-II", y: 31 },
				{ label: "Class-III", y: 7 },
				{ label: "Class-IV", y: 7 },
				{ label: "Class-V", y: 6 },
				{ label: "Class-VI", y: 10 },
				{ label: "Class-VII", y: 3 }
			]
	}]
};
$("#classFeesPieChart").CanvasJSChart(options1);    
<?php } ?>

<?php if($login_info['admin_type'] == 2) {
         if(in_array(2,$login_info['staff_role_id']) ) { 
 ?>
        var options = {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Attendance Graph",
                padding:20
            },
            axisX:{
                margin:10,
                valueFormatString: "DD MMM"
            },
            axisY: {
                margin:10,
                title: "Number of Student",
                suffix: "K",
                minimum: 30
            },
            toolTip:{
                shared:true
            },  
            legend:{
                cursor:"pointer",
                verticalAlign: "bottom",
                horizontalAlign: "left",
                dockInsidePlotArea: true,
                itemclick: toogleDataSeries
            },
            data: [{
                type: "line",
                showInLegend: true,
                name: "Day Wise",
                markerType: "square",
                xValueFormatString: "DD MMM, YYYY",
                color: "#F08080",
                yValueFormatString: "#,##0K",
                dataPoints: [
                    { x: new Date(2017, 01, 1), y: 63 },
                    { x: new Date(2017, 01, 2), y: 69 },
                    { x: new Date(2017, 01, 3), y: 65 },
                    { x: new Date(2017, 01, 4), y: 70 },
                    { x: new Date(2017, 01, 5), y: 71 },
                    { x: new Date(2017, 01, 6), y: 65 },
                    { x: new Date(2017, 01, 7), y: 73 },
                    { x: new Date(2017, 01, 8), y: 96 },
                    { x: new Date(2017, 01, 9), y: 84 },
                    { x: new Date(2017, 01, 10), y: 85 },
                    { x: new Date(2017, 01, 11), y: 86 },
                    { x: new Date(2017, 01, 12), y: 94 }
                ]
            }]
        };
        $("#attendanceChart").CanvasJSChart(options);

        function toogleDataSeries(e){
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else{
                e.dataSeries.visible = true;
            }
            e.chart.render();
        }
<?php } }
 ?>

}

$(document).ready(function() {
	var showChar = 100;
	var ellipsestext = "...";
	var moretext = "Read more";
	var lesstext = "Read less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		} 
	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});

$(document).ready(function () {
    var table = $('#notice-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#visitor-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: false,
        info: false,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#enquiry-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#admission-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "10%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#occupied-rooms-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#leave-rooms-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#fees-pending-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "35%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#fees-paid-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "15%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#fees-due-student-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "12%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#book-expired-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "12%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
    var table = $('#book-issued-table').DataTable({
        processing: true,
        searching: false,
        bLengthChange: false,
        bPaginate: true,
        info: false,
        "pageLength": 5,
        columnDefs: [
            {
                "targets": 0, // your case first column
                "width": "15%"
            },
            {
                "targets": 1, // your case first column
                "width": "12%"
            },
            {
                targets: [ 0, 1],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    });
});
</script>
@endsection

