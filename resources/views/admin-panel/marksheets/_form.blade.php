@if(isset($certificate['certificate_id']) && !empty($certificate['certificate_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('certificate_id',old('certificate_id',isset($certificate['certificate_id']) ? $certificate['certificate_id'] : ''),['class' => 'gui-input', 'id' => 'certificate_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.template_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('template_name', old('template_name',isset($certificate['template_name']) ? $certificate['template_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.template_name'), 'id' => 'template_name','disabled']) !!}
        </div>
    </div>
    @if($certificate['template_type'] == 2)
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.template_for') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('template_for', $certificate['arr_template_for'],old('template_for',isset($certificate['template_for']) ? $certificate['template_for'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'template_for','disabled'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    @endif
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.template_link') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('template_link', $certificate['arr_template'],old('template_link',isset($certificate['template_link']) ? $certificate['template_link'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'template_link','disabled'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>
<div class="row clearfix">   
    @if($certificate['template_type'] == 0 || $certificate['template_type'] == 1)
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.book_no_start') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('book_no_start', old('book_no_start',isset($certificate['book_no_start']) ? $certificate['book_no_start']: ''), ['class' => 'form-control fee','placeholder'=>trans('language.book_no_start'), 'id' => 'book_no_start' , 'min'=> 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.book_no_end') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('book_no_end', old('book_no_end',isset($certificate['book_no_end']) ? $certificate['book_no_end']: ''), ['class' => 'form-control fee','placeholder'=>trans('language.book_no_end'), 'id' => 'book_no_end', 'min'=> 0]) !!}
        </div>
    </div>
    @endif

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.serial_no_start') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('serial_no_start', old('serial_no_start',isset($certificate['serial_no_start']) ? $certificate['serial_no_start']: ''), ['class' => 'form-control fee','placeholder'=>trans('language.serial_no_start'), 'id' => 'serial_no_start' , 'min'=> 0]) !!}
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.serial_no_end') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('serial_no_end', old('serial_no_end',isset($certificate['serial_no_end']) ? $certificate['serial_no_end']: ''), ['class' => 'form-control fee','placeholder'=>trans('language.serial_no_end'), 'id' => 'serial_no_end', 'min'=> 0]) !!}
        </div>
    </div>
    
    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/admission') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) >= parseFloat($otherElement.val(), 10);
        },"The value must be greater");
        
        $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $otherElement = $(param);
            return parseFloat(value, 10) <= parseFloat($otherElement.val(), 10);
        },"The value  must be less");

        $("#certificate-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                template_for: {
                    required: true
                },
                template_name: {
                    required: true,
                    lettersonly:true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                book_no_start: {
                    required: true,
                    number: true,
                    lessThan: '#book_no_end'
                },
                book_no_end: {
                    required: true,
                    number: true,
                    greaterThan: '#book_no_start'
                },
                serial_no_start: {
                    required: true,
                    number: true,
                    lessThan: '#serial_no_end'
                },
                serial_no_end: {
                    required: true,
                    number: true,
                    greaterThan: '#serial_no_start'
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
             highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
        
    });

    

</script>