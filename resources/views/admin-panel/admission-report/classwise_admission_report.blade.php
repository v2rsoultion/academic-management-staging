@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.classwise_admission') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/admission-report') !!}">{!! trans('language.menu_admission_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.classwise_admission') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search','disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}

                                <div class="table-responsive" style="margin-top:10px;">
                                    <table class="table m-b-0 c_list" id="admission-report" style="width:100%">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.class')}}</th>
                                                <th>{!! trans('language.form_name') !!}</th>
                                                <th>{!! trans('language.mediums') !!}</th>
                                                <th>{!! trans('language.no_of_intake') !!}</th>
                                                <th>{!! trans('language.no_of_applicant') !!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        $("#admission-report").append(
            $('<tfoot/>').append( $("#admission-report thead tr").clone() )
        );
        var table = $('#admission-report').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                { extend: 'csv', footer: true },
                { extend: 'print', footer: true }
            ],
            ajax: {
                url: '{{url('admin-panel/admission-report/show-classwise-admission-report')}}',
                data: function (d) {
                    d.class_id = $('select[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'class_name', name: 'class_name' },
                {data: 'form_name', name: 'form_name'},
                {data: 'mediums', name: 'mediums'},
                {data: 'no_of_intake', name: 'no_of_intake'},
                {data: 'no_of_applicant', name: 'no_of_applicant'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ],
            drawCallback: function (row, data, start, end, display) {
                var api = this.api();
                var no_of_intake = api.column( 4, {page:'current'} ).data().sum();
                var no_of_applicant = api.column( 5, {page:'current'} ).data().sum(); 
                var cal_percent = (no_of_applicant*100)/no_of_intake;
                $(api.column(0).footer()).html('');
                $(api.column(1).footer()).html('');
                $(api.column(2).footer()).html('');
                $(api.column(3).footer()).html('');
                $( api.column(4).footer() ).html(
                    round(api.column( 4, {page:'current'} ).data().sum(),2)
                );
                $( api.column(5).footer() ).html(
                    round(api.column( 5, {page:'current'} ).data().sum(),2)+' ('+cal_percent+'%)'
                );
            },
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        });
    });


    function checkSearch(){
        var class_id = $("#class_id").val();
        if(class_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
    
    function round(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }
</script>
@endsection