@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" id="top-block">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Manage</strong> Report Card  </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <!-- <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('type', $manage['arr_type'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'type','onChange' => 'checkSearch(),showBlock(this.value)'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3" id="term-block" style="display: none">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('term_id', $manage['arr_terms'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'term_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div> -->
                            <div class="col-lg-3 col-md-3" id="exam-block" >
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('exam_id', $manage['arr_exams'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3" id="class-block" >
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('class_id', $manage['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="list-block" style="display: none">
                            <hr>
                            <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="class-section-table" style="width:100%">
                                    {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{!! trans('language.class_name') !!}</th>
                                            <th>{!! trans('language.total_strength') !!}</th>
                                            <th>{!! trans('language.total_pass_students') !!}</th>
                                            <th>{!! trans('language.total_fail_students') !!}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#class-section-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-class-data')}}',
                data: function (d) {
                    d.exam_id = $('select[name=exam_id]').val();
                    d.term_id = $('select[name=term_id]').val();
                    d.type = $('select[name=type]').val();
                    d.class_id = $('select[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'class_name', name: 'class_name'},
                {data: 'total_strength', name: 'total_strength'},
                {data: 'total_pass_students', name: 'total_pass_students'},
                {data: 'total_fail_students', name: 'total_fail_students'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2,3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            var term_id = $("#term_id").val();
            var exam_id = $("#exam_id").val();
            if(exam_id == "" ){
                $("#list-block").hide();
            } else {
                table.draw();
                $("#list-block").show();
            }
            e.preventDefault();
        });
    
        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    function showBlock(type){
        // if(type == 2){
        //     $("#term-block").show();
        //     $("#exam-block").hide();
        //     $("#class-block").show();
        //     $("#exam_id").val('');
        // } else if(type == 1){
        //     $("#term-block").hide();
        //     $("#exam-block").show();
        //     $("#class-block").show();
        //     $("#term_id").val('');
        // } else {
        //     $("#term-block").hide();
        //     $("#exam-block").hide();
        //     $("#class-block").hide();
        //     $("#exam_id").val('');
        //     $("#term_id").val('');
        // }
    }
    function checkSearch(){
        var exam_id = $("#exam_id").val();
        var term_id = $("#term_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(exam_id == "" && term_id == ''){
            $(':input[name="Search"]').prop('disabled', true);
            $("#marks-grid").css("display", "none");
        } else {
            
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }


</script>
@endsection