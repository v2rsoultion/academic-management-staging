@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/manage-report-card') !!}">{!! trans('language.manage_report_card') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" id="top-block">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>View</strong> Marksheet  
                            <small>Exam Name: {!! $exam_result['examInfo']->exam_name !!}</small>
                            <small>Class: {!! $exam_result['classInfo']->class_name !!} </small> 
                            <small>Total Students: {!! $total_students !!}</small> 
                        </h2>

                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            {!! Form::hidden('exam_id',$exam_result->exam_id,['id' => 'exam_id', 'readonly' => 'true']) !!}
                            {!! Form::hidden('class_id',$exam_result->class_id,['id' => 'class_id', 'readonly' => 'true']) !!}
                            <div class="col-lg-2 col-md-2">
                                <div class="input-group ">
                                    {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="input-group ">
                                    {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                </div>
                            </div>
                            
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div id="list-block">
                            <hr>
                            <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="student-result-table" style="width:100%">
                                    {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{!! trans('language.student_name') !!}</th>
                                            <th>{!! trans('language.student_enroll_number') !!}</th>
                                            <th>{!! trans('language.student_percentage') !!}(%)</th>
                                            <th>{!! trans('language.class_rank') !!}</th>
                                            <th>{!! trans('language.section_rank') !!}</th>
                                            <th>{!! trans('language.pass_fail_status') !!}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#student-result-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/student-result-table')}}',
                data: function (d) {
                    d.exam_id = $('input[name=exam_id]').val();
                    d.enroll_no = $('input[name=enroll_no]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.class_id = $('input[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_name', name: 'student_name'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'percentage', name: 'student_percentage'},
                {data: 'class_rank', name: 'class_rank'},
                {data: 'section_rank', name: 'section_rank'},
                {data: 'pass_fail_status', name: 'pass_fail_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2,3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
    
        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    
   


</script>
@endsection