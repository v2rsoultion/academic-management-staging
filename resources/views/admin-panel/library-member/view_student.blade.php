@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.member_student_view') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.library_member') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.member_student_view') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'],isset($listData['class_id']) ? $listData['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $listData['arr_section'],isset($listData['section_id']) ? $listData['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="student-member-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('language.member_student_roll_no')}}</th>
                                                <th>{{trans('language.member_student_name')}}</th>
                                                <th>{{trans('language.member_student_class_section')}}</th>
                                                <th>{{trans('language.member_no_of_book_issued')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#student-member-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: "{{url('admin-panel/member/view-students-data')}}",
                data: function (d) {
                    d.roll_no           = $('input[name=roll_no]').val();
                    d.student_name      = $('input[name=student_name]').val();
                    d.class_id          = $('select[name="class_id"]').val();
                    if($('select[name=section_id]').find(':selected').val() != 'Select section'){
                        d.section_id    = $('select[name=section_id]').find(':selected').val();
                    }else{
                        d.section_id    = '';
                    }
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'roll_no', name: 'roll_no'},
                {data: 'student_name', name: 'student_name'},
                {data: 'class_section' , name: 'class_section'},
                {data: 'no_of_book_issued', name: 'no_of_book_issued'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0,
                    "orderable": false,
                    "width": "12%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "20%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
            $("select[name='class_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })

        
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $("select[name='section_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
        }
    }

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

    function checksubmitvalues(){
        return $(".check:checked").length != 0 ? true : false;
    }

</script>
@endsection