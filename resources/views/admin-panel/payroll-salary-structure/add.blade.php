@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
   <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_sal_structure') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-salary-structure') !!}">{!! trans('language.manage_sal_structure') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_sal_structure' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-salary-structure._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12" style="margin-left:-13px;">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_sal_structure','',['class' => 'form-control','placeholder' => trans('language.sal_structure_name'), 'id' => 's_sal_structure']) !!}
            </div>
          </div>
          
          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="sal-structure-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.sal_head_s_no') !!}</th>
              <th>{!! trans('language.sal_structure_name') !!}</th>
              <th>{!! trans('language.sal_head_info') !!}</th>
              <th>{!! trans('language.arrear_round_off') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#sal-structure-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-salary-structure-view')}}",
              data: function (f) {
                  f.s_sal_structure = $('#s_sal_structure').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'sal_structure_name', name: 'sal_structure_name'},
            {data: 'sal_head_info', name: 'sal_head_info'},
            {data: 'round_off', name: 'round_off'},
            {data: 'action', name: 'action'}
        ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "35%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "12%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "12%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });

    $(document).on('click', '.sal-head-info', function(e) {
        var sal_structure_id = $(this).attr('sal_structure_id'); 
        var table = $('#sal-head-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-salary-structure-data')}}",
              type: 'GET',
              data: {
                'sal_structure_id': sal_structure_id
              }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'salary_head_name', name: 'salary_head_name'},
            {data: 'salary_head_pf', name: 'salary_head_pf'},
            {data: 'salary_head_esi', name: 'salary_head_esi'}
        ],
            columnDefs: [
                {
                  "targets": 0, // your case first column
                  "orderable": false
                },
                {
                  targets: [ 0, 1],
                  className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#viewSalaryHeadModel').on('hidden.bs.modal', function () {
        location.reload();
        });
    }); 
</script>
<div class="modal fade" id="viewSalaryHeadModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Salary Head Info</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="table-responsive">
          <table class="table m-b-0 c_list" id="sal-head-table" width="100%">
            <thead>
              <tr>
              <th>S.No</th>
              <th>Salary Head</th>
              <th>PF Volume</th>
              <th>ESI Volume</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>  
@endsection