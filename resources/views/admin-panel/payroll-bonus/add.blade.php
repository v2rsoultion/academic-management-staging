@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
   <div class="block-header">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_bonus') !!}</h2>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-bonus') !!}">{!! trans('language.manage_bonus') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_bonus' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.payroll-bonus._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap">
      <div class="headingcommon col-lg-12" style="margin-left:-13px;">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_bonus_name','',['class' => 'form-control','placeholder' => trans('language.bonus_name'), 'id' => 's_bonus_name']) !!}
            </div>
          </div>
          
          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="bonus-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.bonus_s_no') !!}</th>
              <th>{!! trans('language.bonus_name') !!}</th>
              <th>{!! trans('language.bonus_amount') !!}</th>
              <th>{!! trans('language.no_staff_mapped') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#bonus-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-bonus-view')}}",
              data: function (f) {
                  f.s_bonus_name = $('#s_bonus_name').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'bonus_name', name: 'bonus_name'},
            {data: 'bonus_amount', name: 'bonus_amount'},
            {data: 'staff_mapped', name: 'staff_mapped'},
            {data: 'action', name: 'action'}
        ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "3%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        });     
    });
    $(document).on('click', '.bonus-staff', function(e) {
          var bonus_id = $(this).attr('bonus_id');
          var table = $('#bonus-staff-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-bonus-employees-data')}}",
              type: 'GET',
              data: {
                'bonus_id': bonus_id
              }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'employee_profile', name: 'employee_profile'},
            {data: 'previous_bonus', name: 'previous_bonus'}
        ],
            columnDefs: [
                {
                  "targets": 0, // your case first column
                  "orderable": false
                },
                {
                  targets: [ 0, 1],
                  className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#viewStaffModel').on('hidden.bs.modal', function () {
        location.reload();
        });
    });
</script>
<div class="modal fade" id="viewStaffModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Staffs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table m-b-0 c_list" id="bonus-staff-table" width="100%">
            <thead>
              <tr>
              <th>S.No</th>
              <th>{!! trans('language.emp_name') !!}</th>
              <th>Previous Arrear</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>   
@endsection