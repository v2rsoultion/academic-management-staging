@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .checkbox label, .radio label{
        line-height: 19px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>{!! trans('language.map_employees') !!}</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
        <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll') !!}">{!! trans('language.payroll') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-bonus') !!}">{!! trans('language.manage_bonus') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-bonus/view-map-employees') !!}">{!! trans('language.map_employees') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="headingcommon  col-lg-12" style="margin-left: -13px">Map Employees :-</div>
                <input type="hidden", value="{{$bonus_id}}" id="bonus_id">
                @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach ($errors->all() as $error)
                    {{ $error }}</br >
                    @endforeach
                </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get('success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                {!! Form::open(['files'=>TRUE, 'class'=>'form-horizontal','url' =>$save_url]) !!}
          
                <div class="table-responsive">
                <table class="table m-b-0 c_list" id="map-employee-table" width="100%">
                  <thead>
                    <tr>
                    <th>
                      <div class="checkbox" id="customid">
                        <input type="checkbox" id="check_all" class="check" >
                        <label  class="from_one1" style="margin-bottom: 4px !important;"  for="check_all"></label>
                        Select All</div> 
                    </th>
                      <th>{!! trans('language.emp_name') !!}</th>
                      <th>Previous Bonus</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
                <div class="container-fluid">
                  <div class= "row">
                    <button type="submit" class="float-right btn btn-raised btn-primary saveBtn" title="Save">Save</button>
                  </div>
                </div>
                {!! Form::close() !!}
              </div>     
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
    var bonus_id = $("#bonus_id").val();
        var table = $('#map-employee-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/payroll/manage-bonus-employees-data')}}",
              type: 'GET',
                data: {
                    'bonus_id': bonus_id
                },
          },
          columns: [
            {data: 'checkbox', name: 'checkbox'},
            {data: 'employee_profile', name: 'employee_profile'},
            {data: 'previous_bonus', name: 'previous_bonus'}
        ],
            columnDefs: [
                {
                  "targets": 0, // your case first column
                  "orderable": false
                },
                {
                  targets: [ 0, 1],
                  className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
        
    });
</script>
@endsection