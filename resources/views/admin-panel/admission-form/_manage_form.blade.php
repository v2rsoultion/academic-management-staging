@if(isset($admissionForm['admission_form_id']) && !empty($admissionForm['admission_form_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('admission_field_id',old('admission_field_id',isset($admissionForm['fields']['admission_field_id']) ? $admissionForm['fields']['admission_field_id'] : ''),['class' => 'gui-input', 'id' => 'admission_field_id', 'readonly' => 'true']) !!}

{!! Form::hidden('admission_form_id',old('admission_form_id',isset($admissionForm['admission_form_id']) ? $admissionForm['admission_form_id'] : ''),['class' => 'gui-input', 'id' => 'admission_form_id', 'readonly' => 'true']) !!}

@if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<!-- Basic Info section -->


<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.form_name') !!}  :</lable>
        <div class="form-group">
            {!! Form::text('form_name', old('form_name',isset($admissionForm['form_name']) ? $admissionForm['form_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_name'), 'id' => 'form_name',$disabled]) !!}
        </div>
        @if ($errors->has('form_name')) <p class="help-block">{{ $errors->first('form_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('medium_type', $admissionForm['arr_medium'],old('medium_type',isset($admissionForm['medium_type']) ? $admissionForm['medium_type'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=> 'getClass(this.value)',$disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('session_id', $admissionForm['arr_session'],old('session_id',isset($admissionForm['session_id']) ? $admissionForm['session_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id', 'onChange'=> 'getBrochure(this.value)',$disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.class_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('class_id', $admissionForm['arr_class'],old('class_id',isset($admissionForm['class_id']) ? $admissionForm['class_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange'=>'getIntake(this.value)',$disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    
</div>
<br />
<div class="row clearfix">
    @foreach($admissionForm['arr_student_fields'] as $key => $fields)
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="checkbox">
            <input id="check{{$key}}" type="checkbox" @if(isset($admissionForm['fields']['form_keys'])) @if(in_array($key, $admissionForm['fields']['form_keys'])) checked @endif @endif name="fields[]" class="checkBoxClass" value="{{$key}}">
            <label for="check{{$key}}" class="checkbox_1">{{$fields}}</label>
        </div>
    </div>
    @endforeach
</div>

<br />
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.form_pay_mode') !!}(Multiple selection) :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('form_pay_mode[]', $admissionForm['payment_mode'],old('form_pay_mode',isset($admissionForm['fields']['form_pay_mode']) ? $admissionForm['fields']['form_pay_mode'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_pay_mode', 'multiple'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/admission') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#admission-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                form_name: {
                    required: true,
                    lettersonly:true
                },
                session_id: {
                    required: true,
                },
                class_id: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                no_of_intake: {
                    required: true
                },
                form_prefix: {
                    required: true
                },
                form_type: {
                    required: true
                },
                form_success_message: {
                    required: true
                },
                form_failed_message: {
                    required: true
                },
                form_message_via: {
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#form_last_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        
    });

    function showOptions(type){
        $(".mycustloading").show();
        if(type == 0){
            $('#admission_fields').show();
            $(".mycustloading").hide();
        } else {
            $('#admission_fields').hide();
            $(".mycustloading").hide();
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    function getBrochure(session_id)
    {
        if(session_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/brochure/get-brochure-data')}}",
                type: 'GET',
                data: {
                    'session_id': session_id
                },
                success: function (data) {
                    $("select[name='brochure_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='brochure_id'").html('<option value="">Select Brochure</option>');
            $(".mycustloading").hide();
        }
    }
    function getIntake(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-total-intake-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("#no_of_intake").val(data);
                    $("#no_of_intake1").val(data);
                    $(".mycustloading").hide();
                    if(data == 0){
                        $('#error-block').html("Sorry, We have already exceed class's student capacity.");
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $(':input[type="submit"]').prop('disabled', false);
                        $('#error-block').html('');
                    }
                }
            });
        } else {
            $("#no_of_intake").vl('0');
            $("#no_of_intake1").vl('0');
            $(".mycustloading").hide();
        }
    }

</script>