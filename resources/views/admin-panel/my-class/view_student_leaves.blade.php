@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important; 
    }
    .modal-dialog {
  max-width: 550px !important;
}
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_student_leaves') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/my-class') !!}">{!! trans('language.menu_my_class') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/my-class') !!}">{!! trans('language.leave_application') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_student_leaves') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('father_name', old('father_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_father_name'), 'id' => 'father_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        {!! Form::hidden('class_id', old('class_id', $staff_class_info['class_id']), ['id' => 'class_id']) !!}
                                        {!! Form::hidden('section_id', old('section_id', $staff_class_info['section_id']), ['id' => 'section_id']) !!}
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="leave-table" style="width:100%">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>{{trans('language.profile')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th>{{trans('language.leave_date_range')}}</th>
                                                <th>{{trans('language.leave_app_reason')}}</th>
                                                <th>{{trans('language.leave_app_attachment')}}</th>
                                                <th>{{trans('language.leave_app_status')}}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#leave-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/my-class/get-student-leaves')}}',
                data: function (d) {
                    d.enroll_no = $('input[name=enroll_no]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.father_name = $('input[name=father_name]').val();
                    d.section_id = $('input[name=section_id]').val();
                    d.class_id = $('input[name=class_id]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'student_profile', name: 'student_profile'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'date_range', name: 'date_range'},
                {data: 'student_leave_reason', name: 'student_leave_reason'},
                {data: 'attachment', name: 'attachment'},
                {data: 'leave_status', name: 'leave_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "3%"
                },
                {
                    "targets": 1, // your case first column
                    "className": "text-center",
                    "width": "12%"
                },
                {
                    "targets": 2, // your case first column
                    "className": "text-center",
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 7, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })

        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    

</script>

@endsection