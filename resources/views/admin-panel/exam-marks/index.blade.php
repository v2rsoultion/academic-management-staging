@extends('admin-panel.layout.header')
@section('content')
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix" id="top-block">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Manage</strong> Marks  </h2>
                    </div>
                    <div class="body form-gap">
                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('exam_id', $manage['arr_exams'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'exam_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('class_id', $manage['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value);checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <label class=" field select" style="width: 100%">
                                {!!Form::select('section_id', $manage['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','onChange' => 'checkSearch()'])!!}
                                <i class="arrow double"></i>
                                </label>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search', 'disabled']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        
                        {!! Form::open(['files'=>TRUE,'id' => 'grid-form' , 'class'=>'form-horizontal']) !!}
                        <hr>

                        <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
                        <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
                        <div id="marks-grid" style="display: none">
                            
                        </div>
                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    

    $(document).on('keyup','.marks',function(e){
        var max_marks = $(this).attr("max-marks");
        var optain_marks = $(this).val();
        var isValid = false;
        var regex = /^[0-9]+(\.[0-9][0-9]?)?$/;
        isValid = regex.test(optain_marks);
        if( optain_marks.match(regex) ){
            if(parseFloat(optain_marks) > parseFloat(max_marks)){
                $(this).val(max_marks);
            }
            $(this).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        } else if( optain_marks == 'A' ) {
            $(this).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        } else {
            $(this).parents( ".form-group" ).removeClass( "has-success" ).addClass( "has-error" );
            $(this).val('');
        }
        
    })
    $('#search-form').on('submit', function(e) {
        var exam_id = $("#exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        $(".mycustloading").show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/examination/get-marks-grid')}}",
            type: 'GET',
            data: {
                'exam_id' : exam_id,
                'class_id': class_id,
                'section_id' : section_id
            },
            success: function (data) {
                $("#marks-grid").css("display", "block");
                $("#marks-grid").html(data);
                $(".mycustloading").hide();
                
            }
        });
        e.preventDefault();
    });
    $(document).on('click','.clearBtn1',function(e){
        var exam_id = $("#exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        $(".mycustloading").show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/examination/get-marks-grid')}}",
            type: 'GET',
            data: {
                'exam_id' : exam_id,
                'class_id': class_id,
                'section_id' : section_id
            },
            success: function (data) {
                $("#marks-grid").css("display", "block");
                $("#marks-grid").html(data);
                $(".mycustloading").hide();
                
            }
        });
        e.preventDefault();
    });
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                    
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }
    
    function checkSearch(){
        var exam_id = $("#exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(exam_id == "" || class_id == "" || section_id == ""){
            $(':input[name="Search"]').prop('disabled', true);
            $("#marks-grid").css("display", "none");
        } else {
            $("#st_section_id").val(section_id);
            $("#st_class_id").val(class_id);
            $("#st_exam_id").val(exam_id);
            $(':input[name="Search"]').prop('disabled', false);
        }
        return true;
    }
    $('#grid-form').on('submit', function(e) { 
        var exam_id = $("#exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        var formData = $(this).serialize();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/examination/save-student-marks')}}",
            data     : formData,
            cache    : false,
            success  : function(data) {
                var trimmedResponse = $.trim(data);
                if(trimmedResponse == "success"){
                    $("#success-block").html("Marks successfully updated.");
                    $("#success-block").show();
                    $("#error-block").html("");
                    $("#error-block").hide();
                    $('html, body').animate({
                        scrollTop: $("#top-block").offset().top
                    }, 1000);
                    $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/examination/get-marks-grid')}}",
                    type: 'GET',
                    data: {
                        'exam_id' : exam_id,
                        'class_id': class_id,
                        'section_id' : section_id
                    },
                    success: function (data) {
                        $("#marks-grid").css("display", "block");
                        $("#marks-grid").html(data);
                        $(".mycustloading").hide();
                        
                    }
                });
                } else {
                    $("#success-block").html("");
                    $("#success-block").hide();
                    $("#error-block").html("Something goes wrong.");
                    $("#error-block").show();
                    $('html, body').animate({
                        scrollTop: $("#top-block").offset().top
                    }, 1000);
                }
            }
        })
        e.preventDefault();
    });


    $(document).on('click','.final_submission',function(e){
        var exam_id = $("#exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/examination/marks-submission')}}",
            data: {
                'exam_id': exam_id,
                'class_id': class_id,
                'section_id': section_id
            },
            cache    : false,
            success  : function(data) {
                var trimmedResponse = $.trim(data);
                if(trimmedResponse == "success"){
                    $("#success-block").html("Marks successfully Submitted.");
                    $("#success-block").show();
                    $("#error-block").html("");
                    $("#error-block").hide();
                    $('html, body').animate({
                        scrollTop: $("#top-block").offset().top
                    }, 1000);
                    $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/examination/get-marks-grid')}}",
                    type: 'GET',
                    data: {
                        'exam_id' : exam_id,
                        'class_id': class_id,
                        'section_id' : section_id
                    },
                    success: function (data) {
                        $("#marks-grid").css("display", "block");
                        $("#marks-grid").html(data);
                        $(".mycustloading").hide();
                        
                    }
                });
                } else {
                    $("#success-block").html("");
                    $("#success-block").hide();
                    $("#error-block").html("Something goes wrong.");
                    $("#error-block").show();
                    $('html, body').animate({
                        scrollTop: $("#top-block").offset().top
                    }, 1000);
                }
            }
        })
        e.preventDefault();
    });

</script>
@endsection