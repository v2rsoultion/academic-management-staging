<aside id="leftsidebar" class="sidebar">
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <!-- <li>
                        <div class="user-info">
                          <div class="image" style="padding-top: 8px !important;"><a href=""><img src="{!! URL::to('public/admin/assets/images/logo.png') !!}" alt="User" width="70%" ></a></div>
                        </div>
                        </li> -->
                    <div class="user-info">
                        <div class="image" style="margin-top: 7px"><a href="{{ url('admin-panel/dashboard') }}" class=" waves-effect waves-block">
                            <img src="{!! URL::to('public/assets/images/admin_photo.jpg') !!}" style="width: 60px; height: 60px; border: 3px solid #acadaf;">
                            </a>
                        </div>
                        <div class="detail"><b>{!! $login_info['admin_name'] !!} </b>
                            <br>
                            <small>{!! $login_info['admin_name'] !!}</small>
                        </div>
                    </div>
                    <!-- ======== Start here side menu bar ========== -->
                    <div class="adminmenu" style="padding-top: 6px;">
                        @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                        <ul class="text-center">
                            <a href="{{ url('admin-panel/configuration') }}" title="configuration" class="icyLink">
                              <li class="@if(Request::segment(3) == "configuration" || Request::segment(2) == "configuration" || Request::segment(2) == "school" || Request::segment(2) == "academic-year" || Request::segment(2) == "holidays" || Request::segment(2) == "shift" || Request::segment(2) == "facilities" || Request::segment(2) == "document-category" || Request::segment(2) == "designation" || Request::segment(2) == "title" || Request::segment(2) == "caste" || Request::segment(2) == "religion" || Request::segment(2) == "nationality" || Request::segment(2) == "schoolgroup" || Request::segment(2) == "room-no" || Request::segment(2) == "stream" || Request::segment(2) == "school-board" || Request::segment(2) == "country" || Request::segment(2) == "state" || Request::segment(2) == "city" )  active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Configuration.svg') !!}" alt="{!! trans('language.menu_configuration') !!}">
                              <span>{!! trans('language.menu_configuration') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/academic') }}" title="Academic">
                              <li class="@if(Request::segment(3) == "academic" || Request::segment(2) == "academic" || Request::segment(2) == "class" || Request::segment(2) == "section" || Request::segment(2) == "class-teacher-allocation" || Request::segment(2) == "subject" || Request::segment(2) == "competition" || Request::segment(2) == "time-table" || Request::segment(2) == "teacher-subject-mapping" || Request::segment(2) == "student-subject" || Request::segment(2) == "mediums" || Request::segment(2) == "class-subject-mapping" || Request::segment(2) == "section-subject-mapping" ) active @endif">
                              <img src="{!! URL::to('public/assets/images/academic/Academic.svg') !!}" alt="{!! trans('language.menu_academic') !!}">  
                              <span>{!! trans('language.menu_academic') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/admission') }}" title="Admission">
                              <li class="@if(Request::segment(3) == "admission" || Request::segment(2) == "admission" || Request::segment(2) == "admission-form") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/admission.svg') !!}" alt="{!! trans('language.menu_admission') !!}">
                              <span>{!! trans('language.menu_admission') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/student') }}" title="Student">
                              <li class="@if(Request::segment(3) == "student" || Request::segment(2) == "student" || Request::segment(2) == "student-leave-application" || Request::segment(2) == "daily-remarks"  ) active @endif">
                              <img src="{!! URL::to('public/assets/images/Student/student.svg') !!}" alt="{!! trans('language.menu_student') !!}">
                              <span>{!! trans('language.menu_student') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/examination') }}" title="Examination">
                              <li class="@if(Request::segment(2) == "examination" || Request::segment(2) == "examination") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Examination.svg') !!}" alt="{!! trans('language.menu_examination') !!}">
                              <span>{!! trans('language.menu_examination') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/fees-collection') }}" title="Fees Collection">
                              <li class="@if(Request::segment(2) == "one-time" || Request::segment(2) == "fees-collection" || Request::segment(2) == "recurring-head" || Request::segment(2) == "bank" || Request::segment(2) == "receive-cheque" || Request::segment(2) == "rte-cheque" || Request::segment(2) == "rte-head" ) active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/fees_collection.svg') !!}" alt="{!! trans('language.menu_fee_collection') !!}">
                              <span>{!! trans('language.menu_fee_collection') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/staff') }}" title="Staff">
                              <li class="@if(Request::segment(3) == "staff" || Request::segment(2) == "staff") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Staff.svg') !!}" alt="{!! trans('language.menu_staff') !!}">
                              <span>{!! trans('language.menu_staff') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/visitor') }}" title="Visitor">
                              <li class="@if( Request::segment(2) == "visitor") active @endif">
                              <img src="{!! URL::to('public/assets/images/Visitor/Visitor.svg') !!}" alt="">
                              <span>{!! trans('language.menu_visitor') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/notice-board') }}" title="Noticeboard">
                              <li class="@if(Request::segment(3) == "notice-board" || Request::segment(2) == "notice-board") active @endif">
                              <img src="{!! URL::to('public/assets/images/notice_board.svg') !!}" alt="{!! trans('language.menu_notice_board') !!}">
                              <span>{!! trans('language.menu_notice_board') !!}</span> 
                              </li>
                            </a>

                            <a href="{{ url('admin-panel/online-content') }}" title="Online Content">
                              <li class="@if(Request::segment(3) == "notes" || Request::segment(2) == "question-paper") active @endif">
                              <img src="{!! URL::to('public/assets/images/OnlineContent/Online Content.svg') !!}" alt="{!! trans('language.menu_online_content') !!}">
                              <span>{!! trans('language.menu_online_content') !!}</span> 
                              </li>
                            </a>

                            @if(in_array('12',$login_info['permissions']))
                            <a href="{{ url('admin-panel/task-manager') }}" title="Task Manager">
                              <li class="@if(Request::segment(3) == "task-manager" || Request::segment(2) == "task-manager") active @endif">
                                <img src="{!! URL::to('public/assets/images/Task Manager/Task Manager.svg') !!}" alt="">
                                <span>{!! trans('language.menu_task_manager') !!}</span> 
                              </li>
                            </a>
                            @endif
                            
                            @if(in_array('13',$login_info['permissions']))
                            <a href="{{ url('admin-panel/transport') }}" title="Transport">
                              <li class="@if(Request::segment(3) == "transport" || Request::segment(2) == "transport") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Transport.svg') !!}" alt="{!! trans('language.menu_transport') !!}">
                              <span>{!! trans('language.menu_transport') !!}</span> 
                              </li>
                            </a>
                            @endif

                            @if(in_array('14',$login_info['permissions']))
                            <a href="{{ url('admin-panel/hostel') }}" title="Hostel">
                              <li class="@if(Request::segment(3) == "hostel" || Request::segment(2) == "hostel") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Hostel.svg') !!}" alt="{!! trans('language.menu_hostel') !!}">
                              <span>{!! trans('language.menu_hostel') !!}</span> 
                              </li>
                            </a>
                            @endif

                            @if(in_array('15',$login_info['permissions']))
                            <a href="{{ url('admin-panel/library') }}" title="Library">
                              <li class="@if(Request::segment(2) == "library" || Request::segment(2) == "book" || Request::segment(2) == "book-cupboard" || Request::segment(2) == "book-category" || Request::segment(2) == "book-vendor" || Request::segment(2) == "book-cupboard" || Request::segment(2) == "book" || Request::segment(2) == "member" || Request::segment(2) == "issue-book" || Request::segment(2) == "return-book" ) active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Library.svg') !!}" alt="{!! trans('language.menu_library') !!}">
                              <span>{!! trans('language.menu_library') !!}</span> 
                              </li>
                            </a>
                            @endif

                            @if(in_array('16',$login_info['permissions']))
                            <a href="{{ url('admin-panel/payroll') }}" title="Payroll">
                              <li class="@if(Request::segment(3) == "payroll" || Request::segment(2) == "payroll") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/SideBar Payroll.svg') !!}" alt="">
                              <span>Payroll</span> 
                              </li>
                            </a>
                            @endif

                            @if(in_array('17',$login_info['permissions']))
                            <a href="{{ url('admin-panel/inventory') }}" title="Inventory">
                              <li class="@if(Request::segment(3) == "inventory" || Request::segment(2) == "inventory") active @endif">
                                <img src="{!! URL::to('public/assets/images/menu/Inventory.svg') !!}" alt="">
                                <span>Inventory</span> 
                              </li>
                            </a>
                            @endif

                            @if(in_array('18',$login_info['permissions']))
                            <a href="{{ url('admin-panel/recruitment') }}" title="Recruitment">
                              <li class="@if(Request::segment(3) == "recruitment" || Request::segment(2) == "recruitment") active @endif">
                              <img src="{!! URL::to('public/assets/images/Recruitment/Recruitment.svg') !!}" alt="{!! trans('language.menu_recruitment') !!}">
                              <span>{!! trans('language.menu_recruitment') !!}</span> 
                              </li>
                            </a>
                            @endif

                            @if(in_array('19',$login_info['permissions']))
                            <a href="{!! url('admin-panel/substitute-management') !!}" title="Substitute Management">
                              <li class="@if( Request::segment(2) == "substitute-management") active @endif">
                              <img src="{!! URL::to('public/assets/images/permissions.svg') !!}" alt="{!! trans('language.menu_substitute_management') !!}">
                              <span>{!! trans('language.menu_substitute_management') !!}</span> 
                              </li>
                            </a>
                            @endif
                            @if(in_array('20',$login_info['permissions']))
                            <a href="{!! url('admin-panel/account') !!}" title="Accounts">
                              <li class="@if( Request::segment(2) == "account") active @endif">
                              <img src="{!! URL::to('public/assets/images/menu/Account.svg') !!}" alt="{!! trans('language.menu_account') !!}">
                              <span>{!! trans('language.menu_account') !!}</span> 
                              </li>
                            </a>
                            @endif
                            <a href="{!! url('admin-panel/permissions') !!}" title="Permissions">
                              <li class="@if( Request::segment(2) == "permissions") active @endif">
                              <img src="{!! URL::to('public/assets/images/permissions.svg') !!}" alt="{!! trans('language.permissions') !!}">
                              <span>{!! trans('language.permissions') !!}</span> 
                              </li>
                            </a>
                            <!-- <a href="#" title="Task Manager">
                            <li class="@if(Request::segment(3) == "task-manager" || Request::segment(2) == "task-manager") active @endif">
                            <img src="{!! URL::to('public/assets/images/Task Manager/Task Manager.svg') !!}" alt="{!! trans('language.menu_task_manager') !!}">
                            <span>{!! trans('language.menu_task_manager') !!}</span> 
                            </li>
                            </a> -->
                        </ul>
                        @endif
                        @if($login_info['admin_type'] == 2 )
                        <ul class="text-center">
                            <a href="{!! url('admin-panel/staff/view-profile/'.$login_info['encrypted_staff_id']) !!}" title="Profile">
                            <li class="@if( Request::segment(2) == "view-profile") active @endif">
                            <img src="{!! URL::to('public/assets/images/Staff/Staff.svg') !!}" alt="{!! trans('language.view_profile') !!}">
                            <span>{!! trans('language.profile') !!}</span> 
                            </li>
                            </a>

                            @if(in_array(2,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/my-class') }}" title="{!! trans('language.menu_my_class') !!}">
                            <li class="@if(Request::segment(2) == "my-class") active @endif">
                            <img src="{!! URL::to('public/assets/images/Staff/My Class.svg') !!}" alt="{!! trans('language.menu_my_class') !!}">
                            <span>{!! trans('language.menu_my_class') !!}</span> 
                            </li>
                            </a>
                            @endif
                            <a href="{{ url('admin-panel/my-subjects') }}" title="{!! trans('language.menu_my_subject') !!}">
                            <li class="@if(Request::segment(2) == "my-subjects") active @endif">
                            <img src="{!! URL::to('public/assets/images/Staff/My Subjects.svg') !!}" alt="{!! trans('language.menu_my_subject') !!}">
                            <span>{!! trans('language.menu_my_subject') !!}</span> 
                            </li>

                            <a href="{{ url('admin-panel/my-task') }}" title="My Task">
                            <li class="@if(Request::segment(3) == "my-task" || Request::segment(2) == "my-task") active @endif">
                              <img src="{!! URL::to('public/assets/images/Task Manager/Task Manager.svg') !!}" alt="">
                              <span>{!! trans('language.my_task') !!}</span> 
                            </li>

                            <a href="{{ url('admin-panel/plan-schedule') }}" title="My Plan Schedule">
                            <li class="@if(Request::segment(3) == "plan-schedule" || Request::segment(2) == "plan-schedule") active @endif">
                              <img src="{!! URL::to('public/assets/images/schedule_icon.svg') !!}" alt="">
                              <span>{!! trans('language.my_plan_schedule') !!}</span> 
                            </li>

                            </a>
                            @if(in_array(8,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/recruitment') }}" title="Recruitment">
                            <li class="@if(Request::segment(3) == "recruitment" || Request::segment(2) == "recruitment") active @endif">
                            <img src="{!! URL::to('public/assets/images/recruitment/recruitment.svg') !!}" alt="{!! trans('language.menu_recruitment') !!}">
                            <span>{!! trans('language.menu_recruitment') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(5,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/examination') }}" title="Examination">
                            <li class="@if(Request::segment(2) == "examination" || Request::segment(2) == "examination") active @endif">
                            <img src="{!! URL::to('public/assets/images/Examination.svg') !!}" alt="{!! trans('language.menu_examination') !!}">
                            <span>{!! trans('language.menu_examination') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(9,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/fees-collection') }}" title="Fees Collection">
                            <li class="@if(Request::segment(2) == "one-time" || Request::segment(2) == "fees-collection" || Request::segment(2) == "recurring-head" || Request::segment(2) == "bank" || Request::segment(2) == "receive-cheque" || Request::segment(2) == "rte-cheque" || Request::segment(2) == "rte-head" ) active @endif">
                            <img src="{!! URL::to('public/assets/images/menu/fees_collection.svg') !!}" alt="{!! trans('language.menu_fee_collection') !!}">
                            <span>{!! trans('language.menu_fee_collection') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(7,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/hostel') }}" title="Hostel">
                            <li class="@if(Request::segment(3) == "hostel" || Request::segment(2) == "hostel") active @endif">
                            <img src="{!! URL::to('public/assets/images/menu/Hostel.svg') !!}" alt="{!! trans('language.menu_hostel') !!}">
                            <span>{!! trans('language.menu_hostel') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(12,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/library') }}" title="Library">
                            <li class="@if(Request::segment(2) == "library" || Request::segment(2) == "book" || Request::segment(2) == "book-cupboard" || Request::segment(2) == "book-category" || Request::segment(2) == "book-vendor" || Request::segment(2) == "book-cupboard" || Request::segment(2) == "book" || Request::segment(2) == "member" || Request::segment(2) == "issue-book" || Request::segment(2) == "return-book" ) active @endif">
                            <img src="{!! URL::to('public/assets/images/menu/library.svg') !!}" alt="{!! trans('language.menu_library') !!}">
                            <span>{!! trans('language.menu_library') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(14,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/transport') }}" title="Transport">
                            <li class="@if(Request::segment(3) == "transport" || Request::segment(2) == "transport") active @endif">
                            <img src="{!! URL::to('public/assets/images/menu/transport.svg') !!}" alt="{!! trans('language.menu_transport') !!}">
                            <span>{!! trans('language.menu_transport') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(11,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/visitor') }}" title="Visitor">
                            <li class="@if( Request::segment(2) == "visitor") active @endif">
                            <img src="{!! URL::to('public/assets/images/visitor/visitor.svg') !!}" alt="">
                            <span>{!! trans('language.menu_visitor') !!}</span> 
                            </li>
                            </a>
                            @endif
                            @if(in_array(15,$login_info['staff_role_id']) )
                            <a href="{{ url('admin-panel/notice-board') }}" title="Noticeboard">
                            <li class="@if(Request::segment(3) == "notice-board" || Request::segment(2) == "notice-board") active @endif">
                            <img src="{!! URL::to('public/assets/images/menu/noticeboard.svg') !!}" alt="{!! trans('language.menu_notice_board') !!}">
                            <span>{!! trans('language.menu_notice_board') !!}</span> 
                            </li>
                            </a>
                            @endif
                            <a href="{{ url('admin-panel/staff-leave-management') }}" title="Leave Management">
                            <li class="@if(Request::segment(2) == "staff-leave-management") active @endif">
                            <img src="{!! URL::to('public/assets/images/Staff/LeaveManagement.svg') !!}" alt="{!! trans('language.menu_leave_management') !!}">
                            <span>{!! trans('language.menu_leave_management') !!}</span> 
                            </li>
                            <a href="{{ url('admin-panel/online-content') }}" title="Online Content">
                                <li class="@if(Request::segment(3) == "notes" || Request::segment(2) == "question-paper") active @endif">
                                <img src="{!! URL::to('public/assets/images/OnlineContent/Online Content.svg') !!}" alt="{!! trans('language.menu_online_content') !!}">
                                <span>{!! trans('language.menu_online_content') !!}</span> 
                                </li>
                            </a>
                            <a href="{!! url('admin-panel/staff/notice/') !!}" title="{!! trans('language.notice') !!}">
                            <li class="@if( Request::segment(3) == "notice") active @endif">
                            <img src="{!! URL::to('public/assets/images/Notice.svg') !!}" alt="{!! trans('language.notice') !!}">
                            <span>{!! trans('language.notice') !!}</span> 
                            </li>
                            </a>
                        </ul>
                        @endif
                        @if($login_info['admin_type'] == 4 )
                        <ul class="text-center">
                        <a href="{!! url('admin-panel/student/student-profile/'.$login_info['encrypted_student_id']) !!}" title="Profile">
                        <li class="@if( Request::segment(3) == "student-profile") active @endif">
                        <img src="{!! URL::to('public/assets/images/Profile/student.svg') !!}" alt="{!! trans('language.view_profile') !!}">
                        <span>{!! trans('language.profile') !!}</span> 
                        </li>
                        </a>

                        <a href="{{ url('admin-panel/student/student-attendance') }}" title="{!! trans('language.my_attendance') !!}">
                            <li class="@if(Request::segment(3) == "student-attendance") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/Attendence.svg') !!}" alt="{!! trans('language.my_attendance') !!}">
                              <span>{!! trans('language.my_attendance') !!}</span> 
                            </li>
                          </a>
                           <a href="{{ url('admin-panel/student/student-time-table') }}" title="{!! trans('language.my_timetable') !!}">
                            <li class="@if(Request::segment(3) == "student-time-table") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/TimeTable.svg') !!}" alt="{!! trans('language.my_timetable') !!}">
                              <span>{!! trans('language.my_timetable') !!}</span> 
                            </li>
                        </a>

                        <a href="{{ url('admin-panel/my-task') }}" title="My Task">
                        <li class="@if(Request::segment(3) == "my-task" || Request::segment(2) == "my-task") active @endif">
                          <img src="{!! URL::to('public/assets/images/Task Manager/Task Manager.svg') !!}" alt="">
                          <span>{!! trans('language.my_task') !!}</span> 
                        </li>

                        <a href="{{ url('admin-panel/student/view-homework') }}" title="{!! trans('language.student_homework') !!}">
                            <li class="@if(Request::segment(3) == "view-homework") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/homework.svg') !!}" alt="{!! trans('language.student_homework') !!}">
                              <span> {!! trans('language.student_homework') !!} </span> 
                            </li>
                        </a>
                        <a href="{{ url('admin-panel/student/view-remarks') }}" title="{!! trans('language.student_remark') !!}">
                            <li class="@if(Request::segment(3) == "view-remarks") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/remarks.svg') !!}" alt="{!! trans('language.student_remark') !!}">
                              <span>{!! trans('language.student_remark') !!}</span> 
                            </li>
                        </a>

                        <a href="{{ url('admin-panel/student/examination') }}" title="Examination">
                        <li class="@if(Request::segment(2) == "examination" || Request::segment(2) == "examination") active @endif">
                        <img src="{!! URL::to('public/assets/images/menu/Examination.svg') !!}" alt="{!! trans('language.menu_examination') !!}">
                        <span>{!! trans('language.my_examination') !!}</span> 
                        </li>
                        </a>

                        <a href="{{ url('admin-panel/view-student-leave') }}" title="Leave Management">
                        <li class="@if(Request::segment(2) == "view-student-leave") active @endif">
                        <img src="{!! URL::to('public/assets/images/Staff/LeaveManagement.svg') !!}" alt="{!! trans('language.menu_leave_management') !!}">
                        <span>{!! trans('language.menu_leave_management') !!}</span> 
                        </li>
                        <a href="{{ url('admin-panel/student-online-content') }}" title="Online Content">
                        <li class="@if(Request::segment(3) == "student-online-content" || Request::segment(2) == "student-online-content") active @endif">
                        <img src="{!! URL::to('public/assets/images/OnlineContent/Online Content.svg') !!}" alt="{!! trans('language.menu_online_content') !!}">
                        <span>{!! trans('language.menu_online_content') !!}</span> 
                        </li>
                        <a href="{!! url('admin-panel/student/notice/') !!}" title="{!! trans('language.notice') !!}">
                            <li class="@if( Request::segment(3) == "notice") active @endif">
                            <img src="{!! URL::to('public/assets/images/Notice.svg') !!}" alt="{!! trans('language.notice') !!}">
                            <span>{!! trans('language.notice') !!}</span> 
                            </li>
                        </a>

                        </ul>
                        @endif


                        @if($login_info['admin_type'] == 3 )
                        <ul class="text-center">

                        <a href="{!! url('admin-panel/student/student-parent-detail/'.$login_info['parent_id']) !!}" title="Profile">
                        <li class="@if( Request::segment(3) == "view-parent-detail") active @endif">
                        <img src="{!! URL::to('public/assets/images/Profile/Parent.svg') !!}" alt="{!! trans('language.view_profile') !!}">
                        <span>{!! trans('language.profile') !!}</span> 
                        </li>
                        </a>

                        <a href="{!! url('admin-panel/student/view-student-profile/'.$login_info['encrypted_student_id']) !!}" title="Profile">
                        <li class="@if( Request::segment(3) == "view-student-profile") active @endif">
                        <img src="{!! URL::to('public/assets/images/Profile/student.svg') !!}" alt="{!! trans('language.view_profile') !!}">
                        <span>{!! trans('language.virtual_class_stu_profile') !!}</span> 
                        </li>
                        </a>

                        <a href="{{ url('admin-panel/my-task') }}" title="My Task">
                        <li class="@if(Request::segment(3) == "my-task" || Request::segment(2) == "my-task") active @endif">
                          <img src="{!! URL::to('public/assets/images/Task Manager/Task Manager.svg') !!}" alt="">
                          <span>{!! trans('language.my_task') !!}</span> 
                        </li>

                        <a href="{{ url('admin-panel/student/student-attendance') }}" title="{!! trans('language.my_attendance') !!}">
                            <li class="@if(Request::segment(3) == "student-attendance") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/Attendence.svg') !!}" alt="{!! trans('language.my_attendance') !!}">
                              <span>{!! trans('language.my_attendance') !!}</span> 
                            </li>
                          </a>

                        <a href="{{ url('admin-panel/student/communication') }}" title="{!! trans('language.communication') !!}">
                            <li class="@if(Request::segment(3) == "communication") active @endif">
                              <img src="{!! URL::to('public/assets/images/communication.svg') !!}" alt="{!! trans('language.communication') !!}">
                              <span>{!! trans('language.communication') !!}</span> 
                            </li>
                        </a>

                           <a href="{{ url('admin-panel/student/student-time-table') }}" title="{!! trans('language.my_timetable') !!}">
                            <li class="@if(Request::segment(3) == "student-time-table") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/TimeTable.svg') !!}" alt="{!! trans('language.my_timetable') !!}">
                              <span>{!! trans('language.my_timetable') !!}</span> 
                            </li>
                        </a>

                        <a href="{{ url('admin-panel/student/view-homework') }}" title="{!! trans('language.student_homework') !!}">
                            <li class="@if(Request::segment(3) == "view-homework") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/homework.svg') !!}" alt="{!! trans('language.student_homework') !!}">
                              <span> {!! trans('language.student_homework') !!} </span> 
                            </li>
                        </a>
                        <a href="{{ url('admin-panel/student/view-remarks') }}" title="{!! trans('language.student_remark') !!}">
                            <li class="@if(Request::segment(3) == "view-remarks") active @endif">
                              <img src="{!! URL::to('public/assets/images/Student-Login/remarks.svg') !!}" alt="{!! trans('language.student_remark') !!}">
                              <span>{!! trans('language.student_remark') !!}</span> 
                            </li>
                        </a>

                        <a href="{{ url('admin-panel/student/examination') }}" title="Examination">
                        <li class="@if(Request::segment(2) == "examination" || Request::segment(2) == "examination") active @endif">
                        <img src="{!! URL::to('public/assets/images/menu/Examination.svg') !!}" alt="{!! trans('language.menu_examination') !!}">
                        <span>{!! trans('language.my_examination') !!}</span> 
                        </li>
                        </a>

                        <a href="{{ url('admin-panel/student-leave-management') }}" title="Leave Management">
                        <li class="@if(Request::segment(2) == "student-leave-management") active @endif">
                        <img src="{!! URL::to('public/assets/images/Staff/LeaveManagement.svg') !!}" alt="{!! trans('language.menu_leave_management') !!}">
                        <span>{!! trans('language.menu_leave_management') !!}</span> 
                        </li>
                        <a href="{{ url('admin-panel/student-online-content') }}" title="Online Content">
                        <li class="@if(Request::segment(3) == "student-online-content" || Request::segment(2) == "student-online-content") active @endif">
                        <img src="{!! URL::to('public/assets/images/OnlineContent/Online Content.svg') !!}" alt="{!! trans('language.menu_online_content') !!}">
                        <span>{!! trans('language.menu_online_content') !!}</span> 
                        </li>
                        <a href="{!! url('admin-panel/student/notice/') !!}" title="{!! trans('language.notice') !!}">
                            <li class="@if( Request::segment(3) == "notice") active @endif">
                            <img src="{!! URL::to('public/assets/images/Notice.svg') !!}" alt="{!! trans('language.notice') !!}">
                            <span>{!! trans('language.notice') !!}</span> 
                            </li>
                        </a>

                        </ul>
                        @endif
                        <br><br><br>
                        <div class="clearfix"></div>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</aside>