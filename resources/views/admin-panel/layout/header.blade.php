<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="School management.">
    <!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> Favicon -->
    <title>{!! $page_title !!} | {!!trans('language.project_title') !!}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! Html::style('public/admin/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('public/admin/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css') !!}
    {!! Html::style('public/admin/assets/plugins/morrisjs/morris.min.css') !!}
    {!! Html::style('public/admin/assets/css/bootstrap-tagsinput.css') !!}
    
    {!! Html::style('public/admin/assets/plugins/dropzone/dropzone.css') !!}
    {!! Html::style('public/admin/assets/plugins/bootstrap-select/css/bootstrap-select.css') !!}

    <!-- Custom Css -->
    {!! Html::style('public/admin/assets/css/main.css') !!}
    {!! Html::style('public/admin/assets/css/color_skins.css') !!}
    {!! Html::style('public/admin/assets/css/style1.css') !!}
    <!-- {!! Html::style('public/admin/assets/css/development.css') !!} -->
    {!! Html::style('public/admin/assets/css/development1.css') !!}
    {!! Html::style('public/admin/assets/css/custom.css') !!}
    
    {!! Html::style('public/admin/assets/plugins/fullcalendar/fullcalendar.min.css') !!}
    {!! Html::style('public/admin/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css') !!}

    <!-- {!! Html::style('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css') !!} -->
    {!! Html::style('public/admin/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') !!}
    {!! Html::script('public/admin/assets/js/jquery.min.js') !!}

    <!-- {!! Html::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') !!} -->

    @if(Request::segment(3) != "student-profile" && Request::segment(3) != "map-classes" && Request::segment(3) != "view-profile" && Request::segment(3) != "view-parent-detail"  && Request::segment(2) != "fees-counter" && Request::segment(3) != "import-students" && Request::segment(3) != "view-school-detail" && Request::segment(3) != "manage-stock-register" && Request::segment(3) != "fees-counter" && Request::segment(3) != "manage-salary-generation") 
    {!! Html::script('public/admin/assets/js/bootstrap.min.js') !!}
    @endif

    {!! Html::script('public/admin/assets/js/bootstrap-tagsinput.min.js') !!}
    {!! Html::script('public/admin/assets/bundles/libscripts.bundle.js') !!}

    <!-- {!! Html::style('public/admin/assets/css/all.css') !!} -->
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

    <!-- jQuery Modal -->
    <!-- {!! Html::script('public/admin/assets/js/jquery.modal.min.js') !!}
    {!! Html::style('public/admin/assets/css/jquery.modal.min.css') !!} -->
    {!! Html::style('public/admin/assets/css/font-awesome.min.css') !!}
    {!! Html::style('public/admin/assets/css/select2-bootstrap.css') !!}
    {!! Html::style('public/admin/assets/css/select2.min.css') !!}
</head>

<style type="text/css">
    .student_names button{
        color: white !important;
    }
    .student_names .dropdown-menu>li>a{
        color: black !important;
    }

</style>

<body class="theme-blush">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="{!! URL::to('public/admin/assets/images/logo.svg') !!}" width="48" height="48" alt="Oreo"></div>
            <p>Please wait...</p>        
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Top Bar -->
    <nav class="navbar p-l-5 p-r-5">
        <ul class="nav navbar-nav navbar-left">
            <li>
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="{!! URL::to('admin-panel/dashboard') !!}"><img src="{!! URL::to('public/admin/assets/images/logo.svg') !!}" width="30" alt="Oreo"><span class="m-l-10">{!!trans('language.project_title') !!}</span></a>
                </div>
            </li>
            <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
         
            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="zmdi zmdi-notifications"></i>
                <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                </a>
                <ul class="dropdown-menu pullDown">
                    <li class="body">
                        <ul class="menu list-unstyled">
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object" src="http://via.placeholder.com/50x50" alt="">
                                        <div class="media-body">
                                            <span class="name">Sophia <span class="time">30min ago</span></span>
                                            <span class="message">There are many variations of passages</span>                                        
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object" src="http://via.placeholder.com/50x50" alt="">
                                        <div class="media-body">
                                            <span class="name">Sophia <span class="time">31min ago</span></span>
                                            <span class="message">There are many variations of passages of Lorem Ipsum</span>                                        
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object" src="http://via.placeholder.com/50x50" alt="">
                                        <div class="media-body">
                                            <span class="name">Isabella <span class="time">35min ago</span></span>
                                            <span class="message">There are many variations of passages</span>                                        
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object" src="http://via.placeholder.com/50x50" alt="">
                                        <div class="media-body">
                                            <span class="name">Alexander <span class="time">35min ago</span></span>
                                            <span class="message">Contrary to popular belief, Lorem Ipsum random</span>                                        
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object" src="http://via.placeholder.com/50x50" alt="">
                                        <div class="media-body">
                                            <span class="name">Grayson <span class="time">1hr ago</span></span>
                                            <span class="message">There are many variations of passages</span>                                        
                                        </div>
                                    </div>
                                </a>
                            </li>                        
                        </ul>
                    </li>
                    <li class="footer"> <a href="javascript:void(0);">View All</a> </li>
                </ul>
            </li>
            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="zmdi zmdi-flag"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
                </a>
                <ul class="dropdown-menu pullDown">
                    <li class="header">Department</li>
                    <li class="body">
                        <ul class="menu tasks list-unstyled">
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="progress-container progress-primary">
                                        <span class="progress-badge">Computer</span>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100" style="width: 86%;">
                                                <span class="progress-value">86%</span>
                                            </div>
                                        </div>                        
                                        <ul class="list-unstyled team-info">
                                            <li class="m-r-15"><small class="text-muted">Team</small></li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>                            
                                        </ul>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="progress-container progress-info">
                                        <span class="progress-badge">Medical</span>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                                <span class="progress-value">45%</span>
                                            </div>
                                        </div>
                                        <ul class="list-unstyled team-info">
                                            <li class="m-r-15"><small class="text-muted">Team</small></li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="progress-container progress-warning">
                                        <span class="progress-badge">Art & Design</span>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%;">
                                                <span class="progress-value">29%</span>
                                            </div>
                                        </div>
                                        <ul class="list-unstyled team-info">
                                            <li class="m-r-15"><small class="text-muted">Team</small></li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="http://via.placeholder.com/35x35" alt="Avatar">
                                            </li>                            
                                        </ul>
                                    </div>
                                </a>
                            </li>                    
                        </ul>
                    </li>
                    <li class="footer"><a href="javascript:void(0);">View All</a></li>
                </ul>
            </li>
            <!-- <li class="hidden-sm-down">
                <div class="input-group">                
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-addon">
                        <i class="zmdi zmdi-search"></i>
                    </span>
                </div>
            </li>         -->

            @if($login_info['admin_type'] == 3 )
            <li>
                @php
                    $updated_student_seesion    = get_current_student_session();
                    $updated_student_id = !empty($updated_student_seesion) ? $updated_student_seesion['student_id'] : null;     
                @endphp
                
                <div class="row" style="margin-top: 10px;">
                    <label class="field select col-md-12 student_names">
                        {!!Form::select('student_id',$parent_student,isset($updated_student_id) ? $updated_student_id : null, ['class' => 'form-control show-tick select_form1','id'=>'set_dashboard_student'])!!}
                        <i class="arrow double"></i>
                    </label>
                    <div class="clearfix"></div>
                </div>
                        
            </li>
            @endif


            <li class="float-right">
                <a href="javascript:void(0);" class="fullscreen hidden-sm-down" data-provide="fullscreen" data-close="true"><i class="zmdi zmdi-fullscreen"></i></a>
                <a href="{{ url('/admin-panel/logout') }}" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a>
                <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>
            </li>
        </ul>
    </nav>
<div class="mycustloading" style="display: none"><div class="mycustloadingBg"><i class="fa fa-spinner fa-spin"></i></div></div>

<script>
    $(document).ready(function () {
        $("#set_dashboard_student").on('change', function (e)
        {
            var student_id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/set-student-session')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'student_id': student_id,
                },
                success: function (response) {
                    location.reload();
                    alert(response.message);
                }
            });
        });
    });
</script>

@extends('admin-panel.layout.sidebar')
@yield('content')
@extends('admin-panel.layout.footer')