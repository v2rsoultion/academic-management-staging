@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_cupboard_shelf') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/cupboard-shelf/add-cupboard-shelf') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/library') !!}">{!! trans('language.menu_library') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_cupboard_shelf') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                         <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('book_cupboard_id', $listData['arr_cubboard'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'book_cupboard_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.cupboard_shelf_name'), 'id' => 'city_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="cupboard-shelf-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.cupboard_shelf_name')}}</th>
                                            <th>{{trans('language.cupboard_shelf_cup')}}</th>
                                            <th>{{trans('language.cupboard_shelf_capacity')}}</th>
                                            <th>{{trans('language.cupboard_shelf_detail')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#cupboard-shelf-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/cupboard-shelf/data')}}',
                data: function (d) {
                    d.book_cupboard_id   = $('select[name="book_cupboard_id"]').val();
                    d.name  = $('input[name="name"]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'book_cupboardshelf_name', name: 'book_cupboardshelf_name'},
                {data: 'cupboard_name', name: 'cupboard_name'},
                {data: 'book_cupboard_capacity', name: 'book_cupboard_capacity'},
                {data: 'details', name: 'details'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 5, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });


</script>
@endsection




