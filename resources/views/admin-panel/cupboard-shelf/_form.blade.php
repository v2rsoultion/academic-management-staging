@if(isset($cupboard_shelf['book_cupboardshelf_id']) && !empty($cupboard_shelf['book_cupboardshelf_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif


{!! Form::hidden('book_cupboardshelf_id',old('book_cupboardshelf_id',isset($cupboard_shelf['book_cupboardshelf_id']) ? $cupboard_shelf['book_cupboardshelf_id'] : ''),['class' => 'gui-input', 'id' => 'book_cupboardshelf_id', 'readonly' => 'true']) !!}
@if ($errors->any())
    <div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('cupboardshelf_name', old('cupboardshelf_name',isset($cupboard_shelf['book_cupboardshelf_name']) ? $cupboard_shelf['book_cupboardshelf_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_name'), 'id' => 'book_cupboardshelf_name']) !!}
        </div>
        
    </div>

    <div class="col-lg-4 col-md-4">
        <div class="form-group m-bottom-0">
            <lable class="from_one1">{!! trans('language.cupboard_shelf_cup') !!} <span class="red-text">*</span> :</lable>
            <label class=" field select" style="width: 100%">
                {!!Form::select('cupboard_name', $listData['arr_cubboard'],isset($cupboard_shelf['book_cupboard_id']) ? $cupboard_shelf['book_cupboard_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'cupboard_name'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>    

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_capacity') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::number('book_cupboard_capacity', old('book_cupboard_capacity',isset($cupboard_shelf['book_cupboard_capacity']) ? $cupboard_shelf['book_cupboard_capacity']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_capacity'), 'id' => 'book_cupboard_capacity']) !!}
        </div>
    </div>

    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_detail') !!} :</lable>
        <div class="form-group">
            
            {!! Form::textarea('book_cupboard_shelf_detail',old('book_cupboard_shelf_detail',isset($cupboard_shelf['book_cupboard_shelf_detail']) ? $cupboard_shelf['book_cupboard_shelf_detail']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.cupboard_shelf_detail'),'rows' => 3, 'cols' => 50)) !!}
        </div>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/library') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#cupboard-shelf-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                cupboardshelf_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                cupboard_name: {
                    required: true,
                },
                book_cupboard_capacity: {
                    required: true,
                    min:0,
                    digits: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>