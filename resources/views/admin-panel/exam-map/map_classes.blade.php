@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    td{
    padding: 14px 10px !important;
    }
    .checkbox{
    float: left;
    margin-right: 20px;
    margin-top: 20px;
    }
    .nav-tabs>.nav-item>.nav-link {
    min-width: 50px !important;
    margin-right: 5px !important;
    border: 1px solid #ccc !important;
    }
    #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 5px !important;
    line-height: 18px;
    }
    #tabingclass .nav-link:hover {
    background: #1f2f60 !important;
    color: #fff !important;
    }
    #tabingclass .nav-link:active {
    background: #1f2f60 !important;
    color: #fff !important;
    }
    .nav-tabs>.nav-item>.nav-link.active{
        font-size: 14px !important;
    }
    .idi .nav-tabs>.nav-item>.nav-link.active {
    background: #1f2f60 !important;
    color: #fff !important;
    }
    .modal-dialog {
    max-width: 550px !important;
    }
</style>
<!--  Main content here -->
<section class="content">
	<div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/map-exam-class-subjects') !!}">{!! trans('language.map_exam_class_subject') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12" id="bodypadd">
                <div class="tab-content">
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
							<div class="header">
                                <h2><strong>Exam:</strong> {!! $exam->exam_name !!}  </h2>
                            </div>
                            <div class="body form-gap" >
								@if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <div class="" >
									{!! Form::open(['files'=>TRUE,'id' => 'class-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}

									{!! Form::hidden('decrypted_exam_id',isset($decrypted_exam_id) ? $decrypted_exam_id : '',['class' => 'gui-input', 'id' => 'exam_id', 'readonly' => 'true']) !!}
									@if(!empty($map_data)) 
                                    <div class="body idi" id="tabingclass">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
											@php $counter = 1; @endphp
											@foreach($map_data as $class)
											@if ($counter == 1)
												@php $addClass = 'active show'; @endphp
											@else
												@php $addClass = ''; @endphp
											@endif
                                                 
                                            <li class="nav-item"><a class="nav-link {!!$addClass!!}" data-toggle="tab" href="#class{{$counter}}"> <b>{{$class['class_name']}}</b></a></li>
                                            @php $counter++; @endphp
											@endforeach
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content ">
											@php $counter1 = 1; @endphp
											@foreach($map_data as $class)
											@if ($counter1 == 1)
												@php $addClass = 'active show'; @endphp
											@else
												@php $addClass = ''; @endphp
											@endif
                                            
											<div role="tabpanel" class="tab-pane tabingclasss {!! $addClass !!}" id="class{!! $counter1 !!}">
											<div class="row clearfix">
												{!! Form::hidden('map_class['.$counter1.'][class_id]',$class['class_id'],['class' => 'gui-input', 'id' => 'class_id']) !!}
                                                {!! Form::hidden('map_class['.$counter1.'][section_id]',$class['section_id'],['class' => 'gui-input', 'id' => 'section_id']) !!}
												@foreach($class['all_subjects'] as $key => $subject)
												
												@php $subject_exist = 0; $check = ''; @endphp
												@if(in_array($key, $class['subject']))
													@php $subject_exist = 1; $check = "checked"; @endphp
												@endif

												{!! Form::hidden("map_class[$counter1][subjects][$key][exist]",$subject_exist,['class' => 'gui-input', 'id' => 'class_id']) !!}

												{!! Form::hidden("map_class[$counter1][subjects][$key][id]",$key,['class' => 'gui-input', 'id' => 'class_id']) !!}

												<div class="col-md-2 col-lg-2" style="margin-right: 20px;">
													<div class="form-group">
														<div class="checkbox">
															<input id="checkbox{{$counter1}}{{$key}}" name="map_class[{{$counter1}}][subjects][{{$key}}][subject_id]" value="{{$key}}" {!!$check !!} type="checkbox">
															<label for="checkbox{{$counter1}}{{$key}}">{!! $subject !!}</label>
														</div>
													</div>
												</div>
												@endforeach
                                                
                                            	</div>
												<div>
                                                    <button type="button"  onclick="remove_record({{$decrypted_exam_id}},{{$class['section_id']}})" class="btn btn-raised btn-danger custom_btn"><i class="fas fa-minus-circle" style="margin-top: 0px"></i> Section</button>
                                                </div>
											</div>
											
                                            @php $counter1++; @endphp
											@endforeach
                                        </div>
                                    </div>
									<hr>
									<div class="col-lg-2 padding-0">
										<button type="submit" class="btn btn-raised btn-primary" title="Save">Save
										</button>
									</div>
									@else 
										No record found !!
									@endif
									{!! Form::close() !!}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<script type="text/javascript">
  
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9][\s0-9a-zA-Z]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#class-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                class_id: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });


	function remove_record(exam_id,section_id){
        var r = confirm("Are you sure to remove this class ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/examination/remove-map-sections/')}}?exam_id="+exam_id+"&section_id="+section_id,
                success: function (data) {
                    $(".mycustloading").hide();
					location.reload();
                }
            });
        }
    }

</script>
<!-- Content end here  -->
@endsection