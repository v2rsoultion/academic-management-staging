@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! $page_title !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination') !!}">{!! trans('language.menu_examination') !!}</a></li>
                    <li class="breadcrumb-item">{!! $page_title !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                
                                {!! Form::hidden('select_exam_id','',['class' => 'gui-input', 'id' => 'select_exam_id']) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('section_id', $map['arr_section'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <div id="list-block" style="display: none">
                                    <hr />
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="map-table" style="width:100%">
                                            {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{!! trans('language.exam_name') !!}</th>
                                                    <th>{!! trans('language.subjects') !!}</th>
                                                    <th>{!! trans('language.manage_criteria') !!}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="subjectModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Subjects - Exam - <span id="exam_name"></span>  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body" style="min-height: 200px; padding-top: 0px;">
            <div id="form-block" style="display: block">
            {!! Form::open(['files'=>TRUE,'id' => 'info-form' , 'class'=>'form-horizontal']) !!}
                
                <p class="green" id="grade_success"></p>
                <div class="alert alert-danger" role="alert" id="error-block" style="display: none;"></div>
                <div class="alert alert-success" role="alert" id="success-block" style="display: none;"></div>
                <table class="table m-b-0 c_list" id="subject-table" width="100%">
                    <thead>
                        <tr>
                            <th>{{trans('language.s_no')}}</th>
                            <th>{!! trans('language.class_name') !!}</th>
                            <th>{!! trans('language.subject_name') !!}</th>
                            <th>{!! trans('language.marks_criteria') !!}</th>
                        
                        </tr>
                    </thead>
                </table>
                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2">
                        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'submit']) !!}
                    </div>
                    <div class="col-lg-3 col-md-3">
                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round ','name'=>'clear','data-dismiss'=>'modal']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#map-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/examination/exam-class-subject-report-data')}}',
                data: function (d) {
                    d.section_id = $('select[name=section_id]').val();
                    d.class_id = $('select[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'exam_name', name: 'exam_name'},
                {data: 'subject_count', name: 'subject_count'},
                {data: 'manage_criteria', name: 'manage_criteria'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            if(class_id == "" && section_id == ""){
                $("#list-block").hide();
            } else {
                $("#list-block").show();
            }
            e.preventDefault();
        });
    
        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    
    $(document).on('click','.exams',function(e){
        var exam_id = $(this).attr('exam-id');
        var exam_name = $(this).attr('exam-name');
        var class_name = $(this).attr('class-name');
        var section_name = $(this).attr('section-name');
        $('#subjectModel').modal('show');
        $('#select_exam_id').val(exam_id);
        $('#exam_name').html(exam_name);
        $('#class_name').html(class_name);
        $('#section_name').html(section_name);
        var exam_id = $("#select_exam_id").val();
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        var table1 = $('#subject-table').DataTable({
            //dom: 'Blfrtip',
            destroy: true,
            pageLength: 50,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{url('admin-panel/examination/exam-subjects')}}',
                data: function (d) {
                    d.exam_id = exam_id;
                    d.class_id = class_id;
                    d.section_id = section_id;
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'class_name', name: 'class_name'},
                {data: 'subject_name', name: 'subject_name'},
                {data: 'marks_criteria', name: 'marks_criteria'},
            ],
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "14%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "30%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        e.preventDefault();
    })
   
    $('#info-form').on('submit', function(e) { 
        var formData = $(this).serialize();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type     : "POST",
            url      : "{{url('admin-panel/examination/save-markcriteria-subject')}}",
            data     : formData,
            cache    : false,

            success  : function(data) {
                data = $.trim(data);
                console.log(data);
                if(data == "success"){
                    $("#success-block").html("Criteria successfully mapped !!");
                    $("#success-block").show();
                    $("#error-block").hide();

                } else {
                    $("#error-block").html("No record found !!");
                    $("#error-block").show();
                    $("#success-block").hide();
                }
            }
        })
        e.preventDefault();
    });
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='section_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function checkSearch(){
        var class_id = $("#class_id").val();
        var section_id = $("#section_id").val();
        if(class_id != '' && section_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#form-block").hide();
            $("#Search").prop('disabled', true);
        }
    }
    $('#subjectModel').on('hidden.bs.modal', function () {
        location.reload();
    })
</script>
@endsection