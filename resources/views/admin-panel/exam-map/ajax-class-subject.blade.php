
@if(!empty($subjects))
  @foreach($subjects as $key => $value)
    <div class="col-md-2 col-lg-2" style="margin-right: 20px;">
      <div class="form-group">
        <div class="checkbox">
        <input id="checkbox{{$counter}}{{$key}}" type="checkbox" value="{{$key}}" required name="addClasses[{{$counter}}][subjects][]">
        <label for="checkbox{{$counter}}{{$key}}">{{ $value }}</label>
      </div>
      </div>
    </div>
  @endforeach
@endif
<hr />