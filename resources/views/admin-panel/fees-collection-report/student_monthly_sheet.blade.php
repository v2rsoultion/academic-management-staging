@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.student_monthly') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection-report') !!}">{!! trans('language.menu_fees_collection_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.student_monthly') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal', 'url' =>$save_url]) !!}
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange' => 'getStudent(this.value);checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('student_id', $map['arr_student'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_id'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search', 'disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        <a href="student-monthly-sheet" class="btn btn-raised btn-round btn-primary" name="Clear">Clear</a>
                                        
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <div id="grid-block"> 
                                <div class="table-responsive" style="margin-top: 30px;" >
                                    @if(!empty($final_sheet_record) && !empty($sessionPeriodArr))
                                    <table class="table m-b-0 c_list table-bordered" id="class-monthly-table" style="width:100%;">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <!-- <th rowspan=2>{{trans('language.s_no')}}</th> -->
                                                <th rowspan=2>{{trans('language.student')}}</th>
                                                @if(!empty($sessionPeriodArr))
                                                @foreach($sessionPeriodArr as $session_list)
                                                    <th class="text-center" colspan="3">{!! $session_list['month_year_string'] !!} </th>
                                                @endforeach
                                                @endif
                                            </tr>
                                            <tr>
                                                @foreach($sessionPeriodArr as $session_list)
                                                    <td>Total Paid</td>
                                                    <td>Total Due</td>
                                                    <td>Concession</td>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($final_sheet_record))
                                                @foreach($final_sheet_record as $key => $record)
                                            <tr>
                                                <!-- <td style="width: 30px !important;">{!! $key !!}</td> -->
                                                <td>{!! $record['student_name'] !!}</td>
                                                @foreach($final_sheet_record[$key]['fees_data'] as $value)
                                                    @if($value['total_paid_amount'] != "")
                                                        <td>{!! $value['total_paid_amount'] !!}</td>
                                                    @else
                                                        <td>0.00</td>
                                                    @endif
                                                    @if($value['total_dues_amount'] != "")
                                                        <td>{!! $value['total_dues_amount'] !!}</td>
                                                    @else
                                                        <td>0.00</td>
                                                    @endif
                                                    @if($value['total_concession_amount'] != "")
                                                        <td>{!! $value['total_concession_amount'] !!}</td>
                                                    @else
                                                        <td>0.00</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    @else
                                        <div class="alert alert-danger" role="alert">
                                            No Record found.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    
                                    @endif
                                    
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    function getStudent(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-student-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='student_id'").html(data.options);
                    $(".mycustloading").hide();
                  
                }
            });
        } else {
            $("select[name='student_id'").html('');
            $(".mycustloading").hide();
        }
    }

    function checkSearch(){
        var class_id = $("#class_id").val();
        if(class_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
</script>
@endsection