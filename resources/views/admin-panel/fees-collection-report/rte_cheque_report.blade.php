@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.rte_cheque') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection-report') !!}">{!! trans('language.menu_fees_collection_report') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.rte_cheque') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ session()->get('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                <!-- <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3">
                                        <label class=" field select" style="width: 100%">
                                        {!!Form::select('class_id', $map['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'checkSearch()'])!!}
                                        <i class="arrow double"></i>
                                        </label>
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search','id'=>'Search','disabled']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-1">
                                        {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                    </div>
                                </div> -->
                                {!! Form::close() !!}

                                <div class="table-responsive" style="margin-top:10px;">
                                    <table class="table m-b-0 c_list" id="fees-report" style="width:100%">
                                        {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.bank_name')}}</th>
                                                <th>{!! trans('language.cheque_date') !!}</th>
                                                <th>{!! trans('language.cheque_no') !!}</th>
                                                <th>{!! trans('language.cheque_amount') !!}</th>
                                                <th>{!! trans('language.status') !!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content end here  -->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#fees-report').DataTable({
            dom: 'Blfrtip',
            destroy: true,
            pageLength: 20,
            processing: true,
            serverSide: true,
            searching:false,
            bLengthChange: false,
            buttons: [
                'csv', 'print'
            ],
            ajax: {
                url: '{{url('admin-panel/fees-collection/rte-cheques-report-data')}}',
                data: function (d) {
                    // d.class_id = $('select[name=class_id]').val();
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex' },
                {data: 'bank_name', name: 'bank_name' },
                {data: 'rte_cheque_date', name: 'rte_cheque_date'},
                {data: 'rte_cheque_no', name: 'rte_cheque_no'},
                {data: 'rte_cheque_amt', name: 'rte_cheque_amt'},
                {data: 'rte_cheque_status', name: 'rte_cheque_status'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            location.reload();
        });
    });


    function checkSearch(){
        var class_id = $("#class_id").val();
        if(class_id != ''){
            $("#Search").prop('disabled', false);
        } else {
            $("#Search").prop('disabled', true);
        }
    }
</script>
@endsection