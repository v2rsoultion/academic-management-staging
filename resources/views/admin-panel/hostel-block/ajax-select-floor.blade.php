<option value="">Select Floor</option>
@if(!empty($floor))
  @foreach($floor as $key => $value)
    <option value="{{ $key }}" {{ ($key == $hostel_floor_id) ? 'selected' : '' }}>{{ $value }}</option>
  @endforeach
@endif