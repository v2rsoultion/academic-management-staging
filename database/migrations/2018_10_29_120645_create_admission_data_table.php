<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'admission_data';
    protected $primaryKey = 'admission_data_id';
    public function up()
    {
        if (!Schema::hasTable('admission_data')) { 
            Schema::create('admission_data', function (Blueprint $table) {
                $table->increments('admission_data_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('admission_form_id')->unsigned()->nullable();
                $table->tinyInteger('form_type')->default(0)->comment = '0=Admission,1=Enquiry';
                $table->string('form_number')->nullable(0);

                $table->string('student_enroll_number', 255)->nullable();
                $table->string('student_roll_no', 255)->nullable();
                $table->string('student_name', 255)->nullable();
                $table->text('student_image')->nullable();
                $table->date('student_reg_date')->nullable();
                $table->date('student_dob')->nullable();
                $table->string('student_email', 255)->nullable();
                $table->tinyInteger('student_gender')->default(0)->comment = '0=Boy,1=Girl';
                $table->tinyInteger('student_type')->default(0)->comment = '0=PaidBoy,1=RTE,2=FREE';
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                
                $table->string('student_category', 255)->nullable();
                $table->integer('title_id')->unsigned()->nullable();
                $table->integer('caste_id')->unsigned()->nullable();
                $table->integer('religion_id')->unsigned()->nullable();
                $table->integer('nationality_id')->unsigned()->nullable();
                $table->string('student_sibling_name', 255)->nullable();
                $table->integer('student_sibling_class_id')->unsigned()->nullable();
                $table->string('student_adhar_card_number', 255)->nullable();

                $table->string('student_privious_school',255)->nullable();
                $table->string('student_privious_class',255)->nullable();
                $table->string('student_privious_tc_no',255)->nullable();
                $table->date('student_privious_tc_date')->nullable();
                $table->text('student_privious_result')->nullable();

                $table->text('student_temporary_address')->nullable();
                $table->integer('student_temporary_city')->unsigned()->nullable();
                $table->integer('student_temporary_state')->unsigned()->nullable();
                $table->integer('student_temporary_county')->unsigned()->nullable();
                $table->string('student_temporary_pincode', 20)->nullable();

                $table->text('student_permanent_address')->nullable();
                $table->integer('student_permanent_city')->unsigned()->nullable();
                $table->integer('student_permanent_state')->unsigned()->nullable();
                $table->integer('student_permanent_county')->unsigned()->nullable();
                $table->string('student_permanent_pincode', 20)->nullable();

                $table->integer('admission_session_id')->unsigned()->nullable();
                $table->integer('admission_class_id')->unsigned()->nullable();
                $table->integer('admission_section_id')->unsigned()->nullable();
                $table->integer('current_session_id')->unsigned()->nullable();
                $table->integer('current_class_id')->unsigned()->nullable();
                $table->integer('current_section_id')->unsigned()->nullable();
                $table->string('student_unique_id', 255)->nullable();
                $table->integer('group_id')->unsigned()->nullable();
                $table->integer('stream_id')->unsigned()->nullable();

                $table->string('student_height',255)->nullable();
                $table->string('student_weight',255)->nullable();
                $table->string('student_blood_group',255)->nullable();
                $table->text('student_vision_left')->nullable();
                $table->text('student_vision_right')->nullable();
                $table->longText('medical_issues')->nullable();

                $table->string('student_login_name', 255)->nullable();
                $table->string('student_login_email', 255)->nullable();
                $table->string('student_login_contact_no', 255)->nullable();

                $table->string('student_father_name', 255)->nullable();
                $table->string('student_father_mobile_number', 255)->nullable();
                $table->string('student_father_email', 255)->nullable();
                $table->string('student_father_occupation', 255)->nullable();
                $table->string('student_father_annual_salary',255)->nullable();
                $table->string('student_mother_name', 255)->nullable();
                $table->string('student_mother_mobile_number', 255)->nullable();
                $table->string('student_mother_email', 255)->nullable();
                $table->string('student_mother_occupation', 255)->nullable();
                $table->string('student_mother_annual_salary',255)->nullable();
                $table->text('student_mother_tongue')->nullable();
                $table->string('student_guardian_name', 255)->nullable();
                $table->string('student_guardian_email', 255)->nullable();
                $table->string('student_guardian_mobile_number', 255)->nullable();
                $table->string('student_guardian_relation')->nullable();

                $table->tinyInteger('admission_data_mode')->default(1)->comment = '0=Offline,1=Online';
                $table->string('admission_data_pay_mode')->nullable();
                $table->tinyInteger('admission_data_fees')->default(0)->comment = '0=Pending,1=Paid';
                
                $table->integer('student_id')->unsigned()->nullable();
                $table->tinyInteger('admission_data_status')->default(1)->comment = '0=Pending,1=Contacted, 2=Admitted ';
                $table->timestamps();
            });

            Schema::table('admission_data', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('admission_form_id')->references('admission_form_id')->on('admission_forms');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('student_sibling_class_id')->references('class_id')->on('classes');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('title_id')->references('title_id')->on('titles');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('caste_id')->references('caste_id')->on('caste');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('religion_id')->references('religion_id')->on('religions');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('nationality_id')->references('nationality_id')->on('nationality');
            });

            Schema::table('admission_data', function($table) {
                $table->foreign('student_permanent_county')->references('country_id')->on('country');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('student_permanent_state')->references('state_id')->on('state');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('student_permanent_city')->references('city_id')->on('city');
            });

            Schema::table('admission_data', function($table) {
                $table->foreign('student_temporary_county')->references('country_id')->on('country');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('student_temporary_state')->references('state_id')->on('state');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('student_temporary_city')->references('city_id')->on('city');
            });

            Schema::table('admission_data', function($table) {
                $table->foreign('admission_class_id')->references('class_id')->on('classes');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('admission_section_id')->references('section_id')->on('sections');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('admission_session_id')->references('session_id')->on('sessions');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('current_session_id')->references('session_id')->on('sessions');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('current_class_id')->references('class_id')->on('classes');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('current_section_id')->references('section_id')->on('sections');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('group_id')->references('group_id')->on('groups');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('stream_id')->references('stream_id')->on('streams');
            });
            Schema::table('admission_data', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_data');
    }
}
