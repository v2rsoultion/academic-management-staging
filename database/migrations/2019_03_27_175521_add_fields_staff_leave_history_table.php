<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsStaffLeaveHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_leave_history_info', function (Blueprint $table) {
            $table->integer('staff_leave_id')->unsigned()->nullable();
        });
        Schema::table('staff_leave_history_info', function($table) {
            $table->foreign('staff_leave_id')->references('staff_leave_id')->on('staff_leaves');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_leave_history_info', function (Blueprint $table) {
            $table->dropColumn('staff_leave_id');
        });
    }
}
