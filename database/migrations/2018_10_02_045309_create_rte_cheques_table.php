<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRteChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'rte_cheques';
    protected $primaryKey = 'rte_cheque_id';
    public function up()
    {
        if (!Schema::hasTable('rte_cheques')) {
            Schema::create('rte_cheques', function (Blueprint $table) {
                $table->increments('rte_cheque_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('bank_id')->unsigned()->nullable();
                $table->text('rte_cheque_no')->nullable();
                $table->date('rte_cheque_date')->nullable();
                $table->text('rte_cheque_amt')->nullable();
                $table->tinyInteger('rte_cheque_status')->default(0)->comment = '0=Pending,1=Cancel,2=Cleared,3=Bounce';
                $table->timestamps();
            });
            Schema::table('rte_cheques', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('rte_cheques', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('rte_cheques', function($table) {
                $table->foreign('bank_id')->references('bank_id')->on('banks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rte_cheques');
    }
}
