<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_cheques';
    protected $primaryKey = 'student_cheque_id';
    public function up()
    {
        if (!Schema::hasTable('student_cheques')) {
            Schema::create('student_cheques', function (Blueprint $table) {
                $table->increments('student_cheque_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('bank_id')->unsigned()->nullable();
                $table->text('student_cheque_no')->nullable();
                $table->date('student_cheque_date')->nullable();
                $table->text('student_cheque_amt')->nullable();
                $table->tinyInteger('student_cheque_status')->default(0)->comment = '0=Pending,1=Cancel,2=Cleared,3=Bounce';
                $table->timestamps();
            });
            Schema::table('student_cheques', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_cheques', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_cheques', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('student_cheques', function($table) {
                $table->foreign('bank_id')->references('bank_id')->on('banks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_cheques');
    }
}
