<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldExamResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('st_exam_result', function (Blueprint $table) {
            $table->tinyInteger('cronjob_status')->default(0)->after('result_time')->comment = '0=Not Sent,1=Sent';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('st_exam_result', function (Blueprint $table) {
            $table->dropColumn('cronjob_status');
        });
    }
}
