<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTaskManagerResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('task_response')) {
            Schema::table('task_response', function (Blueprint $table) {
                $table->dropColumn('user_name');
                $table->integer('task_type')->unsigned()->nullable()->after('task_id')->comment = '0=Admin,1=Student,2=Staff';
                $table->integer('staff_student_id')->unsigned()->nullable()->after('task_type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
