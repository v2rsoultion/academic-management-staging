<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableMapStudentStaffVehicle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'map_student_staff_vehicle';
    protected $primaryKey = 'map_student_staff_id';
    
    public function up()
    {
        Schema::create('map_student_staff_vehicle', function (Blueprint $table) {
            $table->increments('map_student_staff_id');
            $table->integer('admin_id')->unsigned();
            $table->integer('update_by')->unsigned();
            $table->integer('vehicle_id')->unsigned();
            $table->integer('route_id')->unsigned();
            $table->integer('route_location_id')->unsigned();
            $table->integer('student_staff_id')->unsigned()->nullable();
            $table->integer('student_staff_type')->unsigned()->nullable()->comment = '1 = Student ,2 = Staff';;
            $table->timestamps();
        });


        Schema::table('map_student_staff_vehicle', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins');
        });

        Schema::table('map_student_staff_vehicle', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins');
        });

        Schema::table('map_student_staff_vehicle', function($table) {
            $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles');
        });

        Schema::table('map_student_staff_vehicle', function($table) {
            $table->foreign('route_id')->references('route_id')->on('routes');
        });
        Schema::table('map_student_staff_vehicle', function($table) {
            $table->foreign('route_location_id')->references('route_location_id')->on('route_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
