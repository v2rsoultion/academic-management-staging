<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualClassMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'virtual_class_map';
    protected $primaryKey = 'virtual_class_map_id';

    public function up()
    {
        if (!Schema::hasTable('virtual_class_map')) { 
            Schema::create('virtual_class_map', function (Blueprint $table) {
                $table->increments('virtual_class_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('virtual_class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('virtual_class_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('virtual_class_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('virtual_class_map', function($table) {
                $table->foreign('virtual_class_id')->references('virtual_class_id')->on('virtual_classes');
            });
            Schema::table('virtual_class_map', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_class_map');
    }
}
