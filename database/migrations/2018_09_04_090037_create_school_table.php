<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'school';
    protected $primaryKey = 'school_id';
    public function up()
    {
        if (!Schema::hasTable('school')) {
            Schema::create('school', function (Blueprint $table) {
                $table->increments('school_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('school_name', 255);
                $table->string('school_registration_no', 255)->nullable();
                $table->text('school_sno_numbers')->nullable()->comment = 'SNO numbers are stored with comma';
                $table->text('school_description')->nullable();
                $table->text('school_address')->nullable();
                $table->string('school_district', 255)->nullable();
                $table->integer('city_id')->unsigned()->nullable();
                $table->integer('state_id')->unsigned()->nullable();
                $table->integer('country_id')->unsigned()->nullable();
                $table->string('school_pincode', 10)->nullable();
                $table->text('school_medium')->nullable()->comment = 'Medium names are stored with comma';
                $table->text('school_board_of_exams')->nullable()->comment = 'Board names are stored with comma';
                $table->text('school_class_from')->nullable();
                $table->text('school_class_to')->nullable();
                $table->integer('school_total_students')->unsigned()->nullable();
                $table->integer('school_total_staff')->unsigned()->nullable();
                $table->double('school_fee_range_from',18, 2)->unsigned()->nullable();
                $table->double('school_fee_range_to',18, 2)->unsigned()->nullable();
                $table->text('school_facilities')->nullable()->comment = 'Facilities are stored with comma';
                $table->text('school_url')->nullable();
                $table->text('school_logo')->nullable();
                $table->string('school_email', 255)->nullable();
                $table->string('school_mobile_number', 20)->nullable();
                $table->string('school_telephone', 20)->nullable();
                $table->string('school_fax_number', 20)->nullable();
                $table->text('school_img1')->nullable();
                $table->text('school_img2')->nullable();
                $table->text('school_img3')->nullable();
                $table->text('school_img4')->nullable();
                $table->text('school_img5')->nullable();
                $table->text('school_img6')->nullable();
                $table->tinyInteger('imprest_ac_status')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('school_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('school', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('school', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('school', function($table) {
                $table->foreign('country_id')->references('country_id')->on('country');
            });
            Schema::table('school', function($table) {
                $table->foreign('state_id')->references('state_id')->on('state');
            });
            Schema::table('school', function($table) {
                $table->foreign('city_id')->references('city_id')->on('city');
            });

            DB::table('school')->insert(
                array(
                    'school_id'         => 1,
                    'admin_id'          => 1,
                    'update_by'         => 1,
                    'school_email'      => 'school@gmail.com',
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school');
    }
}
