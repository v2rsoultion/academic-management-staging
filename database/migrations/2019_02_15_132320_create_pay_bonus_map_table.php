<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayBonusMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_bonus_map';
    protected $primaryKey = 'pay_bonus_map_id';

    public function up()
    {
        if(!Schema::hasTable('pay_bonus_map')) {
            Schema::create('pay_bonus_map', function (Blueprint $table) {
                $table->increments('pay_bonus_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('pay_bonus_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('pay_bonus_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_bonus_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('pay_bonus_map', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('pay_bonus_map', function($table) {
                $table->foreign('pay_bonus_id')->references('pay_bonus_id')->on('pay_bonus');
            });

            Schema::table('pay_bonus_map', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_bonus_map');
    }
}
