<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'marks_criteria';
    protected $primaryKey = 'marks_criteria_id';
    public function up()
    {
        if (!Schema::hasTable('marks_criteria')) {
            Schema::create('marks_criteria', function (Blueprint $table) {
                $table->increments('marks_criteria_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('criteria_name',255)->nullable();
                $table->double('max_marks')->unsigned()->nullable();
                $table->double('passing_marks')->unsigned()->nullable();
                $table->tinyInteger('criteria_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('marks_criteria', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('marks_criteria', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks_criteria');
    }
}
