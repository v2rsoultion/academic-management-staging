<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletAcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   

    protected $table      = 'wallet_ac';
    protected $primaryKey = 'wallet_ac_id';
    public function up()
    {
        if (!Schema::hasTable('wallet_ac')) {
            Schema::create('wallet_ac', function (Blueprint $table) {
                $table->increments('wallet_ac_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->string('wallet_ac_name', 255)->nullable();
                $table->double('wallet_ac_amt')->nullable();
                $table->tinyInteger('wallet_ac_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('wallet_ac', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('wallet_ac', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('wallet_ac', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('wallet_ac', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('wallet_ac', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_ac');
    }
}
