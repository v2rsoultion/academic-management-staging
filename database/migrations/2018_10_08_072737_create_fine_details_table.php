<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFineDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'fine_details';
    protected $primaryKey = 'fine_detail_id';

    public function up()
    {
        if (!Schema::hasTable('fine_details')) {
            Schema::create('fine_details', function (Blueprint $table) {
                $table->increments('fine_detail_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('fine_id')->unsigned()->nullable();
                $table->string('fine_detail_days')->nullable();
                $table->double('fine_detail_amt',255)->nullable();
                $table->timestamps();
            });
            Schema::table('fine_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('fine_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('fine_details', function($table) {
                $table->foreign('fine_id')->references('fine_id')->on('fine');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fine_details');
    }
}
