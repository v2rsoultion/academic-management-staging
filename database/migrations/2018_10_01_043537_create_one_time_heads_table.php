<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOneTimeHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'one_time_heads';
    protected $primaryKey = 'ot_head_id';
    public function up()
    {
        if (!Schema::hasTable('one_time_heads')) {
            Schema::create('one_time_heads', function (Blueprint $table) {
                $table->increments('ot_head_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('ot_particular_name',255)->nullable();
                $table->text('ot_alias')->nullable();
                $table->text('ot_classes')->nullable()->comment = 'Classes ids are comma seperated store here';
                $table->double('ot_amount',18, 2)->unsigned()->nullable();
                $table->date('ot_date')->nullable();
                $table->tinyInteger('ot_refundable')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('for_students')->default(0)->comment = '0=New,1=Old,2=Both';
                $table->tinyInteger('deduct_imprest_status')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('receipt_status')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('ot_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('one_time_heads', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('one_time_heads', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('one_time_heads');
    }
}
