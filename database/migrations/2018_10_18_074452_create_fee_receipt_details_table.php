<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeReceiptDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'fee_receipt_details';
    protected $primaryKey = 'fee_receipt_detail_id';

    public function up()
    {
        if (!Schema::hasTable('fee_receipt_details')) {
            Schema::create('fee_receipt_details', function (Blueprint $table) {
                $table->increments('fee_receipt_detail_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('receipt_id')->unsigned()->nullable();
                $table->integer('head_id')->unsigned()->nullable();
                $table->string('head_type',255)->nullable();
                $table->integer('head_inst_id')->unsigned()->nullable();
                $table->decimal('inst_amt',10,2)->unsigned()->nullable()->comment = 'Total installment amount';
                $table->decimal('inst_paid_amt',10,2)->unsigned()->nullable()->comment = 'Total paid installment amount';
                $table->tinyInteger('inst_status')->default(1)->comment = '0=Less,1=complete';
                $table->integer('r_concession_map_id')->unsigned()->nullable();
                $table->decimal('r_concession_amt',10,2)->unsigned()->nullable()->comment = 'Concession amount';
                $table->integer('r_fine_id')->unsigned()->nullable();
                $table->integer('r_fine_detail_id')->unsigned()->nullable();
                $table->integer('r_fine_detail_days')->unsigned()->nullable();
                $table->decimal('r_fine_amt',10,2)->unsigned()->nullable()->comment = 'Fine amount';
                $table->tinyInteger('cancel_status')->default(0)->comment = '0=No,1=Yes';
                $table->timestamps();
            });

            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('receipt_id')->references('receipt_id')->on('fee_receipt');
            });
            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('head_inst_id')->references('rc_inst_id')->on('recurring_inst');
            });
            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('r_concession_map_id')->references('concession_map_id')->on('concession_map');
            });
            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('r_fine_id')->references('fine_id')->on('fine');
            });
            Schema::table('fee_receipt_details', function($table) {
                $table->foreign('r_fine_detail_id')->references('fine_detail_id')->on('fine_details');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_receipt_details');
    }
}
