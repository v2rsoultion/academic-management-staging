<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddFieldsFeeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fee_receipt_details', function (Blueprint $table) {
            $table->string('head_month_year',255)->nullable()->after('head_type');
            $table->integer('map_student_staff_id')->unsigned()->nullable();
        });

        Schema::table('fee_receipt_details', function($table) {
            $table->foreign('map_student_staff_id')->references('map_student_staff_id')->on('map_student_staff_vehicle');
        });

        Schema::table('map_student_staff_vehicle', function (Blueprint $table1) {
            $table1->integer('session_id')->unsigned()->nullable();
        });

        Schema::table('map_student_staff_vehicle', function($table1) {
            $table1->foreign('session_id')->references('session_id')->on('sessions');
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fee_receipt_details', function (Blueprint $table) {
            $table->dropForeign('fee_receipt_details_map_student_staff_id_foreign');
            $table->dropColumn('map_student_staff_id');
            $table->dropColumn('head_month_year');
        });

        Schema::table('map_student_staff_vehicle', function (Blueprint $table1) {
            $table1->dropForeign('map_student_staff_vehicle_session_id_foreign');
            $table1->dropColumn('session_id');
        });
    }
}
