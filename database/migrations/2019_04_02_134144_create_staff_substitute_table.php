<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffSubstituteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'staff_substitute';
    protected $primaryKey = 'substitute_id';

    public function up()
    {
        if(!Schema::hasTable('staff_substitute')) {
            Schema::create('staff_substitute', function (Blueprint $table) {
                $table->increments('substitute_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('ref_leave_id')->unsigned()->nullable();
                $table->integer('leave_staff_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('lecture_no')->unsigned()->nullable();
                $table->integer('day')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->date('from_date')->nullable();
                $table->date('to_date')->nullable();
                $table->tinyInteger('as_class_teacher')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('pay_extra')->default(1)->comment = '1=NA,2=Per Day, 3=Per Lecture';
                $table->timestamps();
            });

            Schema::table('staff_substitute', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('leave_staff_id')->references('staff_id')->on('staff');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('ref_leave_id')->references('staff_leave_id')->on('staff_leaves');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('staff_substitute', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_substitute');
    }
}
