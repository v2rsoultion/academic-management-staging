<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignDriverConductorRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'assign_driver_conductor_routes';
    protected $primaryKey = 'assign_id';
    public function up()
    {
        Schema::create('assign_driver_conductor_routes', function (Blueprint $table) {
            $table->increments('assign_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->integer('vehicle_id')->unsigned()->nullable();
            $table->integer('driver_id')->unsigned()->nullable();
            $table->integer('conductor_id')->unsigned()->nullable();
            $table->integer('route_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('assign_driver_conductor_routes', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins');
        });

        Schema::table('assign_driver_conductor_routes', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins');
        });

        Schema::table('assign_driver_conductor_routes', function($table) {
            $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles');
        });

        Schema::table('assign_driver_conductor_routes', function($table) {
            $table->foreign('driver_id')->references('staff_id')->on('staff');
        });

        Schema::table('assign_driver_conductor_routes', function($table) {
            $table->foreign('conductor_id')->references('staff_id')->on('staff');
        });

        Schema::table('assign_driver_conductor_routes', function($table) {
            $table->foreign('route_id')->references('route_id')->on('routes');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_driver_conductor_routes');
    }
}
