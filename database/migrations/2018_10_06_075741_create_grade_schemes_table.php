<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'grade_schemes';
    protected $primaryKey = 'grade_scheme_id';

    public function up()
    {
        if (!Schema::hasTable('grade_schemes')) {
            Schema::create('grade_schemes', function (Blueprint $table) {
                $table->increments('grade_scheme_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('scheme_name',255)->nullable();
                $table->tinyInteger('grade_type')->default(0)->comment = '0=Percent,1=Marks';
                $table->tinyInteger('scheme_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('grade_schemes', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('grade_schemes', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grade_schemes');
    }
}
