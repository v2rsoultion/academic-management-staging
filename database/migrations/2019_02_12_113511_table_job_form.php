<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableJobForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'job_forms';
    protected $primaryKey = 'job_form_id';


    public function up()
    {
        
        if (!Schema::hasTable('job_forms')) {
            Schema::create('job_forms', function (Blueprint $table) {
                $table->increments('job_form_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('form_name',255)->nullable();
                $table->integer('job_id')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->string('form_prefix',255)->nullable();
                $table->text('form_instruction')->nullable();
                $table->text('form_information')->nullable();
                $table->text('form_success_message')->nullable();
                $table->text('form_failed_message')->nullable();
                $table->tinyInteger('form_message_via')->default(0)->comment = '0=Email,1=Phone,2=Both';
                $table->tinyInteger('form_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('job_forms', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('job_forms', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('job_forms', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('job_forms', function($table) {
                $table->foreign('job_id')->references('job_id')->on('job');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_forms');
    }
}
