<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_attendance';
    protected $primaryKey = 'student_attendance_id';

    public function up()
    {
        if (!Schema::hasTable('student_attendance')) {
            Schema::create('student_attendance', function (Blueprint $table) {
                $table->increments('student_attendance_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('total_present_student')->unsigned()->nullable();
                $table->integer('total_absent_student')->unsigned()->nullable();
                $table->date('student_attendance_date')->nullable();
                $table->tinyInteger('student_attendance_status')->default(1)->comment = '0=Unmark,1=Mark';
                $table->timestamps();
            });
            Schema::table('student_attendance', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_attendance', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_attendance', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_attendance', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_attendance', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attendance');
    }
}
