<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'sections';
    protected $primaryKey = 'section_id';
    public function up()
    {
        if (!Schema::hasTable('sections')) { 
            Schema::create('sections', function (Blueprint $table) {
                $table->increments('section_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('shift_id')->unsigned()->nullable();
                $table->integer('stream_id')->unsigned()->nullable();
                $table->string('section_name', 255)->nullable();
                $table->integer('section_intake')->unsigned()->default(0)->comment = 'capacity/seats';
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->integer('section_order')->unsigned()->default(0);
                $table->tinyInteger('section_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('sections', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('sections', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('sections', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('sections', function($table) {
                $table->foreign('shift_id')->references('shift_id')->on('shifts');
            });
            Schema::table('sections', function($table) {
                $table->foreign('stream_id')->references('stream_id')->on('streams');
            });
            Schema::table('sections', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
