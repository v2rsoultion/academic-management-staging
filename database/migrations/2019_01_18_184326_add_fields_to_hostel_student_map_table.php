<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToHostelStudentMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hostel_student_map', function (Blueprint $table) {
            $table->text('h_fees_head_ids')->nullable()->comment = 'Hostel fees head ids are comma seperate stored';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hostel_student_map', function (Blueprint $table) {
            $table->dropColumn('h_fees_head_ids');
        });
    }
}
