<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStChequesUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_cheques', function (Blueprint $table) {
            $table->integer('cheques_session_id')->unsigned()->nullable();
            $table->integer('cheques_class_id')->unsigned()->nullable();
            $table->integer('cheques_receipt_id')->nullable();
        });

        Schema::table('student_cheques', function($table) {
            $table->foreign('cheques_session_id')->references('session_id')->on('sessions');
        });
        Schema::table('student_cheques', function($table) {
            $table->foreign('cheques_class_id')->references('class_id')->on('classes');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_cheques', function (Blueprint $table) {
            $table->dropForeign('student_cheques_cheques_session_id_foreign');
            $table->dropColumn('cheques_session_id');

            $table->dropForeign('student_cheques_cheques_class_id_foreign');
            $table->dropColumn('cheques_class_id');
            
            $table->dropColumn('cheques_receipt_id');
        });
    }
}
