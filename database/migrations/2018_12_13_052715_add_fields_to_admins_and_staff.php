<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAdminsAndStaff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function ($table) {
            $table->string('android_current_app_version',255)->nullable();
            $table->string('ios_current_app_version',255)->nullable();
        });

        Schema::table('staff', function ($table) {
            $table->text('shift_ids')->nullable();
            $table->tinyInteger('teaching_status')->default(0)->comment = '0=No,1=Yes';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('staff', function ($table) {
        //     $table->dropColumn('shift_ids');
        //     $table->dropColumn('teaching_status');
        // });

        Schema::table('admins', function ($table) {
            $table->dropColumn('android_current_app_version');
            $table->dropColumn('ios_current_app_version');
        });
    }
}
