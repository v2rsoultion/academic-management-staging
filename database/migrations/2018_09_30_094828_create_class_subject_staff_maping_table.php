<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSubjectStaffMapingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'class_subject_staff_map';
    protected $primaryKey = 'class_subject_staff_map_id';
    public function up()
    {
        if (!Schema::hasTable('class_subject_staff_map')) {
            Schema::create('class_subject_staff_map', function (Blueprint $table) {
                $table->increments('class_subject_staff_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('staff_ids')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('class_subject_staff_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('class_subject_staff_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('class_subject_staff_map', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('class_subject_staff_map', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_subject_staff_map');
    }
}
