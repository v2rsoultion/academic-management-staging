<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'student_leaves';
    protected $primaryKey = 'student_leave_id';
    public function up()
    {
        if (!Schema::hasTable('student_leaves')) {
            Schema::create('student_leaves', function (Blueprint $table) {
                $table->increments('student_leave_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->text('student_leave_reason')->nullable();
                $table->date('student_leave_from_date')->nullable();
                $table->date('student_leave_to_date')->nullable();
                $table->text('student_leave_attachment')->nullable();
                $table->tinyInteger('student_leave_status')->default(0)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('student_leaves', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_leaves', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_leaves', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_leaves');
    }
}
