<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToPfSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pf_setting', function (Blueprint $table) {
            $table->integer('pf_setting_status')->default(1)->comment = '0=Deactive,1=Active';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pf_setting', function (Blueprint $table) {
            $table->dropColumn('pf_setting_status');
        });
    }
}
