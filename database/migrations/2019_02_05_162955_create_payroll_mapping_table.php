<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = "payroll_mapping";
    protected $primaryKey = "payroll_mapping_id";

    public function up()
    {
        if(!Schema::hasTable('payroll_mapping')) {
            Schema::create('payroll_mapping', function (Blueprint $table) {
                $table->increments('payroll_mapping_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('pay_dept_id')->unsigned()->nullable();
                $table->integer('pay_grade_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('payroll_mapping', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('payroll_mapping', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            Schema::table('payroll_mapping', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });

            Schema::table('payroll_mapping', function($table) {
                $table->foreign('pay_dept_id')->references('pay_dept_id')->on('pay_dept');
            });

            Schema::table('payroll_mapping', function($table) {
                $table->foreign('pay_grade_id')->references('pay_grade_id')->on('pay_grade');
            });

            Schema::table('payroll_mapping', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_mapping');
    }
}
