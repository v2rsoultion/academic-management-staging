<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddFieldMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('exam_marks')) {
            Schema::table('exam_marks', function (Blueprint $table) {
                $table->tinyInteger('attend_flag')->default(1)->comment = '0=Absent,1=Present';
                $table->tinyInteger('marks_status')->default(0)->comment = '0=Not submit,1=Marks submit';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('exam_marks')) {
            Schema::table('exam_marks', function (Blueprint $table) {
                $table->dropColumn('attend_flag');
                $table->dropColumn('marks_status');
            });
        }
    }
}
