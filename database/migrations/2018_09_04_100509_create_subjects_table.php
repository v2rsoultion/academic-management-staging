<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'subjects';
    protected $primaryKey = 'subject_id';
    public function up()
    {
        if (!Schema::hasTable('subjects')) { 
            Schema::create('subjects', function (Blueprint $table) {
                $table->increments('subject_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('subject_name', 255)->nullable();
                $table->string('subject_code', 255)->nullable();
                $table->tinyInteger('subject_is_coscholastic')->default(0)->comment = '0=No,1=Yes';
                $table->text('class_ids')->nullable();
                $table->integer('co_scholastic_type_id')->unsigned()->nullable();
                $table->text('co_scholastic_subject_des')->nullable();
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->tinyInteger('subject_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('subjects', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('subjects', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('subjects', function($table) {
                $table->foreign('co_scholastic_type_id')->references('co_scholastic_type_id')->on('co_scholastic_types');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
