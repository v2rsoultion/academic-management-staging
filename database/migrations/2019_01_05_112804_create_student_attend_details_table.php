<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('student_attend_details')) {
            Schema::create('student_attend_details', function (Blueprint $table) {
                $table->increments('student_attend_d_id');
                $table->integer('student_attendance_id')->unsigned()->nullable();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->tinyInteger('student_attendance_type')->nullable()->comment = '0=Absent,1=Present';
                $table->date('student_attendance_date')->nullable();
                $table->timestamps();
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('student_attendance_id')->references('student_attendance_id')->on('student_attendance');
            });
            Schema::table('student_attend_details', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attend_details');
    }
}
