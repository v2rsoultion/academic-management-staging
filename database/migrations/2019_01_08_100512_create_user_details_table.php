<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'user_details';
    protected $primaryKey = 'user_details_id';
    public function up()
    {
        if(!Schema::hasTable('user_details')) {
            Schema::create('user_details', function (Blueprint $table) {
                $table->increments('user_details_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('task_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned();
                $table->integer('student_id')->unsigned();
                $table->tinyInteger('user_type')->nullable()->comment = '1=student,2=staff';
                $table->timestamps();
            });

            Schema::table('user_details', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('user_details', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });

            Schema::table('user_details', function($table) {
                $table->foreign('task_id')->references('task_id')->on('tasks')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
