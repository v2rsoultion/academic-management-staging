<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'time_tables';
    protected $primaryKey = 'time_table_id';
    public function up()
    {
        if (!Schema::hasTable('time_tables')) { 
            Schema::create('time_tables', function (Blueprint $table) {
                $table->increments('time_table_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->string('time_table_name', 255)->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('lecture_no')->unsigned()->nullable();
                $table->text('time_table_week_days')->nullable();
                $table->tinyInteger('time_table_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('time_tables', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('time_tables', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('time_tables', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('time_tables', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('time_tables', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_tables');
    }
}
