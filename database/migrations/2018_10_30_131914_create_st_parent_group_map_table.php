<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStParentGroupMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'st_parent_group_map';
    protected $primaryKey = 'st_parent_group_map_id';

    public function up()
    {
        if (!Schema::hasTable('st_parent_group_map')) {

            Schema::create('st_parent_group_map', function (Blueprint $table) {
                $table->increments('st_parent_group_map_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('st_parent_group_id')->unsigned()->nullable();
                $table->integer('group_member_id')->nullable();
                $table->tinyInteger('flag')->default(0)->comment = '0=student,1=parent';
                $table->timestamps();
            });

            Schema::table('st_parent_group_map', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('st_parent_group_map', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('st_parent_group_map', function($table) {
                $table->foreign('st_parent_group_id')->references('st_parent_group_id')->on('st_parent_groups');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_parent_group_map');
    }
}
