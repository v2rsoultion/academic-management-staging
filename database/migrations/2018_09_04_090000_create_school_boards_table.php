<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'school_boards';
    protected $primaryKey = 'board_id';
    public function up()
    {
        if (!Schema::hasTable('school_boards')) { 
            Schema::create('school_boards', function (Blueprint $table) {
                $table->increments('board_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('board_name', 255)->nullable();
                $table->tinyInteger('board_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('school_boards', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('school_boards', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });

            DB::table('school_boards')->insert(
                array(
                    array(
                        'board_id'          => 1,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'board_name'        => 'CBSE Board',
                        'board_status'      => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'board_id'          => 2,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'board_name'        => 'RBSE Board',
                        'board_status'      => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'board_id'          => 3,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'board_name'        => 'IB Board',
                        'board_status'      => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'board_id'          => 4,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'board_name'        => 'IGCSE Board',
                        'board_status'      => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'board_id'          => 5,
                        'admin_id'          => 1,
                        'update_by'         => 1,
                        'board_name'        => 'State Board',
                        'board_status'      => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_boards');
    }
}
