<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToHostelStudentMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hostel_student_map', function (Blueprint $table) {
            $table->integer('h_room_cate_id')->unsigned()->nullable()->after('h_floor_id');
        });
        Schema::table('hostel_student_map', function($table) {
            $table->foreign('h_room_cate_id')->references('h_room_cate_id')->on('hostel_room_cate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hostel_student_map', function (Blueprint $table) {
            $table->dropForeign('hostel_student_map_h_room_cate_id_foreign');
            $table->dropColumn('h_room_cate_id');
        });
    }
}
