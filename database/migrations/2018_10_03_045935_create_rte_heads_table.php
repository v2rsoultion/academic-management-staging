<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRteHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'rte_heads';
    protected $primaryKey = 'rte_head_id';

    public function up()
    {
        if (!Schema::hasTable('rte_heads')) {
            Schema::create('rte_heads', function (Blueprint $table) {
                $table->increments('rte_head_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('rte_head',255)->nullable();
                $table->double('rte_amount',18, 2)->unsigned()->nullable();
                $table->tinyInteger('rte_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('rte_heads', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('rte_heads', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rte_heads');
    }
}
