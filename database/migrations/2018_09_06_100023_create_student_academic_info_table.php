<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAcademicInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_academic_info';
    protected $primaryKey = 'student_academic_info_id';

    public function up()
    {
        if (!Schema::hasTable('student_academic_info')) { 
            Schema::create('student_academic_info', function (Blueprint $table) {
                $table->increments('student_academic_info_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('admission_session_id')->unsigned()->nullable();
                $table->integer('admission_class_id')->unsigned()->nullable();
                $table->integer('admission_section_id')->unsigned()->nullable();
                $table->integer('current_session_id')->unsigned()->nullable();
                $table->integer('current_class_id')->unsigned()->nullable();
                $table->integer('current_section_id')->unsigned()->nullable();
                $table->string('student_unique_id', 255)->nullable();
                $table->integer('group_id')->unsigned()->nullable();
                $table->integer('stream_id')->unsigned()->nullable();
                $table->timestamps();
            });

            Schema::table('student_academic_info', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('admission_class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('admission_section_id')->references('section_id')->on('sections');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('admission_session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('current_session_id')->references('session_id')->on('sessions');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('current_class_id')->references('class_id')->on('classes');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('current_section_id')->references('section_id')->on('sections');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('group_id')->references('group_id')->on('groups');
            });
            Schema::table('student_academic_info', function($table) {
                $table->foreign('stream_id')->references('stream_id')->on('streams');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_academic_info');
    }
}
