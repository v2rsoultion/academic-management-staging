<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'remarks';
    protected $primaryKey = 'remark_id';
    public function up()
    {
        if (!Schema::hasTable('remarks')) {
            Schema::create('remarks', function (Blueprint $table) {
                $table->increments('remark_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->text('remark_text')->nullable();
                $table->date('remark_date')->nullable();
                $table->timestamps();
            });

            Schema::table('remarks', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff');
            });
            Schema::table('remarks', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remarks');
    }
}
