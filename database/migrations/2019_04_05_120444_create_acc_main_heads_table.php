<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccMainHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'acc_main_heads';
    protected $primaryKey = 'acc_main_head_id';
    public function up()
    {
        if(!Schema::hasTable('acc_main_heads')) {
            Schema::create('acc_main_heads', function (Blueprint $table) {
                $table->increments('acc_main_head_id');
                $table->string('acc_main_head')->nullable();
                $table->tinyInteger('acc_main_head_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            DB::table('acc_main_heads')->insert(
                array(
                    array(
                        'acc_main_head_id'      => 1,
                        'acc_main_head'         => 'Assets',
                        'acc_main_head_status'  => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_main_head_id'      => 2,
                        'acc_main_head'         => 'Liabilities',
                        'acc_main_head_status'  => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_main_head_id'      => 3,
                        'acc_main_head'         => 'Income',
                        'acc_main_head_status'  => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ),
                    array(
                        'acc_main_head_id'      => 4,
                        'acc_main_head'         => 'Expenses',
                        'acc_main_head_status'  => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_main_heads');
    }
}
