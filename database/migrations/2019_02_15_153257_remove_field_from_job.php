<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldFromJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('job')) {
            Schema::table('job', function (Blueprint $table) {
                $table->dropColumn('job_durations');
            });
        }


        Schema::table('job', function (Blueprint $table) {
            $table->string('job_durations')->nullable()->after('job_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
