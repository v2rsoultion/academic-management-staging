<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = "pay_grade";
    protected $primaryKey = "pay_grade_id";

    public function up()
    {
        if(!Schema::hasTable('pay_grade')) {
            Schema::create('pay_grade', function (Blueprint $table) {
                $table->increments('pay_grade_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('grade_name',255)->nullable();
                $table->text('grade_description')->nullable();
                $table->tinyInteger('grade_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('pay_grade', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_grade', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_grade');
    }
}
