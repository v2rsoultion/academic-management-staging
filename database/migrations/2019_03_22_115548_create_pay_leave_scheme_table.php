<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayLeaveSchemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'pay_leave_scheme';
    protected $primaryKey = 'pay_leave_scheme_id';

    public function up()
    {
        if(!Schema::hasTable('pay_leave_scheme')) {
            Schema::create('pay_leave_scheme', function (Blueprint $table) {
                $table->increments('pay_leave_scheme_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('lscheme_name', 255)->nullable();
                $table->integer('lscheme_period')->default(1)->comment = '1=Monthly,2=Yearly';
                $table->integer('carry_forward_limit')->nullable();
                $table->integer('no_of_leaves')->nullable();
                $table->text('special_instruction')->nullable();
                $table->timestamps();
            });

            Schema::table('pay_leave_scheme', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });

            Schema::table('pay_leave_scheme', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_leave_scheme');
    }
}
