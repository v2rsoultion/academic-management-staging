<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'module_permissions';
    protected $primaryKey = 'module_permission_id';

    public function up()
    {
        if(!Schema::hasTable('module_permissions')) {
            Schema::create('module_permissions', function (Blueprint $table) {
                $table->increments('module_permission_id');
                $table->text('permissions')->nullable();
                $table->timestamps();
            });

            DB::table('module_permissions')->insert(
                array(
                    array(
                        'module_permission_id'  => 1,
                        'permissions'       => '1,2,3,4,5,6,7,8,9,10,11',
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_permissions');
    }
}
