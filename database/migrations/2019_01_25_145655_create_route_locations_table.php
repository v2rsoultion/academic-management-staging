<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'route_locations';
    protected $primaryKey = 'route_location_id';
    public function up()
    {
        Schema::create('route_locations', function (Blueprint $table) {
            $table->increments('route_location_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->integer('route_id')->unsigned()->nullable();
            $table->string('location_sequence', 255)->nullable();
            $table->string('location_name', 255)->nullable();
            $table->string('location_latitude', 255)->nullable();
            $table->string('location_logitude', 255)->nullable();
            $table->string('location_pickup_time', 255)->nullable();
            $table->string('location_drop_time', 255)->nullable();
            $table->string('location_km_distance', 255)->nullable();
            $table->timestamps();
        });

        Schema::table('route_locations', function($table) {
            $table->foreign('admin_id')->references('admin_id')->on('admins');
        });

        Schema::table('route_locations', function($table) {
            $table->foreign('update_by')->references('admin_id')->on('admins');
        });

        Schema::table('route_locations', function($table) {
            $table->foreign('route_id')->references('route_id')->on('routes');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_locations');
    }
}
