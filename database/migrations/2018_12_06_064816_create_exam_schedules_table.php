<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'exam_schedules';
    protected $primaryKey = 'exam_schedule_id';

    public function up()
    {
        if (!Schema::hasTable('exam_schedules')) {
            Schema::create('exam_schedules', function (Blueprint $table) {
                $table->increments('exam_schedule_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->text('schedule_name')->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('exam_schedules', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('exam_schedules', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('exam_schedules', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('exam_schedules', function($table) {
                $table->foreign('exam_id')->references('exam_id')->on('exams');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_schedules');
    }
}
