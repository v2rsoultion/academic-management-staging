<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'recurring_heads';
    protected $primaryKey = 'rc_head_id';
    public function up()
    {
        if (!Schema::hasTable('recurring_heads')) {
            Schema::create('recurring_heads', function (Blueprint $table) {
                $table->increments('rc_head_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('rc_particular_name',255)->nullable();
                $table->text('rc_alias')->nullable();
                $table->text('rc_classes')->nullable()->comment = 'Classes ids are comma seperated store here';
                $table->double('rc_amount',18, 2)->unsigned()->nullable();
                $table->tinyInteger('rc_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('recurring_heads', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('recurring_heads', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurring_heads');
    }
}
