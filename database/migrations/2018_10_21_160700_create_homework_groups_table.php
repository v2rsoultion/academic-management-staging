<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeworkGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'homework_groups';
    protected $primaryKey = 'homework_group_id';
    public function up()
    {
        if (!Schema::hasTable('homework_groups')) {
            Schema::create('homework_groups', function (Blueprint $table) {
                $table->increments('homework_group_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->string('homework_group_name',255)->nullable();
                $table->timestamps();
            });
            Schema::table('homework_groups', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('homework_groups', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('homework_groups', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('homework_groups', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homework_groups');
    }
}
