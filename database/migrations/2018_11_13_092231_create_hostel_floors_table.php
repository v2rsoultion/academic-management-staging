<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelFloorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'hostel_floors';
    protected $primaryKey = 'h_floor_id';

    public function up()
    {
        if (!Schema::hasTable('hostel_floors')) {
            Schema::create('hostel_floors', function (Blueprint $table) {
                $table->increments('h_floor_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('h_block_id')->unsigned()->nullable();
                $table->string('h_floor_no', 255)->nullable();
                $table->integer('h_floor_rooms')->unsigned()->nullable();
                $table->text('h_floor_description')->nullable();
                $table->timestamps();
            });

            Schema::table('hostel_floors', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_floors', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('hostel_floors', function($table) {
                $table->foreign('h_block_id')->references('h_block_id')->on('hostel_blocks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostel_floors');
    }
}
