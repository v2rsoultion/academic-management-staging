<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER update_student_counter AFTER INSERT ON `students` FOR EACH ROW
            BEGIN
            UPDATE `school` 
            SET `school_total_students` = (SELECT COUNT(*) FROM `students` where student_status = 1);
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `update_student_counter`');
    }
}
