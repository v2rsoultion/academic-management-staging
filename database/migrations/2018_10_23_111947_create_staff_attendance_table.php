<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'staff_attendance';
    protected $primaryKey = 'staff_attendance_id';

    public function up()
    {
        if (!Schema::hasTable('staff_attendance')) {
            Schema::create('staff_attendance', function (Blueprint $table) {
                $table->increments('staff_attendance_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('total_present_staff')->unsigned()->nullable();
                $table->integer('total_absent_staff')->unsigned()->nullable();
                $table->integer('total_halfday_staff')->unsigned()->nullable();
                $table->date('staff_attendance_date')->nullable();
                $table->tinyInteger('staff_attendance_status')->default(1)->comment = '0=Unmark,1=Mark';
                $table->timestamps();
            });
            Schema::table('staff_attendance', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('staff_attendance', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('staff_attendance', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_attendance');
    }
}
