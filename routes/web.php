<?php
error_reporting(0);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group(['prefix' => 'admin-panel','middleware' => 'admin.guest'], function () {
  Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}/{username}', 'AdminAuth\ResetPasswordController@showResetForm1');
});

Route::group(['prefix' => 'admin-panel','middleware' => 'admin'], function () {

  Route::get('/unauthorized', 'AdminPanel\DashboardController@unauthorized'); // Route for unauthorized
  Route::get('/dashboard', 'AdminPanel\DashboardController@index');
  Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::post('/academic-year/set-academic-year', 'AdminPanel\SessionController@setSession');

  Route::group(['middleware' => 'App\Http\Middleware\SchoolMiddleware'], function(){

    // ============= For Configuration menu ============= 
    Route::get('/configuration', 'AdminPanel\MenuController@configuration');

    Route::get('/school/add-school/{id?}', 'AdminPanel\SchoolController@add'); // For School
    Route::post('/school/add-school/save/{id?}', 'AdminPanel\SchoolController@save');
    Route::get('/school/view-school-detail/{id?}', 'AdminPanel\SchoolController@profile');
    Route::get('/school/setting/{id?}', 'AdminPanel\SchoolController@show_setting');
    Route::post('/school/setting/save/{id?}', 'AdminPanel\SchoolController@save_setting');

    Route::get('/academic-year/add-academic-year/{id?}', 'AdminPanel\SessionController@add'); // For Academic Year
    Route::post('/academic-year/save/{id?}', 'AdminPanel\SessionController@save');
    Route::get('/academic-year/view-academic-years', 'AdminPanel\SessionController@index');
    Route::get('/academic-year/data', 'AdminPanel\SessionController@anyData');
    Route::get('/academic-year/delete-academic-year/{id?}', 'AdminPanel\SessionController@destroy');
    Route::get('/academic-year/academic-year-status/{status?}/{id?}', 'AdminPanel\SessionController@changeStatus');

    Route::get('/holidays/add-holiday/{id?}', 'AdminPanel\HolidayController@add'); // For Holidays
    Route::post('/holidays/save/{id?}', 'AdminPanel\HolidayController@save');
    Route::get('/holidays/view-holidays', 'AdminPanel\HolidayController@index');
    Route::get('/holidays/data', 'AdminPanel\HolidayController@anyData');
    Route::get('/holidays/delete-holiday/{id?}', 'AdminPanel\HolidayController@destroy');
    Route::get('/holidays/holiday-status/{status?}/{id?}', 'AdminPanel\HolidayController@changeStatus');

    Route::get('/shift/add-shift/{id?}', 'AdminPanel\ShiftController@add'); // For shifts
    Route::post('/shift/save/{id?}', 'AdminPanel\ShiftController@save');
    Route::get('/shift/view-shifts', 'AdminPanel\ShiftController@index');
    Route::get('/shift/data', 'AdminPanel\ShiftController@anyData');
    Route::get('/shift/delete-shift/{id?}', 'AdminPanel\ShiftController@destroy');
    Route::get('/shift/shift-status/{status?}/{id?}', 'AdminPanel\ShiftController@changeStatus');
    
    Route::get('/facilities/add-facility/{id?}', 'AdminPanel\FacilityController@add'); // For facilities @Pratyush on 19 July 2018
    Route::post('/facilities/save/{id?}', 'AdminPanel\FacilityController@save');
    Route::get('/facilities/view-facilities', 'AdminPanel\FacilityController@index');
    Route::get('/facilities/data', 'AdminPanel\FacilityController@anyData');
    Route::get('/facilities/delete-facility/{id?}', 'AdminPanel\FacilityController@destroy');
    Route::get('/facilities/facility-status/{status?}/{id?}', 'AdminPanel\FacilityController@changeStatus');

    Route::get('/document-category/add-document-category/{id?}', 'AdminPanel\DocumentCategoryController@add'); // For document category @Pratyush on 19 July 2018
    Route::post('/document-category/save/{id?}', 'AdminPanel\DocumentCategoryController@save');
    Route::get('/document-category/view-document-categories', 'AdminPanel\DocumentCategoryController@index');
    Route::get('/document-category/data', 'AdminPanel\DocumentCategoryController@anyData');
    Route::get('/document-category/delete-document-category/{id?}', 'AdminPanel\DocumentCategoryController@destroy');
    Route::get('/document-category/document-categories-status/{status?}/{id?}', 'AdminPanel\DocumentCategoryController@changeStatus');

    Route::get('/designation/add-designation/{id?}', 'AdminPanel\DesignationController@add'); // For designation @Pratyush on 19 July 2018
    Route::post('/designation/save/{id?}', 'AdminPanel\DesignationController@save');
    Route::get('/designation/view-designations', 'AdminPanel\DesignationController@index');
    Route::get('/designation/data', 'AdminPanel\DesignationController@anyData');
    Route::get('/designation/delete-designation/{id?}', 'AdminPanel\DesignationController@destroy');
    Route::get('/designation/designation-status/{status?}/{id?}', 'AdminPanel\DesignationController@changeStatus');

    Route::get('/title/add-title/{id?}', 'AdminPanel\TitleController@add'); // For Title @Pratyush on 20 July 2018
    Route::post('/title/save/{id?}', 'AdminPanel\TitleController@save');
    Route::get('/title/view-titles', 'AdminPanel\TitleController@index');
    Route::get('/title/data', 'AdminPanel\TitleController@anyData');
    Route::get('/title/delete-title/{id?}', 'AdminPanel\TitleController@destroy');
    Route::get('/title/title-status/{status?}/{id?}', 'AdminPanel\TitleController@changeStatus');

    Route::get('/caste/add-caste/{id?}', 'AdminPanel\CasteController@add'); // For Caste  @Pratyush on 20 July 2018
    Route::post('/caste/save/{id?}', 'AdminPanel\CasteController@save');
    Route::get('/caste/view-caste', 'AdminPanel\CasteController@index');
    Route::get('/caste/data', 'AdminPanel\CasteController@anyData');
    Route::get('/caste/delete-caste/{id?}', 'AdminPanel\CasteController@destroy');
    Route::get('/caste/caste-status/{status?}/{id?}', 'AdminPanel\CasteController@changeStatus');

    Route::get('/religion/add-religion/{id?}', 'AdminPanel\ReligionController@add'); // For Religions  @Pratyush on 20 July 2018
    Route::post('/religion/save/{id?}', 'AdminPanel\ReligionController@save');
    Route::get('/religion/view-religions', 'AdminPanel\ReligionController@index');
    Route::get('/religion/data', 'AdminPanel\ReligionController@anyData');
    Route::get('/religion/delete-religion/{id?}', 'AdminPanel\ReligionController@destroy');
    Route::get('/religion/religion-status/{status?}/{id?}', 'AdminPanel\ReligionController@changeStatus');
    
    Route::get('/nationality/add-nationality/{id?}', 'AdminPanel\NationalityController@add'); // For Nationality @Pratyush on 20 July 2018
    Route::post('/nationality/save/{id?}', 'AdminPanel\NationalityController@save');
    Route::get('/nationality/view-nationality', 'AdminPanel\NationalityController@index');
    Route::get('/nationality/data', 'AdminPanel\NationalityController@anyData');
    Route::get('/nationality/delete-nationality/{id?}', 'AdminPanel\NationalityController@destroy');
    Route::get('/nationality/nationality-status/{status?}/{id?}', 'AdminPanel\NationalityController@changeStatus');

    Route::get('/schoolgroup/add-group/{id?}', 'AdminPanel\SchoolGroupController@add'); // For School Group @Pratyush on 20 July 2018
    Route::post('/schoolgroup/save/{id?}', 'AdminPanel\SchoolGroupController@save');
    Route::get('/schoolgroup/view-groups', 'AdminPanel\SchoolGroupController@index');
    Route::get('/schoolgroup/data', 'AdminPanel\SchoolGroupController@anyData');
    Route::get('/schoolgroup/delete-group/{id?}', 'AdminPanel\SchoolGroupController@destroy');
    Route::get('/schoolgroup/group-status/{status?}/{id?}', 'AdminPanel\SchoolGroupController@changeStatus');

    Route::get('/room-no/add-room-no/{id?}', 'AdminPanel\RoomNoController@add'); // For Room No @Pratyush on 28 July 2018
    Route::post('/room-no/save/{id?}', 'AdminPanel\RoomNoController@save');
    Route::get('/room-no/view-room-no', 'AdminPanel\RoomNoController@index');
    Route::get('/room-no/data', 'AdminPanel\RoomNoController@anyData');
    Route::get('/room-no/delete-room-no/{id?}', 'AdminPanel\RoomNoController@destroy');
    Route::get('/room-no/room-no-status/{status?}/{id?}', 'AdminPanel\RoomNoController@changeStatus');

    Route::get('/stream/add-stream/{id?}', 'AdminPanel\StreamController@add'); // For Stream @Pratyush on 11 Aug 2018
    Route::post('/stream/save/{id?}', 'AdminPanel\StreamController@save');
    Route::get('/stream/view-streams', 'AdminPanel\StreamController@index');
    Route::get('/stream/data', 'AdminPanel\StreamController@anyData');
    Route::get('/stream/delete-stream/{id?}', 'AdminPanel\StreamController@destroy');
    Route::get('/stream/stream-status/{status?}/{id?}', 'AdminPanel\StreamController@changeStatus'); 

    Route::get('/school-board/add-school-board/{id?}', 'AdminPanel\BoardController@add');  // For School Boards  @shree on 29 Sept 2018
    Route::post('/school-board/save/{id?}', 'AdminPanel\BoardController@save');
    Route::get('/school-board/view-school-boards', 'AdminPanel\BoardController@index');
    Route::get('/school-board/data', 'AdminPanel\BoardController@anyData');
    Route::get('/school-board/delete-school-board/{id?}', 'AdminPanel\BoardController@destroy');
    Route::get('/school-board/school-board-status/{status?}/{id?}', 'AdminPanel\BoardController@changeStatus'); 

    Route::get('/country/add-country/{id?}', 'AdminPanel\CountryController@add'); //For Country @Khushbu on 08 Sept 2018
    Route::post('/country/save/{id?}', 'AdminPanel\CountryController@save');
    Route::get('/country/view-country', 'AdminPanel\CountryController@index');
    Route::get('/country/data', 'AdminPanel\CountryController@anyData');
    Route::get('/country/country-status/{status?}/{id?}', 'AdminPanel\CountryController@changeStatus');
    Route::get('/country/delete-country/{id?}', 'AdminPanel\CountryController@destroy');

    Route::get('/state/add-state/{id?}', 'AdminPanel\StateController@add'); //For State @Khushbu on 08 Sept 2018
    Route::post('/state/save/{id?}', 'AdminPanel\StateController@save');
    Route::get('/state/view-state', 'AdminPanel\StateController@index');
    Route::get('/state/data', 'AdminPanel\StateController@anyData');
    Route::get('/state/state-status/{status?}/{id?}', 'AdminPanel\StateController@changeStatus');
    Route::get('/state/delete-state/{id?}', 'AdminPanel\StateController@destroy');

    Route::get('/city/add-city/{id?}', 'AdminPanel\CityController@add'); //For City @Khushbu on 08 Sept 2018
    Route::post('/city/save/{id?}', 'AdminPanel\CityController@save');
    Route::get('/city/view-city', 'AdminPanel\CityController@index');
    Route::get('/city/data', 'AdminPanel\CityController@anyData');
    Route::get('/city/city-status/{status?}/{id?}', 'AdminPanel\CityController@changeStatus');
    Route::get('/city/delete-city/{id?}', 'AdminPanel\CityController@destroy');
    
    // ============= For Academic menu  =============
    Route::get('/academic', 'AdminPanel\MenuController@academic');

    Route::get('/mediums/add-medium/{id?}', 'AdminPanel\MediumsController@add'); // For Mediums 
    Route::post('/mediums/save/{id?}', 'AdminPanel\MediumsController@save');
    Route::get('/mediums/view-mediums', 'AdminPanel\MediumsController@index');
    Route::get('/mediums/data', 'AdminPanel\MediumsController@anyData');
    Route::get('/mediums/delete-medium/{id?}', 'AdminPanel\MediumsController@destroy');
    Route::get('/mediums/medium-status/{status?}/{id?}', 'AdminPanel\MediumsController@changeStatus');

    Route::get('/class/add-class/{id?}', 'AdminPanel\ClassesController@add'); // For Class
    Route::post('/class/save/{id?}', 'AdminPanel\ClassesController@save');
    Route::get('/class/view-classes', 'AdminPanel\ClassesController@index');
    Route::get('/class/data', 'AdminPanel\ClassesController@anyData');
    Route::get('/class/delete-class/{id?}', 'AdminPanel\ClassesController@destroy');
    Route::get('/class/class-status/{status?}/{id?}', 'AdminPanel\ClassesController@changeStatus');
    
    Route::get('/section/add-section/{id?}', 'AdminPanel\SectionController@add'); // For Section
    Route::post('/section/save/{id?}', 'AdminPanel\SectionController@save');
    Route::get('/section/view-sections', 'AdminPanel\SectionController@index');
    Route::get('/section/data', 'AdminPanel\SectionController@anyData');
    Route::get('/section/delete-section/{id?}', 'AdminPanel\SectionController@destroy');
    Route::get('/section/section-status/{status?}/{id?}', 'AdminPanel\SectionController@changeStatus');
    Route::get('/section/students', 'AdminPanel\SectionController@getStudents');
    Route::get('/section/view-students/{id?}', 'AdminPanel\SectionController@view_students');
     
    // For Class Teacher Allocation @Pratyush on 06 Aug 2018
    Route::get('/class-teacher-allocation/allocate-class/{id?}', 'AdminPanel\ClassTeacherAllocationController@add');
    Route::post('/class-teacher-allocation/save/{id?}', 'AdminPanel\ClassTeacherAllocationController@save'); // Allot Teacher
    Route::get('/class-teacher-allocation/view-allocations', 'AdminPanel\ClassTeacherAllocationController@index');
    Route::get('/class-teacher-allocation/data', 'AdminPanel\ClassTeacherAllocationController@anyData');
    Route::get('/class-teacher-allocation/delete-teacher/{id?}', 'AdminPanel\ClassTeacherAllocationController@destroy');
    Route::get('/class-teacher-allocation/get-section-data/{id?}', 'AdminPanel\ClassTeacherAllocationController@getSectionData');

    Route::get('/subject/add-co-scholastic/{id?}', 'AdminPanel\CoScholasticController@add'); // For Type of Co-Scholastic 
    Route::post('/subject/save-co-scholastic/{id?}', 'AdminPanel\CoScholasticController@save');
    Route::get('/subject/view-co-scholastic', 'AdminPanel\CoScholasticController@index');
    Route::get('/subject/get-data-co-scholastic', 'AdminPanel\CoScholasticController@anyData');
    Route::get('/subject/delete-co-scholastic/{id?}', 'AdminPanel\CoScholasticController@destroy');
    Route::get('/subject/co-scholastic-status/{status?}/{id?}', 'AdminPanel\CoScholasticController@changeStatus');

    Route::get('/subject/add-subject/{id?}', 'AdminPanel\SubjectController@add'); // For subjects
    Route::post('/subject/save/{id?}', 'AdminPanel\SubjectController@save');
    Route::get('/subject/view-subjects', 'AdminPanel\SubjectController@index');
    Route::get('/subject/data', 'AdminPanel\SubjectController@anyData');
    Route::get('/subject/delete-subject/{id?}', 'AdminPanel\SubjectController@destroy');
    Route::get('/subject/subject-status/{status?}/{id?}', 'AdminPanel\SubjectController@changeStatus');

    // For Class Subject Allocation @Pratyush on 06 Aug 2018
    Route::post('/class-subject-mapping/save/{id?}', 'AdminPanel\SubjectClassmappingController@save'); 
    Route::get('/class-subject-mapping/view-subject-class-mapping', 'AdminPanel\SubjectClassmappingController@index');
    Route::get('/class-subject-mapping/subject-class-mapping-data', 'AdminPanel\SubjectClassmappingController@anyData');
    Route::get('/class-subject-mapping/get-section-data/{id?}', 'AdminPanel\SubjectClassmappingController@getSectionData');
    Route::get('/class-subject-mapping/map-subject/{id?}', 'AdminPanel\SubjectClassmappingController@getMapSubject');
    Route::get('/class-subject-mapping/map-subject-data/{id?}', 'AdminPanel\SubjectClassmappingController@anyMapSubjectData');
    Route::get('/class-subject-mapping/view-report/{id?}', 'AdminPanel\SubjectClassmappingController@getViewReport');
    Route::get('/class-subject-mapping/view-report-data/{id?}', 'AdminPanel\SubjectClassmappingController@anyViewReportData');

    // For Section Subject Allocation @Shree on 10 Oct 2018
    Route::post('/section-subject-mapping/save/{id?}', 'AdminPanel\SubjectSectionmappingController@save'); 
    Route::get('/section-subject-mapping/view-subject-section-mapping', 'AdminPanel\SubjectSectionmappingController@index');
    Route::get('/section-subject-mapping/subject-section-mapping-data', 'AdminPanel\SubjectSectionmappingController@anyData');
    Route::get('/section-subject-mapping/get-section-data/{id?}', 'AdminPanel\SubjectSectionmappingController@getSectionData');
    Route::get('/section-subject-mapping/map-subject/{id?}', 'AdminPanel\SubjectSectionmappingController@getMapSubject');
    Route::get('/section-subject-mapping/map-subject-data/{id?}', 'AdminPanel\SubjectSectionmappingController@anyMapSubjectData');
    Route::get('/section-subject-mapping/view-report/{id?}', 'AdminPanel\SubjectSectionmappingController@getViewReport');
    Route::get('/section-subject-mapping/view-report-data/{id?}', 'AdminPanel\SubjectSectionmappingController@anyViewReportData');

    // For manage student's subjects
    Route::get('/student-subject/manage', 'AdminPanel\StudentSubjectManageController@index');
    Route::get('/student-subject/get-student-list', 'AdminPanel\StudentSubjectManageController@get_student_list');
    Route::get('/student-subject/manage-subject/{id?}', 'AdminPanel\StudentSubjectManageController@get_student_subjects');
    Route::get('/student-subject/map-subject-data/{id?}', 'AdminPanel\StudentSubjectManageController@anyMapSubjectData');
    Route::post('/student-subject/manage-save/{id?}', 'AdminPanel\StudentSubjectManageController@save');

    // For Class Subject Staff map @Pratyush on 06 Aug 2018
    Route::post('/teacher-subject-mapping/save/{id?}', 'AdminPanel\SubjectTeachermappingController@save'); 
    Route::get('/teacher-subject-mapping/view-subject-teacher-mapping', 'AdminPanel\SubjectTeachermappingController@index');
    Route::get('/teacher-subject-mapping/subject-teacher-mapping-data', 'AdminPanel\SubjectTeachermappingController@anyData');
    Route::get('/teacher-subject-mapping/get-section-data/{id?}', 'AdminPanel\SubjectTeachermappingController@getSectionData');
    Route::get('/teacher-subject-mapping/map-teacher/{id?}', 'AdminPanel\SubjectTeachermappingController@getMapSubject');
    Route::get('/teacher-subject-mapping/map-teacher-data/{id?}', 'AdminPanel\SubjectTeachermappingController@anyMapSubjectData');
    Route::get('/teacher-subject-mapping/view-report/{id?}', 'AdminPanel\SubjectTeachermappingController@getViewReport');
    Route::get('/teacher-subject-mapping/view-report-data/{id?}', 'AdminPanel\SubjectTeachermappingController@anyViewReportData');

    // For Virtual Class @Pratyush on 21 July 2018 modify on 30 July 2018
    Route::get('/virtual-class/add-virtual-class/{id?}', 'AdminPanel\VirtualClassController@add');
    Route::post('/virtual-class/save/{id?}', 'AdminPanel\VirtualClassController@save');
    Route::get('/virtual-class/view-virtual-classes', 'AdminPanel\VirtualClassController@index');
    Route::get('/virtual-class/data', 'AdminPanel\VirtualClassController@anyData');
    Route::get('/virtual-class/delete-class/{id?}', 'AdminPanel\VirtualClassController@destroy');
    Route::get('/virtual-class/delete-student/{id?}', 'AdminPanel\VirtualClassController@destroyStudent');
    Route::get('/virtual-class/class-status/{status?}/{id?}', 'AdminPanel\VirtualClassController@changeStatus');

    // For Add student to virtual class
    Route::get('/virtual-class/add-student/{id?}', 'AdminPanel\VirtualClassController@studentList'); 
    Route::get('/virtual-class/student-list-data/', 'AdminPanel\VirtualClassController@studentListData');
    Route::get('/virtual-class/add-student-in-class/{classid}/{studentid}', 'AdminPanel\VirtualClassController@addStudentInClass');
      
    // For Delete student to virtual class
    Route::get('/virtual-class/student-list/{id?}', 'AdminPanel\VirtualClassController@studentDeleteList'); 
    Route::get('/virtual-class/student-delete-list-data/', 'AdminPanel\VirtualClassController@studentDeleteListData');
    Route::get('/virtual-class/delete-student-in-class/{classid}/{studentid}', 'AdminPanel\VirtualClassController@destroyStudent');

    // For Competitions @Pratyush on 21 July 2018
    Route::get('/competition/add-competition/{id?}', 'AdminPanel\CompetitionController@add');
    Route::post('/competition/save/{id?}', 'AdminPanel\CompetitionController@save');
    Route::get('/competition/view-competitions', 'AdminPanel\CompetitionController@index');
    Route::get('/competition/data', 'AdminPanel\CompetitionController@anyData');
    Route::get('/competition/delete-competition/{id?}', 'AdminPanel\CompetitionController@destroy');
    Route::get('/competition/competition-status/{status?}/{id?}', 'AdminPanel\CompetitionController@changeStatus');
    Route::get('/competition/map-students/{competitionid?}', 'AdminPanel\CompetitionController@map_students');
    Route::get('/competition/get-student-list', 'AdminPanel\CompetitionController@getStudentData');
    Route::post('/competition/save-map-students', 'AdminPanel\CompetitionController@save_map_students');
    Route::get('/competition/select-winner/{competitionid?}', 'AdminPanel\CompetitionController@select_winner');
    Route::get('/competition/issue-certificate/{competitionid?}', 'AdminPanel\CompetitionController@issue_certificates');
    Route::get('/competition/certificate/{competitionmapid?}', 'AdminPanel\CompetitionController@certificate');
    Route::post('/competition/save-certificate-status', 'AdminPanel\CompetitionController@save_certificate_status');

    // Competition certificate
    Route::get('/certificates/competition-certificates', 'AdminPanel\CertificateController@competition_certificates');
    Route::get('/certificates/character-certificates', 'AdminPanel\CertificateController@character_certificates');
    Route::get('/certificates/transfer-certificates', 'AdminPanel\CertificateController@transfer_certificates');
    Route::get('/certificates/edit-certificate/{id?}', 'AdminPanel\CertificateController@add');
    Route::post('/certificates/save/{id?}', 'AdminPanel\CertificateController@save');
    Route::get('/certificates/certificate-data', 'AdminPanel\CertificateController@anyData');
    Route::get('/certificates/certificate-status/{status?}/{id?}', 'AdminPanel\CertificateController@changeStatus');

    Route::get('/certificates', 'AdminPanel\MenuController@certificates'); // For Certificate menu

    // Certificate templates (Competition,TC,CC)
    Route::get('/certificates/participation-template1', 'AdminPanel\CertificateController@participation_template1');
    Route::get('/certificates/winner-template1', 'AdminPanel\CertificateController@winner_template1');
    Route::get('/certificates/character-template1', 'AdminPanel\CertificateController@character_template1');
    Route::get('/certificates/character-template2', 'AdminPanel\CertificateController@character_template2');
    Route::get('/certificates/transfer-template1', 'AdminPanel\CertificateController@transfer_template1');
    Route::get('/certificates/transfer-template2', 'AdminPanel\CertificateController@transfer_template2');
    Route::get('/certificates/transfer-template3', 'AdminPanel\CertificateController@transfer_template3');

    // ============= For Admission menu  =============
    Route::get('/admission', 'AdminPanel\MenuController@admission');

    // For brochure module on 10 Sept 2018
    Route::get('/brochure/add-brochure/{id?}', 'AdminPanel\BrochureController@add');
    Route::post('/brochure/save/{id?}', 'AdminPanel\BrochureController@save');
    Route::get('/brochure/view-brochures', 'AdminPanel\BrochureController@index');
    Route::get('/brochure/data', 'AdminPanel\BrochureController@anyData');
    Route::get('/brochure/delete-brochure/{id?}', 'AdminPanel\BrochureController@destroy');
    Route::get('/brochure/brochure-status/{status?}/{id?}', 'AdminPanel\BrochureController@changeStatus');

    // For Admission form module on 11 Sept 2018
    Route::get('/admission/create-form/{id?}', 'AdminPanel\AdmissionController@add');
    Route::post('/admission/save/{id?}', 'AdminPanel\AdmissionController@save');
    Route::get('/admission/view-forms', 'AdminPanel\AdmissionController@index');
    Route::get('/admission-form/data', 'AdminPanel\AdmissionController@anyData');
    Route::get('/admission/manage-fields/{id?}', 'AdminPanel\AdmissionController@manageFieldsView');
    Route::post('/admission-form/save-manage-fields/{id?}', 'AdminPanel\AdmissionController@saveManageFields');
    Route::get('/admission/delete-admission-form/{id}', 'AdminPanel\AdmissionController@destroy');
    Route::get('/admission/form-status/{status?}/{id?}', 'AdminPanel\AdmissionController@changeStatus');
    Route::get('/admission-form/fill-enquiry-form/{id?}', 'AdminPanel\AdmissionController@showForm');
    Route::post('/admission-form/save-data-form/{id?}', 'AdminPanel\AdmissionController@saveDataForm');
    Route::get('/admission-form/manage-enquiries', 'AdminPanel\AdmissionController@showEnquiries');
    Route::get('/admission-form/enquiry-data', 'AdminPanel\AdmissionController@getEnquiriesData');
    Route::get('/admission-form/enquiry-status/{status?}/{id?}', 'AdminPanel\AdmissionController@changeEnquiryStatus');
    Route::get('/admission-form/fill-admission-form/{id?}', 'AdminPanel\AdmissionController@showForm');
    Route::get('/admission-form/manage-admissions', 'AdminPanel\AdmissionController@showAdmissions');
    Route::get('/admission-form/admission-data', 'AdminPanel\AdmissionController@getAdmissionsData');
    Route::get('/admission-form/form-data/{id?}', 'AdminPanel\AdmissionController@showFormData');
    
    Route::get('/admission-form/view-forms', 'AdminPanel\AdmissionController@admission_form_index');
    Route::get('/admission-form/view-enquiry-forms', 'AdminPanel\AdmissionController@enquiry_form_index');

    Route::get('/admission-form/selected-students', 'AdminPanel\AdmissionController@showSelectedStudents');
    Route::get('/admission-form/selected-students-data', 'AdminPanel\AdmissionController@getSelectedStudents');
    Route::get('/admission-form/view-students/{id?}', 'AdminPanel\AdmissionController@showStudents');
    Route::get('/admission-form/show-students-data/{id?}', 'AdminPanel\AdmissionController@getStudents');
    Route::get('/student/check-enquiry-existance/{enquiry_no?}', 'AdminPanel\AdmissionController@check_enquiry_existance');

    // ============= For Student menu  =============
    Route::get('/student', 'AdminPanel\MenuController@student');

    Route::get('/student/add-student/{id?}', 'AdminPanel\StudentController@add');
    Route::get('/student/change-parent/{id?}', 'AdminPanel\StudentController@change_parent');
    Route::post('/student/save-parent/{id?}', 'AdminPanel\StudentController@save_parent');
    Route::post('/student/save/{id?}', 'AdminPanel\StudentController@save');
    Route::get('/student/view-list', 'AdminPanel\StudentController@index');
    Route::get('/student/list-data', 'AdminPanel\StudentController@listData');
    Route::get('/student/student-list-data', 'AdminPanel\StudentController@studentListData');
    Route::get('/student/view-students/{sectionid?}', 'AdminPanel\StudentController@view_students');
    Route::get('/student/assign-roll-no/{sectionid?}', 'AdminPanel\StudentController@assign_roll_numbers');
    Route::get('/student/delete-student/{id?}', 'AdminPanel\StudentController@destroy');
    Route::get('/student/student-status/{status?}/{id?}', 'AdminPanel\StudentController@changeStatus');
    Route::get('/student/get-section-data/{id?}', 'AdminPanel\StudentController@getSectionData');
    Route::get('/student/parent-information', 'AdminPanel\StudentController@parent_information');
    Route::get('/student/parent-list', 'AdminPanel\StudentController@parentListData');
    Route::get('/student/view-parent-detail/{id?}', 'AdminPanel\StudentController@view_parent_detail');
    Route::get('/student/check-parent-existance/{mobile_no?}', 'AdminPanel\StudentController@check_parent_existance');
    Route::get('/student/edit-document/{id?}', 'AdminPanel\StudentController@edit_document'); // For document @Pratyush 10 Aug 2018
    Route::post('/student/save-document/{id?}', 'AdminPanel\StudentController@save_document'); // For document @Pratyush 10 Aug 2018
    Route::get('/student/student-document-status/{status?}/{id?}', 'AdminPanel\StudentController@changeDocumentStatus'); // For document @Pratyush 10 Aug 2018
    Route::get('/student/delete-student-document/{id?}/{type}', 'AdminPanel\StudentController@destroyDocument'); // For document @Pratyush 10 

    // Student attendance module in school dashboard.
    Route::get('/student/attendance', 'AdminPanel\StudentController@show_attendance');
    Route::get('/student/get-attendance', 'AdminPanel\StudentController@attendance_data');

    // Import students module in school dashboard.
    Route::get('/student/import-students', 'AdminPanel\StudentController@importView');
    Route::get('/student/get-attendance', 'AdminPanel\StudentController@attendance_data');
    Route::post('/student/student-import-save', 'AdminPanel\StudentController@student_import_save');

    // Student Homework.
    Route::get('/student/homework-group', 'AdminPanel\StudentController@homework_group');
    Route::get('/student/get-homework-info/{id?}', 'AdminPanel\StudentController@get_homework_info');

    // Studenc TC
    Route::get('/student/issue-transfer-certificate', 'AdminPanel\TcController@add_tc');
    Route::get('/student/get-tc-records', 'AdminPanel\TcController@get_tc_records');
    Route::post('/student/save-tc', 'AdminPanel\TcController@save_tc');
    Route::get('/student/view-issue-transfer-certificate', 'AdminPanel\TcController@index');
    Route::get('/student/cancel-transfer-certificate/{id}', 'AdminPanel\TcController@cancel_tc');
    Route::get('/student/transfer-certificate/{id}', 'AdminPanel\TcController@tc_certificate');

    // Dairy Remark
    Route::get('/dairy-remarks/remarks', 'AdminPanel\RemarkController@remark_list');
    Route::get('/daily-remarks/remarks-data', 'AdminPanel\RemarkController@remark_data');
    Route::get('/daily-remarks/remark-single-data', 'AdminPanel\RemarkController@remark_single_data');

    // ============= For Staff menu  =============
    Route::get('/staff', 'AdminPanel\MenuController@staff');

    // For Staff
    Route::get('/staff/add-staff/{id?}', 'AdminPanel\StaffController@add');
    Route::post('/staff/save/{id?}', 'AdminPanel\StaffController@save');
    Route::get('/staff/view-staff', 'AdminPanel\StaffController@index');
    Route::get('/staff/data', 'AdminPanel\StaffController@anyData');
    Route::get('/staff/delete-staff/{id?}', 'AdminPanel\StaffController@destroy');
    Route::get('/staff/staff-status/{status?}/{id?}', 'AdminPanel\StaffController@changeStatus');
    // Route::get('/staff/view-profile/{id?}', 'AdminPanel\StaffController@view_profile');

    Route::get('/staff/import-staff', 'AdminPanel\StaffController@importView');
    Route::post('/staff/staff-import-save', 'AdminPanel\StaffController@staff_import_save');

    // Staff Attendance
    Route::get('/staff/attendance/add-attendance/{id?}', 'AdminPanel\StaffAttendanceController@add_attendance');
    Route::get('/staff/attendance/attendance-data', 'AdminPanel\StaffAttendanceController@attendance_data');
    Route::post('/staff/attendance/save-attendance/{id?}', 'AdminPanel\StaffAttendanceController@attendance_save');
    Route::get('/staff/attendance/view-attendance', 'AdminPanel\StaffAttendanceController@view_attendance');
    Route::get('/staff/attendance/attendance-records', 'AdminPanel\StaffAttendanceController@attendance_records');

    Route::get('/staff-leave-application/manage-leave-application', 'AdminPanel\StaffLeaveApplicationController@manage');
    Route::get('/staff-leave-application/manage-data', 'AdminPanel\StaffLeaveApplicationController@manage_leaves');

    // ============= For Hostel menu  =============
    // For Hostel Details @Shree on 14 Nov 2018
    Route::get('/hostel/configuration/manage-hostel/{id?}', 'AdminPanel\HostelController@add_hostel');
    Route::post('/hostel/configuration/save-hostel/{id?}', 'AdminPanel\HostelController@save_hostel');
    Route::get('/hostel/configuration/data-hostel', 'AdminPanel\HostelController@anyDataHostel');
    Route::get('/hostel/configuration/delete-hostel/{id?}', 'AdminPanel\HostelController@destroyHostel');
    Route::get('/hostel/configuration/hostel-status/{status?}/{id?}', 'AdminPanel\HostelController@changeStatusHostel');

    // For Hostel room category @Shree on 14 Nov 2018
    Route::get('/hostel/rooms/manage-room-category/{id?}', 'AdminPanel\HostelController@add_room_cate');
    Route::post('/hostel/rooms/save-room-category/{id?}', 'AdminPanel\HostelController@save_room_cate');
    Route::get('/hostel/rooms/data-room-category', 'AdminPanel\HostelController@anyDataRoomCate');
    Route::get('/hostel/rooms/delete-room-category/{id?}', 'AdminPanel\HostelController@destroyRoomCate');
    Route::get('/hostel/rooms/room-category-status/{status?}/{id?}', 'AdminPanel\HostelController@changeStatusRoomCate');

    // For Hostel Blocks @Shree on 14 Nov 2018
    Route::get('/hostel/configuration/manage-hostel-block/{id?}', 'AdminPanel\HostelController@add_block');
    Route::post('/hostel/configuration/save-hostel-block/{id?}', 'AdminPanel\HostelController@save_block');
    Route::get('/hostel/configuration/data-hostel-block', 'AdminPanel\HostelController@anyDataBlock');
    Route::get('/hostel/configuration/delete-hostel-block/{id?}', 'AdminPanel\HostelController@destroyBlock');
    Route::get('/hostel/configuration/hostel-block-status/{status?}/{id?}', 'AdminPanel\HostelController@changeStatusBlock');
    
    Route::get('/hostel/configuration/delete-floor/{id?}', 'AdminPanel\HostelController@destroyFloor');
    Route::get('/hostel/configuration/floors', 'AdminPanel\HostelController@floors');

    // For Hostel Blocks @Shree on 14 Nov 2018
    Route::get('/hostel/rooms/manage-room/{id?}', 'AdminPanel\HostelController@add_room');
    Route::post('/hostel/rooms/save-room/{id?}', 'AdminPanel\HostelController@save_room');
    Route::get('/hostel/rooms/data-room', 'AdminPanel\HostelController@anyDataRoom');
    Route::get('/hostel/rooms/delete-room/{id?}', 'AdminPanel\HostelController@destroyRoom');
    Route::get('/hostel/rooms/room-status/{status?}/{id?}', 'AdminPanel\HostelController@changeStatusRoom');
    // Hostel Ajax
    Route::get('/hostel/configuration/get-block-data/{id?}', 'AdminPanel\HostelController@getBlockData');
    Route::get('/hostel/configuration/get-floor-data/{id?}', 'AdminPanel\HostelController@getFloorData');
    Route::get('/hostel/rooms/get-room-data/{id?}', 'AdminPanel\HostelController@getRoomData');
    Route::get('/hostel/rooms/get-room-availability/{hostel_id?}', 'AdminPanel\HostelController@getRoomAvailability');
    
    Route::get('/hostel/manage-registration/', 'AdminPanel\HostelController@manage_registration');
    Route::get('/hostel/registration/get-student-list/', 'AdminPanel\HostelController@get_student_list');
    Route::get('/hostel/register-student/{id?}', 'AdminPanel\HostelController@register_student');
    Route::get('/hostel/room-allocation/', 'AdminPanel\HostelController@room_allocation');
    Route::get('/hostel/allocation/get-student-list/', 'AdminPanel\HostelController@get_register_student_list');
    Route::post('/hostel/allocation/allocate', 'AdminPanel\HostelController@room_allocate');

    Route::get('/hostel/student-transfer/', 'AdminPanel\HostelController@student_transfer');
    Route::get('/hostel/room-leave/', 'AdminPanel\HostelController@room_leave');
    Route::get('/hostel/room-leave-data/{id?}', 'AdminPanel\HostelController@room_leave_by_id');
    Route::get('/hostel/collect-fees/', 'AdminPanel\HostelController@collect_fees');
    // Hostel reports
    Route::get('/hostel/registered-student-report/', 'AdminPanel\HostelController@register_student_report');
    // Route::get('/hostel/allocation-report/', 'AdminPanel\HostelController@allocation_report');
    Route::get('/hostel/leave-student-report/', 'AdminPanel\HostelController@leave_student_report');
    Route::get('/hostel/allocation-report/', 'AdminPanel\HostelController@free_space_room_report');
    Route::get('/hostel/allocation-report/room-data', 'AdminPanel\HostelController@freeSpaceRoomAnyData');

    // For Hostel Fees Head @Khushbu 18 Jan 2019
    Route::get('/hostel/manage-hostel-fees-head/{id?}', 'AdminPanel\HostelController@add_h_fees_head');
    Route::post('/hostel/save-hostel-fees-head/{id?}', 'AdminPanel\HostelController@save_h_fees_head');
    Route::get('/hostel/data-hostel-fees-head', 'AdminPanel\HostelController@anyDataHostelFeesHead');
    Route::get('/hostel/delete-hostel-fees-head/{id?}', 'AdminPanel\HostelController@destroyHostelFeesHead');
    Route::get('/hostel/hostel-fess-head-status/{status?}/{id?}', 'AdminPanel\HostelController@changeStatusHostelFeesHead');
  });
  
  Route::get('/student/get-homework-info/{id?}', 'AdminPanel\StudentController@get_homework_info');
  Route::get('/student/student-profile/{id?}', 'AdminPanel\StudentController@student_profile');
 
  // Route::get('/student/student-profile/{id?}', 'AdminPanel\StudentController@student_profile');
  Route::get('/staff/view-profile/{id?}', 'AdminPanel\StaffController@view_profile');
  // // For Dashboard
  // Route::get('/dashboard', 'AdminPanel\DashboardController@index');

  // // For Admin Profile
  // Route::get('/dashboard', 'AdminPanel\DashboardController@index');

  // For Online Content menu
  Route::get('/online-content', 'AdminPanel\MenuController@online_content');
  // For Inventory Menu
  Route::get('/inventory', 'AdminPanel\MenuController@inventory');
  // For Fees collection
  Route::get('/fees-collection', 'AdminPanel\MenuController@fees_collection');
  // For Library
  Route::get('/library', 'AdminPanel\MenuController@library');
  // For Examination
  Route::get('/examination', 'AdminPanel\MenuController@examination');
  // For Visitor
  Route::get('/visitor', 'AdminPanel\MenuController@visitor');
  
  // For Hostel
  Route::get('/hostel', 'AdminPanel\MenuController@hostel');
  Route::get('/hostel/configuration', 'AdminPanel\MenuController@hostel');
  Route::get('/hostel/rooms', 'AdminPanel\MenuController@hostel');
  // For Transport
  Route::get('/transport', 'AdminPanel\MenuController@transport');
  // For Notice Board
  Route::get('/notice-board', 'AdminPanel\MenuController@noticeboard');
  // For Task Manager
  Route::get('/task-manager', 'AdminPanel\MenuController@taskmanager');
  // For Recruitment
  Route::get('/recruitment', 'AdminPanel\MenuController@recruitment');
  // For Leave Management
  Route::get('/staff-leave-management', 'AdminPanel\MenuController@staff_leave_management');
  Route::get('/student-leave-management', 'AdminPanel\MenuController@student_leave_management');
  Route::get('/my-class', 'AdminPanel\MenuController@my_class');
  Route::get('/my-subjects', 'AdminPanel\MenuController@my_subject');

  

  // Academic section start here
  

  Route::get('/student/get-homework-data', 'AdminPanel\StudentController@get_homework_group_data');
  
  // For Ajax
  Route::get('/holiday/check-holidays', 'AdminPanel\HolidayController@checkHolidays');
  Route::get('/class/get-class-data/{medium?}', 'AdminPanel\ClassesController@getClassData');
  Route::get('/class/get-class-stream-status/{id?}', 'AdminPanel\ClassesController@getstreamStatus');
  Route::get('/stream/get-stream-data/{medium?}', 'AdminPanel\StreamController@getStreamData');
  Route::get('/student/check-section-capacity/{id?}', 'AdminPanel\StudentController@checkSectionCapacity');
  Route::get('/state/show-state/{id?}', 'AdminPanel\StateController@showStates');
  Route::get('/city/show-city/{id?}', 'AdminPanel\CityController@showCity');
  Route::get('/brochure/get-brochure-data/{session_id?}', 'AdminPanel\BrochureController@getBrochureData');
  Route::get('/class/get-total-intake-data/{class_id?}', 'AdminPanel\ClassesController@getIntakeData');
  Route::get('/staff/get-staff-data/{medium?}', 'AdminPanel\StaffController@getStaffData');
  Route::get('/examination/get-term-data/{medium?}', 'AdminPanel\TermController@getTermData');
  Route::get('/subject/get-class-subject-data/{class_id?}/{medium?}', 'AdminPanel\SubjectController@getClassSubjectData');
  Route::get('/recurring-head/get-heads/{type?}', 'AdminPanel\RecurringHeadController@getHeadeData');
  
  Route::get('/hostel/configuration/get-hostel-data', 'AdminPanel\HostelController@getHostelData');
  Route::get('/hostel/rooms/get-category-data', 'AdminPanel\HostelController@getRoomCategoryData');

  // For staff Leave Application
  Route::get('/staff-leave-application/add-leave-application/{id?}', 'AdminPanel\StaffLeaveApplicationController@add');
  Route::post('/staff-leave-application/save/{id?}', 'AdminPanel\StaffLeaveApplicationController@save');
  Route::get('/staff-leave-application/view-leave-application', 'AdminPanel\StaffLeaveApplicationController@index');
  Route::get('/staff-leave-application/data', 'AdminPanel\StaffLeaveApplicationController@anyData');
  Route::get('/staff-leave-application/delete/{id?}', 'AdminPanel\StaffLeaveApplicationController@destroy');
  Route::get('/staff-leave-application/edit/{id?}', 'AdminPanel\StaffLeaveApplicationController@add');
  Route::get('/staff-leave-application/staff-leave-application-status/{status?}/{id?}', 'AdminPanel\StaffLeaveApplicationController@changeStatus');
  
  
  // For Notes @Ashish on 31 July 2018
  Route::get('/notes/add-notes/{id?}', 'AdminPanel\NoteController@add');
  Route::post('/notes/save/{id?}', 'AdminPanel\NoteController@save');
  Route::get('/notes/view-notes', 'AdminPanel\NoteController@index');
  Route::get('/notes/data', 'AdminPanel\NoteController@anyData');
  Route::get('/notes/delete-notes/{id?}', 'AdminPanel\NoteController@destroy');
  Route::get('/notes/notes-status/{status?}/{id?}', 'AdminPanel\NoteController@changeStatus');
  Route::get('/notes/get-section-data/{id?}', 'AdminPanel\NoteController@getSectionData');

  // Route::get('/teacher-subject-mapping/delete-all-teacher/{id?}', 'AdminPanel\SubjectTeachermappingController@destroy');


  // For Student Leave Application @Pratyush on 10 Aug 2018
  Route::get('/student-leave-application/add-leave-application/{id?}', 'AdminPanel\StudentLeaveApplicationController@add');
  Route::post('/student-leave-application/save/{id?}', 'AdminPanel\StudentLeaveApplicationController@save');
  Route::get('/student-leave-application/view-leave-application', 'AdminPanel\StudentLeaveApplicationController@index');
  Route::get('/student-leave-application/data', 'AdminPanel\StudentLeaveApplicationController@anyData');
  Route::get('/student-leave-application/delete/{id?}', 'AdminPanel\StudentLeaveApplicationController@destroy');
  Route::get('/student-leave-application/edit/{id?}', 'AdminPanel\StudentLeaveApplicationController@add');
  Route::get('/student-leave-application/student-leave-application-status/{status?}/{id?}', 'AdminPanel\StudentLeaveApplicationController@changeStatus');

  
  Route::get('/student-leave-application/manage-leave-application', 'AdminPanel\StudentLeaveApplicationController@manage');
  Route::get('/student-leave-application/manage-data', 'AdminPanel\StudentLeaveApplicationController@manage_leaves');



  // For Term @Pratyush on 11 Aug 2018
  Route::get('/examination/manage-term/{id?}', 'AdminPanel\TermController@add');
  Route::post('/examination/save-term/{id?}', 'AdminPanel\TermController@save');
  // Route::get('/term/view-terms', 'AdminPanel\TermController@index');
  Route::get('/examination/data-term', 'AdminPanel\TermController@anyData');
  Route::get('/examination/delete-term/{id?}', 'AdminPanel\TermController@destroy');
  Route::get('/examination/term-status/{status?}/{id?}', 'AdminPanel\TermController@changeStatus');

  // For Exam @Pratyush on 11 Auclass_nameg 2018
  Route::get('/examination/manage-exam/{id?}', 'AdminPanel\ExamController@add');
  Route::post('/examination/save-exam/{id?}', 'AdminPanel\ExamController@save');
  // Route::get('/exam/view-exams', 'AdminPanel\ExamController@index');
  Route::get('/examination/data-exam', 'AdminPanel\ExamController@anyData');
  Route::get('/examination/delete-exam/{id?}', 'AdminPanel\ExamController@destroy');
  Route::get('/examination/exam-status/{status?}/{id?}', 'AdminPanel\ExamController@changeStatus');

  // For Book Category @Pratyush on 13 Aug 2018
  Route::get('/book-category/add-book-category/{id?}', 'AdminPanel\BookCategoryController@add');
  Route::post('/book-category/save/{id?}', 'AdminPanel\BookCategoryController@save');
  Route::get('/book-category/view-book-categories', 'AdminPanel\BookCategoryController@index');
  Route::get('/book-category/data', 'AdminPanel\BookCategoryController@anyData');
  Route::get('/book-category/delete-book-category/{id?}', 'AdminPanel\BookCategoryController@destroy');
  Route::get('/book-category/book-category-status/{status?}/{id?}', 'AdminPanel\BookCategoryController@changeStatus');

  // For Book Allowance @Pratyush on 13 Aug 2018
  Route::get('/book-allowance/manage/{id?}', 'AdminPanel\BookAllowanceController@add');
  Route::post('/book-allowance/save/{id?}', 'AdminPanel\BookAllowanceController@save');

  // For Book Book Vender @Pratyush on 13 Aug 2018
  Route::get('/book-vendor/add-book-vendor/{id?}', 'AdminPanel\BookVendorController@add');
  Route::post('/book-vendor/save/{id?}', 'AdminPanel\BookVendorController@save');
  Route::get('/book-vendor/view-book-vendors', 'AdminPanel\BookVendorController@index');
  Route::get('/book-vendor/data', 'AdminPanel\BookVendorController@anyData');
  Route::get('/book-vendor/delete-book-vendor/{id?}', 'AdminPanel\BookVendorController@destroy');
  Route::get('/book-vendor/book-vendor-status/{status?}/{id?}', 'AdminPanel\BookVendorController@changeStatus');

  // For Book CupBoard @Pratyush on 13 Aug 2018
  Route::get('/book-cupboard/add-book-cupboard/{id?}', 'AdminPanel\BookCupboardController@add');
  Route::post('/book-cupboard/save/{id?}', 'AdminPanel\BookCupboardController@save');
  Route::get('/book-cupboard/view-book-cupboard', 'AdminPanel\BookCupboardController@index');
  Route::get('/book-cupboard/data', 'AdminPanel\BookCupboardController@anyData');
  Route::get('/book-cupboard/delete-book-cupboard/{id?}', 'AdminPanel\BookCupboardController@destroy');
  Route::get('/book-cupboard/book-cupboard-status/{status?}/{id?}', 'AdminPanel\BookCupboardController@changeStatus');

  // For Book CupBoard @Pratyush on 13 Aug 2018
  Route::get('/cupboard-shelf/add-cupboard-shelf/{id?}', 'AdminPanel\BookCupboardshelfController@add');
  Route::post('/cupboard-shelf/save/{id?}', 'AdminPanel\BookCupboardshelfController@save');
  Route::get('/cupboard-shelf/view-cupboard-shelf', 'AdminPanel\BookCupboardshelfController@index');
  Route::get('/cupboard-shelf/data', 'AdminPanel\BookCupboardshelfController@anyData');
  Route::get('/cupboard-shelf/delete-cupboard-shelf/{id?}', 'AdminPanel\BookCupboardshelfController@destroy');
  Route::get('/cupboard-shelf/cupboard-shelf-status/{status?}/{id?}', 'AdminPanel\BookCupboardshelfController@changeStatus');

  // For Book CupBoard @Pratyush on 13 Aug 2018
  Route::get('/book/add-book/{id?}', 'AdminPanel\BookController@add');
  Route::post('/book/save/{id?}', 'AdminPanel\BookController@save');
  Route::get('/book/view-books', 'AdminPanel\BookController@index');
  Route::get('/book/view-book-details/{id?}', 'AdminPanel\BookController@displayBook');
  Route::get('/book/data', 'AdminPanel\BookController@anyData');
  Route::get('/book/single-book-data', 'AdminPanel\BookController@singleBookData');
  Route::get('/book/delete-book/{id?}', 'AdminPanel\BookController@destroy');
  Route::get('/book/book-status/{status?}/{id?}', 'AdminPanel\BookController@changeStatus');
  Route::get('/book/get-cupboard-shelf-data/{id?}', 'AdminPanel\BookController@getCupboardShelfData');
  Route::get('/book/copies', 'AdminPanel\BookController@copies');
  Route::get('/book/delete-single-book/{id?}', 'AdminPanel\BookController@destroySingleBook');

  // For Library Mamber @Pratyush on 19 Aug 2018
  // -- Student
  Route::get('/member/register-student/{id?}', 'AdminPanel\LibraryMemberController@addStudent');
  Route::get('/member/register-student-data/{id?}', 'AdminPanel\LibraryMemberController@anyStudnetData');
  Route::any('/member/save-students/{id?}', 'AdminPanel\LibraryMemberController@saveStudent');
  Route::get('/member/view-students', 'AdminPanel\LibraryMemberController@viewStudent');
  Route::get('/member/view-students-data', 'AdminPanel\LibraryMemberController@viewStudentData');
  Route::get('/member/view-students/issued-books/{id?}', 'AdminPanel\LibraryMemberController@studentIssuedBooks');
  Route::get('/member/view-students/issued-books-data', 'AdminPanel\LibraryMemberController@studentIssuedBooksData');


  // -- Sfaff
  Route::get('/member/register-staff/{id?}', 'AdminPanel\LibraryMemberController@addStaff');
  Route::get('/member/register-staff-data/{id?}', 'AdminPanel\LibraryMemberController@anyStaffData');
  Route::any('/member/save-staff/{id?}', 'AdminPanel\LibraryMemberController@saveStaff');
  Route::get('/member/view-staff', 'AdminPanel\LibraryMemberController@viewStaff');
  Route::get('/member/view-staff-data', 'AdminPanel\LibraryMemberController@viewStaffData');
  Route::get('/member/view-staff/issued-books/{id?}', 'AdminPanel\LibraryMemberController@staffIssuedBooks');
  Route::get('/member/view-staff/issued-books-data', 'AdminPanel\LibraryMemberController@staffIssuedBooksData');

  // For Library Book Issue @Pratyush on 19 Aug 2018
  Route::get('/issue-book', 'AdminPanel\BookIssueController@index');  
  Route::get('/issue-book-data', 'AdminPanel\BookIssueController@anyData');
  Route::get('/issue-book/books/{id?}', 'AdminPanel\BookIssueController@displayBooks');  
  Route::get('/issue-book-data/books-data', 'AdminPanel\BookIssueController@displayBooksData');
  Route::post('/issue-book/issue-member-book/{id?}', 'AdminPanel\BookIssueController@issueBook');

  // For Question Paper @Ashish on 21 Aug 2018
  Route::get('/question-paper/add-question-paper/{id?}', 'AdminPanel\QuestionPaperController@add');
  Route::post('/question-paper/save/{id?}', 'AdminPanel\QuestionPaperController@save');
  Route::get('/question-paper/view-question-paper', 'AdminPanel\QuestionPaperController@index');
  Route::get('/question-paper/data', 'AdminPanel\QuestionPaperController@anyData');
  Route::get('/question-paper/delete-question-paper/{id?}', 'AdminPanel\QuestionPaperController@destroy');
  Route::get('/question-paper/question-paper-status/{status?}/{id?}', 'AdminPanel\QuestionPaperController@changeStatus');
  Route::get('/question-paper/get-section-data/{id?}', 'AdminPanel\QuestionPaperController@getSectionData');
  Route::get('/question-paper/get-subject-data/{id?}', 'AdminPanel\QuestionPaperController@getSubjectData');


  // For Library Book return / Renew / Update @Pratyush on 19 Aug 2018
  Route::get('/return-book', 'AdminPanel\BookIssueController@showReturnBookPage');  
  Route::get('/return-book-data', 'AdminPanel\BookIssueController@ReturnBookPageData');
  Route::get('/return-book/return-book/{id?}', 'AdminPanel\BookIssueController@returnBook');
  Route::post('/return-book/renew-book', 'AdminPanel\BookIssueController@renewBook');
  Route::post('/return-book/update-book', 'AdminPanel\BookIssueController@updateBook');


  // For Library Fine @Pratyush on 11 Aug 2018
  Route::get('library-fine/get-member/{id?}', 'AdminPanel\LibraryFineController@getMemberDetail');
  Route::get('library-fine', 'AdminPanel\LibraryFineController@add');
  Route::post('library-fine/save/{id?}', 'AdminPanel\LibraryFineController@save');
  Route::get('library-fine/data', 'AdminPanel\LibraryFineController@anyData');
  Route::get('library-fine/delete/{id?}', 'AdminPanel\LibraryFineController@destroy');
  Route::get('library-fine/status/{status?}/{id?}', 'AdminPanel\LibraryFineController@changeStatus');


  //For Time Table @Khushbu on 06 Sept 2018
  Route::get('/time-table/', 'AdminPanel\TimeTableController@index');
  Route::get('/time-table/add-time-table/{id?}', 'AdminPanel\TimeTableController@add');
  Route::post('/time-table/save/{id?}', 'AdminPanel\TimeTableController@save');
  Route::get('/time-table/view-time-table', 'AdminPanel\TimeTableController@index');
  Route::get('/time-table/data', 'AdminPanel\TimeTableController@anyData');
  Route::get('/time-table/delete-time-table/{id?}', 'AdminPanel\TimeTableController@destroy');
  Route::get('/time-table/time-table-status/{status?}/{id?}', 'AdminPanel\TimeTableController@changeStatus');
  Route::get('/time-table/set-time-table/{id?}', 'AdminPanel\TimeTableController@set_time_table');
  Route::get('/time-table/get-available-staff', 'AdminPanel\TimeTableController@get_available_staff');
  Route::post('/time-table/save-time-table-map', 'AdminPanel\TimeTableController@save_time_table_period');
  Route::get('/time-table/delete-time-table-period/{id?}', 'AdminPanel\TimeTableController@destroyPeriod');
  Route::get('/time-table/get-period-info/', 'AdminPanel\TimeTableController@get_period_info');
  Route::get('/time-table/check-time-availability/', 'AdminPanel\TimeTableController@check_time_availability');

  //For Ajax
  Route::get('/section/get-class-section-data/{class_id?}', 'AdminPanel\SectionController@getClassSectionData');
  // Set Time Table
  Route::get('/time-table/set-time-table', 'AdminPanel\TimeTableController@showTimeTable');
  Route::get('/time-table/view-time-table/data', 'AdminPanel\TimeTableController@setTimeTableData');

  
  //For Ajax
  Route::get('/state/get-country-state-data/{country_id?}', 'AdminPanel\StateController@showStates');

  
  
  // For One Time Head
  Route::get('/one-time/add-one-time/{id?}', 'AdminPanel\OneTimeController@add');
  Route::post('/one-time/save/{id?}', 'AdminPanel\OneTimeController@save');
  Route::get('/one-time/view-one-time', 'AdminPanel\OneTimeController@index');
  Route::get('/one-time/data', 'AdminPanel\OneTimeController@anyData');
  Route::get('/one-time/delete-one-time/{id?}', 'AdminPanel\OneTimeController@destroy');
  Route::get('/one-time/one-time-status/{status?}/{id?}', 'AdminPanel\OneTimeController@changeStatus');
  Route::get('/one-time/classes', 'AdminPanel\OneTimeController@classes');

  // For Recurring Head
  Route::get('/recurring-head/add-recurring-head/{id?}', 'AdminPanel\RecurringHeadController@add');
  Route::post('/recurring-head/save/{id?}', 'AdminPanel\RecurringHeadController@save');
  Route::get('/recurring-head/view-recurring-heads', 'AdminPanel\RecurringHeadController@index');
  Route::get('/recurring-head/data', 'AdminPanel\RecurringHeadController@anyData');
  Route::get('/recurring-head/delete-recurring-head/{id?}', 'AdminPanel\RecurringHeadController@destroy');
  Route::get('/recurring-head/recurring-head-status/{status?}/{id?}', 'AdminPanel\RecurringHeadController@changeStatus');
  Route::get('/recurring-head/delete-inst/{id?}', 'AdminPanel\RecurringHeadController@destroyInst');
  Route::get('/recurring-head/installment', 'AdminPanel\RecurringHeadController@installment');

  // For Bank master module
  Route::get('/bank/add-bank/{id?}', 'AdminPanel\BankController@add');
  Route::post('/bank/save/{id?}', 'AdminPanel\BankController@save');
  Route::get('/bank/view-bank', 'AdminPanel\BankController@index');
  Route::get('/bank/data', 'AdminPanel\BankController@anyData');
  Route::get('/bank/delete-bank/{id?}', 'AdminPanel\BankController@destroy');
  Route::get('/bank/bank-status/{status?}/{id?}', 'AdminPanel\BankController@changeStatus');

  // For RTE cheque module
  Route::get('/rte-cheque/add-rte-cheque/{id?}', 'AdminPanel\RTEChequeController@add');
  Route::post('/rte-cheque/save/{id?}', 'AdminPanel\RTEChequeController@save');
  Route::get('/rte-cheque/view-rte-cheque', 'AdminPanel\RTEChequeController@index');
  Route::get('/rte-cheque/data', 'AdminPanel\RTEChequeController@anyData');
  Route::get('/rte-cheque/delete-rte-cheque/{id?}', 'AdminPanel\RTEChequeController@destroy');
  Route::get('/rte-cheque/rte-cheque-status/{status?}/{id?}', 'AdminPanel\RTEChequeController@changeStatus');

  // For Cheque details
  Route::get('/receive-cheque/view-receive-cheque', 'AdminPanel\RTEChequeController@receive_cheques');
  Route::get('/receive-cheque/data', 'AdminPanel\RTEChequeController@receiveChequeData');
  Route::get('/receive-cheque/receive-cheque-status/{status?}/{id?}', 'AdminPanel\RTEChequeController@changeRChequeStatus');
  
  // For RTE Head 
  Route::get('/rte-head/rte-fees-head/{id?}', 'AdminPanel\RTEHeadController@add');
  Route::post('/rte-head/save-rte-head/{id?}', 'AdminPanel\RTEHeadController@save');
  Route::get('/rte-head/data-rte-head', 'AdminPanel\RTEHeadController@anyData');
  Route::get('/rte-head/delete-rte-head/{id?}', 'AdminPanel\RTEHeadController@destroy');
  Route::get('/rte-head/rte-head-status/{status?}/{id?}', 'AdminPanel\RTEHeadController@changeStatus');
  Route::get('/rte-head/apply-fees-on-head', 'AdminPanel\RTEHeadController@apply_fees_on_head');
  Route::get('/rte-head/get-student-list', 'AdminPanel\RTEHeadController@get_student_list');
  Route::get('/rte-head/get-student-counts', 'AdminPanel\RTEHeadController@get_student_counts');
  Route::post('/rte-head/map-students', 'AdminPanel\RTEHeadController@map_students');
  // For Prepaid Account
  Route::get('/prepaid-account/manage-account/{id?}', 'AdminPanel\PrepaidAccountController@add');
  Route::post('/prepaid-account/save-account/{id?}', 'AdminPanel\PrepaidAccountController@save');
  Route::get('/prepaid-account/data-account', 'AdminPanel\PrepaidAccountController@anyData');
  Route::get('/prepaid-account/delete-account/{id?}', 'AdminPanel\PrepaidAccountController@destroy');
  Route::get('/prepaid-account/account-status/{status?}/{id?}', 'AdminPanel\PrepaidAccountController@changeStatus');

  // For Imprest AC Entries
  Route::get('/fees-collection/manage-entry/{id?}', 'AdminPanel\ImprestAcController@add');
  Route::post('/fees-collection/save-entry/{id?}', 'AdminPanel\ImprestAcController@save');
  Route::get('/fees-collection/data-entry', 'AdminPanel\ImprestAcController@anyData');
  Route::get('/fees-collection/delete-entry/{id?}', 'AdminPanel\ImprestAcController@destroy');

  // Imprest A/C counter
  Route::get('/fees-collection/imprest-ac-counter', 'AdminPanel\ImprestAcController@counter_index');
  Route::get('/imprest/get-class-data/{id?}', 'AdminPanel\ImprestAcController@getFeesClasses');
  Route::get('/imprest/get-student-list', 'AdminPanel\ImprestAcController@get_student_list');
  Route::post('/imprest/fees-deduct', 'AdminPanel\ImprestAcController@fees_deduction');
  Route::get('/imprest/get-student-counts', 'AdminPanel\ImprestAcController@get_student_count');

  //For Visitor 
  Route::get('/visitor/add-visitor/{id?}', 'AdminPanel\VisitorController@add');
  Route::post('/visitor/save/{id?}', 'AdminPanel\VisitorController@save');
  Route::get('/visitor/view-visitor', 'AdminPanel\VisitorController@index');
  Route::get('/visitor/visitor-data', 'AdminPanel\VisitorController@anyData');
  Route::get('/visitor/delete-visitor/{id?}', 'AdminPanel\VisitorController@destroy');

  // For free student map
  Route::get('/map-student/free-by-rte', 'AdminPanel\RTEHeadController@free_by_rte');
  Route::get('/map-student/student-rte-list', 'AdminPanel\RTEHeadController@studentListData');
  Route::get('/map-student/rte-status/{status?}/{id?}', 'AdminPanel\RTEHeadController@rteStatus');

  //For Grade Scheme 
  Route::get('/grade-scheme/add-grade-scheme/{id?}', 'AdminPanel\GradeSchemeController@add');
  Route::post('/grade-scheme/save/{id?}', 'AdminPanel\GradeSchemeController@save');
  Route::get('/grade-scheme/view-grade-scheme', 'AdminPanel\GradeSchemeController@index');
  Route::get('/grade-scheme/data', 'AdminPanel\GradeSchemeController@anyData');
  Route::get('/grade-scheme/grade-scheme-status/{status?}/{id?}', 'AdminPanel\GradeSchemeController@changeStatus');
  Route::get('/grade-scheme/delete-grade-scheme/{id?}', 'AdminPanel\GradeSchemeController@destroy');
  Route::get('/grade-scheme/delete-grade/{id?}', 'AdminPanel\GradeSchemeController@destroyGrade');
  Route::get('/grade-scheme/grades', 'AdminPanel\GradeSchemeController@grades');

  //For Fine
  Route::get('/fine/add-fine/{id?}', 'AdminPanel\FineController@add');
  Route::post('/fine/save/{id?}', 'AdminPanel\FineController@save');
  Route::get('/fine/view-fine', 'AdminPanel\FineController@index');
  Route::get('/fine/data', 'AdminPanel\FineController@anyData');
  Route::get('/fine/fine-status/{status?}/{id?}', 'AdminPanel\FineController@changeStatus');
  Route::get('/fine/delete-fine/{id?}', 'AdminPanel\FineController@destroy');
  Route::get('/fine/delete-detail/{id?}', 'AdminPanel\FineController@destroyDetail');
  Route::get('/fine/details', 'AdminPanel\FineController@fine_details');

  // For Marks criteria
  Route::get('/examination/manage-marks-criteria/{id?}', 'AdminPanel\MarksCriteriaController@add');
  Route::post('/examination/save-marks-criteria/{id?}', 'AdminPanel\MarksCriteriaController@save');
  Route::get('/examination/data-marks-criteria', 'AdminPanel\MarksCriteriaController@anyData');
  Route::get('/examination/delete-marks-criteria/{id?}', 'AdminPanel\MarksCriteriaController@destroy');
  Route::get('/examination/marks-criteria-status/{status?}/{id?}', 'AdminPanel\MarksCriteriaController@changeStatus');

  // For Concession
  Route::get('/concession/manage-concession/', 'AdminPanel\ConcessionController@index');
  Route::get('/concession/get-student-list', 'AdminPanel\ConcessionController@get_student_list');
  Route::get('/concession/get-student-fees', 'AdminPanel\ConcessionController@get_student_fees');
  Route::post('/concession/concession-map', 'AdminPanel\ConcessionController@concession_map');

  // For Student's account
  Route::get('/manage-account/wallet-account/', 'AdminPanel\ManageStudentAccount@wallet');
  Route::get('/manage-account/imprest-account/', 'AdminPanel\ManageStudentAccount@imprest');
  Route::get('/manage-account/get-student-list', 'AdminPanel\ManageStudentAccount@get_student_list');

  // Map heads to students
  Route::get('/map-head/heads/', 'AdminPanel\MapHeadsController@index');
  Route::get('/map-heads/get-heads-list/', 'AdminPanel\MapHeadsController@get_heads_list');
  Route::get('/map-heads/classes/', 'AdminPanel\MapHeadsController@classes');
  Route::get('/map-heads/map-student/{id?}/{type?}', 'AdminPanel\MapHeadsController@map_student');
  Route::get('/map-heads/get-student-list', 'AdminPanel\MapHeadsController@get_student_list');
  Route::post('/map-heads/save-map-student', 'AdminPanel\MapHeadsController@save_map_student');
  Route::get('/map-heads/get-student-counts', 'AdminPanel\MapHeadsController@get_student_counts');

  // Fees counter
  Route::get('/fees-counter', 'AdminPanel\FeesCounterController@index');
  Route::get('/fees-counter/add/{id?}', 'AdminPanel\FeesCounterController@add');
  Route::get('/fees-counter/get-student-list', 'AdminPanel\FeesCounterController@get_student_list');
  Route::post('/fees-counter/save-fees-data/{id?}', 'AdminPanel\FeesCounterController@save_fees_data');
  Route::get('/map-heads/get-student-counts', 'AdminPanel\MapHeadsController@get_student_counts');
  
  // For Reasons 
  Route::get('/reason/add-reason/{id?}', 'AdminPanel\ReasonController@add');
  Route::post('/reason/save/{id?}', 'AdminPanel\ReasonController@save');
  Route::get('/reason/view-reasons', 'AdminPanel\ReasonController@index');
  Route::get('/reason/data', 'AdminPanel\ReasonController@anyData');
  Route::get('/reason/delete-reason/{id?}', 'AdminPanel\ReasonController@destroy');
  Route::get('/reason/reason-status/{status?}/{id?}', 'AdminPanel\ReasonController@changeStatus');

  // For Fees Receipt
  Route::get('/fees-collection/view-fees-receipt', 'AdminPanel\FeesReceiptController@index');
  Route::get('/fees-collection/fees-receipt-data', 'AdminPanel\FeesReceiptController@anyData');
  Route::get('/fees-collection/fees-receipt-status/{status?}/{id?}', 'AdminPanel\FeesReceiptController@changeStatus');
  Route::get('/fees-collection/fee-receipt/{id?}', 'AdminPanel\FeesReceiptController@fees_receipt');

  // For Exam mapping 
  Route::get('/examination/map-exam-class-subjects', 'AdminPanel\ExamMapController@index');
  Route::get('/examination/exam-class-subject-data', 'AdminPanel\ExamMapController@show_exams');
  Route::get('/examination/add-classes/{id?}', 'AdminPanel\ExamMapController@add_classes');
  Route::get('/examination/show-section-subjects/{counter?}/{id?}', 'AdminPanel\ExamMapController@show_section_subject');
  Route::get('/examination/get-sections/{id?}', 'AdminPanel\ExamMapController@get_sections');
  Route::get('/examination/get-map-sections/{id?}', 'AdminPanel\ExamMapController@get_map_sections');
  Route::post('/examination/save-classes/{id?}', 'AdminPanel\ExamMapController@save_classes');
  Route::get('/examination/map-classes/{id?}', 'AdminPanel\ExamMapController@view_map_classes');
  Route::post('/examination/map-save-classes/{id?}', 'AdminPanel\ExamMapController@map_save_classes');
  Route::get('/examination/remove-map-sections/{exam_id?}/{section_id?}', 'AdminPanel\ExamMapController@remove_map_section');
  Route::get('/examination/exam-class-subject-report', 'AdminPanel\ExamMapController@exam_class_report');
  Route::get('/examination/exam-class-subject-report-data', 'AdminPanel\ExamMapController@show_exam_class_report');
  Route::get('/examination/exam-subjects', 'AdminPanel\ExamMapController@show_exam_subjects');
  Route::get('/examination/manage-markcriteria-subject', 'AdminPanel\ExamMapController@manage_marks_criteria');
  Route::post('/examination/save-markcriteria-subject', 'AdminPanel\ExamMapController@save_marks_criteria');
  Route::get('/examination/manage-grade-scheme-subject', 'AdminPanel\ExamMapController@manage_grade_scheme');
  Route::post('/examination/save-gradescheme-subject', 'AdminPanel\ExamMapController@save_grade_scheme');
  Route::get('/examination/markscriteria-map-report', 'AdminPanel\ExamMapController@show_markscriteria_report');
  Route::get('/examination/show-markscriteria-report', 'AdminPanel\ExamMapController@show_marks_criteria_report');
  Route::get('/examination/gradescheme-map-report', 'AdminPanel\ExamMapController@show_gradescheme_report');
  Route::get('/examination/show-gradescheme-report', 'AdminPanel\ExamMapController@show_grade_scheme_report');
  Route::get('/examination/show-class-exam-report', 'AdminPanel\ExamMapController@show_exam_class_report_data');

  // For exam schedule
  Route::get('/examination/add-schedule/{id?}', 'AdminPanel\ExamScheduleController@add');
  Route::post('/examination/schedule/save/{id?}', 'AdminPanel\ExamScheduleController@save');
  Route::get('/examination/schedules', 'AdminPanel\ExamScheduleController@index');
  Route::get('/examination/schedule/data', 'AdminPanel\ExamScheduleController@anyData');
  Route::get('/examination/delete-schedule/{id?}', 'AdminPanel\ExamScheduleController@destroy');
  Route::get('/examination/schedules/manage-classes/{id?}', 'AdminPanel\ExamScheduleController@manage_classes');
  Route::get('/examination/schedules/get-subjects', 'AdminPanel\ExamScheduleController@get_subjects');
  Route::get('/examination/schedules/get-available-staff', 'AdminPanel\ExamScheduleController@get_available_staff');
  Route::post('/examination/schedules/save-schedule', 'AdminPanel\ExamScheduleController@save_schedule_map');
  Route::get('/examination/schedules/view-exam-schedules/{id?}', 'AdminPanel\ExamScheduleController@view_exam_schedule');
  Route::get('/examination/exam-schedule-data', 'AdminPanel\ExamScheduleController@view_exam_schedule_data');
  Route::get('/examination/exam-schedule-subjects', 'AdminPanel\ExamScheduleController@exam_schedule_subjects');
  Route::get('/examination/schedules/room-allocation/{id?}', 'AdminPanel\ExamScheduleController@room_allocation');
  Route::post('/examination/schedules/room-save/', 'AdminPanel\ExamScheduleController@room_save');
  Route::get('/examination/schedules/remove-room/{id?}', 'AdminPanel\ExamScheduleController@destroyAllocateRoom');
  Route::get('/examination/schedule/publish-schedule', 'AdminPanel\ExamScheduleController@publish_schedule');
  
  Route::get('/examination/manage-marks', 'AdminPanel\ExamMarksController@manage_marks');
  Route::get('/examination/get-marks-grid', 'AdminPanel\ExamMarksController@get_marks_grid');
  Route::post('/examination/save-student-marks', 'AdminPanel\ExamMarksController@save_student_marks');
  Route::post('/examination/marks-submission', 'AdminPanel\ExamMarksController@marks_submission');

  Route::get('/examination/manage-report-card', 'AdminPanel\ExamReportCardController@manage_report_card');
  Route::get('/examination/exam-class-data', 'AdminPanel\ExamReportCardController@classList');
  Route::get('/examination/generate-marksheet/{examid}/{classid}', 'AdminPanel\ExamReportCardController@generate_marksheet');
  Route::get('/examination/view-marksheet/{examid}/{classid}', 'AdminPanel\ExamReportCardController@view_marksheet');
  Route::get('/examination/student-result-table', 'AdminPanel\ExamReportCardController@get_students_result');
  Route::get('/examination/get-missing-marks-subjects/', 'AdminPanel\ExamReportCardController@get_missing_marks');
  Route::get('/examination/marksheet/{examid}/{classid}/{studentid}', 'AdminPanel\ExamReportCardController@show_marksheet');
  Route::post('/examination/save-result/', 'AdminPanel\ExamReportCardController@save_result');

  

  Route::get('/groups/add-group/{id?}', 'AdminPanel\GroupsController@add');
  Route::post('/groups/save/{id?}', 'AdminPanel\GroupsController@save');
  Route::get('/groups/view-student-groups', 'AdminPanel\GroupsController@student_group');
  Route::get('/groups/view-parent-groups', 'AdminPanel\GroupsController@parent_group');
  Route::get('/groups/group-data', 'AdminPanel\GroupsController@anyData');
  Route::get('/groups/delete-group/{id?}', 'AdminPanel\GroupsController@destroy');
  Route::get('/groups/group-status/{status?}/{id?}', 'AdminPanel\GroupsController@changeStatus');


  
  Route::get('/my-class/view-students', 'AdminPanel\MyClassController@view_students');
  Route::get('/my-class/get-students', 'AdminPanel\MyClassController@get_student_list');
  Route::get('/my-class/view-leaves', 'AdminPanel\MyClassController@view_student_leaves');
  Route::get('/my-class/get-student-leaves', 'AdminPanel\MyClassController@get_student_leaves');
  Route::get('/my-class/remarks/add-remark/{id?}', 'AdminPanel\MyClassController@add_remark');
  Route::post('/my-class/remarks/save-remark/{id?}', 'AdminPanel\MyClassController@save_remark');
  Route::get('/my-class/remarks/view-remarks', 'AdminPanel\MyClassController@view_remarks');
  Route::get('/my-class/remarks/remark-data', 'AdminPanel\MyClassController@reviewData');
  Route::get('/my-class/remarks/delete-remark/{id?}', 'AdminPanel\MyClassController@destroy_remark');
  Route::get('/my-class/manage-homework', 'AdminPanel\MyClassController@show_class_subjects');

  Route::get('/my-class/attendance/add-attendance/{id?}', 'AdminPanel\MyClassController@add_attendance');
  Route::get('/my-class/attendance/attendance-data', 'AdminPanel\MyClassController@attendance_data');
  Route::post('/my-class/attendance/save-attendance/{id?}', 'AdminPanel\MyClassController@attendance_save');
  Route::get('/my-class/attendance/view-attendance', 'AdminPanel\MyClassController@view_attendance');
  Route::get('/my-class/attendance/attendance-records', 'AdminPanel\MyClassController@attendance_records');

 

  Route::get('/staff/notice', 'AdminPanel\NoticeController@staff_notice');
  Route::get('/student/notice', 'AdminPanel\NoticeController@student_notice');


  Route::get('/my-subjects', 'AdminPanel\MySubjectsController@subjects');
  Route::get('/my-subjects/get-subjects', 'AdminPanel\MySubjectsController@get_subjects');
  Route::get('/my-subjects/view-students/{section?}/{subject?}', 'AdminPanel\MySubjectsController@view_students');
  Route::get('/my-subjects/get-students', 'AdminPanel\MySubjectsController@get_student_list');
  Route::get('/my-subjects/add-remark/{student?}/{subject?}/{id?}', 'AdminPanel\MySubjectsController@add_remark');
  Route::post('/my-subjects/save-remark/{id?}', 'AdminPanel\MySubjectsController@save_remark');
  Route::get('/my-subjects/view-remarks/{student?}/{subject?}', 'AdminPanel\MySubjectsController@view_remarks');
  Route::get('/my-subjects/remark-data/', 'AdminPanel\MySubjectsController@reviewData');
  Route::get('/my-subjects/delete-remark/{id?}', 'AdminPanel\MySubjectsController@destroy_remark');

  Route::get('/my-subjects/homework-group/{subject?}/{id?}', 'AdminPanel\MySubjectsController@homework');
  Route::post('/send-homework', 'AdminPanel\MySubjectsController@send_homework');

  Route::get('/notice-board/add-notice/{id?}', 'AdminPanel\NoticeController@add');
  Route::post('/notice-board/save/{id?}', 'AdminPanel\NoticeController@save');
  Route::get('/notice-board/view-notice', 'AdminPanel\NoticeController@index');
  Route::get('/notice-board/notice-data', 'AdminPanel\NoticeController@anyData');
  Route::get('/notice-board/delete-notice/{id?}', 'AdminPanel\NoticeController@destroy');
  Route::get('/notice-board/notice-status/{status?}/{id?}', 'AdminPanel\NoticeController@changeStatus');



  //For Inventory Module @Khushbu on 07 Jan 2019
  //For Vendor 
  Route::get('/inventory/manage-vendor/{id?}','AdminPanel\InventoryVendorController@add');
  Route::post('/inventory/manage-vendor-save/{id?}','AdminPanel\InventoryVendorController@save');
  Route::get('/inventory/manage-vendor-view','AdminPanel\InventoryVendorController@anyData');
  Route::get('/inventory/manage-vendor-status/{status?}/{id?}','AdminPanel\InventoryVendorController@changeStatus');
  Route::get('/inventory/delete-manage-vendor/{id?}','AdminPanel\InventoryVendorController@destroy');

  //For Items
  Route::get('/inventory/manage-items/{id?}','AdminPanel\InventoryItemController@add');
  Route::post('/inventory/manage-items-save/{id?}','AdminPanel\InventoryItemController@save');
  Route::get('/inventory/manage-items-view','AdminPanel\InventoryItemController@anyData');
  Route::get('/inventory/manage-items-status/{status?}/{id?}','AdminPanel\InventoryItemController@changeStatus');
  Route::get('/inventory/delete-manage-items/{id?}','AdminPanel\InventoryItemController@destroy');

  //For Ajax
  Route::get('/inventory/get-subcategory-data/{category_id?}','AdminPanel\InventoryCategoryController@getSubCategoryData');

  //For Category
  Route::get('/inventory/manage-category/{id?}','AdminPanel\InventoryCategoryController@add');
  Route::post('/inventory/manage-category-save/{id?}','AdminPanel\InventoryCategoryController@save');
  Route::get('/inventory/manage-category-view','AdminPanel\InventoryCategoryController@anyData');
  Route::get('/inventory/manage-category-status/{status?}/{id?}','AdminPanel\InventoryCategoryController@changeStatus');
  Route::get('/inventory/delete-manage-category/{id?}','AdminPanel\InventoryCategoryController@destroy');

  //For Units 
  Route::get('/inventory/manage-unit/{id?}','AdminPanel\InventoryUnitController@add');
  Route::post('/inventory/manage-unit-save/{id?}','AdminPanel\InventoryUnitController@save');
  Route::get('/inventory/manage-unit-view','AdminPanel\InventoryUnitController@anyData');
  Route::get('/inventory/manage-unit-status/{status?}/{id?}','AdminPanel\InventoryUnitController@changeStatus');
  Route::get('/inventory/delete-manage-unit/{id?}','AdminPanel\InventoryUnitController@destroy');



  // For Task Manager @Khushbu 08 Jan 2019
    Route::get('/task-manager/add-task/{id?}','AdminPanel\TaskManagerController@add');
    Route::post('/task-manager/save/{id?}','AdminPanel\TaskManagerController@save');
    Route::get('/task-manager/view-task','AdminPanel\TaskManagerController@index');
    Route::get('/task-manager/data','AdminPanel\TaskManagerController@anyData');
    Route::get('/task-manager/task-status/{status?}/{id?}','AdminPanel\TaskManagerController@changeStatus');
    Route::get('/task-manager/delete-task/{id?}','AdminPanel\TaskManagerController@destroy');
    // Route::get('/task-manager/view-task/view-responses','AdminPanel\TaskManagerController@viewResponses');
    // Route::get('/task-manager/view-task/mapping','AdminPanel\TaskManagerController@mapping');

  //For Student Module - My Remarks @Sandeep on 08 Jan 2019
  Route::get('/student/view-remarks', 'AdminPanel\RemarkController@student_view_remark');
  Route::get('/student/remarks/remark-student-data', 'AdminPanel\RemarkController@student_remark_data');
  Route::get('/daily-remarks/remark-student-single-data', 'AdminPanel\RemarkController@remark_student_single_data');


  //For Student Module - My homework @Sandeep on 09 Jan 2019
  Route::get('/student/view-homework', 'AdminPanel\StudentController@student_view_homework');


  // Student attendance module - My Attendence  @Sandeep on 09 Jan 2019
  Route::get('/student/student-attendance', 'AdminPanel\StudentController@student_attendance');
  Route::get('/student/get-student-attendance', 'AdminPanel\StudentController@get_student_attendance');

  // Student Time table @Sandeep on 09 Jan 2019
  Route::get('/student/student-time-table', 'AdminPanel\TimeTableController@student_time_table');

// Student Examination @Sandeep on 10 Jan 2019
  Route::get('/student/examination', 'AdminPanel\ExamScheduleController@student_examination');

// Student Examination @Sandeep on 14 Jan 2019
  Route::get('/examination/exam-schedule-data-student', 'AdminPanel\ExamScheduleController@view_exam_schedule_data_student');

// Student Examination @Sandeep on 14 Jan 2019
  Route::get('/examination/exam-schedule-subjects-student', 'AdminPanel\ExamScheduleController@exam_schedule_subjects_student');

// Student Examination @Sandeep on 14 Jan 2019
  Route::get('/examination/exam-marks-student', 'AdminPanel\ExamScheduleController@exam_marks_student');

// Student online Content @Sandeep on 18 Jan 2019
  Route::get('/student-online-content', 'AdminPanel\NoteController@student_notes');  

// Student Leave Management @Sandeep on 18 Jan 2019
  Route::get('/view-student-leave', 'AdminPanel\StudentLeaveApplicationController@view_student_leaves');

  // Set Student Session @Sandeep on 21 Jan 2019
  Route::post('/set-student-session', 'AdminPanel\DashboardController@set_student_session');

  // Student parent Details @Sandeep on 23 Jan 2019
  Route::get('/student/student-parent-detail/{id?}', 'AdminPanel\StudentController@student_parent_detail');

  // Student profile Details @Sandeep on 23 Jan 2019
  Route::get('/student/view-student-profile/{id?}', 'AdminPanel\StudentController@view_student_profile');


  // Transport vehicle Module @sandeep 0n 25 Jan 2019 
    Route::get('/transport/vehicle/add-vehicle/{id?}', 'AdminPanel\VehicleController@add');
    Route::post('/transport/vehicle/save/{id?}', 'AdminPanel\VehicleController@save');
    Route::get('/transport/vehicle/view-vehicle', 'AdminPanel\VehicleController@index');
    Route::get('/transport/vehicle/data', 'AdminPanel\VehicleController@anyData');
    Route::get('/transport/vehicle/delete-vehicle/{id?}', 'AdminPanel\VehicleController@destroy');
    Route::get('/transport/vehicle/vehicle-status/{status?}/{id?}', 'AdminPanel\VehicleController@changeStatus');
    Route::get('/transport/vehicle/vehicle-documents', 'AdminPanel\VehicleController@vehicle_documents');
    Route::get('/transport/vehicle/delete-vehicle-document/{id?}', 'AdminPanel\VehicleController@destroy_document');



  // For Invoice Configuration @Khushbu 23 Jan 2019
  Route::get('/inventory/manage-invoice-configuration/{id?}','AdminPanel\InventoryConfigurationController@add');
  // For Invoice Configuration @Khushbu 24 Jan 2019
  Route::post('/inventory/manage-invoice-configuration-save/{id}','AdminPanel\InventoryConfigurationController@save');
  Route::get('/inventory/manage-invoice-configuration-view','AdminPanel\InventoryConfigurationController@anyData');

  // For Purchase @Khushbu 24 Jan 2019
  Route::get('/inventory/purchase/add-purchase/{id?}','AdminPanel\PurchaseController@add');
  Route::post('/inventory/purchase/save/{id?}','AdminPanel\PurchaseController@save');
  Route::get('/inventory/purchase/view-purchase','AdminPanel\PurchaseController@index');
  Route::get('/inventory/purchase/view-purchase-data','AdminPanel\PurchaseController@anyData');
  Route::get('/inventory/purchase/delete-purchase/{id?}','AdminPanel\PurchaseController@destroy');
  Route::get('/inventory/purchase/view-purchase/item-details/{id?}','AdminPanel\PurchaseController@itemDetails');
  Route::get('/inventory/purchase/view-purchase/item-details-data','AdminPanel\PurchaseController@anyDataItemDetails');
  Route::get('/inventory/purchase/view-purchase-model-data/{id?}','AdminPanel\PurchaseController@getPurchaseData');
  Route::get('pdfview',array('as'=>'pdfview','uses'=>'AdminPanel\PurchaseController@pdfview'));
  Route::get('/inventory/delete-purchase-item-data/{id?}','AdminPanel\PurchaseController@puchaseItemDelete');
  
  //For Ajax
  Route::get('/inventory/get-items-data/{sub_category_id?}','AdminPanel\InventoryItemController@getItemsData');
  Route::get('/inventory/get-unit-data/{item_id?}','AdminPanel\InventoryUnitController@getUnitData');  

  // For Payroll Menu
  Route::get('/payroll', 'AdminPanel\MenuController@payroll');

  // For Payroll Document Module
  Route::get('/payroll/manage-department/{id?}','AdminPanel\PayrollDepartmentController@add'); 
  Route::post('/payroll/manage-department-save/{id?}','AdminPanel\PayrollDepartmentController@save');
  Route::get('/payroll/manage-department-view','AdminPanel\PayrollDepartmentController@anyData');
  Route::get('/payroll/manage-department-status/{status?}/{id?}','AdminPanel\PayrollDepartmentController@changeStatus');
  Route::get('/payroll/delete-manage-department/{id?}','AdminPanel\PayrollDepartmentController@destroy');
  Route::get('/payroll/manage-department/view-map-employees/{id?}','AdminPanel\PayrollDepartmentController@viewMapEmployees');
  Route::get('/payroll/manage-department-employees-data','AdminPanel\PayrollDepartmentController@anyDataMapEmployees');
  Route::post('/payroll/manage-department-map-employees-save/{id?}','AdminPanel\PayrollDepartmentController@saveMapEmployees');
  
  // For Payroll Grade Module
  Route::get('/payroll/manage-grade/{id?}','AdminPanel\PayrollGradeController@add'); 
  Route::post('/payroll/manage-grade-save/{id?}','AdminPanel\PayrollGradeController@save');
  Route::get('/payroll/manage-grade-view','AdminPanel\PayrollGradeController@anyData');
  Route::get('/payroll/manage-grade-status/{status?}/{id?}','AdminPanel\PayrollGradeController@changeStatus');
  Route::get('/payroll/delete-manage-grade/{id?}','AdminPanel\PayrollGradeController@destroy');
  Route::get('/payroll/manage-grade/view-map-employees/{id?}','AdminPanel\PayrollGradeController@viewMapEmployees');
  Route::get('/payroll/manage-grade-employees-data','AdminPanel\PayrollGradeController@anyDataMapEmployees');
  Route::post('/payroll/manage-grade-map-employees-save/{id?}','AdminPanel\PayrollGradeController@saveMapEmployees');

  // Route::get('/payroll/manage-leave-scheme','AdminPanel\PayrollController@manageLeaveScheme');  
  // Route::get('/payroll/manage-leave-scheme/view-map-employees','AdminPanel\PayrollController@leaveSchemeMapEmployees'); 

  // Route::get('/payroll/manage-salary-generation','AdminPanel\PayrollController@manageSalaryGeneration'); 

    // Transport route Module @sandeep 0n 1 Feb 2019 
    Route::get('/transport/route/add-route/{id?}', 'AdminPanel\RouteController@add');
    Route::post('/transport/route/save/{id?}', 'AdminPanel\RouteController@save');
    Route::get('/transport/route/view-route', 'AdminPanel\RouteController@index');
    Route::get('/transport/route/data', 'AdminPanel\RouteController@anyData');
    Route::get('/transport/route/delete-route/{id?}', 'AdminPanel\RouteController@destroy');
    Route::get('/transport/route/route-status/{status?}/{id?}', 'AdminPanel\RouteController@changeStatus');


    // Transport manage route Module @sandeep 0n 3 Feb 2019 
    Route::get('/transport/route/manage-route', 'AdminPanel\RouteController@manage_route');
    Route::get('/transport/map-route-data', 'AdminPanel\RouteController@map_route_data');
    Route::post('/transport/manage-route/save', 'AdminPanel\RouteController@save_manage_route');


    // Transport Fees Head Module @sandeep 0n 3 Feb 2019 
    Route::get('/transport/fees-head/add-fees-head/{id?}', 'AdminPanel\FeesHeadController@add');
    Route::post('/transport/fees-head/save/{id?}', 'AdminPanel\FeesHeadController@save');
    Route::get('/transport/fees-head/view-fees-head', 'AdminPanel\FeesHeadController@index');
    Route::get('/transport/fees-head/data', 'AdminPanel\FeesHeadController@anyData');
    Route::get('/transport/fees-head/delete-fees-head/{id?}', 'AdminPanel\FeesHeadController@destroy');
    Route::get('/transport/fees-head/fees-head-status/{status?}/{id?}', 'AdminPanel\FeesHeadController@changeStatus');


    // Transport mappping driver conductor Module @sandeep 0n 4 Feb 2019 
    Route::POST('/transport/vehicle/manage-vehicle-data', 'AdminPanel\VehicleController@mapping_vehicle');
    Route::post('/transport/vehicle/manage-vehicle/save', 'AdminPanel\VehicleController@save_map_vehicle');


    // Transport route Module @sandeep 0n 5 Feb 2019 
    Route::post('/transport/route/delete-route-location', 'AdminPanel\RouteController@destroy_route_location');
    Route::get('/transport/route/get-route-location', 'AdminPanel\RouteController@get_route_location');

    // Transport mappping student staff vehicle Module @sandeep 0n 6 Feb 2019 
    Route::get('/transport/vehicle/manage-student-staff', 'AdminPanel\VehicleController@manage_student_staff');
    Route::get('/transport/vehicle/manage-student-staff-data', 'AdminPanel\VehicleController@manage_student_staff_data');


    // Transport manage staff @sandeep 0n 7 Feb 2019 
    Route::get('/transport/vehicle/manage-staff/{id}', 'AdminPanel\VehicleController@manage_staff');
    Route::get('/transport/vehicle/manage-staff-data', 'AdminPanel\VehicleController@manage_staff_data');
    Route::post('/transport/vehicle/get-manage-route', 'AdminPanel\VehicleController@get_manage_route');
    Route::post('/transport/vehicle/get-manage-route-save/{id}', 'AdminPanel\VehicleController@get_manage_route_save');
    Route::post('/transport/vehicle/get-manage-route-location', 'AdminPanel\VehicleController@get_manage_route_location');


    // Transport manage student @sandeep 0n 7 Feb 2019 
    Route::get('/transport/vehicle/manage-student/{id}', 'AdminPanel\VehicleController@manage_student');
    Route::get('/transport/vehicle/student-list-data', 'AdminPanel\VehicleController@student_list_data');


    // Transport view all staff @sandeep 0n 8 Feb 2019 
    Route::get('/transport/vehicle/view-all-staff/{id}', 'AdminPanel\VehicleController@view_all_staff');
    Route::get('/transport/vehicle/view-all-staff-data', 'AdminPanel\VehicleController@view_all_staff_data');
    Route::get('/transport/vehicle/view-all-staff/delete-vehicle-staff/{id?}', 'AdminPanel\VehicleController@destroy_staff_vehicle');

    // Transport view all student @sandeep 0n 8 Feb 2019 
    Route::get('/transport/vehicle/view-all-student/{id}', 'AdminPanel\VehicleController@view_all_student');
    Route::get('/transport/vehicle/view-all-student-data', 'AdminPanel\VehicleController@view_all_student_data');
    Route::get('/transport/vehicle/view-all-student/delete-vehicle-student/{id?}', 'AdminPanel\VehicleController@destroy_student_vehicle');


    // Transport manage vehicle attendence Module @sandeep 0n 8 Feb 2019 
    Route::get('/transport/vehicle/vehicle-attendance', 'AdminPanel\VehicleController@vehicle_attendence');
    Route::get('/transport/vehicle/vehicle-attendance-data', 'AdminPanel\VehicleController@vehicle_attendence_data');
    Route::get('/transport/vehicle/add-attendance/{id}/{vehicle_attendence_id?}', 'AdminPanel\VehicleController@add_attendence');
    Route::get('/transport/vehicle/attendance-data', 'AdminPanel\VehicleController@attendance_data');
    Route::get('/transport/vehicle/view-attendance/{id}', 'AdminPanel\VehicleController@view_attendence');


    Route::post('/transport/vehicle/save-attendance/{id?}', 'AdminPanel\VehicleController@save_attendence');
    Route::get('/transport/vehicle/attendance/attendance-records', 'AdminPanel\VehicleController@attendance_records');
    Route::get('/transport/vehicle/view-attendance-details/{id}/{vehicle_attendence_id?}', 'AdminPanel\VehicleController@vehicle_attendence_details');
    Route::get('/transport/vehicle/view-attendance-details-data', 'AdminPanel\VehicleController@vehicle_attendence_details_data');
    // Route::POST('/transport/vehicle/manage-attendence', 'AdminPanel\VehicleController@mapping_vehicle');


  // Payroll Configuration Module @Khushbu on 06 Feb 2019
  Route::get('/payroll/configuration','AdminPanel\PayrollConfigurationController@add'); 
  Route::get('/payroll/configuration-data','AdminPanel\PayrollConfigurationController@anydata');
  Route::post('/payroll/configuration-save','AdminPanel\PayrollConfigurationController@save');
  
  // Payroll Pf Setting Module @Khushbu on 07 Feb 2019
  Route::get('/payroll/manage-pf-setting/{id?}','AdminPanel\PayrollPfEsiTdsPtController@addPf');
  Route::post('/payroll/manage-pf-setting-save/{id?}','AdminPanel\PayrollPfEsiTdsPtController@savePf');
  Route::get('/payroll/manage-pf-setting-view','AdminPanel\PayrollPfEsiTdsPtController@anyDataPf');
  Route::get('/payroll/delete-manage-pf-setting/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@destroyPf');
  Route::get('/payroll/manage-pf-setting-status/{status?}/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@changeStatusPf');
  // Payroll TDS Setting Module @Khushbu on 08 Feb 2019
  Route::get('/payroll/manage-tds-setting/{id?}','AdminPanel\PayrollPfEsiTdsPtController@addTds');
  Route::post('/payroll/manage-tds-setting-save/{id?}','AdminPanel\PayrollPfEsiTdsPtController@saveTds');
  Route::get('/payroll/manage-tds-setting-view','AdminPanel\PayrollPfEsiTdsPtController@anyDataTds');
  Route::get('/payroll/delete-manage-tds-setting/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@destroyTds');
  Route::get('/payroll/manage-tds-setting-status/{status?}/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@changeStatusTds');

  // Payroll Esi Setting Module @Khushbu on 08 Feb 2019
  Route::get('/payroll/manage-esi-setting/{id?}','AdminPanel\PayrollPfEsiTdsPtController@addEsi');
  Route::post('/payroll/manage-esi-setting-save/{id?}','AdminPanel\PayrollPfEsiTdsPtController@saveEsi');
  Route::get('/payroll/manage-esi-setting-view','AdminPanel\PayrollPfEsiTdsPtController@anyDataEsi');
  Route::get('/payroll/delete-manage-esi-setting/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@destroyEsi');
  Route::get('/payroll/manage-esi-setting-status/{status?}/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@changeStatusEsi');

  // Payroll PT Setting Module @Khushbu on 08 Feb 2019
  Route::get('/payroll/manage-pt-setting/{id?}','AdminPanel\PayrollPfEsiTdsPtController@addPt');
  Route::post('/payroll/manage-pt-setting-save/{id?}','AdminPanel\PayrollPfEsiTdsPtController@savePt');
  Route::get('/payroll/manage-pt-setting-view','AdminPanel\PayrollPfEsiTdsPtController@anyDataPt');
  Route::get('/payroll/delete-manage-pt-setting/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@destroyPt');
  Route::get('/payroll/manage-pt-setting-status/{status?}/{id?}', 'AdminPanel\PayrollPfEsiTdsPtController@changeStatusPt');
  Route::get('/payroll/pt-setting-model-data/{id?}','AdminPanel\PayrollPfEsiTdsPtController@getPtSettingData');

  // Reporting vehicle attendence Module @sandeep 0n 11 Feb 2019 
  Route::get('/transport/vehicle/attendance-report', 'AdminPanel\VehicleController@attendence_report');
  Route::get('/transport/vehicle/attendance/report-attendance-records', 'AdminPanel\VehicleController@report_attendance_records');

  // Recruitment Module - Job @sandeep 0n 11 Feb 2019 
  Route::get('/recruitment/job/add-job/{id?}', 'AdminPanel\JobController@add');
  Route::post('/recruitment/job/save/{id?}', 'AdminPanel\JobController@save');
  Route::get('/recruitment/job/view-job', 'AdminPanel\JobController@index');
  Route::get('/recruitment/job/data', 'AdminPanel\JobController@anyData');
  Route::get('/recruitment/job/delete-job/{id?}', 'AdminPanel\JobController@destroy');
  Route::get('/recruitment/job/job-status/{status?}/{id?}', 'AdminPanel\JobController@changeStatus');
  Route::get('/recruitment/job/single-recruitment-description', 'AdminPanel\JobController@single_recruitment_description');

  // For Hostel Fees Head @Khushbu 18 Jan 2019
  Route::get('/hostel/manage-hostel-fees-head/{id?}', 'AdminPanel\HostelController@add_h_fees_head');
  Route::post('/hostel/save-hostel-fees-head/{id?}', 'AdminPanel\HostelController@save_h_fees_head');
  Route::get('/hostel/data-hostel-fees-head', 'AdminPanel\HostelController@anyDataHostelFeesHead');
  Route::get('/hostel/delete-hostel-fees-head/{id?}', 'AdminPanel\HostelController@destroyHostelFeesHead');
  Route::get('/hostel/hostel-fess-head-status/{status?}/{id?}', 'AdminPanel\HostelController@changeStatusHostelFeesHead');
 

    // Transport Recruitment Module - Form @sandeep 0n 12 Feb 2019
    Route::get('/recruitment/forms/add-form/{id?}', 'AdminPanel\JobFormController@add');
    Route::post('/recruitment/forms/save/{id?}', 'AdminPanel\JobFormController@save');
    Route::get('/recruitment/forms/view-form', 'AdminPanel\JobFormController@index');
    Route::get('/recruitment/forms/data', 'AdminPanel\JobFormController@anyData');
    Route::get('/recruitment/forms/delete-job-form/{id}', 'AdminPanel\JobFormController@destroy');
    Route::get('/recruitment/forms/form-status/{status?}/{id?}', 'AdminPanel\JobFormController@changeStatus');
    Route::get('/recruitment/forms/manage-fields/{id?}', 'AdminPanel\JobFormController@manageFieldsView');
    Route::post('/recruitment/forms/save-manage-fields/{id?}', 'AdminPanel\JobFormController@saveManageFields');
    Route::get('/recruitment/recruitment-forms', 'AdminPanel\JobFormController@recruitment_forms');
    Route::get('/recruitment/recruitment-form-data', 'AdminPanel\JobFormController@recruitment_forms_date');
    
  // For Payroll Arrear Module @Khushbu on 15 Feb 2019
  Route::get('/payroll/manage-arrear/{id?}','AdminPanel\PayrollArrearController@add'); 
  Route::post('/payroll/manage-arrear-save/{id?}','AdminPanel\PayrollArrearController@save');
  Route::get('/payroll/manage-arrear-view','AdminPanel\PayrollArrearController@anyData');
  Route::get('/payroll/delete-manage-arrear/{id?}','AdminPanel\PayrollArrearController@destroy');
  Route::get('/payroll/manage-arrear/view-map-employees/{id?}','AdminPanel\PayrollArrearController@viewMapEmployees');
  Route::get('/payroll/manage-arrear-employees-data','AdminPanel\PayrollArrearController@getArrearStaffData');
  // Route::post('/payroll/manage-arrear-map-employees-save/{id?}','AdminPanel\PayrollArrearController@saveMapEmployees');
  // Route::get('/payroll/view-staff-model-data/{id?}','AdminPanel\PayrollArrearController@getArrearStaffData');
  
  // For Payroll Bonus Module @Khushbu on 15 Feb 2019
  Route::get('/payroll/manage-bonus/{id?}','AdminPanel\PayrollBonusController@add'); 
  Route::post('/payroll/manage-bonus-save/{id?}','AdminPanel\PayrollBonusController@save');
  Route::get('/payroll/manage-bonus-view','AdminPanel\PayrollBonusController@anyData');
  Route::get('/payroll/delete-manage-bonus/{id?}','AdminPanel\PayrollBonusController@destroy');
  Route::get('/payroll/manage-bonus/view-map-employees/{id?}','AdminPanel\PayrollBonusController@viewMapEmployees');
  Route::get('/payroll/manage-bonus-employees-data','AdminPanel\PayrollBonusController@getBonusStaffData');
  Route::post('/payroll/manage-bonus-map-employees-save/{id?}','AdminPanel\PayrollBonusController@saveMapEmployees');

    // Applied Canditates Module - Form @sandeep 0n 14 Feb 2019
    Route::get('/recruitment/applied-candidates', 'AdminPanel\JobFormController@applied_candidates');
    Route::get('/recruitment/applied-candidates-data', 'AdminPanel\JobFormController@applied_candidates_data');
    Route::post('/recruitment/schedule-approved-disaaproved', 'AdminPanel\JobFormController@schedule_approved_disaaproved');
    Route::post('/recruitment/schedule-approved-disaaproved-save', 'AdminPanel\JobFormController@schedule_approved_disaaproved_save');

    // Selected Canditates Module - Form @sandeep 0n 15 Feb 2019
    Route::get('/recruitment/selected-candidates', 'AdminPanel\JobFormController@selected_candidates');
    Route::get('/recruitment/selected-candidates-data', 'AdminPanel\JobFormController@selected_candidates_data');
    Route::get('/recruitment/reporting', 'AdminPanel\JobFormController@reporting');
    Route::get('/recruitment/reporting_data', 'AdminPanel\JobFormController@reporting_data');

    // Selected Canditates Module - Form @sandeep 0n 16 Feb 2019
    Route::get('/recruitment/view-details/{id}', 'AdminPanel\JobFormController@canditate_deatils');

    // For Payroll Salary Head @Khushbu on 19 Feb 2019
    Route::get('/payroll/manage-salary-head/{id?}','AdminPanel\PayrollSalaryHeadController@add'); 
    Route::post('/payroll/manage-salary-head-save/{id?}','AdminPanel\PayrollSalaryHeadController@save');
    Route::get('/payroll/manage-salary-head-view','AdminPanel\PayrollSalaryHeadController@anyData');
    Route::get('/payroll/manage-salary-head-status/{status?}/{id?}','AdminPanel\PayrollSalaryHeadController@changeStatus');
    Route::get('/payroll/delete-manage-salary-head/{id?}','AdminPanel\PayrollSalaryHeadController@destroy');

    // For Payroll Loan @Khushbu on 19 Feb 2019
    Route::get('/payroll/manage-loan/{id?}','AdminPanel\PayrollLoanController@add'); 
    Route::post('/payroll/manage-loan-save/{id?}','AdminPanel\PayrollLoanController@save');
    Route::get('/payroll/manage-loan-view','AdminPanel\PayrollLoanController@anyData');
    Route::get('/payroll/delete-manage-loan/{id?}','AdminPanel\PayrollLoanController@destroy');

    // For Payroll Advance @Khushbu on 20 Feb 2019
    Route::get('/payroll/manage-advance/{id?}','AdminPanel\PayrollAdvanceController@add'); 
    Route::post('/payroll/manage-advance-save/{id?}','AdminPanel\PayrollAdvanceController@save');
    Route::get('/payroll/manage-advance-view','AdminPanel\PayrollAdvanceController@anyData');
    Route::get('/payroll/delete-manage-advance/{id?}','AdminPanel\PayrollAdvanceController@destroy');

    // For Payroll Salary Structure @Khushbu on 20 Feb 2019
    Route::get('/payroll/manage-salary-structure/{id?}','AdminPanel\SalaryStructureController@add'); 
    Route::post('/payroll/manage-salary-structure-save/{id?}','AdminPanel\SalaryStructureController@save');
    Route::get('/payroll/manage-salary-structure-view','AdminPanel\SalaryStructureController@anyData');
    Route::get('/payroll/delete-manage-salary-structure/{id?}','AdminPanel\SalaryStructureController@destroy');
    Route::get('/payroll/manage-salary-structure-data','AdminPanel\SalaryStructureController@getSalaryHeadData');
    Route::get('/payroll/salary-structure-map-staff', 'AdminPanel\SalaryStructureController@getStaffMap');
    Route::get('/payroll/salary-structure-map-staff-data','AdminPanel\SalaryStructureController@anyDataMapEmployees');
    Route::post('/payroll/salary-structure-map-staff-save','AdminPanel\SalaryStructureController@saveStaffMap');
    
    // For Task manager @Sandeep on 20 Feb 2019
    Route::get('/task-manager/get-student-data','AdminPanel\TaskManagerController@get_student_data');
    Route::get('/task-manager/view-response/{id?}','AdminPanel\TaskManagerController@view_task_response');
    Route::post('/task-manager/send-task-response', 'AdminPanel\TaskManagerController@send_task_response');

    // For Staff Task manager @Sandeep on 21 Feb 2019
    Route::get('/my-task','AdminPanel\TaskManagerController@my_task');
    Route::get('/my-task-data','AdminPanel\TaskManagerController@my_taskData');
    Route::get('/view-response/{id?}','AdminPanel\TaskManagerController@my_view_task_response');
    Route::post('/send-task-response', 'AdminPanel\TaskManagerController@my_send_task_response');

    // Studenc CC
    Route::get('/student/issue-character-certificate', 'AdminPanel\CcController@add_cc');
    Route::get('/student/get-cc-records', 'AdminPanel\CcController@get_cc_records');
    Route::post('/student/save-cc', 'AdminPanel\CcController@save_cc');
    Route::get('/student/view-issue-character-certificate', 'AdminPanel\CcController@index');
    Route::get('/student/cancel-character-certificate/{id}', 'AdminPanel\CcController@cancel_cc');
    Route::get('/student/character-certificate/{id}', 'AdminPanel\CcController@cc_certificate');

    Route::get('/my-class/homework-group', 'AdminPanel\MyClassController@homework');
    Route::post('/my-class/send-homework', 'AdminPanel\MyClassController@send_homework');
    // For Parent communication  @Sandeep on 23 Feb 2019
    Route::get('/student/communication', 'AdminPanel\CommunicationController@index');
    Route::get('/student/communication-staff-data', 'AdminPanel\CommunicationController@communication_staff_data');

    // For Parent communication  @Sandeep on 24 Feb 2019
    Route::get('/communication/{id?}/{sender_type?}','AdminPanel\CommunicationController@communication');
    Route::post('/send-communication-response', 'AdminPanel\CommunicationController@send_communication_response');
    Route::get('/student-class/view-parents/{section?}/{subject?}/{class?}', 'AdminPanel\CommunicationController@view_parents');
    Route::get('/student-class/get-parents', 'AdminPanel\CommunicationController@get_parents_list');

    // Marksheet Templates
    Route::get('/marksheet-templates/examwise-templates', 'AdminPanel\MarksheetTemplateController@examwise_templates');
    Route::get('/marksheet-templates/termwise-templates', 'AdminPanel\MarksheetTemplateController@termwise_templates');
    Route::get('/marksheet-templates/annual-templates', 'AdminPanel\MarksheetTemplateController@annual_templates');
    Route::get('/marksheet-templates/template-data', 'AdminPanel\MarksheetTemplateController@anyData');
    Route::get('/marksheet-templates/template-status/{status?}/{id?}', 'AdminPanel\MarksheetTemplateController@changeStatus');

    Route::get('/marksheet-templates', 'AdminPanel\MenuController@marksheet_templates'); // For Marksheet Template menu

    Route::get('/marksheet-templates/examwise-template1', 'AdminPanel\MarksheetTemplateController@examwise_template1');
    Route::get('/marksheet-templates/termwise-template1', 'AdminPanel\MarksheetTemplateController@termwise_template1');
    Route::get('/marksheet-templates/annual-template1', 'AdminPanel\MarksheetTemplateController@annual_template1');
    Route::get('/examination/tabsheet', 'AdminPanel\ExamReportCardController@tabsheet'); 
    Route::post('/examination/get-tabsheet-grid', 'AdminPanel\ExamReportCardController@get_tabsheet_grid'); 

    Route::get('/transport/tracking-api', 'AdminPanel\RouteController@traking_api');

    // For Purchase Return
    Route::get('/inventory/manage-purchase-return','AdminPanel\PurchaseController@manage_purchase_return');
    Route::get('/inventory/get-purchase-return','AdminPanel\PurchaseController@get_purchase_return'); 
    Route::post('/inventory/purchase-return/save','AdminPanel\PurchaseController@purchase_return_save'); 


    // Hostel Ajax
    Route::get('/hostel/get-hostel-fees-head-data/{id?}', 'AdminPanel\HostelController@getFeesHeadData');

    // For Examination Reports
    Route::get('/examination-report', 'AdminPanel\MenuController@examination_reports');
    Route::get('/examination-report/examwise-marks-report', 'AdminPanel\ExaminationReportController@examWiseMarks'); 
    Route::post('/examination-report/examwise-marks-report-data', 'AdminPanel\ExaminationReportController@get_tabsheet_grid');
    Route::get('/examination-report/subjectwise-marks-report', 'AdminPanel\ExaminationReportController@subjectWiseMarks'); 
    Route::get('/examination-report/show-subjectwise-marks-report', 'AdminPanel\ExaminationReportController@showSubjectwiseMarksData'); 
    Route::get('/examination-report/get-subject-data', 'AdminPanel\ExaminationReportController@getSubjectData'); 
    Route::get('/examination-report/percentage-report','AdminPanel\ExaminationReportController@getPerwiseReport');
    Route::get('/examination-report/percentage-report-data','AdminPanel\ExaminationReportController@getPerwiseReportData');
    Route::get('/examination-report/rank-report','AdminPanel\ExaminationReportController@getRankReport');
    Route::get('/examination-report/rank-report-data','AdminPanel\ExaminationReportController@getRankReportData');
    Route::get('/examination-report/topper-list-report','AdminPanel\ExaminationReportController@getTopperReport');
    Route::get('/examination-report/topper-list-report-data','AdminPanel\ExaminationReportController@getTopperReportData');

    // For Admission Reports
    Route::get('/admission-report', 'AdminPanel\MenuController@admission_reports');
    Route::get('/admission-report/classwise-admission-report', 'AdminPanel\AdmissionReportController@classWiseAdmission');
    Route::get('/admission-report/show-classwise-admission-report', 'AdminPanel\AdmissionReportController@showClassWiseAdmission');
    Route::get('/admission-report/classwise-enquiry-report', 'AdminPanel\AdmissionReportController@classWiseEnquiry');
    Route::get('/admission-report/show-classwise-enquiry-report', 'AdminPanel\AdmissionReportController@showClassWiseEnquiry');
    Route::get('/admission-report/formwise-fees-report', 'AdminPanel\AdmissionReportController@formWiseFees');
    Route::get('/admission-report/show-formwise-fees-report', 'AdminPanel\AdmissionReportController@showFormWiseFees');


        // For Sales @Sandeep 1 march 2019
  Route::get('/inventory/sale/add-sale/{id?}','AdminPanel\SaleController@add');
  Route::post('/inventory/sale/save/{id?}','AdminPanel\SaleController@save');
  Route::get('/inventory/sale/view-sale','AdminPanel\SaleController@index');
  Route::get('/inventory/sale/view-sale-data','AdminPanel\SaleController@anyData');
  Route::get('/inventory/sale/delete-sale/{id?}','AdminPanel\SaleController@destroy');
  Route::get('/inventory/sale/view-sale/item-details/{id?}','AdminPanel\SaleController@itemDetails');
  Route::get('/inventory/sale/view-sale/item-details-data','AdminPanel\SaleController@anyDataItemDetails');
  Route::get('/inventory/sale/view-sale-model-data/{id?}','AdminPanel\SaleController@getSaleData');
  Route::get('salespdfview',array('as'=>'salespdfview','uses'=>'AdminPanel\SaleController@salespdfview'));
  Route::get('/inventory/delete-sale-item-data/{id?}','AdminPanel\SaleController@saleItemDelete');


  // For Sale Return
    Route::get('/inventory/manage-sales-return','AdminPanel\SaleController@manage_Sale_return');
    Route::get('/inventory/get-sale-return','AdminPanel\SaleController@get_Sale_return'); 
    Route::post('/inventory/sale-return/save','AdminPanel\SaleController@Sale_return_save'); 

    Route::get('/inventory/manage-stock-register','AdminPanel\SaleController@manage_stock_register');
    
    // For Student Reports
    Route::get('/student-report', 'AdminPanel\MenuController@student_reports');
    Route::get('/student-report/student-attendance-report', 'AdminPanel\StudentReportController@studentAttendance');
    Route::get('/student-report/student-attendance-report-data', 'AdminPanel\StudentReportController@anyDataStudentAttendance');
    Route::get('/student-report/examwise-marks-report', 'AdminPanel\ExaminationReportController@examWiseMarks'); 
    Route::post('/student-report/examwise-marks-report-data', 'AdminPanel\ExaminationReportController@get_tabsheet_grid');
    Route::get('/student-report/subjectwise-marks-report', 'AdminPanel\ExaminationReportController@subjectWiseMarks'); 
    Route::get('/student-report/show-subjectwise-marks-report', 'AdminPanel\ExaminationReportController@showSubjectwiseMarksData'); 
  
    // For Staff Reports
    Route::get('/staff/staff-attendance-report', 'AdminPanel\StaffReportController@staffAttendance');
    Route::get('/staff/staff-attendance-report-data', 'AdminPanel\StaffReportController@anyDataStaffAttendance');

    // For Fees Collection Reports
    Route::get('/fees-collection-report', 'AdminPanel\MenuController@fees_collection_reports');
    Route::get('/fees-collection/rte-cheques-report', 'AdminPanel\FeesReportController@rteCheques');
    Route::get('/fees-collection/rte-cheques-report-data', 'AdminPanel\FeesReportController@anyDataRteCheques');
    Route::get('/fees-collection/imprest-entries-report', 'AdminPanel\FeesReportController@imprestEntries');
    Route::get('/fees-collection/imprest-entries-report-data', 'AdminPanel\FeesReportController@anyDataImprestEntries');
    Route::any('/fees-collection/class-monthly-sheet', 'AdminPanel\FeesReportController@classMonthly');
    // Route::any('/fees-collection/class-monthly-sheet-data', 'AdminPanel\FeesReportController@classMonthly');
    Route::any('/fees-collection/student-monthly-sheet', 'AdminPanel\FeesReportController@studentMonthly');
    Route::get('/student/get-student-data/{id?}','AdminPanel\FeesReportController@getStudentsData');
    Route::any('/fees-collection/student-headwise-report', 'AdminPanel\FeesReportController@studentHeadwise');
    Route::any('/fees-collection/student-headwise-report-data', 'AdminPanel\FeesReportController@anyDataStudentHeadwise');
    Route::get('/fees-collection/cheques-report', 'AdminPanel\FeesReportController@cheques');
    Route::get('/fees-collection/cheques-report-data', 'AdminPanel\FeesReportController@anyDatacheques');
    Route::get('/fees-collection/concession-report', 'AdminPanel\FeesReportController@concession');
    Route::get('/fees-collection/concession-report-data', 'AdminPanel\FeesReportController@anyDataConcession');

    Route::get('/fees-collection/student-fine-report', 'AdminPanel\FeesReportController@fine');
    Route::get('/fees-collection/student-fine-report-data', 'AdminPanel\FeesReportController@anyDatafine');
    Route::any('/fees-collection/class-headwise-report', 'AdminPanel\FeesReportController@classHeadwise');
    Route::any('/fees-collection/class-headwise-report-data', 'AdminPanel\FeesReportController@anyDataClassHeadwise');
  
    
    // Student Examination @Sandeep on 6 march 2019
  Route::get('/examination/exam-marks-student-total', 'AdminPanel\ExamScheduleController@exam_marks_student_total');

  // Hostel Fees counter
  Route::get('/hostel/fees-counter', 'AdminPanel\HostelFeeController@index');
  Route::get('/hostel/fees-counter/add/{mapid}/{student_id}', 'AdminPanel\HostelFeeController@add');
  Route::get('/hostel/fees-counter/get-student-list', 'AdminPanel\HostelFeeController@get_student_list');
  Route::post('/hostel/fees-counter/save-fees-data/{id?}', 'AdminPanel\HostelFeeController@save_fees_data');
  Route::get('/map-heads/get-student-counts', 'AdminPanel\MapHeadsController@get_student_counts');

    // For Library Reports
    Route::get('/library-report', 'AdminPanel\MenuController@library_reports');
    Route::get('/library-report/registered-student-report', 'AdminPanel\LibraryReportController@registeredStudent');
    Route::get('/library-report/registered-student-report-data', 'AdminPanel\LibraryReportController@anyDataRegisteredStudent');
    Route::get('/library-report/books-report', 'AdminPanel\LibraryReportController@getBooks');
    Route::get('/library-report/books-report-data', 'AdminPanel\LibraryReportController@anyDataBooks');
    Route::get('/library-report/issued-books-report', 'AdminPanel\LibraryReportController@issuedBooks');
    Route::get('/library-report/issued-books-report-data', 'AdminPanel\LibraryReportController@anyDataIssuedBooks');
    Route::get('/library-report/get-member-name-data', 'AdminPanel\LibraryReportController@getMemberName');
    Route::get('/library-report/dues-report', 'AdminPanel\LibraryReportController@duesReport');
    Route::get('/library-report/dues-report-data', 'AdminPanel\LibraryReportController@anyDataDues');


    // For Plan Schedule
    Route::get('/staff/plan-schedule/add-plan-schedule/{id?}', 'AdminPanel\PlanScheduleController@add');
    Route::post('/staff/plan-schedule/save/{id?}', 'AdminPanel\PlanScheduleController@save');
    Route::get('/staff/plan-schedule/view-plan-schedule', 'AdminPanel\PlanScheduleController@index');
    Route::get('/staff/plan-schedule/data', 'AdminPanel\PlanScheduleController@anyData');
    Route::get('/staff/plan-schedule/delete-plan-schedule/{id?}', 'AdminPanel\PlanScheduleController@destroy');
    Route::get('/staff/plan-schedule/plan-schedule-status/{status?}/{id?}', 'AdminPanel\PlanScheduleController@changeStatus');

    // For Staff Plan Schedule @Sandeep on 21 Feb 2019
    Route::get('/plan-schedule','AdminPanel\PlanScheduleController@staff_plan_schedule');
    Route::get('/plan-schedule-data','AdminPanel\PlanScheduleController@staff_plan_schedule_data');
   
    // For Payroll Leave Scheme
    Route::get('/payroll/manage-leave-scheme/{id?}','AdminPanel\PayrollLeaveSchemeController@add'); 
    Route::post('/payroll/manage-leave-scheme-save/{id?}','AdminPanel\PayrollLeaveSchemeController@save');
    Route::get('/payroll/manage-leave-scheme-view','AdminPanel\PayrollLeaveSchemeController@anyData');
    Route::get('/payroll/delete-manage-leave-scheme/{id?}','AdminPanel\PayrollLeaveSchemeController@destroy');
    // Route::get('/payroll/manage-leave-scheme/view-map-employees/{id?}','AdminPanel\PayrollLeaveSchemeController@viewMapEmployees');
    Route::get('/payroll/manage-leave-scheme-employees-data','AdminPanel\PayrollLeaveSchemeController@getLschemeStaffData');

    
    // For Hostel Fees Receipt
    Route::get('/hostel/view-fees-receipt', 'AdminPanel\HostelFeesReceiptController@index');
    Route::get('/hostel/fees-receipt-data', 'AdminPanel\HostelFeesReceiptController@anyData');
    Route::get('/hostel/fees-receipt-status/{status?}/{id?}', 'AdminPanel\HostelFeesReceiptController@changeStatus');
    Route::get('/hostel/fee-receipt/{id?}', 'AdminPanel\HostelFeesReceiptController@fees_receipt');

    Route::any('/hostel/fees-due-report', 'AdminPanel\HostelController@feesDueAnyData');

    // For Staff Leave URL
    Route::get('/staff-leave-application/manage-leave-data/{id?}','AdminPanel\StaffLeaveApplicationController@manageLeave');
    Route::get('/staff-leave-application/staff_leave-history-data/{id?}','AdminPanel\StaffLeaveApplicationController@leaveHistoryData');
    // Route::get('/staff-leave-application/manage-model-data','AdminPanel\StaffLeaveApplicationController@getStaffLeaveData');
    Route::any('/staff-leave-application/get_paid_leave_status/{staff_leaves?}/{status?}','AdminPanel\StaffLeaveApplicationController@changePaidLeaveStatus');
    Route::any('/staff-leave-application/get-model-data','AdminPanel\StaffLeaveApplicationController@saveModelData');
    Route::get('staff-leave-application/get-staff-leave-data','AdminPanel\StaffLeaveApplicationController@getStaffData');
    // For Salary Generation
    Route::get('/payroll/manage-salary-generation','AdminPanel\SalaryGenerationController@manageSalaryGeneration');
    
    // Master module Permissions
    Route::get('/permissions', 'AdminPanel\PermissionsController@index');
    Route::post('/save-permissions/{id?}', 'AdminPanel\PermissionsController@save');

    // For Substitute Management
    Route::get('/substitute-management', 'AdminPanel\MenuController@substitute_management');
    Route::get('/substitute-management/manage-substitution','AdminPanel\SubstitutionController@index');
    Route::get('/substitute-management/data', 'AdminPanel\SubstitutionController@anyData');
  
    // For System Configuration
    Route::get('/system-configuration/add-system-configuration/{id?}', 'AdminPanel\SystemConfigurationController@add'); 
    Route::post('/system-configuration/add-system-configuration/save/{id?}', 'AdminPanel\SystemConfigurationController@save');

    // For Account Module
    Route::get('/account','AdminPanel\MenuController@account');
    Route::get('/account/manage-accounts-group/{id?}','AdminPanel\AccGroupController@add');
    Route::post('/account/manage-accounts-group-save/{id?}','AdminPanel\AccGroupController@save');
    Route::get('/account/manage-accounts-group-view','AdminPanel\AccGroupController@anyData');
    Route::get('/account/manage-accounts-group-status/{status?}/{id?}','AdminPanel\AccGroupController@changeStatus');
    Route::get('/account/delete-manage-accounts-group/{id?}','AdminPanel\AccGroupController@destroy');

    // For Hostel Allocation Ajax 
    Route::get('/hostel/rooms/get-category-room-data/{id?}', 'AdminPanel\HostelController@getCateRoomData');
    // Download Homework attechment
    Route::get('/download-homework-attechment/{id}', 'AdminPanel\MySubjectsController@download_homework_attechment');
  
    // For Staff Roles
    Route::get('/staff/manage-staff-roles', 'AdminPanel\StaffRolesController@index');
    Route::get('/staff-role/data', 'AdminPanel\StaffRolesController@anyData');
    Route::get('/staff/staff-role-status/{status?}/{id?}', 'AdminPanel\StaffRolesController@changeStatus');
    


    // Download Homework attechment
    Route::get('/download-homework-attechment/{id}', 'AdminPanel\MySubjectsController@download_homework_attechment');
    
    // Allocate subsitute
    Route::get('/substitute-management/allocate/{id}/{subid?}','AdminPanel\SubstitutionController@allocate');
    Route::post('/substitute-management/save-allocate/{id}/{subid?}','AdminPanel\SubstitutionController@save_allocate');
    Route::get('/substitute-management/view-allocations/{id}','AdminPanel\SubstitutionController@view_allocations');
    Route::get('/substitute-management/get-sections/{id?}', 'AdminPanel\SubstitutionController@getSectionData');
    Route::get('/substitute-management/get-lectures/{id?}', 'AdminPanel\SubstitutionController@getLectureData');
    Route::get('/substitute-management/get-staff/{id?}', 'AdminPanel\SubstitutionController@getstaffData');
    Route::get('/substitute-management/allocation-data', 'AdminPanel\SubstitutionController@allocationData');
 
    // For Cupboard Shelf
    Route::get('/cupboard/cupboard-shelf', 'AdminPanel\BookCupboardController@cupboardshelf');
    Route::get('/cupboard/delete-cupboard-shelf/{id?}', 'AdminPanel\BookCupboardController@destroyCupboardshelf');

    // Report
    Route::get('/substitute-management/substitute-report', 'AdminPanel\SubstitutionController@substitute_report');
    Route::get('/substitute-management/substitutedata', 'AdminPanel\SubstitutionController@substituteData');
    Route::get('/substitute-management/missed-substitute-report', 'AdminPanel\SubstitutionController@missed_substitute_report');

    // Generate Salary
    Route::get('/payroll/salary-generation','AdminPanel\SalaryGenerationController@generate_salary');
    
    //  Manage Group Heads
    Route::get('/account/acc-groups/{acc_sub_head_id?}', 'AdminPanel\AccGroupController@getAccGroupData');
    Route::get('/account/manage-accounts-head/{id?}','AdminPanel\AccGroupHeadController@add');
    Route::post('/account/manage-accounts-head-save/{id?}','AdminPanel\AccGroupHeadController@save');
    Route::get('/account/manage-accounts-head-view','AdminPanel\AccGroupHeadController@anyData');
    Route::get('/account/manage-accounts-head-status/{status?}/{id?}','AdminPanel\AccGroupHeadController@changeStatus');
    Route::get('/account/delete-manage-accounts-head/{id?}','AdminPanel\AccGroupHeadController@destroy');

    //  Manage Opening Balance
    Route::get('/account/acc-group-heads/{acc_sub_head_id?}', 'AdminPanel\AccGroupHeadController@getAccGroupHData');
    Route::get('/account/manage-opening-balance/{id?}','AdminPanel\AccOpenBalanceController@add');
    Route::post('/account/manage-opening-balance-save/{id?}','AdminPanel\AccOpenBalanceController@save');
    Route::get('/account/manage-opening-balance-view','AdminPanel\AccOpenBalanceController@anyData');
    Route::get('/account/delete-manage-opening-balance/{id?}','AdminPanel\AccOpenBalanceController@destroy');

  });
  

    // Online Recruitment Form @sandeep 0n 13 Feb 2019
    Route::get('recruitment-form/{job_id}/{job_form_id}', 'JobFormController@recruitment_form');
    Route::post('recruitment-form/save', 'JobFormController@save');

    // Admission Form @khushbu on 24 Feb 2019
    Route::get('admission-form/{id?}', 'AdmissionController@showForm');
    Route::post('admission-form/save-data-form/{id?}', 'AdmissionController@saveDataForm');
    Route::get('enquiry-form/{id?}', 'AdmissionController@showForm');
    Route::post('enquiry-form/save-data-form/{id?}', 'AdmissionController@saveDataForm');
    Route::get('state/show-state/{id?}', 'AdminPanel\StateController@showStates');
    Route::get('city/show-city/{id?}', 'AdminPanel\CityController@showCity');

    // Certificate @sandeep 0n 28 Feb 2019
    Route::get('/student/transfer-certificate/{id}', 'AdminPanel\TcController@tc_certificate');
    Route::get('/student/character-certificate/{id}', 'AdminPanel\CcController@cc_certificate');
    Route::get('/competition/certificate/{competitionmapid?}', 'AdminPanel\CompetitionController@certificate');

    // Cronjob for exams notification
    Route::get('cronjob/exam-day-notification', 'Cronjob\ExamController@exam_day_notification');

