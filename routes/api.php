<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/clear-cache', function() {
$exitCode = Artisan::call('config:cache');
return $exitCode;
});

/* ---------------- Common API ---------------- */

//Get school details
Route::any('generat-api-token', 'Api\ApiController@generateToken');

//Login 
Route::any('login', 'Api\ApiController@login');

//Get school details
Route::any('get-school-details', 'Api\ApiController@getSchoolDetails');

//Get profile details
Route::any('get-profile-details', 'Api\ApiController@getProfileDetails');

//Get all data
Route::any('get-all-data', 'Api\ApiController@getAllData');

//Change Password
Route::any('change-password', 'Api\ApiController@changePassword');

//Forgot Password
Route::any('forgot-password', 'Api\ForgotPasswordController@forgotPassword');

//get Message
Route::any('edit-profile', 'Api\EditProfileController@editProfile');

/* ---------------- Staff API ---------------- */

//Get staff details
Route::any('get-staff-details', 'Api\StaffController@getStaffDetails');

//Get single staff details
Route::any('get-single-staff-details', 'Api\StaffController@getSingleStaffDetails');

//Get staff shifts
Route::any('get-staff-shifts', 'Api\StaffController@getStaffShifts');

//Get staff Schedule
Route::any('get-staff-schedule', 'Api\StaffController@getStaffSchedule');


/* ---------------- Student API ---------------- */

//Get student class
Route::any('get-student-class', 'Api\StudentController@getStudentClass');

//Get student basic details
Route::any('get-student-details', 'Api\StudentController@getStudentsDetails');

//Get student basic details
Route::any('get-single-student-details', 'Api\StudentController@getSingleStudentDetail');

/* ---------------- Documents API ---------------- */

//Get documents
Route::any('get-documents', 'Api\DocumentsController@getDocuments');

/* ---------------- Leaves API ---------------- */

//Get leaves
Route::any('get-leaves', 'Api\LeavesController@getLeaves');

//update leaves status
Route::any('update-leaves-status', 'Api\LeavesController@updateStatus');

/* ---------------- Attendance API ---------------- */

//Get attendance date wise
Route::any('get-attendance-date-wise', 'Api\AttendanceController@getAttendanceDateWise');

//Get attendance
Route::any('get-attendance', 'Api\AttendanceController@getAttendance');

/* ---------------- HomeWork Group API ---------------- */

//Get homework group
Route::any('get-homework-group', 'Api\HomeworkGroupController@getHomeworkGroup');

//Get homework group details
Route::any('get-homework-group-details', 'Api\HomeworkGroupController@getHomeworkGroupDetails');

//Get homework with details
Route::any('get-homework-with-details', 'Api\HomeworkGroupController@getHomeworkWithDetails');

//Staff save homework
Route::any('staff-save-homework', 'Api\HomeworkGroupController@saveStaffHomework');

/* ---------------- Exams API ---------------- */

//Get exams schedule
Route::any('get-exams-schedule', 'Api\ExamsController@getExamSchedule');

//Get view schedule
Route::any('get-schedule', 'Api\ExamsController@getSchedule');

//Get subjects of schedule
Route::any('get-subjects-of-schedule', 'Api\ExamsController@getSubjectOfSchedule');

//Get mark-of-students
Route::any('get-mark-of-students', 'Api\ExamsController@getMarksOfStudents');

//Save Student Marks
Route::any('save-student-marks', 'Api\ExamsController@saveStudentMarks');


/* ---------------- Fees API ---------------- */

//Get student fees
Route::any('get-student-fees', 'Api\FeesController@getStudentFees');

//Get fees
Route::any('get-fees', 'Api\FeesController@getFees');

//Get concession data
Route::any('get-concession-data', 'Api\FeesController@getConcessionData');

//Save concession data
Route::any('save-concession-data', 'Api\FeesController@saveConcessionData');

//Get concession reason
Route::any('get-concession-reason', 'Api\FeesController@getConcessionReason');

//Get student fees details
Route::any('get-student-fees-details', 'Api\FeesController@getStudentFeesDetails');

//Get latest fees details
Route::any('get-latest-fees-details', 'Api\FeesController@getLatestFeesDetails');

//Get paid fees details
Route::any('get-paid-fees-details', 'Api\FeesController@getPaidFeesDetails');

/* ---------------- Remarks API ---------------- */

//Get all remarks
Route::any('get-all-remarks', 'Api\ApiController@getAllRemarks');

/* ---------------- Parent's API ---------------- */

//Get Parent Details
Route::any('get-parent-details', 'Api\ParentController@getParentDetails');

//Get Remarks
Route::any('get-remarks', 'Api\ParentController@getRemarks');

//Save Student Leave
Route::any('save-student-leave', 'Api\ParentController@saveStudentLeave');

//Get Student Marks
Route::any('get-student-marks', 'Api\ParentController@getStudentMarks');

//Get all parent list
Route::any('get-all-parent-list', 'Api\ParentController@getAllParentList');


/* ---------------- Competition API ---------------- */

//Get student certificates
Route::any('get-student-certificates', 'Api\CertificateController@getStudentCertificates');


/* ---------------- QuestionPaper API ---------------- */

//Get all question paper
Route::any('get-question-paper', 'Api\QuestionPaperController@getQuestionPaper');

//Add/Edit question paper
Route::any('save-question-paper', 'Api\QuestionPaperController@saveQuestionPaper');

//Delete question paper
Route::any('delete-question-paper', 'Api\QuestionPaperController@deleteQuestionPaper');

//Change status question paper
Route::any('change-status-question-paper', 'Api\QuestionPaperController@changeStatusQuestionPaper');

/* ---------------- Notes API ---------------- */

//Get all online notes
Route::any('get-online-notes', 'Api\NotesController@getNotes');

//Add/Edit online notes
Route::any('save-online-notes', 'Api\NotesController@saveNotes');

//Delete online notes
Route::any('delete-online-notes', 'Api\NotesController@deleteNotes');

//Change status notes
Route::any('change-status-notes', 'Api\NotesController@changeStatusNotes');

/* ---------------- Visitors API ---------------- */

//Get all visitors
Route::any('get-visitors', 'Api\VisitorsController@getVisitors');

/* ---------------- TimeTable API ---------------- */

//Get time table
Route::any('get-time-table', 'Api\TimeTableController@getTimeTable');

/* ---------------- Library API ---------------- */

//Get Books
Route::any('get-books', 'Api\LibraryController@getBooks');

//Get Book Details
Route::any('get-book-details', 'Api\LibraryController@getBookDetails');

//Get library member
Route::any('get-library-member', 'Api\LibraryController@getLibraryMenber');

//Get issued book
Route::any('get-issued-book', 'Api\LibraryController@getIssuedBook');


/* ---------------- Hostel API ---------------- */

//Get hostel registered students
Route::any('get-hostel-registered-students', 'Api\HostelController@getStudentForHostel');

//Register student for hoster
Route::any('registernew-hostel-student', 'Api\HostelController@RegisterHostelStudent');

//Get hostel allocated student list
Route::any('get-hostel-allocated-student-list', 'Api\HostelController@getHostelAllocatedStudentList');

//Allocate hostel student
Route::any('allocate-hostel-student', 'Api\HostelController@allocateHostelStudent');

//Leave hostel room
Route::any('leave-hostel-room', 'Api\HostelController@leaveHostelRoom');

//Leave hostel room
Route::any('get-room-students', 'Api\HostelController@getRoomStudent');

//Get hostel data
Route::any('get-hostel-data', 'Api\HostelController@getHostelData');

/* ---------------- Admission API ---------------- */

//Get form
Route::any('get-form', 'Api\AdmissionController@getForm');

//Get Applicant
Route::any('get-applicant', 'Api\AdmissionController@getApplicant');

//Get Applicant Details
Route::any('get-applicant-details', 'Api\AdmissionController@getApplicantDetails');


/* ---------------- Notice API ---------------- */

//Get notice
Route::any('get-notice', 'Api\NoticeController@getNotices');

//Save notice
Route::any('save-notice', 'Api\NoticeController@saveNotices');

//Delete notice
Route::any('delete-notice', 'Api\NoticeController@deleteNotices');


/* ---------------- Staff App API ---------------- */

//Get staff profile 
Route::any('staff-profile', 'Api\StaffController@staffProfile');

//Update Staff
Route::any('update-staff', 'Api\StaffController@update_staff');

//Change Staff Leave Status
Route::any('change-staff-leave-status', 'Api\StaffController@changeStaffLeaveStatus');

//Mark Attendace
Route::any('mark-attendace', 'Api\StaffController@marksAttendace');

//Staff add remarks
Route::any('staff-save-remark', 'Api\StaffController@staffAddRemarks');

//Get Staff Subject
Route::any('get-staff-subject', 'Api\StaffController@getStaffSubjects');

//Save Staff Homework
Route::any('save-staff-homework', 'Api\StaffController@saveStaffHomework');

//Save Staff Leave
Route::any('save-staff-leave', 'Api\StaffController@saveStaffLeave');

//Staff delete remarks
Route::any('staff-delete-remark', 'Api\StaffController@staffDeleteRemarks');

//Staff exam duty
Route::any('staff-exam-duty', 'Api\StaffController@staffExamDuty');


/* ---------------- Communication API ---------------- */

//Send Message
Route::any('send-communication-message', 'Api\CommunicationController@sendCommunicationMessage');

//get Message
Route::any('get-communication-message', 'Api\CommunicationController@getCommunicationMessage');


/* ---------------- Notification API ---------------- */

//Change Notification Status
Route::any('change-notification-status', 'Api\NotificationController@changeNotificationStatus');

//Get Notification 
Route::any('get-notification', 'Api\NotificationController@getNotification');


/* ---------------- Recruitment API ---------------- */

//Get Jobs 
Route::any('get-jobs', 'Api\RecruitmentController@getJobList');

//Get Candidate 
Route::any('get-job-candidates', 'Api\RecruitmentController@getCandidateofJob');

//Get Interview Schedule 
Route::any('get-interview-schedule', 'Api\RecruitmentController@getInterviewSchedule');

//Get Selected Candidate
Route::any('get-selected-candidate', 'Api\RecruitmentController@getSelectedCandidate');

//Schedule Interview
Route::any('schedule-interview', 'Api\RecruitmentController@scheduleInterview');

//Candidate Details 
Route::any('candidate-detail', 'Api\RecruitmentController@candidateDetail');


/* ---------------- Transport API ---------------- */

//Get Vehicle 
Route::any('get-vehicle', 'Api\TransportController@getVehicleList');

//Get Attendance Vehicle 
Route::any('get-vehicle-attendance', 'Api\TransportController@getVehicleAttendance');

//Save Attendance Vehicle 
Route::any('mark-vehicle-attendance', 'Api\TransportController@markVehicleAttendance');


/* ---------------- Plan Schedule ---------------- */
/* ------------- Sandeep 19 Feb 2019 ------------- */

//Add/Edit Plan Schedule
Route::any('save-plan-schedule', 'Api\PlanScheduleController@savePlanSchedule');

//Get all Plan Schedule
Route::any('get-plan-schedule', 'Api\PlanScheduleController@getPlanSchedule');

//Delete Plan Schedule
Route::any('delete-plan-schedule', 'Api\PlanScheduleController@deletePlanSchedule');

//Change Plan Schedule
Route::any('change-plan-schedule', 'Api\PlanScheduleController@changeStatusPlanSchedule');


/* ---------------- Task Manager ---------------- */
/* ------------- Sandeep 22 Feb 2019 ------------- */

//Add/Edit Task Manager
Route::any('save-task-manager', 'Api\TaskManagerController@saveTaskManager');

//Get all Task Manager
Route::any('get-task-manager', 'Api\TaskManagerController@getTaskManager');

//Delete Task Manager
Route::any('delete-task-manager', 'Api\TaskManagerController@deleteTaskManager');

//Change Task Manager
Route::any('change-task-manager', 'Api\TaskManagerController@changeStatusTaskManager');

//Add Task Response
Route::any('add-task-response', 'Api\TaskManagerController@add_task_response');

//View Task Response
Route::any('view-task-response', 'Api\TaskManagerController@view_task_response');

/* ---------------- Communication ---------------- */
/* ------------- Sandeep 25 Feb 2019 ------------- */

//Get staff details
Route::any('get-communication-staff', 'Api\StaffController@getCommunicationStaff');

//Get all parent list
Route::any('get-communication-parent', 'Api\StaffController@getCommunicationParent');

//Get student certificates
Route::any('student-academic-certificates', 'Api\CertificateController@StudentAcademicCertificates');

//View Tabsheet
Route::any('view-tabsheet', 'Api\ExamsController@view_tabsheet');


//View Country
Route::any('view-country', 'Api\CountryController@view_country');

//View State
Route::any('view-state', 'Api\StateController@view_state');

//View City
Route::any('view-city', 'Api\CityController@view_city');
